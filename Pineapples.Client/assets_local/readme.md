A few words on assets_local and what is contains
_
The contents of this folder are NOT deployed to client sites. This folder holds site-specific resources. 
Some of it is held in github but not all. For example, Google Service Account credential keys in the JSON format are
*not* committed to the public repository for security reasons. If they were, Google would actually render them locked and unsable.
An example of a Google Service Account credential *is* committed in google_credentials.json but it is a dummy example only.
The developer would need to replace that with a valid key for the Cloudfiles feature of the EMIS to work in development
In production you would also manually go put the key there for the appropriate Google Service account. 
Otherwise, those keys must be kept securely on the production server and on developer machine with care not to expose them publicly.

The content in here is also 'deployed' to Pineapples\assets_local\_ from Pineapples.Client\assets_local\ for testing. It is
important that the Dropbox script *does not* sync this to Dropbox and production server (which it does *not*).