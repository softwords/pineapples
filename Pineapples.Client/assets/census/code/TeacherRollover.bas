' TeacherRollover
' Version date: 20201102
' Version msg: Rollover

Option Explicit

'' roll over a single row read from the src table, into the dest table

''
'' the rollover depends on these fields of the Src
'' - Completed
'' - Outcome
'' - Grade

'' COMPLETED - NO
'' -> Outcome = Repeat - Rollover Grade is current grade, school is current school
'' -> any other - no rollover

'' COMPLETED - YES
'' Grade = Last Grade -> no rollkover created
'' Otherwise - rollover grade is next grade, school is current school

'' TRANSFERS - transfers are then manually adjusted

Sub RollFilteredTeacherList(dest As ListObject, src As ListObject, Optional filterColumn As String = "", Optional filterValue As Variant)

'' first get a dictionary of all the fileds  we want to copy
'' ie fields that exist in both dest and src, and are not calculated

Dim col As ListColumn
Dim colName As Variant

Dim matches As Collection
Set matches = New Collection
For Each col In dest.ListColumns
    ' is it calculated?
    Dim r As Range
    Set r = col.DataBodyRange(1, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    ' its calculated - don't copy it
    Else
        ' mark to copy if it is in the source as well
        Dim srcCol As ListColumn
        For Each srcCol In src.ListColumns
            If srcCol.Name = col.Name Then
                matches.Add col.Name
                Exit For
            End If
        Next
    End If
Next


'' now step through the rows in src, moving the data into dest

Dim srcRow As ListRow
Dim destRow As ListRow
Dim isAdding As Boolean
Dim continue As String

Dim srcRowCount As Integer
srcRowCount = src.ListRows.Count

Dim destIdx As Integer

' aim for better performance by turning off screen updates and recalc
' be sure to turn on again in event of error

On Error GoTo ErrorHandler

Dim idx As Integer
If filterColumn <> "" Then
    idx = src.ListColumns(filterColumn).Index
End If

Application.ScreenUpdating = False
Application.Calculation = xlCalculationManual

For Each srcRow In src.ListRows
    ' are we adding or updating? to do...
    '' first, look at the source to see if this is a row that needs to be rolled
    '' we don;t roll over this row if:
    '' teacher is Inactive
    ''
    If srcRow.Index Mod 100 = 0 Then
        Debug.Print Now
        Application.StatusBar = "Staff Rollover " & srcRow.Index & " of " & srcRowCount
        If srcRow.Index Mod 1000 = 0 Then
           Application.Calculate
        End If
        Application.ScreenUpdating = True
        DoEvents
        Application.ScreenUpdating = False
    End If
    
    If filterColumn = "" Then
        continue = isTeacherContinuing(src, srcRow.Index)
    ElseIf srcRow.Range.Cells(1, idx).Value = filterValue Then
        
        continue = isTeacherContinuing(src, srcRow.Index)
    Else
        continue = "X"          ' mark those we are not interested in reporting as non-matches
    End If
    Select Case continue
        Case "Y"

            Set destRow = dest.ListRows.Add(, True)
        
            '' step through the matched columns, and move the value from srcRow to destRow
            For Each colName In matches
                Select Case colName
                   '' allow for potential translation / mapping of columns later if required
                        
                    Case Else
                    ''dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) =
                        ''Debug.Print colName
                        destRow.Range.Cells(1, dest.ListColumns(colName).Index) = _
                            src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
                End Select
            Next
    Case ""
        Debug.Print "Not Continue: ", srcRow.Index, src.ListColumns("Full Name").DataBodyRange.Cells(srcRow.Index, 1), _
        src.ListColumns("Employment Status").DataBodyRange.Cells(srcRow.Index, 1), _
        src.ListColumns("Reason").DataBodyRange.Cells(srcRow.Index, 1), _
        src.ListColumns("Date of Exit").DataBodyRange.Cells(srcRow.Index, 1)
    End Select

Next
    
Application.ScreenUpdating = True
Application.CalculateFull
Application.Calculation = xlCalculationAutomatic

Exit Sub

ErrorHandler:
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "An error occured rolling over staff data: " & Err.Description, vbCritical, "Rollover"
End Sub


Function isTeacherContinuing(src As ListObject, srcRow As Integer) As String

Dim status As String
status = listValue(src, srcRow, "Employment Status")
If (status) = "Inactive" Then
    isTeacherContinuing = "N"
Else
    isTeacherContinuing = "Y"
End If

End Function



Function doTeacherRollover()
' merge the other lists
Dim selectedSchool As String
Dim dest As ListObject
Dim src As ListObject
Dim wkName As String

wkName = Range("selectedWorkbookName")

selectedSchool = ""
''selectedSchool = "Bael Elementary School"
''selectedSchool = "Colonia Middle School"

shtStaff.Unprotect
Set dest = shtStaff.ListObjects(1)
Set src = getListObject(wkName, "SchoolStaff")
If selectedSchool <> "" Then
    MergeFilteredListObject dest, src, "Employment Status", "Active", "School Name", selectedSchool
Else
    MergeFilteredListObject dest, src, "Employment Status", "Active"
End If
doProtect shtStaff
End Function

