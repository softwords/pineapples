' AppEvents
' Version date: 20201102
' Version msg: Rollover

Option Explicit

Public WithEvents app As Application


Private Sub app_WorkbookBeforeClose(ByVal wb As Workbook, Cancel As Boolean)
Application.CalculateFull
End Sub

Private Sub app_WorkbookBeforeSave(ByVal wb As Workbook, ByVal SaveAsUI As Boolean, Cancel As Boolean)
Application.CalculateFull
End Sub

Private Sub app_WorkbookOpen(ByVal wb As Workbook)
Application.CalculateFull
End Sub


