' shtStaff
' Version date: 20201102
' Version msg: Rollover

Private Sub Worksheet_Change(ByVal Target As Range)

Dim dv As Validation

Set dv = Target.Validation
On Error Resume Next
Dim f As String
f = dv.Formula1
If Err Then Exit Sub
If f = "X,x" Then
    Application.EnableEvents = False
    Target.Value = UCase(Target.Value)
End If
Application.EnableEvents = True
End Sub
