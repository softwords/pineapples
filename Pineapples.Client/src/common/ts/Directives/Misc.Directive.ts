﻿namespace Pineapples {

	// The quickest way to move sw-directives.js directives here in TypeScript
	// 

	angular
		.module('sw.common')

		.directive('fitContent', ['$window', '$timeout', '$location', function ($window, $timeout, $location) {
			return {
				restrict: 'A',
				link: function (scope, element, attr) {

					var setHeight = function () {
						// this is very specifc to the current layout
						var diff = $('#pineapples-license-footer').parent().height() + element.offset().top + 20;

						var newHeight = ($(window).height() - diff);

						if (newHeight > 0) {
							element.css('height', newHeight + 'px');
						};
					};

					$(window).on('resize', _.debounce(setHeight, 100));
					//function () {
					//setHeight();
					//$timeout(setHeight, 0, false)

					// });
					// also check on a view change, becuase panels may have come in or out
					scope.$on('$viewContentLoaded', function () {
						//does this even get called anymore?
						$timeout(setHeight, 0, false);
						//setHeight();
					});
					$timeout(setHeight, 0, false)
					//setHeight();
					//setHeight();
					//$timeout(, 1000);
				}
			};
		}])

		.directive('pdfDownload', function () {
			// jQuery needed, uses Bootstrap classes, adjust the path of templateUrl
			return {
				restrict: 'E',
				//templateUrl: '/path/to/pdfDownload.tpl.html',
				template: '<a href="" class="btn btn-primary" ng-click="downloadPdf()">Download</a>',
				scope: true,
				link: function (scope, element, attr) {
					var anchor = element.children()[0];

					// When the download starts, disable the link
					scope.$on('download-start', function () {
						$(anchor).attr('disabled', 'disabled');
					});

					// When the download finishes, attach the data to the link. Enable the link and change its appearance.
					scope.$on('downloaded', function (event, data) {
						$(anchor).attr({
							href: 'data:application/pdf;base64,' + data,
							download: attr.filename
						})
							.removeAttr('disabled')
							.text('Save')
							.removeClass('btn-primary')
							.addClass('btn-success');

						// Also overwrite the download pdf function to do nothing.
						scope.downloadPdf = function () {
						};
					});
				},
				controller: ['$scope', '$attrs', '$http', function ($scope, $attrs, $http) {
					$scope.downloadPdf = function () {
						$scope.$emit('download-start');
						$http.get($attrs.url).then(function (response) {
							$scope.$emit('downloaded', response.data);
						});
					};
				}]
			};
		})

		//http://stackoverflow.com/questions/28114970/angularjs-ng-options-using-number-for-model-does-not-select-initial-value
		.directive('toNumber', function () {
			return {
				require: 'ngModel',
				link: function (scope, element, attrs, ngModel: any) {
					ngModel.$parsers.push(function (val) {
						return val ? parseInt(val, 10) : null;
					});
					ngModel.$formatters.push(function (val) {
						return val ? '' + val : null;
					});
				}
			};
		})

}