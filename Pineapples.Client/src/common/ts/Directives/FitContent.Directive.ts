﻿namespace Sw.Directives {

	class FitContent implements ng.IDirective {

		public restrict = "A";		
		public link: any;

		constructor($window, $timeout, $location) {
			FitContent.prototype.link = (scope: ng.IScope, element, attrs) => {

				var setHeight = function () {
					// this is very specifc to the current layout
					var diff = $('#pineapples-license-footer').parent().height() + element.offset().top + 20;

					var newHeight = ($(window).height() - diff);

					if (newHeight > 0) {
						element.css('height', newHeight + 'px');
					};

				};

				$(window).on('resize', _.debounce(setHeight, 100));
				//function () {
				//setHeight();
				//$timeout(setHeight, 0, false)

				// });
				// also check on a view change, becuase panels may have come in or out
				scope.$on('$viewContentLoaded', function () {
					//does this even get called anymore?
					$timeout(setHeight, 0, false);
					//setHeight();
				});
				$timeout(setHeight, 0, false)
				//setHeight();
				//setHeight();
				//$timeout(, 1000);
			};

		}

		public static factory() {
			let directive = ($window, $timeout, $location) => {
				return new FitContent($window, $timeout, $location);
			};
			directive['$inject'] = ['$window', '$timeout', '$location'];
			return directive;
		}

	}

	// This compiled and seemed to work but I packaged all remaining JS directives in Misc.Directive.ts instead
	//angular
	//	.module('sw.common')
	//	.directive('fitContent', FitContent.factory());

}
