﻿module Sw {
  //
  // ui.Router configure
  //
  function urlRouterConfig($urlRouterProvider: ng.ui.IUrlRouterProvider) {

    // ui-router
    $urlRouterProvider.when('', '/home');
  };

	function caseInsensitive(urlService) {
		// this is the better way to set case insensitivity
		// has to be set before any states are created
		urlService.config.caseInsensitive(true);
	}

  function standardRoutes($stateProvider:ng.ui.IStateProvider) {

		// ----------- 'site' state ------------------------
		// This is the state sitting at the base of all authenticated system access

		// the base handler for permission redirection
		// if the user fails on permission 'authenticated' => goes to login
		// otherwise goes to 'unauthorized'
		let permissionRedirector = (rejectedPermission, transitionProperties) => {
			if (rejectedPermission == "authenticated") {
				return 'signin';
			}
			return {
				state: 'unauthorized',
				params: {
					rejectedPermission: rejectedPermission
				}
			}
		}

		let state:ng.ui.IState = {
			abstract: true,
			data: {
				permissions: {
					only: ["authenticated"],
					redirectTo: permissionRedirector
				}
			},
			resolve: {
				lookupsResolver: ['Lookups', function (Lookups) {
					return Lookups.init();
				}],
				rootPageTitle: ["lookupsResolver", "Lookups", (lookupsResolver, lookups: Sw.Lookups.LookupService) => {
					return lookups.byCode("sysParams", "APP_NAME", "N") || "Pacific EMIS";
				}],
				pageTitle: ["rootPageTitle", (rootPageTitle) => rootPageTitle]
			}
		}
		$stateProvider.state('site', state);

		//---------------------------- unauthorized ----------------------------
		// this is the informational response when a route is rejected by angular-permission

		 state = {
			params: {
				rejectedPermission: null
			},
			onEnter: ["identity", "$state", "$stateParams",
				(identity, $state, $stateParams: ng.ui.IStateParamsService) => {
					if (identity.isAuthenticated === false) {
						// how can this be?????
					} else {
						window.alert(`Not authorized: ${$stateParams.rejectedPermission}`);
						$state.reload();
					}
				}]
		}
		$stateProvider.state('unauthorized', state);

		$stateProvider
    .state('site.home', {
      url: '/home',
      // moving away from ng-token-auth
      //onEnter: ['$auth', '$state', function ($auth, $state) {
      //    $state.go($auth.user.home);
      onEnter: ["identity", "$state", function (identity, $state) {
        if (identity.isAuthenticated === false) {
          // how can this be?????
        } else {
          $state.go(identity.home);
        }
      }]
    })
    .state('signin', {

      url: '/signin',
      data: {
        roles: []
      },
      views: {
        '@': {
          templateUrl: 'user/login',
          controller: 'LoginController',
          controllerAs: 'vm'
        }
      },
      resolve: {
        lookupCache: ['Lookups', function (Lookups) {
          return Lookups.init();
        }]
      }
		})
.state('site.changepassword', {
  url: '/changepassword',
  views: {
    '@': {
      templateUrl: 'user/ChangePassword',
      controller: 'LoginController',
      controllerAs: 'vm'

    }
  }
})
.state('site.register', {
  url: '/register',
  views: {
    '@': {
      templateUrl: 'user/Register',
      controller: 'LoginController',
      controllerAs: 'vm'
    }
  }
})

.state('restricted', {
  parent: 'site',
  url: '/restricted',
  data: {
    roles: ['Admin']
  },
  views: {
    'content@': {
      templateUrl: 'user/accessrestricted'
    }
  }
}).state('accessdenied', {

		url: '/denied',
		data: {
			roles: []
		},
		views: {
			'@': {
				templateUrl: 'user/accessdenied'
			}
		}
	});
};

	
	//------------------ Handling for transitions ---------------------------
	function transitionHandlerConfig(transitions, ) {
		transitions.onSuccess({}, function (transition) {
			console.log(
				"Successful Transition from " + transition.from().name +
				" to " + transition.to().name
			);
			let timeout: ng.ITimeoutService = transition.injector().get("$timeout")
			let pageTitle = transition.injector().get('pageTitle');
			timeout(() => pageTitle, 0).then((pageTitle) => {
				window.document.title = pageTitle;
			});
		});

		let errfn = (transition => {
			console.log(`Error on transition from ${transition.from().name} to ${transition.to().name}: ${transition.error().reason}`);
			console.log(transition.error());
			return false;
			
		})
		transitions.onError({}, errfn);
	}
	function stateHandlerRun(state) {
		state.defaultErrorHandler(function () {
			// Do not log transitionTo errors
		});
	}

  angular
		.module('sw.common')
		.config(["$urlServiceProvider", caseInsensitive])
    .config(['$urlRouterProvider', urlRouterConfig])
    .config(['$stateProvider', standardRoutes])
	  .config(['$transitionsProvider', transitionHandlerConfig])
		.run(['$state', stateHandlerRun])
}