﻿namespace Sw {

  let checkmark = (value) => {
    if (value == null || value == '')
      return '';
    return value ? '\u2713' : '\u2718';
  };

  let block = (value) => {

    if (value == null || value == '')
      return '';
    return value ? '\u25A0' : '';
  };

  let z0 = (value) => {
    if (value) {
      return value;
    }
    return '';
  }

  let percentage = ($window) => {
    return function (input, decimals, suffix) {
      decimals = angular.isNumber(decimals) ? decimals : 3;
      suffix = suffix || '%';
      if ($window.isNaN(input)) {
        return '';
      }
      return Math.round(input * Math.pow(10, decimals + 2)) / Math.pow(10, decimals) + suffix
    };
  };

  let percif = ($window, $filter) => {
    return function (input, conditional, decimals, suffix) {
      decimals = angular.isNumber(decimals) ? decimals : 3;
      suffix = suffix || '%';
      if ($window.isNaN(input)) {
        return '';
      }
      if (conditional)
        return Math.round(input * Math.pow(10, decimals + 2)) / Math.pow(10, decimals) + suffix;
      else
        return $filter('number')(input, decimals);
    };
  };

  let html = ($sce) => {
    return function (val) {
      return $sce.trustAsHtml(val);
    };
  }

  /**
   * FinderResults - this will highlight the nominated text in a string, case-insensitive.
   * It is principally aimed at highlighting the search text in each row of the results in a Finder 
   */
  let finderResults = ($sce) => {
    return function (input, searchString) {   // input represents the text to ve formatted
      if (!searchString) {
        return $sce.trustAsHtml(input);
      }
      var regex = new RegExp('(' + searchString + ')', 'gi');         // global, case-insensitive
      var highlighted = input.replace(regex, '<span class="highlight">$1</span>');
      return $sce.trustAsHtml(highlighted);
    };
  }

  angular
    .module('sw.common')
    .filter('checkmark', function () { return checkmark; })
    .filter('block', function () { return block; })
    .filter('z0', function () { return z0; })
    .filter('percentage', ['$window', percentage])
    .filter('percif', ['$window', '$filter', percif])
    .filter('html', ["$sce", html])
    .filter('finderResults', ["$sce", finderResults]);
}