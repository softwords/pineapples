﻿namespace Sw.Config {

  class Controller  {
    public states: Array<ng.ui.IState>;

    static $inject = ["$state"]
    constructor(public state: ng.ui.IStateService) {
      this.states = state.get();
    }
    public $onChanges(changes) {
		}

		public pageTitle = (st: ng.ui.IState) => {
			if (st?.resolve?.['pageTitle']) {
				console.log(st.resolve.pageTitle);
				
				console.log(st?.resolve?.pageTitle[st?.resolve?.pageTitle.length]('...'));

				return st.resolve.pageTitle[st?.resolve?.pageTitle.length]('...');
			}
			return null;

		}
		
  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        log: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "common/stateaudit";
    }
  }

	class ControllerRestTest {
		public data;
		public cols;
		public endpoint: string
		public isWaiting: boolean;
		public info: any;
		public useReflator: boolean;
		public ignoreETag: boolean;			// do not send if-None-Match header
		static $inject = ["$state", "$http","reflator"]
		constructor(public state: ng.ui.IStateService, public http: ng.IHttpService
								, public reflator: Sw.Api.IReflator ) {
			
		}
		public $onChanges(changes) {
		}

		public getData() {
			this.isWaiting = true;
			this.info = null;
			let svc = (this.useReflator ? this.reflator : this.http);
			let configheaders = {};
			if (this.ignoreETag) {
				configheaders["If-None-Match"] = null;		// kill the default value
			}
			let config = { headers: configheaders };
			svc.get("api/warehouse/" + this.endpoint,config)
				.then(result => {
					this.data = result.data;
					if (this.data.length > 0) {
						this.cols = Object.keys(this.data[0]);
					}
					this.info = {
						status: result.status,
						statusText: result.statusText,
						numRecords: this.data.length,
						contentLength: result.headers("content-length"),
						eTag: result.headers("etag"),
						contentEncoding: result.headers("content-encoding")
					}

				})
				.catch(result => {
					this.data = null;
					this.info = {
						status: result.status,
						statusText: result.statusText,
						numRecords: null,
						contentLength: result.headers("content-length"),
						eTag: result.headers("etag"),
						contentEncoding: result.headers("content-encoding"),
						msg: ""
					}
				})
				.finally(() => {
					this.isWaiting = false;
				});
		}
	}
	class ComponentRestTest implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				log: '<'
			};
			this.controller = ControllerRestTest;
			this.controllerAs = "vm";
			this.templateUrl = "common/resttest";
		}
	}

  function routes(stateProvider: ng.ui.IStateProvider) {
    let state = {
        url: "^/admin/stateaudit",
        views: <{ [name: string]: ng.ui.IState }>{
          "@": {
            template: `<component-state-audit></component-state-audit>`
          }
        }
    }
    // while changing the Url, leave out of admin tree so it does not need admin security
    // this is becuase stateaudit can be a useful debugging tool
		stateProvider.state("site.stateaudit", state);

		state = {
			url: "^/admin/resttest",
			views: <{ [name: string]: ng.ui.IState }>{
				"@": "componentRestTest"
			}
		}
		stateProvider.state("site.resttest", state);
  }

  angular
    .module("sw.common")
		.component("componentStateAudit", new Component())
		.component("componentRestTest", new ComponentRestTest())
    .config(["$stateProvider", routes]);


}
