﻿namespace sw.common.maps {
  /*
  Base Map View helper

  this service provides handlkers for creating markers for maps
  These handlers are called by the MapController when required
  this is the default implementation which may be subclassed (object.Create())
  */
  // return the icon to represent the row data - ie the person or school or....

  interface IIconDef {
    default(): any;
  }
  interface IMapViewHelper {
    RowIcon(): any;
    MakeMarker(row, maptarget);
    Spiderfy: boolean;
    Cluster: boolean;
    Icons;
  }
  export class mapViewHelper {

    constructor() {

    }

    public RowIcon(row, ignoreColoc: boolean = false) {
      var icon = null;
      if (!ignoreColoc && (row.coloc > 1)) {

        return this.Icons.circle.default();

        //    };
      };
      switch (row.sGender) {
        case 'M':
          icon = this.Icons.mars;
          break;
        case 'F':
          icon = this.Icons.venus;
      }
      return icon.default();
    }

    public MakeMarker(row, maptarget) {
      var latLng = new google.maps.LatLng(row[this.LatField], row[this.LngField]);
      var icon;


      // add the markers to the spiderfier
      var marker = new google.maps.Marker({
        position: latLng,
        title: (row.coloc > 1 ? row.coloc + " " + this.EntityPlural + " here..." : row[this.TitleField]),
        map: maptarget,
        icon: this.RowIcon(row)
      });
      // extend google marker
      (<any>marker).row = row;

      return marker;
    }

    public Icons = {
      mars: {
        default: this.mars
      },
      venus: {
        default: this.venus
      },
      circle: {
        default: this.circle
      }
    };

    private _spiderfy: boolean = true;
    public get Spiderfy(): boolean {
      return this._cluster;
    };
    public set Spiderfy(value: boolean) {
      this._spiderfy = value;
    };

    private _cluster: boolean = true;
    public get Cluster(): boolean {
      return this._cluster;
    };
    public set Cluster(value: boolean) {
      this._cluster = value;
    };


    public LatField: string = 'sLat';
    public LngField: string = 'sLng';
    public TitleField: string = 'sFullName';
    public EntityPlural: string = "people";

    // interactivity
    private infoWindow: any;     // placehlder for a single, resued infowindow

    public InfoWindowOptions(row, marker, map) {
      var iw = {
        content: this.InfoWindowContent(row, marker, map)
      }
      return iw;
    }

    // return the html to use as the info window content

    public InfoWindowContent(row, marker, map) {
      return '<div>' + row[this.TitleField] + '</div>';
    };
    public OnMarkerClick(row, marker, map) {
      let self = this;
      function f() {
        if (self.infoWindow !== void 0) {    //see http://stackoverflow.com/questions/1291942/what-does-javascriptvoid0-mean
          self.infoWindow.close();
        }
        // create new window
        var infoWindowOptions = self.InfoWindowOptions(row, marker, map);
        self.infoWindow = new google.maps.InfoWindow(infoWindowOptions);
        self.infoWindow.open(map, marker);
      };
      return f;
    }

    // click event for spiderfier 
    // RETURNS the function(marker,event) that is passed a reference to the clicked marker

    public OnSpiderClick = (spiderfier, map) => {
      var self = this;
      function f(marker, event) {
        if (self.infoWindow !== void 0) {    //see http://stackoverflow.com/questions/1291942/what-does-javascriptvoid0-mean
          self.infoWindow.close();
        }
        // create new window
        var infoWindowOptions = self.InfoWindowOptions(marker.row, marker, map);
        self.infoWindow = new google.maps.InfoWindow(infoWindowOptions);
        self.infoWindow.open(map, marker);
      };
      return f;
    }

    // path: 'M 200 100 m0,300 v150 m -75,-75 h150 M 200 100 m 106 43  l106 -106 m -75 0 h 75 m0 75 v -85 M 200 100 a 150 150 0 1 0 0.1 0 Z ',
    private mars() {
      return {
        path: 'M 200 100 m 106 43  l106 -106 m -75 0 h 75 m0 75 v -85 M 200 100 a 150 150 0 1 0 0.1 0 Z ',

        //path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
        anchor: new google.maps.Point(200, 250),

        fillColor: '#f88',
        fillOpacity: 0.9,
        scale: .05,
        strokeColor: 'black',
        strokeWeight: 2
      };
    };

    private venus() {
      return {
        path: 'M 200 100 m0,300 v150 m -75,-75 h150 M 200 100 a 150 150 0 1 0 0.1 0 Z ',
        //path: ' m47,59H28m9.5,10V46.2a18.3,18.3 0 1,1 .1,0',
        //path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
        anchor: new google.maps.Point(200, 250),

        fillColor: '#88f',
        fillOpacity: 0.9,
        scale: .05,
        strokeColor: 'black',
        strokeWeight: 2
      };
    };

    private circle() {
      return {
        path: 'M 200 100 a 150 150 0 1 0 0.1 0 Z ',
        //path: ' m47,59H28m9.5,10V46.2a18.3,18.3 0 1,1 .1,0',
        //path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
        anchor: new google.maps.Point(200, 250),

        fillColor: '#DDD',
        fillOpacity: 0.9,
        scale: .07,
        strokeColor: 'black',
        strokeWeight: 2
      };
    };


    /// Interactivity

    // return a GM InfowWindowOptions object, base on the row /map / marker



    //// a 'static' method!
    //function create() {
    //  //this = basefilter here
    //  var fltr = Object.create(this);
    //  init.call(fltr);
    //  return fltr;
    //}


  }


  angular
    .module('sw.common')
    .service('baseMapView', mapViewHelper)
}