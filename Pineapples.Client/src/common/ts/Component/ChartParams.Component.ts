﻿/// <reference path="TableParams.Component.ts"/>
namespace Sw.Components {

	class ChartParamsController extends Sw.Components.TableParamsController {

		public palette: Sw.Maps.IColorBrewerPalette;
		public mapColorScale: Sw.Maps.IMapColorScale;

		static $inject = ["$scope", "Lookups", "$state"];
		constructor(scope: Sw.IScopeEx, public lookups: Sw.Lookups.LookupService
			, public state: ng.ui.IStateService) {
			super(scope, lookups, state);
		}
		public showDataOption() {
			return this.chartType === "map";
		}
		public showColumnOption() {
			return this.chartType !== "map";
		}
		public showChartOptions() {
			return true;
		}

	}

	class ChartParamsComponent implements ng.IComponentOptions {
		public bindings = {
			theFilter: "<",
			findConfig: "<",
			palette: "<",
			mapColorScale: "<"
		};
		public controller = ChartParamsController;
		public controllerAs = "vm";
		public templateUrl = "Generic/ChartParamsComponent";

	}

	angular
		.module('sw.common')
		.component("chartParams", new ChartParamsComponent())

}