﻿namespace Sw.Component {
  let trackingCols = [
    {
      field: "pEditUser", name: "EditUser", displayName: "Edited by...",
      editable: false,
      cellClass: "tracking",
      width: 120,
    },
    {
      field: "pEditDateTime", name: "EditDateTime", displayName: "edited on...",
      editable: false, cellFilter: 'date:"dd-MMM-yyyy HH:mm"',
      cellClass: "tracking",
      width: 120
    }
  ];
  // modelled on the ui-grid row header, but that cannot be turned off once it is on,
  // so fake it with a regular column
  let rowHeaderCol = {
    name: 'rowHeader', displayName: '',
    cellTemplate: '<row-edit-manager row="row"></row-edit-manager>',
    headerCellTemplate: `<div class=ui-grid-header-cell" ng-click="grid.appScope.vm.newRow()">
                            <span ng-show="grid.appScope.vm.canInsert()"><ng-md-icon icon="add" size=18 /></span></div`,
    editable: false, cellClass: "ui-grid-row-header-cell",
    width: 40, pinnedLeft: true
  };
  let deleteCol = {
    name: "Delete", displayName: '',
    cellTemplate: `<i class="material-icons md-18 md-dark" 
                    ng-click="grid.appScope.vm.delete(row)"
                    ng-show="!row.entity.isNew",
                    ng-enabled="grid.appScope.vm.canDelete(row.entity)">delete_forever</i>`,
    editable: false,
    width: 30
  };
  // calculate the width of a set of columns
  // assumes each columndef has a width attribute, in px

  var colwidths = function (columndefs, start, end) {
    var tot = 0;
    for (var i = start; i <= end; i++) {
      tot += parseFloat(columndefs[i].width);
    }
    return tot;

	};
	interface INotatedArray extends Array<any> {
		numResults: number;
		pageNo: number;
		pageSize: number;
	}

	// the parent binding points to an object
	// that may support these methods - an alternative to using the particular & callbacks
	export interface ISuperGridParent {
		registerGridApi(gridApi: uiGrid.IGridApi);
		onSortClear(): void;
		onSort(column: string, direction: string);
		onShowPage(newPage: number, pageSize: number);
		onHighlightChanged(rowEntity: any, isSelected: boolean);
		onAction(actionName: string, columnField: string, rowEntity: any);
	}
  /**
   * @ngdoc controller
   * @name
   * Controller
   * @description
   * manages the display of the grid
   *
   **/
  class Controller {

    public pagination: any = {};
    public runOnLoad = false;

		// bindings
		// grid data
		public rowset: INotatedArray;
		// definition of columns and grid setup; 
		// derived from but extending the ui-grid ColumnDefs collection
		public viewMode: any;
		// default settings and column definitions that are applied to any viewMode
		public viewDefaults: any;
    public entityType: any;
    public isWaiting = false;

    // track rendered view
    public renderedViewMode = "";
    public renderedPageNo = 1;
    public renderedPageSize = 50;

    public gridClass: any;

    public baseState: string;
    protected baseOptions: any;

    public gridOptions: any;
    public gridApi: uiGrid.IGridApi;

    public columnWidths: any;

    private _isEditing = false;
    private paged: boolean;   // binding
    public isEditableView = false;
    public insertable = false;      // can insert rows when in editing mode
    public gridState = "";          // addds a class to the grid

		// callbacks may be specified individually as bindings
		// or else by a binding to a 'parent' (usually the controller hosting the supergrid)
    public onSortClear;
    public onSort;
    public onShowPage;
		public onAction;
		public onHighlightChanged;
    public parent: ISuperGridParent;
    public parentResource: any;     //{resource, id}

    // provide a setup for singe row selection, that can be turned on or off
    // use the ui-grid selection module
    //http://ui-grid.info/docs/#/api/ui.grid.selection.api:GridOptions
    // enableFooterTotalSelected
    // enableFullRowSelection
    // enableRowHeaderSelection
    // enableRowSelection
    // enableSelectAll
    // enableSelectionBatchEvent
    // multiSelect

    public get isEditing() {
      return this._isEditing;
    }
    public set isEditing(editing) {
      this._isEditing = editing;
      if (this.gridApi) {
        this.gridApi.grid.refresh();
      };
    }

    /**
     * OnRegisterApi
     * performs the setup of all grid related paramters after the grid is initialised
		 * If parent is set, and parent implements registerGridApi than call that function here
		 * This allows the parent component to set any callbacks it needs from the ui-grid
     * @param gridApi
     */
    protected onRegisterApi(gridApi) {
      if (this.gridApi) {
        return;
      }

      this.gridApi = gridApi;
			// register on parent if parent exposes registerGridApi method
      if (this.parent && this.parent.registerGridApi) {
        this.parent.registerGridApi.call(this.parent, gridApi);
      }

      // set up the ui-grid handlers for sorting and pagination changes
      // these just reflect these into the enclosing context by raising the appropirate event
      gridApi.core.on.sortChanged(this.scope, (grid, sortColumns) => {
        // only requery if useExternalSorting
        if (this.pagination.enablePagination) {
					if (sortColumns.length === 0) {
						// sorting is now handled by the supergrid's host via the callback
						// or via 'parent'
						this.onSortClear();
						if (this.parent && this.parent.onSortClear) {
							this.parent.onSortClear.call(this.parent);
						}

          } else {
						// invoke the callback passing column and direction ( '&' binding syntax)
						this.onSort({ column: sortColumns[0].field, direction: sortColumns[0].sort.direction });
						if (this.parent?.onSort) {
							this.parent.onSort.call(this.parent, sortColumns[0].field, sortColumns[0].sort.direction);
						}
          }
        }
      });
      gridApi.pagination.on.paginationChanged(this.scope, (newPage, pageSize) => {
        if (this.pagination.enablePagination) {
          if (+newPage !== +this.renderedPageNo || +pageSize !== +this.renderedPageSize) {
            //this.theFilter.ShowPage(this.findConfig.current, newPage, pageSize);
						this.onShowPage({ newPage: newPage, pageSize: pageSize });			// callback (& binding) syntax
						if (this.parent?.onShowPage) {
							this.parent.onShowPage.call(this.parent, newPage, pageSize);
						}
          };
        };
      });

      gridApi.rowEdit.on.saveRow(this.scope, this.onSave);
      gridApi.cellNav.on.navigate(this.scope, this.onNavigate);
      gridApi.grid.options.rowEditWaitInterval = -1;  // force manual saving
      // register a column processor for the row edit column 
      gridApi.core.registerColumnsProcessor((columns) => {
        columns.some((column) => {
          if (column.name === "rowHeader") {
            column.visible = this.isEditing;
            return true;
          }
          return false;
        });
        return columns;
      });

      // allow any highlighted row (maintained by grid.selection module) 
      // to be available to the supergrid host
      gridApi.selection.on.rowSelectionChanged(this.scope, (row, evt) => {
        // just return this change to the host via the callback
        this.onHighlightChanged({ item: row.entity, highlightOn: row.isSelected });
        if (this.parent?.onHighlightChanged) {
          this.parent.onHighlightChanged.call(this.parent, row.entity, row.isSelected);
        }
      });
      // finally appply any initial viewMode and rowset
      if (this.viewMode) {
        this.applyViewMode(this.viewMode);
      }
      if (this.rowset) {
        this.applyData(this.rowset);
      }
    }

    protected isRowSelectable = (gridRow) => {
      return (this.gridOptions.enableRowSelection ? true : false);
    }

    static $inject = ['$scope', '$state', '$q', "$compile", "$timeout", "gridUtil", "uiGridConstants", "Restangular", "Lookups"
      , "PermAuthorization", "PermPermissionMap"];
    constructor(public scope, public $state: ng.ui.IStateService,
      public q: ng.IQService,
      public compile: ng.ICompileService,
      public timeout: ng.ITimeoutService,
      public gridUtil: any,         // ui-grid gridUtil service
      private uiGridConstants: uiGrid.IUiGridConstants,
      public restangular: restangular.IService

      , public lookups: Sw.Lookups.LookupService
      , public permAuthorization: any
      , public permPermissionMap: any) {


      // reliance on statedata should be legacy use only
      var statedata = $state.current.data;

      if (statedata) {
        if (statedata.pagination) {
          this.pagination = statedata.pagination;
        };
        this.runOnLoad = statedata.runOnLoad || false;
      }

      if (this.pagination.enablePagination === undefined) {
        this.pagination.enablePagination = true;
      }

      // get a pointer to the underlying list state
      // this may not be the current state, if this controller was loaded
      // by navigating the Url to a child of that list state (e.g. .item)
      // 4 7 2016 - account for lists not named .list
      if ($state.current.name.indexOf(".list") >= 0) {
        this.baseState = $state.current.name.substring(0, $state.current.name.indexOf(".list") + 5);
      } else {
        this.baseState = $state.current.name;
      }

      // utilities for grid management
      this.columnWidths = colwidths;

      this.baseOptions = {
        paginationPageSizes: [10, 25, 50, 100, 500],
        paginationPageSize: 25,

        enablePagination: this.pagination.enablePagination,
        enablePaginationControls: this.pagination.enablePagination,
        useExternalPagination: this.pagination.enablePagination,
        useExternalSorting: this.pagination.enablePagination,
        // v 3 introduces various options about sorting that are overly complex and have to be removed 7 2 2016
        enableColumnMenus: false,

        // selection defaults are off
        enableRowSelection: false,
        enableRowHeaderSelection: false,
        enableFullRowSelection: false,
        isRowSelectable: this.isRowSelectable,
        multiSelect: false,

				// scrollbars
				//https://stackoverflow.com/questions/26015500/hide-horizontal-scrollbar-angular-ui-grid
				// value = 0; /* NEVER */
				// value = 1; /* ALWAYS */
				// value = 2; /* WHEN_NEEDED */

				enableHorizontalScrollbar: 2,
				enableVerticalScrollbar: 2,

        // support for dynamic row template - initialise to the default
        rowTemplate: "ui-grid/ui-grid-row",
        // a smarter editable cell template
        // note uigrid, not ui-grid, to distinguish between templates installed by ui-grid and our own mvc routes
        //editableCellTemplate may be set in GridOptions (for all columnDefs) or by columnDef
        editableCellTemplate: "uigrid/cellEditor",
        rowHeight: 30,
        onRegisterApi: (gridApi) => {
          this.onRegisterApi.call(this, gridApi);
        }
      };

      this.gridOptions = angular.copy(this.baseOptions);
    };

		public action = (actionName, columnField, rowData) => {
			// & binding requires an object of parameters passed... es6 syntax make this easy
			this.onAction({ actionName, columnField, rowData });
			if (this.parent?.onAction) {
				this.parent.onAction.call(this.parent, actionName, columnField, rowData);
			}
    };

    public $onChanges(changes) {
      if (changes.viewMode && changes.viewMode.currentValue && this.gridApi) {
        this.applyViewMode(this.viewMode);
      }
      if ((changes.rowset || changes.isEditing) && this.gridApi) {
        this.applyData(this.rowset);
      }

      if (changes.paged) {
        this.pagination.enablePagination = (this.paged? true: false)
      }

    }
    /**
  * searchComplete
  * @description
  * handler to receive new data
  *
  * @param {event} event the broadcast event
  * @param {object} resultpack the data package returned from the server
  **/
    protected applyData = (result) => {

      let d = this.q.defer();
      d.resolve();
      let p: ng.IPromise<any> = d.promise;


      if (this.viewMode !== this.renderedViewMode) {
        p = this.applyViewMode(this.viewMode);
      }
      // before changing the data we have a chance to get the selectedRows 
      // they are gone when the data changes
      p.then(() => {
        //// TO DO
        let idField = null;
        //let idField = this.theFilter.defaultId();
        let selectedId = null;
        //////if (this.selectionCount() === 1) {
        //////  selectedId = this.selectedRows()[0][idField];
        //////}
        // insert the data int the options
        this.gridOptions.data = this.rowset;

        // pagination info is supplied by the incoming data too
        this.gridOptions.totalItems = this.rowset.numResults;
        if (this.rowset.numResults > 0 && this.pagination.enablePagination) {

          if (this.gridOptions.paginationCurrentPage !== this.rowset.pageNo) {

            this.gridOptions.paginationCurrentPage = this.rowset.pageNo;
          }
          if (this.gridOptions.paginationPageSize !== this.rowset.pageSize) {
            this.gridOptions.paginationPageSize = this.rowset.pageSize;
          }
          this.renderedPageNo = this.rowset.pageNo;
          this.renderedPageSize = this.rowset.pageSize;
				};
				// ensure these changes reach the live options
				angular.extend((<any>this.gridApi.grid).options, this.gridOptions);

        // reinstate the selection
        let e: any;
        if (this.rowset.length === 1) {
          e = this.rowset[0];
        }
        else if (selectedId) {
          e = _.find(this.rowset, r => (r[idField] === selectedId));
        }
        if (e) {
          this.timeout(() => { this.gridApi.selection.selectRow(e) }, 0);
        }
      })
        .then(() => {
          this.isWaiting = false;
        });
    };
    protected applyViewMode(vm) {
      // set the gridClass from the view mode
      this.gridClass = vm.gridClass;

      // is this viewmode editable?
      // defaults to false, editable property can be true , false or array of roles
      // get a promise representing the editable state...
      let vmEditable: boolean;
      let dEditable = this.q.defer();
      let pEditable: ng.IPromise<any>;
      if (vm.editable === undefined) {
        vmEditable = false;
        dEditable.reject();
        pEditable = dEditable.promise;
      } else {

        if (vm.editable.only || vm.editable.except) {
          // in angular-permission: this unusual structure:
          // the service "PermPermissionMap" does not return an object, but a "constructor" function
          // when the "service" is retrieved via angular injection, we have that function...
          // so we can use "new" to get an "instance" of that 'class' 
          let permissionMap = new this.permPermissionMap(vm.editable);
          pEditable = this.permAuthorization.authorizeByPermissionMap(permissionMap);
        } else {
          vmEditable = (vm.editable ? true : false);
          if (vm.editable) {
            dEditable.resolve();
          } else {
            dEditable.reject();
          }
          pEditable = dEditable.promise;
        }
      }

      // return the promise
      return pEditable
        .then(() => {
          vmEditable = true;
        }, () => {
          vmEditable = false;
        })
        // vmEditable is established
        .then(() => {
          this.isEditableView = vmEditable;       // change the UI if it is potentially editable
          this.insertable = vmEditable && vm.insertable;   // by default can't insert

          // tracking columns are standard columns for EditUser and EditDateTime
          // derived from property includeTrackingColumns - if not present, true if layout is editable
          let vmIncludeTrackingColumns: boolean = false;
          if (vm.includeTrackingColumns === undefined) {
            vmIncludeTrackingColumns = vmEditable;
          } else {
            vmIncludeTrackingColumns = vm.includeTrackingColumns;
          }
          // do we need the Delete icon column?
          let vmDeletable = vmEditable ? vm.deletable : false; // no deleting without editing
          let vmIncludeDeleteColumn = false;
          if (vm.includeDeleteColumn === undefined) {
            vmIncludeDeleteColumn = vmDeletable;
          } else {
            // the view mode may make its own ui arrangements for delete?
            vmIncludeDeleteColumn = vm.vmIncludeDeleteColumn;
          }


          // set up the columndef array
          var cd = new Array();
          // if its an editable view mode, add the row header
          if (vmEditable) {
            cd.push(rowHeaderCol);
          }

					// add in the default columns that belong to every viewmode if present
					if (this.viewDefaults && this.viewDefaults.columnDefs) {
						// append these columns to the set
						cd = cd.concat(this.viewDefaults.columnDefs);
					}
          // add the viewmode's particular columndefs
          cd = cd.concat(vm.gridOptions.columnDefs);
          // if the viewMode specifies deleteable, add the Delete icon
          if (vmIncludeDeleteColumn) {
            cd = cd.concat(deleteCol);
          }
          // if the viewMode specifies editable
          // add the tracking columns (Edited by... on..) if the viewmode asks for that
          if (vmIncludeTrackingColumns) {
            cd = cd.concat(trackingCols);
          }

          // configure each column with some defaults, and some custom settings
          cd.forEach(c => {
            c.headerTooltip = c.headerTooltip === false ? false: c.headerTooltip??true;
            c.headerCellFilter = "vocab";     // is it inefficient to always translate?
            if (c.enableSorting === undefined) {
              c.enableSorting = true;
            }
            if (c.enableSorting && c.sortDirectionCycle === undefined) {
              c.sortDirectionCycle = ["asc", "desc"];
            }
            // lookup is the name of a lookup table in the Lookup cache
            // if defined on a column, it is used to
            // - filter that column
            // - define a dropdown when editing
						// note that the column can use the builtin properties
						// - editDropDownOptionsArray
						// - editDropdownRowEntityOptionsArrayPath
						// - editDropdownOptionsFunction
						// to get a dropdown list explicitly

            if (c.lookup) {
							c.editDropdownOptionsFunction = (row, col) => {
								return this.lookups.getList(c.lookup)
									.then(list => {
										// provide a null entry in the list so it can be cleared
										return <Sw.Lookups.ILookupEntry[]> [{ C: null, N: "" }].concat(list);
									});
              };
              // note uigrid, not ui-grid -> getting customised from the server
              c.editDropdownIdLabel = c.editDropdownIdLabel || 'C';
              c.editDropdownValueLabel = c.editDropdownValueLabel || 'N';
              
						}
						// if there is anysort of dropdown, get the default template
						if (c.editDropdownOptionsFunction || c.editDropdownOptionsArray || c.editDropdownRowEntityOptionsArrayPath) {
              c.editableCellTemplate = c.editableCellTemplate || 'uigrid/dropdownEditor';
              c.cellFilter = c.cellFilter || (c.lookup ? "lookup:'" + c.lookup + "'" : "dropdownValue:col" );
            }
            // some default formatting
            switch (c.type) {
              case "number":
                c.cellClass = c.cellClass || "gdAlignRight";
                break;
              case "boolean":
                c.cellFilter = c.cellFilter||"tick";
                c.cellClass = c.cellClass||"gdAlignCenter";

                c.editableCellTemplate = c.editableCellTemplate || "uigrid/booleanEditor";
                break;
						}

            // is the column editable?
            let colEditable: boolean = false;
            if (vmEditable) {
              if (c.editable === undefined || c.editable == "new") {
                colEditable = true;   // default the whole column to editable when viewmode is editable
              } else {
                if (Array.isArray(c.editable)) {
                  // TO DO
                  // use angular permissions to check the array of roles?
                } else {
                  colEditable = (c.editable ? true : false);
                }
              }
            }

            c.enableCellEdit = colEditable;
            if (c.editable == "new") {
              c.cellEditableCondition = (scope) => this.isEditing && scope.row.entity.isNew;
            } else {
              c.cellEditableCondition = () => this.isEditing;
            }
            c.enableCellEditOnFocus = c.cellEditOnFocus || vm.defaultCellEditOnFocus || c.enableCellEdit;
            // can the column accept the focus?
            c.allowCellFocus = c.cellFocus || (c.enableCellEdit ? true : vm.defaultAllowCellFocus || false);
          });
          var newOptions: any = angular.copy(this.baseOptions);

          // support selection
          switch (vm.selectable) {
            case "single":
              // implement single select
              newOptions.enableRowSelection = true;
              newOptions.multiSelect = false;
              newOptions.enableRowHeaderSelection = true;
              newOptions.enableFullRowSelection = true;
              break;

            case "multi":
              newOptions.enableRowSelection = true;
              newOptions.multiSelect = true;
              newOptions.enableRowHeaderSelection = false;
              newOptions.enableFullRowSelection = true;
              break;
            default:
              // clear any current selection, no longer applicable
              newOptions.enableRowSelection = false;
              newOptions.multiSelect = false;
              newOptions.enableRowHeaderSelection = false;
              newOptions.enableFullRowSelection = false;
              if (this.gridApi.selection) {
                this.gridApi.selection.clearSelectedRows();
              }
              break;

          }
          // note the viewMode can explicitly override the selection settings in its gridOptions
          // if something more customised is wanted
          angular.extend(newOptions, vm.gridOptions);
          newOptions.columnDefs = cd;
					this.gridOptions = angular.copy(newOptions);
					// but this seems to be not enough for some properties - 
					//they have to be forced into the grid.options
					// the special handling from here allows
					// rowHeight, and rowTemplate
					// to be dynamically set by the viewmode

          let rowTemplateChanged = ((<any>this.gridApi.grid).options.rowTemplate == this.gridOptions.rowTemplate ? false : true);
          angular.extend((<any>this.gridApi.grid).options, this.gridOptions);
          if (rowTemplateChanged) {
            this.setUpRowTemplate(this.gridApi.grid);
          }
					this.timeout(0, false)
						.then(() => {
							//this.gridApi.core.notifyDataChange(this.uiGridConstants.dataChange.ALL);
							this.gridApi.core.notifyDataChange("all");
						});
          this.renderedViewMode = vm;
        });
    }

    // Selection support
    public selectionCount() {
      // this can get out of kilter
      //return this.gridApi.selection.getSelectedCount();
      // this is more reliable
      return this.gridApi.selection.getSelectedRows().length;
    }
    public selectedRows() {
      return this.gridApi.selection.getSelectedRows();
    }

    // row template support
    // this code is copied from ui-grid GridClassFactory
    // this is needed if we want to dynamically alter the rowTemplate
    private setUpRowTemplate(grid) {
      var rowTemplateFnPromise = this.q.defer();
      grid.getRowTemplateFn = rowTemplateFnPromise.promise;

      this.gridUtil.getTemplate(grid.options.rowTemplate)
        .then(
        (template) => {
          var rowTemplateFn = this.compile(template);
          rowTemplateFnPromise.resolve(rowTemplateFn);
        },
        (res) => {
          // Todo handle response error here?
          throw new Error("Couldn't fetch/use row template '" + grid.options.rowTemplate + "'");
        });
    }

    /************************************************************************************
    * Row edit support
    *************************************************************************************/
    private entityToSave;

    // callable from the view to manually invoke an immediate save
    public saveRow = (row) => {
      this.entityToSave = row.entity;
      this.setDirty([row.entity]);
      this.gridApi.rowEdit.flushDirtyRows();
    };

    public onNavigate = (newRolCol, oldRowCol) => {
      // oldRowCol can be null if we click into the grid from outside
      if (oldRowCol === null || newRolCol.row === oldRowCol.row) {
        return;
      }
      if (oldRowCol.row.isDirty && !oldRowCol.row.isSaving) {
        this.saveRow(oldRowCol.row);
      }
    };

    // callback from rowEdit
    // rowEdit requires this method to return a promise, or else it fails
    // 
    public onSave = (rowEntity) => {
      if (this.entityToSave && rowEntity !== this.entityToSave) {
        // ui-grid has already set isSaving = true - so turn that off
        (<any>this.gridApi.grid).rowEdit.dirtyRows.forEach((row) => {
          if (row.entity === rowEntity) {
            delete row.isSaving;
          }
        });
        return;     // only do a single record at a time
      }

      delete rowEntity.errorData;
      let promise: ng.IPromise<any> = null;
      // identify a new row in a grid

      //we assume that restangular has added the "put" method to this object t update itself
      // that is, the collection we are editing has been "restangularised"
      if (rowEntity.isNew) {
        promise = rowEntity.post();
      } else {
        promise = rowEntity.put();       // update
      }
      promise.then((newData) => {
				// newData is a simple collection of the fields that were passed in, with new values
        console.log("post success");
        console.log(newData);
        
				// https://github.com/mgonto/restangular/issues/367
        // gonto says we need to rebuild the restangular link
        // 8 5 2016 see link above for my comment and why newData.plain() is the fix
				// ie we don't override the restangular methods on rowEntity with the ones bound to newData
        angular.extend(rowEntity, newData.plain());
        if (rowEntity.isNew) {
          delete rowEntity.isNew;
        }
      }, // error response
        (errorData) => {
          rowEntity.errorData = errorData;
          return this.q.reject(errorData);
        }
      );
      this.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

    public setDirty = (rowEntities) => {
      this.gridApi.rowEdit.setRowsDirty(rowEntities);
    };
    /******************************************************************
    * Insert
    *******************************************************************/
    public canInsert() {
      return this.isEditing && this.insertable;
    }
    public newRow() {
      // create an object whose property names are inferred from either:
      // the first element of the existing array
      // or the fields bound into the grid
      let data: any[] = (<any>this.gridApi.grid).options.data;

      // is new indicates this is a new row
      let newRow: any = { isNew: true };
      // support for parent resouce
      this.restangular.restangularizeElement(this.parentResource??null, newRow, (<any>data).route);
      data.push(newRow);
    }

    public newRowFn = (scope) => scope.row.entity.isNew;

    /******************************************************************
    * Delete  Row                                                     *
    *******************************************************************/

    /**
     * Test if the row can be deleted
     * This is primarily to ng-enable the delete button
     * {param} row - the ui-grid row
     */
    public canDelete(row) {
      if (row.isSaving || row.isDeleting) {
        return false;
      }
      return true;
    }
    public delete = (row) => {
      // check that we are not trying to save this data, and 
      const DELETE_DELAY = 250;
      if (!this.canDelete(row)) {
        return;
      }
      row.isDeleting = true;
      let promise: ng.IPromise<any> = null;
      // identify a new row in a grid

      //we assume that restangular has added the "put" method to this object t update itself
      // that is, the collection we are editing has been "restangularised"
      let rowEntity = row.entity;
      delete rowEntity.errorData;
      delete row.isDeleteError;

      promise = rowEntity.remove();
      promise.then((response) => {
        delete row.isDeleting;
        row.isDeleted = true;
        this.timeout(DELETE_DELAY).then(() => {
          let index = this.gridOptions.data.indexOf(rowEntity);
          this.gridOptions.data.splice(index, 1);
        });
      }, // error response
        (errorData) => {
          row.isDeleteError = true;
          rowEntity.errorData = errorData;
          return this.q.reject(errorData);
        });
    }
    public onDelete = (rowEntity) => {
      if (this.entityToSave && rowEntity !== this.entityToSave) {
        // ui-grid has already set isSaving = true - so turn that off
        (<any>this.gridApi.grid).rowEdit.dirtyRows.forEach((row) => {
          if (row.entity === rowEntity) {
            delete row.isSaving;
          }
        });
        return;     // only do a single record at a time
      }

      delete rowEntity.errorData;
      let promise: ng.IPromise<any> = null;
      // identify a new row in a grid

      //we assume that restangular has added the "put" method to this object t update itself
      // that is, the collection we are editing has been "restangularised"
      promise = rowEntity.remove();
      promise.then((response) => {
        let index = this.gridOptions.data.indexOf(rowEntity);
        this.gridOptions.data.splice(index, 1);
      }, // error response
        (errorData) => {
          rowEntity.errorData = errorData;
          return this.q.reject(errorData);
        }
      );
      this.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;
    public require;

    constructor() {
			this.bindings = {
				// the data to display in the grid
        rowset: "<",
				// definition of columns and grid setup; 
				// derived from but extending the ui-grid ColumnDefs collection
				viewMode: "<",
				// default settings and column definitions that are applied to any viewMode
				viewDefaults: "<", 
				entityType: "<",

				isEditing: "<",
				// the enclosing context can signal that the grid is waiting for data
				// the grid responds with ui changes
				isWaiting: "<",

        // bindings to 'events' of the grid       
				// sort has been cleared - host should send new data
				onSortClear: "&",
				// a sort has been applied ( ie via clicking a column header)
				// host should fetch the sorted data
				onSort: "&",
				// a page has been requested - host should return the requested page of data
				onShowPage: "&",
				// an 'action' has been invoked
				onAction: "&",
				// selection has been applied or removed from a row
				onHighlightChanged: "&",
				// object supporting handlers for the callbacks -
				// generally the component hosting the supergrid e.g. parent="vm"
        parent: "<",
        // if we want to have a composite url generated for the RESt endpoint,
        // we need to have the parent resource defined for new rows
        parentResource: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "common/supergrid";
    }
  }

  angular
    .module("sw.common")
    .component("superGrid", new Component());
} 