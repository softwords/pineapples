﻿namespace Sw.Component {
  export class MdDialogController {
    static $inject = ["$mdDialog"];
    constructor(protected mdDialog: ng.material.IDialogService) { }

    public modelForm: ng.IFormController;     // provide this here for chiildren that need it
    public closeDialog() {
      this.mdDialog.cancel();
    }

    public returnFail(failData) {
      this.mdDialog.cancel(failData);
    }

    public returnSuccess(successData?: any) {
      this.mdDialog.hide(successData);
    }

    // useful where the child class contains a form which must be valid befre submitting the dialog
    public returnFormOk(formToCheck: ng.IFormController, successData?: any) {
      if (formToCheck.$valid) {
        this.returnSuccess(successData);
      }
    }
  }
}
