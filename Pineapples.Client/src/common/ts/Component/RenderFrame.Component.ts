﻿namespace Sw.Component {

	export class RenderFrameController {
		// bindings
		public theFilter: Sw.Filter.IFilterCached;
		public findConfig: Sw.Filter.FindConfig;

		private baseState: string;

		static $inject = ["$scope", "Lookups", "$state"];
		constructor(scope: Sw.IScopeEx, public lookups: Sw.Lookups.LookupService,
			public state: ng.ui.IStateService) {
			scope.hub.subscribe("SearchComplete", (data, sender) => {
				// Better to let the user decide when to close the searcher.
				//this._showSearcher = false;
			})
		}

		public $onChanges(changes) {
		}

		public $onInit() {
		}

		/**
		 * true if we have navigated to an action state
		 **/
		public actionVisible() {
			let foundAction: boolean = false;
			Object.keys(this.state.current.views).some(k => {
				if (k.substring(0, 10) == "actionpane") {
					return foundAction = true;
				}
			});
			return foundAction;
		}

		/**
		 * by default, hide the rendering if there is something in the action pane
		 **/
		public get showRender() {
			return !this.actionVisible();
		}

		/**
		 * allow the user to toggle the display of the searcher panel
		 **/
		private _showSearcher: boolean = false;
		public get showSearcher() {
			return this._showSearcher;
		}
		public set showSearcher(show: boolean) {
			this._showSearcher = show;
		}

	}

	export class RenderFrameComponent implements ng.IComponentOptions {
		public bindings = {
			theFilter: "<",
			findConfig: "<"
		};
		public controller: any;
		public controllerAs = "vm";
		public templateUrl: string;

		constructor(feature: string, controller?) {
			this.controller = controller || RenderFrameController;
			this.templateUrl = `${feature}/RenderFrame`;
		}
	}

	angular
		.module("sw.common")
		.component("renderFrame", new RenderFrameComponent("generic"));
}