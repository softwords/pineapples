﻿namespace Pineapples.Components {

  type StateData = {
    name: string,
    params: any
  }

  class PanelController {
    public panelClass: string;
    public panelIcon: string;
    public panelClose: boolean|string|StateData;
    public panelControlCollapse: boolean = false;
    public heading: string;

    static $inject = ["$state", "$window"]
    constructor(public state: ng.ui.IStateService, public window: ng.IWindowService) { }

    public $onChanges(changes) {
      if (changes.heading) {
      }
    }

    /**
     * panelClose binding may now accept:
     * true : go back in history , or if there is no history, go up to parent
     * string : a state name to go to
     * StateData a StateData object: go to the name using the params
     * */
    public onPanelClose() {
      if (this.panelClose === true) {
        // if there is history in this window, go back...
        if (this.window.history.length > 2) {
          this.window.history.back();
        } else {
          // go to the parent state
          this.state.go("^");
				}
        return;
      }
      if (typeof this.panelClose === 'string') {
        // its a state name
        this.state.go(this.panelClose);
        return;
      }
      this.state.go((<StateData>this.panelClose).name, (<StateData>this.panelClose).params );
      return;
    }

    public $onInit() {

    }

    private _collapsed: boolean = false;
    public get collapsed() {
      return this._collapsed;
    }
    public toggleCollapse() {
      this._collapsed = !this._collapsed;
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;
    public transclude;

    constructor() {
      this.bindings = {
        panelClass: "@",
        panelIcon: "@",
        panelClose: "<",          // we want the literal true to be boolean true, not string "true"
        panelControlCollapse: "<",
        heading: "@"
      };
      this.transclude = {};
      this.transclude["panelControls"] = "?panelControls";
      this.controller = PanelController;
      this.controllerAs = "vm";
      this.templateUrl = "generic/panel";
    }
  }

  angular
    .module("pineapples")
    .component("panel", new Component());

}