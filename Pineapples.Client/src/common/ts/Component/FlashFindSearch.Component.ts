﻿namespace Sw.Component {

  interface IBindings {
    entity: any;
  }

  class Controller implements IBindings {
    public entity: any;

    constructor() { }

    public $onChanges(changes) {
      if (changes.entity) {
        console.log('$onChanges: ', this.entity);
      }
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        entity: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "generic/FlashFindSearchComponent";
    }
  }

  angular
    .module("pineapples")
    .component("flashFindSearchComponent", new Component());
}