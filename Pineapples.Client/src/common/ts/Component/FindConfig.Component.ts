﻿namespace Sw.Components {

	export class FindConfigOperations {

		public rowData: Sw.Lookups.ILookupEntry;
		public params: any;

		public theFilter: Sw.Filter.IFilter;
		public findConfig: Sw.Filter.FindConfig;

		static $inject = ["$scope", "Lookups", "$state"];
		constructor(scope: Sw.IScopeEx, public lookups: Sw.Lookups.LookupService
			, public state: ng.ui.IStateService) {

		};

		// LifeCycle events
		public $onChanges(changes) {

		}

		public $onInit() {
			this.params = this.findConfig.current.filter;
		}

		public FindNow() {
			this.theFilter.FindNow(this.findConfig.current);
			this.findConfig.prepared = true;
		}
		public FlashFind() {
			this.theFilter.FlashFind(this.findConfig.current);
			this.findConfig.prepared = true;
		}
		public Reset() {
			this.findConfig.reset();
		}
		public get viewMode() {
			return this.findConfig.current.viewMode;
		}

		public set viewMode(newViewMode) {
			if (this.findConfig.current.viewMode !== newViewMode) {
				this.findConfig.current.viewMode = newViewMode;
				let vm = this.theFilter.GetViewMode(newViewMode);
				if (vm) {
					this.FindNow();
				}
			}
		}

		public supportedViewModes() {
			if (this.findConfig.allowedViews && this.findConfig.allowedViews.length > 0) {
				return this.findConfig.allowedViews;
			}
			return this.theFilter.AllViewModes();
		}
	};

	/**
	 * @class Sw.Components.FindConfigComponent
	 * @description revamped class to use as base class for searcher controllers
	 * this is designed to work with the 'RenderFrame' architecture. It is used by all features
	 * search components (e.g. Sw.Components.FindConfigComponent("school/SearcherComponent")
	 * the component responsible for layout out search field such school name, district, etc.
	 */
	export class FindConfigComponent implements ng.IComponentOptions {
		public bindings = {
			theFilter: "<",
			findConfig: "<"
		};
		public controller;
		public controllerAs = "vm";
		public templateUrl;

		constructor(templateUrl, controller?) {
			this.controller = controller || FindConfigOperations;
			this.templateUrl = templateUrl;
		}
	}

}

