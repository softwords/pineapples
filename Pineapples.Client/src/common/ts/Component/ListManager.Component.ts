﻿namespace Sw.Component {


	class Controller extends ListMonitor {
		public rowset: any[]; // holds the data that is being managed
		public renderedViewMode: string;			// name of the c.naurrently displayed ViewMode
		public selectedViewMode;						// a view mode
		public isWaiting: boolean = false;

		public theFilter: Sw.Filter.IFilter;
		public findConfig: Sw.Filter.FindConfig;
		public monitorSelector: any ;
		public monitorKey: string;			// the key field of the data in this list

		public baseState: string;

		static $inject = ['$scope', '$state', '$q', 'Restangular', 'Lookups'];
		constructor(public scope
			, public $state: ng.ui.IStateService
			, public q: ng.IQService
			, public restangular: restangular.IService
			, public lookups: Sw.Lookups.LookupService) {

			super();
			var statedata = $state.current.data;

			// get a pointer to the underlying list state
      // this may not be the current state, if this controller was loaded
      // by navigating the Url to a child of that list state (e.g. .item)
			// may be better to bind this??
			this.baseState = $state.current.name.substring(0, $state.current.name.indexOf(".list") + 5);

			if (statedata) {
				if (statedata.pagination) {
					//this.pagination = statedata.pagination;
				};
			}
			// handle the notifications about data requests and deliveries
			// TO DO convert to publish/subscribe

			scope.$on('SearchComplete', this.searchComplete);

			//scope.$on('SearchError', this.searchError);

			scope.$on('FindNow', this.findNow);
		}
		//------------------- life cycle events ---------------------------
		public $onChanges(changes) {

		}

		public $onInit() {
			// when first getting the component, fire a find now
			this.theFilter.FindNow(this.findConfig.current);
			if (this.monitorSelector) {
				this.monitor(this.scope, this.monitorSelector, this.monitorKey||"id")
			}
		}
		//------------------- Handlers for data synchronization --------------------------------

		protected searchComplete = (event, resultpack) => {

			if (resultpack.entity === this.theFilter.entity && resultpack.method === 'search') {
				// we have received new data
				// package that up to transmit it to the grid.
				this.rowset = resultpack.resultset; // this is now a restangular object
				if (this.findConfig.current.viewMode !== this.renderedViewMode) {
					let modeName = this.findConfig.current.viewMode;

					var vm = this.theFilter.GetViewMode(modeName);
					if (!vm) {
						vm = this.theFilter.ViewModes[0];
					}
					this.selectedViewMode = vm;
					this.renderedViewMode = vm.key;
				}
			}
			this.isWaiting = false;
		}

		protected findNow = (event, data: Sw.Filter.IFindNowBroadcast) => {
			if (data.filter.entity === this.theFilter.entity) {
				data.actions.push(Sw.Filter.Request.search);
				this.isWaiting = true;
			}
		};

		//------------- Callbacks --------------------------------------------------

		/**
		 * Called when a request is received to change page in the displayed data
		 * This is generally raised by the supergrid using the paginiation controls on its ui-grid
		 * @param newPage
		 * @param pageSize
		 */
		public onShowPage(newPage, pageSize) {
			console.log(newPage);
			this.theFilter.ShowPage(this.findConfig.current, newPage, pageSize);
		}

		/**
		 * sorting is removed
		 */
		public onSortClear() {
			this.theFilter.SortOn(this.findConfig.current);
		}

		/**
		 * sort requested - action this request by invoking sorting
		 * @param column
		 * @param direction
		 */
		public onSort(column, direction) {
			this.theFilter.SortOn(this.findConfig.current, column, direction);
		}

		public onAction(actionName, columnField, rowData) {
			// the first implementation of such actions goes back to the filter?
			// probably not necessary...?
			//this.theFilter.Action(actionName, columnField, rowData, this.baseState);

			let newstate = this.baseState + '.' + actionName;
			if (rowData && columnField) {
				let id;
				if (Array.isArray(columnField)) {
					id = columnField.map(c => rowData[c]).join("_");
				} else {
					id = rowData[columnField];
				}
				this.$state.go(newstate, { id: id, rowData: rowData, columnField: columnField });
			}
			else {
				this.$state.go(newstate);
			}
		};

		//--------------------- Monitored List -----------------------------

		public monitoredList() {
			return this.rowset;
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public controller = Controller;
		public controllerAs: string = "vm";
		public bindings = {
			theFilter: "<",
			findConfig: "<",
			monitorSelector: "<",		// the relevant function for monitoring, or the item type as a string
			monitorKey: "@"
		}
		constructor(public templateUrl: string) { }
	}

	angular
		.module("sw.common")
		.component("pagedListManager", new ComponentOptions("generic/renderlist"));
}