﻿module Sw.Component {

  export abstract class ComponentEditController {


    public modelForm: ng.IFormController;   // angular matches this to the name of the form in the view

    // bindings
    public model: Sw.Api.IEditable;

    private modelInit: any;
    public isWaiting = false;
    public isEditing = false;
    public isNew = false;
    public activeTab = 0;
		public editTab = 0; // can be overridable if the details tab is not the first

		protected getNew: () => ng.IPromise<any>;				// virtual function to create a new item of the current type

    constructor(private apiUi: Sw.Api.IApiUi ) { }

    public $onChanges(changes) {
      if (changes.model) {
				this.pristine();
				// using ==, not === so we get isNew = true when  _id() == undefined
        if (this.model._id() == null) {
          this.isNew = true;
          this.setEditing(true);
        }

      }
    }
    public setEditing(editing: boolean) {
      if (editing) {
        if (this.activeTab !== this.editTab) {
          this.activeTab = this.editTab;
        }
      }
      this.isEditing = editing;

    }

    /**
     * Called when the model has been changed - ie by saving
     * can be overriden in subclasses to do extra stuff, but subclasses
     * shouold ccall this base class
     * @param newData - 
     */
		public onModelUpdated(newData: any) {
			if (this.isNew && newData._id) {
				// default behaviour on insert is to replace the model with the new one
				// if it is already an instance of Editable
				this.model = newData;
			} else {
				// fall back on the definition in the model, or in Editable
				this.model._onUpdated(newData);
				this.pristine();
			}
		}
   
		public onInserted(model) {
			if (this.getNew) {
				this.apiUi.addNewReport(this.model).then((response) => {
					// Create a new one
					this.isNew = true;
					// remove any values from model that are not functions
					this.getNew().then((newData) => {
						this.model = newData;
						this.pristine();
						this.setEditing(true);
					});
				}, (errorData) => {
					this.isNew = false;
				});
			}
    }

   /*
    * flows for saving data
    * we may need in rare cases to allow a 'save' of work-in-progress, that may not be full of valid data
    * <form submit="vm.save()">
    * iin this case we need a button that can force the submit
    * <button type=submit form="vm.modeForm"> - doesn't work on IE
    *
    *
    */
		
    public save(ignoreValidation?: boolean) {
      if ((!ignoreValidation) && this.modelForm.$invalid) {
        // the css is set up so that submitted will force the display of all invalid classes
        // note this is the default bahaviour of md-input-container
        // the md-is-error setting defaults to showing any errors on submitted 
        // (the documentation doesn't says only that the error will be displayed on touched)
        this.modelForm.$setSubmitted();     
        return;
      }
      // use the restangular method on the model
      this.isWaiting = true;
      let promise: ng.IPromise<any> = null;
      if (this.isNew) {
        promise = this.model.post();
      } else {
        promise = this.model.put();
      }
      promise.then(
        // success reponse - the matching data fields are returned from the server
        (newData) => {
          this.isWaiting = false;
					if (this.isNew) {
						// by convention, new will return the same data as get (read),
						// whereas put just returns the affected fields
						// so we replace the model entirely in this case:
						this.model = newData;
						this.isNew = false;
						this.onInserted(this.model);     // default logic here is to change 
						this.pristine();
					} else {
						// update fields on the  record from the returned data
						// use .plain() method to ignore restangular adornments added to newData
						// call a handler on the object in case it wants to do more stuff
						this.onModelUpdated(newData.plain());
						this.pristine();
					}
       //   if (this.isNew && this.getNew) {

       //     this.apiUi.addNewReport(this.model).then((response) => {
       //       // Create a new one
       //       this.isNew = true;
       //       // remove any values from model that are not functions
							//this.getNew().then((newData) => {
							//	// assume that new() has retiurned an object that is
							//	// - strongly typed
							//	// - IEditable
							//	// - restangularized
       //         this.model = newData;
       //         this.pristine();
       //         this.setEditing(true);
       //       });
       //     }, (errorData) => {
       //       this.isNew = false;
       //     });

       //   } 
        },
        (errorData) => {
          // TO DO some analysis of the error like in RowEditManager,
          // with opportunity to recover from Concurreny error, or continue if validation error,
          // or even back out by restoring this.SchoolInit
          this.isWaiting = false;
          this.apiUi.showErrorResponse(this.model, errorData).then((response) => {
            switch (response) {
              case "retry":
                // retry the save  - note this is not re-entrant becuase it is a callback from the promise
                this.save();
                break;
              case "overwrite":
                // force a save by overwriting the Rowversion and try again
                this.model._rowversion = errorData.data[this.model._rowversionProperty()];
                this.save();
                break;
              case "discard":
                // in a conflict, use the new data
                angular.extend(this.model, errorData.data);
                this.pristine();
                break;
            }
          });
        });
    }

    public undo() {
      
			// For an existing record, its safer to reread, than to attempt to cache it...

			if (!this.isNew) {
				this.refresh();
			} else {
				// for a new record, get another new one. Same as Add Another, expcet we didn't save
				this.getNew().then((newData) => {

					this.model = newData;
					this.pristine();
					this.setEditing(false);
				});
			};
    }

    protected refresh() {
      this.model.get().then((newData) => {
        angular.extend(this.model, newData.plain());
        this.pristine();

      }, (error) => {
      });
    }

    protected pristine() {
      if (this.modelForm) {
        this.modelForm.$setUntouched();
        this.modelForm.$setPristine();

        if (this.modelInit) {
          // modelInit may have been supplied from an enclosing context
          // e.g. if this is a popup modal from a grid.
          // so we extend it, rather than replace it
          // the changes are now propogated back 
          angular.extend(this.modelInit, this.model.plain());
        } else {
          this.modelInit = angular.copy(this.model.plain());      // if we want to implement Undo
        }
        this.setEditing(false);
      }
    }

    /*
     methods for managing the display of error messages
   */

    // show errors when the control is invalid, and the control has been touched or the form is submitted
    // this mimics the built-in angularjs material behaviour of md-input-controller
    public showErrors(controlname) {
      if (controlname) {
        return this.modelForm[controlname].$invalid && (this.modelForm.$submitted || this.modelForm[controlname].$touched);
      }
      return this.modelForm.$invalid && this.modelForm.$submitted;
    }

    /*
    Does the control have an error ?
     */
    public hasError(controlname) {
      
      if (controlname) {
        console.log("hasError ? " + controlname + ":" + this.modelForm[controlname].$invalid);
        return this.modelForm[controlname].$invalid;
      }
      return this.modelForm.$invalid;
    }

  }
}
