﻿namespace Sw.Component {
	/*
	 * A State machine for providing UI feedback for simple operations
	 * 
	 * The process moves through these states:
	 * -- confirm - the user confirms to proceed with the operation
	 * -- inprogress - the operation is performed
	 * -- success - the operation reports it has completed successfully
	 * -- fail - the operation has failed 
	 *
	 * The application can supply locals to configure the dialog by passing the
	 * IProcessDialog Options
	 * Key is the action - which is the function to perform. 
	 * It must return a promise to a string, and will return a string on success or failure. 
	 * this message is by defaut displayed in the dialog.
	 * 
	 * In more complex circumstances the dialog can be complety configured 
	 * by passing a custom template in the TemplateUrl argument. This template 
	 * will use the MaterialProcessDialogLayout.cshtml master page.
	 * Refer to that page for the required sections.
	 * 
	 */ 
	
	export class ProcessDialogOptions {
		// function to perform
		// promise shoudl return a success message on success or fail message on fail
		action: (data) => ng.IPromise<string>;	
		title?: string;								// title of the dialog
		confirmMessage?: string;			// message to display int he dialog in 'confirm' state
		inprogressMessage?: string;		// message to display int he dialog in 'inprogress' state
	}

  export class MdProcessDialogController extends MdDialogController{
    static $inject = ["$mdDialog","$timeout"];
		constructor(protected mdDialog: ng.material.IDialogService
			, protected timeout: ng.ITimeoutService) {
			super(mdDialog);
		}

		public _state: string = "confirm"
		public get state() {
			return this._state;
		}

		public set state(newstate) {
			this._state = newstate;
			switch (newstate) {
				case "inprogress":
					this.timeout<string>(this.action, 10, true, (this.data))
						.then((successmessage) => {
							this.successMessage = successmessage|| this.successMessage;
							this.state = "success";
						})
						.catch((failmessage) => {
							this.failMessage = failmessage ||this.failMessage;
							this.state = "fail";

						})
			}

		}
		public action: (data) => string
		public data: any;

		private _confirmMessage;
		public get confirmMessage() {
			return this._confirmMessage ||"Click Proceed to continue...";
		}
		public set confirmMessage(newValue) {
			this._confirmMessage = newValue;
		}

		private _inprogressMessage;
		public get inprogressMessage() {
			return this._inprogressMessage ||"Executing...";
		}
		public set inprogressMessage(newValue) {
			this._inprogressMessage = newValue;
		}

		private _successMessage;
		public get successMessage() {
			return this._successMessage ||"Successful";
		}
		public set successMessage(newValue) {
			this._successMessage = newValue;
		}

		private _failMessage;
		public get failMessage() {
			return this._failMessage || "An error has occurred...";
		}
		public set failMessage(newValue) {
			this._failMessage = newValue;
		}

		public static processDialogOptions(mdDialog: ng.material.IDialogService
			, locals:ProcessDialogOptions, templateUrl?) {
			let opts: ng.material.IDialogOptions = {
				templateUrl: templateUrl||"dialog/materialprocess",
				locals: locals,
				bindToController: true,			// this makes locals diretly available as members of the controller
				controller: MdProcessDialogController,
				controllerAs: "vm",
				clickOutsideToClose: true
			}
			return opts;
    }
	}

}
