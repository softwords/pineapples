﻿/// <reference path="FindConfig.Component.ts"/>
namespace Sw.Components {

	export class TableParamsController extends Sw.Components.FindConfigOperations {
		//public get tableparams: Sw.Filter.TableParams;
		public get tableparams(): Sw.Filter.TableParams {
			return this.findConfig.current.table;
		}
		public tableOptions: Sw.Lookups.ILookupList;
		public dataOptions: Sw.Lookups.ILookupList;

		static $inject = ["$scope", "Lookups",];
		constructor(scope: Sw.IScopeEx, public lookups: Sw.Lookups.LookupService
			, public state: ng.ui.IStateService) {
			super(scope, lookups, state);
			scope.$watch("vm.tableparams.row", (newValue: string, oldValue) => {
				this.rowData = this.lookups.byCode(this.findConfig.tableOptions, newValue);
			});
		};

		// when moving from injection to binding, 
		// any dependent activity moves to onInit
		public $onInit() {


			//this.tableparams = findConfig.current.table;
			this.lookups.getList(this.findConfig.tableOptions).then(list => {
				this.tableOptions = list;
			});
			this.lookups.getList(this.findConfig.dataOptions).then(list => {
				this.dataOptions = list;
			});

		}
		private _chartType;
		public get chartType() {
			return this.tableparams.chartType;
		}
		public set chartType(newchartType) {
			this.tableparams.chartType = newchartType;
			// this.theFilter.FindNow(this.findConfig.current);
		}

		public get row() {
			return this.tableparams.row;
		}
		public set row(newrow) {
			if (this.tableparams.row !== newrow) {
				this.tableparams.row = newrow;
				this.FindNow();
			}
		}
		public hasRowGeoSet(): boolean {
			let rowData = this.lookups.byCode(this.findConfig.tableOptions, this.tableparams.row);
			if (rowData && rowData.g) {
				return true;
			}
			return false;
		}
		public get col() {
			return this.tableparams.col;
		}
		public set col(newcol) {
			if (this.tableparams.col !== newcol) {
				this.tableparams.col = newcol;
				this.FindNow();
			}
		}
		public get dataItem() {
			return this.tableparams.dataItem;
		}
		public set dataItem(newdataItem) {
			if (this.tableparams.dataItem !== newdataItem) {
				this.tableparams.dataItem = newdataItem;
				//   this.FindNow();
			}
		}
		public swap(): void {
			let tmp = this.col;
			this.tableparams.col = this.row;
			this.tableparams.row = tmp;
			this.FindNow();
		};
		public showDataOption() {
			return true;
		}
		public showColumnOption() {
			return true;
		}
		public showChartOptions() {
			return false;
		}

	}

	class TableParamsComponent implements ng.IComponentOptions {
		public bindings = {
			theFilter: "<",
			findConfig: "<",
			palette: "<",
			mapColorScale: "<"
		};
		public controller = TableParamsController;
		public controllerAs = "vm";
		public templateUrl = "Generic/ChartParamsComponent";

	}

	angular
		.module('sw.common')
		.component("tableParams", new TableParamsComponent())

}