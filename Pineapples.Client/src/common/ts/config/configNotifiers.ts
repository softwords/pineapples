﻿module Sw {

  function config($rootScope, $state, $stateParams, authenticationDialogs: Sw.Auth.IAuthenticationDialogs, httpBuffer) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams) {
      $rootScope.toState = toState;
      $rootScope.toStateParams = toStateParams;
      console.log('$stateChangeStart: ' + toState.name);
    });

    $rootScope.$on('$stateChangeError', function (event, toState, toStateParams, fromState, fromStateParams, error) {
      console.log('$stateChangeError: ' + fromState.name + '=>' + toState.name + ':' + error.reason);
      console.log(error);
      // now we may want to go to Access Denied or login, and probably return
      switch (error.reason) {
        case 'unauthorized':
          if (error.errors[0] = 'No credentials') {
            $state.go('signin');
          } else {
            $state.go('denied');
          };
          break;
        default:
          break;
      }
    });

    $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromStateParams, error) {
      console.log('$stateNotFound: ' + unfoundState);
    });

    $rootScope.$on('auth:login-success', function (ev, user) {
      $rootScope.loggingIn = false;
      console.log("auth:login-success: ");
      console.log(user);
    });

    $rootScope.$on('auth:login-error', function (ev, error) {
      console.log("auth:login-error");
      console.log(error.reason);
      console.log(error);
      $rootScope.loggingIn = false;
    });

    $rootScope.$on('auth:login-required', function (ev, error) {
      console.log("auth:login-required");
      if (!$rootScope.loggingIn) {
        $rootScope.loggingIn = true;
        authenticationDialogs.showLogin();
      };
    });

    $rootScope.$on('auth:no-connection', function (ev, error) {
      console.log("auth:no-connection");
      if (!$rootScope.tryingToConnect) {
        $rootScope.tryingToConnect = true;
        authenticationDialogs.showNoConnection().then(() => {
          $rootScope.tryingToConnect = false;
          httpBuffer.retryAll();
        });
      }


    });
    $rootScope.$on('auth:validation-success', function (ev, user) {
      console.log("auth:validation-success: ");
      console.log(user);
    });

    $rootScope.$on('auth:validation-error', function (ev, error) {
      console.log("auth:validation-error");
      console.log(error.reason);
      console.log(error);
    });

    $rootScope.$on('auth:logout-success', function (ev) {
      console.log("auth:logout-success");
      // go back to signin
      $state.go('signin');
    });
    $rootScope.$on('auth:logout-error', function (ev, error) {
      console.log("auth:logout-error: ");
      console.log(error.reason);
      console.log(error);
    });
  }

  angular
    .module('sw.common')
    .run(['$rootScope', '$state', '$stateParams', 'authenticationDialogs', 'httpBuffer', config]);
}
