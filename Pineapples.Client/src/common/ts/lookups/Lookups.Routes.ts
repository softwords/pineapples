﻿namespace Sw.Lookups {
  let routes = ($stateProvider) => {
    // root state for 'school' feature
    let state: ng.ui.IState;
    state = {

			url: "^/tables/{lkp}",
			params: {
				lkp: { value: null, squash: true }
			},
      views: {
        "@": {
          component: "componentTableSelector"
        }
      },
      resolve: {
        tableList: ["lookupsAPI", (api) => {
          return api.getEditableTables();
				}],
				rowset: ['$stateParams', 'Restangular', ($stateParams, restangular: restangular.IService) => {
					if ($stateParams.lkp) {
						return restangular.all($stateParams.lkp).getList();
					}
					return null;
				}],
				entityType: ['$stateParams', ($stateParams) => {
					return $stateParams.lkp;
				}],
				entityDescription: ['$stateParams', "$http", ($stateParams, http: ng.IHttpService) => {
					if ($stateParams.lkp) {
						return http.get(`api/${$stateParams.lkp}/info/description`).then(result => result.data);
					}
					return null;
				}],

				viewMode: ['$stateParams', "$http", ($stateParams, http: ng.IHttpService) => {
					if ($stateParams.lkp) {
						return http.get(`api/${$stateParams.lkp}/info/viewmode`).then(result => result.data);
					}
					return null;
				}],
				isEditing: () => true
      }
    };
    $stateProvider.state("site.lookups", state);
  }
  angular
    .module("sw.common")
    .config(["$stateProvider", routes]);
}
