﻿/* ------------------------------------------
    lookups Service
    ----------------------------------------
*/
module Sw.Lookups {

  /**
   * Required structure for a LookupEntry. Only used has array item and some
   * retrieval operation of {@link ILookupList}
   */
  export interface ILookupEntry {
    C: string;
    N: string;
    ID?: number;
    g?: string;
  }

  export interface ILookupList extends Array<ILookupEntry> {
    byCode(code: string): ILookupEntry;
    byCode(code: string, prop: string): any;

    // return a match based n a numeric id
    byID(id: number): ILookupEntry;
    byID(id: number, prop: string): any;

    search(srch: string): ILookupEntry;
    search(srch: string, prop: string): any;
    match(srch: string): Array<ILookupEntry>;
  }

  class LookupListMixin extends Array<ILookupEntry> {

    public byCode(code: string): ILookupEntry;
    public byCode(code: string, prop: string): any;
    public byCode(code: string, prop?: string) {
      let row = _.find(this, { C: code });
      if (row) {
        if (prop) {
          return row[prop];    // name is the default return
        }
        return row;
      }
      return null;

    }

    // return a match based n a numeric id
    public byID(id: number): ILookupEntry;
    public byID(id: number, prop: string): any;
    public byID(id: number, prop?: string) {
      let row = _.find(this, { ID: id });
      if (row) {
        if (prop) {
          return row[prop];    // name is the default return
        }
        return row;
      }
      return null;
    }

    public search(srch: string): ILookupEntry;
    public search(srch: string, prop: string): any;
    public search(srch: string, prop?: string) {
      srch = srch.toUpperCase();
      let row = _.find(this,
        (d) => (d.C.toUpperCase() === srch || (d.N && d.N.toUpperCase() === srch) ? true : false)
      );
      if (row) {
        if (prop) {
          return row[prop];    // name is the default return
        }
        return row;
      }
      return null;

    }

    public match(srch: string) {
      srch = srch.toUpperCase();
      let matches = _.filter(this,
        (d) => (d.C.toUpperCase().indexOf(srch) >= 0 || d.N.toUpperCase().indexOf(srch) >= 0 ? true : false)
      );
      return matches;
    }
  }
  /**
   *
   */
	// popup dialog for code Picker
	class PickerDialogController extends Sw.Component.MdDialogController {
		public codeset: any;
		public selected: any;
		public title: string;
		public prompt: string;

		static $inject = ["$mdDialog"];
		constructor(mdDialog: ng.material.IDialogService) {
			super(mdDialog);
		}
		public ok() {
			// validation
			this.returnSuccess(this.selected);
		};

		public cancel() {
			this.cancel();
		};

		public select(selection) {
			this.selected = selection;
		}

	}

  export class LookupService {
    static $inject = ["$q", "$rootScope", "lookupsAPI", "$mdDialog"];
		constructor(protected _q: ng.IQService
			, private rootScope: Sw.IRootScopeEx
			, protected _api
			, protected mdDialog: ng.material.IDialogService) {
			rootScope.hub.subscribe("dataUpdated", (data) => this.onDataMerge(data, "update"));
			rootScope.hub.subscribe("dataInserted", (data) => this.onDataMerge(data, "insert"));
			rootScope.hub.subscribe("dataDeleted", (data) => this.onDataMerge(data, "delete"));
    }
    // this is a perfact example of where an index signature can enable the use of a "dictionary" across randomly created property names
    public cache: {
      [lookupname: string]: ILookupList
    };

    /**
     * ensure that a given set of lookups are available
     * when the promise is resolved all
     * @param names - array of lookup names
     *
     */
    public prepare(names: string[]) {
      // http://www.martin-brennan.com/using-q-all-to-resolve-multiple-promises/
      let p = names.map((name) => this.getList(name));
      return this._q.all<ILookupList>(p);
    }
    public getListFcn(name: string) {
      return () => this.getList(name);
    }

    /**
     * `getList` makes a trip to the backend and retrieves some lookup data from the database
     * (e.g. types of schools, a list of islands). Instead of directly using
     * this one it is better to get it from the cache since it would not require
     * another async trip to backend
     *
     * ### Example of direct use
     *
     * ```typescript
     * // first inject Lookups service
     * let years = this.$q.defer();
     * this.lookups.getList('surveyYears').then((list: Sw.Lookups.ILookupList) => {
     *   years = list
     * }, (reason: any) => {
     *   console.log("Unable to get surveyYears" + reason);
     * });
     * ```
     *
     * ### Example of getting from cache (recommended)
     * ```typescript
     * // first inject Lookups service and make public
     * static $inject = ['Lookups'];
     * constructor(public lookups: Sw.Lookups.LookupService) { }
     * ```
     * and then directly in the view
     * ```html
     * <select style="width:100%; color:#4b4b4b; font-size:14pt"
     *         ng-options="year.C as year.N for year in vm.lookups.cache['surveyYears']"
     *         ng-model="vm.baseYear"></select>
     * ```
     * @param {string} name - a valid lookup string (e.g.'schoolTypes','districts','authorities',
     * 'islands','electoraten','electoratel','listNames','schoolNames',
     * 'vocab','sysParams','paFields','paFrameworks','schoolFieldOptions',
     * 'schoolDataOptions','surveyYears')
     * @returns {ng.IPromise<ILookupList>}
     */
    public getList(name: string): ng.IPromise<ILookupList> {
      var d = this._q.defer<ILookupList>();
      if (this.cache && this.cache[name]) {
        d.resolve(this.cache[name]);
      } else {
        // go read it and wait
        this._api.getList(name).then((result) => {
          Object.keys(result.plain()).forEach((codeType: string) => {
            angular.extend(result[codeType], LookupListMixin.prototype);
          });
          if (this.cache) {

            angular.extend(this.cache, result);
          } else {
            this.cache = result;
          }
          d.resolve(this.cache[name]);
        },
          error => {
            d.reject(error);
          }
        );
      };
      return d.promise;
    }

    public init = () => {
      var d = this._q.defer();
      if (this.cache) {
        d.resolve(this.cache);
      } else {
        this._api.core().then((data) => {
          this.addToCache(data);
            d.resolve(this.cache);
          },
          error => {
            d.reject(error);
          }
        );
      };
      return d.promise;
    };
    public findByID: (codeset: string, id: any) => ILookupEntry = (codeset, id) => {
      return <ILookupEntry>_.find(this.cache[codeset], { ID: id }) || { C: null, N: null };
    };
    public byCode(codeset: string, code: string): ILookupEntry;
    public byCode(codeset: string, code: string, prop: string): any;
    public byCode(codeset: string, code: string, prop?: string) {
      let codeTable: ILookupList = this.cache[codeset];
      if (codeTable) {
        return codeTable.byCode(code, prop);
      } else {
        return null;
      }
    }
    /**
     *
     * @returns {}
     */
		public pickCode = (codeset: string, code: string, title: string, prompt: string) => {
			let options: any = {
				clickOutsideToClose: true,
				templateUrl: "dialog/codepicker",
				controller: PickerDialogController,
				controllerAs: "vm",
				bindToController: true,
				locals: {
						codeset: this.cache[codeset],
						code: code,
						title: title,
						prompt: prompt
				}
			}
			// return the promise - the controller will resolve or reject
			return this.mdDialog.show(options);
     
    };

    public withNull = (codename: string) => {
      let arr = new Array();
      arr.push({ C: null, N: '(all)' });
      return arr.concat(this.cache[codename]);
   }
		
    protected addToCache(result) {
      // plain() removes all the restangular properties - maybe this should happen upstream
      Object.keys(result).forEach((codeType: string) => {
        angular.extend(result[codeType], LookupListMixin.prototype);
      });
      if (this.cache) {
        angular.extend(this.cache, result);
      } else {
        this.cache = result;
      }
		}

    /**
     * Install an array of values as a lookuplist
     * This requires the array to be extended with the expected methods - in the Mixin
     * @param codeType 
     * @param lookups - array of ILookup
     */
    public setLookupList(codeType: string, lookups: ILookupList) {
      angular.extend(lookups, LookupListMixin.prototype);
      this.cache[codeType] = lookups;
    }

	//--------------------------------------------------------------------------------
		// List Monitoring
		//--------------------------------------------------------------------------------
		// As code sets are editied through table mainntenane, the list monitor implementation here
		// will attempt to update the code table to reflect the change
		// monitor methods - these are on rootScope, and will last for the duration of the app

		/*
		* process a reported data change
		*/
		protected onDataMerge(data, operation?: string) {
			let code = data.data["Code"];
			let what = data.what;
			switch ((<string>data.what).toLowerCase()) {
				
				case "employerorgtypes":
					this.processSimpleCode(code, "orgTypes", data.data, operation);
					break;
				case "istcourse":
					this.processSimpleCode(code, "sessionTypes", data.data, operation);
					break;

				default:
					this.processSimpleCode(code, data.what, data.data, operation);
			}
		}

		// update description form a simple code set, or in insert mode, push the new entry
		// this expects the incoming data to have fields Code Description
		// the corresponding in the code list are C and N
		protected processSimpleCode(code: string, listname: string, data: any, operation?: string) {

			if (operation === "delete") {
				let list = this.mapList(listname);;
				if (list) {
					// underscore has a method to remove from the array in situ
					let idx = _.findIndex(list, { C: code });
					if (idx >= 0) {
						list.splice(idx);
					}
				}
				return;
			}
			// handle update and insert
			let o = this.getEntry(code, listname);
			if (o) {
				// for safety, insert defaults to update if the key is already present
				o.N = data.Description;
			} else if (operation === "insert") {
				let list = this.mapList(listname);
				if (list) {
					let newLookup = { ID: null, C: code, N: data.Description };
					list.push(newLookup);
				}
			}
		}

		private getEntry(code: string, listname: string) {
			let list = this.mapList(listname);
			if (!list || !code) {
				return;
			}
			return _.find(list, { C: code });
		}

		protected mapList(listname: string) {
			return this.cache[listname] || this.cache[this.camelize(listname)];
		}

		//https://stackoverflow.com/questions/2970525/converting-any-string-into-camel-case
		private camelize(str) {
			return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
				return index == 0 ? word.toLowerCase() : word.toUpperCase();
			}).replace(/\s+/g, '');
		}
		
    // update description from a simple code set keyed by numeric ID
    private setDescriptionById(id: number, list: ILookupList, data: any, operation?: string) {
      if (!list || !id) {
        return;
      }
      let o = _.find(list, { ID: id });
      if (o) {
        o.N = data.Description;     // this is assuming some knowledge of how these queries are put togther,
      } else if (operation === "insert") {
        let newLookup = { ID: id, C: null, N: data.Description };
        list.push(newLookup);
      }
    }
  };
  angular
    .module('sw.common')
    .service('Lookups', LookupService);
}
