﻿/* ------------------------------------------
    lookupsAPI
    ----------------------------------------
*/
namespace Sw.Lookups {

  export class LookupsApi {
    protected _wreqService: restangular.IElement;
    static $inject = ["Restangular","$http"];
    constructor(restangular: restangular.IService, protected http: ng.IHttpService) {
      this._wreqService = restangular.all("lookups");
    }
		public core = () => this.http.get("api/lookups/collection/core")
			.then(response => response.data);

    public getList = (name: string) => this._wreqService.customGET("getlist/" + name);
    public post = (entry: Sw.Lookups.ILookupEntry) => this._wreqService.customPOST(entry, entry.C);
		public getEditableTables = () => this.http.get("api/lookups/editabletables")
			.then(response => response.data);
  }
  angular
    .module('sw.common')
    .service('lookupsAPI', LookupsApi);
}
