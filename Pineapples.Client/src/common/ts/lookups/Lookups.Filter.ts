﻿namespace Sw.Lookups {
  class LookupFilter {

    public static lookup(lookups: Sw.Lookups.LookupService) {

      let fcn = (value, lookupset, prop) => {
        // force the return of name
        return lookups.byCode(lookupset, value, prop || "N");
      };
      return fcn;
		}

		public static lookupId(lookups: Sw.Lookups.LookupService) {

			let fcn = (value, lookupset, prop) => {
				// force the return of name
				let entry = lookups.findByID(lookupset, value)
				return entry?entry[prop || "N"]:null;
			};
			return fcn;
		}

    public static vocab(lookups: Sw.Lookups.LookupService) {

      let fcn = (value) => {
        // vocab will return the Name associated with the code , unless null
				// then the code itself is returned
        return lookups.byCode("vocab", value, "N") || value;;
      };
      return fcn;
    }
    public static sysParam(lookups: Sw.Lookups.LookupService) {

      let fcn = (value) => {
        // force the return of name
        return lookups.byCode("sysParams", value, "N");
      };
      return fcn;
    }
    public static sysParamInt(lookups: Sw.Lookups.LookupService) {

      let fcn = (value) => {
        // force the return of name
        return lookups.byCode("sysParams", value, "I");
      };
      return fcn;
    }

    public static keyValue() {

      let fcn = (value, kvarray, findprop, returnprop) => {
        // force the return of name
        findprop = findprop || "key";
        returnprop = returnprop || "value";
        let entry = _.find(kvarray, kv => kv[findprop] == value);
        return entry ? entry[returnprop] : null;
      };
      return fcn;
    }
    // this is specifically for ui-grid cellFilter, where the array of values is specified explcitily in editDropdownOptionsArray
    public static dropdownValue() {

      let fcn = (value, col, findprop, returnprop) => {
        let kvarray = col.colDef.editDropdownOptionsArray;
        // force the return of name
        findprop = findprop || "id"; // note 'id' is more common than key
        returnprop = returnprop || "value";
        let entry = _.find(kvarray, kv => kv[findprop] == value);
        return entry ? entry[returnprop] : null;
      };
      return fcn;
    }
  }

  angular
    .module("sw.common")
		.filter("lookup", ['Lookups', LookupFilter.lookup])
		.filter("lookupId", ['Lookups', LookupFilter.lookupId])
    .filter("vocab", ['Lookups', LookupFilter.vocab])
    .filter("sysParam", ['Lookups', LookupFilter.sysParam])
    .filter("sysParamInt", ['Lookups', LookupFilter.sysParamInt])
    .filter("keyValue", LookupFilter.keyValue)
    .filter("dropdownValue", LookupFilter.dropdownValue);
}
