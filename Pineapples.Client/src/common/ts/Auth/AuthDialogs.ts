﻿module Sw.Auth {

  export interface IAuthenticationDialogs {
    showLogin(): void;
    showNoConnection(): ng.IPromise<any>;
    cancelDialogs(): void;
  }


  class AuthenticationDialogs {
    static $inject = ["$mdDialog", "authenticationService", "$state"];

    constructor(private _mdDialog: ng.material.IDialogService, private _authService: IAuthenticationService,
      private _state: angular.ui.IStateService) { }

    private _getIEVersion() {
      let match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
      return match ? parseInt(match[1]) : undefined;
    }

    public showLogin() {
      let dlg: ng.material.IDialogOptions = {
        // Currently making use of same login markup as signin for consistency
        // IDialogOptions' defaults are use
				templateUrl: "user/loginPopup",
        controller: 'LoginPopupController',
        controllerAs: 'vm'
      };
      // if the mdDialog can't be displayed ( ie9?) then let's fall back to
      if (this._getIEVersion() < 10) {
        // just reroute to signon
        this._state.go("signin");
      } else {
        this._mdDialog.show(dlg);
      }

    }
    // this dialog is displayed if there is a 502 or -1 response from http
    public showNoConnection():ng.IPromise<any> {
      let dlg: ng.material.IDialogOptions = {
        template: `
          <div class="panel panel-primary">
              <div class="panel-body">
		          <div>No response from the server. Check your internet and VPN connection</div>
              <div><button ng-click="vm.closeDialog()">Retry</button></div>
              </div>  
          </div>`,

        controller: Sw.Component.MdDialogController,
        controllerAs: 'vm'
      }
      if (this._getIEVersion() < 10) {
        // just reroute to signon
        this._state.go("signin");
      } else {
        return this._mdDialog.show(dlg);
      }
    }
    public cancelDialogs() {
      this._mdDialog.cancel();
    }
  }
  angular
    .module("sw.common")
    .service("authenticationDialogs", AuthenticationDialogs);
}
