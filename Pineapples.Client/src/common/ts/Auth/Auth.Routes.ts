﻿namespace Sw.Auth {
  /* routes relating to user authentication
    confirminvite
    resetpassword
    forgotpassword
    */


  function authenticationRoutes(stateProvider: ng.ui.IStateProvider) {
    // reset password is activated from an email link, that passes the userid and the resetpassword token
    let state: ng.ui.IState = {
      url: "/resetpassword/{userId: string}?e&code",
      params: { userId: null, code: null, e: null },
      views: {
        '@': {
          component: "resetPassword"
        }
      },
      resolve: {
        code: ["$stateParams", ($stateParams) => $stateParams.code],
        userId: ["$stateParams", ($stateParams) => $stateParams.userId],
        email: ["$stateParams", ($stateParams) => $stateParams.e],
        passwordValidator: ["authenticationService",
          (authenticationService: Sw.Auth.IAuthenticationService) => authenticationService.getPasswordInfo()]
      }
    }
    stateProvider.state("resetpassword", state);

    // confirmInvite is activated from an email link, that passes the userid, email address and the resetpassword token
    // this email is sent to the user when their account is created, so they can set their own password

    state = {
      url: "/confirminvite/{userId: string}?e&back&code",
      params: { userId: null, code: null, back: null, e: null },
      views: {
        '@': {
          component: "confirmInvite"
        }
      },
      resolve: {
        code: ["$stateParams", ($stateParams) => $stateParams.code],
        userId: ["$stateParams", ($stateParams) => $stateParams.userId],
        email: ["$stateParams", ($stateParams) => $stateParams.e],
        back: ["$stateParams", ($stateParams) => $stateParams.back],
        passwordValidator: ["authenticationService",
          (authenticationService: Sw.Auth.IAuthenticationService) => authenticationService.getPasswordInfo()]
      }
    }
    stateProvider.state("confirmInvite", state);

    // forgot password navigates to the page where the user can enter their email to request a reset token
    // which is mailed to them in response
    state = {
      url: "/forgotpassword",
      views: {
        '@': {
          component: "forgotPassword"
        }
      }
    }
    stateProvider.state("forgotpassword", state);
  }

  angular
    .module('sw.common')
    .config(['$stateProvider', authenticationRoutes])
}