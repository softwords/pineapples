﻿module Sw.Auth {

  export class Storage {
    private _local;
    private _session;

    // sessionStorage is just a wrapper around a second instance of localStorageService
    static $inject = ["$localStorage", "$sessionStorage"];

    constructor(local, session) {
      this._local = local;
      this._session = session;
    }

    public set = (key, val, persist) => {
      // we should delete both keys here, there may be an expired one sitting there
      this.remove(key);
      if (persist) {
        return this._local[key] = val;
      }
      return this._session[key] = val;
    };

    public get = (key) => {
      var val = this._session[key];
      if (val !== undefined) {
        return val;
      }
      return this._local[key];
    };

    public remove = (key) => {
      delete this._session[key];
      delete this._local[key];
    };
  }

  angular
    .module("sw.common")
    .service("storage", Storage);
}
