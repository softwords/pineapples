﻿namespace Sw.Auth {

  class Controller {
    public model: IPasswordValidator;

    public $onChanges(changes) {
      // changes to passwordvalidator
      console.log("PasswordValidator")
      console.log(changes);
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        model: "<"
      }
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "user/passwordvalidator";
    }
  }
  angular
    .module("sw.common")
    .component("passwordValidator", new Component());

}