﻿namespace Sw.Charts {

		
		/*
		 * Mirror chart is a chart that displays stacked bars or columns on either side of 
		 * the central axis. a Gender Pyramid is a simple example, but exams/accreditations 
		 * may also want to show one or more categories as "negative"
		 * 
		 * The mirror chart gets the usual bindings of a row col chart, as well as :
		 * 
		 * mirrors number[] | string[]
		 * 
		 * Mirrors is the indexes or names of the series that need to be mirrored. 
		 * These will be rendered in this order, on the negative side of the axis.
		 * The remaining series are rendered in their original order, 
		 * on the positive side of the axis.
		 * 
		 * The complete sort order of series is determined by the interaction of mirrors and columnSortBy.
		 * Series are sorted in the order of columnSortBy , then mirrors is applied onto that.
		 * if mirrors is specified as numbers, then those series are just moved across the axis,
		 * in their original order. If mirrors is specified as series names, then those series
		 * in mirros are moved across the axis, and show left-to-right according to the order
		 * specified in mirrors - ie the last series in mirrors
		 * is the one touching the axis; the first series in mirrors is the first in the legend. 
		 * All series not in mirrors appear as set by ColumnSortBy, if provided.
		 */

	class Controller { // extends Sw.Charts.SizeableChartController {

		public mirrors: string[]|number[];

		public onChartRender;				// & binding 
		public onClick;
		public tooltipper;					// & binding just supporting datum
		// called before the chart is rendered, passing the option and the echart instance

		public charttype;

		/**
		 * Sort an array based on the order
		 * @param arr - array to sort
		 * @param order - array of -1 and 1. Elements in arr are mapped to order by
		 * index Those -1 are first, +1 after
		 * @param flipNegatives - all the -1 entries are reverse when true
		 */
		private orderSorter(arr: any[], order: number[]) {
			// create an array of the indexes of arr:
			let indexes = arr.map((value, index) => index);
			// create a comparator function to check the order
			let osort = (n1, n2) =>
				(order[n1] != order[n2]) ?
					// if values are different we sort on those values, but :
					// if both are negative, and we are not flipping negs, don;t reverse
					// this is for use in legend mainly
					(order[n1] - order[n2]) 
					
					// if both have same order, the smaller index is first
					: (n1 - n2) ;
			// sort the indexes in place
			indexes.sort(osort);
			// return the reconstructed array in this order
			return arr.map((value, index) => arr[indexes[index]]);
		}

		// passed as the onChartRender callback to RowColChart
		public applyMirror(option: echarts.EChartOption, echart, renderedType: string) {
			// mirror chart is transparent wrapper on rolcolchart when no mirror is defined

			if (!this.mirrors) {
				// relay the event, wth the modified option, to the caller
				this.onChartRender({ option, echart, renderedType })
				return;
			}

			// mirroring wont work on pie charts
			switch (this.charttype) {
				case "pie":
				case "pieh":
					this.onChartRender({ option, echart, renderedType })
					return;
			}

			// 4 things to do:
			// -- flip the mirrored series values to negative;
			// -- reverse the order of those flipped sequences
			// -- manipulate the palette to match
			// -- ensure the legend shows in the 'left-to-right' order

			// begin by getting all the current series names in their current order
			let seriesnames = option.series.map(series => (<any>series).name);
			// filter the mirrors to remove anything that is not in there
			let _mirrors: string[];
			// this is just to play with 'type guard' a little....
			let mirrorsIsIndexes = (m): m is number[] => (typeof this.mirrors[0] == 'number');
			if (mirrorsIsIndexes(this.mirrors)) {
				_mirrors = this.mirrors.map(value => seriesnames[value]);
			} else {
				_mirrors = this.mirrors;
			}

			// option.dDataset can be an array, but not expected to be 
			// when passed by RolColChart
			// clone it so not to affect the RowColChart's internals
			// note _.clone is shallow -  angular.clone is deep!
			let dataset = _.clone(option.dataset as echarts.EChartOption.Dataset);
			let dims = dataset.dimensions as string[];
			let source = dataset.source as any[];
			dataset.source = source.map(row => {
				return dims.map((dim, dimIdx) => {
					if (_mirrors.indexOf(dim) >= 0) {
						return row[dimIdx] * -1;
					}
					return row[dimIdx];
				})
			});
			
			option.dataset = dataset;

			// before sorting, get the series 

			// now rebuild the sort order for series and palette
			// those in the mirrors have indexes e.g.  0, -1, -2, -3
			// those not in the mirrors have value 1
			let order = seriesnames.map(name => {
				let idx = _mirrors.indexOf(name);
				return idx = idx * (-1);
			});

			let legendorder = order.map(value => value == 1 ? Number.MAX_VALUE : Math.abs(value));
			// make sure the legend corresponds, but it should read left to right as 
			// they are displayed in the chart; in other words, the negative items are NOT flipped
			// catch this order before sorting the series, using flipNegative = false

			if (option.legend) {
				option.legend.data = this.orderSorter(seriesnames, legendorder);
			}

			// now sort the series
			option.series = this.orderSorter(option.series, order);

			// and if there is a palette, sort that too
			if (option.color) {
				option.color = this.orderSorter(option.color, order);
			}

			// now take care of the axis labels - the value axis need the formatter for abs
			let xAxis = option.xAxis as echarts.EChartOption.XAxis;
			if (xAxis && xAxis.type == 'value') {
				xAxis.axisLabel.formatter = (value) => Math.abs(value);
			}
			let yAxis = option.yAxis as echarts.EChartOption.YAxis;
			if (yAxis && yAxis.type == 'value') {
				yAxis.axisLabel.formatter = (value) => Math.abs(value);
			}

			// relay the event, wth the modified option, to the caller
			this.onChartRender({ option: option, echart: echart })

		}

		public mirrorTooltip(params, datum) {
			datum.value = Math.abs(datum.value);
			datum.percent = Math.abs(datum.percent);
			// if you ask for params, you have to manage the abs value yourself!
			this.tooltipper({ params, datum });

		}

		public clickHandler(params) {
			this.onClick({ params });
		}

	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings = {
			dataset: "<",
			touch: "@",							// this value is to force a chart redraw
			flipped: "<",						// boolean, the row and clumns are reversed in presentation
			charttype: "@",
			colors: "<",
			// handlers 
			tooltipper: "&",
			onChartInit: "&",				// consumers of this chart may want a chance to set up their own
			// event handlers
			onChartRender: "&",
			onClick: "&",
			/* sorting of data items in a sequence; ie row sort in the data */
			sortBy: "<",					// can be a column name, an array of values that represents the required order
			// or a comparator function
			sortDescending: "<",
			columnSortBy: "<",
			columnSortDescending: "<",

			selectedSeries: '<',			// array. if supplied, only those series whose names
			// are in this array are displayed.

			highlight: "<",				// a single value or array of row values ie categories
			// highlight formatting is applied to these

			allowedTypes: "<",				// array of the types that are to be displayed the user can switch between these

			mirrors: "<"							// array of series names that will get mirrored

		};;
		public controller: any = Controller;
		public controllerAs: string = "vm";
		public templateUrl: string = "generic/mirrorchart";
	
	}

	angular
		.module("sw.common")
		.component("mirrorChart", new ComponentOptions())
}
