﻿namespace Sw.Charts {
/**
 * An embedded column chart
 * dataset: the array of numeric values to plot
 *
 * Sizing the chart:
 * height and width may be explicitly supplied.
 * If height is not supplied, it defaults to the current ambient line-height.
 * If width is not supplied it is calculated from height, and the "pixels per point".
 * Pixels per point is the width in pixels to allow for every point in the dataset.
 * This value can be supplied, or the default used.
 *
 * In summary, if none of height ,width and pixels per point is supplied, the line chart will have the
 * height of the current text, and its width will be determined by allocating a reasonable space for the length of
 * data to draw.
 * 
 * 
 * color - color for the columns. If not supplied, the ambient text color.
 * 
 * Negative values:
 * supply color-negative as a color-value to set a different color for -ve columns.
 * set color-negative="" to use default colors for + and - . (note that an explicit value for color will override the 
 * default positive color )
 * 
 *Examples:
 * <spark-column dataset="[3,4,7,-2,4,-3,6]"/> Ambient color used for every column
 * <spark-column dataset="[3,4,7,-2,4,-3,6]" color="red"/> all columns are red
 * <spark-column dataset="[3,4,7,-2,4,-3,6]" color-negative="" /> default colors are used for +ve and -ve values
 * <spark-column dataset="[3,4,7,-2,4,-3,6]" color-negative="orange" /> -ve value orange; +ve values ambient color
 * <spark-column dataset="[3,4,7,-2,4,-3,6]" color-negative="pink" color="teal' /> -ve value pink; +ve values teal
 * <spark-column dataset="[3,4,7,-2,4,-3,6]" color-negative="" color="#7560FF/> default _ve color, explicit color for +ve
 * 
 * */
	const ASPECT_RATIO = .14;
	class Controller extends Spark{

		public dataset: number[];

		public option: echarts.EChartOption;			// bound to the EchartComponent
		
		public userColorNegative: string;

		private useDefaultPosNeg(): boolean {
			return this.userColorNegative == "true" || this.userColorNegative == "default"
				|| this.userColorNegative === "";
		}
		private defaultColorNegative() {
			return "maroon";
		}
		private defaultColorPositive() {
			return "seagreen";
		}

		private get colorNegative(): string {
			if (this.userColorNegative == undefined) {
				return this.color;
			}
			if (this.useDefaultPosNeg()) {
				return this.defaultColorNegative();
			}
			return this.userColorNegative;
		}

		private get colorPositive(): string {
			if (this.useDefaultPosNeg()) {
				return this.userColor??this.defaultColorPositive();
			}
			return this.color;
		}


		public userPxPerPoint: number;
		public defaultPxPerPoint(): number {
			return this.height * ASPECT_RATIO * (this.ambientBold()?1.5:1);  // a bit wider than the corresponding line chart 1.4 vs 1.2
		};
		public get pxPerPoint(): number {
			return this.userPxPerPoint || this.defaultPxPerPoint();
		}

		public defaultWidth():number {
			if (this.dataset?.length) {
				return this.pxPerPoint * this.dataset.length;
			}
			return super.defaultWidth();
		}

		public $onInit() {
			this.makeOption();
		}

		public $onChanges(changes) {
			if (changes.dataset?.isFirstChange()) {
				return;
			}
			this.makeOption();
		}

		public makeOption() {
			this.option = {
				xAxis: {
					type: 'category',
					show: false
				},
				yAxis: {
					type: 'value',
					show: false
				},

				grid: {
					top: 0,
					left: 0,
					right: 0,
					bottom: 0,
					containLabel: false
				},
				series: [{
					data: this.dataset,
					type: 'bar',
				}],
				animation: false,
				visualMap: [{
					show: false,
					type: "piecewise",
					pieces: [
						{ max: 0, color: this.colorNegative },
						{ min: 0, color: this.colorPositive }
					]
				}]
			}
		}

	}
	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public template: string;

		constructor() {
			this.bindings = {
				dataset: "<",							// for now, just an array of values

				userColor: "@color",
				// to color +ve and _ve numbers differently, specify true
				// specify a single color as a string as the color to use for _ve values
				// 
				userColorNegative: "@colorNegative",							

				userHeight: "<height",		// size
				userWidth: "<width",
				userPxPerPoint: "<pxPerPoint",
// event handlers
				onChartRender: "&",
				onClick: "&",
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.template = `<spark-chart 
height="vm.height" width="vm.width"
option="vm.option"
ambient-bold="vm.ambientBold()" ambient-color="vm.ambientColor()" ambient-line-height="vm.ambientLineHeight()" 
on-ambient-change="vm.onAmbientChange(color, bold, lineHeight)"
on-click="vm.clickHandler(params)"
/>`;
		}
	}

	angular
		.module("sw.common")
		.component("sparkColumn", new ComponentOptions())



}