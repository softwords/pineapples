﻿namespace Sw.Charts {
	/**
	 * base class for sparkline objects (spark-percent, spark-line, spark-column)
	 * 
	 */
	export abstract class Spark {
		/**
		 * Height supplied by user via height binding
		 */
		public userHeight: number;
		public defaultHeight() {
			return this.ambientLineHeight();
		};
		public get height(): number {
			return this.userHeight || this.defaultHeight();
		}

		public userWidth: number;
		public defaultWidth() {
			return this.defaultHeight();					//square by default?
		};
		public get width(): number {
			return this.userWidth || this.defaultWidth();
		}

		private defaultColor() {
			return this.ambientColor();
		}
		protected userColor;
		public get color() {
			return this.userColor ?? this.defaultColor();
		}

		// ambient properties
		protected ambientBold(): boolean {
			let fw: string = $(this.element).css("font-weight");
			return fw == "bold" || fw == "700";
		}

		protected ambientColor(): string {
			return $(this.element).css("color");
		}

		protected ambientLineHeight(): number {
			return parseInt($(this.element).css("line-height"));
		}
		public option: echarts.EChartOption;			// bound to the EchartComponent

		public onClick;
		public clickHandler(params) {
			this.onClick({ params });
		}

		private onAmbientChange(color: string, bold: boolean, lineHeight: number) {
			this.makeOption();
		}

		static $inject = ["$element"];
		constructor(protected element: ng.IAugmentedJQuery, protected attrs: ng.IAttributes) {}

		protected abstract makeOption();	
	}

	class SparkController {

		// on the base controller, the ambineProperties are bindings
		private ambientBold: boolean;
		private ambientColor: string;
		private ambientLineHeight: number;

		public userHeight: number;
		public defaultHeight() {
			return parseInt($(this.element).css("lineHeight"));
		};
		public get height(): number {
			return this.userHeight || this.defaultHeight();
		}

		public userWidth: number;
		public defaultWidth() {
			return this.defaultHeight();					//square by default?
		};
		public get width(): number {
			return this.userWidth || this.defaultWidth();
		}

		public option: echarts.EChartOption;			// bound to the EchartComponent
		public echart: echarts.ECharts;						// the echart instance

		public onClick;
		public clickDespatcher = (params) => {
			console.log("in clickDespatcher", params)
			this.onClick({ params });
		}

		public onAmbientChange;
		public doAmbientChange() {
			this.onAmbientChange({ color: this.ambientColor, bold: this.ambientBold, lineHeight: this.ambientLineHeight });
		}

		static $inject = ["$element"];
		constructor(protected element: ng.IAugmentedJQuery) { }

		public $onInit() {
			let chartElement = $(this.element).find("span")[0] as HTMLDivElement;
			this.echart = echarts.init(chartElement, null, {
				renderer: "svg"
			});
			this.echart.on('click', this.clickDespatcher);
			this.drawChart();
		}

		private drawChart() {
			if (this.echart && this.option) {
				this.echart.resize({
					height: this.height,
					width: this.width
				});
				this.echart.setOption(this.option, true);
			}
		}

		public $onChanges(changes) {
			if (changes.option?.isFirstChange()) {
				return;
			}
			if (changes.ambientBold || changes.ambientColor||changes.ambientLineHeight) {
				this.doAmbientChange();
			}
			if (changes.option) {
				this.drawChart();
			}

		}

		public $onDestroy() {
			this.echart.dispose();
		}
	}
	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public template: string;

		constructor() {
			this.bindings = {
				option: "<",							// echarts option to render
				userHeight: "<height",		// size
				userWidth: "<width",
				ambientBold: "<",
				ambientColor: "<",
				ambientLineHeight: "<",

				onAmbientChange: "&",
				// event handlers
				onChartInit: "&",
				onChartRender: "&",
				onClick: "&",

			};
			this.controller = SparkController;
			this.controllerAs = "vm";
			this.template = `<span style="display:inline-block;vertical-align:middle;"/>`;
		}
	}

	angular
		.module("sw.common")
		.component("sparkChart", new ComponentOptions())



}