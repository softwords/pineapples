﻿namespace Sw.Charts {

	export interface IWidgetPkg {
		data: any[][],
		X: string,
		Y: string,
		seriesNames?: string[]
	}



	/**
	 * 
	 * this data shape represents a simple row/column cross tabulation
	 * originally returned from the database pEnrolmentRead.SchoolTable
	 * 
	 */
	export type RowColumnDatum = {
		R: string,			// 'row' identifier
		RC: string,			// 'row' code
		RName: string,	// the type of data in the row - this may correspond to a lookup list name e.g. Districts, Authorities...

		C: string,			// 'col' identifier
		CC: string,			// 'col' code
		CName: string,	// the type of data in the col

		Num: number,			// value for the row-col

		Tot?: number,		 // may include the total across C
		P?: number			// percentage across C
	};

	export interface RowColumnCollection extends Array<RowColumnDatum> { }

	/**
	 * define the columns in an Annotated object 
	 * may be a string array, or a function returning a string array
	 */
	export type ColumnCollector = string[] | ((row: any) => string[]);

	export type ValueAccessor = (row: any, column: string, columnIndex: number) => number;

	/**
	 * return the row Identifier associated to a row in the input
	 * if a string => property (ie column) name
	 * if number => index into the row , (if we are given an array of arrays)
	 * or else function to execute against each row
	 */
	export type RowIdAccessor = string | number | ((row: any) => string);

	/**
	 * a key-value pair
	 */
	export type KeyValuePair<TKey=string, TValue=any> = {
		key: TKey;
		value: TValue;
	}

	/** 
	 *  An array of key-value pairs
	 * a crossfilter group.all() is a specialised KeyValueCollection
	 */
	export interface KeyValueCollection<TKey=string, TValue=any> extends Array<KeyValuePair<TKey, TValue>> { }

	/**
	 * A KeyValueCollection bundled with the accessors 
	 * needed to specify how to build a chart dataset 
	 * Note that the row identifier in the dataset is always the key
	 */
	export type AnnotatedKeyValueCollection<TKey=string, TValue=any>  = {
		keyValues: KeyValueCollection<TKey, TValue> ;
		columns: ColumnCollector;
		valueAccessor: ValueAccessor;
	}


	/**
	 * A crossfilter group bundled with the accessors
	 * needed to specify how to build a chart dataset
	 */
	export type AnnotatedGroup = {
		group: CrossFilter.Group<any, any, any>;
		rowId?: RowIdAccessor;
		columns: ColumnCollector;
		valueAccessor: ValueAccessor;
	}

	export type AnnotatedCollection = {
		collection: any[];
		rowId: RowIdAccessor;
		columns: ColumnCollector;
		valueAccessor: ValueAccessor;
	}

	export type AnnotatedCrossfilter = {
		crossfilter: CrossFilter.CrossFilter<any>;
		rowId: RowIdAccessor;
		columns: ColumnCollector;
		valueAccessor: ValueAccessor;
	}


	export function sortComparator(sortDescending) {
		return (a, b) => (a == b) ? 0 :
			(sortDescending ? (a < b ? 1 : -1) : (a < b ? -1 : 1));
	}

	const ROW_ID = "rowID";

	export class EChartTransformer {

		// This wrapper is an unobtrusive fix for the problem that 
		// the visualMap categories can't work on dimension 0
		// cf https://github.com/apache/incubator-echarts/issues/12636
		// clone the rowIDS into another column at the end to get around this for now
		// This will need to be accounted for in flip logic in row col chart
		static transform(data): echarts.EChartOption.Dataset[] {
			let c = this._transform(data);
			if (!c) {
				console.log("EchartTransformer could not process data");
				console.log(data);
				return NullTransformer.transform();
			}
			c.forEach(dataset => {
				dataset.dimensions.push("id_2");

				(<any[]>dataset.source).forEach(row => {
					row.push(row[0]);
				});
			});
			return c;
		}

		static _transform(data): echarts.EChartOption.Dataset[] {

			try {

				// examine the data to determine what it is.....

				if (NullTransformer.isShapeOf(data)) {
					return NullTransformer.transform();
				}
				// a rol/col object?
				if (RowColumnTransformer.isShapeOf(data)) {
					return RowColumnTransformer.transform(data);
				}


				// Crossfilter group....
				if (CrossfilterGroupTransformer.isShapeOf(data)) {
					return CrossfilterGroupTransformer.transform(data);
				}

				// Crossfilter group.all()... made by xReduce
				if (XReduceTransformer.isShapeOf(data)) {
					return XReduceTransformer.transform(data);
				}
				// annotated cross filter pairs
				if (AnnotatedGroupTransformer.isShapeOf(data)) {
					return AnnotatedGroupTransformer.transform(data);
				}

				// annotated key value pairs
				if (AnnotatedKeyValueCollectionTransformer.isShapeOf(data)) {
					return AnnotatedKeyValueCollectionTransformer.transform(data);
				}

				// key value pairs
				if (KeyValueCollectionTransformer.isShapeOf(data)) {
					return KeyValueCollectionTransformer.transform(data);
				}

				// a legacy 'plottableXXX object from Widgets?
				if (WidgetTransformer.isShapeOf(data)) {
					let c = WidgetTransformer.transform(data);
					return TotallingTransformer.transform(c);
				}

				// using crossfilter 1.4 we can set up a declarively filtered collection
				// using crossfilter.allFiltered()
				if (AnnotatedCrossfilterTransformer.isShapeOf(data)) {
					return AnnotatedCrossfilterTransformer.transform(data);
				}

				// this is a simple collection, together with its accessor functions
				// this is the closest approximation to how many d3-based packages
				// work, where you supply data and accessors
				if (AnnotatedCollectionTransformer.isShapeOf(data)) {
					return AnnotatedCollectionTransformer.transform(data);
				}

			}
			catch (err) {
				console.log("*** Error transforming data ***");
				console.log(err.name);
				console.log(err.message);
				console.log(data);
				return NullTransformer.transform();
			}
		}

		/*
		 * Identify the 'type' of an object by checking if it has a specified
		 * collection of properties - note the syntatic sugar of using  'type guard' functions
		 * based on this
		 */
		public static hasAllProps(data, requiredprops: string[]) {
			let myprops = Object.keys(data);
			let result = true;
			requiredprops.forEach(rp => {

				if (result && myprops.indexOf(rp) == -1) {
					result = false;
				}
			})
			return result;
		}

		public static isKeyValuePair(data): data is KeyValuePair<any, any> {
			return EChartTransformer.hasAllProps(data, ["key", "value"]);
		}
	}

	/* produce well-baheved output when empty data is supplied
	 */
	export class NullTransformer {
		public static isShapeOf(data) {
			// its falsy, its an empty object, its a 0 length arrayyy.
			if (!data) {
				return true;
			}
			if (Array.isArray(data) && (<any[]>data).length == 0) {
				return true;
			}
			if (Object.keys(data).length == 0) {
				return true;
			}
			return false;
		}
		public static transform(dataset?) {
			return [{
				id: "data",
				dimensions: [ROW_ID],
				source: []
			},
			{
				id: "columnTotals",
				dimensions: ["column"],
				source: []
			},
			{
				id: "rowTotals",
				dimensions: [ROW_ID],
				source: []
			},
			]
		}
	}
	/**
	 * Transforms the various shapes described in the preambles to 
	 * dashboard/widgets/plottable.....Chart.Component
	 * These are all variations on a theme, some name the sequnece, some provide colours
	 * 
	 * */
	export class WidgetTransformer {
		public static isShapeOf(data) {
			return (EChartTransformer.hasAllProps(data, ["data", "X", "Y"]))
		}

		public static transform(widgetpkg) {
			let rowIds = [];
			widgetpkg.data.forEach((series, i) => {
				rowIds = rowIds.concat(series.map(item => item[widgetpkg.X]));
			});
			rowIds = _(rowIds).uniq().value();			// take sort out of this

			// step through these items, creating a new row for each, and adding the series values
			let source = rowIds.map(rowId => {
				let values = widgetpkg.data.map((series, i) => {
					let item = _.find(series, dataItem => dataItem[widgetpkg.X] == rowId);
					return item ? item[widgetpkg.Y] : 0;
				});
				return [rowId].concat(values);
			})
			widgetpkg.seriesNames = widgetpkg.seriesNames || widgetpkg.data.map((series, i) => `series${i}`)

			return {
				id: "data",
				dimensions: [ROW_ID, ...widgetpkg.seriesNames],
				source: source
			};
		}
	}

	// takes a single input echarts Dataset, from this prodices the ro and column total datasets
	// and returns the 3 in an array
	export class TotallingTransformer {
		public static transform(dataset) {
			let rowTotals = [];
			let colTotals = [];

			rowTotals = dataset.source.map(row => [
				row[0],
				row.reduce((total, column, i) => i > 0 ? total + column : total, 0)
			])

			colTotals = dataset.dimensions.slice(1).map((col, index) => [
				col,
				dataset.source.reduce((total, row) => total + row[index + 1], 0)
			])

			return [dataset,
				{
					id: "columnTotals",
					dimensions: ["column", "total"],
					source: colTotals
				},
				{
					id: "rowTotals",
					dimensions: [ROW_ID, "total"],
					source: rowTotals
				}
			]
		}
	}

	export class RowColumnTransformer {

		public static isShapeOf(data): data is RowColumnCollection { 
			if (Array.isArray(data) && data.length) {
				return (EChartTransformer.hasAllProps(data[0], ["R", "C", "Num"]))
			}
			return false;
		}

		public static transform(dataset:RowColumnCollection) {

			console.log('RowColumnCollection', dataset);
			let d = dataset;
			let r = _(d).map('R').uniq().value().sort(); // unique 'row' identifiers, sorted
			let c = _(d).map('C').uniq().value().sort(); // unique 'column' identifiers, sorted

			let rowIDs = r;
			let columnIDs = c;

			let rowProvider = d.length ? d[0].RName : "row";
			let columnProvider = d.length ? d[0].CName : "column";

			// make datasets of the row and column totals
			let rowTotals = rowIDs.map(id => ([
				id,				// the rowID
				// accumulate the Num field from each row
				_(d).filter({ "R": id }).reduce((sum, row: any) => sum + row.Num, 0)
			])
			).sort((a, b) => sortComparator(false)(a[1], b[1])); // sort asc on total


			let columnTotals = columnIDs.map(id => ([
				id,
				_(d).filter({ "C": id }).reduce((sum, row: any) => sum + row.Num, 0)
			])
			).sort((a, b) => sortComparator(false)(a[1], b[1]));

			// set up the series definitions
			// a simple shell for each series (ie column value)

			let out = []; //the transformed data;

			// this becomes the dimensions - we need a column placeholder for the row identifier
			c = _.concat([ROW_ID], _.clone(c));
			// get the row name on the front
			//out.push(c);
			r.forEach((rName) => {
				let rc = c.map((cName, cIndex) => {
					if (cIndex == 0) {
						return rName;
					}
					let row: any = _.find(d, { R: rName, C: cName });
					return row ? row.Num : 0;			// change null to 0; works better for lines and areas
				});
				out.push(rc);
			});

			return [
				{
					id: "data",
					dimensions: c,
					source: out
				},
				{
					id: "columnTotals",
					dimensions: [columnProvider, "value"],
					source: columnTotals
				},
				{
					id: "rowTotals",
					dimensions: [rowProvider, "value"],
					source: rowTotals
				}
			];
		}
	}

	export class CrossfilterGroupTransformer {
		public static isShapeOf(data): data is CrossFilter.Group<any, any, any> {
			return EChartTransformer.hasAllProps(data, ["top", "all", "reduce", "order", "size"]);
		}

		public static transform(group: CrossFilter.Group<any, any, any>) {
			let grouping = group.all();
			if  (XReduceTransformer.isShapeOf(grouping)) {
				return XReduceTransformer.transform(grouping);
			}
			return KeyValueCollectionTransformer.transform(grouping);
		}
	}

	export class AnnotatedGroupTransformer {
		public static isShapeOf(data): data is AnnotatedGroup {
			return EChartTransformer.hasAllProps(data, ["group"]);
		}

		public static transform(data: AnnotatedGroup) {
			// see the discussion here on why, in general .all()
			// needs .filter
			//https://stackoverflow.com/questions/21352289/how-to-filter-empty-groups-from-a-reduction

			// xFilter builds Count into the value to help manage this
			// so we use this - if it is there
			let collection = data.group.all().filter(kv => (kv.value.Count === undefined) || kv.value.Count > 0);

			// default columns are the discrete values that appear in the value property of the collection
			// but ignore the extras provided by xReduce
			let columnCollector = data.columns ||
				((kv) => (kv.value) ? _.without(Object.keys(kv.value), "keyAccessor", "valueAccessor", "Count", "Tot") : []);
			return AnnotatedCollectionTransformer.transform({
				collection: collection,
				rowId: data.rowId||"key",
				columns: columnCollector,
				valueAccessor: data.valueAccessor
			});
		}
	}

	export class AnnotatedCrossfilterTransformer {
		public static isShapeOf(data): data is AnnotatedCrossfilter {
			return EChartTransformer.hasAllProps(data, ["crossfilter"]);
		}

		public static transform(data: AnnotatedCrossfilter) {

			let collection = data.crossfilter.allFiltered();

			return AnnotatedCollectionTransformer.transform({
				collection: collection,
				rowId: data.rowId || "key",
				columns: data.columns,
				valueAccessor: data.valueAccessor
			});
		}
	}

	export class KeyValueCollectionTransformer {

		public static isShapeOf(data): data is KeyValueCollection {
			if (Array.isArray(data) && data.length) {
				return EChartTransformer.isKeyValuePair(data[0]);
			}
			return false;
		}

		public static transform(dataset: KeyValueCollection) {
			// test for null on the collection
			if (NullTransformer.isShapeOf(dataset)) {
				return NullTransformer.transform();
			}

			// we need to massage the data, and push it into the chart
			let d = dataset;
			let r = [];
			let c = [];
			d.forEach(kv => {
				r.push(kv.key);
				let vkeys = Object.keys(kv.value);
				c = c.concat(vkeys.length ? vkeys : ["value"]);
			})
			r = _(r).uniq().value(); // unique 'row' identifiers, sorted
			c = _(c).uniq().value(); // unique 'column' identifiers, sorted

			let rowIDs = r;
			let columnIDs = c;

			let rowProvider = "row";
			let columnProvider = "column";

			// make datasets of the row and column totals
			let rowTotals = rowIDs.map(id => ([
				id,				// the rowID
				// accumulate the Num field from each row
				_(d).filter({ "key": id }).reduce((sum, row: any) => sum + row.value, 0)
			])
			).sort((a, b) => sortComparator(false)(a[1], b[1])); // sort asc on total


			let columnTotals = columnIDs.map(id => ([
				id,
				// to do deal only with simple value for now
				_(d).reduce((sum, row: any) => sum + row.value, 0)
			])
			).sort((a, b) => sortComparator(false)(a[1], b[1]));


			// set up the series definitions
			// a simple shell for each series (ie column value)

			let out = []; //the transformed data;

			// this becomes the dimensions - we need a column placeholder for the row identifier
			c = _.concat([ROW_ID], _.clone(c));

			r.forEach((rName) => {
				let rc = c.map((cName, cIndex) => {
					if (cIndex == 0) {
						return rName;
					}
					let row: any = _.find(d, { key: rName });
					if (row) {
						if (cName == "value") {
							return row.value;
						} else {
							return row.value[cName];
						}
					}
					return null;
				});
				out.push(rc);
			});

			return [
				{
					id: "data",
					dimensions: c,
					source: out
				},
				{
					id: "columnTotals",
					dimensions: [columnProvider, "value"],
					source: columnTotals
				},
				{
					id: "rowTotals",
					dimensions: [rowProvider, "value"],
					source: rowTotals
				}
			];
		}
	}

	export class AnnotatedKeyValueCollectionTransformer {
		public static isShapeOf(data): data is AnnotatedKeyValueCollection {
			return EChartTransformer.hasAllProps(data, ["keyValues"]);
		}
		public static transform(data: AnnotatedKeyValueCollection) {
			return AnnotatedCollectionTransformer.transform({
				collection: data.keyValues,
				rowId: "key",
				columns: data.columns,
				valueAccessor: data.valueAccessor
			});
		}
	}

	/**
	 * This is a special case of KeyValue object, which has been created by xFilter.xReduce
	 * The value is an object, that may cross tabulate the data, and may be hierarchical
	 * Therefore
	 * 
	 * */
	export class XReduceTransformer {
		public static isShapeOf(data) {
			if (Array.isArray(data) && data.length && data[0].value) {
				return EChartTransformer.hasAllProps(data[0].value, ["valueAccessor"]);
			}
			return false;
		}
		public static transform(dataset) {
			// we need to massage the data, and push it into the chart
			let d = dataset;
			let r = [];
			let c = [];

			// examine the object to determine how to cross tabulate it in the chart
			// we know we have at leasat 1 row, so look at the first non-empty value
			let v = _.find(d, (kv) => kv.value.Tot);
			// we know from how xReduce works it will have
			// keyAccessor, valueAccessor Count and Tot
			// and may have other properties which are the cross tabulations
			// if Tot is a simple value, then we'll use the names of all the other properties as the dimensions
			// if Tot is an object, we'll accumulate on its property names
			let columnCollector;
			let columnAccessor;
			let vkeys = Object.keys(v.value.Tot);		// PROBLEM: this could be null
			if (vkeys.length) {
				// Tot has properties so we'll use those
				columnCollector = (kv) => (kv.value.Tot) ? Object.keys(kv.value.Tot) : [];
				columnAccessor = (row, columnName) => (row.value.Tot) ? row.value.Tot[columnName] : 0;
			} else {
				// Tot is a simple object, assume the cross tab is the properties of value;
				columnCollector = (kv) => _.filter(Object.keys(kv.value)
					, (prop) => ["valueAccessor", "keyAccessor", "Count", "Tot"].indexOf(prop) == -1);
				columnAccessor = (row, columnName) => row.value[columnName];

			}
			d.forEach(kv => {
				r.push(kv.key);
				let vkeys = columnCollector(kv);
				c = c.concat(vkeys);
			})
			r = _(r).uniq().value(); // unique 'row' identifiers, sorted
			c = _(c).uniq().value(); // unique 'column' identifiers, sorted

			let out = []; //the transformed data;

			// this becomes the dimensions - we need a column placeholder for the row identifier
			c = _.concat([ROW_ID], _.clone(c));

			r.forEach((rName) => {
				let rc = c.map((cName, cIndex) => {
					if (cIndex == 0) {
						return rName;
					}
					let row: any = _.find(d, { key: rName });
					if (row) {
						if (cName == "value") {
							return row.value;
						} else {
							return columnAccessor(row, cName);
						}
					}
					return null;
				});
				out.push(rc);
			});

			return TotallingTransformer.transform({
				id: "data",
				dimensions: c,
				source: out
			});

		}
	}

	export class AnnotatedCollectionTransformer {
		public static isShapeOf(data): data is AnnotatedCollection {
				return EChartTransformer.hasAllProps(data, ["collection"]);
		}
		public static transform(dataset: AnnotatedCollection) {
			// test for null on the collection
			if (NullTransformer.isShapeOf(dataset.collection)) {
				return NullTransformer.transform();
			}

			let d = dataset.collection;
			let r = [];			//collects all the rowIds
			let c = [];			//collects all the column names

			// the default row collector gets the first element of an array, or the first property of the object
			const defaultRowCollector = (row) => Array.isArray(row) ? row[0] : row[Object.keys(row)[0]];
			let rowCollector = defaultRowCollector;
			if (dataset.rowId) {
				if (dataset.rowId instanceof Function) {
					rowCollector = <(any) => any>dataset.rowId;
				} else {
					rowCollector = (row) => row[<string|number>dataset.rowId];
				}

			}
			let columnCollector = Array.isArray(dataset.columns) ? () => dataset.columns : dataset.columns;

			const simpleValueAccessor = (row, cname, cindex) => row[cname];
			let valueAccessor = dataset.valueAccessor??simpleValueAccessor;
			d.forEach(row => {
				r.push(rowCollector(row));
				let vkeys = columnCollector(row);
				c = c.concat(vkeys);
			})

			r = _(r).uniq().value(); // unique 'row' identifiers
			c = _(c).uniq().value(); // unique 'column' identifiers

			//let rowIDs = r;
			//let columnIDs = c;

			//let rowProvider = "row";
			//let columnProvider = "column";


			let out = []; //the transformed data;

			// this becomes the dimensions - we need a column placeholder for the row identifier
			c = _.concat([ROW_ID], _.clone(c));

			// step through again, this time generate the output row
			out = d.map((row) => {
				let rc = c.map((cName, cIndex) => {
					if (cIndex == 0) {
						return rowCollector(row);
					}
					//pass the index as 0-based; it corresponds to the columns returned by the column collector
					return valueAccessor(row, cName, cIndex - 1);
				});
				return rc;
			});

			return TotallingTransformer.transform({
				id: "data",
				dimensions: c,
				source: out
			});
		}

	}
}