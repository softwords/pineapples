﻿namespace Sw.Charts {

		// Row / Column Chart
		/*
		 * This comonent encapsulates a variety of ways of charting 2-dimensional data
		 * - stacked bar chart
		 * - clustered bar chart
		 * - stacked column chart (ie horizontal bar)
		 * - clustered column
		 * - area
		 * - line (not stacked)
		 * In addition, stacked bar and column, and area and line, can be drawn with both aboslute quantities, 
		 * or as percentages of the total.
		 * 
		 * Data is presented to the component as an array of rowcolDatum ( described below)
		 * This will mean that the caller of this component is responsible for manipulating the data into this format.
		 * For example, this data can be produced from an array of key-value pairs, as produced by crossfilter.group.all()
		 * Or, the database may provide this data already in this format.
		 * 
		 * Bindings
		 * 
		 * 	this.bindings = {
				dataset: "<",
				touch: "@",							// this value is to force a chart redraw
				flipped: "<",						// boolean, the row and clumns are reversed in presentation
				charttype: "@",
				colors: "<",
				// handlers
				tooltipper: "&",
				onChartInit: "&",				// consumers of this chart may want a chance to set up their own
																// event handlers
				onChartRender: "&",
				onClick: "&",
				// sorting of data items in a sequence; ie row sort in the data 
				sortBy: "<",					// can be a column name, an array of values that represents the required order
															// or a comparator function
				sortDescending: "<",
				columnSortBy: "<",
				columnSortDescending: "<",

				selectedSeries: '<',			// array. if supplied, only those series whose names
																	// are in this array are displayed.
				highlight: "<",				// a single value or array of row values ie categories
																// highlight formatting is applied to these
		 *  public bindings: any = {
				height: '@',
				width: '@',
				data: "<dataset",			the data rolcolDatum[]
				flipped: "<",						true or false to reverse R and C when drawing
				info: "<info",
				charttype: "@",				Select v , vc, vp (percentage) h, hc, hp, a (area), ap, l, lp
				
				clickHandler: "&",		assigned to ng-class on eacher svg rect element representing a point ()
				tooltipper: "&",		// generate the tooltip text from the datum
				colors:		"<"		// an array of colors may be a literal array, or an expression.
			};
	
			Color examples:
			colors="vm.palette"				' the calling component construct the palette
			colors="['#1010FF', '#223344', '#096723', '#123456','#3000FF', '#503344', '#506723', '#503456']"
			colors="'Category20'"			' the special d3 values Category10, Category20, Category20b, Category20c are permitted
			colors="vm.dashboard.colorbrewer.Oranges[9]"   The colorbrewer library is exposed by Dashboard
			, allowing access to any colorbrewer palette
	
	
		 */

	
	/**
	 * Datum that can be passed to a custom tooltipper. The tooltipper can construct the tooltip and return it. Alternatively, 
	 * if the tooltipper returns no value, the default tooltip is constructed with the values returned in datum.
	 */
	export type TooltipDatum = {
		/** series name */
		series: any;
		/** source column name */
		seriesId: string;
		/** item name */
		item: any;
		/** the marker as in the legend or the default tooltip */
		marker: string;
		value: number;	
		percent: number;
		/** the current chart type */
		charttype: string;
		/** if set, a css class to apply to an enclosing <div> */
		class: string;
		/** if set, css styles to apply to an enclosing <div> */
		style: string;
	}

	/**
	 * the column name in the transformed data that holds the row Identifiers 
	 * This is derived from R in the input data
	 */
	const ROW_ID = "rowID";				

	enum TransformIndex {
		data,
		columns,
		rows
	}

	/**
	 * Used for NULL value in Row or Column 
	 */
	const NOT_DEFINED = "<>";

	class Controller { // extends Sw.Charts.SizeableChartController {

		public dataset;
		public option;
		public echart: echarts.ECharts;
		public touch;				// exists only to force a redraw via a binding change


		/////////////////////////////////////////////////////////////////////////////////////
		// properties reprenting the output of the transform
		/////////////////////////////////////////////////////////////////////////////////////


		public transformed: echarts.EChartOption.Dataset[];

		private flippedIndex(idx: TransformIndex) {
			return ((this.flipped && idx == TransformIndex.rows)
				|| (!this.flipped && idx == TransformIndex.columns)) ?
				TransformIndex.columns : TransformIndex.rows;
		}

		public get transformedData() {
			return this.transformed[TransformIndex.data];
		}

		public get rowIDs(): string[] {
			return [...(<any[]>this.transformed[this.flippedIndex(TransformIndex.rows)].source)
				.map(item => item[0])]
		};
		public get rowTotals(): any[] {
			return <any[]>this.transformed[this.flippedIndex(TransformIndex.rows)].source;
		}
		// helper to get a row total
		public getRowTotal(rowId): number {
			let row = _.find(this.rowTotals, (row) => row[0] == rowId);
			return row ? row[1] : null;
		}

		public get rowProvider(): string {
			return <string>this.transformed[this.flippedIndex(TransformIndex.rows)].dimensions[0];
		};
		public get columnIDs() {
			return [...(<any[]>this.transformed[this.flippedIndex(TransformIndex.columns)].source)
				.map(item => item[0])];
		}
		public columnTotals(): any[] {
			return <any[]>this.transformed[this.flippedIndex(TransformIndex.columns)].source;
		}
		public get columnProvider(): string {
			return <string>this.transformed[this.flippedIndex(TransformIndex.columns)].dimensions[0];
		}


		///////////////////////////////////////////////////////////////////////////////////////
		// processing pipeline
		///////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Operations in the processing pipeline take the data Dataset produced by the transformation
		 * of the incoming data, and act sequentially, at each stage consuming and emitting a Dataset
		 * The final product is the dataset that is presented to the chart ( as option.dataset )
		 * This handles manipulations such as flipping, absolute vs percentage, sorting
		 * 
		 * Each function is memoized for efficiency (is this an issue with very large datasets?)
		 * Therefore, $onChanges has to invalidate the cache of each pipeline method that depends on the 
		 * changed binding - and any pipeline method following. 
		 * For example, if 'flip' changes, then the flipData pipeline method cache must be cleared, 
		 * and so percentage, which comes after , must also be cleared
		 */

		public transpose(matrix) {
			return _.zip(...matrix);
		}

		private _flipData = (c: echarts.EChartOption.Dataset): echarts.EChartOption.Dataset => {
			// we have to assume the dataset is on format 
			// strategy it to use 
			let matrix = [c.dimensions].concat(<any[]>c.source);
			matrix = this.transpose(matrix);
			// FIX for https://github.com/apache/incubator-echarts/issues/12636
			// remove this code when this is fixed in echarts
			// we had an extra column on the right for id_2, after inverting we have an extra row at the bottom 

			let dimensions = matrix[0]; 
			//12 636 fix
			matrix = matrix.slice(1, matrix.length - 1)		// up to but not including the last element
			dimensions.push("id_2");

			matrix.forEach((row: any[]) => {
				row.push(row[0]);
			});

			return {
				dimensions: dimensions,
				source: matrix
			}
		}
		public flipData = _.memoize(this._flipData, () => "")

		//-----------------------------------------------------------------------------------
		// Percentage charts
		// Divide values by row totals to ensure sum of row == 1
		//-----------------------------------------------------------------------------------

		private _percentData = (c: echarts.EChartOption.Dataset): echarts.EChartOption.Dataset => {
			let result:echarts.EChartOption.Dataset = {
				dimensions: c.dimensions,
				source: []
			}
			result.source = (<any[]>c.source).map((row, i) => {
				let tot = this.getRowTotal(row[0]);
				return row.map((d, j) => {
					if (j == 0) {
						return d;				// this is the row Identifier
					}
					return Math.round(d * 1000 / tot) / 10;
				})
			})
			return result;
		}

		private percentData = _.memoize(this._percentData, () => "");

		/////////////////////////////////////////////////////////////
		// Total charts
		/////////////////////////////////////////////////////////////
		// pie charts work with the total data
		//
		private _pieData = (c: echarts.EChartOption.Dataset): echarts.EChartOption.Dataset => {
			switch (this.charttype) {
				case "pie":
					return _.clone(this.transformed[this.flippedIndex(TransformIndex.rows)]);
				case "pieh":
					return _.clone(this.transformed[this.flippedIndex(TransformIndex.columns)]);
				default:
					return c;
			}
		}
		private pieData = _.memoize(this._pieData, () => "");

		private _sortData = (c: echarts.EChartOption.Dataset): echarts.EChartOption.Dataset => {
			(<any[]>c.source).sort(this.sorter(this.sortBy, this.sortDescending));
			return c;
		}
		private sortData = _.memoize(this._sortData, () => "");

		public processPipeline: Sw.Charts.Pipeline;
		
		//public eChartDataset;

		public get chartDataset() {
			if (!this.transformed) {
				return {
					dimensions: [],
					source: []
				}
			}
			// process pipeline : flipped, percent, sort
			let c:echarts.EChartOption.Dataset = {
				dimensions: this.transformedData.dimensions,
				source: this.transformedData.source
			}
			return this.processPipeline.execute(c);
		}

		public highlight;

		//static $inject = ["$compile", "$timeout", "$scope", "$element"];
		//constructor(public compile: ng.ICompileService, timeout: ng.ITimeoutService, public scope: ng.IScope, private element: any) {
		//	super(compile, timeout, scope, element);
		constructor() {
			this.processPipeline = new Sw.Charts.Pipeline();
			this.processPipeline.steps.push([this.flipData, () => this.flipped, ["dataset","touch"]]);
			this.processPipeline.steps.push([this.percentData, () => this.percentage, ["flipped"]]);
			this.processPipeline.steps.push([this.pieData, () => this.pietype, ["charttype"]]);
			this.processPipeline.steps.push([this.sortData, () => (this.sortBy)||(this.sortDescending), ["charttype","sortBy", "sortDescending"]]);
			////how best to handle the sort layer???
		}
		// configuration,
		private stacked: boolean = false;
		private orientation = "v";
		private seriesType = "bar"
		private flipped: boolean;
		private percentage: boolean;
		private pietype: boolean;			// pie charts have some distinct characteristics; e.g. color by datum


		//echart will invoke this in its $onInit event -
		// so we can guarantee it is called and echart exists in $postLink
		public eChartInit(echart) {
			this.echart = echart;
			// relay to the client if requested
			this.onChartInit({ echart });
		}
		public onChartInit;			// the binding may be supplied by the client

		// $postLink occurs when all the chld compoenents hae been initialised
		// we can know that onChartInit has been called, so we must have this.echart
		// Therefore, this is the best place to do the initial setup of the initial chart
		// However, if we have no data here, don't attempt to draw - we'll have to wait for 
		// subsequent changes to bindings

		public $postLink() {
			//console.log('$postLink');
			//console.log(this.echart);
			//console.log(this.dataset);
			this.transformDataset();		// create the null transform if there is no dataset
			if (this.dataset) {
				this.processPipeline.clear();
				this.createChart();
			}
		}

		public $onChanges(changes) {
			//console.log('$onChanges');
			//console.log(changes);
			// don;t both doing anything in the $onChanges called before $onInit
			if (changes.dataset && changes["dataset"].isFirstChange()) {
				return;
			}
			let dataChanged = this.processPipeline.config(changes);
			if (changes.dataset||changes.touch) {
				this.transformDataset();
			}
			if (this.echart) {
				this.createChart();
			}
		}

		//
		public createChart() {
			let option: echarts.EChartOption = this.initialOptions();
			option.series = this.createSeries();
			option.dataset = this.chartDataset;
			
			this.stackSeries(option, this.stacked);
			this.setSeriesType(option, this.seriesType);
			this.orientChart(option, this.orientation);
			this.colorMap(option);
			this.highlightMap(option);
			
			this.doOnChartRender(option);
			this.echart.setOption(option, true);			// not merge, but still respects any existing series and items for 'obje
		}


		
	
		////////////////////////////////////////////////////////////////////////////////////////////
		// callbacks

		// called when rendering a tooltip
		/**
		 * tooltipper can request any of these arguments:
		 * params : the params that are suppplied by echarts to tooltip.formatter
		 * see https://echarts.apache.org/en/option.html#tooltip.formatter
		 * charttype: the current value of charttype
		 * series: 'name' property of the current series
		 * item: 'name' property of the dataum within the series
		 * value: value of the current datum
		 * percent: for pie charts, the percentage
		 * marker: the marker supplied in params
		 * datum: all of the above except params
		 *   datum is convenient becuase it already translates the value from params, which otherwise
		 *   requires you to do the translation as below
		 */
		public tooltipper;					// & binding 
		public doTooltipper = (params): string => {
			let args = this.makeCallbackArgs(params);
			let tt = this.tooltipper(args);
			return tt || this.defaultTooltipper(args.datum);

		}															
		private defaultTooltipper = (datum): string => {
			let contents = "";
			if (this.pietype) {
				contents = `${datum.marker} ${datum.item}: <b>${datum.value}</b>
</br/>${datum.percent}%`;
			} else if (this.percentage) {
				if (datum.percent == datum.value) {
					contents = `${datum.item} <br/>
					${datum.marker} ${datum.series}: <b>${datum.percent}%</b>`;
				} else {
					contents = `${datum.item} <br/>
					${datum.marker} ${datum.series}: <b>${datum.value}</b> (${datum.percent}%)`;

				}
			} else {
				contents = `${datum.item} <br/>
					${datum.marker}${datum.series}: <b>${datum.value}</b>`;
			}
			if (!datum.class && !datum.style) {
				return contents;
			}
			return `<div class="${datum.class}" style="${datum.style}">${contents}</div>`
		}

		/**
		 * Parses echarts {params} argument that is supplied to tooltip and click handlers
		 * into a variety of forms. & bindings for tooltipper and onClick can request any of these parameters
		 * depending on their needs.
		 * For example, to implement "selection" via the click event, it can suffice to simply pass back 'item'
		 */

	
		private makeCallbackArgs = (params) => {
			let value;
			let percent;
			switch (this.orientation) {
				case "v": // value is y
					value = params.value[params.encode.y];
					break;
				case "h":
					value = params.value[params.encode.x];
					break;
				default:
					value = params.value[params.encode.value];
					percent = params.percent;
					break;
			}
			if (this.percentage) {
				percent = value;
				// retrieve the orginal value (ie before calculationof percent) from transformedData
				// this data may have been sorted to produce the current dataset, 
				switch (this.orientation) {
					case "v":
						value = _.find(this.transformedData.source, r => r[0] == params.data[0])?.[params.encode.y];
						break;
					case "h":
						value = _.find(this.transformedData.source, r => r[0] == params.data[0])?.[params.encode.x];
						break;
				}
			}
			let datum: TooltipDatum = {
				series: params.seriesName,
				seriesId: params.seriesId,
				item: params.name,
				marker: params.marker,
				value: value,
				percent: percent,
				charttype: this.charttype,
				class: "",
				style: ""
			};
			return {
				params: params,
				series: params.seriesName,
				item: params.name,
				marker: params.marker,
				value: value,
				percent: percent,
				charttype: this.charttype,
				datum: datum
			}
		}

		public onChartRender;				// & binding 
																// called before the chart is rendered, passing the option and the echart instance
																// onChartRender(option, echart)
		public doOnChartRender(option) {
			this.onChartRender({ option: option, echart: this.echart, renderedType: this.charttype })
		}
		public onClick;						// the client's callback - relayed from clickHandler
		

		////////////////////////////////////////////////////////////////////////////////////////////
		// Selection highlighting
		////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Allows one data item to be selected, and highlighted
		 * Uses echart visualMap to control the selection
		 * @param params
		 */
		private highlightMap(option) {

			if (!this.highlight) {
				return;
			}

			let vm: echarts.EChartOption.VisualMap = {};
			let selectedItems = Array.isArray(this.highlight) ? this.highlight : [this.highlight];
			switch (this.pietype) {
				case true:
					// in a pie, the opacity doesn;t seem to apply to the slice?
					// but it will affect the labels
					vm = {
						id: "highlighter",
						type: 'piecewise',
						show: false,
						categories: selectedItems,
						inRange: {},
						outOfRange: {
							opacity: .3,
							color: "lightgray"
						}
					}
					break;
				default:
					// otherwise ...
					vm = {
						id: "highlighter",
						type: 'piecewise',
						show: false,
						categories: selectedItems,
						inRange: {},
						outOfRange: {
							opacity: .7,
							color: "lightgray"
						}
					}
			}
			option.visualMap = option.visualMap || [];
			option.visualMap.push(vm);
		}

		// use the click handler to control selection
		public clickHandler(params) {
			let args = this.makeCallbackArgs(params);
			this.onClick(args);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Chart type manipulation
		///////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * The client can choose the chart type through UI, 
		 * allowing bar or line or area (chartType)
		 * stacked or clustered (stacked),
		 * horizontal or vertical (orientation)
		 * 
		 */
		private _charttype;

		public get charttype() {
			return this._charttype;
		}
		public set charttype(newtype) {

			this.setChartType(newtype)
			this._charttype = newtype;
		}


		public setChartType(newtype) {
			switch (newtype) {
				case "v":				// stacked 
				case "vp":			// stacked percentage
					this.stacked = true;
					this.orientation = "v";
					this.seriesType = "bar";
					break;
				case "vc":			// clustered
					this.stacked = false;
					this.orientation = "v";
					this.seriesType = "bar";
					break;
				case "h":				// horizontal (stacked)
				case "hp":			// horizontal percentage
					this.stacked = true;
					this.orientation = "h";
					this.seriesType = "bar";
					break;
				case "hc":			// horizontal clustered
					this.stacked = false;
					this.orientation = "h";
					this.seriesType = "bar";
					break;
				case "l":				// line - unstacked
					this.stacked = false;
					this.orientation = "v";
					this.seriesType = "line";
					break;
				case "a":				// area - stacked
					this.stacked = true;
					this.orientation = "v";
					this.seriesType = "area";			// not a 'real' series type - indicates line with areaStyle{}
					break;
				case "pie":			// pie based on row totals
				case "pieh":
					this.stacked = false;
					this.orientation = "";
					this.seriesType = "pie";
					break;


			}

			switch (newtype) {
				case "pie":
				case "pieh":			
					this.pietype = true;
					break;
				default:
					this.pietype = false;
					break;
			};

			switch (newtype) {
				case "hp":
				case "vp":
					this.percentage = true;
					break;
				default:
					this.percentage = false;
					break;
			}
		}

		/**
		 * Set the type of each series in the chart - 
		 * @param seriesArray
		 * @param seriesType - line, bar or area
		 */
		public setSeriesType(option:echarts.EChartOption, seriesType) {
			option.series.forEach((series) => {
				series.type = (seriesType == "area")?"line": seriesType;
				switch (seriesType) {
					case "area":
						series = series as echarts.EChartOption.SeriesLine;
						(series).areaStyle = {};
						break;
					case "pie":
						delete (<any>series).areaStyle;
						break;
					default:
						(<any>series).areaStyle = undefined;
						break;

				}
			});
		}

		//------------------------------------------------------------------------
		// Stacking
		// bar charts may be stacked or clustered (whether in horizontal or vertical orientation)
		//  line charts are never stacked
		//  area charts are always stacked
		//  If data is 'precentage' chart is always stacked
		//------------------------------------------------------------------------
		/**
		 * Stack or unstack all series
		 * @param series - the series array
		 * @param stacked - true or false
		 */
		public stackSeries(option: echarts.EChartOption, stacked: boolean) {
			if (stacked) {
				option.series.forEach((s: any) => {
					s.stack = "A";
				})
			} else {
				option.series.forEach((s: any) => {
					s.stack = undefined;
				})
			}
		}

		//------------------------------------------------------------------------
		// Orientation
		//	Support 'horizontal' or vertical layouts for bar charts 
		//  ie what Excel would call 'bar' (horizntal)or 'column' (vertical)
		//-----------------------------------------------------

		/**
		 * set horizontal or vertical orientation
		 * @param option - the echart option object, assumed to contain the series array to act on
		 * @param orientation - "h" or "v"
		 */
		public orientChart(option, orientation) {
			let valueAxis = {
				type: "value",
				axisLabel: {}
			}
			if (this.percentage) {
				valueAxis.axisLabel = {
					formatter: "{value}%"
				}
			}

			switch (orientation) {
				case "v":
					option.xAxis = {
						type: "category",
						axisLabel: {
							rotate: 90
						}
					};
					option.yAxis = valueAxis;
					option.series.forEach((s: any) => {
						s.encode.y = s.name;
						s.encode.x = ROW_ID;

					})
					option.grid.containLabel = true;
					break;
				case "h":
					option.yAxis = {
						type: "category",
						//data: this.categories
					};
					option.xAxis = valueAxis;

					option.series.forEach((s: any) => {
						s.encode.x = s.name;
						s.encode.y = ROW_ID;
					})
					// allow for a wide category label
					option.grid.containLabel = true;
					break;
				case "": // no orientation for a pie
					option.xAxis = undefined;
					option.yAxis = undefined;
					delete option.grid;
			}
			return option;
		}

		/**
		 * Toggle the current orientation 
		 */
		public changeOrientation() {
			this.orientation = (this.orientation == "h" ? "v" : "h");
			let option = this.echart.getOption();
			let s = angular.copy(option.series);
			option = {
				series: s, 
				grid: option.grid						// containLabel may need to get changed
			};
			this.orientChart(option, this.orientation);
			this.echart.setOption(option);
		}


		//-------------------------------------------------------------
		// Initialisation
		//-------------------------------------------------------------
		public initialOptions() {
			return {
				toolbox: {
					feature: {
						saveAsImage: {}	// note save as image only works with canvas renderer
					}
				},
				tooltip: {

					formatter: this.doTooltipper
				},			// display a tooltip, with default options
				legend: { icon: "circle" },
				grid: {
					containLabel: true,
					left: '5%',				// default margins are 10% of total width, reduce this for a bit more space
					right: '5%'
				},
				textStyle: {
					// global text style is Roboto
					fontFamily: "Roboto"
				}
			}
		}

		public initialiseChart() {
			this.echart.setOption(this.initialOptions(), true);
		}

		////////////////////////////////////////////////////////////////////////////////////////////
		// Palette Management
		////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * A 'palette'; ie an array of color values, can be supplied by bindings in various ways as described 
		 * We want to define a permanent binding between a data item and a value in this array
		 * 
		 */



		// the binding where colors may be passed; either as an array or a name
		public colors;
		
		public get palette():string[] {
			if (!this.colors) {
				return Sw.Charts.category20;
			}

			if (Array.isArray(this.colors)) {
				return this.colors;
			}
			switch (this.colors) {
				case "category10":
					return Sw.Charts.category10;
				case "category20":
					return Sw.Charts.category20;
				case "category20b":
					return Sw.Charts.category20b;
				case "category20c":
					return Sw.Charts.category20c;
				case "google10":
					return Sw.Charts.google10c;
				default:
					return Sw.Charts.category20;
			}
		};
		public colorItems: {
			[provider: string]: string[]
		} = {};

		public colorMap(option: echarts.EChartOption) {
			switch (this.pietype) {
				case true:
					// to color the pie by slice, we use a visualMap
					var categories;
					switch (this.charttype) {
						case "pie":
							categories = this.colorItems[this.rowProvider]
							break;
						case "pieh":
							categories = this.colorItems[this.columnProvider]
							break;
					}
					
					let vm: echarts.EChartOption.VisualMap = {
						id: 'sliceColor',
						type: 'piecewise',
						show: false,
						dimension: this.chartDataset.dimensions[0],
						categories: [NOT_DEFINED].concat(categories),
						inRange: {
							color: ["lightgray", ...this.palette, ...this.palette]
						}
					}
					option.visualMap = option.visualMap || [];
					option.visualMap.push(vm);
					break;
				default:
					// to color the whole series, apply the color property of the series
					let c = this.colorItems[this.columnProvider]
					option.series.forEach((s: any) => {
						let idx = c.indexOf(s.id);
						if (idx == -1) {
							s.color = "gray";
						} else {
							idx = idx % this.palette.length;
							s.color = this.palette[idx];
						}
					});
			}
		}


		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Dataset manipulation
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * A number of manipulations of the chart rely on manipulating the data rather than the configuration of the chart itself
		 * Specifically:
		 *  -- sorting - easiest achived by sort the rows of the dataset prior to rendering
		 *		Note that 'object constancy' will be maintained if series and items have ids
		 *	-- percentage vs absolute values - easiest to achive a percentage bar chart by recalculting the data in the series
		 *	-- flip ; ie swap 'row' and columns - a one line transformation of the matrix!
		 */

		/**
		 * Respond to change of bound data
		 * The data needs to be transformed into the echarts Dataset shape
		 */
		public transformDataset() {
			
				// we need to massage the data, and push it into the chart
				/*console.log("transformDataset:",this.dataset);*/
				this.transformed = Sw.Charts.EChartTransformer.transform(this.dataset);
				// set up the accumulated values that have appeared in columns
				//this.colorItems = null;

				
				let items =
					this.colorItems[this.columnProvider] || [];
				this.colorItems[this.columnProvider] = _(items.concat(this.columnIDs)).uniq()
					// don't put the NOT_DEFINED token <> in the array, treat it separately when rendering
					.filter( columnId => columnId != NOT_DEFINED).value();

				//map row colors as well in case we want to draw a pie
				items =
					this.colorItems[this.rowProvider] || [];
				this.colorItems[this.rowProvider] = _(items.concat(this.rowIDs)).uniq()
					.filter(columnId => columnId != NOT_DEFINED).value();
			
		}

		// binding 
		public selectedSeries: string | string[];
		/**
	 * Create all the required series
	 * Called whenever the chart is recreated
	 */
		public createSeries() {

			let series = [];
			let columnsToCreate = [...this.chartDataset.dimensions]

			let columnFilter = (columnName) => {
				// TEMP FIX
				// for the need to have the rowID duplicated at the end
				// cf https://github.com/apache/incubator-echarts/issues/12636
				if (columnName == "id_2") {
					return false;
				}
				if (columnName == ROW_ID) {
					return false;
				}
				if (!this.selectedSeries) {
					return true;
				}
				if (Array.isArray(this.selectedSeries)) {
					return (this.selectedSeries.indexOf(columnName) >= 0);
				}
				return this.selectedSeries == columnName;
			}

			columnsToCreate = columnsToCreate
				.filter(columnFilter)
				.sort(this.sorter(this.columnSortBy, this.columnSortDescending));

			// find the dimension to use for the item Id and name
			// if ROW_ID is present in the dimensions, use it; otherwise, the first column
			let itemDimension = this.chartDataset.dimensions.indexOf(ROW_ID);
			itemDimension = (itemDimension == -1) ? 0 : itemDimension;

			series = columnsToCreate.map((name, idx) => {

				return {
					name: name,
					id: name,
					seriesLayoutBy: "column",
					animationDurationUpdate: 500,
					animationDuration: 500,
					animationDelay: function (idx) {
						// delay for later data is larger
						return 0;
					},
					animationDelayUpdate: function (idx) {
						// delay for later data is larger
						return idx * 10;
					},
					encode: {
						itemId: itemDimension,   // ROW_ID or the first column
						itemName: itemDimension,
						value:name
					},
					//emphasis: {
					//	label: {
					//		fontWeight: "bold"
					//	}
					//}
				}
			});
			return series;

		}


		//------------------------------------------------------------------
		// Sorting
		//------------------------------------------------------------------
		/**
		 * Support sorting ascending or descending, or either the data point name (ie the ROW_ID)
		 * or the value
		 */

		public sortDescending: boolean = false;
		public sortBy: string | any[] | Function = "id";

		public columnSortDescending: boolean = false;
		public columnSortBy: string | any[] | Function = "id";

		public sorter(sortBy, sortDescending): (a: any, b: any)=>number {
			let sortFunction = (a: any, b:any):number => 0;

			if (!sortBy) {
				return sortFunction;				// do nothing
			}
			if (Array.isArray(sortBy)) {
				// this array is assumed to be the ordered values we are sorting on
				return this.arraySort(sortBy, sortDescending);
			}

			if (sortBy instanceof Function) {
				// of passed, sortBy is a function that sorts ascending
				// flip it if we want descending
				return (a,b) => sortBy(a,b) * (sortDescending?-1:1);
			}

			if (_.isString(sortBy)) {
				switch (sortBy) {
					case "id":
						return this.columnSort(0,sortDescending);
						break;
					case "value":
						return this.valueSort;
						break;
					default:
						return this.columnSort(sortBy, sortDescending);
				}
			}
		}

		private comparator = Sw.Charts.sortComparator(this.sortDescending);

		private columnSort = (sortBy: string | number, sortDescending) =>
			(a:any, b:any):number => Sw.Charts.sortComparator(this.sortDescending)(a[sortBy], b[sortBy]);

		/**
		 * Value sort
		 * 
		 */
		private valueSort = (a, b) => {

			return this.comparator(this.getRowTotal(a[0])
				, this.getRowTotal(b[0]));
		}

		private arraySort = (sortBy: any[], sortDescending) => (a, b) => {
			// compare the indexes of the rowIDs in the suppplied array array of values
			// lower index => smaller value
			let aa = Array.isArray(a) ? a[0] : a;
			let bb = Array.isArray(b) ? b[0] : b;
			let aidx = sortBy.indexOf(aa);
			let bidx = sortBy.indexOf(bb);
			// values not in the sortBy array go to the end
			aidx = aidx == -1 ? 9999999999 : aidx;
			bidx = bidx == -1 ? 9999999999 : bidx;
			return Sw.Charts.sortComparator(sortDescending)(aidx, bidx);
		}


		public sortDataset(out) {
			//	

			if (!this.sortBy) {
				return;				// do nothing
			}
			let sortFunction = this.sorter(this.sortBy, this.sortDescending);
			out.sort(sortFunction);
		}

		public applySort() {
			this.sortDataset(this.chartDataset.source);
			this.echart.setOption({
				dataset: this.chartDataset,
			});
		}


		public toggleSortDirection() {
			this.sortDescending = !this.sortDescending;
			this.applySort();
		}



		/**
		 * 
		 * @param matrix array of arrays
		 * 
		 * so many ways!
		 * https://stackoverflow.com/questions/17428587/transposing-a-2d-array-in-javascript
		 * public transpose = m => m[0].map((x, i) => m.map(x => x[i]))
		 * 
		*/

	
	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public template: string;

		constructor() {
			this.bindings = {
				dataset: "<",
				touch: "@",							// this value is to force a chart redraw
				flipped: "<",						// boolean, the row and clumns are reversed in presentation
				charttype: "@",
				colors: "<", 
				// handlers 
				tooltipper: "&", 
				onChartInit: "&",				// consumers of this chart may want a chance to set up their own
																// event handlers
				onChartRender: "&",
				onClick: "&",
				/* sorting of data items in a sequence; ie row sort in the data */
				sortBy: "<"		,					// can be a column name, an array of values that represents the required order
																// or a comparator function
				sortDescending: "<",			
				columnSortBy: "<",					
				columnSortDescending: "<",		

				selectedSeries: '<',			// array. if supplied, only those series whose names
				  												// are in this array are displayed.

				highlight: "<"	,				// a single value or array of row values ie categories
																// highlight formatting is applied to these

			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.template = Sw.Charts.echartWrapper();
		}
	}

	angular
		.module("sw.common")
		.component("rowColChartCore", new ComponentOptions())
}
