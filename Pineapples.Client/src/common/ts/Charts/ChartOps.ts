﻿namespace Sw.Charts {
	export type Translator =
		string | ((code: string) => string) | ({ [code: string]: string }) | (KeyValueCollection<string, string>)

	/**
	 * Create a ChartOps object to apply common 'recipes' to an echart option object
	 * using a convenient 'fluent' interface
	 * @param option the echart option
	 */
	export function chartOps(option:echarts.EChartOption):ChartOps {
		return new ChartOps(option)
	}

	/**
	 * Class to encapsulate some routine operations on a chart
	 * follows a fluent interface 
	 * e.g.
	 * Sw.Chart.chartOps(option)
	 *		.showLegend(false)
	 *		.showPieLabels(true);
	 */
	export class ChartOps {

		private lookupService: Sw.Lookups.LookupService;
		constructor(public option: echarts.EChartOption) {
		}
		/**
		 * provide a reference to the lookups service
		 * @param lookups
		 */
		public lookups(lookups) {
			this.lookupService = lookups;
			return this;
		}
		/**
		 * Show or hide the legend for a chart
		 * @param show
		 */
		public showLegend(show: boolean = true): ChartOps {
			let o = {
				legend: {
					show: show
				}
			};
			angular.merge(this.option, o);
			return this;
		}


		public placeLegend(hposition: string | number, vposition: string | number, orient?: string) {
			if (!orient) {
				switch (hposition) {
					case "center":
						orient = "horizontal";
						break;
					case "left":
					case "right":
						switch (vposition) {
							case "middle":
								orient = "vertical";
								break;
						}
				}
			};
			let o = {
				legend: {
					left: hposition,
					top: vposition,
					orient: orient??this.option.legend?.orient??"horizontal"
				}
			};
			angular.merge(this.option, o);
			return this;
		}
		
		/**
		 * Show or hide labels on all pie series on the chart
		 * @param show
		 */
		public showPieLabels(show: boolean): ChartOps {
			// this option is applied under a series
			let o = {
				label: {
					show: show
				},
				labelLine: {
					show: show
				}
			};
			this.option.series.filter(s => s.type == "pie").forEach(s => {
				angular.merge(s, o);
			});
			return this;
		}

		/**
		 * Translate category names (ie 'row' labels) using a lookup list, custom object or custom function
		 *
		 * @param translator
		 * if a string, it is the name of a lookup set in the Lookups service
		 * If an object, get the value of the property by name e.g.
		 * { a: "Class A",
		 *	 b: "Class B"
		 * }
		 * Otherwise an explicit function to apply to the item name
		 * 
		 * @param when
		 * boolean condition on which to perform the operation
		 */
		public translateLabels(translator: Translator, when: boolean = true): ChartOps {
			if (!when) {
				return this;
			}

			let translatorFcn = this.translator(translator);
			let o: any = {
				label: {
					formatter: (params) => translatorFcn(params.name)
				}
			};
			this.option.series.forEach(s => {
				if (s.type == "pie") {
					angular.merge(s, o);
				}
			});

			// the formatter function takes different arguments on the axis
			o = {
				axisLabel: {
					formatter: translatorFcn
				}
			};
			this.allAxes().filter(ax => ax.type == "category").forEach(axis => {
				angular.merge(axis, o);
			})
			return this;
		}

		public renameSeries(translator, when: boolean = true) {
			if (!when) {
				return this;
			}
			this.option.series.forEach(s => {
				(<echarts.EChartOption.SeriesCustom>s).name = this.translator(translator)(s.id);
			})
		}
		/**
		 * 	apply an edward tufte-inspired minimal ink presentation...
		 * 	see https://medium.com/plotly/maximizing-the-data-ink-ratio-in-dashboards-and-slide-deck-7887f7c1fab
		 * 	 or , read the book! https://www.edwardtufte.com/tufte/books_vdqi
		 */
		public minimalInk():ChartOps {
	
			let o = {
				backgroundColor: "white"
			}
			angular.merge(this.option, o);

			this.allAxes().forEach(axis => {
				angular.merge(axis, {
					axisLine: {
						show: false
					},
					axisTick: {
						show: false
					}
				});
				if (axis.type == "value") {
					angular.merge(axis, {
						z: 99,
						// splitLine is the line coming out of the axis tick and extendng across the grid
						splitLine: {
							lineStyle: {
								color: 'white',
								opacity: 1,
								width: 1
							}
						}
					});
				}
			});
			return this;
		}
		/**
		 * Collect an array of all axes in the chart
		 */
		private allAxes(): Array<echarts.EChartOption.XAxis | echarts.EChartOption.YAxis> {
			let axes = [];
			if (this.option.xAxis) {
				if (Array.isArray(this.option.xAxis)) {
					axes.concat(this.option.xAxis)
				} else {
					axes.push(this.option.xAxis);
				}
			}
			if (this.option.yAxis) {
				if (Array.isArray(this.option.yAxis)) {
					axes.concat(this.option.yAxis)
				} else {
					axes.push(this.option.yAxis);
				}
			}
			return axes;
		}

		/**
		 * Return a function to translate one string into another
		 * 
		 * @param translator
		 * if a string, it is the name of a lookup set in the Lookups service
		 * If an object, get the value of the property by name e.g.
		 * { l0: "Level 0",
		 *	  l1: "Level 1"
		 * }
		 * if an array, must ba an array of key-value pairs, both strings
		 * Otherwise an explicit function to apply to the item name		 
		 */
		public translator(translator: Translator): (code:string) => string {
			let f: (code: string) => string;
			if (typeof (translator) == "string") {
				f = (value) => this.lookupService.byCode(translator, value, "N");
			} else if (Array.isArray(translator)) {
				// assume key value pair
				f = (code) => _.find(translator, { key: code })?.value ?? code;
			} else if (translator instanceof Function) {
				f = translator as (code: string) => string;
			} else {
				f = (value: string) => translator[value] ?? value;
			}
			return f;
		}
	}
}