﻿namespace Sw.Charts {

	class Controller { // extends Sw.Charts.SizeableChartController {


		public onChartRender;				// & binding 
		public onClick;
		public tooltipper;					// & binding just supporting datum
		// called before the chart is rendered, passing the option and the echart instance

		public charttype;
		public userChartType;

		public allowedTypes: string[]|string;

		public allAllowedTypes() {
			let t = ["v", "vp", "vc", "h", "hp", "hc", "l", "a", "pie", "pieh"];
			return t.filter(type => this.isAllowedType(type));
		}

		public isAllowedType(typeCode) {
			
			if (Array.isArray(this.allowedTypes)) {
				return this.allowedTypes.indexOf(typeCode) >= 0 || typeCode == this.userChartType;
			}
			if (this.allowedTypes == "all") {
				return true;
			}
		}
		
		public doTooltip(params, series, item, marker, value, percent, charttype, datum) {
			return this.tooltipper({ params, series, item, marker, value, percent, charttype, datum });
		}

		public doClick(params, series, item, marker, value, percent, charttype, datum) {
			this.onClick({ params, series, item, marker, value, percent, charttype, datum});
		}

		public doChartRender(option, echart, renderedType) {
			this.onChartRender({option, echart, renderedType})
		}

		public $onChanges(changes) {
			/* a chart type supplied by the parent becomes the current chart type
			 * However, this may be changed through interaction with the toolbar in this component
			 */
			if (changes.userChartType && this.userChartType) {
				this.charttype = this.userChartType;
			}
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any = Controller;
		public controllerAs: string = "vm";
		public templateUrl: string = "generic/rowcolchart";

		constructor() {
			this.bindings = {
				dataset: "<",
				touch: "@",							// this value is to force a chart redraw
				flipped: "<",						// boolean, the row and clumns are reversed in presentation
				userChartType: "@charttype",
				colors: "<",
				// handlers 
				tooltipper: "&",
				onChartInit: "&",				// consumers of this chart may want a chance to set up their own
				// event handlers
				onChartRender: "&",
				onClick: "&",


				/* sorting of data items in a sequence; ie row sort in the data */
				sortBy: "<",					// can be a column name, an array of values that represents the required order
				// or a comparator function
				sortDescending: "<",
				columnSortBy: "<",
				columnSortDescending: "<",

				selectedSeries: '<',			// array. if supplied, only those series whose names
				// are in this array are displayed.

				highlight: "<",				// a single value or array of row values ie categories
				// highlight formatting is applied to these

				allowedTypes: "<"				// array of the types that are to be displayed the user can switch between these

			};

		}
	}

	angular
		.module("sw.common")
		.component("rowColChart", new ComponentOptions())
}

