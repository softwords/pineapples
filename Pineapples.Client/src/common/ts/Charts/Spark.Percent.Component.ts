﻿namespace Sw.Charts {
	/**
	 * An embedded pie chart to represent a single value between 0 - 100
	 * The value may be supplied in one of 3 ways, in order of precedence
	 * n, d supply the bindings n = numerator d = denominator the percentage is calculated
	 * ratio: supply the binding ratio between 0 and 1 
	 * percent: supply the percentage between 0 and 100
	 * 
	 * Other bindings:
	 * color - a color for the pie slice. If not supplied, default to the current 'ambient' color;
	 * ie the value of 'color' at the spark-percent tag
	 * size - the size of the pie - which is square. If not supplied, defaults t the current line-height.
	 */
	class Controller extends Spark {

		public value: number;

		public percent: number;
		public ratio: number;
		public numerator: number;
		public denominator: number;

		public userSize: number;
		public defaultSize() {
			return this.ambientLineHeight()
		}
		public get size() {
			return this.userSize || this.defaultSize();
		}

		public option: echarts.EChartOption;			// bound to the EchartComponent

		public $onInit() {
			if (this.numerator || this.denominator) {
				this.value = 100 * this.numerator / this.denominator;
			} else if (this.ratio) {
				this.value = 100 * this.ratio;
			} else if (this.percent) {
				this.value = this.percent;
			}
			this.makeOption();
		}

		public $onChanges(changes) {
			if (changes.percent?.isFirstChange()) {
				return;
			}
			if (changes.numerator || changes.denominator) {
				this.value = 100 * this.numerator / this.denominator;
			} else if (changes.ratio) {
				this.value = 100 * this.ratio;
			} else if (changes.percent) {
				this.value = this.percent;
			}

			this.makeOption();
		}

		public makeOption() {
			this.option = {
				series: [{
					data: [
						{
							value: this.value,
							itemStyle: {
								color: this.color
							}
						},
						{
							value: 100 - this.value,
							itemStyle: {
								color: "rgb(255,255,255,0)", // opacity 0
								borderColor: this.color,
								borderWidth: 1
							}
						}],
					type: 'pie',
					labelLine: {
						show: false
					},
					hoverAnimation: false
				}],
				animation:false
			};
		}

	}
	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public template: string;

		constructor() {
			this.bindings = {
				percent: "<",							// value is a percent - ie 100 is a full circle
				ratio: "<",								// alternatively , a value between 0 and 1
				numerator: "<n",										// alternatively, numerator and denominator
				denominator:	"<d",
				userColor: "@color",

				userSize: "<size",		// override height
				// event handlers
				onChartRender: "&",
				onClick: "&",
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.template = `
<spark-chart
height="vm.size" width="vm.size"
option="vm.option"
ambient-bold="vm.ambientBold()" ambient-color="vm.ambientColor()" ambient-line-height="vm.ambientLineHeight()" 
on-ambient-change="vm.onAmbientChange(color, bold, lineHeight)"
on-click="vm.clickHandler(params)"
/>`;
		}
	}

	angular
		.module("sw.common")
		.component("sparkPercent", new ComponentOptions())



}