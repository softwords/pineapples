﻿namespace Sw.Charts {
	/**
	 * An embedded line chart
	 * dataset: the array of numeric values to plot
	 * 
	 * Sizing the line:
	 * height and width may be explicitly supplied.
	 * If height is not supplied, it defaults to the current ambient line-height.
	 * If width is not supplied it is calculated from height, and the "pixels per point".
	 * Pixels per point is the width in pixels to allow for every point in the dataset.
	 * This value can be supplied, or the default used. 
	 * 
	 * In summary, if none of height ,width and pixels per point is supplied, the line chart will have the
	 * height of the current text, and its width will be determined by allocating a reasonable space for the length of
	 * data to draw. 
	 * 
	 * color - color for the line. If not supplied, the ambient text color.
	 * 
	 */
	class Controller extends Spark{

		public dataset: number[];

		public option: echarts.EChartOption;			// bound to the EchartComponent

		
		public userPxPerPoint: number;
		public defaultPxPerPoint(): number {
			return this.height /10 * 1.2 * (this.ambientBold()?1.5:1);
		};
		public get pxPerPoint(): number {
			return this.userPxPerPoint || this.defaultPxPerPoint();
		}

		public defaultWidth():number {
			if (this.dataset?.length) {
				return this.pxPerPoint * this.dataset.length;
			}
			return super.defaultWidth();
		}

		public $onInit() {
			this.makeOption();
		}

		public $onChanges(changes) {
			if (changes.dataset?.isFirstChange()) {
				return;
			}
			this.makeOption();
		}

		public makeOption() {
			this.option = {
				xAxis: {
					type: 'category',
					show: false
				},
				yAxis: {
					type: 'value',
					show: false
				},

				grid: {
					top: 0,
					left: 0,
					right: 0,
					bottom: 0,
					containLabel: false
				},
				series: [{
					data: this.dataset,
					type: 'line',
					lineStyle: {
						width: this.ambientBold() ? 2 : 1,
						color: this.color
					},
					showAllSymbol: false
				}],
				animation: false
			}
		}

	}
	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public template: string;

		constructor() {
			this.bindings = {
				dataset: "<",							// for now, just an array of values
				userColor: "@color",

				userHeight: "<height",		// size
				userWidth: "<width",
				userPxPerPoint: "<pxPerPoint",
// event handlers
				onChartRender: "&",
				onClick: "&",
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.template = `<spark-chart 
height="vm.height" width="vm.width"
option="vm.option"
ambient-bold="vm.ambientBold()" ambient-color="vm.ambientColor()" ambient-line-height="vm.ambientLineHeight()" 
on-ambient-change="vm.onAmbientChange(color, bold, lineHeight)"
on-click="vm.clickHandler(params)"
/>`;
		}
	}

	angular
		.module("sw.common")
		.component("sparkLine", new ComponentOptions())



}