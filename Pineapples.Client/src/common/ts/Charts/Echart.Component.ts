﻿namespace Sw.Charts {

	//as at 16 June 2024 this wrapper is only used by rowColChartCore
	export function echartWrapper() {
		let template = `
<div id="vm.chartId" ng-class="vm.chartNgClass" class="vm.chartCssClass" layout="column" layout-fill>
	<e-chart-component 
		layout="column" layout-fill
		option="vm.option" on-click="vm.clickHandler(params)"
		on-dblclick="vm.dblClickHandler(params)"
		on-chart-init="vm.eChartInit(echart)"
		renderer="{{vm.renderer}}"
	>
	</e-chart-component>

</div>`;
		return template;
	}

	export const category10 = [
		"#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"
	];

	export const google10c = ["#3366cc"
		, "#dc3912" 
		, "#ff9900"
		, "#109618"
		, "#990099"
		, "#0099c6"
		, "#dd4477"
		, "#66aa00"
		, "#b82e2e"
		, "#316395"
	]

	export const category20 = [
		"#1f77b4"
		, "#aec7e8"
		, "#ff7f0e"
		, "#ffbb78"
		, "#2ca02c"
		, "#98df8a"
		, "#d62728"
		, "#ff9896"
		, "#9467bd"
		, "#c5b0d5"
		, "#8c564b"
		, "#c49c94"
		, "#e377c2"
		, "#f7b6d2"
		, "#7f7f7f"
		, "#c7c7c7"
		, "#bcbd22"
		, "#dbdb8d"
		, "#17becf"
		, "#9edae5"
	]

	export const category20b = [
		, "#393b79"
		, "#5254a3"
		, "#6b6ecf"
		, "#9c9ede"
		, "#637939"
		, "#8ca252"
		, "#b5cf6b"
		, "#cedb9c"
		, "#8c6d31"
		, "#bd9e39"
		, "#e7ba52"
		, "#e7cb94"
		, "#843c39"
		, "#ad494a"
		, "#d6616b"
		, "#e7969c"
		, "#7b4173"
		, "#a55194"
		, "#ce6dbd"
		, "#de9ed6"
	]

	export const category20c = [
		"#3182bd"
		,  "#6baed6"
		,  "#9ecae1"
		,  "#c6dbef"
		,  "#e6550d"
		,  "#fd8d3c"
		,  "#fdae6b"
		,  "#fdd0a2"
		,  "#31a354"
		,  "#74c476"
		,  "#a1d99b"
		,  "#c7e9c0"
		,  "#756bb1"
		,  "#9e9ac8"
		,  "#bcbddc"
		,  "#dadaeb"
		,  "#636363"
		,  "#969696"
		,  "#bdbdbd"
		,  "#d9d9d9"
	]

	
	class Controller extends Sw.Charts.SizeableComponent{

		public renderer: string;
		public option;
		public onClick;
		public onDblClick;
		public onChartInit;
		public echart: echarts.ECharts;

		public $onInit() {
			// this renders using svg
			// get the base element for the echart
			let chartElement = $(this.element).find("div.echart")[0] as HTMLDivElement;
			if (this.renderer.toLowerCase() == "svg") {
				this.echart = echarts.init(chartElement, null, {
					renderer: "svg"
				});
			} else {
				// default will use the canvas renderer
				this.echart = echarts.init(chartElement);
			}
			// use the & binding to give the client the click event
			this.echart.on('click', this.clickDespatcher);
			this.echart.on('dblclick', this.dblClickDespatcher);
			// give the client the echart if it wants to manipulate it directly
			this.onChartInit({ echart: this.echart });

			// client may interact with the echart using the option binding
			if (this.option && this.echart) {
				this.echart.setOption(this.option);
				this.echart.resize();
				this.echart.hideLoading();
				
			}
		}
		public $onChanges(changes) {
			if (changes.option && this.echart) {
				this.echart.setOption(this.option);
				this.echart.resize();
				this.echart.hideLoading();
			}
		}

		public $onDestroy() {
			if (this.echart) {
				this.echart.dispose();
			}
		}

		// overridable
		public onResize(element) {
			if (this.size.height === 0) {
				return;
			}
			if (!this.echart) {
				// no echart
				return;
			}
			let option = this.echart.getOption();
			if (!option) {
				// there is no chart drawn yet, but resize the echart anyway
				this.echart.resize();
				return;
			}
			// turn off any animation before resizing
			let anims: echarts.EChartOption = {
				animation: false
			}
			this.echart.setOption(anims);
			this.echart.resize();
			// restore any animation
			if (option.animation) {
				anims = {
					animation: option.animation
				}
				this.echart.setOption(anims);
			}
		}

		public clickDespatcher = (params) => {
			// invoke the clickHandler & binding, use timeout to get back into angular-land
			this.timeout(this.onClick, 0, true, { params });
		}

		public dblClickDespatcher = (params) => {
			// invoke the clickHandler & binding, use timeout to get back into angular-land
			this.timeout(this.onDblClick, 0, true, { params });
		}

	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			renderer: "@",			// supply the string literal svg for svg rendering; anything else is canvas			
			option: "<",
			onClick: "&",
			onDblClick: "&",
			onChartInit: "&"
		};
		public controller = Controller;
		public controllerAs = "vm";
		// resizing - in order for height 100% to work here, the size of the parents
		// must be deterministic - we can use angularjs-material 
		// to push the size through a hierarchy of  layout = "column" flex
		public template = `<div class="echart" style="width:100%; height: 100%"/>`;
	}

	angular
		.module("sw.common")
		.component("eChartComponent", new Component())

	export interface IDatashapeTransformer {
		transform(data: any): echarts.EChartOption.Dataset[]
	}

	

	/**
	 * The Pipeline class holds an array of processes that will be performaned to shape
	 * data for an echart.
	 * The process begins with the Transformed data  - ie the initial data supplied
	 * by the parent of the chart, and each step acts on the output of the previous
	 * 
	 * Each step is an array of 3 elements:
	 * 0: function that consumes and emits a datset, memoized using _.memoize
	 * 1: condition under which the step needs to be run. This is typically derived from a binding
	 * 2: array of binding names that invalidate any cached result for this step
	 *    - and hence, for any subsequent step
	 * */
	export class Pipeline {
		public steps: Array<[_.MemoizedFunction, Function, string[]]> = [];

		/**
		 * Clear any cached values in the pipline that have become invalid
		 * @param changes - called from the $onChanges event, this is the changes object
		 * So its key names are all the modified bindings
		 */
		public config(changes):boolean {
			let bindings = Object.keys(changes);
			let clear = false;
			this.steps.forEach((step, seq) => {
				if (clear) {
					step[0].cache.clear();
					return;
				} 
				step[2].forEach((bindingName) => {
					if (!clear && bindings.indexOf(bindingName) >= 0) {
						step[0].cache.clear();
						clear = true;
					}
				})
			})
			return clear;
		}

		/**
		 * Clear all caches in the pipline. Used to initialize the pipeline 
		 */
		public clear() {
			this.steps.forEach((step, seq) => {
				step[0].cache.clear();
			});
		}

		/**
		 * Construct the chart-ready Dataset, by applying the pipline to the 
		 * (transformed) user supplied data.
		 * @param start - Dataset to feed into the pipeline
		 */
		public execute(start) {
			let o = start;
			this.steps.forEach((step) => {
				if (step[1]()) {
					o = (<any>step[0])(o);
				} 
			});
			return o;
		}
	}
}