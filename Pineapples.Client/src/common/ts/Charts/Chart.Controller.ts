﻿module Sw {

  export interface IRowColDatum {
    R: string;
    C: any;
    RC: string;       // code, if any for the row value
    Num: number;
  }

  export interface ITableCalculator {
    getValue(datum: IRowColDatum, dataItem: string): number;
    formatValue(datum, dataItem: string): string;
  }

  export class TableCalculator implements ITableCalculator {
    public getValue(datum: IRowColDatum, dataItem) {
      if (datum) {
        // check if we are returning an absolute or ratio or percentage
        //
        return datum.Num;
      }
      return null;
    }

    public formatValue(v: number, dataItem: string) {
      if (dataItem.substring(0, 4) === "perc") {
        return (v * 100).toFixed(1) + "%";
      }
      if (dataItem.substring(0, 3) === "avg") {
        return v.toFixed(1);
      }
      return v.toFixed(0);
    }
  }

  interface IPalette {
    bands: number;        // number of colors in the palette
    scheme: string;       // colorbrewer scheme name
    invert: boolean;       // invert the palette sequence
  }

  export class TableController {
    public isWaiting = false;

    public ready;
    public topodata;

    public get chartType() {
      return this.findConfig.current.table.chartType || "v";
    }
    public get flipped(): boolean {
      return this.findConfig.current.table.flipped;
    }
    public data = [];
    public xtabdata = [];
    public xtabcolumns = [];
    public xtabtotal;

    public mapdata = [];
    public rowTotals = [];

    public tableparams: Sw.Filter.TableParams;

    protected tableCalculator: ITableCalculator;

    public getValue(c) {
      return this.tableCalculator.getValue(c, this.findConfig.current.table.dataItem);
    }

    public v(c) {
      let v = this.getValue(c);
      if (v === null) {
        return null;
      }
      return this.tableCalculator.formatValue(v, this.findConfig.current.table.dataItem);
    }
    public mapValueAccessor = (d: any) => d.Total;

    // map extent is an array of the features to draw on the map
    // e.g. this could be africa, as a subset of the world
    public mapExtent = [];

    static $inject = ['$scope', 'theFilter', 'Lookups', 'findConfig'];

    constructor($scope, public theFilter: Sw.Filter.IFilter, protected lookups: Sw.Lookups.LookupService
      , public findConfig: Sw.Filter.FindConfig) {

      this.ready = false;
      $scope.vm = this;
      this.tableparams = findConfig.current.table;

      this.tableCalculator = theFilter.createTableCalculator();

      // the FindNow button got pushed - we need to redraw the chart
      $scope.$on('FindNow', (event, data: Sw.Filter.IFindNowBroadcast) => {
        if (data.filter.entity === theFilter.entity) {
          data.actions.push(Sw.Filter.Request.table);
          this.ready = true;      // to get the waiting gif to display
          this.isWaiting = true;
        }
      });

      $scope.$on('SearchComplete', (event, resultpack) => {
        if (resultpack.entity === theFilter.entity && resultpack.method === 'table') {
          this.data = resultpack.resultset;
          this._buildXTab();
        }
        this.isWaiting = false;
        this.ready = true;
      });

      $scope.$on('SearchError', (event, resultpack) => {
        if (resultpack.entity === theFilter.entity) {
          // no point waiting around - this train aint comin'
          this.isWaiting = false;
        }
      });

      // finally, if this Filter has already been searched, tell it we want to search again
      if (this.findConfig.prepared) {
        this.theFilter.FindNow(this.findConfig.current);
      }
    };

    protected _rollup = (d) => {

      return d.reduce((p, c: IRowColDatum) => {
        p.RC = c.RC;
        if (c.Num) {
          if (p[c.C] === undefined) {
            p[c.C] = {};
          }
          let pc = p[c.C];
          if (p.Total === undefined) {
            p.Total = {};
          }
          let pt = p.Total;
          for (var propertyname in c) {
            switch (propertyname) {
              case "R":
              case "C":
              case "RC":
              case "CName":
              case "RName":
                break;
              default:
                pc[propertyname] = (pc[propertyname] ? pc[propertyname] + c[propertyname] : c[propertyname]);
                pt[propertyname] = (pt[propertyname] ? pt[propertyname] + c[propertyname] : c[propertyname]);
            }
          }
        };
        return p;
      }, {});
    };

    protected _buildXTab() {
      this.xtabdata = d3.nest()
        .key((d: IRowColDatum) => d.R)
        .sortKeys(d3.ascending)
        .rollup(d => this._rollup(d))
        .entries(this.data);

      this.xtabcolumns = d3.nest()
        .key((d: IRowColDatum) => d.C)
        .sortKeys(d3.ascending)
        .entries(this.data);

      this.xtabtotal = d3.nest()
        .key(d => 'Total')
        .rollup(d => this._rollup(d))
        .entries(this.data);
    }
  };

  class ChartController extends TableController {
    private _loadedMap: string;     /// id of the loaded map - this comes from the FieldOptions table (row.g)
    private _previousChart: string;  // when switching to map , remember the previous type
    // if we need

    static $inject = ['$scope', 'theFilter', 'Lookups', 'findConfig', 'mapAPI', 'palette', 'mapColorScale'];

    constructor($scope, public theFilter: Sw.Filter.IFilter, protected lookups: Sw.Lookups.LookupService
      , public findConfig: Sw.Filter.FindConfig, private mapApi: Sw.Api.IMapService
      , public palette: Sw.Maps.IColorBrewerPalette
      , public mapColorScale: Sw.Maps.IMapColorScale) {
      super($scope, theFilter, lookups, findConfig);
      $scope.$on('SearchComplete', (event, resultpack) => {
        if (resultpack.entity === theFilter.entity && resultpack.method === 'table') {
          this.data = resultpack.resultset;
          this._buildXTab();
          this._buildMapData();
        }
        this.isWaiting = false;
        this.ready = true;
      });
      $scope.$watch("vm.palette.bands", (newValue, oldValue) => {
        // if the number of bands has changed, we'll need to recalc the colorScale
        mapColorScale.calcThresholds(this.mapdata, newValue);
      }, false);

      $scope.$watch("vm.findConfig.current.table.chartType", (newValue, oldValue) => {
        if (newValue && newValue !== oldValue) {
          switch (newValue) {
            case "map":
              this._loadTopoMap(this.findConfig.current.table.row);
              this._previousChart = oldValue;
              break;
          }
        }
      });

      $scope.$watch("vm.findConfig.current.table.row", (newValue, oldValue) => {

        // now we need to get the appropriate map data from a json call and load it
        // this is a bit of duplication //TO DO find a better home for this
        if (newValue && newValue !== oldValue) {
          this._loadTopoMap(newValue);
        }
      });
      $scope.$watch("vm.findConfig.current.table.dataItem", (newValue, oldValue) => {

        // now we need to get the appropriate map data from a json call and load it
        // this is a bit of duplication //TO DO find a better home for this
        if (newValue && newValue !== oldValue) {
          this._buildMapData();
        }
      });
    }

    private _loadTopoMap = (rowOption: string) => {
      let row = this.lookups.byCode(this.findConfig.tableOptions, rowOption);
      if (row && row.g) {
        // we have a map to draw
        if (row.g !== this._loadedMap) {
          this.mapApi.topo(row.g).then((topo) => {
            this.topodata = topo;
            this._loadedMap = row.g;
          });
        }
      } else {
        // we don;t have a map to draw - so, if chartType is map, revert to something else
        if (this.findConfig.current.table.chartType === "map") {
          this.findConfig.current.table.chartType = this._previousChart || "v";
        }
      }
    };

    private _buildMapData() {
      // the map data is a transform to the form { id: <iso_a2>, value: number }
      let newdata = [];
      // use forEach instead of map becuase we need to get rid of any null element
      this.xtabdata.forEach(p => {
        if (p.values.RC != null) {
          newdata.push({
            id: p.values.RC,
            value: this.getValue(p.values.Total)
          });
        };
      });
      this.mapdata = newdata;
      this.mapColorScale.calcThresholds(this.mapdata, this.palette.bands);
    };
  }
  angular
    .module("sw.common")
    .controller("TableController", TableController)
    .controller("ChartController", ChartController);
}
