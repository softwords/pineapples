﻿namespace Sw.Api {
	export interface IDeflatedTable {
		columns: string[];
		rows: any[][];
	}

	export interface IDeflatedTableSet {
		tables: { [tableName: string]: IDeflatedTable}
	}


	export interface IReflator {
		get<T>(url: string, config?: ng.IRequestShortcutConfig): ng.IHttpPromise<T>;
		reflate(table: IDeflatedTable): any;
	}

	class Reflator {

		static isDeflatedTable(data): data is IDeflatedTable {
			return _.has(data, "columns");
		}

		static isDeflatedTableSet(data): data is IDeflatedTableSet {
			return _.has(data, "tables");
		}
		static $inject = ['$http'];
		constructor(public http: ng.IHttpService) { }

		/**
		 * Reflator get wraps the usual http get, but inserts the custom header
		 * X-Json-Deflate = ON
		 * At the server, any method marked with the attribute [Deflatable]
		 * can respond to this flag by "deflating" a DataTable or DataSet
		 * 
		 * When the data is received, it is converted back to a Json collection.
		 * Note that if the method is not marked [Deflatable] this flag has no effect
		 * 
		 * @param url : url to navigate to
		 * @param config : optional configuration - passed to $http.get
		 */
		public get<T>(url: string, config?: ng.IRequestShortcutConfig) {
			config = config || {};
			config.headers = config.headers || {};
			config.headers["X-Json-Deflate"] = "ON"; 
			return this.http.get(url, config).then(response => {
				if (Reflator.isDeflatedTable(response.data)) {
					response.data = this.reflate(response.data);
				} else if (Reflator.isDeflatedTableSet(response.data)) {
					response.data = this.reflateSet(response.data);
				}
				return response as ng.IHttpPromiseCallbackArg<T>;
			});
		}

		/**
		 * decode a compressed datatable
		 * This object has two properties
			* columns: an array of the column names
			* rows: an array of arrays. Each array is the values of the data
			* this will turn this structure into a normal Json collection; ie array of objects
			* Objects in this form are created by Softwords.Web DatasetSerializer
			* @param table: the 'deflated table'
		 */
		public reflate(table: IDeflatedTable) {
			let reflateRow = (row) => {
				let out = {};
				row.forEach((cell, index) => {
					out[table.columns[index]] = cell;
				});
				return out;
			}
			return table.rows.map(reflateRow);
		}

		/**
			* decode a compressed dataset
		  * This is an object with a single property 'tables'
		  * The value is an object where each property name is the table name, 
		  * and the property value is an IDeflatedTable
			* This object has two properties
			* columns: an array of the column names
			* rows: an array of arrays. Each array is the values of the data
			* this will turn this structure into a normal Json collection; ie array of objects
			* Objects in this form are created by Softwords.Web DatasetSerializer
			* @param table
			*/
		public reflateSet(tableset: IDeflatedTableSet) {
			// iterate over all the properties of the object, converting each value to a collection
			return _.mapValues(tableset.tables, (d) => this.reflate(d));
		}

	}
	angular
		.module("sw.common")
		.service("reflator", Reflator)
}