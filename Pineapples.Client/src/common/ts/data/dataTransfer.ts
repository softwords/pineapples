﻿namespace Pineapples.Api {

  
  /** These objects represent a data response returned from a server side IDataResult, 
   * that contains a single array (ie one recordset ), together with some metaData
   * The original interfaces and classes were used
   * to return the array decorated with properties for the metadata.
   * 
   * these objects do the same, but generecially typed, so we can start to have types representing the
   * structure of the recordsets returned.
   * These can be used on http reads, which are replacing more restangular reads where we don't need the restangular
   * decorations to faciliate data IO
   */

  export function isPagedDataResult(data): data is IDataResult<any[]> {
    return (data.HasPageInfo && Array.isArray(data.ResultSet));
  }

  export type IDataResult<resultsetType> = {
    ResultSet: resultsetType;
    NumResults: number;
    FirstRec: number;
    LastRec: number;
    PageNo: number;
    PageSize: number;
    LastPage: number;
    IsLastPage: boolean;
    HasPageInfo: boolean;
  }

  export interface INotatedArray<T> extends Array<T> {
    pageNo: number;
    pageSize: number;
    numResults: number;
    firstRec: number;
    lastRec: number;
    lastPage: number;
    isLastPage: boolean;
    hasPageInfo: boolean;
  }

  export function notateResponse<resultType>(rawData: IDataResult<resultType[]>): INotatedArray<resultType> {
    let data = rawData.ResultSet as INotatedArray<resultType>;
    data.numResults = rawData.NumResults;
    data.pageSize = rawData.PageSize;
    data.pageNo = rawData.PageNo;
    data.firstRec = rawData.FirstRec;
    data.lastRec = rawData.LastRec;
    data.lastPage = rawData.LastPage;
    data.isLastPage = rawData.IsLastPage;
    data.hasPageInfo = true;
    return data;
  }
}
