﻿/**
 * @ngdoc directive
 * @name regexValidator
 *
 * @description
 * Directive to add to an inout control, to validate based on a regular expression
 *
 */
namespace Sw.Api {
  function regexValidator(): angular.IDirective {
    return {
      require: 'ngModel',
      link: (scope: angular.IScope, element, attrs: angular.IAttributes, ngModel: angular.INgModelController) => {
        const regex = new RegExp(attrs['regexValidator']);
        ngModel.$validators['invalidRegex'] = (modelValue: any, viewValue: any) => {
          const value = modelValue || viewValue;
          return regex.test(value);
        };
      }
    };
  }

  angular
    .module("sw.common")
    .directive('regexValidator', regexValidator);
}