﻿namespace Sw.Utils {


	export class UiGridUtils {

		/**
		 * @static Sw.Utils.UiGrid.Utils.actionName
		 * @description Markup for a clickable columnn that invokes the named action in the controller
		 * Note this assumes controllerAs vm (now that we are moving to new RenderFrame instead
		 * of the old PagedList/PagedListEDitable which was assuming listvm)
		 * 
		 * @param actionName what does clicking on the cell will do (e.g. open "item")
		 * @param controllerAs how the controller is expected in the template, defaults to vm when omitted
		 * @param actionField optional string to name a column field (e.g. "schNo"), defaults to "col.field"
		 */
		public static actionColumn(actionName: string, controllerAs = "vm", actionField?) {
			actionField = actionField ? `'${actionField}'` : "col.field";
			return `<div class="ngCellText ui-grid-cell-contents gdAction" ng-class="col.colIndex()" ng-click="grid.appScope.${controllerAs}.action('${actionName}',${actionField}, row.entity);"><a>{{row.entity[col.field]}}</a></div>`;
		}

		public static nameColumn(given: string, family: string) {
			return `<div class="ngCellText ui-grid-cell-contents " ng-class="col.colIndex()" ><span style="font-weight:bold">{{row.entity['${family}'].toUpperCase()}}</span>, {{row.entity['${given}']}}</div>`;
		}

	}
}