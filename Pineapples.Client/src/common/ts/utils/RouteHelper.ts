﻿module Sw.Utils {
	export class RouteHelper {

		// DEPRECATION WARNING: superceded by frameListState?!
		public static listState(entity, allowedViews?, defaultView?) {
			var state = <ng.ui.IState>{
				// all these are the same becuase the variation is in the Filter object
				url: '/list',
				views: <{ [name: string]: ng.ui.IState }>{
					// navbar view removed - view Mode selector is not there anymore
					"@": {
						templateUrl: `${entity}/PagedList`,
						controller: 'PagedListController',
						controllerAs: "listvm"
					}
				},
				resolve: {
					pageTitle: this.simpleTitle("List")
				}
			}

			state.data = state.data || {};
			state.data["allowedViews"] = allowedViews;

			if (defaultView) {
				state.data["defaultView"] = defaultView;
			}

			return state;
		}

		/**
		 * @name Sw.Utils.RouteHelper.frameListState
		 * @description Utility function to do repetitive work of building list states for features.
		 * frameListState should be used as part of the 'frame'; replaces the earlier featurestate/listState
		 * with frameState/frameListState
		 * 
		 * @param {string} [monitorSelector] the relevant function for monitoring, or the item type as a string.
		 * @param {string} [monitorKey]  the key field of the data in this list.
		 * 
		 * @returns {ng.ui.IState} The list state for a given feature (School, Teacher, etc.)
		 *
		 * @example <h1>{{listvm.theFilter.entity|capitalize:first}}</h1>
		 */
		public static frameListState(monitorSelector?: string, monitorKey?: string) {
			let state = <ng.ui.IState>{

				url: '/list',
				data: {
					rendermode: "List"
				},
				views: <{ [name: string]: ng.ui.IState }>{
					"renderoptions": {
						templateUrl: "generic/PagedListParamsComponent",
						controller: "FilterController",
						controllerAs: "vm",
					},
					"renderarea": "pagedListManager"
				},
				resolve: {
					pageTitle: this.simpleTitle("List")
				}

			};
			if (monitorKey) {
				state.resolve.monitorSelector = () => monitorSelector;
				state.resolve.monitorKey = () => monitorKey;
			}
			return state;
		}

		/**
		 * @name Sw.Utils.RouteHelper.reloadState
		 * @description This is a short hand way for developers to test changes to cshtml files wihtout a 
		 * full reload it kills the template ache and reloads the current state.
		 */
		public static reloadState() {
			return <ng.ui.IState>{
				url: '-',
				onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
					$templateCache.removeAll();
					$state.reload();
				}]
			};
		}

		// DEPRECATION WARNING: superceded by frameListState?!
		public static editableListState(entity, allowedViews?, defaultView?) {
			var state = RouteHelper.listState(entity, allowedViews, defaultView);
			state.views["@"] = {
				templateUrl: entity + "/PagedListEditable",
				controller: 'PagedListEditableController',
				controllerAs: "listvm"
			}
			return state;
		}

		/**
		 * @name Sw.Utils.RouteHelper.chartState
		 * @description Utility function to do repetitive work of building chart states for various features.
		 * 
		 * @param {string} [featurename] the name of the feature (Teachers, Schools) to build chart state for.
		 * 
		 * @returns {ng.ui.IState} The chart state for a given feature (School, Teacher, etc.)
		 */
		public static chartState(featurename): ng.ui.IState {
			var state = {
				url: '/chart',
				data: {
					rendermode: "Chart"
				},
				views: <{ [name: string]: ng.ui.IState }>{
					// componentise: build a component from the template and controller
					"renderoptions": "chartParams",
					"renderarea": {
						templateUrl: "common/chart",
						controller: "ChartController",
						controllerAs: "vm"
					}
				},
				resolve: {
					pageTitle: this.simpleTitle("Chart"),
					// these resolves become bindings on component, not injections
					palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
					mapColorScale: function () { return new Sw.Maps.MapColorScale(); }
				}
			};
			return state;
		};

		/**
		 * @name Sw.Utils.RouteHelper.tableState
		 * @description Utility function to do repetitive work of building table states for various features.
		 *
		 * @param {string} [featurename] the name of the feature (Teachers, Schools) to build table state for.
		 *
		 * @returns {ng.ui.IState} The table state for a given feature (School, Teacher, etc.)
		 */
		public static tableState(featurename): ng.ui.IState {
			var state = {
				url: '/table',
				data: {
					rendermode: "Table"
				},
				views: <{ [name: string]: ng.ui.IState }>{
					"renderoptions": "tableParams",
					"renderarea": {
						templateUrl: "common/table",
						controller: "TableController",
						controllerAs: "vm"
					}

				},
				resolve: {
					pageTitle: this.simpleTitle("Table")
				}
			};
			return state;
		};

		/**
		 * @name Sw.Utils.RouteHelper.mapState
		 * @description Utility function to do repetitive work of building map states for various features.
		 *
		 * @param {string} [featurename] the name of the feature (Teachers, Schools) to build map state for.
		 * @param {string} [mapview] A string representation of the map view (e.g. for Schools feature SchoolMapView) to build map state for.
		 *
		 * @returns {ng.ui.IState} The map state for a given feature (School, Teacher, etc.)
		 */
		public static mapState(mapview?): ng.ui.IState {
			if (!mapview) {
				mapview = 'baseMapView';
			};
			var state = {
				url: '/map',
				data: {
					rendermode: "Map"
				},
				views: <{ [name: string]: ng.ui.IState }>{
					"renderoptions": "mapParams",
					"renderarea": {
						templateUrl: "common/map",
						controller: "MapController",
						controllerAs: "mapc"
					}
				},
				resolve: {
					pageTitle: this.simpleTitle("Map"),
					mapView: mapview
				}
			};
			return state;
		};

		// featurename matches the Web Api controller
		// filtername is the filtername js class
		// templatepath matches the mvc controller that dispenses the templates

		/**
		 * @name Sw.Utils.RouteHelper.tb
		 * @description TODO
		 * 
		 * @param {string} [tableOptions] a member of Lookups.cache, which is the dropdown on table and charts pages.
		 * 
		 * @returns TODO
		 */
		public static tb(tableOptions) {
			return () => tableOptions;
		}

		// DEPRECATION WARNING: superceded by frameState?!
		public static featureState(featurename, filtername?, templatepath?, url?, usersettings?, tableoptions?, mapview?): ng.ui.IState {
			if (!mapview) {
				mapview = 'baseMapView';
			};
			var defUserSettings = { defaults: {}, locked: {} };
			usersettings = usersettings || defUserSettings;
			filtername = filtername || featurename + 'Filter';
			templatepath = templatepath || featurename.toLowerCase();
			url = (url || featurename.toLowerCase());
			var state = {
				url: '/' + url,
				data: {
					roles: ['authenticated']
				},
				views: <{ [n: string]: ng.ui.IState }>{
					"headerbar@": {
						templateUrl: templatepath + "/searchercomponent", // mvc formulation allows server-side logic to choose searher version, function encapsulation allows future client override too
						controller: "FilterController",
						controllerAs: "vm",

					},
					"headerbardropdownIcon@": { template: '<i class="fa fa-filter" uib-tooltip="Show Filter" tooltip-placement="right"></i>' },
					"flashFind@": {
						templateUrl: "common/FlashFindSearch",
						controller: "FilterController",
						controllerAs: "vm"
					},
					"flashFindCollapsed@": { template: '<i class="fa fa-bolt opacity-control"></i>' }
				},
				resolve: {
					theFilter: filtername,
					findConfig: [
						filtername, function (fltr) {
							if (fltr.createFindConfig) {
								return fltr.createFindConfig(usersettings);
							}
							return null;
						}
					],
					pageTitle: this.featureTitle(featurename),

					palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
					mapColorScale: function () { return new Sw.Maps.MapColorScale(); },
					mapView: mapview
				}
			};

			if (tableoptions) {
				state.resolve["tableOptions"] = this.tb(tableoptions);
			}

			// TODO - Any way to not repeat same views below?!
			// see when moving to components
			// see https://github.com/angular-ui/ui-router/wiki/Multiple-Named-Views
			// Views specific to .list states
			let viewFlashFindSearchList = `flashfindsearch@site.${featurename.toLowerCase()}.list`;
			let viewSearcherList = `searcher@site.${featurename.toLowerCase()}.list`;
			let viewPagedListParamsList = `pagedlistparams@site.${featurename.toLowerCase()}.list`;

			state.views[viewFlashFindSearchList] = {
				templateUrl: "generic/FlashFindSearchComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewSearcherList] = {
				templateUrl: `${templatepath}/SearcherComponent`,
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewPagedListParamsList] = {
				templateUrl: "generic/PagedListParamsComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			// Views specific to .chart states
			let viewFlashFindSearchChart = "flashfindsearch@site." + featurename.toLowerCase() + ".chart";
			let viewSearcherChart = "searcher@site." + featurename.toLowerCase() + ".chart";
			let viewManipulateChart = "manipulatechart@site." + featurename.toLowerCase() + ".chart";

			state.views[viewFlashFindSearchChart] = {
				templateUrl: "generic/FlashFindSearchComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewSearcherChart] = {
				templateUrl: templatepath + "/SearcherComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewManipulateChart] = {
				templateUrl: "generic/ChartManipulationComponent",
				controller: "ChartParamsController",
				controllerAs: "vm",
			};

			// Views specific to .table states
			let viewFlashFindSearchTable = "flashfindsearch@site." + featurename.toLowerCase() + ".table";
			let viewSearcherTable = "searcher@site." + featurename.toLowerCase() + ".table";
			let viewManipulateTable = "manipulatetable@site." + featurename.toLowerCase() + ".table";

			state.views[viewFlashFindSearchTable] = {
				templateUrl: "generic/FlashFindSearchComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewSearcherTable] = {
				templateUrl: templatepath + "/SearcherComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewManipulateTable] = {
				templateUrl: "generic/ChartManipulationComponent",
				controller: "TableParamsController",
				controllerAs: "vm",
			};

			// Views specific to .map states
			let viewFlashFindSearchMap = "flashfindsearch@site." + featurename.toLowerCase() + ".map";
			let viewSearcherMap = "searcher@site." + featurename.toLowerCase() + ".map";
			let viewManipulateMap = "manipulatemap@site." + featurename.toLowerCase() + ".map";

			state.views[viewFlashFindSearchMap] = {
				templateUrl: "generic/FlashFindSearchComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewSearcherMap] = {
				templateUrl: templatepath + "/SearcherComponent",
				controller: "FilterController",
				controllerAs: "vm",
			};
			state.views[viewManipulateMap] = {
				templateUrl: "generic/MapParamsComponent",
				controller: "MapParamsController",
				controllerAs: "mappc",
			};

			return state;
		};

		// DEPRECATION WARNING: superceded by direct invokation to frameState?!
		public static addFeatureState($stateProvider: ng.ui.IStateProvider, featurename, filtername, templatepath, url, usersettings, tableoptions, mapview) {
			var state = this.featureState(featurename, filtername, templatepath, url, usersettings, tableoptions, mapview);
			var statename = 'site.' + featurename.toLowerCase();
			$stateProvider
				.state(statename, state);
			return this;
		}

		// DEPRECATION WARNING: superceded by direct invokation of frameListState?!
		public static addListState($stateProvider: ng.ui.IStateProvider, featurename, allowedViews?, defaultView?) {
			var state = this.listState(allowedViews, defaultView);
			var statename = 'site.' + featurename.toLowerCase() + '.list';
			$stateProvider
				.state(statename, state);
			return this;
		}

		// DEPRECATION WARNING: superceded by direct invokation of frameListState?!
		public static addEditableListState($stateProvider: ng.ui.IStateProvider, featurename, allowedViews, defaultView) {
			var state = this.editableListState(allowedViews, defaultView);
			var statename = 'site.' + featurename.toLowerCase() + '.list';
			$stateProvider
				.state(statename, state);
			return this;
		}


		// TODO remove below in favour of direct invokation to chartState, tableState, etc.
		public static addChartState($stateProvider: ng.ui.IStateProvider, featurename) {
			var state = this.chartState(featurename);
			var statename = 'site.' + featurename.toLowerCase() + '.chart';
			$stateProvider
				.state(statename, state);
			return this;
		}

		public static addTableState($stateProvider: ng.ui.IStateProvider, featurename) {
			var state = this.tableState(featurename);
			var statename = 'site.' + featurename.toLowerCase() + '.table';
			$stateProvider
				.state(statename, state);
			return this;
		}

		public static addMapState($stateProvider: ng.ui.IStateProvider, featurename, mapview?: any) {
			var state = this.mapState(mapview);
			var statename = 'site.' + featurename.toLowerCase() + '.map';
			$stateProvider
				.state(statename, state);
			return this;
		}

		/**
		 * @name Sw.Utils.RouteHelper.frameState
		 * @description Utility function to do repetitive work of building root states for features.
		 * frameListState should be used as part of the 'frame'; replaces the earlier featurestate/listState
		 * with frameState/frameListState. Follows component-centric routes.
		 * 
		 * Notes on superseding featureState/listState with frameState/frameListState: 
		 * doing this a different way - as a model going forward
		 * the old fashioned way to defined a view on a route is to set
		 * the template, or templateUrl, the controller, and controllerAs relating them
		 *   templateUrl: "common/PagedListParams",
		 *   controller: "FilterController",
		 *   controllerAs: "vm"
		 * Here we combine the Controller, Template and controllerAs by defineing a component.
		 * To reuse the logic in a Controller, we inherit the component's controller from it
		 *
		 *
		 * @param {string} [featurename] The name of feature to generate root state (e.g. Schools, Teachers).
		 * @param {string} [filtername] The name of the filter to use for this feature (e.g. SchoolFilter). 
		 * See Sw.Filter.Filter and Sw.Filter.IFilter
		 * @param {string} [templatepath] The string used to find the right template to use (or if it is a generic template to
		 * load the correct view)
		 * @param {string} [url] The URL of the root state without starting ^/
		 * @param {string} [usersettings]
		 * @param {string} [tableoptions] The name of a database view (e.g. schoolFieldOptions will call [common].[schoolFieldOptions])
		 * @param {string} [mapview] The name of an implementation of a sw.common.maps.mapViewHelper (e.g. SchoolMapView)
		 *
		 * @returns {ng.ui.IState} The list state for a given feature (School, Teacher, etc.)
		 */
		public static frameState(featurename, filtername?, templatepath?, url?, usersettings?, tableoptions?, mapview?): ng.ui.IState {

			if (!mapview) {
				mapview = 'baseMapView';
			};
			var defUserSettings = { defaults: {}, locked: {} };
			usersettings = usersettings || defUserSettings;
			filtername = filtername || featurename + 'Filter';
			templatepath = templatepath || featurename.toLowerCase();
			url = (url || featurename.toLowerCase());

			let state: ng.ui.IState = {
				url: `^/${url}`,
				data: {
					roles: ['authenticated'],
				},
				views: <{ [n: string]: ng.ui.IState }>{
					"@": `${templatepath}RenderFrame`
				},
				resolve: {
					theFilter: filtername,
					findConfig: [
						filtername, function (fltr) {
							if (fltr.createFindConfig) {
								return fltr.createFindConfig(usersettings);
							}
							return null;
						}
					],
					pageTitle: this.featureTitle(featurename),
					featureTitle: this.featureTitle(featurename),
					palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
					mapColorScale: function () { return new Sw.Maps.MapColorScale(); },
				}
			};
			// use templatepath to construct the name of the searcher - because it is singular
			state.views[`searcher@site.${featurename.toLowerCase()}`] = `${templatepath}SearcherComponent`;
			return state;
		}

		public static clone(stateProvider: ng.ui.IStateProvider
			, sourceState: string
			, overrides: any): ng.ui.IState {
			let s = angular.merge({}
				, (<any>stateProvider).stateRegistry.states[sourceState].self
				, overrides);
			return s;
		}

		/**
		 * @name Sw.Utils.RouteHelper.featureTitle
		 * @description Contruct a resolve value for page title for the base route of a feature
		 * ie this is a route with name site.<featureName>
		 * This is set up to create a page Title like
		 * FedEMIS * Schools
		 * 
		 * @param featureName
		 */
		public static featureTitle(featureName) {
			return ["rootPageTitle", (title) => `${title} * ${featureName}`]
		}

		/**
		 * @name Sw.Utils.RouteHelper.simpleTitle
		 * @description construct a resolve value for page title for a page within a feature
		 * e.g. site.schools.list will return 
		 * FedEMIS * Schools: List
		 * 
		 * @param pageName
		 */
		public static simpleTitle(pageName) {
			return ["featureTitle", (title) => `${title}: ${pageName}`]
		}
		/**
		 * @name Sw.Utils.RouteHelper.indirectTitle
		 * @description construct a resolve value for page title for a page within a feature
		 * where the name will vary according to a data parameter; esp invoked from an item in a List
		 * 
		 * @param titleResolve
		 * The name of  resolve value in the state that holds the title text. Defaults to titleId. 
		 * This allows more detailed calculation of the text, specifically for 'item' routes
		 * where we want a name in the page title related to the item displayed
		 */
		public static indirectTitle(titleResolve="titleId") {
			return ["featureTitle", titleResolve, (title, pageName) => `${title}: ${pageName}`]
		}

		/**
		 * @name Sw.Utils.RouteHelper.compoundTitle
		 * @description construct a resolve value for page title for a child page 
		 * that is not a direct child of the feature
		 * e.g. site.schools.list.foo will return 
		 * FedEMIS * Schools: List Foo
		 * 
		 * @param pageName
		 */
		public static compoundTitle(pageName) {
			return ["pageTitle", (title) => `${title} ${pageName}`]
		}

		/**
		 * Assign this to a resolve in a route to create a representation of the current state,
		 * that can be used to navigate back to that state
		 * see discussion at https://stackoverflow.com/questions/16635381/angular-ui-router-get-previous-state
		 * Note that panel component now goes BACK (ie history.back) not UP (is to parent state) on panelClose = true
		 * issue 1155
		 * So this logic is of less potential use. Note though that in a panel you can pass this object as panelClose binding
		 * to force a return to a given state.
		 */
		public static backState = ["$state", function ($state: ng.ui.IStateService) {
			return {
				name: $state.current.name,
				params: angular.copy($state.params),
				url: $state.href($state.current.name, $state.params)
			};
		}];
	}

}