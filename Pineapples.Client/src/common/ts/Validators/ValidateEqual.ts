﻿namespace Sw.Validators {

  interface IIsolateScope extends ng.IScope {
   vm: controller
  }
  class controller {

    public otherModelValue: any;
    public comparator = (modelValue) => {
      return modelValue === this.otherModelValue;
    };
  }
 /*
  * Validator directive to test if the control is equal to some other control
  * used for confirmation password
  */
  class validateEqual implements ng.IDirective {
    public restrict = "A";
    public require = "ngModel";
    public scope = {};

    public controller = controller;
    public controllerAs = "vm"
    public bindToController = {
      otherModelValue: "=validateEqual"
    };

    public link: any;

    constructor() {
      
      validateEqual.prototype.link = (scope: IIsolateScope, element, attrs, ngModelCtrlr) => {
        
        ngModelCtrlr.$validators.compare = scope.vm.comparator;
 
            scope.$watch("vm.otherModelValue", () => {
                ngModelCtrlr.$validate();
            });
      };
    }

    public static factory() {
      let directive = () => {
        return new validateEqual();
      };
      return directive;
    }

  }
  angular
    .module('sw.common')
    .directive('validateEqual', validateEqual.factory());

}
