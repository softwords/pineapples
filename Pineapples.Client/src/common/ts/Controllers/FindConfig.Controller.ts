﻿namespace Sw.Controllers {

	interface IxScope extends ng.IScope {
		vm: FindConfigController;
	}

	/**
	 * @class Sw.Controllers.FindConfigController
	 * @description simple controller to provide access to a Filter object
	 * used by headerbar items like searcher, chart params, table params
	 * adds special support for chart and tables - row, col, chartType and swap
	 * tableoptions is a string naming the member of lookups.cache to use for table and chart dropdowns
	 * 
	 * It is used as the primary controller for most (all) the Feature searcher components
	 * (e.g. Sw.Components.FindConfigComponent("school/SearcherComponent"))
	 * 
	 * @see Sw.Components.FindConfigComponent
	 */
	class FindConfigController {

		public rowData: Sw.Lookups.ILookupEntry;
		public params: any;
		static $inject = ["$scope", 'theFilter', 'Lookups', 'findConfig', "$state"];
		constructor(scope: IxScope, public theFilter: Sw.Filter.IFilter, public lookups: Sw.Lookups.LookupService
			, public findConfig: Sw.Filter.FindConfig, public state: ng.ui.IStateService) {

			scope.vm = this;
			this.params = findConfig.current.filter;

		};

		public FindNow() {
			this.theFilter.FindNow(this.findConfig.current);
			this.findConfig.prepared = true;
		}
		public FlashFind() {
			this.theFilter.FlashFind(this.findConfig.current);
			this.findConfig.prepared = true;
		}
		public Reset() {
			this.findConfig.reset();
		}
		public get viewMode() {
			return this.findConfig.current.viewMode;
		}

		public set viewMode(newViewMode) {
			if (this.findConfig.current.viewMode !== newViewMode) {
				this.findConfig.current.viewMode = newViewMode;
				let vm = this.theFilter.GetViewMode(newViewMode);
				if (vm) {
					this.FindNow();
				}
			}
		}

		public supportedViewModes() {
			if (this.findConfig.allowedViews && this.findConfig.allowedViews.length > 0) {
				return this.findConfig.allowedViews;
			}
			return this.theFilter.AllViewModes();
		}
	};

	// can dispense with this 'local' search variant?
	class FindConfigLocalController extends FindConfigController {
		public localparams: any;

		static $inject = ["$scope", 'theFilter', 'Lookups', 'findConfig', "$state"];
		constructor(scope: IxScope, public theFilter: Sw.Filter.IFilterCached, public lookups: Sw.Lookups.LookupService
			, public findConfig: Sw.Filter.FindConfig, public state: ng.ui.IStateService) {
			super(scope, theFilter, lookups, findConfig, state);
		}
		public localFind() {
			this.theFilter.findNowLocal(this.localparams);
		}
		public localReset() {
			this.localparams = {};
		}
	}

	angular
		.module('sw.common')
		.controller('FilterController', FindConfigController)
		.controller('FindConfigLocalController', FindConfigLocalController)

}

