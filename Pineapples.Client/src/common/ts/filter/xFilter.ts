﻿/* support class for CrossFilter
 *provides some standard methods for creating reductions, etc
*/


namespace Sw.xFilter {
  // token to use instead of null in dimensions
  var nullToken = '<>';
  export type keyAccessor = ((d: any) => string)|string;
  export type valueAccessor = ((d: any) => any)|string;
  export type grpValueAccessor = (g: any) => number;

/**
 * Interface for XFilter object - supplies utilities for generating crossfilter reducations in various ways
 */
  export interface IXFilter {
    crossfilter(data: any[]): CrossFilter.CrossFilter<any>;

    //--------------------------------------------------------------------------------------------------------
    // xReduce
    //--------------------------------------------------------------------------------------------------------
    // Create a crossfilter group that is a 'crosstab' or 'pivot' 
    // dimension is the underlying dimension to use - this will determine the 'row headings' in the crosstab
    // keyAccessor defines the 'column headings' in the crosstab - each distinct value that may be returned by this function
    // becomes a property of the crosstab accumulation. 
    // valueAccessor dermines the value to accumulate. This may be a scalar, but can be an object, in which case it is accumuluted
    // property-by-property
    // e.g, suppose we have data like [{Age: 10, Class: Grade1 , M: 5, F: 4},{Age: 10, Class: Grade2 , M: 2, F: 1} {Age: 11, Class: Grade2 , M: 3, F: 6}]
    // and we have a dimension 'dimAge', created by extracting the property 'Age'
    // the out-of-the-box crossfilter functionality gives:
    // dimAge.group()     -- cretes a record per age, Ccounting the number of records per age
    // More useful is:
    // dimAge.group().reduceSum(d => d.M + d.F); which is the total number of pupils per age
    // Using xReduce we go further:
    // define valueAccessor as above : d => d.M + d.F
    // and add keyAccess to make the Class the column headings: d => d.Class
    // then xReduce(dimAge, keyAccessor, valueAccessor) accumulates total like this:
    // age 10:[Grade1: 9, Grade2: 3, Tot: 12] Age 11:[Grade2: 9, Tot: 9]
    // in angularjs you'll use nested ng-repeats to extract these values into a crosstab
    //
    // In the above, the total that is accrued is a scalar value.
    // if valueAccess returns an object, we get the accumulation of that object by property:
    //e.g. supppose valueAccessor is d => { M:d.M, F:d.F, Tot: d.M + d.F}
    // then xReduce will prodcue this:
    // age 10:[Grade1: {M:4, F: 5, Tot: 9} , Grade2:{ M:2, F:1, Tot:3}, Tot:{M:6, F:6, Tot:12}] etc
    // This can be rendered in a table that shows M, F and Tot values within each class
    // 
    /*
            |-----------------------------------------------------|
            |        |    Grade1    |    Grade2    | Total        |
            |-----------------------------------------------------|
            | Age    |  M | F | Tot |  M | F | Tot |  M | F | Tot |
            |-----------------------------------------------------|
            |   10   |  4 | 5 |   9 |   2| 1 | 3   |  6 | 6 |  12 |
    */

		/**
		 * Generate a 'crosstab' in a crossfilter group. The value of the group 
		 * will be a complex object with properties determined by the keyAccessor.
		 * @param dimension - dimension to build the group on
		 * @param keyAccessor  - function to determine the set of crosstabulations under value. 
		 * If a string, is a property name in the source data.
		 * @param valueAccessor - function to determine the structure of value under each key
		 */
    xReduce(dimension: CrossFilter.Dimension<any, any>,
      keyAccessor: keyAccessor, valueAccessor: valueAccessor): CrossFilter.Group<any, any, any>;

    // xReduceGrouped
    //-------------------
    // Provides the same functionality as xReduce, but also allows grouping on the dimension variable
    // for example, grouper could be set to 
    // d => { if (d.Age > OfficialAge(d.Class)) return 'Over'; return 'OK'}
    // so that the rows of the crosstabulation would now be 'Over' or 'OK'
    //
    xReduceGrouped(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor, grouper): CrossFilter.Group<any, any, any>;

    // xReduceTotal
    //--------------
    // same as xReduce, but collapses all rows to a single row "Total"
    // Note that each dimension may have different filters applied in a crossfilter, so the total produced 
    // is with respect to any cross-cutting filters
    //
    xReduceTotal(dimension: CrossFilter.Dimension<any, any>,
      keyAccessor: keyAccessor, valueAccessor: valueAccessor): CrossFilter.Group<any, any, any>;

    // Helper functions for generating keyAccessor and valueAccessor functions
    //------------------------------------------------------------------------
    // return a function that returns the value of a property
    // this value becomes a property in the reduction, this 'denormalises' or 'crosstabs' the field value
    // useful for tabulations
    getPropAccessor(propname: string): (d: any) => string;
    //
    // builds a reduction based on a property that has 1, 0 representing Yes or No
    // will create the property Y or N in the reduction
    //(the value assigned to this property
    getPropYnAccessor(propname: string): keyAccessor;
    // return a function that returns a constant ( usually 1)
    // can be used to supply the value when generating a reduction; ie when const is 1, this is a count of records.
    getConstAccessor(c: any): valueAccessor;

    // these are used for retrieving values from the constructed group
    // these operate on an array of key / value pairs
    // and are used in presentations - ie for charts or in tables ( ng-repeat="g in grp.all()" )

    // in the group reduction, return the value of a property in the group value object
    // this is for reporting from the group
    getGrpValueAccessor(propname: string): grpValueAccessor;
    // totAccessor is a special case where propname = 'Tot'
    totAccessor(): grpValueAccessor;
    // return the ratio of two values in the group value.
    // useful for returning a percentage
    getGrpRatioAccessor(numerator: string, divisor: string): grpValueAccessor;

    getFlattenedGenderAccessor(genderColumnName: string, valueColumnName: string | string[]);


    getGrpValuePropsArray(): (any) => string[];
    // Dimension type safety
    // crossfilter is unhappy with nulls in dimensions,
    // but doesn;t do any checking for perfomance reasons.



    

    getDimAccessor(propname: string): (d: any) => any;
    nullToken: string;


    toggle(grp: any, newValue): void;

    //simpleFilter enforces only one selection at a time
		simpleFilter(dimension: CrossFilter.Dimension<any, any>, filters: string[]);

		// simple utility to find a key in a group, then return the matching value
		find(grp, accessor): any;

		/**
		 * Retun a key value collection which is the running total, in index sequence,
		 * of the values in group.all()
		 * @param grp the crossfilter group 
		 */
    runningTotal(grp: CrossFilter.Group<any, any, any>): Sw.Charts.KeyValueCollection;

    
  }


/**
 * 
 */
  export class XFilter implements IXFilter {

    /**
     * create a crossfilter object from a table
     *  assume that crossfilter is loaded, and therefore accessible via window.crossfilter
     * @param data - collection containing the table to crossfilter
     */
    public crossfilter(data: any[]) {
      return (<any>window).crossfilter(data);
    }

   /**
    * Function used to generate the function used as the Initial in a reduction
    * The generated function will create the empty accumulator, and store the
    * keyAccessor and valueAccessor in there so they are available to the Add and Remove functions
    * the Tot property will hold the accumulated value - which may be a scalar value, or an object
    */
    private xgetInitial = (keyAccessor, valueAccessor) => {

      var i = function () {
        var pc: any = {};
        // add the accessors as properties
        pc.keyAccessor = keyAccessor;
				pc.valueAccessor = valueAccessor;
				// we use count so we know if this is an empty group
				// see https://stackoverflow.com/questions/21352289/how-to-filter-empty-groups-from-a-reduction
				pc.Count = 0;
        pc.Tot = null;
        return pc;

      }

      return i;

    };

    // reduceAdd
   /**
    * the function that willl used used as the Add in the reduction
    * p - is accumulated totals
    * d is the current data point
    */
    private xreduceAdd = (p, d) => {
			var pc = _.clone(p);
      // get the key for the current item
      var k = (pc.keyAccessor(d) || 'na');
			var v = pc.valueAccessor(d);
			pc.Count++;
      pc.Tot = this.incrTotal(pc.Tot, v);
			pc[k] = this.incrTotal(pc[k], v);
      return pc;

    };

    private incrTotal(accum, value) {
      // 2 distinct cases - either a single value, or an object of values
      if (accum == null) {
				if (typeof (value) == "object") {
					try {
						return angular.copy(value);
					}
					catch {
						console.error("xFilter error", value);
					}
        } else {
          return value;
        }
      }
      if (typeof (value) == "object") {
				Object.keys(value).forEach((prop) => {
					// don't try to accumulate functions, or 'private' properties that begin with _
					// this allows computations to be defined in the accumulator - useful for ratios
					if (value[prop] && typeof(value[prop]) != "function" && prop.charAt(0) != "_") {
						accum[prop] = this.incrTotal(accum[prop], value[prop]);
					}
        });
      } else {
        accum += value;
      }
      return accum;
    }

    // reduceRemove
    private xreduceRemove = (p, d) => {
      var pc = _.clone(p);

      // get the key for the current item
      var k = (pc.keyAccessor(d) || 'na');
      var v = pc.valueAccessor(d);

			pc.Count--;
      pc.Tot = this.decrTotal(pc.Tot, v);
      pc[k] = this.decrTotal(pc[k], v);
      return pc;
    };

    private decrTotal(accum, value) {
 
      // 2 distinct cases - either a single value, or an object of values
      if (typeof (value) == "object") {
        if (accum == null) {
          accum = {}
          Object.keys(value).forEach((prop) => {
            accum[prop] = value[prop] * -1;
          });
        } else {
					Object.keys(value).forEach((prop) => {
						if (value[prop] && typeof (value[prop]) != "function" && prop.charAt(0) != "_") {
							accum[prop] = this.decrTotal(accum[prop], value[prop]);
						}
          });
        }
      } else {
        if (accum == null) {
          accum = value * -1;
        } else {
          accum -= value;
        }
      }
      return accum;
    }


    public xReduce(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor) {

      // based  on the keyAccessor and valueAccessor generate the Initial Add and Remove functions
			// if the keyAccessor is a string, treat it as a property name
			if (typeof keyAccessor === 'string') {
				keyAccessor = this.getPropAccessorStr(keyAccessor, "-");
      }
      if (typeof valueAccessor === 'string') {
        valueAccessor = this.getPropAccessor(valueAccessor, "-");
      }
      var xreduceInitial = this.xgetInitial(keyAccessor, valueAccessor);
      return dimension.group().reduce(this.xreduceAdd, this.xreduceRemove, xreduceInitial);
    };


    public xReduceGrouped(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor, grouper) {

      // based  on the keyAccessor and valueAccessor generate the Initial Add and Remove functions
			// if the keyAccessor is a string, treat it as a property name
			if (typeof keyAccessor === 'string') {
				keyAccessor = this.getPropAccessorStr(keyAccessor, "-");
			}
      var xreduceInitial = this.xgetInitial(keyAccessor, valueAccessor);
      return dimension.group(grouper).reduce(this.xreduceAdd, this.xreduceRemove, xreduceInitial);
    };

    public xReduceTotal(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor) {

      // based  on the keyAccessor and valueAccessor generate the Initial Add and Remove functions
      return this.xReduceGrouped(dimension, keyAccessor, valueAccessor, (d) => "Total");
    };

    /**
     * Simple helper to return a function that extracts a value by property name
     * @param propname
     */
    public getPropAccessor(propname: string, replace: any = null) {
      return (d) => d[propname]|| replace;
    }

		/**
		* Simple helper to return a function that extracts a value by property name, coerced to a string
	  * Better when crosstabbing ( ie keyAccessor) on a numberic value
		* @param propname
		*/
		public getPropAccessorStr(propname: string, replace: any = null) {
			return (d) => (d[propname] || replace).toString();
		}

    /**
     * Simple helper to translate a 1, 0 field into a Y, N accumulation
     * @param propname
     */
    public getPropYnAccessor(propname: string) {
      return (d) => {
        switch (d[propname]) {
          case 1:
            return 'Y';
          case 0:
            return 'N';
          default:
            return '?';
        }
      }
    };

    /**
     * When used as a value accessor, will return a constant. ie when c = 1 this is a count of records
     * @param c
     */
    public getConstAccessor(c: any) {
      return (d) => c;
    }

		
    /**
     * Constructs a Value accessor function for building a cross tabulation where each 'value' 
     * is an object containing M F and T values Use this as the value accessor in xReduce. When valueColumnName is a string output looks like
     * { M: 12, F: 18, T: 30 }
     * If valueColumnName is an array e.g. ["Height","Weight"]
     * the value looks like
     * { M: {Height: 3, Weight: 2}, F: {Height: 4, Weight: 12}, T: {Height: 7, Weight: 14} }
     * @param genderColumnName name of the colummn that contains the Gender value
     * @param valueColumnName name of column to use as the value, or else string[] of column names
     */
		public getFlattenedGenderAccessor(genderColumnName, valueColumnName: string | string[]) {
      if (Array.isArray(valueColumnName)) {
        return function (d) {
          let v = valueColumnName.reduce((prev, cv) => {
            prev[cv] = d[cv];
            return prev;
          }, {});
          return {
            M: d[genderColumnName] == "M" ? v : null,
            F: d[genderColumnName] == "F" ? v : null,
            T: angular.copy(v)
          };
        };

      }
      return function (d) {
				return {
					M: d[genderColumnName] == "M" ? d[valueColumnName] : null,
					F: d[genderColumnName] == "F" ? d[valueColumnName] : null,
					T: d[valueColumnName]
				};
			};
		};
    /**
     * In a crosstab grouping created by xReduce, extract the 'column' value for propname
     * e.g. supppose the keyAccessor is d => d.Class
     * then getGrpValueAccessor('Grade1') return the accumulation in this row for Class=Grade1
     * @param propname - value of the column property to get the accumulation for
     */
    public getGrpValueAccessor(propname: string) {
      return (g) => g.value[propname] || null;
    }

    /**
     * shortcut to extract the 'column total' from a crosstab made with xReduce
     */
    public totAccessor() {
      return this.getGrpValueAccessor("Tot");
    }

    /**
     * Returns a function that will get all the properties of the value of a group key-value
     * as an array. This can be useful to collect columns for a chart.
     */
    public getGrpValuePropsArray() {
      return (g) => {
        let result = [];
        Object.keys(g.value).forEach(val => {
          switch (val) {
            case "valueAccessor":
            case "keyAccessor":
            case "Count":
            case "Tot":
              break;
            default:
              result.push(val);
          }
        });
        return result;
			}
		}


    /**
     * extract a ratio from an accumulation
     * suppose keyAccessor is d 
     * @param numerator - property name of the numerator
     * @param divisor - property name of the denominator
     */
    public getGrpRatioAccessor(numerator: string, divisor: string) {
      return (g) => {
        var d = g.value[divisor];
        var n = g.value[numerator];
        if (d)
          return n / d;
        return null;
      };
    }

    public getDimAccessor(propname: string) {
      return (d) => d[propname] || nullToken;
    };
    public nullToken = nullToken;


    // utility functions

    // splitter is a helper function to simplify creating of selective groups; e.g. for Gender;
    // by totalling only those items where the specified property matches the specified value
    public splitter(field: string, value: any) {
      return function (d) {
        return (d[field] === value ? d.Num : 0);
      };
    };

    // toggle a filter on or off
    public toggle(cht, newValue) {
      var current = cht.filter();
      if (newValue === current) {
        return;     // otherwise we end up toggling it off
      }
      if (newValue) {
        cht.filter(newValue);
      } else {
        cht.filter(current);           // this will toggle the current off when newValue is empty
      }
      // setting the filter doesn;t redraw in dc
      ////////////dc.redrawAll(cht.chartGroup());
    }
    // simpleFilter is a filterHandler to apply to a dimension
    // it prevents multiple selections
    public simpleFilter(dimension, filters) {
      dimension.filter(null);

      if (filters.length === 0) {
        dimension.filter(null);
      } else {
        var f = filters[filters.length - 1];
        dimension.filter(f);
        return [f];
      };
    };


		public find(group: CrossFilter.Group<any, any, any>, predicate) {
			if (typeof predicate == "string") {
				predicate = { key: predicate };
			}
			let g = _.find(group.all(), predicate);
			return g ? g.value : null;
		}

		public runningTotal(group: CrossFilter.Group<any,any,any>	) {
			let g: Sw.Charts.KeyValueCollection = [];
			let accum = null;
			group.all().forEach((kv, index) => {
				accum = this.incrTotal(accum, kv.value);
				g.push({ key: kv.key, value: angular.copy(accum) });
			});
			return g;
		}
  };

	angular
    .module("sw.common")
    .service("xFilter", XFilter);
}
