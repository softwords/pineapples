﻿module Sw.Filter {

	/**
	 * @name Sw.Filter.UtilityFilters
	 * @description class that holds mostly static public methods all with simple
	 * utility behaviors for formatting output in the UI
	 */
	class UtilityFilters {

		/**
		 * @name Sw.Filter.UtilityFilters.capitalize
		 * @description Capitalizes the first letter of a string (default behavior)
		 * or all the letter of a string.
		 *
		 * @returns {function} Refer to anonmous function below
		 *
		 * @example <h1>{{listvm.theFilter.entity|capitalize:first}}</h1>
		 */
		public static capitalize() {

			/**
			 * @description anonymous filter function doing the actual capitalize work
			 *
			 * @param {string} input The string to be formatted.
			 * @param {string} [format] The format to be applied being the options 'first' (default) or 'all'.
			 * @param {string} [separator] The character(s) to be used for separating the string (e.g. go from
			 *                             "school-entity" to "School entity"). Space is the default.
			 *
			 * @returns {string} the capitalized string
			 */
			let fcn = (input: string, format: string, separator: string) => {
				if (!input) {
					return input;
				}

				format = format || 'first';
				separator = separator || ' ';

				if (format === 'first') {
					let words = input.split(separator);
					let output = [];
					words.forEach(w => {
						output.push(w.charAt(0).toUpperCase() + w.slice(1).toLowerCase());
					});
					return output.join(' ');
				}
			}

			return fcn;

		}

		/**
			 * @description filter function to show a boolean as a tick or cross
			 *
			 * @param {string} input The value to be formatted.
			 *
			 * @returns {string} the  string
		*/
		public static tick() {
			let fcn = (input: string, format: string) => {
				//https://en.wikipedia.org/wiki/Check_mark
				let sub = UtilityFilters.truefalse();
				return sub(input, `\u2713`, "", "");
			}

			return fcn;

		}
		public static tickcross() {
			let fcn = (input: string, format: string) => {
				let sub = UtilityFilters.truefalse();
				return sub(input, `\u2713`, `\u2717`,"");
			}
			return fcn;
		}

		public static fromYesno() {
			let fcn = (input: string) => {
				if (input === "" || input === null || input == undefined) {
					return null;
				};
				switch (input.toLowerCase()) {
					case "y":
					case "yes":
						return 1;
					case "n":
					case "no":
						return 0;
					default:
						return null;
				}
			}
			return fcn;
		}

		public static truefalse() {
			let fcn = (input: string, valueiftrue: string, valueiffalse: string = "", valueifnull: string = "") => {
				if (input === "" || input === null || input == undefined) {
					return valueifnull;
				};
				input = input.toString();
				if (input == "false" || input === "0" || input.toLowerCase() === "no" || input.toLowerCase() === "n") {
					return valueiffalse;
				}
				if (input) {
					return valueiftrue;
				}
				return valueiffalse;
			}

			return fcn;
		}

		/**
		 * hideZero filter suppresses display of 0 values
		 * May be simpler than using a lot of ng-show
		 */
		public static hideZero() {
			let fcn = (input: string, valueifZero: string = "") => {
				if (input == "false") {
					return valueifZero;
				}
				let v = Number(input);
				if (v === NaN) {
					return input;
				}
				if (v === 0) {
					return valueifZero;
				}
				return input;
			}

			return fcn;

		}

		/**
		 * @name Sw.Filter.UtilityFilters.append
		 * @description decorate a string with prepended or postpended strings if it is not empty
		 * or all the letter of a string.
		 *
		 * @returns {function} Refer to anonmous function below
		 *
		 * @example <h1>{{listvm.theFilter.entity|capitalize:first}}</h1>
		 */
		public static append() {
			let fcn = (input: string, before: string = "", after: string = "") => {
				if (input === ""||input === null||input === undefined) {
					return "";
				}
				return before + input + after;
			}
			return fcn;
		}

		public static percent($filter) {
		
				return (input, decimals) => 
					$filter('number')(input * 100, decimals) + '%';
		}
	}

	angular
		.module("sw.common")
		.filter("capitalize", UtilityFilters.capitalize)
		.filter("tick", UtilityFilters.tick)
		.filter("truefalse", UtilityFilters.truefalse)
		.filter("tickcross", UtilityFilters.tickcross)
		.filter("fromYesno", UtilityFilters.fromYesno)
		.filter("hideZero", UtilityFilters.hideZero)
		.filter("append", UtilityFilters.append)
		.filter("percent", ["$filter", UtilityFilters.percent]);
}
