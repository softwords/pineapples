﻿namespace Pineapples.SpecialEd {

	export interface IEvaluationEventDialogLocals {
		model: SpecialEdEvaluationEvent;
		getNew: () => ng.IPromise<SpecialEdEvaluationEvent>;
	}

	class DialogController extends Sw.Component.ComponentEditController {

		public model;
		public getNew: () => ng.IPromise<SpecialEdEvaluationEvent>;

		static $inject = ["$mdDialog", "ApiUi"];
		constructor(public mdDialog: ng.material.IDialogService, apiUi: any) {
			super(apiUi);
			if (!this.model._id()) {
				this.isNew = true;
				this.isEditing = true;
			}
		}

		public closeDialog() {
			this.mdDialog.cancel();
		}

		public onModelUpdated(newData) {
			if (newData.plain) {
				// its a restangular object
				//        angular.extend(this.source, newData.plain());
				angular.extend(this.model, newData.plain());
			} else {
				//      angular.extend(this.source, newData);
				angular.extend(this.model, newData);
			}
		}

		//public get minYear() {
		//  return new Date().getFullYear() - 70;
		//}

		//public get maxYear() {
		//  return new Date().getFullYear() + 3;
		//}
	}

	export class EvaluationEventDialog {

		public static options(locals: IEvaluationEventDialogLocals): ng.material.IDialogOptions {
			let o = {
				clickOutsideToClose: true,
				templateUrl: "specialed/evaluationeventdialog",
				controller: DialogController,
				controllerAs: "vm",
				bindToController: true,
				locals: locals
			}
			return o;
		}

	}
}