﻿namespace Pineapples.SpecialEd {

	interface IBindings {
		evaluationevents: any;
		student: SpecialEdStudent;
	}

	class Controller extends Sw.Component.ListMonitor implements IBindings {
		public evaluationevents: any[];
		public student: SpecialEdStudent;
		public parentkeyname = "stuID";

		public isWaiting: boolean;

		static $inject = ["$scope", "$mdDialog", "$location", "$state", "$q", "Restangular", "ApiUi", "Lookups"];
		constructor(
			private scope: Sw.IScopeEx
			, public mdDialog: ng.material.IDialogService
			, private $location: ng.ILocationService
			, public $state: ng.ui.IStateService
			, private q: ng.IQService
			, public restangular: restangular.IService
			, private apiUi: Sw.Api.IApiUi
			, public lookups: Sw.Lookups.LookupService) {
			super();
			this.monitor(scope, "specialedevaluations", "spedEvalID");
			this.multiSelect = false;
		}

		public monitoredList() {
			return this.evaluationevents;
		}
		public isListMember(newData) {
			return (this.student && this.student._id() == newData[this.parentkeyname]);
		}

		public $onChanges(changes) {
			if (changes.evaluationevents) {
				//        console.log(this.surveys);
			}
		}

		public showEvaluationEvent(evaluationevent) {
			let locals: IEvaluationEventDialogLocals = {
				model: this.clone(evaluationevent),
				getNew: this.getNew
			}
			this.mdDialog.show(
				EvaluationEventDialog.options(locals)
			);
		}
		private deleteConfirm(e) {
			let evalevent = this.lookups.byCode("spEdEvaluationEventTypes", e.spedEvalEventCode).N;
			return this.mdDialog.confirm()
				.title("Delete Evaluation Event")
				.content("Delete evaluation event: " + evalevent + "?")
				.clickOutsideToClose(true)
				.ok("Delete")
				.cancel("Cancel");
		}
		public deleteEvaluationEvent(e) {
			this.mdDialog.show(this.deleteConfirm(e))
				.then(() => {
					this.isWaiting = true;
					// clone is a bit of overkill here
					// but it does allow restangular to now handle the deletion
					let tq = this.clone(e);
					tq.remove()
						.then(() => {
							this.isWaiting = false;
						})
						.catch((error) => {
							this.isWaiting = false;
							this.apiUi.showErrorResponse(e, error);
						})
				});
		}

		// record a new evaluation event for this (special ed) student
		public new() {
			// set up the defaults for the new evaluation event
			let model = {
				/*trYear: new Date().getFullYear(),*/
				stuID: this.student._id()
			}
			this.showEvaluationEvent(this.clone(model));
		}

		/** 
		 * Provided to the dialog to generate a new item 
		 */
		public getNew = () => {
			let model = {
				/*trYear: new Date().getFullYear(),*/
				stuID: this.student._id()
			}
			return this.q.resolve(this.clone(model));
		}

		// this function will clone a evaluationevents row for editing
		// this makes it easier to support 'undo' without having to recreate the source object
		// Instead, any changes that are made are indirecty propogated back to the source object (ie, row)
		//  by the ListMonitor.
		//
		// It doesn't matter if the input object is a SpecialEdEvaluationEvent object, or if it 
		// is restangularized - the output object will need to be created and restangularized anyway
		// (noting that you cannot successfully 'clone' a restangularized object becuase the 
		// restangular methods are bound to the source object
		private clone(e) {
			let ee: SpecialEdEvaluationEvent;
			// drop any restangular stuff
			let modeldata = (e.plain ? e.plain() : e);
			// create the new sped student evaluation event object
			ee = SpecialEdEvaluationEvent.create(modeldata);
			//restangularize it
			this.restangular.restangularizeElement(null, ee, "specialedevaluations");
			return ee;
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				evaluationevents: '<',
				student: '<'
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "specialed/evaluationeventlist";
		}
	}

	// popup dialog for audit log
	class DialogController extends Sw.Component.MdDialogController {
		static $inject = ["$mdDialog", "evaluationevent"];
		constructor(mdDialog: ng.material.IDialogService, public payslip) {
			super(mdDialog);
		}
	}

	angular
		.module("pineapples")
		.component("componentSpecialEdEvaluationEventList", new Component());
}
