﻿namespace Pineapples.SpecialEd {

	export class SpecialEdEvaluationEvent extends Pineapples.Api.Editable implements Sw.Api.IEditable {

		constructor(specialedData) {
			super();
			this._transform(specialedData);
			angular.extend(this, specialedData);
		}

		// create static method returns the object constructed from the resultset object from the server
		public static create(resultSet: any) {
			let spedEvalEvent = new SpecialEdEvaluationEvent(resultSet);
			return spedEvalEvent;
		}

		// IEditable implementation
		public _name() {
			return (<any>this).spedEvalEventDate + ' ' + (<any>this).spedEvalEventCode;
		}
		public _type() {
			return "specialedevaluationevent";
		}
		public _id() {
			return (<any>this).spedEvalID
		}

		public _transform(newData) {
			// convert these incoming data values
			return super._transform(newData);
		}

		private spedEvalEventDate;
		private spedEvalEventCode;

	}
}