﻿namespace Pineapples.Teachers {
	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			//{
			//  field: 'tID',
			//  name: 'Teacher ID',
			//  displayName: 'Teacher ID',
			//  editable: false,
			//  pinnedLeft: true,
			//  width: 90,
			//  cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item")
			//},
			{
				field: 'tSurname',
				name: 'ctlFull',
				displayName: 'Name',
				pinnedLeft: true,
				width: 160,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <a  ng-click="grid.appScope.vm.action('item','tID', row.entity);">
            <span class="surname">{{row.entity.tSurname}}</span>, {{row.entity.tGiven}}
           </a></div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'tSex',
				name: 'Gender',
				pinnedLeft: true,
				width: 70,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{ // Feedback was to hide the DoB and I don't see any reason for this column to take space in the list grid (open teacher detail if really needed)
				// reason is perhaps to identify... replace ID with DoB
				field: 'tDOB',
				name: 'tDOB',
				pinnedLeft: true,
				displayName: 'DoB',
				cellFilter: "date:'d-MMM-yyyy'",
				width: 100,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
		]
	};

	let modes = [
		{
			key: "Employment",
			gridOptions: {
				columnDefs: [
					{
						field: 'tPayroll',
						name: 'Payroll',
						displayName: 'Teacher Payroll No',
						headerCellFilter: 'vocab',
						cellClass: 'gdAlignRight',
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"],

					},
					{
						field: 'tProvident',
						name: 'Provident Fund',
						displayName: 'Teacher Provident No',
						headerCellFilter: 'vocab',
						cellClass: 'gdAlignRight',
					},
					{
						field: 'tDatePSAppointed',
						name: 'tDatePSAppointed',
						displayName: 'PS Appointed',
						cellFilter: "date:'d-MMM-yyyy'",
					},
					{
						field: 'tDatePSClosed',
						name: 'tDatePSClosed',
						displayName: 'PS End',
						cellFilter: "date:'d-MMM-yyyy'",
					},


					{
						field: 'tYearStarted',
						name: 'tYearStarted',
						displayName: 'Started Teaching',

					},
					{
						field: 'tYearEnded',
						name: 'tYearEnded',
						displayName: 'Finished Teaching',
					},
				]
			}
		},
		{
			key: "Latest Survey",
			columnSet: 2,
			gridOptions: {
				columnDefs: [
					{
						field: 'svyYear',
						name: 'Year',
						cellClass: 'gdAlignRight',
						enableSorting: false
					},
					{
						field: 'schNo',
						name: 'School ID',
						displayName: 'School ID',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: 'schName',
						name: 'School Name',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: 'tchStatus',
						name: 'Status',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: 'tchRole',
						name: 'Role',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: 'tchTAM',
						name: 'Duties',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: 'tchHouse',
						name: 'House?',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					}
				]  // columndefs
			}       // gridoptions
		},           //mode
		{
			key: "Registration",
			columnSet: 1,
			gridOptions: {
				columnDefs: [
					{
						field: "tRegister",
						name: "tRegister",
						displayName: "Teacher Registration No",
						headerCellFilter: 'vocab',
						cellClass: 'gdAlignLeft',
					},
					{
						field: "tRegisterStatus",
						name: "tRegisterStatus",
						displayName: "Teacher Registration Status",
						headerCellFilter: 'vocab',
						cellClass: 'gdAlignLeft',
					},
					{
						field: "tDateRegister",
						name: "tDateRegister",
						displayName: "Teacher Registration Date",
						headerCellFilter: 'vocab',
						cellClass: 'gdAlignLeft',
						cellFilter: "date:'d-MMM-yyyy'",
					},
					{
						field: "tDateRegisterEnd",
						name: "tDateRegisterEnd",
						displayName: "End Date",
						cellClass: 'gdAlignLeft',
						cellFilter: "date:'d-MMM-yyyy'",
					},
					{
						field: "tRegisterEndReason",
						name: "tRegisterEndReason",
						displayName: "End Reason",
						cellClass: 'gdAlignLeft',
					},
				]
			}
		},
		{
			key: "Latest PA",
			columnSet: 20,
			gridOptions: {
				columnDefs: [
					{
						field: 'paID',
						name: 'paID',
						displayName: 'Performance Assessment ID',
						cellClass: 'gdAlignRight'
					},
					{
						field: 'pafrmCode',
						name: 'Type',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: 'paSchNo',
						name: 'School ID',
						displayName: 'School No',
						headerCellFilter: 'vocab',

						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: 'schName',
						name: 'School Name',
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: "paDate",
						name: 'paDate',
						displayName: "Date",
						cellFilter: "date:'d-MMM-yyyy'",
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
					{
						field: "paConductedBy",
						name: 'paConductedBy',
						displayName: "Conducted By",
						cellClass: 'gdAlignLeft',
						enableSorting: false
					},
				]  // columndefs
			}       // gridoptions
		},           //mode
		{
			key: "History Summary",
			columnSet: 3,
			gridOptions: {
				columnDefs: [
					{
						field: 'FormattedYears',
						name: 'FormattedYears',
						displayName: 'Years Employed',
						width: 200
					},
					{
						field: 'AllSchools',
						name: 'AllSchools',
						displayName: 'Schools',
						width: 200
					},
					{
						field: 'AllRoles',
						name: 'AllRoles',
						displayName: 'Roles',
						width: 300
					}
				]
			}
		}
	];              // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};


	angular
		.module('pineapples')
		.run(['TeacherFilter', pushModes]);
}
