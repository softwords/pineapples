﻿namespace Pineapples.Teachers {

  interface IBindings {
    payslip: any;
  }

  class Controller implements IBindings {
    public payslip: any;

    
    constructor() {}

    public $onChanges(changes) {
    }

  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        payslip: '<',
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "teacher/payslip";
    }
  }
  angular
    .module("pineapples")
    .component("componentTeacherPayslip", new Component());
}