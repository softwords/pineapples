﻿namespace Pineapples.Teachers {

  interface IBindings {
    model: Teacher;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: Teacher;

    static $inject = ["ApiUi", "teachersAPI","PermissionStrategies"];
		constructor(apiui: Sw.Api.IApiUi
			, private api: any
			, public permissionStrategies) {
      super(apiui);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

		protected getNew = () => {
			return this.api.new();
		}

  }

  angular
    .module("pineapples")
    .component("componentTeacher", new Sw.Component.ItemComponentOptions("teacher", Controller));
}