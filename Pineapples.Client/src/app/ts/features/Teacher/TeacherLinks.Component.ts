﻿namespace Pineapples.Teachers {

	interface IBindings {
		links: any[];
	}

	class Controller extends Sw.Component.ListMonitor implements IBindings {
		public links: any[];
		public teacher: Teacher;
		public parentkeyname = "lnkID";

		static $inject = ["$scope", "$mdDialog", "Restangular"]
		constructor(
			scope: Sw.IScopeEx
			, private mdDialog: ng.material.IDialogService
			, private Restangular: restangular.IService
		) {
			super();
			this.monitor(scope, "teacherlinks", "lnkID");
			this.multiSelect = false;
		}

		public monitoredList() {
			return this.links;
		}
		public isListMember(newData) {
			return (this.teacher?._id() == newData[this.parentkeyname]);
		}

		public $onChanges(changes) {
		}
		public newDoc() {
			//this.state.go("site.teachers.upload", { id: this.teacherId });
			this._uploadDialog().then(() => {
			}, (error) => {
			});
		}

		private _uploadDialog() {
			let options: ng.material.IDialogOptions = {
				locals: { teacher: this.teacher },
				controller: TeacherLinkUploadController,
				controllerAs: 'vm',
				bindToController: true,
				clickOutsideToClose: true,
				templateUrl: "teacherlink/uploaddialog"
			}
			return this.mdDialog.show(options);
		}

		private doEdit(link, event) {
			this.Restangular.one("teacherlinks", link.lnkID).get().then(
				(link: Pineapples.Documents.DocumentLink) => {
					this._editDialog(link);
				});
		}

		private _editDialog(tlink: TeacherLink) {
			let options: ng.material.IDialogOptions = {
				locals: { model: tlink, teacher: this.teacher },
				controller: editController,
				controllerAs: 'vm',
				bindToController: true,
				templateUrl: "teacherlink/editdialog"
			}
			return this.mdDialog.show(options);

		}

		private doDelete(link, event) {
			let confirm = this.mdDialog.confirm()
				.title('Delete Document')
				.textContent('Delete the document ' + link.docTitle + ' from the document library?')
				.targetEvent(event)
				.ok('Delete')
				.cancel('Cancel');
			this.mdDialog.show(confirm).then(() => {
				let tlink = new TeacherLink(link);
				// this has cloned the data...
				this.Restangular.restangularizeElement(null, tlink, "teacherlinks");
				tlink.remove().then(() => {
				},
					() => { }
					// remove failed
				);
			});
		}

	}

	/**
	 * Controller for the edit Dialog
	 */
	class editController extends Sw.Component.ComponentEditController {
		static $inject = ["$mdDialog", "ApiUi"];
		constructor(public mdDialog: ng.material.IDialogService
			, apiUi: Sw.Api.IApiUi) {
			super(apiUi);
			this.isEditing = true;
		}
		public teacher: Teacher;

		public onModelUpdated(newData: any) {
			super.onModelUpdated(newData);
			this.teacher.tPhoto = newData.tPhoto;
		}

		public closeDialog() {
			this.mdDialog.cancel();
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				links: "<",
				teacher: "<"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "teacher/linklist";
		}
	}
	angular
		.module("pineapples")
		.component("componentTeacherLinks", new Component());

}