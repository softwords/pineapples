﻿namespace Pineapples.Survey {

  interface IBindings {
    schoolSurvey: any;
  }

  class Controller implements IBindings {
    public schoolSurvey: any;
    public template: string;

    static $inject = ["$mdDialog", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.schoolSurvey) {
        console.log("changes.schoolSurvey: ", this.schoolSurvey);
      }
    }
    public $onInit() {
      this.template = `survey/survey/${this.schoolSurvey.ssSchType}/${this.schoolSurvey.svyYear}`;
    }

    public gotoSchool() {
      this.$state.go("site.schools.list.item", {
        id: this.schoolSurvey.schNo
      });
		}
  }

  class Component implements ng.IComponentOptions {
    public bindings = {
      schoolSurvey: '<'
    };

    public controller = Controller;
    public controllerAs = "vm"
    public template = `<ng-include src="vm.template"></ng-include>`;
  }

  angular
    .module("pineapples")
    .component("survey", new Component());
}
