﻿// Reports Routes
namespace Pineappples.Reports {

	let routes = function ($stateProvider) {

		// root state for 'reports' feature
		let state: ng.ui.IState;
		let statename: string;

		// base reports state
		state = {
			url: "^/reports",
			abstract: true
		};
		$stateProvider.state("site.reports", state);

		// upload state
		state = {
			url: "^/reports/test",

			views: {
				"@": {
					component: "reportDefCard"
				}
			},
		};
		$stateProvider.state("site.reports.test", state);

		// List additional routes for "other reports" that don't fall under specific user modules (e.g. Schools, Exams)
		// These should be grouped into general groups enough that we won't end up with a whole bunch
		// of country specific routes. A good example would be audit as the one below that will include all audit
		// reports specifically geared towards management (e.g. user access, user acitivity, etc.) and can include data from
		// various modules hence they don't fit under specific modules (e.g. Schools, Exams).
		state = {
			url: "^/reports/audit",
			data: {
				// TODO There is no specific feature for reports to control permissions. Perhaps permissions to read
				// all features of the system could be enforced to view reports here since they contains a bit of everything?! 
				// For now, for simplicity and speed's sake, someone with Read permissions
				// on School can view reports since all reports in there are not sensitive data.
				permissions: {
					only: 'SchoolRead'
				}
			},
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "Reports",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always",
				pageTitle: ["pageTitle", (title) => `{title} Reports`]
			}
		}
		$stateProvider.state("site.reports.audit", state);

		state = {
			url: "^/reports/all",
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always"
			}
		}
		$stateProvider.state("site.reports.all", state);

		// reload state for testing
		state = {
			url: '^/reports/reload',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.remove("reports/reportdefcomponent/card");
				$templateCache.remove("reports/reportdefcomponent/button");
				$templateCache.remove("reports/reportdefcomponent/icon");
				$templateCache.remove("reports/reportdefcomponent/row");
				$state.go("site.reports.test");
			}]
		};
		statename = "site.reports.reload";
		$stateProvider.state(statename, state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}