﻿namespace Pineapples.QuarterlyReports {

  interface IBindings {
    model: QuarterlyReport;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: QuarterlyReport;

    static $inject = ["ApiUi", "quarterlyReportsAPI"];
    constructor(apiui: Sw.Api.IApiUi, private api: any) {
      super(apiui);
    }

		protected getNew = () => {
			return this.api.new();
		}

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentQuarterlyReport", new Sw.Component.ItemComponentOptions("quarterlyreport", Controller));
}