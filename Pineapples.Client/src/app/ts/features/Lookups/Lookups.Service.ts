﻿namespace Pineapples.Lookups {
  export class LookupService extends Sw.Lookups.LookupService {
		
		constructor(_q: ng.IQService, rootscope: Sw.IRootScopeEx,_api, dialogs: angular.material.IDialogService) {
			super(_q, rootscope, _api, dialogs);
    }
    /**
     * retrieve code set for Performance Assessments
     */
    public pa() {
      
      if (this.cache && this.cache["paCompetencies"]) {
				return this._q.resolve(this.cache);
			}
			
      return this._api.pa().then((result) => {
					this.addToCache(result);
					return (this.cache);
				},
        error => {
						return this._q.reject(error);
        });
		}

		public student() {

			if (this.cache && this.cache["spEdEnvironment"]) {
				return this._q.resolve(this.cache);
			}

			return this._api.student().then((result) => {
				this.addToCache(result);
				return (this.cache);
			},
				error => {
					return this._q.reject(error);
				});
		}

		public findata() {

			if (this.cache && this.cache["costCentres"]) {
				return this._q.resolve(this.cache);
			}

			return this._api.findata().then((result) => {
				this.addToCache(result);
				return (this.cache);
			},
				error => {
					return this._q.reject(error);
				});
		}

		public scholarship() {

			if (this.cache && this.cache["tertiaryGrades"]) {
				return this._q.resolve(this.cache);
			}

			return this._api.scholarship().then((result) => {
				this.addToCache(result);
				return (this.cache);
			},
				error => {
					return this._q.reject(error);
				});
		}


		// Override of list monitor
		protected onDataMerge = (data, operation?: string) => {
			let code = data.data["Code"];
			let what = data.what;
			super.onDataMerge(data, operation);
		}

		protected mapList(listname: string) {
			return super.mapList(listname);
		}

		/*--------------------------------------------------------------------------------------
		EMIS specific code manipulations
		---------------------------------------------------------------------------------------*/


		/**
		 * Return an arry of the class levels that belong to an education level
		 * @param edLevelCode
		 */
		public edLevelClasses(edLevelCode: string): string[] {
			return this.cache["levels"]
				.filter((classrow: any) => {
					return (classrow.L == edLevelCode);
				})
				.sort((a: any, b: any) => a.YoEd - b.YoEd)
				.map((classrow: any) => classrow.C);
		}
	}

  angular
    .module('pineapples')
    .service('Lookups', LookupService);
}
