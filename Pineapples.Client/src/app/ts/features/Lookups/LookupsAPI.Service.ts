﻿/* ------------------------------------------
    lookupsAPI
    ----------------------------------------
*/
namespace Pineapples.Lookups {

  class LookupsApi extends Sw.Lookups.LookupsApi {
		public pa = () => this.http.get("api/lookups/collection/pa")
			.then(response => response.data);
		public findata = () => this.http.get("api/lookups/collection/findata")
			.then(response => response.data);
		public student = () => this.http.get("api/lookups/collection/student")
			.then(response => response.data);
		public scholarship = () => this.http.get("api/lookups/collection/scholarship")
			.then(response => response.data);

  }
  angular
    .module('pineapples')
    .service('lookupsAPI', LookupsApi);
}
