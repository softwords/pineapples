namespace Pineappples.SchoolAccreditations {

	let RouteHelper = Sw.Utils.RouteHelper;
	let routes = function ($stateProvider: ng.ui.IStateProvider) {
		var featurename = 'SchoolAccreditations';
		var filtername = 'SchoolAccreditationFilter';
		var templatepath = "schoolAccreditation";
		var tableOptions = "schoolaccreditationFieldOptions";
		var url = "schoolaccreditations";
		var usersettings = null;
		//var mapview = 'SchoolMapView';

		// root state for 'schoolaccreditation' feature
		let state: ng.ui.IState = RouteHelper.frameState(featurename, filtername, templatepath);

		// default 'api' in this feature is schoolaccreditationsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "schoolAccreditationsAPI";

		state.data = state.data || {};
		state.data["frameTitle"] = "School Accreditations";
		state.data["icon"] = "assessment";
		state.data["permissions"] = {
			only: "InspectionRead"
		};

		let statename = "site.schoolaccreditations";
		$stateProvider.state("site.schoolaccreditations", state);

		// List state (here not using the @name RouteHelper.frameListState)
		// but defining the state right here with extra renderoptions.
		state = {

			url: '/list',
			data: {
				rendermode: "List"
			},
			views: <{ [name: string]: ng.ui.IState }>{
				"renderarea": "pagedListManager",
				"renderoptions": {
					templateUrl: "generic/PagedListParamsComponent",
					controller: "FilterController",
					controllerAs: "vm",
				}
			}
		};

		statename = "site.schoolaccreditations.list";
		state.url = "^/schoolaccreditations/list";
		$stateProvider.state(statename, state);

		/* reload route */
		state = {
			url: '^/schoolaccreditations/reload',
			onEnter: ["$state", "$templateCache", "$location",
				($state: ng.ui.IStateService, $templateCache: ng.ITemplateCacheService
					, $location: ng.ILocationService) => {
					$templateCache.remove("schoolaccreditation/item");
					$templateCache.remove("schoolaccreditation/searcher");
					$templateCache.remove("schoolaccreditation/formb/legacy");
					$templateCache.remove("schoolaccreditation/formb/v1");
					$templateCache.remove("schoolaccreditation/dashboard");
					$templateCache.remove("schoolaccreditation/dashboard/component/districttable");
					$templateCache.remove("schoolaccreditation/dashboard/component/nationchart");
					$templateCache.remove("schoolaccreditation/dashboard");

					$templateCache.remove("generic/renderlist");
					$templateCache.remove("common/supergrid");



					$templateCache.remove("schoolinspection/inspectiontype/school_accreditation");
					$state.reload();
				}]
		};
		statename = "site.schoolaccreditations.reload";
		$stateProvider.state(statename, state);

		// state for a high level dashboard
		state = {
			url: "^/schoolaccreditations/dashboard",
			data: {
				//permissions: {
				//	only: 'SchoolReadX'
				//},
				rendermode: "Dashboard"
			},
			views: {
				"renderarea": {
					component: "schoolAccreditationDashboard"
				},
				"searcher": "coreOptionsEditor"

			},
			resolve: {
				options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
					return new Pineapples.Dashboards.coreOptions(lookups, scope.hub);
				}],

				tables: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
					// overall level 
					let p = reflator.get("api/warehouse/accreditations").then(response => (<any>response.data));
					// level by standard
					let p2 = reflator.get("api/warehouse/accreditations?bystandard&report").then(response => (<any>response.data));
					// use $q.all to return both sets in an array
					return q.all([p, p2]);
				}],
				pageTitle: RouteHelper.simpleTitle("Dashboard")
			}
		};
		$stateProvider.state("site.schoolaccreditations.dashboard", state);

		// chart table and map
		RouteHelper.addChartState($stateProvider, featurename);
		RouteHelper.addTableState($stateProvider, featurename);
		// RouteHelper.addMapState($stateProvider, featurename, mapview);

		// new - state with a custom url route
		/** New state is no longer available for SchoolAccreditation **/

		//------------------------ Reports -------------------------------------------
		state = {
			url: "^/schoolaccreditations/reports",
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "School_Accreditations",  // actually School Accreditation folder in Jasper but URI is underscore
				promptForParams: () => "always",
				pageTitle: ["pageTitle", (title) => `{title} Reports`]
			}
		}
		$stateProvider.state("site.schoolaccreditations.reports", state);

		// item state
		//$resolve is introduced in ui-route 0.3.1
		// it allows bindings from the resolve directly into the template
		// injections are replaced with bindings when using a component like this
		// the component definition takes care of its required injections - and its controller
		state = {
			url: "^/schoolaccreditations/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.schoolaccreditations": {
					component: "componentSchoolInspection"						// uses school inspection now
				}
			},
			resolve: {
				model: ['schoolAccreditationsAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}]
			}
		};
		$stateProvider.state("site.schoolaccreditations.list.item", state);
		// item state also accessible from dashboard
		state = angular.copy(state);
		state.url = "^/schoolaccreditations/dashboard/{id}";
		$stateProvider.state("site.schoolaccreditations.dashboard.item", state);

		// allow access to the school from here as well -
		// what is the best way to re-use this state definition?? - 
		// item state
		// ----------------- school state : allows a school to be displayed from the list of inspections

		state = {
			url: "^/schoolaccreditations/list/school/{id}",
			views: {
				"actionpane@site.schoolaccreditations": "componentSchool"
			}
		};
		state = RouteHelper.clone($stateProvider, "site.schools.list.item", state);
		$stateProvider.state("site.schoolaccreditations.list.school", state);

		state = RouteHelper.clone($stateProvider
			, "site.schoolaccreditations.list.school"
			, { url: "^/schoolaccreditations/list/school/{id}" });
		$stateProvider.state("site.schoolaccreditations.dashboard.school", state);


		$stateProvider
			.state('site.schoolaccreditations.dataexport', {
				url: '^/schoolaccreditations/dataexport',

				views: {
					"@": "schoolAccreditationDataExportComponent"
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Data Export")
				}
			});

	}
	angular
		.module('pineapples')
		.config(['$stateProvider', routes])
}
