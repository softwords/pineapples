﻿namespace Pineapples.Dashboards {
	export namespace Accreditations {
		export interface IxfData {
			Age: number;
			AuthorityCode: string;
			AuthorityGovtCode: string;
			AuthorityGovt: string;
			InspectionTypeCode: string;
			InspectionResult: string;
			DistrictCode: string;
			SchoolTypeCode: string;
			SurveyYear: number
		}
		export interface IxfStdData {
			Age: number;
			AuthorityCode: string;
			AuthorityGovtCode: string;
			AuthorityGovt: string;
			InspectionTypeCode: string;
			InspectionResult: string;
			DistrictCode: string;
			SchoolTypeCode: string;
			SurveyYear: number;
			Standard: string;
			StandardResult: string;
		}
		export interface INumNumYear {
			Num: number, 
			NumYear: number
		}

		export interface ILevelNums {
			[level:string]: INumNumYear
		}

	}
	export class SchoolAccreditationDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {


		private xReduce = _.memoize((tableName, ...inputs) => this.xFilter.xReduce.bind(this.xFilter)(...inputs))
		private groupReduceSum = _.memoize((groupName, dim, va) => dim.group().reduceSum(va));

		// additional data

		private tables: any[];
		// override with strongly typed crossfilter
		public xf: CrossFilter.CrossFilter<Accreditations.IxfData>;
		public xfStandards: CrossFilter.CrossFilter<Accreditations.IxfStdData>;

		// dimensions and groups based on Accreditations
		public dimSurveyYear: CrossFilter.Dimension<Accreditations.IxfData, number>;
		public dimDistrictCode: CrossFilter.Dimension<Accreditations.IxfData, string>;
		public dimAuthorityCode: CrossFilter.Dimension<Accreditations.IxfData, string>;
		public dimAuthorityGovtCode: CrossFilter.Dimension<Accreditations.IxfData, string>;
		public dimSchoolTypeCode: CrossFilter.Dimension<Accreditations.IxfData, string>;
		public dimAggregate: CrossFilter.Dimension<Accreditations.IxfData, string>;

		public grpDistrictCode: CrossFilter.Group<Accreditations.IxfData, string, number>;
		public grpAuthorityCode: CrossFilter.Group<Accreditations.IxfData, string, number>;
		public grpAuthorityGovtCode: CrossFilter.Group<Accreditations.IxfData, string, number>;

		public xtabDistrictCode: CrossFilter.Group<Accreditations.IxfData, string, Accreditations.ILevelNums>;
		public xtabAuthorityCode: CrossFilter.Group<Accreditations.IxfData, string, Accreditations.ILevelNums>;
		public xtabSurveyYear: CrossFilter.Group<Accreditations.IxfData, string, Accreditations.ILevelNums>;
		public xtabAggregate: CrossFilter.Group<Accreditations.IxfData, string, Accreditations.ILevelNums>;


		// dimensions and groups based on Standards
		// principal grouping is by standard
		public dimStd: CrossFilter.Dimension<Accreditations.IxfStdData, string>;
		public dimStandardYear: CrossFilter.Dimension<Accreditations.IxfStdData, number>;
		public dimStandardDistrictCode: CrossFilter.Dimension<Accreditations.IxfStdData, string>;
		public dimStandardAuthorityCode: CrossFilter.Dimension<Accreditations.IxfStdData, string>;
		public dimStandardAuthorityGovtCode: CrossFilter.Dimension<Accreditations.IxfStdData, string>;
		public dimStandardSchoolTypeCode: CrossFilter.Dimension<Accreditations.IxfStdData, string>;
		public xtabStd: CrossFilter.Group<Accreditations.IxfStdData, string, Accreditations.ILevelNums>;

		public layers: any[];

		public $onChanges(changes) {
			if (changes.tables && changes.tables.currentValue) {
				// data has been retrieved by the ui-router as a Resolve
				// and passed to the dashboard as a binding - process it into a crossfilter
				// create the crossfilter from the warehouse table
				this.xf = crossfilter(this.tables[0]);
				this.xfStandards = crossfilter(this.tables[1]);
				this.createDimensions();
			}
		}

		public $onInit() {
			
		}

		public onOptionChange(data:IOptionChangeData, sender) {
			if (data.selectedYear) {
				this.dimSurveyYear.filter(this.options.selectedYear);
				this.dimStandardYear.filter(this.options.selectedYear);
			}
			if (data.selectedDistrict) {
				this.dimDistrictCode.filter(this.options.selectedDistrict);
				this.dimStandardDistrictCode.filter(this.options.selectedDistrict);
			}
			if (data.selectedAuthorityGovt) {
				this.dimAuthorityGovtCode.filter(this.options.selectedAuthorityGovt);
				this.dimStandardAuthorityGovtCode.filter(this.options.selectedAuthorityGovt);
			}
			if (data.selectedAuthority) {
				this.dimAuthorityCode.filter(this.options.selectedAuthority);
				this.dimStandardAuthorityCode.filter(this.options.selectedAuthority);
			}
			if (data.selectedSchoolType) {
				this.dimSchoolTypeCode.filter(this.options.selectedSchoolType);
				this.dimStandardSchoolTypeCode.filter(this.options.selectedSchoolType);
			}
			// always call the super version so that the clients get to know about the option change as well
			super.onOptionChange(data, sender);
		}

		private elSelector = ({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel)["L"];
		private classLevelSelector = this.xFilter.getPropAccessor("ClassLevel")

		public createDimensions() {
			// create the dimensions

			// Lookup by code like others now...
			//this.dimAuthorityGovtCode = this.xf.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovtCode);
			//	return ag ? ag.N : null;
			//});

			this.dimAuthorityGovtCode = this.xf.dimension(d => d.AuthorityGovtCode);
			this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
			this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);
			this.dimAuthorityCode = this.xf.dimension(d => d.AuthorityCode);
			this.dimSchoolTypeCode = this.xf.dimension(d => d.SchoolTypeCode);
			this.dimAggregate = this.xf.dimension(d => "-");

			// function to shape the value on each row of these reductions
			let levelReducer = d => {
				let isAccredited = ['Level 2', 'Level 3', 'Level 4'].indexOf(d.InspectionResult) >= 0;
				return {
					Num: d.Num, NumThisYear: d.NumThisYear
					, Accredited: (isAccredited ? d.Num : null)
					, AccreditedThisYear: (isAccredited ? d.NumThisYear : null)
				};
			};

			this.xtabDistrictCode = this.xFilter.xReduce(this.dimDistrictCode, "InspectionResult", levelReducer);
			this.xtabSurveyYear = this.xFilter.xReduce(this.dimSurveyYear, "InspectionResult", levelReducer);
			this.xtabAuthorityCode = this.xFilter.xReduce(this.dimAuthorityCode, "InspectionResult", levelReducer);
			this.xtabAggregate = this.xFilter.xReduce(this.dimAggregate, "InspectionResult", levelReducer);

			console.log(this.xtabDistrictCode.all());
			console.log(this.xtabSurveyYear.all());

			this.dimStd = this.xfStandards.dimension(d => d.Standard||"-");
			this.dimStandardAuthorityGovtCode = this.xfStandards.dimension(d => d.AuthorityGovtCode);	
			this.dimStandardYear = this.xfStandards.dimension(d => d.SurveyYear);
			this.dimStandardDistrictCode = this.xfStandards.dimension(d => d.DistrictCode);
			this.dimStandardAuthorityCode = this.xfStandards.dimension(d => d.AuthorityCode);
			this.dimStandardSchoolTypeCode = this.xfStandards.dimension(d => d.SchoolTypeCode);


			// in the data shape we already ahve separate columns for Level 1, Level 2, Level 3, Level 4
			// using standard for the keyAccessor in xReduce just give a property in the 
			// 'value' with the same name as the key, which will be identical to total
			this.xtabStd = this.xFilter.xReduce(this.dimStd, "Standard",
				d => {
					//return {
					//	"Level 1": {
					//		Num: d.Level1,
					//		NumThisYear: d.Level1InYear
					//	},
					//	"Level 2": {
					//		Num: d.Level2,
					//		NumThisYear: d.Level2InYear
					//	},
					//	"Level 3": {
					//		Num: d.Level3,
					//		NumThisYear: d.Level3InYear
					//	},
					//	"Level 4": {
					//		Num: d.Level4,
					//		NumThisYear: d.Level4InYear
					//	}
					//};
					let acc = (d.Level2?d.Level2:0) + (d.Level3?d.Level3:0) + (d.Level4?d.Level4:0);
					let accinyear = (d.Level2InYear ? d.Level2InYear : 0)
						+ (d.Level3InYear ? d.Level3InYear : 0)
						+ (d.Level4InYear ? d.Level4InYear : 0);

					return {
							"Level 1": d.Level1
						, "Level 2": d.Level2 
						, "Level 3": d.Level3
						, "Level 4": d.Level4
						, "Level 1InYear": d.Level1InYear
						, "Level 2InYear": d.Level2InYear
						, "Level 3InYear": d.Level3InYear
						, "Level 4InYear": d.Level4InYear
						, Accredited: acc
						, Num: (d.Level1?d.Level1:0) + acc
						, AccreditedThisYear: accinyear
						, NumThisYear: (d.Level1InYear?d.Level1InYear:0) + accinyear
					};
				});


			// set the surveyYear to be the most recent in the warehouse data, rather than the most recent surveyYear in the lookups
			// we dont want to get empty data on the initial display if the warehouse is not yet generated for the current year
			if (!this.options.selectedYear) {
				this.options.selectedYear = this.dimSurveyYear.top(1)[0].SurveyYear;
			}

			// TO DO 
			this.layers = [
			{
				name: 'L1',
					accessor: (d) => (d.value["Level 1"]? d.value["Level 1"].Num : 0)
			},
			{
				name: 'L2',
				accessor: (d) => (d.value["Level 2"] ? d.value["Level 2"].Num : 0)
			},
			{
				name: 'L3',
				accessor: (d) => (d.value["Level 3"] ? d.value["Level 3"].Num : 0)
			},
			{
				name: 'L4',
				accessor: (d) => (d.value["Level 4"] ? d.value["Level 4"].Num : 0)
			}
			];

			this.dashboard = this;
		}

	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			tables: "<"	,			// array of data tables
			options: "<"			// get the options
		};
		public controller = SchoolAccreditationDashboard;
		public controllerAs = "vm"
		public templateUrl: string = "schoolaccreditation/dashboard";
	}

	angular
		.module("pineapples")
		.component("schoolAccreditationDashboard", new Component());
}
