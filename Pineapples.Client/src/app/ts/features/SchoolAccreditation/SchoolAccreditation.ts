﻿namespace Pineapples.SchoolAccreditations {

  export class SchoolAccreditation extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    //private _scores: Array<Number>;

    constructor(saData) {
			super();
			// saData.accreditation -- now we are retrieving a structured object
			// as the schoolaccreditation which include the entire source survey
			// as InspectionContent
			// This is all converted from Xml, so the additional root element - accreditation -
			// has been introduced
			// cf [pSurveyRead].[SchoolAccreditationRead]
			// which now returns FOR XML PATH
			// Now this will also be returned by pInspectionRead.Inspection_ReadXml
			angular.extend(this, saData);
			this.survey = new Pineapples.Cloudfiles.SchoolStandardAssessment(saData.InspectionContent);
    }

		// this recreates the original survey, from the Xml data
		public survey: Pineapples.Cloudfiles.SchoolStandardAssessment;
    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let sa = new SchoolAccreditation(resultSet);
      return sa;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).schNo + '-' + (<any>this).InspYear;
    }

    public _type() {
      return "schoolaccreditation";
    }

    public _id() {
      return (<any>this).saID
    }


  }
}
