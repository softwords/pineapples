namespace Pineapples.SchoolAccreditations {

	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'inspID',
				name: 'SA ID',
				displayName: 'ID',
				editable: false,
				width: 60,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm")
			},
			{
				field: 'InspectedBy',
				name: 'Completed By',
				displayName: 'Completed By',
				editable: false,
				width: 300,
			},
			{
				field: 'InspectionYear',
				name: 'Year',
				displayName: 'Year',
				editable: false,
				width: 80,
				cellClass: "gdAlignRight",
			},
			{
				field: 'schNo',
				name: 'School ID',
				displayName: 'School ID',
				editable: false,
				width: 100,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("school", "vm")
			},
			{
				field: 'schName',
				name: 'SchoolName',
				displayName: 'School Name',
				editable: false,
				width: 200,
			},
			{
				field: 'StartDate',
				name: 'StartDate',
				displayName: 'Start Date',
				cellFilter: "date:mediumDate",
				editable: false,
				width: 120,
			},
			{
				field: 'EndDate',
				name: 'EndDate',
				displayName: 'End Date',
				cellFilter: "date:mediumDate",
				editable: false,
				width: 120,
			},

			{
				field: 'InspectionResult',
				name: 'InspectionResult',
				displayName: 'Level',
				editable: false,
				width: 100,
				cellTemplate: '<div class="ngCellText ui-grid-cell-contents" ng-class="col.colIndex()"><stars level="row.entity[col.field]"></stars></div>'
			},

		]
	};

	let modes = [
		{
			key: "Default",
			columnSet: 0,
			gridOptions: {
				columnDefs: [
				]
			}
		}
	]; // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};


	angular
		.module('pineapples')
		.run(['SchoolAccreditationFilter', pushModes]);
}
