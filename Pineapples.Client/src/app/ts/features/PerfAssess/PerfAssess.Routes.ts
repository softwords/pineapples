﻿module Pineapples {

	let routes = ($stateProvider: angular.ui.IStateProvider) => {
		var featurename = 'PerfAssess';
		var filtername = 'PerfAssessFilter';
		var templatepath = "perfAssess";
		var url = "perfassess";
		var tableoptions = 'paFields';

		var usersettings = null;

		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath);


		// default 'api' in this feature is schoolsAPI
		state.resolve = state.resolve || {};
		state.resolve["pageTitle"] = Sw.Utils.RouteHelper.featureTitle("Performance Assessment");

		state.data = state.data || {};
		state.data.permissions = {
			only: "PARead"
		};
		state.resolve["api"] = "perfAssessAPI";
		// initialise the pa lookups when pa is first used
		// these are large, so strategy is: don't load them if they are not needed
		// this is a pattern for loading on-demand, specialised codesets:
		// -- create a custom method in LookupsService and in DSLookups
		// -- add to cache when resolved
		// -- return a promise; call it from resolve at the start of the route tree
		state.resolve["customLookups"] = ["Lookups", (lookups: Pineapples.Lookups.LookupService) => lookups.pa()]

		let basestate = "site.perfassess";
		$stateProvider.state(basestate, state);

		// List State
		state = Sw.Utils.RouteHelper.frameListState("perfassess", "paID");
		let statename = `${basestate}.list`;
		$stateProvider.state(statename, state);

		// add the list/new state here as a simple forward to perfassess/entry
		// this captures the default 'new' action button in the renderframe
		// but , see: https://stackoverflow.com/questions/23830421/how-to-implement-path-aliases-in-ui-router
		// for general advice on aliasing state
		state = {
			url: '/new',
			data: {
				permissions: {
					only: "PAWrite"
				}
			},
			onEnter: ["$state", function ($state) {
				$state.go("site.perfassess.entry");
			}]
		};
		statename = "site.perfassess.list.new";
		$stateProvider.state(statename, state);

		state = {
			url: '/-',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.removeAll();
				$state.reload();
			}]
		};
		statename = "site.perfassess.reload";
		$stateProvider.state(statename, state);


		state = {
			url: '/chart',

			views: {
				"navbar@": {
					templateUrl: "common/chartgroupparam",        // local becuase we have to get acess to the data layer
					controller: "TableParamsController",
					controllerAs: "vm"
				},
				"@": {
					templateUrl: "common/chart",
					controller: "ChartController",
					controllerAs: "vm"
				}

			},
			resolve: {
				palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
				mapColorScale: function () { return new Sw.Maps.MapColorScale(); },
				pageTitle: ["pageTitle", (title) => `${title} Chart`]
			}
		};
		$stateProvider.state("site.perfassess.chart", state);

		$stateProvider
			.state("site.perfassess.entry", {
				url: "/entry",
				data: {
					permissions: {
						only: "PAWrite"
					}
				},
				views: {
					"@": {
						templateUrl: "perfassess/entry",
						controller: "perfAssessController",
						controllerAs: "vm"
					}
				},
				resolve: {
					pageTitle: ["pageTitle", (title) => `${title} Entry`]
				}
			});
	};

	angular
		.module("pineapples")
		.config(["$stateProvider", routes]);
}
