﻿namespace Pineapples.PerfAssess {

	let viewDefaults = {
		columnSet: 1,
		columnDefs: [
			{
				field: 'paID',
				name: 'paID',
				displayName: 'PA ID',
				width: 90,
				cellClass: 'gdAlignRight',
				// currently, there is no item state on perfassess
				//cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm")
			},
			{
				field: 'pafrmCode',
				name: 'Type',
				width: 60,
				cellClass: 'gdAlignLeft',
				enableSorting: false
			},
			{
				displayName: 'Name',
				name: 'ctlFull',
				field: 'tSurname',
				cellTemplate: Sw.Utils.UiGridUtils.nameColumn("tGiven", "tSurname"),
				width: 300,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'paSchNo',
				name: 'School No',
				width: 80,
				cellClass: 'gdAlignLeft',
				enableSorting: false
			},
			{
				field: 'schName',
				name: 'School Name',
				width: 200,
				cellClass: 'gdAlignLeft',
				enableSorting: false
			},
			{
				field: "paDate",
				name: 'paDate',
				displayName: "Date",
				cellFilter: "date",
				width: 120,
				cellClass: 'gdAlignLeft',
				enableSorting: false
			},
			{
				field: "paConductedBy",
				name: 'paConductedBy',
				displayName: "Conducted By",
				width: 200,
				cellClass: 'gdAlignLeft',
				enableSorting: false
			},
		]  // columndefs
	};
  let modes = [
        {
          key: "Perf Assess",
          columnSet: 1,
          gridOptions: {
            columnDefs: []
          }
        }
  ];
	var pushModes = function (filter) {
		filter.ViewDefaults = viewDefaults;
    filter.PushViewModes(modes);
  };

  angular
      .module('pineapples')
      .run(['PerfAssessFilter', pushModes])
}
