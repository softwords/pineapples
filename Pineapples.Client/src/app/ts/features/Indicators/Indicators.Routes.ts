﻿namespace Pineapples.Indicators {

	let RouteHelper = Sw.Utils.RouteHelper;


	function indicatorsRoute(sp: ng.ui.IStateProvider, context: string, edLevel: string, schooltype: string, sector: string) {
		let stateName: string = "";
		if (context === "") {
			stateName = "site.indicators." + edLevel.toLowerCase();
		}
		else {
			stateName = "site.indicators." + context + "." + edLevel.toLowerCase();
		};
		let url = "/" + edLevel.toLowerCase();
		let templateUrl = "indicators/" + edLevel.toLowerCase();

		let data: any = {
			selectedEdLevel: edLevel.toUpperCase(),
			selectedSchoolType: schooltype,
			selectedSector: sector
		};
		let state: ng.ui.IState = {
			url: url,
			data: data,
			views: {
				"@": {
					component: "indicatorsComponent"
				}
			},
			resolve: {
				pageTitle: ["pageTitle", "Lookups", (title, lookups: Sw.Lookups.LookupService) => {
					let edLevelName = lookups.byCode("educationLevels", edLevel, "N") || edLevel;
					return `${title}: ${edLevelName}`
				}]
			}
		};
		sp.state(stateName, state);
	}

	/**
	 * Generate a route to create an Indicators page for a district
	 * This only needs to modify the call to get the vermdata
	 * @param sp stateProviderservice - needed to register the new state
	 * @param context
	 * @param edLevel
	 * @param schooltype
	 * @param sector
	 * @param districtCode
	 */
	function indicatorsDistrictRoute(sp: ng.ui.IStateProvider, context: string, edLevel: string, schooltype: string, sector: string, districtCode: string) {
		// construct the route name including the district
		let stateName: string = "";
		if (context === "") {
			stateName = "site.indicators." + edLevel.toLowerCase() + "." + districtCode.toLowerCase(); // e.g. site.indicators.pri.pni
		}
		else {
			stateName = "site.indicators." + context + "." + edLevel.toLowerCase() + "." + districtCode.toLowerCase();        // e.g. site.indicators.fedemis.pri.ksa
		};

		let url = "/" + districtCode.toLowerCase();                                // #/indicators/fedemis/pri/yap
		let templateUrl = "indicators/" + edLevel.toLowerCase();                     // doesn't change - same template in each district

		let data: any = {
			selectedEdLevel: edLevel.toUpperCase(),
			selectedSchoolType: schooltype,
			selectedSector: sector,
			selectedDistrict: districtCode
		};
		let state: ng.ui.IState = {
			url: url,
			data: data,
			views: {
				"@": {
					component: "indicatorsComponent"
				},
			},
			// resolves are now specific to the route for districts, allowing the customised vermpaf
			resolve: {
				// resolve the calculator used in the component - this will get a cached one
				// or else read the required vermdata and make one
				indicatorCalc: ['IndicatorsMgr', (mgr: IndicatorsMgr) => mgr.getCalculator(districtCode)],
				pageTitle: ["pageTitle", "Lookups", (title, lookups: Sw.Lookups.LookupService) => {
					let edLevelName = lookups.byCode("educationLevels", edLevel, "N") || edLevel;
					let districtName = lookups.byCode("districts", districtCode, "N") || districtCode;
					return `${title}: ${edLevelName} ${districtName}`
				}]
			}
		};
		sp.state(stateName, state);
	}
	var routes = function ($stateProvider) {
		$stateProvider
			.state('site.indicators', {
				url: '/indicators',
				abstract: true,

				resolve: {
					// resolve the calculator
					indicatorCalc: ['IndicatorsMgr', function (mgr: IndicatorsMgr) {
						return mgr.getCalculator("");
					}],
					pageTitle: Sw.Utils.RouteHelper.featureTitle("Indicators")
				}
			})

		// The following are particular to each country where this EMIS is deployed
		// The code should reflect what has been setup in the database tables lkpEducationLevels, SchoolTypes, EducationSectors
		// This should also be configured in the IdentitiesP's Navigation table

		// KEMIS Indicators pages
		$stateProvider
			.state('site.indicators.kemis', {
				url: '/kemis',
				abstract: true,
			});
		indicatorsRoute($stateProvider, "kemis", "PRI", "P", "PRI");
		indicatorsRoute($stateProvider, "kemis", "JS", "JS", "JSS");
		indicatorsRoute($stateProvider, "kemis", "SS", "SS", "SEC");

		// SIEMIS Routes
		$stateProvider
			.state('site.indicators.siemis', {
				url: '/siemis',
				abstract: true,
			});
		indicatorsRoute($stateProvider, "siemis", "PRI", "PS", "PRI");
		indicatorsRoute($stateProvider, "siemis", "JS", "CHS", "JS");
		indicatorsRoute($stateProvider, "siemis", "SS", "PSS", "SEC");
		indicatorsRoute($stateProvider, "siemis", "SEC", "PSS", "SEC");

		// MIEMIS Routes
		$stateProvider
			.state('site.indicators.miemis', {
				url: '/miemis',
				abstract: true,
			});
		indicatorsRoute($stateProvider, "miemis", "PRI", "PRI", "PS");
		indicatorsRoute($stateProvider, "miemis", "SEC", "SEC", "SS");
		indicatorsRoute($stateProvider, "miemis", "ECE", "ECE", "ECE");

		// FedEMIS Routes
		$stateProvider
			.state('site.indicators.fedemis', {
				url: '/fedemis',
				abstract: true,
			});
		indicatorsRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE");
		indicatorsRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI");
		indicatorsRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC");
		// state specific routes
		indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE", "PNI");
		indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "PNI");
		indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "PNI");

		indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE", "YAP");
		indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "YAP");
		indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "YAP");

		indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE", "KSA");
		indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "KSA");
		indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "KSA");

		indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE", "CHK");
		indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "CHK");
		indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "CHK");

		// PalEMIS Routes
		$stateProvider
			.state('site.indicators.palemis', {
				url: '/palemis',
				abstract: true,
			});
		indicatorsRoute($stateProvider, "palemis", "ECE", "STECE", "ECE");
		indicatorsRoute($stateProvider, "palemis", "PRI", "STPS", "PRI");
		indicatorsRoute($stateProvider, "palemis", "SEC", "STSS", "SEC");

		$stateProvider
			.state('site.indicators.drill', {
				url: '/drill',
				params: { theModel: [], drill: "enrol" },
				views: {
					//"@": {
					//  //https://github.com/angular-ui/ui-router/wiki#alternative-ways-to-set-the-template
					//  templateUrl: function (stateParams) {
					//    return "indicators/drill/" + stateParams.drill;
					//  },
					//  controller: 'ModelController',
					//  controllerAs: "vm"
					//}
					"@": "indicatorsNerAudit"
				},
				resolve: {
					theModel: ['$stateParams', function ($stateParams) {
						return $stateParams.theModel;
					}],

					table: ['reflator', (reflator: Sw.Api.IReflator) => {
						return reflator.get("api/warehouse/edLevelAge/nation?report").then(response => (<any>response.data));
					}],
				}
			});

		let state: ng.ui.IState = {
			url: '/teachers/{surveyYear}/{edLevelCode}/{districtCode}',
			params: {
				surveyYear: null, edLevelCode: null
				, districtCode: {
					value: null,
					squash: true
				}
			},
			views: {
				"@": "indicatorsTeacherExplore"
			},
			resolve: {
				surveyYear: ["$stateParams", (stateParams) => stateParams.surveyYear],
				edLevelCode: ["$stateParams", (stateParams) => stateParams.edLevelCode],
				districtCode: ["$stateParams", (stateParams) => stateParams.districtCode],
				indicatorCalc: ["IndicatorsMgr", "$stateParams", (mgr, stateParams) => mgr.getCalculator(stateParams.districtCode)],

				tables: ['reflator', '$q', "$stateParams",
					(reflator: Sw.Api.IReflator, q: ng.IQService, stateParams) => {
						let endpt1 = "api/warehouse/teacheredlevel/nation"
						let endpt2 = "api/warehouse/enrol/nation?byAge";
						if (stateParams.districtCode) {
							endpt1 = `api/warehouse/teacheredlevel/district/${stateParams.districtCode}`;
							endpt2 = `api/warehouse/enrol/district/${stateParams.districtCode}?byAge`;
						}
						let ept = [endpt1, endpt2];
						// make a single resolve
						return q.all(ept.map(ep => reflator.get(ep).then(response => (<any>response.data))));
					}]
			}
		};
		$stateProvider.state("site.indicators.teachers", state);

		state = {
			url: '/repeaters/{surveyYear}/{edLevelCode}/{districtCode}',
			params: {
				surveyYear: null, edLevelCode: null,
				districtCode: {
					value: "",
					squash: true
				}
			},
			views: {
				"@": "indicatorsRepeaterExplore"
			},
			resolve: {
				surveyYear: ["$stateParams", (stateParams) => stateParams.surveyYear],
				edLevelCode: ["$stateParams", (stateParams) => stateParams.edLevelCode],
				districtCode: ["$stateParams", (stateParams) => stateParams.districtCode],
				indicatorCalc: ["IndicatorsMgr", "$stateParams", (mgr, stateParams) => mgr.getCalculator(stateParams.districtCode)],
				pageTitle: ["pageTitle", "$stateParams", "Lookups", (title, stateParams, lookups: Sw.Lookups.LookupService) => {
					let edLevelName = lookups.byCode("educationLevels", stateParams.edLevelCode, "N") || stateParams.edLevelCode;
					return `${title}: ${edLevelName} Repeaters`;
				}],
				tables: ['reflator', '$q', "$stateParams",
					(reflator: Sw.Api.IReflator, q: ng.IQService, stateParams) => {

						let endpt1 = "api/warehouse/enrol/nation?byAge=false";
						// endpoint for drill down into ClassLevel by school - suppress Age to reduce download
						let endpt2 = `api/warehouse/enrol/school?byAge=false`;
						if (stateParams.districtCode) {
							endpt1 = `api/warehouse/enrol/district/${stateParams.districtCode}?byAge=false`;
							endpt2 = `api/warehouse/enrol/school/${stateParams.districtCode}?byAge=false`;
						}
						let ept = [endpt1, endpt2];
						// make a single resolve
						return q.all(ept.map(ep => reflator.get(ep).then(response => (<any>response.data))));
					}]
			}
		};
		$stateProvider.state("site.indicators.repeater", state);

		//----------------------------------------------------------------
		// GER NER drilldown
		//----------------------------------------------------------------
		state = {
			url: '/ger/{surveyYear:int}/{edLevelCode}/{districtCode}',
			params: {
				surveyYear: null, edLevelCode: null,
				districtCode: {
					value: "",
					squash: true
				}
			},
			views: {
				"@": "indicatorsGerExplore"
			},
			resolve: {
				surveyYear: ["$stateParams", (stateParams) => stateParams.surveyYear],
				edLevelCode: ["$stateParams", (stateParams) => stateParams.edLevelCode],
				districtCode: ["$stateParams", (stateParams) => stateParams.districtCode],
				indicatorCalc: ["IndicatorsMgr", "$stateParams", (mgr, stateParams) => mgr.getCalculator(stateParams.districtCode)],
				pageTitle: ["pageTitle", "$stateParams", "Lookups", (title, stateParams, lookups: Sw.Lookups.LookupService) => {
					let edLevelName = lookups.byCode("educationLevels", stateParams.edLevelCode, "N") || stateParams.edLevelCode;
					return `${title}: ${edLevelName} GER`;
				}],

				tables: ['reflator', '$q', "$stateParams",
					(reflator: Sw.Api.IReflator, q: ng.IQService, stateParams) => {
						// overall level 
						let endpt1 = "api/warehouse/enrol/nation?byAge";
						// for school drilldown
						let endpt2 = `api/warehouse/enrol/school?byAge&year=${stateParams.surveyYear}`;
						if (stateParams.districtCode) {
							endpt1 = `api/warehouse/enrol/district/${stateParams.districtCode}?byAge`;
							endpt2 = `api/warehouse/enrol/school/${stateParams.districtCode}?byAge&year=${stateParams.surveyYear}`;
						}
						let ept = [endpt1, endpt2];
						// map the endpt2 to promises, make a single resolve
						return q.all(ept.map(ep => reflator.get(ep).then(response => (<any>response.data))));

					}]
			}
		};


		$stateProvider.state("site.indicators.ger", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.indicators.ger",
			{
				url: '/ner/{surveyYear}/{edLevelCode}/{districtCode}',
				views: {
					"@": "indicatorsNerExplore"
				},
				resolve: {
					pageTitle: ["pageTitle", "$stateParams", "Lookups", (title, stateParams, lookups: Sw.Lookups.LookupService) => {
						let edLevelName = lookups.byCode("educationLevels", stateParams.edLevelCode, "N") || stateParams.edLevelCode;
						return `${title}: ${edLevelName} NER`;
					}]
				}
			});
		$stateProvider.state("site.indicators.ner", state);

		//GIR NIR drilldown
		state = {
			url: '/gir/{surveyYear:int}/{edLevelCode}/{districtCode}',
			params: {
				surveyYear: null, edLevelCode: null,
				districtCode: {
					value: "",
					squash: true
				}
			},
			views: {
				"@": "indicatorsGirExplore"
			},
			resolve: {
				surveyYear: ["$stateParams", (stateParams) => stateParams.surveyYear],
				edLevelCode: ["$stateParams", (stateParams) => stateParams.edLevelCode],
				districtCode: ["$stateParams", (stateParams) => stateParams.districtCode],
				indicatorCalc: ["IndicatorsMgr", "$stateParams", (mgr, stateParams) => mgr.getCalculator(stateParams.districtCode)],
				pageTitle: ["pageTitle", "$stateParams", "Lookups", (title, stateParams, lookups: Sw.Lookups.LookupService) => {
					let edLevelName = lookups.byCode("educationLevels", stateParams.edLevelCode, "N") || stateParams.edLevelCode;
					return `${title}: ${edLevelName} GIR`;
				}],
				/// tables differ from those for ger / ner
				tables: ['reflator', '$q', "$stateParams",
					(reflator: Sw.Api.IReflator, q: ng.IQService, stateParams) => {
						let endpt1 = "api/warehouse/enrol/nation?byAge";
						let endpt2 = `api/warehouse/enrol/school?year=${stateParams.surveyYear}&byAge&byClassLevel`;
						if (stateParams.districtCode) {
							endpt1 = `api/warehouse/enrol/district/${stateParams.districtCode}?byAge`;
							endpt2 = `api/warehouse/enrol/list/school?
startYear=${stateParams.surveyYear}&edLevelCode=${stateParams.edLevelCode}`;
						}
						let ept = [endpt1, endpt2];
						// make a single resolve
						return q.all(ept.map(ep => reflator.get(ep).then(response => (<any>response.data))));
					}]
			}
		};
		$stateProvider.state("site.indicators.gir", state);

		// clone gir to make nir
		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.indicators.gir",
			{
				url: '/nir/{surveyYear}/{edLevelCode}/{districtCode}',
				views: {
					"@": "indicatorsNirExplore"
				},
				resolve: {
					pageTitle: ["pageTitle", "$stateParams", "Lookups", (title, stateParams, lookups: Sw.Lookups.LookupService) => {
						let edLevelName = lookups.byCode("educationLevels", stateParams.edLevelCode, "N") || stateParams.edLevelCode;
						return `${title}: ${edLevelName} NIR`;
					}]
				}
			});
		$stateProvider.state("site.indicators.nir", state);

		// clone gir to make survival
		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.indicators.gir",
			{
				url: '/survival/{surveyYear:int}/{edLevelCode}/{districtCode}',
				views: {
					"@": "indicatorsSurvivalExplore"
				}
			});
		$stateProvider.state("site.indicators.survival", state);

		$stateProvider.state("site.indicators.reports", {
			url: "^/indicators/reports",
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "Indicators", // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always",
				pageTitle: ["pageTitle", (title) => `{title} Reports`]
			}
		});


		$stateProvider.state("site.indicators.dashboard", {
			url: "^/indicators/dashboard",
			views: {
				"@": "indicatorDashboard"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
			}
		});

		$stateProvider
			.state('site.admin', {
				url: '/admin',
				resolve: {
					featureTitle: RouteHelper.featureTitle("Admin")
				},
				abstract: true,
			});

		$stateProvider
			.state('site.admin.warehouse', {
				url: '/warehouse',
				data: {
					permissions: {
						only: 'SurveyAdmin'
					}
				},
				views: {
					"@": {
						template: `<component-warehouse-admin></component-warehouse-admin>`
					}
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Warehouse")
				}
			});

		$stateProvider
			.state('site.admin.uissurvey', {
				url: '^/uis',

				views: {
					"@": "uisSurveyComponent"
				},
				resolve: {
					pageTitle: RouteHelper.featureTitle("UIS Survey")
				}
			});

		$stateProvider
			.state('site.indicators.dataexport', {
				url: '^/indicators/dataexport',

				views: {
					"@": "indicatorsDataExportComponent"
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Data Export")
				}
			});

	};

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])
}
