namespace Pineapples.Schools {

	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'schNo',
				name: 'School ID',
				displayName: 'School ID',
				headerCellFilter: 'vocab',
				editable: false,
				width: 80,
				pinnedLeft: true,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item")
			},
			{
				field: 'schName',
				name: 'Name',
				editable: false,
				width: 180,
				pinnedLeft: true
			},
			{
				field: 'schType',
				name: 'schType',
				displayName: 'School Type',
				headerCellFilter: 'vocab',
				editable: false,
				width: 105,
				pinnedLeft: true,
				lookup: 'schoolTypes',
			},
			{
				field: 'schClass',
				name: 'Class',
				displayName: 'School Class',
				headerCellFilter: 'vocab',
				editable: false,
				width: 100,
				pinnedLeft: true,
				lookup: 'schoolClasses',
			},
			{
				field: 'schAuth',
				name: 'auth',
				displayName: 'Authority',
				editable: false,
				pinnedLeft: true,
				lookup: 'authorities',
				width: 80
			},
		]
	};

	let modes = [
		{
			key: "Location",
			columnSet: 0,
			gridOptions: {
				columnDefs: [
					{
						field: 'iName',
						name: 'island',
						displayName: 'Island',
						headerCellFilter: 'vocab',
						cellClass: 'gdAlignLeft'
					},
					// not sure why this field alone was editable?
					//{
					//  field: 'dID',
					//  name: 'district',
					//  displayName: 'District',
					//  headerCellFilter:'vocab',
					//  cellClass: 'gdAlignLeft',
					//  enableCellEdit: true,
					//  editType: 'dropdown',
					//  lookup: 'districts',
					//  editableCellTemplate: 'ui-grid/dropdownEditor',
					//  editDropdownIdLabel: 'C',
					//  editDropdownValueLabel: 'N'
					//},
					{
						field: 'dName',
						name: 'dName',
						displayName: 'District',
						headerCellFilter: 'vocab',
						cellClass: 'gdAlignLeft'
					},
					{
						field: 'schVillage',
						name: 'village',
						displayName: 'Village',
						cellClass: 'gdAlignLeft'
					},
					{
						field: 'schLat',
						name: 'LatLng',
						displayName: 'Latitude/Longitude',
						cellTemplate: '<div class="ui-grid-cell-contents"><a  ng-click="grid.appScope.vm.action(\'latlng\',\'schNo\', row.entity);">[{{row.entity.schLat | number:4}}, {{row.entity.schLong |number:4}}]</a></div>',
						cellClass: "gdAction"
					},
				]
			}
		},
		{
			key: "Latest Survey",
			columnSet: 1,
			gridOptions: {
				columnDefs: [
					{
						field: 'svyYear',
						name: 'svyYear',
						displayName: 'Survey Year',
						cellTemplate: `
							<div class="ui-grid-cell-contents">
								<a ui-sref="site.surveys.item(
									{schoolNo: '{{row.entity.schNo}}',
										year:{{row.entity.svyYear}}
								})">{{row.entity.svyYear}}</a>
							</div>`
					},
					{
						field: 'ssEnrol',
						name: 'ssEnrol',
						displayName: 'Enrol',
						cellClass: 'gdAlignRight'
					},
					{
						field: 'ssEnrolM',
						name: 'ssEnrolM',
						displayName: 'Enrol Male',
						cellClass: 'gdAlignRight'
					},
					{
						field: 'ssEnrolF',
						name: 'ssEnrolF',
						displayName: 'Enrol Female',
						cellClass: 'gdAlignRight'
					},
					{
						field: 'TNum',
						name: 'TNum',
						displayName: 'Teachers',
						cellClass: 'gdAlignRight'
					},
					{
						field: 'ssSource',
						name: 'classinfo',
						displayName: '',
						width: 60,
						cellClass: 'gdAlignRight',
						cellTemplate: `
							<div class="ui-grid-cell-contents">
								<span permission permission-only="'SurveyOps'" >
								<a ui-sref="site.schools.classinfo(
									{id: '{{row.entity.schNo}}',
										year:{{row.entity.svyYear}}
								})"><md-icon class="material-icons md-24 ng-scope" role="img" aria-label="search">search</md-icon></a>
								<md-tooltip>Open Class Explorer</md-tooltip>
								</span>
								
							</div>`
					},
					{
						field: 'ssSource',
						name: 'SurveySource',
						displayName: '',
						width: 60,
						cellClass: 'gdAlignRight',
						cellTemplate: `
							<div class="ui-grid-cell-contents gdAction" >
								<span permission permission-only="'SurveyOps'" >
									<document-image document = "row.entity.ssSource" height = "10" > </document-image>
								</span>
							</div>`
					}
				]         // columndefs
			}           // gridoptions
		},            // mode
		{
			key: 'Registration',
			columnSet: 2,
			editable: true,
			gridOptions: {
				columnDefs: [
					{
						field: 'schRegStatus',
						name: 'schRegStatus',
						displayName: 'Registration Status',
						editable: true,
						lookup: "schoolRegistration"

					},
					{
						field: 'schRegStatusDate',
						name: 'schRegStatusDate',
						displayName: 'Status Date',
						cellFilter: 'date:"dd-MMM-yyyy"',
						editable: true,
						type: 'date',
					},
					{
						field: 'schReg',
						name: 'schReg',
						displayName: 'Registration No',
						editable: true,
					},
					{
						fields: "SchEst",
						name: "schEst",
						displayName: "Estimate",
						editable: true,
						type: 'number',
					},
					{
						fields: "SchClosed",
						name: "schClosed",
						displayName: "Closed",
						editable: true,
						type: 'number',
					},

				]      // columndefs
			}        // gridoptions
		}          // mode
	];           // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};

	angular
		.module('pineapples')
		.run(['SchoolFilter', pushModes]);
}
