﻿namespace Pineapples.Dashboards {
	const ROW_LOOKUP = "regions";

	class Controller extends DashboardChild implements IDashboardChild {

		public dashboard: SchoolsDashboard;
		public chartData: Sw.Charts.AnnotatedGroup;
		public grpRegionCode: CrossFilter.Group<Enrolments.IxfData, string, any>;


		public onDashboardReady() {
			let xFilter = this.dashboard.xFilter;
			this.grpRegionCode = xFilter.xReduce(
				this.dashboard.dimRegionCode, "AuthorityCode", Enrolments.vaGendered);

			this.chartData = {
				group: this.grpRegionCode,
				columns: ["Female", "Male"],
				valueAccessor: (row, col) => row.value.Tot[col]
			};
		}

		public get highlight() {
			return this.options.selectedRegion;
		}

		public clickHandler(params) {
			this.options.toggleSelectedRegion(params.name);
		}

		public onChartRender(option, echarts, renderedType) {
			Sw.Charts.chartOps(option)
				.showLegend(renderedType != "pie" && (option.series.length > 1))
				.showPieLabels(renderedType == "pie")
				.minimalInk()
				.lookups(this.lookups)
				.translateLabels(ROW_LOOKUP, ["h", "hp", "hc"].indexOf(renderedType) >= 0);
		}

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			datum.item = this.lookups.byCode(ROW_LOOKUP, datum.item, "N");
		}

		private _allowedTypes = ["v", "vp", "h", "hp", "pie"];
		public get allowedTypes(): string[] {
			return this.isSelected() ? this._allowedTypes : undefined;
		}
	}

	angular
		.module("pineapples")
		.component("enrolmentRegionChart", new EnrolComponent(Controller, "EnrolmentRegionChart"));
}
