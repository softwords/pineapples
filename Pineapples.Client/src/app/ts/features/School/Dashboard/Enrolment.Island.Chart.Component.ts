﻿namespace Pineapples.Dashboards {
	const ROW_LOOKUP = "islands";

	class Controller extends DashboardChild implements IDashboardChild {

		public dashboard: SchoolsDashboard;
		public chartData: Sw.Charts.AnnotatedGroup;
		public grpIslandCode: CrossFilter.Group<Enrolments.IxfData, string, any>;


		public onDashboardReady() {
			let xFilter = this.dashboard.xFilter;
			this.grpIslandCode = xFilter.xReduce(
				this.dashboard.dimIslandCode, "AuthorityCode", Enrolments.vaGendered);

			this.chartData = {
				group: this.grpIslandCode,
				columns: ["Female", "Male"],
				valueAccessor: (row, col) => row.value.Tot[col]
			};
		}

		public get highlight() {
			return this.options.selectedIsland;
		}

		public clickHandler(params) {
			this.options.toggleSelectedIsland(params.name);
		}

		public onChartRender(option, echarts, renderedType) {
			Sw.Charts.chartOps(option)
				.showLegend(renderedType != "pie" && (option.series.length > 1))
				.showPieLabels(renderedType == "pie")
				.minimalInk()
				.lookups(this.lookups)
				.translateLabels(ROW_LOOKUP, ["h", "hp", "hc"].indexOf(renderedType) >= 0);
		}

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			datum.item = this.lookups.byCode(ROW_LOOKUP, datum.item, "N");
		}

		private _allowedTypes = ["v", "vp", "h", "hp", "pie"];
		public get allowedTypes(): string[] {
			return this.isSelected() ? this._allowedTypes : undefined;
		}
	}

	angular
		.module("pineapples")
		.component("enrolmentIslandChart", new EnrolComponent(Controller, "EnrolmentIslandChart"));
}
