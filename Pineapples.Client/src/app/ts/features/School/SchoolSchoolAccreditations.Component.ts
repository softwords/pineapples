﻿namespace Pineapples.Schools {

  interface IBindings {
    schoolaccreditations: any;
  }

  class Controller implements IBindings {
    public schoolaccreditations: any;

    static $inject = ['$state'];
    constructor(public $state: ng.ui.IStateService) {  }

    public $onChanges(changes) {
      if (changes.schoolaccreditations) {
        console.log('$onChanges: ', this.schoolaccreditations);
      }
    }

    public showSchoolAccreditation(id) {
      this.$state.go("site.schoolaccreditations.list.item", { id: id });
    }

  }

  class Component implements ng.IComponentOptions {
      public bindings = {
        schoolaccreditations: '<'
      };
      public controller = Controller;
      public controllerAs = "vm";
      public templateUrl = "school/schoolaccreditations";
  }

  angular
    .module("pineapples")
    .component("schoolAccreditations", new Component());
}