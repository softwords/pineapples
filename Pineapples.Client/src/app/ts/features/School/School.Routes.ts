// School Routes
module Pineappples.Schools {

	let RouteHelper = Sw.Utils.RouteHelper;

	let routes = function ($stateProvider) {
		var featurename = 'Schools';
		var filtername = 'SchoolFilter';
		var templatepath = "school";
		var tableOptions = "schoolFieldOptions";
		var url = "schools";
		var usersettings = null;
		var mapview = 'SchoolMapView';

		// root state for 'school' feature

		let state: ng.ui.IState = RouteHelper.frameState(featurename, filtername, templatepath, url, usersettings, tableOptions, mapview);

		// default 'api' in this feature is schoolsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "schoolsAPI";

		state.data = state.data || {};
		state.data["frameTitle"] = "Schools";
		state.data["icon"] = "location_city";
		state.data["permissions"] = {
			only: "SchoolRead"
		};

		let basestate = "site.schools";
		$stateProvider.state(basestate, state); // why not use addFeatureState?

		// state for a high level schools dashboard
		state = {
			url: '^/schools/dashboard',
			data: {
				permissions: {
					only: 'SchoolRead'
				},
				rendermode: "Dashboard"
			},
			views: {
				"renderarea": {
					component: "schoolsDashboard"
				},
				"searcher": "coreOptionsEditor"
			},
			resolve: {
				options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
					return new Pineapples.Dashboards.coreOptions(lookups, scope.hub);
				}],
				// resolve the calculator
				table: ['reflator', (reflator: Sw.Api.IReflator) => {
					return reflator.get("api/warehouse/tableEnrolX/r").then(response => (<any>response.data));
				}],
				pageTitle: RouteHelper.simpleTitle("Dashboard")
			}
		};
		$stateProvider.state("site.schools.dashboard", state);

		// TODO integrate into site.schools.list.item state
		// This route can stand; as the component can now work either standalone or inside 
		// school item (becuase the binding dependency is now reduced to 'school')
		state = {
			url: "/schooldashboard/{id}",

			views: {
				"@": "schoolDashboard"
			},
			resolve: {
				pageTitle: RouteHelper.simpleTitle("Dashboard"),
				school: ['schoolsAPI', '$stateParams', (api, $stateParams) => api.read($stateParams.id)]
			}
		};
		$stateProvider.state("site.schooldashboard", state);

		// List state
		state = RouteHelper.frameListState("schools", "schNo");
		let statename = `${basestate}.list`;
		$stateProvider.state(statename, state);

		// chart table and map
		RouteHelper.addChartState($stateProvider, featurename);
		RouteHelper.addTableState($stateProvider, featurename);
		RouteHelper.addMapState($stateProvider, featurename, mapview);

		// new - state with a custom url route
		state = {
			url: "^/schools/new",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'SchoolWriteX'
				}
			},
			views: {
				"actionpane@site.schools": "componentSchool"
			},
			resolve: {
				model: ['schoolsAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}]
			}
		};
		$stateProvider.state("site.schools.list.new", state);

		state = {
			url: "^/schools/reports",
			data: {
				permissions: {
					only: 'SchoolRead'
				}
			},
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "Schools",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always",
				pageTitle: RouteHelper.simpleTitle("Reports")
			}
		}
		$stateProvider.state("site.schools.reports", state);

		// item state
		// This is where I wanted the dashboards for individual schools (same goes for other entities) - GH
		state = {
			url: "^/schools/{id}",
			data: {
				permissions: {
					only: 'SchoolRead'
				}
			},
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.schools": {
					component: "componentSchool"
				}
			},
			// ui-router 1.x automatically binds the component bindings to a resolve of the same name
			// otherwise you can add a 'bindings' property to the state to map rsolve names to component inputs 
			//see
			//https://ui-router.github.io/guide/ng1/route-to-component
			resolve: {
				model: ['schoolsAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}],
				titleId: ['model', (model) => `${model.schName}`],
				pageTitle: RouteHelper.indirectTitle()
			}
		};
		$stateProvider.state("site.schools.list.item", state);

		// unique to schools
		state = {
			url: "^/schools/{id}/latlng",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.schools": "schoolLatLng"
			},
			resolve: {
				model: ['schoolsAPI', '$stateParams', (api, $stateParams) => api.read($stateParams.id)]
			}
		};
		$stateProvider.state("site.schools.list.latlng", state);

		state = {
			url: "^/schools/{id}/classinfo?{year:int}/{level}",
			params: {
				id: null, year: null, level: null
			},

			views: {
				"actionpane@site.schools": "schoolClassInfo"
			},
			resolve: {
				classInfo: ["$http", '$stateParams', (http: ng.IHttpService, $stateParams) =>
					http.get(`api/schools/${$stateParams.id}/classinfo`, {
						params: {
							year: $stateParams.year,
							level: $stateParams.level
						}
					})
						.then(result => result.data["ResultSet"])
				],
				pageTitle: ['rootPageTitle', '$stateParams', 'Lookups',
					(pageTitle, stateParams, lookups: Sw.Lookups.LookupService) =>
						`${pageTitle} * Class Explorer${stateParams.id ? ": " + lookups.byCode("schoolCodes", stateParams.id.toUpperCase(), "N") : ""}`]
			}
		};
		$stateProvider.state("site.schools.classinfo", state);

		$stateProvider
			.state('site.schools.dataexport', {
				url: '^/schools/dataexport',

				views: {
					"@": "schoolDataExportComponent"
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Data Export")
				}
			});

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes]);
}
