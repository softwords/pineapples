﻿namespace Pineapples.Schools {

	// don;t need to subclass the controller 
	// optional controller argument is not supplied to FindConfigComponent, so the base is used
	//class Controller extends Sw.Components.FindConfigOperations {
	//}

	angular
		.module("pineapples")
		.component("schoolSearcherComponent"
			, new Sw.Components.FindConfigComponent("school/SearcherComponent"));
}