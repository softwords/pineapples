﻿namespace Pineapples.Dashboards {

	export namespace School {

		// school data has this format
		export interface AgedEnrolment {
			SurveyYear: number,
			ClassLevel: string,
			Age: number,
			EnrolM: number,
			EnrolF: number,
			Enrol: number
		}

		export interface AgedEnrolment_Years {
			[year: number]: AgedEnrolment[];
		}

		// district data has this format - age is not disaggregated
		export interface ClassLevelEnrolmentDistrict {
			SurveyYear: number,
			ClassLevel: string,
			EnrolM: number,
			EnrolF: number,
			Enrol: number,
			DistrictCode: string
		}

		export interface ClassLevelEnrolmentDistrict_Years {
			[year: number]: ClassLevelEnrolmentDistrict[];
		}
		// calculated nation data has this format - age is not disaggregated
		export interface ClassLevelEnrolment {
			SurveyYear: number,
			ClassLevel: string,
			EnrolM: number,
			EnrolF: number,
			Enrol: number
		}

		export interface ClassLevelEnrolment_Years {
			[year: number]: ClassLevelEnrolment[];
		}
	}

	export class SchoolDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

		// bindings
		public school: Pineapples.Schools.School;
		// Set temporary options fixed instead of the whole Options stuff
		// this will move out into a SchoolExamsDashboard eventually where options are more relevant
		public options: any = {};

		public rawEnrolment: School.AgedEnrolment[];           // bound to name 'enrolment'
		public districtEnrolment: any;

		public schoolTeacherCount: any;
		public schoolFlow: any;

		// holds the school enrolment by year
		public enrolment: School.AgedEnrolment_Years;

		private colors: Array<string> = ['#FF6666', '#FFB266', '#FFFF66', '#B2FF66', '#66FF66', '#66FFB2', '#66FFFF', '#66B2FF', '#6666FF', '#B266FF', '#FF66FF', '#FF66B2', 'C00000', '#C06600', 'CCCC00', '66DD00'];

		public schoolNo() {
			return this.school._id();
		}

		public dataForFlowBarChart;
		public dataForBarChart;
		public dataForLineChart;

		public lastSurveyYear = () => this.school.LatestSurvey.svyYear;
		public lastWarehouseYear = () => this.school.LatestSurvey.svyYear;  // this will get updated when actual warehouse data is available

		public surveyYearArray = (numYears: number) => _.range(this.lastWarehouseYear() - numYears + 1, this.lastWarehouseYear() + 1);
		// flow data is always one year behind the current survey
		public lastFlowYear = () => this.lastWarehouseYear() - 1;
		public flowYearArray = (numYears: number) => _.range(this.lastFlowYear() - numYears + 1, this.lastFlowYear() + 1);

		public gradesForSchool;
		public dataForSchoolFlowLineChart;

		// this is meta
		public schoolDistrictNationMeta = ["School", "District", "Nation"];

		private national = (year) => x => x.filter(_.matchesProperty('SurveyYear', year))
		private district = (districtCode, year) => x => x.filter(_.matchesProperty('SurveyYear', year))
			.filter(_.matchesProperty('DistrictCode', districtCode));

		private classLevels;

		public dataForFemalePercentBarChart; // to be initialised in $onChanges

		public dataForStackedGenderHistory;

		private _classSorter = x => this.lookups.byCode("levels", x.ClassLevel, "YoEd");

		public enrol: any; // = _.memoize((year: number) => _.sortBy(this.enrolment[year]), this._classSorter);

		public enrolmentYears;

		// retrieve the grade from the year of ed
		// this may be dependent upon school type
		public grade = yearOfEd => {
			let types = (<any>this.lookups.cache["schoolTypeLevels"])
				.filter(({ YoEd, ST }) => (YoEd === yearOfEd && ST === this.school["schType"]));
			if (types.length) {
				return types[0]["L"];					// return the level code
			}
			// this school has a year of ed not compatible with its type
			// TO DO - full treatment should check default EducationPath to get this grade
			console.log("Using default grade for YoEd = ", yearOfEd);
			return (<any>this.lookups.cache["levels"])
				.filter(({ YoEd }) => (YoEd === yearOfEd))[0]["C"];

		};

		public schoolFlowData;

		public dataForFemalePercentHistoryLineChart;

		public schoolFlowDataValue = (schNo, year, grade, indicator) => {
			const data = this.schoolFlowData(schNo)[`${year}:${grade}`];
			return data && data[indicator] && `${(Math.round(data[indicator] * 10) / 10.0)}`;
		}

		//----------------------- Life Cycle hooks ----------------------------------
		public $onInit() {
			// While we can collect data onInit we can not rely on data arriving in time
			// to be part of the whole life cycle of dashboard->dashboard child components
			// like we have with the global dashboards (e.g. Schools, Teachers, etc.)
			// Instead with individual entity dashboards (e.g. School, Teacher) we collect data and 
			// check for in in the onChanges of dashboard childs
			console.log("SchoolDashboard $onInit()");
			this._collectData(this.schoolNo());
		}

		public $onChanges(changes) {
			console.log("SchoolDashboard $onChanges()");
		}
		//----------------------- Life Cycle hooks ----------------------------------

		private _collectData(schNo) {
			// this code is migrated from resolves in dashboard.routes, (line 23 ff) which was removed in commmit
			// fdd4739 (fdd4739fe58e2ac14f3a2e99a7c1b4f390d723d1) 22-03-2019

			// api/schools is not needed now - passed in from School Component
			// although enrolments are available in the school object, 
			// leave this if we want to get district or national averages

			let pEnrol = this.reflator.get(`api/warehouse/enrol/school/${schNo}?report`);
			let pDistrict = this.reflator.get(`api/warehouse/enrol/district?report`);
			let pTeacher = this.reflator.get(`api/warehouse/schoolteachercount/${schNo}`);
			let pFlow = this.reflator.get(`api/warehouse/flow/school/${schNo}?report&asperc`);

			// use $q.all so we only proceeed to construct the various data shapes 
			// when all the data is available
			this.$q.all({ pEnrol, pDistrict, pTeacher, pFlow })
				.then(response => {
					this.rawEnrolment = response.pEnrol.data;
					this.districtEnrolment = response.pDistrict.data;
					this.schoolTeacherCount = response.pTeacher.data;
					this.schoolFlow = response.pFlow.data;
					this.rebuild();
				});
		}
		// sequencing issues mean that functions bound to child components are called before the enrolments are populated
		// so these initialisations have to be moved so that they are called when enrolment changes
		private rebuild() {

			this.enrolment = _(this.rawEnrolment).groupBy((x: any) => [x.SurveyYear, x.ClassLevel])
				.map(val => _(val).reduce((acc: any, vv: any) => ({
					SurveyYear: vv.SurveyYear,			// handle change of case here for compatibility with change in datalayer
					ClassLevel: vv.ClassLevel,
					EnrolF: (acc.EnrolF || 0) + vv.EnrolF,
					EnrolM: (acc.EnrolM || 0) + vv.EnrolM,
					Enrol: (acc.Enrol || 0) + vv.Enrol
				}), {}
				))
				.groupBy(x => x.SurveyYear).value()


			this.enrol = _.memoize((year: number) => _.sortBy(this.enrolment[year], this._classSorter)); // fixes issue 556
			this.enrolmentYears = () => _.keys(this.enrolment);
			// move this here to close down a whole lot of console errors
			this.lastWarehouseYear = () => <number>_.max(this.enrolmentYears());

			const schoolDistrict = this.lookups.byCode("islands", this.school["iCode"], "D"); // extract the district value from the lookup table
			const schoolDistrictName = this.lookups.byCode("districts", schoolDistrict, "N"); // extract the district value from the lookup table

			this.schoolDistrictNationMeta = [this.school.schName, schoolDistrictName, "National"];


			// returned data is now in this format, just needs filtering
			const districtEnrolmentByClass: School.ClassLevelEnrolmentDistrict_Years = _(this.districtEnrolment)
				.filter(({ DistrictCode }) => (DistrictCode == schoolDistrict))
				.groupBy(x => x.SurveyYear)
				.value();

			const nationEnrolmentByClass = _(this.districtEnrolment)
				.groupBy((x: any) => [x.SurveyYear, x.ClassLevel])
				.map(val => _(val).reduce((acc: any, vv: any) => ({
					SurveyYear: vv.SurveyYear,			// handle change of case here for compatibility with change in datalayer
					ClassLevel: vv.ClassLevel,
					EnrolF: (acc.EnrolF || 0) + vv.EnrolF,
					EnrolM: (acc.EnrolM || 0) + vv.EnrolM,
					Enrol: (acc.Enrol || 0) + vv.Enrol
				}), {}
				))
				.groupBy(x => x.SurveyYear)
				.value();

			this.classLevels = (year) => _(this.enrol(year)).map('ClassLevel').value();



			const schoolEnrolmentYearTotals = _(this.rawEnrolment)
				.groupBy((x: any) => [x.SurveyYear])
				.map(val => _(val).reduce((acc: any, vv: any) => ({
					SurveyYear: vv.SurveyYear,			// handle change of case here for compatibility with change in datalayer
					EnrolF: (acc.EnrolF || 0) + vv.EnrolF,
					EnrolM: (acc.EnrolM || 0) + vv.EnrolM,
					Enrol: (acc.Enrol || 0) + vv.Enrol
				}), {}
				))
				.groupBy(x => x.SurveyYear).value()

			const districtEnrolmentYearTotals = _(this.districtEnrolment)
				.filter(({ DistrictCode }) => (DistrictCode == schoolDistrict))
				.groupBy((x: any) => [x.SurveyYear])
				.map(val => _(val).reduce((acc: any, vv: any) => ({
					SurveyYear: vv.SurveyYear,			// handle change of case here for compatibility with change in datalayer
					EnrolF: (acc.EnrolF || 0) + vv.EnrolF,
					EnrolM: (acc.EnrolM || 0) + vv.EnrolM,
					Enrol: (acc.Enrol || 0) + vv.Enrol
				}), {}
				))
				.groupBy(x => x.SurveyYear)
				.value();

			const nationEnrolmentYearTotals = _(this.districtEnrolment)
				.groupBy((x: any) => [x.SurveyYear])
				.map(val => _(val).reduce((acc: any, vv: any) => ({
					SurveyYear: vv.SurveyYear,			// handle change of case here for compatibility with change in datalayer
					EnrolF: (acc.EnrolF || 0) + vv.EnrolF,
					EnrolM: (acc.EnrolM || 0) + vv.EnrolM,
					Enrol: (acc.Enrol || 0) + vv.Enrol
				}), {}
				))
				.groupBy(x => x.SurveyYear)
				.value();

			this.dataForBarChart = _.memoize((year) => [         // Will need to clear memoize cache for all memoized funcs in $onChange if .enrolment is updated
				this.enrol(year).map(({ ClassLevel, EnrolF: Enrol }) => ({ ClassLevel, Enrol })),
				this.enrol(year).map(({ ClassLevel, EnrolM: Enrol }) => ({ ClassLevel, Enrol }))
			]);


			this.dataForLineChart = _.memoize(() => {
				const colors = ["#34b24c", "#ffa500", "#551a8b"];
				const meta = ['Female', 'Male', 'Total'];

				// _.assign is equivalent to angular.extend
				// _.merge is a deep copy , like angular.merge
				const addObjs = (a, b) => (<any>_.assign)(

					// spread operator here is typescript shorthand to use the array as the arguments to the function
					// the resulting javascript is _.assign.apply(_, Object.keys(b).map(etc ect))
					...Object.keys(b).map(
						// get an array of all keys in b, 
						// then map this array of strings to an array of objects.
						// For each key in b, the resulting object has that same key, and its value is the sum of the [key] value in b and a
						// we end up with an array of object each with one key, and and keys are distinct:
						// [{a:1}, {b:3}, {c:5}]
						// _.assign pulls them back to to a single object
						// {a:1, b:3, c:5}
						key => ({ [key]: (a[key] || 0) + (b[key] || 0) })
					)
				);

				// lodash 'implicit object wrapper' is used to create a 'fluent' syntax
				// allowing data transformation using a pipeline of lodash operations
				const totals = _(this.enrolment)
					//https://lodash.com/docs/4.17.11#mapValues
					// this.enrolment is an object with a key for each year. The value of each key is a collection of the rows for that year
					// the elements of this colection are passed to mapValues
					.mapValues(y => _(y)
						.map(({ EnrolF, EnrolM }) => ({ Female: EnrolF, Male: EnrolM, Total: EnrolF + EnrolM }))
						.reduce(addObjs, {}))
					.value()

				return _(meta)
					.map(g => _(totals).toPairs()
						.map(x => ({ year: parseInt(x[0]), enrol: x[1][g] }))
						.value())
					.map((dataset, i) => ({ dataset, meta: meta[i], color: colors[i] }))
					.value();
			}, () => this.enrolment);

			this.dataForFemalePercentBarChart = _.memoize((year) => [
				// School Dataset
				_(this.enrol(year))
					.map(({ ClassLevel, EnrolF, EnrolM }) => ({ ClassLevel, Ratio: _.round(100 * EnrolF / (EnrolF + EnrolM), 1) }))
					// the sort by order of class levels can be retrieved from the yearofeducation
					.sortBy(({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel, "YoEd"))
					.value(),

				// District Dataset
				_(districtEnrolmentByClass[year])
					.filter(({ ClassLevel }) => (this.classLevels(year).indexOf(ClassLevel) !== -1))
					.map(({ ClassLevel, EnrolF, EnrolM, Enrol }) => ({ ClassLevel, Ratio: _.round(100 * EnrolF / Enrol, 1) }))
					.value(),

				// National Dataset
				_(nationEnrolmentByClass[year])
					.filter(({ ClassLevel }) => (this.classLevels(year).indexOf(ClassLevel) !== -1))
					.map(({ ClassLevel, EnrolF, EnrolM, Enrol }) => ({ ClassLevel, Ratio: _.round(100 * EnrolF / Enrol, 1) }))
					.value(),
			]);


			this.schoolFlowData = _.memoize(
				(schNo) => _(this.schoolFlow)
					//.filter(_.matchesProperty('SchoolNo', schNo)) FlowSchoolR is filtered on school when read
					.map(x => _.assign({}, x, { grade: this.grade(x.YearOfEd) }))
					.groupBy(({ SurveyYear, grade }) => `${SurveyYear}:${grade}`)
					.mapValues(_.first)
					.value()
			);

			// TO DO: get this from metaSchoolTypesLevelMap? for now, adapt the existing logic which filters
			// all possible levels based on what is found in schoolflowdata
			this.gradesForSchool = _.memoize(
				(schNo) => _.map(this.lookups.cache["levels"], "C")
					.filter(x => _(this.schoolFlowData(schNo)).values().map(_.property('grade')).uniq().sort().value().indexOf(x) != -1)
			);

			this.dataForSchoolFlowLineChart = _.memoize(
				(indicator) =>
					_(this.gradesForSchool(this.school._id()))
						.map((grade, i) => (
							{
								meta: grade,
								color: this.colors[i],
								dataset: this.flowYearArray(5).map(year => (
									{
										year,
										value: Number(this.schoolFlowDataValue(this.school._id(), year, grade, indicator)) || 0
									}
								))
							}
						)).value()
			);


			this.dataForFemalePercentHistoryLineChart = _.memoize(() => {
				const schoolDataset = _(<any>schoolEnrolmentYearTotals)
					// step through the SurveyYear properties, return the array, then the first element of that array
					// this "unnests" the data to return a flat array again
					.map(x => x).map(x => x[0])
					// then process the resulting array
					.map(({ SurveyYear, EnrolF, Enrol }) => ({ year: SurveyYear, Ratio: _.round(100 * EnrolF / Enrol, 1) }))
					.value();

				const years = schoolDataset.map(x => x.year);
				const keepYear = ({ SurveyYear }) => _.includes(years, SurveyYear);
				return [
					{
						meta: this.school.schName,
						color: "#34b24c",
						dataset: schoolDataset
					},
					{
						meta: schoolDistrictName,
						color: "#ffa500",
						dataset:
							_(<any>districtEnrolmentYearTotals)
								.map(x => x).map(x => x[0])		// flatten, as above
								.pickBy(<any>keepYear)			// remove unwanted years
								.map(({ SurveyYear, EnrolF, Enrol }) => ({ year: SurveyYear, Ratio: _.round(100 * EnrolF / Enrol, 1) }))
								.value()
					},
					{
						meta: 'National',
						color: "#551a8b",
						dataset:
							_(<any>nationEnrolmentYearTotals)
								.map(x => x).map(x => x[0])		// flatten, as above
								.pickBy(<any>keepYear)
								.map(({ SurveyYear, EnrolF, Enrol }) => ({ year: SurveyYear, Ratio: _.round(100 * EnrolF / Enrol, 1) }))
								.value()
					}
				];
			});

			this.dataForStackedGenderHistory = _.memoize(() => {
				const colors = ["#34b24c", "#ffa500"];
				const meta = ['Female', 'Male'];

				const addObjs = (a, b) => (<any>_.assign)(
					...Object.keys(b).map(
						key => ({ [key]: (a[key] || 0) + (b[key] || 0) })
					)
				);

				const totals = _(this.enrolment)
					.mapValues(y => _(y)
						.map(({ EnrolF, EnrolM }) => ({ Female: EnrolF, Male: EnrolM }))
						.reduce(addObjs, {}))
					.value()

				return _(meta)
					.map(g => _(totals).toPairs()
						.map(x => ({ year: parseInt(x[0]), enrol: x[1][g] }))
						.value())
					.map((dataset, i) => ({ dataset, meta: meta[i], color: colors[i] }))
					.value();
			}, () => this.enrolment);

			this.dataForFlowBarChart = _.memoize(
				(schNo, year, indicator) => [
					_.map(
						this.gradesForSchool(schNo),
						(grade) => ({
							ClassLevel: grade,
							Value: this.schoolFlowDataValue(schNo, year, grade, indicator)
						}
						)),
				],
				(schNo, year, indicator) => `${schNo} - ${year} - ${indicator}`
			);
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			school: "<"
		};
		public controller: any = SchoolDashboard;
		public controllerAs: string = "vm";
		public templateUrl: string = `school/SchoolDashboard`;
	}

	angular
		.module("pineapples")
		.component("schoolDashboard", new Component());
}