﻿// Exams Routes
namespace Pineappples.Exams {

	let RouteHelper = Sw.Utils.RouteHelper;
	let routes = function ($stateProvider) {
		var featurename = 'Exams';
		var filtername = 'ExamFilter';
		var templatepath = "exam";
		var tableOptions = "examFieldOptions";
		var url = "exams";
		var usersettings = null;
		//var mapview = 'ExamMapView';
		let state: ng.ui.IState = RouteHelper.frameState(featurename, filtername, templatepath);

		// default 'api' in this feature is examsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "examsAPI";

		state.data = state.data || {};
		state.data["frameTitle"] = "Exams";
		state.data["icon"] = "assignment_turned_in";
		state.data["permissions"] = {
			only: 'ExamRead'
		};
		let basestate = "site.exams";
		$stateProvider.state(basestate, state);

		// List State
		state = RouteHelper.frameListState("exam", "exID");
		let statename = `${basestate}.list`;
		$stateProvider.state(statename, state);

		// Reload state
		state = {
			url: '^/exams/-',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.removeAll();
				$state.reload();
			}]
		};
		statename = "site.exams.reload";
		$stateProvider.state(statename, state);

		// chart, table and map
		RouteHelper.addChartState($stateProvider, featurename);
		RouteHelper.addTableState($stateProvider, featurename);
		RouteHelper.addMapState($stateProvider, featurename); // , mapview

		// new - state with a custom url route
		state = {
			url: "^/exams/new",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'ExamWrite'
				}
			},
			views: {
				"actionpane@site.exams": {
					component: "componentExam"
				}
			},
			resolve: {
				model: ['examsAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}]
			}
		};
		$stateProvider.state("site.exams.list.new", state);

		state = {
			url: "/reports",
			data: {
				permissions: {
					only: 'ExamRead'
				}
			},
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "Exams",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always",
				pageTitle: RouteHelper.simpleTitle("Reports")
			}
		}
		$stateProvider.state("site.exams.reports", state);

		// state for a high level exams dashboard
		state = {
			url: "/dashboard",
			data: {
				permissions: {
					only: 'ExamRead'
				},
				rendermode: "Dashboard"
			},
			views: {
				"renderarea": {
					component: "examsDashboard"
				},
				"searcher": "examOptionsEditor"
			},
			resolve: {
				options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
					return new Pineapples.Dashboards.examOptions(lookups, scope.hub);
				}],
				table: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
					// overall level 
					let p = reflator.get("api/warehouse/exams/tablex")
						.then(response => (<any>response.data));
					// legacy endpoint now removed?
					//let p2 = reflator.get("api/warehouse/examsdistrictresults").then(response => (<any>response.data));
					// use $q.all to return both sets in an array
					return q.all([p]);
				}],
				pageTitle: RouteHelper.simpleTitle("Dashboard")
			}
		}
		$stateProvider.state("site.exams.dashboard", state);

		// item state
		//$resolve is intriduced in ui-route 0.3.1
		// it allows bindings from the resolve directly into the template
		// injections are replaced with bindings when using a component like this
		// the component definition takes care of its required injections - and its controller
		state = {
			url: "^/exams/{id}",
			data: {
				permissions: {
					only: 'ExamAdmin'
				}
			},
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.exams": {
					component: "componentExam"
				}
			},
			resolve: {
				model: ['examsAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}],
				titleId: ["model", (model) => `${model.exCode} ${model.exYear}`],
				pageTitle: RouteHelper.indirectTitle()
			}
		};
		$stateProvider.state("site.exams.list.item", state);

		state = {
			url: "^/exams/upload",
			data: {
				permissions: {
					only: 'ExamWrite'
				}
			},
			views: {
				"@": {
					component: "examUploadComponent"
				}
			},
			resolve: {
				pageTitle: RouteHelper.simpleTitle("Upload")
			}
		};
		$stateProvider.state("site.exams.upload", state);

		$stateProvider
			.state('site.exams.dataexport', {
				url: '^/exams/dataexport',

				views: {
					"@": "examDataExportComponent"
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Data Export")
				}
			});

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}