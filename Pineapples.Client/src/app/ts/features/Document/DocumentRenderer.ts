﻿namespace Pineapples.Documents {

	// these regex are set up to match either
	// a full path (ie including a dot) or the extension on its own
	// the two cases are need to 
	// - identfy the type of a selected local file
	// - identify the type of a document object, where the file type is in the property docType

	class docTypes {
		static word = /\.(doc|docx|docm|dot|dotx|dotm)$/;
		static excel = /^(xls|xla|xlt|xlsx|xlst|xlsm)$|\.(xls|xla|xlt|xlsx|xlst|xlsm)$/;
		static excelOpenXml = /^(xlsx|xlst|xlsm)$|\.(xlsx|xlst|xlsm)$/;
		static pdf = /^(pdf)$|\.(pdf)$/;
		static pdfXfdf = /^(pdf|xfdf)$|\.(pdf|xfdf)$/;
		static pdfXfdfZip = /^(pdf|xfdf|zip)$|\.(pdf|xfdf|zip)$/;
		static image = /^(jpg|jpeg|png|tif|tiff|ico|bmp|gif)$|\.(jpg|jpeg|png|tif|tiff|ico|bmp|gif)$/;
		static xml = /^(xml)$|\.(xml)$/;
		static executable = /^(exe|com|dll|cpl|scr|bat|js|vbs|wsh)$|\.(exe|com|dll|cpl|scr|bat|js|vbs|wsh)$/;
		static officeMacro = /^(docm|dotm|xlsm|xltm)$|\.(docm|dotm|xlsm|xltm)$/;
		static xmlExcelOpenXml = /^(xlsx|xlst|xlsm|xml|zip)$|\.(xlsx|xlst|xlsm|xml|zip)$/;
	}

	export function documentIsType(docPath: string, regex) {
		return (docPath ? regex.test(docPath.toLowerCase()) : false);
	}

	export class DocumentManager {

		static $inject = ["identity", "documentsAPI", "$mdDialog"];
		constructor(public identity: Sw.Auth.IIdentity
			, public docApi: IDocumentApi
			, public mdDialog: ng.material.IDialogService) { }

		protected missingImage = "assets/img/no-image.png";

		public isWord(docPath) {
			return documentIsType(docPath, docTypes.word);
		}

		public isExcel(docPath) {
			return documentIsType(docPath, docTypes.excel);
		}

		public isExcelOpenXml(docPath) {
			return documentIsType(docPath, docTypes.excelOpenXml);
		}

		public isPdf(docPath) {
			return documentIsType(docPath, docTypes.pdf);
		}

		public isPdfXfdf(docPath) {
			return documentIsType(docPath, docTypes.pdfXfdf);
		}

		public isPdfXfdfZip(docPath) {
			return documentIsType(docPath, docTypes.pdfXfdfZip);
		}

		public isXml(docPath) {
			return documentIsType(docPath, docTypes.xml);
		}

		public isImage(docPath: string) {
			return documentIsType(docPath, docTypes.image);
		}

		public isExecutable(docPath: string) {
			return documentIsType(docPath, docTypes.executable);
		}

		public isXmlExcelOpenXml(docPath) {
			return documentIsType(docPath, docTypes.xmlExcelOpenXml);
		}

		private isaDocument(doc: any): doc is Document {
			return doc?.docID != undefined;
		}


		/// construct the url to retrieve the given document
		public documentPath(doc: any): string;
		public documentPath(doc: any, height: number): string;
		public documentPath(doc: any, height?: number): string {

			if (!this.identity.isAuthenticated) {
				return "";
			}
			if (!doc) {
				return this.missingImage;
			}

			if (!height) {
				height = 1200;    // default
			}

			if (this.isaDocument(doc)) {
				if (doc.docDeleted) {
					return null;
				}


				// these routes will be handled by imageprocessor if necessary
				// otherwise, will dropthrough to LibraryController 
				let url: string = "library/" + doc.docID
				url = url + "/" + doc.docRotate;
				if (height) {
					url = url + "/" + height;    // the server will determine the height when 0 is passed
				}
				return `${url}?&t=${this.identity.tokenhash(doc.docID)}`;
			}

			// also support the construction of a path if we are just provided the ID into fileDB
			return `library/${doc}?&t=${this.identity.tokenhash(doc)}`;

		}
		public thumbPath(doc, height?: number) {
			if (!this.identity.isAuthenticated) {
				return "";
			}

			if (!doc) {
				return this.missingImage;
			}
			let url: string = "thumb/";
			// for rendering suport during uploaders, support thumb from a file name - just to get the extension
			if (this.isaDocument(doc)) {
				url += doc.docID;
				url = url + "/" + doc.docRotate;
				if (height) {
					url = url + "/" + height;    // the server will determine the height when 0 is passed
				}
				return url + "?&t=" + this.identity.tokenhash(doc.docID);
			}

			// it a simple file name e.g. of an uploaded file, or else a file token
			url += doc + '/0';
			if (height) {
				url = url + "/" + height;    // the server will determine the height when 0 is passed
			}
			return url + "?&t=" + this.identity.tokenhash(doc);
		}


		/**
		 * Rotate a document 
		 * this just changes the roration recorded on the Document record, 
		 * 
		 * @param ev
		 * @param doc
		 * @param newRotate
		 */
		public rotate(ev, doc, newRotate) {
			if (doc.docRotate) {
				doc.docRotate += newRotate;
			} else {
				doc.docRotate = newRotate;
			}

			if (doc.docRotate >= 360) {
				doc.docRotate -= 360;
			}
			this.docApi.update(doc).then(newData => {
				// this is needed to pick up the new rowversion
				angular.extend(doc, (<any>newData).plain());
			});
		}

		public confirmDeleteDocument(doc) {
			this.mdDialog.show(this.confirmDeleteDlg(doc))
				.then(() => {
					//this.docApi.delete(doc);
					this.docApi.get(doc.docID)
						.then(document => this.deleteDocument(doc));
				})

		}

		public deleteDocument(doc) {
			this.docApi.get(doc.docID)
				.then(document => document.remove());
		}

		private confirmDeleteDlg(doc) {
			return this.mdDialog.confirm()
				.title('Delete Document')
				.textContent("The document will be permanently deleted. Click DELETE to proceed")
				.ok('Delete')
				.cancel('Cancel');
		}
	}
	angular
		.module("pineapples")
		.service("documentManager", DocumentManager)
}