﻿namespace Pineapples.Documents {
  export class DocumentBaseController  {
    public document: any;
    public height: number;

    static $inject = ["identity", "documentManager"]
		constructor(public identity: Sw.Auth.IIdentity
			, private renderer: Pineapples.Documents.DocumentManager) {
    }

    public $onChanges(changes) {
    }

    public $onInit() {
     
    }

  }
  class Component implements ng.IComponentOptions {
    public bindings:any = {
      document: '<',
      height: '<',
		};

    public controller: any = DocumentBaseController;
    public controllerAs: string = "vm";
		public templateUrl: string ;
		constructor(template: string) {
			this.templateUrl = template;
		}

  }
  angular
    .module("pineapples")
		.component("documentThumbnail", new Component("library/thumbnail"))
		.component("documentImage", new Component("library/image"))
}