﻿namespace Pineapples.Documents {

	export interface IDocumentApi {
			read(id: number): ng.IPromise<Document>;
			get(id: number): ng.IPromise<Document>;
			update(docData): ng.IPromise<Document>;
			save(docData): ng.IPromise<Document>;
			delete(docData): ng.IPromise<Document>;
			filterPaged(fltr): ng.IPromise<any[]>;
		}

	const ENTITY = "documents";
	class apiService implements IDocumentApi{

		static $inject = ["$q", "$http", "Restangular"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService) {
		}

		public read(id) {
			return this.restangular.one(ENTITY, id).get();
		}
		public get(id) {
			return this.restangular.one(ENTITY, id).get();
		}

		public update(rowData) {
			return this.restangular.all(ENTITY).customPUT(rowData, rowData.docID);
		}
		
		public save(docData) {
			return this.restangular.one(ENTITY).post(docData);
		}

		public delete(docData) {
			return this.restangular.one(ENTITY, docData.docID).remove(docData);
		}

		public filterPaged(fltr) {
			return this.collectionMethod("filter", fltr);
		}

		private collectionMethod(method, fltr) {
			return this.http.post(`api/${ENTITY}/collection/${method}`, fltr)
				.then(response => <any[]>(response.data));
		}

	}

	angular.module('pineapplesAPI')
		.service('documentsAPI', apiService);
}
