// Books Routes
namespace Pineappples.Books {

	let RouteHelper = Sw.Utils.RouteHelper;

	let routes = function ($stateProvider) {
		var featurename = 'Books';
		var filtername = 'BookFilter';
		var templatepath = "book";
		var tableOptions = "bookFieldOptions";
		var url = "books";
		var usersettings = null;
		//var mapview = 'SchoolMapView';

		// root state for 'book' feature
		let state: ng.ui.IState = RouteHelper.frameState(featurename, filtername, templatepath);

		// default 'api' in this feature is schoolsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "booksAPI";
		// minimum data in this tree 
		state.data = state.data || {};
		state.data.frameTitle = "Books";
		state.data.icon = "book";
		state.data.permissions = {
			only: "InfrastructureRead"
		};

		let basestate = "site.books";
		$stateProvider.state(basestate, state);

		// takes an editable list state
		state = RouteHelper.frameListState("book", "bkCode");
		let statename = `${basestate}.list`;
		$stateProvider.state(statename, state);

		// Reload state
		state = {
			url: '^/books/reload',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.remove("book/item");
				$templateCache.remove("book/searcher");
				$state.go("site.books.list");
			}]
		};
		statename = "site.books.reload";
		$stateProvider.state(statename, state);

		// chart table and map
		Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
		// Sw.Utils.RouteHelper.addMapState($stateProvider, featurename, mapview);

		// new - state with a custom url route
		state = {
			url: "^/books/new",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'SchoolWriteX'
				}
			},
			views: {
				"actionpane@site.books": {
					component: "componentBook"
				}

			},
			resolve: {
				model: ['booksAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}]
			}
		};
		$stateProvider.state("site.books.list.new", state);

		// item state
		//$resolve is intriduced in ui-route 0.3.1
		// it allows bindings from the resolve directly into the template
		// injections are replaced with bindings when using a component like this
		// the component definition takes care of its required injections - and its controller
		state = {
			url: "^/books/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.books": {
					component: "componentBook"
				}
			},
			resolve: {
				model: ['booksAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}],
				titleId: ["model", (model) => `${model.bkTitle}`],
				pageTitle: RouteHelper.indirectTitle()
			}
		};
		$stateProvider.state("site.books.list.item", state);
	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}
