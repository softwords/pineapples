﻿namespace Pineapples.Books {
  interface IBindings {
    model: Book;
  }
  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: Book;

    static $inject = ["ApiUi", "booksAPI"];
    constructor(apiui: Sw.Api.IApiUi, private api: any) {
      super(apiui);
    }

		/**
		 * get a new book
		 */
		protected getNew = () => {
			return this.api.new();
		}

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentBook", new Sw.Component.ItemComponentOptions("book", Controller));
}