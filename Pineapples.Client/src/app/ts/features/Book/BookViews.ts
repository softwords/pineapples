namespace Pineapples.Books {
	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'bkCode',
				name: 'Book Code',
				editable: false,
				width: 80,
				pinnedLeft: true,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item")
			},
			{
				field: 'bkTitle',
				name: 'Title',
				editable: false,
				width: 250,
				pinnedLeft: true
			},
			{
				field: 'TG',
				name: 'TG',
				editable: false,
				width: 150,
				cellClass: 'gdAlignCenter'
			},
			{
				field: 'Publisher',
				displayName: 'Publisher',
				editable: false,
				width: 150
			},
			{
				field: 'bkISBN',
				name: 'isbn',
				displayName: 'ISBN',
				editable: false,
				width: 100, cellClass: 'gdAlignLeft'
			},
		]
	};

	let modes = [
    {
      key: "ERU",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'qty',
            name: 'eruqoh',
            displayName: 'ERU',
            cellClass: 'gdAlignLeft'
          }
        ]
      }
    },
    {
      key: "Total Holding",
      columnSet: 1,
      gridOptions: {
        columnDefs: [
          {
            field: 'QOH',
            name: 'qoh',
            displayName: 'QOH',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'QOHReplace',
            name: 'qohreplace',
            displayName: 'Replace',
            cellClass: 'gdAlignRight'
          }
        ]
      } // gridoptions
    }
  ]; // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};

  angular
    .module('pineapples')
    .run(['BookFilter', pushModes]);
}
