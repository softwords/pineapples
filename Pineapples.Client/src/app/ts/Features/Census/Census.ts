﻿namespace Pineapples.CensusAdmin {

	export type censusTeacherQual = {
		svyYear: number,
		ytqCertified: number,
		ytqQualified: number,
		ytqQual: string,
		ytqSector: string
	}

	export type censusSchoolType = {
		svyYear: string,
		stCode: string,
		ytAgeMin: number,
		ytAgeMax: number,
		ytEForm: string
	}
	export class Census extends Pineapples.Api.Editable implements Sw.Api.IEditable {

		static $inject = ["Restangular", "data"];
		constructor(restangular: restangular.IService, censusData) {
			super();
			this._transform(censusData);
			angular.extend(this, censusData);
			// bit of a kludge here: restangular will add the property 'route' (='census')
			// when the item is restangularized, but that hasn;t happened yet
			// so set the route manually here so it is bound into the collection as well
			this.route = "census";
			// this way we get url in the school types collection like
			// api/census/2021/schooltypes/PS
			// otherwise its api/undefined/2021/schooltypes/PS
			restangular.restangularizeCollection(this, this.schoolTypes, "censusschooltypes");
			restangular.restangularizeCollection(this, this.teacherQuals, "censusteacherquals");
		}

		// create static method returns the object constructed from the resultset object from the server
		
		public static create(resultSet: any, injector: ng.auto.IInjectorService) {
			let app = injector.instantiate(Census, { data: resultSet });
			return app;
		};

		public svyYear: number;
		public svyCensusDate: Date;				// date to complete school survey
		public svyCollectionDate: Date;			// date completed school survey to be collected
		public svyPSAge: number;					// official start age of primary

		protected route;

		// IEditable implementation
		public _name() {
			return (<any>this).stuGiven;
		}
		public _type() {
			return "census";
		}
		public _id() {
			// want to report no id on a new record, even though it does have survey year set
			// this forces isNew on the edit component 
			return this.isNew?null:(<any>this).svyYear
		}

		public isNew = false;

		public _transform(newData) {
			// convert these incoming data values
			// is no longer new...
			this.isNew = false;
			return super._transform(newData);
		}

		public schoolTypes: censusSchoolType[];
		public teacherQuals: censusTeacherQual[];
		public effectiveTeacherQuals: censusTeacherQual[];
	
	}
}