﻿namespace Pineapples.CensusAdmin {


	class Controller {
		public model: Census;
		static $inject = ["$scope", "ApiUi", "censusAPI"];
		constructor(private scope: Sw.IScopeEx, apiui: Sw.Api.IApiUi, private api: ICensusApi) {

		}


		public effectiveTeacherQuals: censusTeacherQual[];

		private onDataChange = (data: Sw.IDataChangePublication, sender: any): void => {
			if (data.what > "") {
				this.getEffective();
			}
		}

		public $onInit() {
			this.scope.hub.subscribe("dataUpdated", this.onDataChange);
			this.scope.hub.subscribe("dataDeleted", this.onDataChange);
			this.scope.hub.subscribe("dataInserted", this.onDataChange);
			this.getEffective();
		}

		public $onChanges(changes) {
		}

		protected getNew = () => {
			return this.api.newCensus();
		}

		public getEffective() {
			this.api.effectiveQuals(this.model.svyYear)
				.then(effectiveQuals => {
					this.effectiveTeacherQuals = effectiveQuals;
				})
		}


		public teacherQualsViewMode = {
			"editable": true,
			"deletable": true,
			"insertable": true,

			"gridOptions": {

				columnDefs: [
					{
						name: "Qual",
						displayName: "Qual Code",
						field: "ytqQual",
						lookup: "teacherQuals",
						editable: "new"
					},

					{
						"name": "Sector",
						"displayName": "Sector",
						"field": "ytqSector",
						lookup: "educationSectors",
						editable: true
					},

					{
						"name": "C",
						"displayName": "Certified",
						"field": "ytqCertified",
						type: "boolean",
						editable: true
					},
					{
						"name": "Q",
						"displayName": "Qualified",
						"field": "ytqQualified",
						type: "boolean",
						editable: true
					}

				]
			}
		}

	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			model: "<"
		};
		public controller: any = Controller;
		public controllerAs: string = "vm";
		public templateUrl: string = `census/censusTeacherQuals`;
	}
	angular
		.module("pineapples")
		.component("componentCensusTeacherQuals", new Component());
}