﻿// Teachers Routes
namespace Pineapples.CensusAdmin {

	let RouteHelper = Sw.Utils.RouteHelper;

	let routes = function ($stateProvider) {
		//var mapview = 'TeacherMapView';

		// root state for 'teachers' feature
		let state: ng.ui.IState;
		let statename: string;

		// base censusworkbook state
		state = {
			url: "^/census",
			data: {
				permissions: {
					only: "SurveyRead"
				}
			},
			resolve: {
				featureTitle: RouteHelper.featureTitle('Census')
			},
			abstract: true
		};
		$stateProvider.state("site.census", state);


		state = {
			url: "^/census/current",
			data: {
				permissions: {
					only: 'SurveyRead'
				}
			},
			views: {
				"@": {
					component: "componentCensus"
				}
			},
			resolve: {
				model: ["censusAPI", (api: ICensusApi) => {
					return api.readCurrent();
				}],
				titleId: ['model', (model) => `Current Census ${model.svyYear}`],
				pageTitle: RouteHelper.indirectTitle()
			}
		};
		$stateProvider.state("site.census.current", state);

		state = {
			url: "^/census/init",
			data: {
				permissions: {
					only: 'SurveyRead'
				}
			},
			views: {
				"@": {
					component: "componentCensus"
				}
			},
			resolve: {
				model: ["censusAPI", (api: ICensusApi) => {
					return api.newCensus();
				}],
				titleId: ['model', (model) => `New Census ${model.svyYear}`],
				pageTitle: RouteHelper.indirectTitle()

			}
		};
		$stateProvider.state("site.census.create", state);

		state = {
			url: "^/census/{yr}",
			data: {
				permissions: {
					only: 'SurveyAdmin'
				}
			},
			params: { yr: null, columnField: null, rowData: {} },
			views: {
				"@": {
					component: "componentCensus"
				}
			},
			resolve: {
				model: ['censusAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.yr);
				}],
				titleId: ['model', (model) => `${model.svyYear}`],
				pageTitle: RouteHelper.indirectTitle()
			}
		}
		$stateProvider.state("site.census.item", state);
	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}