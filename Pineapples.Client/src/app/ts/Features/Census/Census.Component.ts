﻿namespace Pineapples.CensusAdmin {

	interface IBindings {
		model: Census;
	}

	class Controller extends Sw.Component.ComponentEditController implements IBindings {
		public model: Census;
		public init: boolean;

		static $inject = ["ApiUi", "censusAPI","$state"];
		constructor(apiui: Sw.Api.IApiUi, private api: ICensusApi, private state:ng.ui.IStateService) {
			super(apiui);
		}

		public $onChanges(changes) {
			super.$onChanges(changes);
		}

		public onInserted(data) {
			this.state.go("site.census.current");
		}
		// override of generic undo
		public undo() {
			// For an existing record, its safer to reread, than to attempt to cache it...

			if (!this.isNew) {
				this.refresh();
			} else {
				// this occurs in census/init. If we abort init, we'll go back to the current census display
				this.state.go("site.census.current");
			};
		}

		public schoolTypesViewMode = {
			"editable": true,
			"deletable": true,
			"insertable": true,

			"gridOptions": {

				columnDefs: [
					{
						name: "C",
						displayName: "Code",
						field: "stCode",
						lookup: "schoolTypes",
						editable: "new"
					},

					{
						"name": "N",
						"displayName": "Min Grid Age",
						"field": "ytAgeMin",
						type: "number",
						editable: true
					},
					{
						"name": "MAX",
						"displayName": "Max Grid Age",
						"field": "ytAgeMax",
						type: "number",
						editable: true
					},
					{
						"name": "EForm",
						"displayName": "EForm Name",
						"field": "ytEForm",
						editable: true
					}


				]
			}
		}

		public teacherQualsViewMode = {
			"editable": true,
			"deletable": true,
			"insertable": true,

			"gridOptions": {

				columnDefs: [
					{
						name: "Qual",
						displayName: "Qual Code",
						field: "ytqQual",
						lookup: "teacherQuals",
						editable: "new"
					},

					{
						"name": "Sector",
						"displayName": "Sector",
						"field": "ytqSector",
						lookup: "educationSectors",
						editable: true
					},
					{
						"name": "Q",
						"displayName": "Qualified",
						"field": "ytqQualified",
						type: "boolean",
						editable: true
					},

					{
						"name": "C",
						"displayName": "Certified",
						"field": "ytqCertified",
						type: "boolean",
						editable: true
					}

				]
			}
		}

	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any = {
			model: "<",
			api: "<",        // api is only need when we allow 'new'
			t: "@",          // optional (dynamic) the current tab to open on
			init: "<"

		};
		public controllerAs: string = "vm";
		public templateUrl = "census/item";
		public controller = Controller

	}
	angular
		.module("pineapples")
		.component("componentCensus", new Sw.Component.ItemComponentOptions("census", Controller));
}