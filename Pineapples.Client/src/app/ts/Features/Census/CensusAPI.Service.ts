﻿namespace Pineapples.CensusAdmin {

	export interface ICensusApi {
		read(year: number): ng.IPromise<Census>;
		readCurrent():ng.IPromise<Census>;
		newCensus(): ng.IPromise<Census>;
		effectiveQuals(year: number): ng.IPromise<censusTeacherQual[]>
	}

	const ENTITY = "census";
	class apiService implements ICensusApi {

		static $inject = ["$q", "$http", "Restangular", "xFilter", "reflator"];
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService
			, public xFilter: Sw.xFilter.IXFilter
			, public reflator: Sw.Api.IReflator) {
		}

		public read(id) {
			return <ng.IPromise<Census>>this.restangular.one(ENTITY, id).get();
		}

		public readCurrent() {
			return <ng.IPromise<Census>>this.restangular.all(ENTITY).customGET("current");
		}
		
		public newCensus() {
			return this.readCurrent()
				.then(census => {
					census.svyYear += 1;
					//https://stackoverflow.com/questions/33070428/add-a-year-to-todays-date
					census.svyCensusDate.setFullYear(census.svyCensusDate.getFullYear() + 1);
					census.svyCollectionDate.setFullYear(census.svyCollectionDate.getFullYear() + 1);
					census.isNew = true;
					return census;
				})
		}

		public effectiveQuals(year: number) {
			return this.http.get<Api.IDataResult<censusTeacherQual[]>>(`api/census/${year}/effectiveteacherquals`)
				.then(response => response.data.ResultSet);
		}

	}

angular.module("pineapplesAPI")
	.service("censusAPI", apiService);
}