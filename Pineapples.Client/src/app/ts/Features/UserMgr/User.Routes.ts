﻿// User Management Routes
namespace Sw.Auth {

  let RouteHelper = Sw.Utils.RouteHelper;

  let routes = function ($stateProvider) {
		let state: ng.ui.IState = {
			data: {
				permissions: {
					only: "Admins"
				}
      },
      resolve: {
        featureTitle: RouteHelper.featureTitle("Users")
      },
      abstract: true
    }
    let statename = "site.users";
    $stateProvider.state(statename, state);

    state = {
      url: "^/users/list",
      views: {
        "@": "userList"
      },
      resolve: {
        users: ['usersAPI',  (api) => {
          return api.list();
        }],
        pageTitle: RouteHelper.simpleTitle("List")
      }
    };
    statename = "site.users.list";
    $stateProvider.state(statename, state);

    state = {
      url: "^/users/{id}",
      params: { id: null },
      views: {
        "actionpane@site.users.list": "componentUser"
      },
      resolve: {
        model: ['usersAPI', "$stateParams", (api, stateParams) => {
          return api.read(stateParams.id);
        }],
        roles: ['umgrAPI', (api) => {
          return api.roles();
        }],
        menuKeys: ['umgrAPI', (api) => {
          return api.menuKeys();
				}],
				// $q.resolve creates a resolved promise - the payload of that promise
				// is a function returning a promise
        getNew: ["$q", "usersAPI", (q: ng.IQService, api) => q.resolve(() => api.read(0))],
        titleId: ["model", (model) => model.UserName],
        pageTitle: RouteHelper.indirectTitle()
      }
    };
    statename = "site.users.list.item";
    $stateProvider.state(statename, state);
  }

  angular
    .module("sw.common")
    .config(['$stateProvider', routes])
}
