﻿namespace Sw.Auth {

  class Controller {
    public roles: any[];

    // can be directly invoked from the row containing the user
    public showRow(role) {
      this.state.go("site.role.list.item", { id: role.Id });
    };

    static $inject = ["$state"];
    constructor(public state: ng.ui.IStateService) {

    }

  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        roles: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "usermgr/rolelist";
    }
  }
  angular
    .module("sw.common")
    .component("roleList", new Component());
}