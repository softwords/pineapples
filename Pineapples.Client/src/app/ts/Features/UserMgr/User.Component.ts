﻿namespace Sw.Auth {
  interface IBindings {
    model: any;
    roles: any[];
		menuKeys: any[];
		getNew: () => ng.IPromise<User>
  }
  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: any;
    // list of available roles
    public roles: any[]; 
		public menuKeys: any[]; 
		public getNew: () => ng.IPromise<User>;

    static $inject = ["ApiUi","Lookups"];
		constructor(apiUi: Sw.Api.IApiUi
			, public lookups: Sw.Lookups.LookupService) {
      super(apiUi);
    }
  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        model: '<',
        roles: '<',
				menuKeys: '<',
				getNew: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "usermgr/Item";
    }
  }
  angular
    .module("sw.common")
    .component("componentUser", new ComponentOptions());
}
