﻿namespace Pineapples.Scholarships {

  export class Scholarship extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(scholarshipData) {
      super();
			this._transform(scholarshipData);
			angular.extend(this, scholarshipData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let scholarship = new Scholarship(resultSet);
      return scholarship;
    }

    // IEditable implementation
    public _name() {
			return (<any>this).schoID;
    }
    public _type() {
      return "scholarship";
    }
    public _id() {
			return (<any>this).schoID;
    }

    public _transform(newData) {
      // convert these incoming data values
      // transformDates is deprecated: http interceptor now converts dates on load
      //this._transformDates(newData, ["stuDoB"]);
      return super._transform(newData);
    }

		public Enrollments: any[];
		public Documents: any[];
		public Audit: any[];
    //public Scorecards: any[];
    //public stuPhoto: string;    // the current photo

    //public get currentPhoto() {
    //  return _.find(this.Documents, { docID: this.stuPhoto });
    //}
  }
}