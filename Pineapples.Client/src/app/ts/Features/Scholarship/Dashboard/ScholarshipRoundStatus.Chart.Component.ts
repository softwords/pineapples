﻿namespace Pineapples.Dashboards.Scholarship {

	// Base class for components that want to split some lookup value (Environment, Ethnicity, english learner, disability)
	// the group passed as binding is assumed to have a array as 'key' [ <code>, <name> , <name of code>]

	export class GenderBaseDashboardChild extends DashboardChild implements IDashboardChild {

		public group: CrossFilter.Group<any, any, any>;
		private groupData: any[]; // filtered all() array of keyvalue pairs
		public options: scholarshipOptions;
		public onClick: any;			// callback
		public chartData: Sw.Charts.AnnotatedGroup; 
		public dashboard: ScholarshipDashboard;

		public colorbrewer: ColorBrewer.Base = colorbrewer;

		public paletteName;
		public viewOptions;

		// needs to be overriden by the heading-title binding
		// since the contents are determind by the group passed in
		protected defaultTitle = "Scholarships By Gender";
		

		public $onInit() {
			this.chartData = {
				group: this.group,
				rowId: kv => kv.key[1],
				columns: ["F", "M"],
				valueAccessor: (kv, col) => kv.value.Tot[col]
			}
		}

		public onOptionChange() {
			this.refreshChart();
		};

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			// default
		}
		public onChartRender(option: echarts.EChartOption, echarts: echarts.ECharts, renderedType: string) {
			Sw.Charts.chartOps(option)
				.showLegend(renderedType != "pie")
		}

		public clickHandler(item) {
			console.log(item);
			this.onClick({ item: item });
		}
	}
	export class XTabDashboardChild extends DashboardChild implements IDashboardChild {

		public group: CrossFilter.Group<any, any, any>;
		private groupData: any[]; // filtered all() array of keyvalue pairs
		public options: coreOptions;
		public onClick: any;			// callback
		public chartData: any; //Sw.Charts.AnnotatedGroup; 
		public dashboard: ScholarshipDashboard;

		public colorbrewer: ColorBrewer.Base = colorbrewer;

		public paletteName;
		public viewOptions;

		// needs to be overriden by the heading-title binding
		// since the contents are determind by the group passed in
		protected defaultTitle = "Scholarships By Gender";


		public $onInit() {
			this.chartData = {
				group: this.group,
				rowId: kv =>  (Array.isArray(kv.key)?kv.key[1]:kv.key),
				columns: null,
				valueAccessor: (kv, col) => kv.value[col]?.T
			}
		}

		public onOptionChange() {
			this.refreshChart();
		};

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			// default
		}
		public onChartRender(option: echarts.EChartOption, echarts: echarts.ECharts, renderedType: string) {
			Sw.Charts.chartOps(option)
				.showLegend(renderedType != "pie")
		}

		public clickHandler(item) {
			console.log(item);
			this.onClick({ item: item });
		}
	}
	angular
		.module("pineapples")
		.component("scholarshipXTab", new ScholarshipDashboardComponent(XTabDashboardChild, "scholarshipGender"))
		.component("scholarshipGender", new ScholarshipDashboardComponent(GenderBaseDashboardChild, "scholarshipGender"))
		.component("scholarshipRoundStatus", new ScholarshipDashboardComponent(GenderBaseDashboardChild, "scholarshiproundstatus"));
}