﻿namespace Pineapples.Scholarships {

  export class ScholarshipStudy extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(data) {
      super();
			this._transform(data);
			angular.extend(this, data);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let scholarshipstudy = new ScholarshipStudy(resultSet);
      return scholarshipstudy;
    }

    // IEditable implementation
    public _name() {
			return (<any>this).posID;
    }
    public _type() {
      return "scholarshipstudy";
    }
    public _id() {
			return (<any>this).posID;
    }

    public _transform(newData) {
      // convert these incoming data values
      // transformDates is deprecated: http interceptor now converts dates on load
      //this._transformDates(newData, ["stuDoB"]);
      return super._transform(newData);
    }

  
  }
}