﻿namespace Pineapples.Scholarships {

  interface IBindings {
    links: any[];
  }

	class Controller extends Sw.Component.ListMonitor implements IBindings {
    public links: any[];
		public scholarship: Scholarship;
		public parentkeyname = "lnkID";

		static $inject = ["$scope", "$mdDialog", "Restangular"]
		constructor(
			scope: Sw.IScopeEx
      , private mdDialog: ng.material.IDialogService
			, private Restangular: restangular.IService
		) {
			super();
			this.monitor(scope, "scholarshiplinks", "lnkID");
			this.multiSelect = false;
    }

		public monitoredList() {
			return this.links;
		}
		public isListMember(newData) {
			return (this.scholarship?._id() == newData[this.parentkeyname]);
		} 

    public $onChanges(changes) {
    }
    public newDoc() {
      //this.state.go("site.teachers.upload", { id: this.teacherId });
      this._uploadDialog().then(() => {
      }, (error) => {
      });
    }

    private _uploadDialog() {
      let options: ng.material.IDialogOptions = {
        locals: { scholarship: this.scholarship },
        controller: ScholarshipLinkUploadController,
        controllerAs: 'vm',
				bindToController: true,
				clickOutsideToClose: true,
        templateUrl: "scholarshiplink/uploaddialog"
      }
      return this.mdDialog.show(options);
    }

    private doEdit(link, event) {
      let slink = new ScholarshipLink(link);
      // this has cloned the data...
      this.Restangular.restangularizeElement(null, slink, "scholarshiplinks");
      this._editDialog(slink, link);
    }

    private _editDialog(slink: ScholarshipLink, link: any) {
      let options: ng.material.IDialogOptions = {
        locals: { model: slink, scholarship: this.scholarship },
        controller: editController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: "scholarshiplink/editdialog"
      }
      return this.mdDialog.show(options);

    }

    private doDelete(link, event) {
      let confirm = this.mdDialog.confirm()
        .title('Delete Document')
        .textContent('Delete the document ' + link.docTitle + ' from the document library?')
        .targetEvent(event)
        .ok('Delete')
        .cancel('Cancel');
      this.mdDialog.show(confirm).then(() => {
        let slink = new ScholarshipLink(link);
        // this has cloned the data...
        this.Restangular.restangularizeElement(null, slink, "scholarshiplinks");
        slink.remove().then(() => {
        }, 
          () => { }
          // remove failed
        );
      });
    }

  }

  /**
   * Controller for the edit Dialog
   */
  class editController extends Sw.Component.ComponentEditController {
    static $inject = ["$mdDialog", "ApiUi"];
    constructor(public mdDialog: ng.material.IDialogService
      , apiUi: Sw.Api.IApiUi) {
      super(apiUi);
      this.isEditing = true;
    }
    public scholarship: Scholarship;

    public onModelUpdated(newData: any) {
      super.onModelUpdated(newData);
      //this.scholarship.tPhoto = newData.tPhoto;
    }
   
    public closeDialog() {
      this.mdDialog.cancel();
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        links: "<",
        scholarship: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "scholarship/linklist";
    }
  }
  angular
    .module("pineapples")
    .component("componentScholarshipLinks", new Component());

}