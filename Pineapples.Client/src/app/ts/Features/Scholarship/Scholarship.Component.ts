﻿namespace Pineapples.Scholarships {

  interface IBindings {
    model: Scholarship;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
		public model: Scholarship;

		static $inject = ["$state","ApiUi", "Lookups", "scholarshipsAPI","Restangular"];
		constructor(
			public state: ng.ui.IStateService,
			apiui: Sw.Api.IApiUi, public lookups: Pineapples.Lookups.LookupService,
			private api: any,
			private restangular: restangular.IService) {
      super(apiui);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

		// after saving a new item, we change the state to reflect that we are now displaying an exisitng item
		public onInserted(model) {
			this.state.go("site.scholarships.list.item", {id: model._id()})
		}

		public onModelUpdated(newData) {
			super.onModelUpdated(newData);
			// keep the audit up to date
			this.refreshAudit();
		}

		private refreshAudit() {
			this.restangular.one("scholarships", this.model._id()).customGET("audit")
				.then(data => {
					this.model.Audit = data.ResultSet;
				});
		}

		public showstudent() {

			this.state.go("site.students.list.item", { id: this.model["stuID"] });
		}

  }

  angular
    .module("pineapples")
    .component("componentScholarship", new Sw.Component.ItemComponentOptions("scholarship", Controller));
}