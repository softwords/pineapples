﻿namespace Pineapples.SpecialEd {

	angular
		.module("pineapples")
		.component("scholarshipRenderFrame", new Sw.Component.RenderFrameComponent("scholarship"));
}