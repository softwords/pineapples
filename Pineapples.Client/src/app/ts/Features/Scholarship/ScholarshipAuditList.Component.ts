﻿namespace Pineapples.Scholarships {

	class Controller extends Sw.Component.ListMonitor {

		public audit: any[];
		public scholarship: Scholarship;
		public parentkeyname = "schoID";

 
    static $inject = ["$scope", "$mdDialog", "$location", "$state","Restangular"];
		constructor(
			private scope:Sw.IScopeEx,
			public mdDialog: ng.material.IDialogService,
			private $location: ng.ILocationService,
			public $state: ng.ui.IStateService,
			private restangular: restangular.IService) {
			super();
			this.monitor(scope, "scholarshipaudit", "posID");
			this.multiSelect = false;
		}

		public monitoredList() {
			return this.audit;
		}
		public isListMember(newData) {
			return (this.scholarship?._id() == newData[this.parentkeyname]);
		}
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
				audit: "<",
				scholarship: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "scholarship/auditlist";
    }
  }

  //// popup dialog for audit log
  //class DialogController extends Sw.Component.MdDialogController {
  //  static $inject = ["$mdDialog", "enrollment"];
  //  constructor(mdDialog: ng.material.IDialogService, public payslip) {
  //    super(mdDialog);
  //  }
  //}

  angular
    .module("pineapples")
    .component("componentScholarshipAuditList", new Component());
}
