﻿namespace Pineapples.Scholarships {

	type statusChangeModel = {
		newStatus: string;
		comment: string
	}

	/**
	 * Invoke the status change dialog
	 * Thus function is assigned to the onEnter event of the state
	 * assigned to this dialog
	 * Note that this is a shortcut way of assigning the transition event onEnter - they are the same thing
	 * Resolves are available and can be psued into the function
	 * The function returns a rejected promise always - this cancels the transition
	 * and leaves the current state unchanged
	 * the parameters to the function are injectables defined by the functions $inject property
	 * @param $q 
	 * @param mdDialog
	 * @param application
	 */
	export function invokeEditStudy(
		transition,
		mdDialog: ng.material.IDialogService,
		model: ScholarshipStudy) {
		let options: ng.material.IDialogOptions = {
			locals: {
				model: model
			},
			controller: DialogController,
			controllerAs: 'vm',
			bindToController: true,
			clickOutsideToClose: true,
			templateUrl: "scholarship/studydialog"

		}
		return mdDialog.show(options)
			.finally(() => false);

	}
	invokeEditStudy.$inject = ["$transition$", "$mdDialog", "model"];

	class DialogController extends Sw.Component.MdDialogController {
		static $inject = ["$mdDialog", "ApiUi", "Restangular", "Lookups"];
		constructor(mdDialog: ng.material.IDialogService, private apiUi: Sw.Api.IApiUi, private restangular: restangular.IService
			, private lookups: Sw.Lookups.LookupService
			, private api) {
			super(mdDialog);
		}

		public isEditing = true;
		public isWaiting = false;
		public model: ScholarshipStudy;
		public message: string

		public save() {

			this.message = "Saving...";
			this.isWaiting = true;
			let promise: ng.IPromise<any>;
			//if (this.model._isNew()) {
			//	//promise = this.restangular.all("scholarshipsstudy").post(this.model);
			//	promise = this.model.post();
			//} else {
			//	promise = this.model.put();
			//}
			this.model.save()
				.then((data) => {
					this.message = "Saved"
					this.closeDialog();
				}, (error) => {
						this.message = error.message;
						this.apiUi.showErrorResponse(this.model, error)
							.catch(() => {
								this.returnFail(this.message);
							});
				})
				.finally(() => { this.isWaiting = false; });
		}
	}
}