﻿// Scholarship routes
namespace Pineapples.Scholarships {

	let RouteHelper = Sw.Utils.RouteHelper;
	let routes = function ($stateProvider) {
		var featurename = 'Scholarships';
		var filtername = 'ScholarshipFilter';
		var templatepath = "scholarship";
		var url = "scholarships";
		var usersettings = null;

		let state: ng.ui.IState = RouteHelper.frameState(featurename, filtername, templatepath);

		state.resolve = state.resolve || {};
		// default 'api' in this feature is scholarshipsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "scholarshipsAPI";
		// load scholarship loookups
		state.resolve["customLookups"] = ["Lookups", (lookups: Pineapples.Lookups.LookupService) => lookups.scholarship()]


		state.data = state.data || {};
		state.data.frameTitle = "Scholarships";
		state.data.icon = "list";
		state.data.permissions = {
			only: 'ScholarshipRead'
		};
		let basestate = "site.scholarships";
		$stateProvider.state(basestate, state);

		// List state
		state = RouteHelper.frameListState("scholarships", "schoID");
		let statename = `${basestate}.list`;
		state.url = `^/${url}/list`;
		state.views["renderoptions"] = {
			templateUrl: "generic/PagedListParamsComponent",
			controller: "FilterController",
			controllerAs: "vm",
		};

		$stateProvider.state(statename, state);

		// list - new
		state = {
			url: "^/scholarships/new?studentid",
			params: { studentid: null },
			data: {
				permissions: {
					only: 'ScholarshipWriteX'
				}
			},
			onEnter: invokeGetStudent,
			views: {
				"actionpane@site.scholarships": "componentScholarship"
			},
			resolve: {
				model: ['scholarshipsAPI', '$stateParams', function (api, $stateParams) {
					return api.new($stateParams.studentid);
				}]
			}
		};
		$stateProvider.state("site.scholarships.list.new", state);

		// item state
		state = {
			url: "^/scholarships/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.scholarships": {
					component: "componentScholarship"
				}
			},
			resolve: {
				model: ['scholarshipsAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}]
			}
		};
		$stateProvider.state("site.scholarships.list.item", state);

		// popup to edit a study record under a scholarship
		// this is a 'modal route'
		state = {
			onEnter: invokeEditStudy,
			url: "^/scholarships/{scholarshipId: int}/study/{posid: int}",
			params: { scholarshipId: null, posid: null },

			resolve: {
				model: ["Restangular", '$stateParams', (restangular: restangular.IService, $stateParams) => {
					if ($stateParams.posid == null) {
						return restangular.one("scholarships", $stateParams.scholarshipId)
							.one("scholarshipsstudy", "new").get()
							.then(data => {
								// restangular's save() method uses fromServer to decied whether to POST (ie insert)
								// or PUT (update)
								// if you use a server call to initialise a new empty object, like here, 
								// set fromServer to false so you can safely call save()
								data.fromServer = false;
								return data;
							})
					}
					return restangular.one("scholarshipsstudy", $stateParams.posid).get();
				}]

			}
		};
		$stateProvider.state("site.scholarships.list.item.study", state);



		// code tables
		//
		state = {

			url: "^/scholarshiptables/{lkp}",
			params: {
				lkp: { value: null, squash: true }
			},
			data: {
				rendermode: "Lookups"
			},
			views: {
				"@": {
					component: "componentTableSelector"
				}
			},
			resolve: {
				tableList: ["scholarshipsAPI", (api) => {
					return api.getEditableTables();
				}],
				rowset: ['$stateParams', 'Restangular', ($stateParams, restangular: restangular.IService) => {
					if ($stateParams.lkp) {
						return restangular.all($stateParams.lkp).getList();
					}
					return null;
				}],
				entityType: ['$stateParams', ($stateParams) => {
					return $stateParams.lkp;
				}],
				entityDescription: ['$stateParams', 'Restangular', ($stateParams, restangular: restangular.IService) => {
					if ($stateParams.lkp) {
						return restangular.all($stateParams.lkp).get("info/description");
					}
					return null;
				}],

				viewMode: ['$stateParams', "$http", ($stateParams, http: ng.IHttpService) => {
					if ($stateParams.lkp) {
						return http.get(`api/${$stateParams.lkp}/info/viewmode`).then(result => result.data);
					}
					return null;
				}],
				isEditing: () => true
			}
		};
		$stateProvider.state("site.scholarships.lookups", state);

		state = {

			url: "/test",
			params: {
				lkp: { value: null, squash: true }
			},
			data: {
				rendermode: "Test "
			},
			views: {
				"@": {
					templateUrl: "scholarship/test"
				}
			}
		};
		$stateProvider.state("site.scholarships.test", state);

		// Dashboard 
		//---------------------
		// state for a high level dashboard
		state = {
			url: "/dashboard",
			data: {
				rendermode: "Dashboard"
			},
			views: {
				"renderarea": "scholarshipDashboard",
				"searcher": "scholarshipOptionsEditor"
			},
			resolve: {
				options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
					return new Pineapples.Dashboards.scholarshipOptions(lookups, scope.hub);
				}],

				tables: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
					let p = reflator.get("api/warehouse/scholarships/rounds").then(response => (<any>response.data));
					let p2 = reflator.get("api/warehouse/scholarships/payments").then(response => (<any>response.data));
					return q.all([p, p2]);		// an array of 1 promise
				}],

			}
		};
		$stateProvider.state(`${basestate}.dashboard`, state);

		$stateProvider
			.state('site.scholarships.dataexport', {
				url: '^/scholarships/dataexport',

				views: {
					"@": "scholarshipDataExportComponent"
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Data Export")
				}
			});

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}