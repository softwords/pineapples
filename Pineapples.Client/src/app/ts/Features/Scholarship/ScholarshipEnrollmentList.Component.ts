﻿namespace Pineapples.Scholarships {

	class Controller extends Sw.Component.ListMonitor {

		public enrollments: any[];
		public scholarship: Scholarship;
		public parentkeyname = "schoID";

 
    static $inject = ["$scope", "$mdDialog", "$location", "$state","Restangular"];
		constructor(
			private scope:Sw.IScopeEx,
			public mdDialog: ng.material.IDialogService,
			private $location: ng.ILocationService,
			public $state: ng.ui.IStateService,
			private restangular: restangular.IService) {
			super();
			this.monitor(scope, "scholarshipsstudy", "posID");
			this.multiSelect = false;
		}

		public monitoredList() {
			return this.enrollments;
		}
		public isListMember(newData) {
			return (this.scholarship?._id() == newData[this.parentkeyname]);
		}

    public $onChanges(changes) {
      if (changes.enrollments) {
//        console.log(this.enrollments);
      }
		}

		// callback to refresh the audit log if something has happened
		public monitoredListInsert(data) {
			this.refreshAudit();
		}
		public monitoredListUpdate(data) {
			this.refreshAudit();
		}
		public monitoredListDelete(data) {
			this.refreshAudit();
		}


		private refreshAudit() {
			this.restangular.one("scholarships", this.scholarship._id()).customGET("audit")
				.then(data => {
					this.scholarship.Audit = data.ResultSet;
				});
		}
		// display the study record in a dialog...
		// this is now handled by a 'dialog route'
		public show(study) {
				console.log(study);
			this.$state.go(".study", { posid: study.posID });
		}
		// display the study record in a dialog...
		// this is now handled by a 'dialog route'
		public new() {

			this.$state.go(".study", { scholarshipId: this.scholarship._id(), posid: null});
		}
		public delete(study) {
			this.clone(study).remove();
			//this.restangular.one("scholarshipsstudy", study.posID).remove(study);
		}

		private clone(q) {
			// drop any restangular stuff
			let modeldata = (q.plain ? q.plain() : q);
			// create the new teacher qualification object
			let ss = ScholarshipStudy.create(modeldata);
			//restangularize it
			this.restangular.restangularizeElement(null, ss, "scholarshipsstudy");
			return ss;
		}

		// not used - there is no template "student/enrollmentdialog"
    //public showEnrollment(enrollment) {
    //  let options: any = {
    //    clickOutsideToClose: true,
    //    templateUrl: "student/enrollmentdialog",
    //    controller: DialogController,
    //    controllerAs: "vm",
    //    bindToController: true,
    //    locals: {
    //      enrollment: enrollment
    //    }

    //  }
    //  this.mdDialog.show(options);
    //}

 
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
				enrollments: "<",
				scholarship: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "scholarship/enrollmentlist";
    }
  }

  //// popup dialog for audit log
  //class DialogController extends Sw.Component.MdDialogController {
  //  static $inject = ["$mdDialog", "enrollment"];
  //  constructor(mdDialog: ng.material.IDialogService, public payslip) {
  //    super(mdDialog);
  //  }
  //}

  angular
    .module("pineapples")
    .component("componentScholarshipEnrollmentList", new Component());
}
