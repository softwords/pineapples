﻿namespace Pineapples.Students {
	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'schoID',
				name: 'schoID',
				displayName: 'Scholarship ID',
				editable: false,
				pinnedLeft: true,
				width: 80,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"],
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm", "schoID")
			},
			{
				field: 'scholCode',
				name: 'scholCode',
				displayName: 'Type',
				editable: false,
				pinnedLeft: true,
				width: 80,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"],
			},
			{
				field: 'schrYear',
				name: 'schrYear',
				displayName: 'Round',
				editable: false,
				pinnedLeft: true,
				width: 80,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'schoStatus',
				name: 'schoStatus',
				displayName: 'Status ',
				editable: false,
				pinnedLeft: true,
				width: 100,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"],
				lookup: "scholarshipStatus"
			},
			//{
			//	field: 'stuGiven',
			//	name: 'Given Name',
			//	displayName: 'Given Name',
			//	pinnedLeft: true,
			//	width: 160,
			//	enableSorting: true,
			//	sortDirectionCycle: ["asc", "desc"]
			//},
			//{
			//	field: 'stuFamilyName',
			//	name: 'Family Name',
			//	displayName: 'Family Name',
			//	pinnedLeft: true,
			//	width: 160,
			//	enableSorting: true,
			//	sortDirectionCycle: ["asc", "desc"]
			//},
			{
				field: 'stuFamilyName',
				name: 'combinedname',
				displayName: 'Name',
				pinnedLeft: true,
				width: 180,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"],
				cellTemplate: Sw.Utils.UiGridUtils.nameColumn("stuGiven", "stuFamilyName")
			}
		]
	};
  let modes = [
    {
			key: "Demographics",
			columnSet: 0,
      gridOptions: {
				columnDefs: [

					{
						field: 'stuDoB',
						name: 'Date of Birth',
						displayName: 'Date of Birth',
						cellFilter: "date:'d-MMM-yyyy'",
					},
					{
						field: 'stuGender',
						name: 'Gender',
						displayName: 'Gender',
						lookup: 'gender',
					},
					{
						field: 'stuEthnicity',
						name: 'Ethnicity',
						displayName: 'Ethnicity',
						lookup: 'ethnicities',
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"],
					}
        ]
      }
		},    
		{
			key: "Timeline",
			columnSet: 1,
			gridOptions: {
				columnDefs: [

					{
						field: 'schoStatusDate',
						name: 'StatusDate',
						displayName: 'Status Date ',			// adding a space at the end prevents translation by vocab
						cellFilter: "date:'d-MMM-yyyy'",
						width: 100
					},
					{
						field: 'schoExpectedCompletion',
						name: 'expectedCompletion',
						displayName: 'Expected Completion ',
						cellFilter: "date:'d-MMM-yyyy'",
						width: 100
					},
					{
						field: 'schoAppliedDate',
						name: 'appliedDate',
						displayName: 'Date of Application',
						cellFilter: "date:'d-MMM-yyyy'",
						width: 100
					},
					{
						field: 'schoCompliantDate',
						name: 'compliantDate',
						displayName: 'Date Compliant',
						cellFilter: "date:'d-MMM-yyyy'",
						width: 100
					},
					{
						field: 'schoAwardedDate',
						name: 'awardedDate',
						displayName: 'Date Awarded ',
						cellFilter: "date:'d-MMM-yyyy'",
						width: 100
					},
					{
						field: 'schoCompletedDate',
						name: 'completedDate',
						displayName: 'Date Completed',
						cellFilter: "date:'d-MMM-yyyy'",
						width: 100
					}
				]
			}
		},    
		{
			key: "Probation",
			columnSet: 2,
			gridOptions: {
				columnDefs: [

					{
						field: 'lastGPA',
						name: 'LastGPA',
						displayName: 'Last GPA',
					},
					{
						field: 'lastYear',
						name: 'LastYear',
					},

					{
						field: 'newStatus',
						name: 'newStatus',
						displayName: 'New Status',
					},

				]
			}
		},    
  ];

	var pushModes = function (filter) {
		filter.ViewDefaults = viewDefaults;
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['ScholarshipFilter', pushModes]);
}
