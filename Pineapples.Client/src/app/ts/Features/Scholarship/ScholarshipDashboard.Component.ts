﻿namespace Pineapples.Dashboards {

	export namespace Scholarships {

		// refer to the view warehouse.ScholarshipRounds
		// which is accessed via api/warehouse/scholarships/rounds
		export interface IxfData {
			ScholarshipType: string;
			Round: number;
			MajorCode: string;
			Major: string;
			StatusCode: string;
			Status: string;
			Gender: string;
			Number: number;
			Paid: number; 
			LastEnrol: string;
		}
		// corresponds to the view warehouse.ScholarshipPayments
		// which is accessed via api/warehouse/scholarships/payments
		export interface IxfPaymentsData {
			ScholarshipType: string;
			Round: number;
			MajorCode: string;
			Major: string;
			StatusCode: string;
			Status: string;
			Gender: string;
			Paid: number;
			LastEnrol: string;
			MajorGroupCode: string;
			MajorGroup: string;
			AcademicYear: number;
			AcademicSemester: number;
			AcademicYearSemester: string;
			TertiaryGradeCode: number;
			TertiaryGrade: string;
			PaymentYear: string;
			Amount: number;
			Number: number;
		}

	}
	export class ScholarshipDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

		// additional data

		private tables: any[];
		// override with strongly typed crossfilter
		public xf: CrossFilter.CrossFilter<Scholarships.IxfData>;
		public xfP: CrossFilter.CrossFilter<Scholarships.IxfPaymentsData>;

		// dimensions and groups based on Scholarships

		public dimYear: CrossFilter.Dimension<Scholarships.IxfData, number>;
		public dimScholarshipType: CrossFilter.Dimension<Scholarships.IxfData, string[]>;
		public dimMajor: CrossFilter.Dimension<Scholarships.IxfData, string[]>;
		public dimStatus: CrossFilter.Dimension<Scholarships.IxfData, string[]>;

		public grpYear: CrossFilter.Group<Scholarships.IxfData, string, number>;
		public grpScholarshipType: CrossFilter.Group<Scholarships.IxfData, string, number>;
		public grpMajor: CrossFilter.Group<Scholarships.IxfData, string, number>;
		public grpStatus: CrossFilter.Group<Scholarships.IxfData, string, number>;

		// for payments data
		public dimYearP: CrossFilter.Dimension<Scholarships.IxfPaymentsData, number>;
		public dimScholarshipTypeP: CrossFilter.Dimension<Scholarships.IxfPaymentsData, string[]>;
		public dimMajorP: CrossFilter.Dimension<Scholarships.IxfPaymentsData, string[]>;
		public dimStatusP: CrossFilter.Dimension<Scholarships.IxfPaymentsData, string[]>;
		public dimPaymentYearP: CrossFilter.Dimension<Scholarships.IxfPaymentsData, string[]>;

		public grpYearP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;
		public grpScholarshipTypeP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;
		public grpMajorP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;
		public grpStatusP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;
		public grpPaymentYearP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;
		public grpPaymentYearTierP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;
		public grpPaymentYearRoundP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;
		public grpPaymentYearTertiaryGradeP: CrossFilter.Group<Scholarships.IxfPaymentsData, string, number>;

		public $onChanges(changes) {
			if (changes.tables && changes.tables.currentValue) {
				// data has been retrieved by the ui-router as a Resolve
				// and passed to the dashboard as a binding - process it into a crossfilter
				// create the crossfilter from the warehouse table
				this.xf = crossfilter(this.tables[0]);
				this.xfP = crossfilter(this.tables[1]);
				this.createDimensions();
			}
		}

		public $onInit() {

		}

		// helper function to filter when key in the dimension is an array: filter on the first element
		// note if code is null, want to clear the filter ( ie filterAll()) so return null 
		private codeFilter(code) {
			return (code)?(d => d[0] == code): null;
		}

		public onOptionChange(data: IOptionChangeData, sender) {
			if (data.selectedYear) {
				this.dimYear.filter(this.codeFilter(this.options.selectedYear));
				this.dimYearP.filter(this.codeFilter(this.options.selectedYear));
			}
			if (data.selectedScholarshipType) {
				this.dimScholarshipType.filter(this.codeFilter(this.options.selectedScholarshipType));
				this.dimScholarshipTypeP.filter(this.codeFilter(this.options.selectedScholarshipType));
			}
			if (data.selectedMajor) {
				this.dimMajor.filter(this.codeFilter(this.options.selectedMajor));
				this.dimMajorP.filter(this.codeFilter(this.options.selectedMajor));
			}
			if (data.selectedStatus) {
				this.dimStatus.filter(this.codeFilter(this.options.selectedStatus));
				this.dimStatusP.filter(this.codeFilter(this.options.selectedStatus));
			}
			// always call the super version so that the clients get to know about the option change as well
			super.onOptionChange(data, sender);
		}

		private elSelector = ({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel)["L"];
		private classLevelSelector = this.xFilter.getPropAccessor("ClassLevel")

		public createDimensions() {
			// create the crossfilter dimensions

			//this.dimAuthorityGovt = this.xf.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovt);
			//	return ag ? ag.N : null;
			//});

			let d: (xf: CrossFilter.CrossFilter<any>) => CrossFilter.Dimension<any, any>;
			d = (xf: CrossFilter.CrossFilter<Scholarships.IxfData>) => xf.dimension(d => [
				d.Round,
				d.Round,
				"Scholarship Round"]);
			this.dimYear = d(this.xf);
			this.dimYearP = d(this.xfP);

			d = (xf: CrossFilter.CrossFilter<Scholarships.IxfData>) => xf.dimension(d => [
				d.ScholarshipType || 'na',
				this.lookups.byCode("scholarshipTypes",d.ScholarshipType,"N") || d.ScholarshipType || 'na',
				"Scholarship Type"]);

			this.dimScholarshipType = d(this.xf);
			this.dimScholarshipTypeP = d(this.xfP);


			d = (xf: CrossFilter.CrossFilter<Scholarships.IxfData>) => xf.dimension(d => [
				d.MajorCode || 'na',
				d.Major || d.MajorCode || 'na',
				"Major"]);

			this.dimMajor = d(this.xf);
			this.dimMajorP = d(this.xfP);


			d = (xf: CrossFilter.CrossFilter<Scholarships.IxfData>) => xf.dimension(d => [
				d.StatusCode || 'na',
				d.Status || d.StatusCode || 'na',
				"Status"]);
			this.dimStatus = d(this.xf);
			this.dimStatusP = d(this.xfP);

			d = (xf: CrossFilter.CrossFilter<Scholarships.IxfPaymentsData>) => xf.dimension(d => [
				d.PaymentYear || 'na',
				d.PaymentYear ||  'na',
				"Payment Year"]);
			this.dimPaymentYearP = d(this.xfP);



			this.grpYear = this.xFilter.xReduce(this.dimYear, d => d.ScholarshipType
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Number"));

			this.grpYearP = this.xFilter.xReduce(this.dimYearP, d => d.ScholarshipType
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Amount"));

			this.grpScholarshipType = this.xFilter.xReduce(this.dimScholarshipType, d => d.ScholarshipType
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Number"));
			this.grpScholarshipTypeP = this.xFilter.xReduce(this.dimScholarshipType, d => d.ScholarshipType
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Number"));

			this.grpMajor = this.xFilter.xReduce(this.dimMajor, d => d.Major
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Number"));

			this.grpStatus = this.xFilter.xReduce(this.dimStatus, d => d.Status
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Number"));

			this.grpPaymentYearP = this.xFilter.xReduce(this.dimPaymentYearP, d => d.ScholarshipType
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Amount"));

			this.grpPaymentYearTierP = this.xFilter.xReduce(this.dimPaymentYearP, d => d.MajorGroup
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Amount"));

			this.grpPaymentYearRoundP = this.xFilter.xReduce(this.dimPaymentYearP, d => d.Round
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Amount"));

			this.grpPaymentYearTertiaryGradeP = this.xFilter.xReduce(this.dimPaymentYearP, d => d.TertiaryGrade
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Amount"));


			this.dashboard = this;			// expose the dashboard as intialised
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			tables: "<",			// array of data tables
			options: "<",			// get the options
		};
		public controller = ScholarshipDashboard;
		public controllerAs = "vm"
		public templateUrl: string = "scholarship/dashboard";
	}



	angular
		.module("pineapples")
		.component("scholarshipDashboard", new Component());

	// component options for dashboard children
	export class ScholarshipDashboardComponent extends DashboardChildComponent implements ng.IComponentOptions {
		public get feature() {
			return "scholarship";
		}

		constructor(public controller: any, templateUrl) {
			super(controller, templateUrl);
			// have to copy this, before changing, becuase it points to a constant
			this.bindings = angular.copy(this.bindings);
			this.bindings["onClick"] = "&";
			this.bindings["highlight"] = "<";
		}
	}
}
