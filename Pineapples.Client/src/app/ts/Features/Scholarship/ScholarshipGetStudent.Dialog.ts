﻿namespace Pineapples.Scholarships {


	/**
	 * Invoke the status change dialog
	 * Thus function is assigned to the onEnter event of the state
	 * assigned to this dialog
	 * Note that this is a shortcut way of assigning the transition event onEnter - they are the same thing
	 * Resolves are available and can be pushed into the function
	 * The function returns a rejected promise always - this cancels the transition
	 * and leaves the current state unchanged
	 * the parameters to the function are injectables defined by the functions $inject property
	 * @param $q 
	 * @param mdDialog
	 * @param application
	 */
	export function invokeGetStudent(
		transition,
		mdDialog: ng.material.IDialogService,
		model: Scholarship) {

		if (transition.params().studentid)
			return;
		let options: ng.material.IDialogOptions = {
			locals: {
				model: model
			},
			controller: DialogController,
			controllerAs: 'vm',
			bindToController: true,
			clickOutsideToClose: true,
			templateUrl: "scholarship/getstudentdialog"

		}
		mdDialog.show(options)
			.then((studentId) => {
				const paramsCopy = angular.copy(transition.params());
				const stateService = transition.router.stateService;

				if (studentId != null) {
					// supply 'en' as a default language
					paramsCopy.studentid = studentId;
					stateService.go(transition.to(), paramsCopy);
				}
			}, () => {
					const stateService = transition.router.stateService;
					stateService.go(transition.from());
			});
		
	

	}
	invokeGetStudent.$inject = ["$transition$", "$mdDialog", "model"];

	class DialogController extends Sw.Component.MdDialogController {
		static $inject = ["$mdDialog", "ApiUi", "Restangular", "Lookups"];
		constructor(mdDialog: ng.material.IDialogService, apiUi: any, private restangular: restangular.IService
			, private lookups: Sw.Lookups.LookupService
			, private api) {
			super(mdDialog);
		}

		public isEditing = true;
		public isWaiting = false;
		public model: Scholarship;
		public message: string;

		public studentId;
		public student;
		public select() {
			this.returnSuccess(this.studentId);
		}

		public selectedHandler = (item) => {
			this.student = item;
		}
		public save() {
		
			this.message = "Saving...";
			this.isWaiting = true;
			let promise: ng.IPromise<any>;
			//if (this.model._isNew()) {
			//	//promise = this.restangular.all("scholarshipsstudy").post(this.model);
			//	promise = this.model.post();
			//} else {
			//	promise = this.model.put();
			//}
			this.model.save()
				.then((data) => {
					this.message = "Saved";
					this.returnSuccess(this.studentId);
				}, (error) => { this.message = error; })
				.finally(() => { this.isWaiting = false; });
		}
	}
}