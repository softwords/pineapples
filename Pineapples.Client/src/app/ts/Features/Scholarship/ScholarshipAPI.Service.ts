﻿namespace Pineapples.Api {
	const ENTITY = "scholarships";
	class apiService {

		static $inject = ["$q", "$http", "Restangular"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService) {
		}

		public read(id) {
			return this.restangular.one(ENTITY, id).get();
		}

		public new(studentid) {
		//	//return this.restangular.one(ENTITY, 0).get();
		//	let e = new Pineapples.Scholarships.Student({
		//		stuID: null
		//	});
		//	this.restangular.restangularizeElement(null, e, ENTITY);
		//	return this.$q.resolve(e);
			return this.restangular.one(ENTITY, "new").get({studentid: studentid})
				.then((data) => {
					data.fromServer = false;
					return data;
				});
		}

		public filterPaged(fltr) {
			return this.http.post(`api/${ENTITY}/collection/filter`, fltr)
				.then(response => (response.data));
		}

		public getEditableTables() {
			return this.http.get("api/lookups/scholarshipeditabletables")
				.then(response => response.data);
		}
	}

	angular.module("pineapplesAPI")
		.service("scholarshipsAPI", apiService);
}