﻿namespace Pineapples.Teachers {


	class Controller {
		public teacher: Teacher;

		static $inject = [ "$mdDialog", "Restangular", "documentsAPI"]
		constructor(

			public mdDialog: ng.material.IDialogService
			, public Restangular: restangular.IService
			, docAPI) {
	
		}

		public $onChanges(changes) {
		}
		public newDoc() {
			//this.state.go("site.teachers.upload", { id: this.teacherId });
			this._uploadDialog().then(() => {
			}, (error) => {
			});
		}

		private _uploadDialog() {
			let options: ng.material.IDialogOptions = {
				locals: { teacher: this.teacher },
				controller: TeacherLinkUploadController,
				controllerAs: 'vm',
				bindToController: true,
				templateUrl: "teacherlink/uploaddialog"
			}
			return this.mdDialog.show(options);
		}
	
	}

  /**
   * Controller for the edit Dialog
   */
	class editController extends Sw.Component.ComponentEditController {
		static $inject = ["$mdDialog", "ApiUi"];
		constructor(public mdDialog: ng.material.IDialogService
			, apiUi: Sw.Api.IApiUi) {
			super(apiUi);
			this.isEditing = true;
		}
		public teacher: Teacher;

		public onModelUpdated(newData: any) {
			super.onModelUpdated(newData);
			this.teacher.tPhoto = newData.tPhoto;
		}

		public closeDialog() {
			this.mdDialog.cancel();
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				teacher: "<",
				buttonText: "@"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "library/newlink";
		}
	}
	angular
		.module("pineapples")
		.component("teacherNewLink", new Component());

}
