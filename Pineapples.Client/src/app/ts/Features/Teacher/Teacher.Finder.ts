namespace Pineapples.Teachers {


	class Controller {

		static $inject = ["$http", "$q", "Lookups"]
		public constructor(private http: ng.IHttpService, private $q: ng.IQService, private lookups: Pineapples.Lookups.LookupService) {
			this.url = "api/selectors/teacher";
		}

		public $onInit() {
			this.ngModelCtlr.$render = () => {
				this.selectedItem = this.ngModelCtlr.$viewValue ?
					{
						C: this.ngModelCtlr.$viewValue,
						// A sad hack to get the teacher name (i.e. use a newly added cached list of all teachers). 
						// I could not get it to work by retrieving the teacher name using the teacherAPI
						// due to my lack of AngularJS/TypeScript/Javascript skills.
						N: this.lookups.byCode("teacherNames", this.ngModelCtlr.$viewValue, "N")
					} : null;
			}
		}

		public url;
		public searchTextChange() {
		}
		public searchText;
		public selectedItem;

		public searchResults: any[];
		private _lastFetchString = "";
		private _fetchData: any[] = [];

		public ngModelCtlr: ng.INgModelController;

		public selectedHandler: ({ teacher }) => void;

		//public doSearch(s: string) {
		//	if (s.length < 3) {
		//		// if we don't have 3 chars don;t do anything
		//		this.searchResults = [];
		//		return;
		//	}
		//	s = s.toLowerCase();
		//	let l = this._fetchData.length;
		//	// we don;t need to fetch:
		//	// the current search is the last fetch
		//	if (s === this._lastFetchString) {
		//		this.searchResults = this._filterLocal(s);
		//		return;
		//	}
		//	// the current search is backspaced and there is already a full list
		//	if (_.startsWith(this._lastFetchString, s) && l === 30) {
		//		this.searchResults = this._filterLocal(s);
		//		return;
		//	}
		//	// the current search string is longer than  the last search and that search returned < 30 records
		//	// ie we have everything
		//	if (_.startsWith(s, this._lastFetchString) && l > 0 && l < 30) {
		//		this.searchResults = this._filterLocal(s);
		//		return;
		//	}
		//	// return the promise with its resolution
		//	this.http.post(this.url, "\"" + s + "\"")
		//		.then((response) => {
		//			this._fetchData = response.data["ResultSet"][0];
		//			this._lastFetchString = s;
		//			this.searchResults = this._fetchData;
		//		});
		//};

		public doSearch(s: string) {
			if (s.length < 3) {
				// if we don't have 3 chars don;t do anything
				return [];
			}
			s = s.toLowerCase();
			let l = this._fetchData.length;
			// we don;t need to fetch:
			// the current search is the last fetch
			if (s === this._lastFetchString) {
				return this.searchResults = this._filterLocal(s);
			}
			// the current search is backspaced and there is already a full list
			if (_.startsWith(this._lastFetchString, s) && l === 30) {
				return this.searchResults = this._filterLocal(s);
			}
			// the current search string is longer than  the last search and that search returned < 30 records
			// ie we have everything
			if (_.startsWith(s, this._lastFetchString) && l > 0 && l < 30) {
				return this.searchResults = this._filterLocal(s);
			}
			// return the promise with its resolution
			return this.http.post(this.url, "\"" + s + "\"")
				.then((response) => {
					this._fetchData = response.data["ResultSet"][0];
					this._lastFetchString = s;
					return this.searchResults = this._fetchData;
				});
		};

		private _filterLocal = (s: string) => {

			let k = s.indexOf(",");
			if (k > 0) {
				let sf = s.substring(0, k - 1).trim();
				let sg = s.substring(k + 1).trim();
				return _.filter(this._fetchData, d => d.F.toLowerCase().indexOf(sf) >= 0 &&
					d.G.toLowerCase().indexOf(sg) >= 0);

			}
			k = s.indexOf(" ");
			if (k > 0) {
				let sg = s.substring(0, k - 1).trim();
				let sf = s.substring(k + 1).trim();
				return _.filter(this._fetchData, d => d.F.toLowerCase().indexOf(sf) >= 0 &&
					d.G.toLowerCase().indexOf(sg) >= 0);

			}
			return _.filter(this._fetchData, d => d.N.toLowerCase().indexOf(s) >= 0 ||
				d.C.toLowerCase().indexOf(s) >= 0);
		};

		public onSelected(item) {
			// now we have a unique element that should be returned to the paramChanged event handler
			if (this.ngModelCtlr) {
				this.ngModelCtlr.$setViewValue(item ? item.C : null);
				this.selectedHandler({ teacher: item })
			}

		};

		public onKey($event) {
			if ($event.which === 13 && this.searchResults.length) {
				this.selectedItem = this.searchResults[0];
				this.onSelected(this.selectedItem);
			}
		}

	}

	class ComponentOptions implements ng.IComponentOptions {
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "teacher/finder";
		public require = {
			ngModelCtlr: "ngModel"
		};
		public bindings = {
			ngDisabled: "<",
			ngRequired: "<",
			selectedHandler: "&onSelected"
		}
	}


	angular
		.module("pineapples")
		.component("teacherFinder", new ComponentOptions());
}