﻿namespace Pineapples.Dashboards.Teachers {

	export class Controller extends DashboardChild implements IDashboardChild {


		protected defaultTitle = "Certified and Qualified";

		public get viewOptions() {
			//return [];  future use?
			return null;
		}

		// reference to the host dashboard
		public dashboard;

		// mechanism to allow user override of palette in a chart

		
		protected defaultPalette = ["#00B050", "#FFC000", "#FF00C0", "#D0D0D0"];

		// callback from chart is passed the entire row
		public sortBy = (rowa: any[], rowb: any[]) => this.ageSorter(rowa[0],rowb[0]);
		public sortDescending = false;

		public touch;

		public dimension: CrossFilter.Dimension<any, any>;
		public group;
		public chartData;
		public charttype = "h";

		public mirrors = ["F", "C_F", "Q_F", "CQ_F" ]; // the F fields are moved over the axis

		public onDashboardReady() {
			// in this case, because we want an unusual group not likely to be of
			// use to other dashboard items, we get the dimension from the dashboard,
			// and create the group here
			// if the group was more general purpose, we could create it in the dashboard,
			// so it would be available to others
			this.makeGroup();		
			// note this is only called from $onInit, becuase this object never changes
			// to notify the chart that the data has changed (ie change of option, or change of selected view)
			// we just use 'touch' binding on-row-col-chart to force a data transformation and redraw
			this.makeChartData();

		}

		private makeGroup() {
			// reducer is the function passed to xReduce - it describes the structure of each base level 
			// node inn the 'value' property in group.all()
			let reducer = (row) => ({
				M: {
					Num: row.NumTeachersM,
					Certified: row.CertifiedM,
					Qualified: row.QualifiedM,
					CertQual: row.CertQualM
				},
				F: {
					Num: row.NumTeachersF,
					Certified: row.CertifiedF,
					Qualified: row.QualifiedF,
					CertQual: row.CertQualF
				},
				Tot: {
					Num: (row.NumTeachersM || 0) + (row.NumTeachersF || 0),
					Certified: (row.CertifiedM||0) + (row.CertifiedF||0),
					Qualified: row.QualifiedM + row.QualifiedF,
					CertQual: row.CertQualM + row.CertQualF
				}
			});
			this.group = (<Sw.xFilter.XFilter>this.dashboard.xFilter).xReduce(
				this.dimension,
				row => row.DistrictCode,
				reducer
			);
			console.log("AgeQualChart - makeGroup");
			console.log(this.group.all());
		}

		public makeChartData() {
			/** chartData format is 'AnnotatedGroup'
			 * ie it is a crossfilter group, with some extra info to describe how it is presented in the chart:
			*  group : the crossfilter group itself
			*  columns: an array of column names, or a function returning an array of column names
			*     if a function, it is evaluated on each row of group.all() - each of these is a key-value pair
			*		 , but the value may be a complex object, especially if created by xReduce
		  *	 valueAccessor: thisfunction extracts the column value from the key value pair:
		  *	 arguments row - the key value object
		  *	           columnName - taken from 'columns'
		  *	           columnIndex - index in columns array
		  *	 EchartTransformer can explicitly support this format, so it can be passed directly to row-col-chart as 
		  *	 the dataset binding
			*/
			// put some accessors here for now
			let NumAccessor = (row, columnName, columnIdx) => (row.value.Tot && row.value.Tot[columnName]) ?
				row.value.Tot[columnName].Num : 0;

			let qcColumns = ["CQ_F", "Q_F", "C_F", "F", "CQ_M", "Q_M", "C_M", "M"]
			let qcAccessor = (row, columnName, columnIdx) => {

				let base = (columnIdx < 4) ? row.value.Tot?.F : row.value.Tot?.M;
				if (!base) {
					return 0;
				}
				// to meaningfully chart these together, we need to make
				// the categories exclusive
				var value;
				switch (columnIdx % 4) {
					case 0:
						value = base.CertQual;
						break;
					case 1:
						value = base.Qualified - base.CertQual;
						break;
					case 2:
						value = base.Certified - base.CertQual;
						break;
					case 3:
						value = base.Num - base.Certified -base.Qualified + base.CertQual;
						break;
				}
				return value;
			}

			this.chartData = {
				group: this.group,
				columns: qcColumns, 
				valueAccessor: qcAccessor
			};
		}

		/**
		 * Sorter for the age groups returned in the data 
		 * These may ranges (30-39) or one sided ( > 60, < 20, 60+)
		 */
		public ageSorter = (a: string, b: string) => {
			if (a == b) {
				return 0;
			}
			// deal with null or empty values
			if (!a) {
				return b ? -1 : 0;
			}
			if (!b) {
				return 1;
			}
			if (a[0] == "<" || b[0] == ">") {
				return -1;
			}
			if (a[0] == ">" || b[0] == "<") {
				return 1;
			}
			return Sw.Charts.sortComparator(false)(a, b);
		}
		public columnTranslator = {
			CQ_F: ["Certified and Qualified","Female"],
			C_F: ["Certified (not qualified)", "Female"],
			Q_F: ["Qualified (not certified)", "Female"],
			F: ["Other", "Female"],
			CQ_M: ["Certified and Qualified", "Male"],
			C_M: ["Certified (not qualified)", "Male"],
			Q_M: ["Qualified (not certified)", "Male"],
			M: ["Other","Male"]
		}
		// show the absolute value, not the negative value in the tooltip
		// if tooltipper does not return a value, ChartRowcol executes a default format
		// using datum
		// therfore, the tooltipper can change the values in datum to see these applied in the 
		// standard layout
		public tooltipper(datum) {
			let t = this.columnTranslator[datum.series];
			datum.series = t[0];
			datum.item = `${t[1]} ${datum.item}`;
		}

		public onChartRender(option:echarts.EChartOption, echart) {
			
			let o:echarts.EChartOption = {
				yAxis: {
					name: "\u21e6 FEMALE                          MALE \u21e8",
					nameLocation: "start",
					nameTextStyle: {
						color: "darkgrey"
					},
					nameGap: 25
				},
				legend: {
					formatter: (name) => this.columnTranslator[name][0]
				}
			}
			angular.merge(option, o);
			// have to replace, not merge
			option.legend.data = ["CQ_F", "Q_F", "C_F", "F"];
		}
		
		public onOptionChange(optionchange) {
			this.filters = this.options.descriptionExcluding("Year");
			this.refreshChart();
		}

		public onChildSelected() {
			if (!this.isSelected()) {
				this.drillfltr = null;
				this._selectedcell = "";
			}
		}

		private _selectedViewOption = 1;

		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.touch = Date.now();

			}
		}

		private drillfltr;
		private redrill() {
			if (this.drillfltr && this.isSelected()) {
				angular.merge(this.drillfltr, {
					Authority: this.options.selectedAuthority,
					AuthorityGroup: this.options.selectedAuthorityGovt,
					SchoolType: this.options.selectedSchoolType,
					BestUpTo: this.options.selectedYear
				});
				if (this.drillfltr.InspYear) {
						this.drillfltr.InspYear = this.options.selectedYear;
				}
				this.getDrillData();
			}
		}
		public drilldown(level, districtCode, cumulative = "") {

			this.drillfltr = {
				InspResult: level,
				District: districtCode,
				Authority: this.options.selectedAuthority,
				AuthorityGroup: this.options.selectedAuthorityGovt,
				SchoolType: this.options.selectedSchoolType,
				InspYear: cumulative ? null : this.options.selectedYear,
				BestUpTo: this.options.selectedYear
			};
			this._selectedcell = `${level}|${districtCode}|${cumulative}`;
			this.getDrillData()
				.then(() => {
					this.setSelected();
			});;
		}
		private getDrillData() {
			return this.http.post("api/warehouse/accreditations/filter", this.drillfltr)
				.then(result => {
					//this.accreditations = result.data["ResultSet"];
				});

		}

		private _selectedcell: string;
		public isSelectedCell(level: string, districtCode: string, cumulative = "") {
			return this.isSelected() && this._selectedcell == `${level}|${districtCode}|${cumulative}`;
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			dashboard: "<",
			dimension: "<",
			options: "<",			
			optionChange: "<",
			selectedChild: "<",
			reportPath: "@",
			// user configurable: ie specific dashboard html for different places
			// can change set the title or palette according to local needs/standards
			userPalette: "<colors",
			userTitle: "@headingTitle"

		};
		public templateUrl: string;
		public controllerAs: string = 'vm';

		constructor(public controller: any, templateUrl) {
			this.templateUrl = "teacher/dashboard/component/" + templateUrl;
		}
	}

  angular
		.module("pineapples")
		.component("teacherAgeQualChart", new Component(Controller, "ageQualChart"))

}
