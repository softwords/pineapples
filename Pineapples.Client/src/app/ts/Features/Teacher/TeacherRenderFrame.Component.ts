﻿namespace Pineapples.Teachers {

	angular
		.module("pineapples")
		.component("teacherRenderFrame", new Sw.Component.RenderFrameComponent("teacher"));
}