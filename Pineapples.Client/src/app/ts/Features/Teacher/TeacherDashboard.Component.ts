﻿
namespace Pineapples.Dashboards {

	export namespace Teachers {
		export interface IxfData {
			SurveyYear: number;
			AgeGroup: string;
			DistrictCode: string;
			IslandCode: string;
			RegionCode: string;
			AuthorityCode: string;
			AuthorityGovtCode: string;
			SchoolTypeCode: string;
			Sector: string;
			ISCEDSubClassCode: string;
			NumTeachersM: number;
			NumTeachersF: number;
			NumTeachersNA: number;
			CertifiedM: number;
			CertifiedF: number;
			CertifiedNA: number;
			QualifiedM: number;
			QualifiedF: number;
			QualifiedNA: number;
			CertQualM: number;
			CertQualF: number;
			CertQualNA: number;
		}

		export function vaNumTeachers(d: IxfData) {
			return d.NumTeachersF + d.NumTeachersM + d.NumTeachersNA;
		};

		export function vaDetail(d: IxfData) {
			return {
				'NumTeachers@Male': d.NumTeachersM,
				'NumTeachers@Female': d.NumTeachersF,
				'NumTeachers@NA': d.NumTeachersNA,
				'NumTeachers@Total': d.NumTeachersF + d.NumTeachersM + d.NumTeachersNA,
				'Certified@Male': d.CertifiedM,
				'Certified@Female': d.CertifiedF,
				'Certified@NA': d.CertifiedNA,
				'Certified@Total': d.CertifiedF + d.CertifiedM + d.CertifiedNA,
				'Qualified@Male': d.QualifiedM,
				'Qualified@Female': d.QualifiedF,
				'Qualified@NA': d.QualifiedNA,
				'Qualified@Total': d.QualifiedF + d.QualifiedM + d.QualifiedNA,
				'CertQual@Male': d.CertQualM,
				'CertQual@Female': d.CertQualF,
				'CertQual@NA': d.CertQualNA,
				'CertQual@Total': d.CertQualF + d.CertQualM + d.CertQualNA,
			};
		};
	}

	export class TeachersDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

		// if a or b is 0 or null, return '' else return sum(a,b)
		private checkThenSum = (a, b, c) => x => !x[a] || !x[b] || !x[c] || x[a] === 0 || x[b] === 0 || x[c] === 0 ? '' : x[a] + x[b] + x[c];


		public chartAccessors = {
			Total: {
				columns: ['M', 'F', 'NA'],
				valueAccessor: (row, col, i) => (row.value.Tot) ? row.value.Tot[`NumTeachers${col}`] : 0
			},
			Certified: {
				columns: ['M', 'F', 'NA'],
				valueAccessor: (row, col, i) => (row.value.Tot) ? row.value.Tot[`Certified${col}`] : 0
			},
			Qualified: {
				columns: ['M', 'F', 'NA'],
				valueAccessor: (row, col, i) => (row.value.Tot) ? row.value.Tot[`Qualified${col}`] : 0
			},
			"Certified and Qualified": {
				columns: ['M', 'F', 'NA'],
				valueAccessor: (row, col, i) => (row.value.Tot) ? row.value.Tot[`CertQual${col}`] : 0
			}
		}

		public totalAccessors = {
			Total: this.checkThenSum('NumTeachersM', 'NumTeachersF', 'NumTeachersNA'),
			Certified: this.checkThenSum('CertifiedM', 'CertifiedF', 'CertifiedNA'),
			Qualified: this.checkThenSum('QualifiedM', 'QualifiedF', 'QualifiedNA'),
			CertQual: this.checkThenSum('CertQualM', 'CertQualF', 'CertQualNA'),
		}

		public accessors = {
			NumTeachersM: (d) => d.NumTeachersM,
			NumTeachersF: (d) => d.NumTeachersF,
			NumTeachersNA: (d) => d.NumTeachersNA,
			NumTeachers: this.checkThenSum('NumTeachersF', 'NumTeachersM', 'NumTeachersNA'),
			CertifiedM: (d) => d.CertifiedM,
			CertifiedF: (d) => d.CertifiedF,
			CertifiedNA: (d) => d.CertifiedNA,
			Certified: this.checkThenSum('CertifiedF', 'CertifiedM', 'CertifiedNA'),
			QualifiedM: (d) => d.QualifiedM,
			QualifiedF: (d) => d.QualifiedF,
			QualifiedNA: (d) => d.QualifiedNA,
			Qualified: this.checkThenSum('QualifiedF', 'QualifiedM', 'QualifiedNA'),
			CertQualM: (d) => d.CertQualM,
			CertQualF: (d) => d.CertQualF,
			CertQualNA: (d) => d.CertQualNA,
			CertQual: this.checkThenSum('CertQualF', 'CertQualM', 'CertQualNA'),
		}

		private fullReducer = d => ({
			NumTeachersM: d.NumTeachersM,
			NumTeachersF: d.NumTeachersF,
			NumTeachersNA: d.NumTeachersNA,
			CertifiedM: d.CertifiedM,
			CertifiedF: d.CertifiedF,
			CertifiedNA: d.CertifiedNA,
			QualifiedM: d.QualifiedM,
			QualifiedF: d.QualifiedF,
			QualifiedNA: d.QualifiedNA,
			CertQualM: d.CertQualM,
			CertQualF: d.CertQualF,
			CertQualNA: d.CertQualNA,
		})




		public filterString = (...exclude) => `
       ${!_(exclude).includes('Year') && this.options.selectedYear || ''}
       ${!_(exclude).includes('District') && this.lookups.byCode('districts', this.options.selectedDistrict, 'N') || ''}
       ${!_(exclude).includes('Island') && this.lookups.byCode('islands', this.options.selectedIsland, 'N') || ''}
       ${!_(exclude).includes('Region') && this.lookups.byCode('regions', this.options.selectedRegion, 'N') || ''}
       ${!_(exclude).includes('Authority') && this.lookups.byCode('authorities', this.options.selectedAuthority, 'N') || ''}
       ${!_(exclude).includes('AuthorityGovt') && this.lookups.byCode('authorityGovts', this.options.selectedAuthorityGovt, 'N') || ''}
       ${!_(exclude).includes('SchoolType') && this.lookups.byCode('schoolTypes', this.options.selectedSchoolType, 'N') || ''}
       ${!_(exclude).includes('ISCEDSubClass') && this.lookups.byCode('iscedLevelsSub', this.options.selectedISCEDSubClass, 'N') || ''}
      `

		//// override with strongly typed crossfilter
		//public xf: CrossFilter.CrossFilter<Enrolments.IxfData>;

		public dimSurveyYear: CrossFilter.Dimension<Teachers.IxfData, number>;
		public dimDistrictCode: CrossFilter.Dimension<Teachers.IxfData, string>;
		public dimIslandCode: CrossFilter.Dimension<Teachers.IxfData, string>;
		public dimRegionCode: CrossFilter.Dimension<Teachers.IxfData, string>;
		public dimAuthorityCode: CrossFilter.Dimension<Teachers.IxfData, string>;
		public dimAuthorityGovtCode: CrossFilter.Dimension<Teachers.IxfData, string>;
		public dimSchoolTypeCode: CrossFilter.Dimension<Teachers.IxfData, string>;
		public dimISCEDSubClassCode: CrossFilter.Dimension<Teachers.IxfData, string>;

		public dimAge: CrossFilter.Dimension<Teachers.IxfData, number>;

		// groups
		// note we have both a chart and simple-table on each dimension
		// the chart needs the complex value in xxxDetail
		// but the simple-table cannot process this - it needs a scalar value for 'value'
		// so - pairs of groups created
		public grpDistrictCode: CrossFilter.Group<Teachers.IxfData, string, number>;
		public grpDistrictCodeDetail: CrossFilter.Group<Teachers.IxfData, string, any>;
		public grpIslandCode: CrossFilter.Group<Teachers.IxfData, string, number>;
		public grpIslandCodeDetail: CrossFilter.Group<Teachers.IxfData, string, any>;
		public grpRegionCode: CrossFilter.Group<Teachers.IxfData, string, number>;
		public grpRegionCodeDetail: CrossFilter.Group<Teachers.IxfData, string, any>;
		public grpAuthorityCode: CrossFilter.Group<Teachers.IxfData, string, number>;
		public grpAuthorityCodeDetail: CrossFilter.Group<Teachers.IxfData, string, any>;
		public grpAuthorityGovtCode: CrossFilter.Group<Teachers.IxfData, string, number>;
		public grpAuthorityGovtCodeDetail: CrossFilter.Group<Teachers.IxfData, string, any>;

		// cross tabulated groups
		public tabSchoolTypeByDistrict;
		public tabDistrictBySchoolType;
		public tabSchoolTypeByIsland;
		public tabIslandBySchoolType;
		public tabSchoolTypeByRegion;
		public tabRegionBySchoolType;
		public tabSchoolTypeByAuthorityGovt;
		public tabAuthorityGovtBySchoolType;

		public tabISCEDSubClassByDistrict;
		public tabDistrictByISCEDSubClass;
		public tabISCEDSubClassByIsland;
		public tabIslandByISCEDSubClass;
		public tabISCEDSubClassByRegion;
		public tabRegionByISCEDSubClass;
		public tabISCEDSubClassByAuthorityGovt;
		public tabAuthorityGovtByISCEDSubClass;

		public onOptionChange(data: IOptionChangeData, sender) {
			console.log('TeacherDashboard.onOptionChange', data, sender)

			if (data.selectedYear) {
				this.dimSurveyYear.filter(this.options.selectedYear);
			}
			if (data.selectedDistrict) {
				this.dimDistrictCode.filter(this.options.selectedDistrict);
			}
			if (data.selectedIsland) {
				this.dimIslandCode.filter(this.options.selectedIsland);
			}
			if (data.selectedRegion) {
				this.dimRegionCode.filter(this.options.selectedRegion);
			}
			if (data.selectedAuthorityGovt) {
				this.dimAuthorityGovtCode.filter(this.options.selectedAuthorityGovt);
			}
			if (data.selectedAuthority) {
				this.dimAuthorityCode.filter(this.options.selectedAuthority);
			}
			if (data.selectedSchoolType) {
				this.dimSchoolTypeCode.filter(this.options.selectedSchoolType);
			}
			if (data.selectedISCEDSubClass) {
				this.dimISCEDSubClassCode.filter(this.options.selectedISCEDSubClass);
			}
			// always call the super version so that the clients get to know about the option change as well
			super.onOptionChange(data, sender);
		}

		public createDimensions() {
			//  // create the dimensions

			// Lookup by code like others now...
			//this.dimAuthorityGovtCode = this.xf.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovtCode);
			//	return ag ? ag.N : null;
			//});

			this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
			this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);
			this.dimIslandCode = this.xf.dimension(d => d.IslandCode);
			this.dimRegionCode = this.xf.dimension(d => d.RegionCode);
			this.dimAuthorityCode = this.xf.dimension(d => d.AuthorityCode);
			this.dimAuthorityGovtCode = this.xf.dimension(d => d.AuthorityGovtCode);
			this.dimSchoolTypeCode = this.xf.dimension(d => d.SchoolTypeCode);
			this.dimISCEDSubClassCode = this.xf.dimension(d => d.ISCEDSubClassCode);
			this.dimAge = this.xf.dimension(d => d.AgeGroup);


			this.grpDistrictCode = this.dimDistrictCode.group().reduceSum(Teachers.vaNumTeachers);
			this.grpDistrictCodeDetail = this.xFilter.xReduce(
				this.dimDistrictCode,
				x => x.DistrictCode,
				this.fullReducer);

			this.grpIslandCode = this.dimIslandCode.group().reduceSum(Teachers.vaNumTeachers);
			this.grpIslandCodeDetail = this.xFilter.xReduce(
				this.dimIslandCode,
				x => x.IslandCode,
				this.fullReducer);

			this.grpRegionCode = this.dimRegionCode.group().reduceSum(Teachers.vaNumTeachers);
			this.grpRegionCodeDetail = this.xFilter.xReduce(
				this.dimRegionCode,
				x => x.RegionCode,
				this.fullReducer);

			this.grpAuthorityCode = this.dimAuthorityCode.group().reduceSum(Teachers.vaNumTeachers);
			this.grpAuthorityCodeDetail = this.xFilter.xReduce(
				this.dimAuthorityCode,
				x => x.AuthorityCode,
				this.fullReducer);

			this.grpAuthorityGovtCode = this.dimAuthorityGovtCode.group().reduceSum(Teachers.vaNumTeachers);
			this.grpAuthorityGovtCodeDetail = this.xFilter.xReduce(
				this.dimAuthorityGovtCode,
				x => x.AuthorityGovtCode,
				this.fullReducer);

			this.tabDistrictBySchoolType = this.xFilter.xReduce(
				this.dimDistrictCode,
				x => x.SchoolTypeCode,
				Teachers.vaDetail);

			this.tabSchoolTypeByDistrict = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.DistrictCode,
				Teachers.vaDetail);

			this.tabRegionBySchoolType = this.xFilter.xReduce(
				this.dimRegionCode,
				x => x.SchoolTypeCode,
				Teachers.vaDetail);

			this.tabSchoolTypeByRegion = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.RegionCode,
				Teachers.vaDetail);

			this.tabIslandBySchoolType = this.xFilter.xReduce(
				this.dimIslandCode,
				x => x.SchoolTypeCode,
				Teachers.vaDetail);

			this.tabSchoolTypeByIsland = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.IslandCode,
				Teachers.vaDetail);

			this.tabAuthorityGovtBySchoolType = this.xFilter.xReduce(
				this.dimAuthorityGovtCode,
				x => x.SchoolTypeCode,
				Teachers.vaDetail);

			this.tabSchoolTypeByAuthorityGovt = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.AuthorityGovtCode,
				Teachers.vaDetail);

			this.tabDistrictByISCEDSubClass = this.xFilter.xReduce(
				this.dimDistrictCode,
				x => x.ISCEDSubClassCode,
				Teachers.vaDetail);

			this.tabISCEDSubClassByDistrict = this.xFilter.xReduce(
				this.dimISCEDSubClassCode,
				x => x.DistrictCode,
				Teachers.vaDetail);

			this.tabIslandByISCEDSubClass = this.xFilter.xReduce(
				this.dimIslandCode,
				x => x.ISCEDSubClassCode,
				Teachers.vaDetail);

			this.tabISCEDSubClassByIsland = this.xFilter.xReduce(
				this.dimISCEDSubClassCode,
				x => x.IslandCode,
				Teachers.vaDetail);

			this.tabRegionByISCEDSubClass = this.xFilter.xReduce(
				this.dimRegionCode,
				x => x.ISCEDSubClassCode,
				Teachers.vaDetail);

			this.tabISCEDSubClassByRegion = this.xFilter.xReduce(
				this.dimISCEDSubClassCode,
				x => x.RegionCode,
				Teachers.vaDetail);

			this.tabAuthorityGovtByISCEDSubClass = this.xFilter.xReduce(
				this.dimAuthorityCode,
				x => x.ISCEDSubClassCode,
				Teachers.vaDetail);

			this.tabISCEDSubClassByAuthorityGovt = this.xFilter.xReduce(
				this.dimISCEDSubClassCode,
				x => x.AuthorityGovtCode,
				Teachers.vaDetail);

			// set the surveyYear to be the most recent in the warehouse data, rather than the most recent surveyYear in the lookups
			// we dont want to get empty data on the initial display if the warehouse is not yet generated for the current year
			if (!this.options.selectedYear) {
				this.options.selectedYear = this.dimSurveyYear.top(1)[0].SurveyYear;;
			}
		}

		public vaDetail = Teachers.vaDetail;

		public lookupFormatter(lookupset) {
			return (code) => this.lookups.byCode(lookupset, code, "N");
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				table: "<",
				options: "<"			// get the options
			};
			this.controller = TeachersDashboard;
			this.controllerAs = "vm";
			this.templateUrl = `teacher/Dashboard`;
		}
	}

	angular
		.module("pineapples")
		.component("teachersDashboard", new Component());

}
