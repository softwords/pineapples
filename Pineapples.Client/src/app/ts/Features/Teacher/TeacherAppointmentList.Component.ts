﻿namespace Pineapples.Teachers {

  interface IBindings {
    appointments: any;
  }

  class Controller implements IBindings {
    public appointments: any;
 
    static $inject = ["$mdDialog", "$location", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, private $location: ng.ILocationService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.appointments) {
      }
    }

		// not used - there is no template "teacher/appointmentdialog"
    public showAppointment(appointment) {
      let options: any = {
        clickOutsideToClose: true,
        templateUrl: "teacher/appointmentdialog",
        controller: DialogController,
        controllerAs: "vm",
        bindToController: true,
        locals: {
          appointment: appointment
          //payslip: ["teacherApi", (api) => {
          //  return api.payslip(id);
          //}]
        }

      }
      this.mdDialog.show(options);
    }

 
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        appointments: '<',
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "teacher/appointmentlist";
    }
  }

  // popup dialog for audit log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog", "appointment"];
    constructor(mdDialog: ng.material.IDialogService, public payslip) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("componentTeacherAppointmentList", new Component());
}
