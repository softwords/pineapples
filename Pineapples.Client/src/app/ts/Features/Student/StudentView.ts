﻿namespace Pineapples.Students {
	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'stuCardID',
				name: 'Student Card ID',
				displayName: 'Student Card ID',
				editable: false,
				pinnedLeft: true,
				width: 140,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"],
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm", "stuID")
			},
			{
				field: 'stuGiven',
				name: 'Given Name',
				displayName: 'Given Name',
				pinnedLeft: true,
				width: 160,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'stuFamilyName',
				name: 'Family Name',
				displayName: 'Family Name',
				pinnedLeft: true,
				width: 160,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
		]
	};
  let modes = [
    {
      key: "Demographics",
      gridOptions: {
        columnDefs: [
          {
            field: 'stuDoB',
            name: 'Date of Birth',
            displayName: 'Date of Birth',
            cellFilter: "date:'d-MMM-yyyy'",
          },
          {
            field: 'stuGender',
            name: 'Gender',
            displayName: 'Gender',
            cellClass: 'gdAlignRight',
            lookup: 'gender',
          },
          {
            field: 'stuEthnicity',
            name: 'Ethnicity',
            displayName: 'Ethnicity',
            cellClass: 'gdAlignRight',
            lookup: 'ethnicities',
            enableSorting: true,
            sortDirectionCycle: ["asc", "desc"],
          },
          
        ]
      }
    },    // end demographic mode
		{
			key: "Enrollments",
			columnSet: 1,
			gridOptions: {

				columnDefs: [
					{
						field: 'stuMinSchool',
						name: 'colMinSchNo',
						displayName: 'School(s)',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 250,
						cellTemplate:
						`<div class="ui-grid-cell-contents gdAction">
							<div>
								<span>{{row.entity.stuMinSchNo}}
										<md-tooltip>{{row.entity.stuMinSchNo|lookup:"schoolNames"}} </md-tooltip>
								</span>
								<span ng-show="row.entity.stuMinSchNo != row.entity.stuMaxSchNo">, {{row.entity.stuMaxSchNo}}
										<md-tooltip>{{row.entity.stuMaxSchNo|lookup:"schoolNames"}} </md-tooltip>
								</span>
							</div>
            </div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					},
					{
						field: 'stuMinYear',
						name: 'colminYear',
						displayName: 'Year(s)',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 120,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
								<div>{{row.entity.stuMinYear}}
									<span ng-show="row.entity.stuMinYear != row.entity.stuMaxYear"> - {{row.entity.stuMaxYear}}</span>
								</div>
							</div>`,
						enableSorting: false
					},
					{
						field: 'stuMinClassLevel',
						name: 'colminClassLevel',
						displayName: 'Grade(s)',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 160,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
								<div>{{row.entity.stuMinClassLevel|lookup:"levels"}}
									<span ng-show="row.entity.stuMinClassLevel != row.entity.stuMaxClassLevel">
										- {{row.entity.stuMaxClassLevel|lookup:"levels"}}
									</span>
								</div>
							</div>`,
						enableSorting: false
					},
					{
						name: 'conflicts',
						displayName: '',
						pinnedLeft: false,
						width: 20,
						cellTemplate:
							`<div  layout="row" layout-align="center center" class="ui-grid-cell-contents gdAction">
									{{row.entity.countConflictYear|truefalse:"!"}}
           </div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					}
				]
			}					// gridoptions
		}						// enrollment view mode
  ];

	var pushModes = function (filter) {
		filter.ViewDefaults = viewDefaults;
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['StudentFilter', pushModes]);
}
