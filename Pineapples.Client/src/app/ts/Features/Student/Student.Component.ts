﻿namespace Pineapples.Students {

  interface IBindings {
    model: Student;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: Student;

    static $inject = ["ApiUi", "studentsAPI"];
    constructor(apiui: Sw.Api.IApiUi, private api: any) {
      super(apiui);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

		protected getNew = () => {
			return this.api.new();
		}

  }

  angular
    .module("pineapples")
    .component("componentStudent", new Sw.Component.ItemComponentOptions("student", Controller));
}