﻿namespace Pineapples.Students {

	// Not in used yet. Here as placeholder for future work
	export class StudentLink extends Pineapples.Documents.Document implements Sw.Api.IEditable {

		constructor(linkData) {
			super(linkData);
		}

		// create static method returns the object constructed from the resultset object from the server
		public static create(resultSet: any) {
			let studentlink = new StudentLink(resultSet);
			return studentlink;
		}

		// IEditable implementation
		public _name() {
			return (<any>this).docDescr;
		}
		public _type() {
			return "studentlink";
		}
		public _id() {
			return (<any>this).lnkID
		}

	}
}