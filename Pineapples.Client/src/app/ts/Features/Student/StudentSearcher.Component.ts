﻿namespace Pineapples.Students {

	// don;t need to subclass the controller 
	// optional controller argument is not supplied to FindConfigComponent, so the base is used
  //class Controller extends Sw.Components.FindConfigOperations {
  //}
 
  angular
    .module("pineapples")
		.component("studentSearcherComponent"
			, new Sw.Components.FindConfigComponent("student/SearcherComponent"));
}