﻿// Students Routes
namespace Pineappples.Students {

	let RouteHelper = Sw.Utils.RouteHelper;

	let routes = function ($stateProvider: ng.ui.IStateProvider) {
		var featurename = 'Students';
		var filtername = 'StudentFilter';
		var templatepath = "student";
		var tableOptions = "studentFieldOptions";
		var url = "students";
		var usersettings = null;
		//var mapview = 'StudentMapView';

		// root state for 'students' feature
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath, url, usersettings, tableOptions);

		// default 'api' in this feature is studentsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "studentsAPI";
		state.resolve["customLookups"] = ["Lookups", (lookups: Pineapples.Lookups.LookupService) => lookups.student()]

		state.data = state.data || {};
		state.data.frameTitle = "Students";
		state.data.icon = "people_outline";
		state.data.permissions = {
			only: 'StudentRead'
		};
		let basestate = "site.students";
		$stateProvider.state(basestate, state);

		// List state
		state = Sw.Utils.RouteHelper.frameListState("students", "stuID");
		let statename = "site.students.list";
		$stateProvider.state(statename, state);

		// reload is now identical everywhere 
		state = Sw.Utils.RouteHelper.reloadState();
		statename = "site.students.reload";
		$stateProvider.state(statename, state);

		// chart, table and map
		Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addMapState($stateProvider, featurename); // , mapview

		// new - state with a custom url route
		state = {
			url: "^/students/new",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'StudentWriteX'
				}
			},
			views: {
				"actionpane@site.students": "componentStudent"
			},
			resolve: {
				model: ['studentsAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}],
				pageTitle: RouteHelper.simpleTitle("(new student)")
			}
		};
		$stateProvider.state("site.students.list.new", state);

		state = {
			url: "^/students/reports",
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "Students",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always",
				pageTitle: RouteHelper.simpleTitle("Reports")
			}
		}
		$stateProvider.state("site.students.reports", state);

		// item state
		state = {
			url: "^/students/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.students": {
					component: "componentStudent"
				}
			},
			resolve: {
				model: ['studentsAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}],
				titleId: ['model', (model) => `${model.stuGiven} ${model.stuFamilyName}`],
				pageTitle: RouteHelper.indirectTitle()
			}
		};
		$stateProvider.state("site.students.list.item", state);

		// upload file / image state
		//state = {
		//  url: "^/upload/students/{id}",
		//  params: { id: null, columnField: null, rowData: {} },
		//  views: {
		//    "@": {
		//      component: "teacherLinkUploadComponent"
		//    }
		//  },
		//  resolve: {
		//    student: ['studentsAPI', '$stateParams', function (api, $stateParams) {
		//      return api.read($stateParams.id);
		//    }]
		//  }
		//};
		//$stateProvider.state("site.students.upload", state);
	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}