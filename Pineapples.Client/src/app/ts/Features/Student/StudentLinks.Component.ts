﻿namespace Pineapples.Students {

	interface IBindings {
		links: any[];
	}

	class Controller extends Sw.Component.ListMonitor implements IBindings {
		public links: any[];
		public student: Student;
		public parentkeyname = "lnkID";

		static $inject = ["$scope", "$mdDialog", "Restangular"]
		constructor(
			scope: Sw.IScopeEx
			, private mdDialog: ng.material.IDialogService
			, private Restangular: restangular.IService
		) {
			super();
			this.monitor(scope, "studentlinks", "lnkID");
			this.multiSelect = false;
		}

		public monitoredList() {
			return this.links;
		}
		public isListMember(newData) {
			return (this.student?._id() == newData[this.parentkeyname]);
		}
		public $onChanges(changes) {
		}
		public newDoc() {
			this._uploadDialog().then(() => {
			}, (error) => {
			});
		}

		private _uploadDialog() {
			let options: ng.material.IDialogOptions = {
				locals: { student: this.student },
				controller: StudentLinkUploadController,
				controllerAs: 'vm',
				bindToController: true,
				templateUrl: "studentlink/uploaddialog"
			}
			return this.mdDialog.show(options);
		}

		private doEdit(link, event) {
			let stulink = new StudentLink(link);
			// this has cloned the data...
			this.Restangular.restangularizeElement(null, stulink, "studentlinks");
			this._editDialog(stulink, link);
		}

		private _editDialog(stulink: Pineapples.Documents.DocumentLink, link: any) {
			let options: ng.material.IDialogOptions = {
				locals: { model: stulink, student: this.student },
				controller: editController,
				controllerAs: 'vm',
				bindToController: true,
				templateUrl: "studentlink/editdialog"
			}
			return this.mdDialog.show(options);

		}

		// to do refactor this to somewhere else its duplicated at the moment with teacher links
		private doDelete(link, event) {
			let confirm = this.mdDialog.confirm()
				.title('Delete Document')
				.textContent('Delete the document ' + link.docTitle + ' from the document library?')
				.targetEvent(event)
				.ok('Delete')
				.cancel('Cancel');
			this.mdDialog.show(confirm).then(() => {
				let stulink = new StudentLink(link);
				// this has cloned the data...
				this.Restangular.restangularizeElement(null, stulink, "studentlinks");
				stulink.remove().then(() => {
					// don't need to do anything becuase MonitoredList has removed the item from the array already
				},
					() => { }
					// remove failed
				);
			});
		}

	}

	/**
	 * Controller for the edit Dialog
	 */
	class editController extends Sw.Component.ComponentEditController {
		static $inject = ["$mdDialog", "ApiUi"];
		constructor(public mdDialog: ng.material.IDialogService
			, apiUi: Sw.Api.IApiUi) {
			super(apiUi);
			this.isEditing = true;
		}
		public student: Student;

		public onModelUpdated(newData: any) {
			super.onModelUpdated(newData);
			/*(newData.isCurrentPhoto == 1) ? this.student.stuPhoto = newData.stuPhoto : this.student.stuPhoto = null;*/
		}

		public closeDialog() {
			this.mdDialog.cancel();
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				links: "<",
				student: "<"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "student/linklist";
		}
	}
	angular
		.module("pineapples")
		.component("componentStudentLinks", new Component());

}