﻿namespace Pineapples.Students {

  class Controller {
		public scholarships: any;
		public student: Student;
 
    static $inject = ["$mdDialog", "$location", "$state"];
		constructor(public mdDialog: ng.material.IDialogService
			, private $location: ng.ILocationService, public state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.scholarships) {
//        console.log(this.enrollments);
      }
    }

		/**
		 * Navigate to the scholarship record
		 * @param scholarshipID
		 */
		public show(scholarship) {
			let route = "site.scholarships.list.item";
			this.state.go(route, { id: scholarship.schoID });
		}

		public new() {
			let route = "site.scholarships.list.new";
			this.state.go(route, { studentid: this.student._id() });
		}
  }

  class Component implements ng.IComponentOptions {
		public bindings = {
			scholarships: '<',
			student: "<"
		};
    public controller = Controller;
    public controllerAs = "vm";
		public templateUrl = "student/scholarshiplist";
  }

  angular
    .module("pineapples")
    .component("componentStudentScholarshipList", new Component());
}
