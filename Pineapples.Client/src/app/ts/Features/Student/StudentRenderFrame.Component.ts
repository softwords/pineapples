﻿namespace Pineapples.Students {

	angular
		.module("pineapples")
		.component("studentRenderFrame", new Sw.Component.RenderFrameComponent("student"));
}