﻿namespace Pineapples.Students {

	export class StudentLinkUploadController extends Pineapples.Documents.DocumentUploader {

		public doc: any;      // the document record representing the photo



		public imageHeight: number;
		public student: Student;

		public docFunction: string = "Photo";
		public document: any;     // this is the document object
		public allowUpload: boolean

		public docPath: string;

		static $inject = ["identity", "documentsAPI", "FileUploader", "$mdDialog", "$state", "documentManager"];
		constructor(public identity: Sw.Auth.IIdentity
			, docApi: any
			, FileUploader
			, mdDialog: ng.material.IDialogService
			, state: ng.ui.IStateService
			, renderer: Pineapples.Documents.DocumentManager) {
			super(identity, docApi, FileUploader, mdDialog, state, renderer);
			this.uploader.url = "api/studentlinks/upload";

			this.imageHeight = this.imageHeight || 1200;
			this.model = {};      // TO DO new StudentLink?
		}

		protected onSuccessItem(fileItem, response, status, headers) {
			this.doc = response.ResultSet;    // note slightly different format - not an array item
			this.student.Documents.push(this.doc);
			// update the stuPhoto in the student object - we may have changed it
			/*this.student.stuPhoto = this.doc.stuPhoto*/
			// call the default implementation for housekeeping
			super.onSuccessItem(fileItem, response, status, headers);

		}

		protected onErrorItem(fileItem, response, status, headers) {
		}
		protected onCompleteItem(fileItem, response, status, headers) {
			if (status === 200) {
				this.doc = response.ResultSet;    // note slightly different format - not an array item
				// update the stuPhoto in the student object - we may have changed it
				/*this.student.stuPhoto = this.doc.stuPhoto*/
				// call the default implementation for housekeeping
			}
			super.onCompleteItem(fileItem, response, status, headers);
		};

		protected onBeforeUploadItem(item) {

			// note that bindings that are not initialised in the component tag are stillpushed to the controller as undefined
			this.model.docTitle = item.file.name;
			this.model.docDate = item.file.lastModifiedDate;
			this.model.stuID = this.student._id();
			//if (this.model.lnkFunction !== "PHOTO") {
			//  this.model.isCurrentPhoto = 0;
			//}
			// call the default implementation
			super.onBeforeUploadItem(item);
		}

		public upload() {

			if (this.identity.isAuthenticated) {
				this.uploader.headers.Authorization = 'Bearer ' + this.identity.token;
			}
			this.uploader.uploadAll();
		}

		public get photoPath() {
			if (this.doc) {
				return this.renderer.documentPath(this.doc);
			}
			return this.missingImage;
		}

		public get photoThumbPath() {
			return this.renderer.thumbPath(this.doc, this.imageHeight);
		}

		// life cycle hooks
		public $onChanges(changes) {
			if (changes.docFunction) {
				if (this.docFunction === undefined) {
					this.docFunction = "photo";
				}
			}
			if (this.document) {
				this.doc = this.document;
			}

		}

		public $onInit() {
			this.imageHeight = this.imageHeight || 1200;
			this.model = {};      // TO DO new StudentLink?
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any = {
			imageHeight: "<",
			student: "<",
			docFunction: "<",
			document: "<",
			allowUpload: "<"
		};
		public controller: any = StudentLinkUploadController;
		public controllerAs = "vm";
		public templateUrl = "studentlink/Upload";

	}
	angular
		.module("pineapples")
		.component("studentLinkUploadComponent", new ComponentOptions());
}