﻿namespace Pineapples.SchoolInspections {

	class Controller {
		public survey: any
		public inspectionType: string;

		public template: string;


		public $onInit() {
			this.template = `schoolinspection/survey/${this.inspectionType}`;
		}

		/**
		 * Function to return an answer from a question of type VAR|BINARY
		 * as found on the WASH survey
		 * @param variant - the variant in the list - ie the row (e.g. 'Piped Water')
		 * @param variant_item the variant in the list - ie the column (e.g. 'Used for Drinking')
		 * @param answer_node - the base object of the answer to the question
		 */
		public vbAnswer(variant, variant_item, answer_node) {
			if (!answer_node) {
				return null;
			}
			let av = (<any[]>answer_node.variant).filter(v => v.name == variant);
			if (!av || av.length == 0) {
				return null;
			};
			if (Array.isArray(av[0].variant_item)) {
				let avi = <any[]>(av[0].variant_item).filter(vi => vi.name == variant_item);
				if (!avi || avi.length == 0) {

					return null;
				};
				return avi[0].answer;
			}
		}

		/**
		 * this version returns a 'gettersetter' function to return an answer from a question of type VAR|BINARY
		 * as found on the WASH survey. This i used with ng-model-options {getterSetter: true} and eliminates the 
		 * errors coming from ng-model[nonassign]
		 * In the 'setter case' we just throw the value away ( ie do nothing)
		 * 
		 */
		public vbAnswerGetterSetter(variant, variant_item, answer_node) {
			let f = (value?) => {
				if (value == undefined) {
					return this.vbAnswer(variant, variant_item, answer_node);
				}
			}
			return f;
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			survey: "<",
			inspectionType: "@"
		};
		public controller = Controller
		public controllerAs: string = 'vm';
		public template = `<ng-include src="vm.template"></ng-include>`;
	}

	angular
		.module("pineapples")
		.component("inspectionSurvey", new Component());

}