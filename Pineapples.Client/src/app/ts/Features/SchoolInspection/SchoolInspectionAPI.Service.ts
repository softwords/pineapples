﻿module Pineapples.Api {

const ENTITY = "schoolinspections";
class apiService {

	static $inject = ["$q", "$http", "Restangular"]
	constructor(public $q: ng.IQService
		, public http: ng.IHttpService
		, public restangular: restangular.IService) {
	}

	public read(id) {
		return this.restangular.one(ENTITY, id).get();
	}

	public new() {
		return this.restangular.one(ENTITY, 0).get();
	}

	// try without an update method, becuase we'll get that directly from the object
	
	public filterPaged(fltr) {
		return this.http.post(`api/${ENTITY}/collection/filter`, fltr)
			.then(response => (response.data));
	}
}

angular.module("pineapplesAPI")
	.service("schoolInspectionsAPI", apiService);

}