namespace Pineapples.SchoolInspections {

	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'inspID',
				name: 'ID',
				displayName: 'ID',
				editable: false,
				width: 60,
				pinnedLeft: true,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item","vm")
			},
			{
				field: 'InspTypeCode',
				name: 'InspectionType',
				displayName: 'Type',
				lookup: 'inspectionTypes',
				editable: false,
				width: 60,
				pinnedLeft: true,
			},
			{
				field: 'InspectedBy',
				name: 'Completed By',
				displayName: 'Completed By',
				editable: false,
				width: 120,
				pinnedLeft: true
			},
			{
				field: 'InspectionYear',
				name: 'Year',
				displayName: 'Year',
				editable: false,
				width: 80,
				pinnedLeft: true
			},
			{
				field: 'schNo',
				name: 'School ID',
				displayName: 'School ID',
				editable: false,
				width: 80,
				pinnedLeft: true,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("school", "vm")
			},
			{
				field: 'schName',
				name: 'School Name',
				displayName: 'School Name',
				editable: false,
				width: 180,
				pinnedLeft: true
			}
		]
	};

  let modes = [
    {
      key: "Default",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
        ]
      }
    },
  ]; // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};


  angular
    .module('pineapples')
    .run(['SchoolInspectionFilter', pushModes]);
}
