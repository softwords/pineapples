﻿namespace Pineapples.SchoolInspections {

  export type KoboInfo = {
    __version__: string,
    _id: number,
    _index: number;
    _submission_time: Date,
    _uuid: string,
	}
  export class SchoolInspection extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    //private _scores: Array<Number>;

    constructor(data) {
      super();
      angular.extend(this, data)
      if (this.InspectionContent?.row) {
        // clean up those pesky @ 
        let row = this.InspectionContent.row;
        row = Object.keys(row).reduce((prev, v: string, i: number) => {
          if (v.substr(0, 1) == '@') {
            prev[v.substr(1)] = row[v];
            return prev;
          }
        }, {});
        if (row._id) {
         // separate out the kobo Info fields
         this.koboInfo = Object.keys(row).reduce((prev, v: string, i: number) => {
            if (v.substr(0, 1) == '_') {
              prev[v] = row[v];
            }
            return prev;
          }, {
            __version__: null,
            _id: null,
            _index: null,
            _submission_time: null,
            _uuid: null
          });
          // ..a nd remove the kobo Info and system mapped fields from InspectionContent
          row = Object.keys(row).reduce((prev, v: string, i: number) => {
            if (!(v.substr(0, 1) == '_')) {
              switch (v) {
                case "start":
                case "end":
                case "inspBy":
                case "inspectionType":
                case "schNo":
                case "Index":
                  break;
                default:
                  prev[v] = row[v];
                  break;
							}
            }
            return prev;
          }, {});
          this.InspectionContent = row;
				}
      };
    }

    // create static method returns the object constructed from the resultset object from the server
		// data delviered into here is a structred object derived from Xml, with root node 'inspection'
		// ie data is an object with one property: 'inspection'
		public static create(data: any) {
			switch (data.inspection.inspTypeCode) {
				case "SCHOOL_ACCREDITATION":
					return new Pineapples.SchoolAccreditations.SchoolAccreditation(data.inspection);
				case "WASH":
					return new Pineapples.Wash.Wash(data.inspection);

				default:
					return new SchoolInspection(data.inspection);
			}
    }
    public InspectionContent: any;      // the contents of inspXml
    public koboInfo: KoboInfo;

    public isKobo() {
      return (this.koboInfo != null);
		}
    // IEditable implementation
    public _name() {
      return (<any>this).schNo + '-' + (<any>this).InspYear;
    }

    public _type() {
      return "schoolinspection";
    }

    public _id() {
      return (<any>this).inspID
    }

		public inspTypeCode: string;
  }
}
