﻿namespace Pineapples.Schools {

	angular
		.module("pineapples")
		.component("schoolInspectionRenderFrame", new Sw.Component.RenderFrameComponent("schoolinspection"));
}