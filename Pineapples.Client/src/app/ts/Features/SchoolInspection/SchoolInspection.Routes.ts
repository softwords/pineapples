namespace Pineappples.SchoolInspections {

	let routes = function ($stateProvider) {
		var featurename = 'SchoolInspections';
		var filtername = 'SchoolInspectionFilter';
		var templatepath = "schoolInspection";
		var tableOptions = "schoolinspectionFieldOptions";
		var url = "schoolInspections";
		var usersettings = null;
		//var mapview = 'SchoolMapView';

		// root state for 'schoolInspection' feature
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath);


		// default 'api' in this feature is schoolInspectionsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "schoolInspectionsAPI";

		state.data = state.data || {};
		state.data.frameTitle = "Reviews and Inspections";
		state.data.icon = "list";
		state.data.permissions = {
			only: "InspectionRead"
		};

		let statename = "site.schoolinspections";
		$stateProvider.state("site.schoolinspections", state);

		// List state (here not using the @name Sw.Utils.RouteHelper.frameListState)
		// but defining the state right here with extra renderoptions.
		state = {

			url: "/list/{InspType}",
			params: {
				InspType: { value: null, squash: true }
			},
			data: {
				rendermode: "List"
			},
			views: <{ [name: string]: ng.ui.IState }>{
				"renderarea": "pagedListManager"

				//{
				//	templateUrl: `generic/renderlist`, // begin move to server-side mvc routes
				//	controller: 'PagedListController',
				//	controllerAs: "listvm"
				//},

			},
			resolve: {
				// see whether we need to apply the state param to filter the list
				findConfig: ['$stateParams', "findConfig",
					($stateParams, findConfig: Sw.Filter.FindConfig) => {
						let params = _.mapValues({ InspType: null }, (v, k) => $stateParams[k] ? $stateParams[k].toUpperCase() : null);
						findConfig.applyLocked(params);
						return findConfig;
					}]
			}
		};

		statename = "site.schoolinspections.list";
		$stateProvider.state(statename, state);



		//-------------- Reload State ----------------------------//
		state = {
			url: '^/schoolinspections/reload',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.remove("schoolinspection/item");
				$templateCache.remove("schoolinspection/searcher");
				$state.go("site.schoolinspections.list");
			}]
		};
		statename = "site.schoolinspections.reload";
		$stateProvider.state(statename, state);

		//--------------- Dashboard ----------------------------//
		state = {
			url: "^/schoolinspections/dashboard",
			data: {
				permissions: {
					only: 'SchoolReadX'
				}
			},
			views: {
				"@": {
					component: "schoolInspectionDashboardComponent"
				}

			},
			resolve: {
				pageTitle: Sw.Utils.RouteHelper.simpleTitle("Dashboard")
			}
		};
		$stateProvider.state("site.schoolinspections.dashboard", state);

		// chart table and map
		Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
		// Sw.Utils.RouteHelper.addMapState($stateProvider, featurename, mapview);

		//------------------------------new state list.new
		state = {
			url: "^/schoolinspections/new",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'SchoolWriteX'
				}
			},
			views: {
				"actionpane@site.schoolinspections.list": {
					component: "componentSchoolInspection"
				}

			},
			resolve: {
				model: ['schoolInspectionsAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}]
			}
		};
		$stateProvider.state("site.schoolinspections.list.new", state);

		//------------------- Reports
		state = {
			url: "^/schoolinspections/reports",
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "School_inspections",  // actually School inspection folder in Jasper but URI is underscore
				promptForParams: () => "always",
				pageTitle: ["pageTitle", (title) => `{title} Reports`]
			}
		}
		$stateProvider.state("site.schoolinspections.reports", state);

		// ----------------- item state
		state = {
			url: "^/schoolinspections/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.schoolinspections": {
					component: "componentSchoolInspection"
				}
			},
			resolve: {
				model: ['schoolInspectionsAPI', '$stateParams', (api, $stateParams) => api.read($stateParams.id)],
				pageTitle: ['model', 'rootPageTitle', (model, title) => `${title} * ${model.InspectionType}: ${model.schName}`]

			}
		};
		$stateProvider.state("site.schoolinspections.list.item", state);

		// ----------------- school state : allows a school to be displayed from the list of inspections

		state = {
			url: "^/schoolinspections/school/{id}",
			views: {
				"actionpane@site.schoolinspections": "componentSchool"
			}
		};
		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.schools.list.item", state);
		$stateProvider.state("site.schoolinspections.list.school", state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}
