﻿module Pineapples {

  class SchoolInspectionParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };
  }

  class SchoolInspectionFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "schoolInspectionsAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.entity = "schoolinspection";
      this.ParamManager = new SchoolInspectionParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // cycle through any filters in the identity, use them
      // to construct the identity filter
      // school Inspections are subject to same resitrctions as schools
      for (var propertyName in this.identity.filters) {
        switch (propertyName) {
          case "Authority":
          case "District":
          case "ElectorateN":
          case "ElectorateL":
          case "SchoolNo":      // support access to a single school - for a principal?
            fltr[propertyName] = this.identity.filters[propertyName];
            break;
        }
      }
      return fltr;
    }

    //public createTableCalculator() {
    //  return new SchoolTableCalculator();
    //}

    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer<Sw.Filter.FindConfig>();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "saID";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "School";
      config.defaults.table.col = "InspYear";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      //config.tableOptions = "schoolFieldOptions";
      //config.dataOptions = "schoolDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      d.resolve(config);
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("SchoolInspectionFilter", SchoolInspectionFilter);
}
