﻿namespace Pineapples.SchoolInspections {

  interface IBindings {
    model: SchoolInspection;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
		public model: SchoolInspection;
		/**
		 * Name of the template for the current inspection type - dependant on the bound model
		 */
		public template: string
    static $inject = ["ApiUi", "schoolInspectionsAPI"];
    constructor(apiui: Sw.Api.IApiUi, private api: any) {
      super(apiui);
		}

		public $onInit() {
			this.template = `schoolinspection/inspectiontype/${this.model.inspTypeCode}`;
		}

    public $onChanges(changes) {
      super.$onChanges(changes);
		}

		protected getNew = () => {
			return this.api.new();
		}

  }
	class Component implements ng.IComponentOptions {
		public bindings: any = {
			model: "<",
		};
		public controller = Controller
		public controllerAs: string = 'vm';
		/**
		 * Polymorphic UI for SchoolInspection
		 * The template for a school inspection loads the template whose name is returned from the 
		 * template property. This invokes the route 
		 * schoolinspection/inspectiontype/<inspTypeCode> 
		 * The controller handling this route looks for a template named 
		 * from the inspection type (SCHOOL_ACCREDITATION, WASH etc)
		 * in the inspectiontype subfolder of the schoolinspection views.
		 */ 
		public template = `<ng-include src="vm.template"></ng-include>`;
	}
  angular
    .module("pineapples")
    .component("componentSchoolInspection", new Component());
}