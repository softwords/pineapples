﻿namespace Pineapples.Exams {

	// Exam data can be displayed by exam - year and type - and school
	// This component fixes school ( ie passed in as a binding, and allows selection exam


	class Controller{

		public examXf: ExamXf;

		private _examCode: string;
		public get examCode() {
			return this._examCode;
		}

		public set examCode(code) {
			this._examCode = code;
			this.getData();
		}

		private _examYear: number;
		public get examYear() {
			return this._examYear;
		}

		public set examYear(Year) {
			this._examYear = Year;
			this.getData();
		}


		public schoolNo: string
		
		static $inject = ["xFilter","Lookups", "examsAPI"]
		constructor(
			public xFilter: Sw.xFilter.IXFilter,
			public lookups: Pineapples.Lookups.LookupService,
			public api: Api.IExamsApi
		) {
		}
		////////////////////////////////////////////////////////////////
		// LifeCycle events
		/////////////////////////////////////////////////////////////////
		public $onChanges(changes) {
		}

		private getData() {
		 if (this.examCode && this.examYear) {
				// fetch the data
				this.api.contextDataXf(this.schoolNo, this.examCode, this.examYear)
					.then((xf) => {
						this.examXf = xf;
					});
			};
		}


	}

	class ChartComponent  implements ng.IComponentOptions {

		public bindings = {
			schoolNo: "<",						// pass in a school
		}
		public templateUrl: string = `exam/dashboard/schooldashboardchart`;
		public controller = Controller;
		public controllerAs = "vm";
	}

	class CrosstabComponent implements ng.IComponentOptions {

		public bindings = {
			schoolNo: "<",						// pass in an exam record
		}
		public templateUrl: string = `exam/dashboard/schooldashboardcrosstab`;
		public controller = Controller;
		public controllerAs = "vm";
	}

	angular
		.module("pineapples")
		.component("examSchoolDashboardChart", new ChartComponent())
		.component("examSchoolDashboardCrosstab", new CrosstabComponent());

}