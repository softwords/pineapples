﻿namespace Pineapples.Dashboards.Exam {

	class Controller extends DashboardChild implements IDashboardChild {

		/** the chart will source its data from a crossfilter group
		 *	we can get this group in a number of ways:
		 *	1) a binding directly to a group: group="vm.<groupname>"
		 *	    this assumes that the dashboard has created the group and can provide it
		 *	 2) get a dimension and create the group in this component:
		 *	    dimension="vm.<dimensionname>"
		 *	 3) Use the binding to the dashboard itself, and retrieve the group somehow from there:
		 *			dashboard="vm"
		 *			.....
		 *			this.dashboard.....
		 *	1 or 2 are generally to be preferred - so which is better?
		 *	If the group we are making is quite specific in purpose, and not likely to be of use to other 
		 *	child components, make it locally. ie 2) : get a dimension from the dashboard, and make the group yourself
		 *	
		 *	But, if the group is likely to be created the same by a number of child components, 
		 *	then consider creating the group in the dashboard, and binding it on to this component. ie 1)
		 */

		// one of these three must be defined as a binding
		
		public dimension: CrossFilter.Dimension<any, any>;
		public group: CrossFilter.Group<any, any, any>;
		public dashboard: ExamsDashboard;

		public options: examOptions;
		private chartData; 
		public mirrors = ["F"];	// the F series is mirrored to the left of the axis by mirror-chart

		
		// these 2 properties supply defaults for 'palette' and 'title'
		// they may be overriden by bindings to 'color' and 'headingTitle'
		// see issue849
		protected defaultPalette = ['#1010FF', '#223344'];
		protected defaultTitle = "Achievement by Gender";				

		public $onInit() {
			
			// $onInit is called when all the bindings are instantiated
			// if we have a binding to group - we now have the group
			// if we have a binding to dimension, we have the dimension and can make the group:
			this.group = this.dashboard.xFilter.xReduce(this.dashboard.examXf.xf.dimension(d=>d.achievementLevel)
				, "achievementLevel"			// crosstabulation is also by achievementLevel,
																	//  this doesn; t accomplish anything becuase we don't need a crosstabulation, 
																	// but have to put something
				// the third agrument is the function to calculate the value stored under each item in the
				// crosstabulation
				// Here we construct this function using a helper to build it from the gender field ( suppplies Gender)
				// and "Candidates" - which supplies the value
				, this.dashboard.xFilter.getFlattenedGenderAccessor("Gender", "candidateCount")
			);
			// note we are drawing a chart where the 'items' ie the values in each series are AchievementLevels,
			// and the series correspond to genders. Becuase the 'rows' in the echart data will come from the 
			// 'row' property in group.all(), we need a group based on a dimension on achivementLevel.
			// This achievementLevel comes from the dimension. It is also used as the crosstabulation in the xReduce

			// now we have the group, and a row in group.all() will look like this:
			/**
			{row: 1,
				value: {
					1: {
						M: 3,
						F: 2,
						T: 5
					},
					Tot: {
						M: 3,
						F: 2,
						T: 5
					}
				}
			}
			*/
			// in this structure:
			//  -- the direct chikdren of value (1,"Tot") are produced by the crosstabulation in xReduce
			//  -- ie from the keyAccessor argument
			//  -- the structure below each of those {M: 3, F: 2, T: 5} is prodcued by the valueAccessor argument
			//   

			// From this structure, for each row, we want to pull out the value.Tot.M and valut.Tot.F values as the M and F series
			// we create an 'AnnotatedGroup' to describe this:

			let columns = ["F", "M"];			// the columns - ie series - in the chart. 
																		// This could also be a function that collects the column values in each row
																		// so that column names can be discovered dynamically
			// The value accessor is given the rowdata (the key/value object) and the columnName and id
			// and gets the value out - this gets M and F from the structure shown above.
			// To make a gender pyramid, we pass ["F"] as the value of mirrors to mirror-chart

			// the Y axis in the chart:
			let valueAccessor = (row, columnName, columnIndex) => (row.value.Tot) ?
					row.value.Tot[columnName] : 0;

			// put all this together into the 'AnnotatedGroup'
			// this object is bound to the dataset property of rowColChart
			// rowColChart will invoke EchartTransformer to transform this input, and EchartTransformer will recognize
			// this object and transform it into echart format
			// This object is created only once and never changed.
			// So for the chart to redraw, we need some way 
			// 1) to detect it is out of date, and 
			// 2) to get the chart to redraw
			// For 2) we use the touch binding on the rowColChart.
			// the chart does a full recaalculation, including data transformation,
			// whenever the touch binding is changed
			this.chartData = {
				group: this.group,
				columns: columns,
				valueAccessor: valueAccessor
			};
		}

		public $onChanges(changes) {
			// isFirstChange() returns true when the bindings are first initialised PRIOR to onInit
			// better to ignore those
			if (changes.dashboard?.isFirstChange()) {
				return;
			}
			// a dashboard child should always call super in here,
			// or else onOptionChange will never fire
			super.$onChanges(changes);
			// whatever is changed, we need a redraw
			// all we need to do is 'touch' the chart
			this.refreshChart();
			// How does this work? By chnaging a dashboard option, a filter gets applied to its corresponding 
			// crossfilter dimension
			// when we transform the data from the AnnotatedGroup object chartData, the group it contains will, 
			// thanks to the magic of crossfilter, automatically reflect the filters applied to other dimensions
		}

		private _selectedRecordType: string;

		public optionDescription() {
					return this.options.descriptionExcluding() + " (" + this._selectedRecordType + ")";
		}
		////////////////////////////////////////////////////////////////////////////////
		// Chart presentation
		////////////////////////////////////////////////////////////////////////////////
		//
		// helper function to convert an achievement level 1 2 3 4 to text
		// for illustration only - in real world this needs to account for different exam structures
		private levelLookup = (achievementLevel) => {
			let n = [{ name: "Well below competent" }
				, { name: "Approaching competence" }
				, { name: "Minimally competent" }
				, { name: "Competent" }
			][achievementLevel - 1].name;
			return n;
		}

		/**
		 * callback set up in bindings to the rowcolchart. 
		 * on-chart-render='vm.onChartRender(option, echart)'
		 * This is invoked immediately before the chart is drawn. 
		 * Modify (but don't replace!) option to change aspects of the chart using echarts option API
		 * @param option - the option object
		 * @param echart - the echart instance
		 */
		public onChartRender(option: echarts.EChartOption, echart: echarts.ECharts) {
			// before the refresh, capture the recordType we will be drawing
			// this is not what onChartRender is there for ... feels a bit kludgy to use this 
			// as the point to gaurantee we catch every chart redraw....
			let currentRecordType = <string>this.dashboard.examXf.examXf.dimensions.recordType.currentFilter();
			if (this._selectedRecordType != currentRecordType) {
				this._selectedRecordType = currentRecordType;
			}
			// a typical strategy in many case is to create an option object containing the changes, and merge it
			let o = {
				yAxis: {
					axisLabel: {
						formatter: this.levelLookup				// formatter is a function taking a single value, being the axis label value
					}
				}
			}
			// angular.merge is a deep copy
			angular.merge(option, o);
		}

		/**
		 * callback to formt a tooltip
		 * There are lots of options for this, but this is perhaps the simplest:
		 * get the 'datum' format contiing the series name, item name marker and value and percent
		 * via the binding 
		 * tooltipper="vm.tooltipper(datum)"
		 * Change whaver parts of datum you want to change
		 * Don't return a value - then rowcolchart will format the modified datum in the standard tooltip format
		 * Note you can return a value - and that is used as the tooltip. Furthermore
		 * the returned value can be html
		 */ 
		public tooltipper(datum) {
			// replace the item name (1 2 3 4) with its lookup
			datum.item = this.levelLookup(datum.item);
			// change F M to Female or Male
			datum.series = (datum.series == "F") ? "Female" : "Male";
		}
	}

	angular
		.module("pineapples")
		.component("examResultsGenderTree", new ExamDashboardComponent(Controller, "resultsGenderTree"));
}