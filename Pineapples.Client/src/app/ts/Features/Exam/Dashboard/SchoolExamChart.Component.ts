﻿namespace Pineapples.Exams {

	// Exam data can be displayed by exam - year and type - and school
	// This component fixes exam ( ie passed in as a binding, and allows selection of school
	// Conversely, schoolexamdashboardEx fixes the school, and allows selection of exam
	

	class Controller{

		public examXf: ExamXf;

		public exam: Exam;
		private _schoolNo;
		public get schoolNo() {
			return this.national?null:this._schoolNo;
		}
		public set schoolNo(value) {
			this._schoolNo = value;
			this.getData();
		}

		
		private _national: boolean;
		public get national() {
			return this._national;
		}
		public set national(value) {
			this._national = value;
			this.getData();
		}


		static $inject = ["xFilter","Lookups", "examsAPI"]
		constructor(
			public xFilter: Sw.xFilter.IXFilter,
			public lookups: Pineapples.Lookups.LookupService,
			public api: Api.IExamsApi
		) {
		}
		////////////////////////////////////////////////////////////////
		// LifeCycle events
		/////////////////////////////////////////////////////////////////
		public $onChanges(changes) {
			if (changes.exam && this.exam.Results) {
				// the exams includes national totals that we can populate the chart from
				// thereafter, we can show particular school, results,
				this.examXf = new ExamXf(this.exam.Results);
				this.national = true;
			}
		}

		private getData() {
			if (this.national) {
				this.api.contextDataXf("(nation)", this.exam.exCode, this.exam.exYear)
					.then((xf) => {
						this.examXf = xf;
					});
			} else if (this.schoolNo) {
				// fetch the data
				this.api.contextDataXf(this.schoolNo, this.exam.exCode, this.exam.exYear)
					.then((xf) => {
						this.examXf = xf;
					});
			};
		}


	}

	class ChartComponent  implements ng.IComponentOptions {

		public bindings = {
			exam: "<",						// pass in an exam record
		}
		public templateUrl: string = `exam/dashboard/schoolchart`;
		public controller = Controller;
		public controllerAs = "vm";
	}

	class CrosstabComponent implements ng.IComponentOptions {

		public bindings = {
			exam: "<",						// pass in an exam record
		}
		public templateUrl: string = `exam/dashboard/schoolcrosstab`;
		public controller = Controller;
		public controllerAs = "vm";
	}



	angular
		.module("pineapples")
		.component("examSchoolChart", new ChartComponent())
		.component("examSchoolCrosstab", new CrosstabComponent());

}