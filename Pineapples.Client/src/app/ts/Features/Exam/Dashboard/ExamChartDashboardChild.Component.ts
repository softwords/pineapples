﻿namespace Pineapples.Dashboards.Exam {

	class Controller extends DashboardChild implements IDashboardChild {

		/** the chart will source its data from a crossfilter group
		 *	we can get this group in a number of ways:
		 *	1) a binding directly to a group: group="vm.<groupname>"
		 *	    this assumes that the dashboard has created the group and can provide it
		 *	 2) get a dimension and create the group in this component:
		 *	    dimension="vm.<dimensionname>"
		 *	 3) Use the binding to the dashboard itself, and retrieve the group somehow from there:
		 *			dashboard="vm"
		 *			.....
		 *			this.dashboard.....
		 *	1 or 2 are generally to be preferred - so which is better?
		 *	If the group we are making is quite specific in purpose, and not likely to be of use to other 
		 *	child components, make it locally. ie 2) : get a dimension from the dashboard, and make the group yourself
		 *	
		 *	But, if the group is likely to be created the same by a number of child components, 
		 *	then consider creating the group in the dashboard, and binding it on to this component. ie 1)
		 *	
		 *	In Exams v2, we have the exam-chart component with bultin smarts for stats by 'recordType' 
		 *	( Exam, Standard, Benchmark, Indicator ) and Calulcation method ( IndicaatorCount, weight, CandidateCount)
		 *	This component is a wrapper around that, to make it play nicely as a dashboardChild
		 */

		// one of these three must be defined as a binding

		public dimension: CrossFilter.Dimension<any, any>;
		public group: CrossFilter.Group<any, any, any>;
		public dashboard: ExamsDashboard;

		public aggregation: string;

		public options: examOptions;
		private chartData;
		public mirrors = ["F"];	// the F series is mirrored to the left of the axis by mirror-chart


		// these 2 properties supply defaults for 'palette' and 'title'
		// they may be overriden by bindings to 'color' and 'headingTitle'
		// see issue849
		protected defaultPalette = ['#1010FF', '#223344'];
		protected defaultTitle = "Results";

		public $onInit() {
		}

		public optionDescription() {
			switch (this.aggregation) {
				case "district":
					return this.options.descriptionExcluding("District");
				case "island":
					return this.options.descriptionExcluding("Island");
				case "region":
					return this.options.descriptionExcluding("Region");
				case "year":
					return this.options.descriptionExcluding("Year");
				case "authorityGovt":
					return this.options.descriptionExcluding("AuthorityGovt");

				default:
					return this.options.descriptionExcluding();
			}
		}

		public $onChanges(changes) {
			// isFirstChange() returns true when the bindings are first initialised PRIOR to onInit
			// better to ignore those
			if (changes.dashboard?.isFirstChange()) {
				return;
			}
			// a dashboard child should always call super in here,
			// or else onOptionChange will never fire
			super.$onChanges(changes);
			// whatever is changed, we need a redraw
			// all we need to do is 'touch' the chart
			this.refreshChart();
			// How does this work? By changing a dashboard option, a filter gets applied to its corresponding 
			// crossfilter dimension
			// when we transform the data from the AnnotatedGroup object chartData, the group it contains will, 
			// thanks to the magic of crossfilter, automatically reflect the filters applied to other dimensions
		}
	}
	class Component extends ExamDashboardComponent implements ng.IComponentOptions {

		constructor(public controller: any, templateUrl) {
			super(controller, templateUrl);
			this.bindings["aggregation"] = "@";
		}
	}
	angular
		.module("pineapples")
		.component("examChartDashboard", new Component(Controller, "examChartDashboardChild"));
}