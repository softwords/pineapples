﻿namespace Pineapples.Exams {

	class Controller {

		private chartData: Sw.Charts.AnnotatedGroup;
		private touch: number;
		private columnSortBy: any[];
		public examXf: ExamXf;
		public aggregation: string;

		private _genderSplit: string;

		protected defaultPalette;				// an array holding the chart palette
		public colorbrewer: ColorBrewer.Base = colorbrewer;
		public staMirrors;			// allow this to be passed as a literal binding
		public mirrors;					// bound onto mirror-chart

		public paletteName;
		public viewOptions;

		public palettes = {
			default: ['#1010FF', '#223344', '#096723', '#123456'],
			// for STA, levels 1 2 3 4
			sta: ["#FF0000", "#FFC000", "#92D050", "#00B050"]
		}

		public onViewOptionChange() {
			this.refreshChart();				// this currently doesn;t work - viewOptions are not implemented
		};

		static $inject = ["xFilter", "Lookups"]
		constructor(
			public xFilter: Sw.xFilter.IXFilter,
			public lookups: Pineapples.Lookups.LookupService
		) {
		}
		////////////////////////////////////////////////////////////////
		// LifeCycle events
		/////////////////////////////////////////////////////////////////
		public $onChanges(changes) {

			if (changes.staMirrors) {
				this.mirrors = this.staMirrors;
			}

			// this is the only place makeChartData is called
			// its job is to set up the AnnotatedGroup, which does not change:
			// the dependencies on changes are accounted for in the access functions in that annotated group
			// so they will be reflected whenever the data is 'transformed'
			// inside RowColChart.
			// Therefore, once the group is established, just 'touch' the chart ( ie change the value of 'touch')
			if (changes.examXf && this.examXf) {
				this.makeChartData();
			}

			// on the dashboard, there may be multiple charts sharing the same examXf. This is necessary to keep them
			// all synchronised to the current options (by filtering the shared crossfilter )
			// however, this compoenent will have to use the current filter for recordType, which may not match 
			// what is displayed inthe dropdown on this component
			// so - take the opportunity, when there is a change in options, to bring this back into synch
			if (this.examXf && this._selectedRecordType != this.examXf.dimensions.recordType.currentFilter()) {
				// setting selected record type forces a refresh
				this.selectedRecordType = this.examXf.dimensions.recordType.currentFilter();
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////
		// setup and configuration
		//////////////////////////////////////////////////////////////////////////////////////////////////

		public makeChartData() {

			this.applySelectedRecordType(this.selectedRecordType);			// force to apply the RecrdType filter on the crossfilter

			// this crosstab has an array for key, so we need a rowId function, normally this can be omitted
			// and allowed to default
			let rowId = (row) => row.key;


			let columns: Sw.Charts.ColumnCollector = this.xFilter.getGrpValuePropsArray();
			let valueAccessor = (row, col, colIdx) => row.value[col]?.T[this.calculationMethod] || 0;

			if (this.genderSplit) {
				columns = ["1.M", "1.T", "1.F", "2.M", "2.T", "2.F", "3.M", "3.T", "3.F", "4.M", "4.T", "4.F"];
				valueAccessor = (row, col, colIdx) => {
					let c = col.split(".");
					return row.value[c[0]]?.[c[1]]?.[this.calculationMethod] || 0;
				};
			};

			this.chartData = {
				group: this.currentAggregationGroup(),
				rowId: rowId,
				columns: columns,
				valueAccessor: valueAccessor
			}
		}

		public currentAggregationGroup() {
			switch (this.aggregation) {
				case "district":
					return this.examXf.groups.district;
				case "island":
					return this.examXf.groups.island;
				case "region":
					return this.examXf.groups.region;
				case "year":
					return this.examXf.groups.year;
				case "authorityGovt":
					return this.examXf.groups.authorityGovt;
			}
			return this.examXf.groups.key;
		}


		public refreshChart() {
			// ensure that touch is bound onto the RowColChart 
			// note it is an @ binding ie use like this:  touch="{{vm.touch}}"
			this.touch = Date.now();
		}

		private _selectedRecordType;

		// this exists so we can set from makeChartData without invoking it again
		private applySelectedRecordType(value) {
			this._selectedRecordType = value;
			let r = this.examXf.dimensions.recordType;
			r.filter(value);

		}
		public get selectedRecordType() {
			return this._selectedRecordType ?? 'Exam';
		}

		public set selectedRecordType(value) {
			let currentGenderSplit = this.genderSplit;
			this.applySelectedRecordType(value);
			// perhaps we can refresh, but if we have changed the (auto) genderSplit, we'll
			// have to make the chart again to get the changed columns
			if (this.genderSplit == currentGenderSplit) {
				this.refreshChart();
			} else {
				this.makeChartData();
			}

		}

		private _calculationMethod: string = "candidateCount";
		public get calculationMethod() {

			return this._calculationMethod;
		}

		public set calculationMethod(value) {
			this._calculationMethod = value;
			this.refreshChart();
		}

		public get genderSplit() {
			switch (this._genderSplit?.toUpperCase()) {
				case "Y":
				case "YES":
					return true;
				case "N":
				case "NO":
					return false;
			}
			return (this.selectedRecordType == "Exam")
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////
		// crosstab support - this controller does double duty as the vm for exam-chart and exam-crosstab
		/////////////////////////////////////////////////////////////////////////////////////////////////
		public tableValueAccessor = (row, col, gender) => {
			if (gender) {
				return row.value?.[col]?.[gender]?.[this.calculationMethod];
			}
			return row.value?.[col]?.T?.[this.calculationMethod];
		}

		public tableRowFilter = (row) => {
			// remember that keys appear in all() even if their accumulation is across 0 rows
			if (row.value.Count === 0) {
				return false;
			}
			let xdc = ExamXf.getExamDataXfKey(row.key);
			// in a school aggregation (school, district, nation) show only the school records
			// or for nation level aggregation, show only the nation record
			// otherwise return all records ( a district, year, govt/non-govt value for aggregation)
			switch (this.aggregation) {
				case "district":
				case "island":
				case "region":
				case "year":
				case "authorityGovt":
					return (xdc.recordType == this.selectedRecordType);
			}
			if (xdc.aggregation == "") {
				return (xdc.recordType == this.selectedRecordType);
			}
			return (xdc.recordType == this.selectedRecordType && xdc.aggregation == this.examXf.aggregationLevels?.first());
		}

		public makeRecordTypeFilter = _.memoize((recordtype: string) =>
			(row) => ExamXf.getExamDataXfKey(row.key).recordType == recordtype);

		public tableRowAccessor = (row) => {
			switch (this.aggregation) {
				case "district":
				case "island":
				case "region":
				case "year":
				case "authorityGovt":
				//	return `${ExamXf.getExamDataXfKey(row.key).key} ${ExamXf.getExamDataXfKey(row.key).aggregationDesc}`;
			}
			return `${ExamXf.getExamDataXfKey(row.key).key}`;
		}
		/////////////////////////////////////////////////////////////
		// Chart callbacks
		/////////////////////////////////////////////////////////////


		public onChartRender(option, echart) {
			option.series.forEach((series, idx) => {

				series.backgroundStyle = {
					opacity: .3
				};
				series.barMaxWidth = 60;
				series.barGap = "5%";
				// apply the sorted palette to the sorted series
				/// for handle both aggregates and gender splits
				// if gender is split, we have separate column sets for F M T, e.g. 1.F, @.F  ... etc
				let idarr = series.id.split(".");
				let genderSplit = idarr.length > 1;
				let seriesGenderCode = genderSplit ? idarr[1] : "T";
				let seriesAchievementLevel = +idarr[0];

				// change the series name to the achievement description ('Well below competent'...)
				// or 'Level 1', 
				// Changing the series name will be reflected in tooltip, legend etc
				// although in this case, we still have to regenerate the legend data  to match
				series.name = this.examXf.achievementLevels.description(seriesAchievementLevel);

				series.color = this.palettes.sta[(+seriesAchievementLevel - 1)];
				series.stack = seriesGenderCode;
				series.itemStyle = series.itemStyle ?? {};
				switch (seriesGenderCode) {
					case "M":
						series.itemStyle.opacity = .7;
						series.barWidth = "20%";
						break;
					case "F":
						series.itemStyle.opacity = .7;
						series.barWidth = "20%";
						break;
					case "T":
						series.itemStyle.opacity = 1;
						series.barWidth = genderSplit ? "50%" : "80%";
						series.showBackground = true;
						break;
				}
				if (genderSplit && +seriesAchievementLevel == 2) {

					series.label = {
						show: (seriesGenderCode != "T"),
						color: '#101010',
						position: 'insideRight',
						fontFamily: 'sans-serif',
						opacity: 1,
						rich: {
							bold: {
								fontWeight: "bold",
								color: 'blue',
								opacity: 1
							}
						},
						formatter: (params) => { return seriesGenderCode }
					}
				}
				if (genderSplit) {
					// this is the hard part - the percentage data we have is a percentage of all columns
					// not the columns for the gender

					// strategy : for each column, decide if it is M F or T, store this
					// for each row in the source, accumulate the total of M F T columns
					// then convert each M F T value back to a percent
					let data = <echarts.EChartOption.Dataset>option.dataset;
					let gdims = (<string[]>data.dimensions).map((v, i) => {
						let ss: string[] = v.split(".");
						return (ss.length == 2) ? ss[1] : "";
					});
					let adjustedSource = (<any[]>data.source).map((row: any[]) => {
						let gtots = row.reduce((tots, v, i) => {
							if (gdims[i] > "") {
								tots[gdims[i]] += Math.abs(+v);			// note these may be signed to do the mirroring
							}
							return tots;
						}, { M: 0, F: 0, T: 0 });
						// now we have the totals by gender, convert each value in the row to a percentage
						return row.map((v, i) => {
							if (gdims[i] == "") {
								// not a gender value column, just quote it
								return v;
							}
							return Math.round(v * 1000 / gtots[gdims[i]]) / 10;
						});
					});
					option.dataset.originalSource = option.dataset.source;
					option.dataset.source = adjustedSource;
				}
				if (seriesAchievementLevel == this.examXf.achievementLevels.max() && seriesGenderCode == "T") {
					//on the last one, put a label
					series.label = {
						formatter: (params) => {
							let xdc = ExamXf.getExamDataXfKey(params.name);
							switch (xdc.aggregation) {
								case 'School':
									return `{bold|${xdc.aggregationKey.toUpperCase()}}`;		// for school return the school code, bold
								case "District":
									return this.lookups.byCode("districts", xdc.aggregationKey, "N");
								case "Island":
									return this.lookups.byCode("islands", xdc.aggregationKey, "N");
								case "Region":
									return this.lookups.byCode("regions", xdc.aggregationKey, "N");
								case "Year":
									return xdc.aggregationDesc;			// the year name
								case "AuthorityGovt":
									return this.lookups.byCode("authorityGovts", xdc.aggregationKey, "N");			// the country name
								case "Nation":
									return this.lookups.byCode("sysParams", "USER_NATION", "N");			// the country name
								case "":			//filtered, in the dashboard
									return "";
							}
						},
						show: true,
						color: '#101010',
						position: 'right',
						fontFamily: 'sans-serif',
						opacity: 1,
						rich: {
							bold: {
								fontWeight: "bold",
								color: "blue",
								opacity: 1
							}
						}
					}
				}

			});

			let o = {

				yAxis: {
					axisLabel: {
						// apply some detailed formatting to the benchmark description
						formatter: (value: string) => {
							let xdc = ExamXf.getExamDataXfKey(value);
							// apply some formatting to the key
							let axisLabel = `{bold|${xdc.key.toUpperCase()}}`;
							if (xdc.key == xdc.description) {
								return axisLabel;
							}

							return [axisLabel, xdc.description].join(":");
						},
						rich: {
							bold: {
								color: "darkgrey",
								fontWeight: "bold"
							}
						}
					}
				},
				xAxis: {
					max: 100,
					min: -100,
					minInterval: 20,
					maxInterval: 50,

				},
				legend: {
					// mirrorChart has given us a legend in the correct order, but the name translate of series here means
					// we have to name translate the legend entries
					data: option.legend.data.map(value => this.examXf.achievementLevels.description(value)),
					left: 'right',
					top: 'middle',
					orient: "vertical"
				},
				grid: {
					left: 0,
					right: 120,
					top: 0,
					bottom: 60
				}
			}
			angular.merge(option, o);
		}

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			// mirror chart has lready dealt with the abs of value
			// prettify the benchmark name, as in the Y axis:
			let itm: string = ''
			let xdc = ExamXf.getExamDataXfKey(datum.item);
			switch (xdc.aggregation) {
				case 'School':
					itm = xdc.aggregationDesc;			// for school return the school name in the tooltip
					break;
				case "District":
					itm = xdc.aggregationDesc;			// the district name
					break;
				case "Nation":
					itm = this.lookups.byCode("sysParams", "USER_NATION", "N");			// the country name
					break;
			}
			let gnames = { M: " (Male)", F: " (Female)", T: " (all)" };
			let idarr = datum.seriesId.split(".");
			let genderSplit = idarr.length > 1;
			let seriesGenderCode = genderSplit ? idarr[1] : "T";
			let gitm = genderSplit ? gnames[seriesGenderCode] : "";
			datum.item = [`<b>${xdc.key.toUpperCase()}</b>`, itm + gitm].join(":");
			// limit the width of the tooltip, 
			datum.style = "width:200px; white-space:normal";

		}

		public aggregationSortFunction(row1, row2): number {
			let aggregationSortMap = {
				School: 0,
				District: 1,
				Nation: 2
			};
			let xdc1 = ExamXf.getExamDataXfKey(row1[0]);
			let xdc2 = ExamXf.getExamDataXfKey(row2[0]);
			let result = Sw.Charts.sortComparator(0)(xdc1.key, xdc2.key);
			if (result != 0) {
				return result;
			}
			return Sw.Charts.sortComparator(0)(aggregationSortMap[xdc1.aggregation], aggregationSortMap[xdc2.aggregation]);
		}
	}

	class ChartComponent implements ng.IComponentOptions {

		public bindings = {
			examXf: "<",
			aggregation: "@",
			_genderSplit: "<genderSplit",
			staMirrors: "<",
			touch: "@"
		}
		public templateUrl: string = `exam/dashboard/chart`;
		public controller = Controller;
		public controllerAs = "vm";
		public transclude = {
			'tools': "?chartTools",			       // a selector of some sort to put in the chart
		}
	}


	class CrosstabComponent implements ng.IComponentOptions {

		public bindings = {
			examXf: "<",
			aggregation: "@",					// for charting of aggregations by district or year or....
			_genderSplit: "<genderSplit",
			staMirrors: "<",
			touch: "@"
		}
		public templateUrl: string = `exam/dashboard/crosstab`;
		public controller = Controller;
		public controllerAs = "vm";
		public transclude = {
			'tools': "?chartTools",			       // a selector of some sort to put in the chart
		}
	}
	angular
		.module("pineapples")
		.component("examChart", new ChartComponent())
		.component("examCrosstab", new CrosstabComponent());
}