﻿namespace Pineapples.Dashboards.Exam {

	class Controller extends DashboardChild implements IDashboardChild {

		public group: CrossFilter.Group<any, any, any>;
		public groupData: any[]; // filtered all() array of keyvalue pairs
		public options: examOptions;
		public chartData; 
		public touch: number;
		public columnSortBy: any[];
		public dashboard: ExamsDashboard;

		protected defaultPalette;				// an array holding the chart palette
		public colorbrewer: ColorBrewer.Base = colorbrewer;
		public staMirrors;			// allow this to be passed as a literal binding
		public mirrors;					// bound onto mirror-chart

		public paletteName;
		public viewOptions;

		public onViewOptionChange() {
			this.refreshChart();				// this currently doesn;t work - viewOptions are not implemented
		};

		////////////////////////////////////////////////////////////////
		// LifeCycle events
		/////////////////////////////////////////////////////////////////
		public $onInit() {
			this.viewOptions = this.dashboard.dimStandardCode.group().all().map(d => d.key); //??
			this.configSupportingData();
			// this is the only place makeChartData is called
			// its job is to set up the AnnotatedGroup, which does not change:
			// the dependencies on changes are accounted for in the access functions in that annotated group
			// so they will be reflected whenever the data is 'transformed'
			// inside RowColChart.
			// Therefore, once the group is established, just 'touch' the chart ( ie change the value of 'touch')
			this.makeChartData();
		}

		public onOptionChange(changes:IOptionChangeData) {
			if (changes.selectedYear || changes.selectedExamCode) {
				this.configSupportingData();
			}
			this.refreshChart();
		};

		//////////////////////////////////////////////////////////////////////////////////////////////////
		// setup and configuration
		//////////////////////////////////////////////////////////////////////////////////////////////////

		public achievements: { C: number, D: string, N: string  }[];		

		/**
		 * The accessor functions in chartData reference the achievements list and multipliers
		 * As well, palette may change according to exam type,
		 * and the column sort order may change ( columnSortBy is bound)
		 * So these get recreated on an option chnage to selectedYear or selectedExamCode
		 */
		private configSupportingData() {
			if (this.options.selectedYear && this.options.selectedExamCode) {

				this.achievements = this.dashboard.achievementLevelsByExamYear();
				
				switch (this.achievements.length) {
					case 4:			// assume its an STA
				
						this.defaultPalette = this.dashboard.palettes.sta;
						this.mirrors = this.staMirrors;
						break;
					default:
						this.mirrors = [];			// in general no mirroring
						this.defaultPalette = colorbrewer.Oranges[9];
				}
				this.columnSortBy = this.achievements.map(a => a.C);
				this.viewOptions = _.map(this.dashboard.standardsByExamYear(), "N");
			}

		}

		private achievementDescription = (achievementLevel) => {
			let a = _.find(this.achievements, { C: parseInt(achievementLevel) });
			return a ? (<any>a).D : achievementLevel;
		}
		public makeChartData() {
			// this crosstab has an array for key, so we need a rowId function, normally this can be omitted
			// and allowed to 
			let rowId = (row) => `${row.key[1]}: ${row.key[2]}`;	
			let columns = (row) => this.achievements.map(achievement => achievement.C.toString());
			let valueAccessor = (row, col, colIdx) => row.value[col]?.T || 0;

			this.chartData = {
				group: this.group,
				rowId: rowId,
				columns: columns,
				valueAccessor: valueAccessor
			}
		}

		public refreshChart() {
			// ensure that touch is bound onto the RowColChart 
			// note it is an @ binding ie use like this:  touch="{{vm.touch}}"
			this.touch = Date.now();
		}

		/////////////////////////////////////////////////////////////
		// Chart callbacks
		/////////////////////////////////////////////////////////////


		public onChartRender(option, echart) {
			option.series.forEach((series, idx) => {
				// change the series name to the achievement description ('Well below competent'...)
				// or 'Level 1', 
				// Changing the series name will be reflected in tooltip, legend etc
				// although in this case, we still have to regenerate the legend data  to match
				series.name = _.find(this.achievements, { C: parseInt(series.id) })["D"];
				// apply the sorted palette to the sorted series
				/////series.color = this.palette[idx];
			});

			let o = {

				yAxis: {
					axisLabel: {
						// apply some detailed formatting to the benchmark description
						formatter: (value: string) => {
							let a = value.split(":");
							a[0] = `{bold|${a[0].toUpperCase()}}`
							return a.join(":");
						},
						rich: {
							bold: {
								color: "darkgrey",
								fontWeight: "bold"
							}
						}
					}
				},
				legend: {
					// mirrorChart has given us a legend in the correct order, but the name translate of series here means
					// we have to name translate the legend entries
					data: option.legend.data.map(value => this.achievementDescription(value))
				},
				grid: {
					left: 0
				}
			}
			angular.merge(option, o);
		}

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			// mirror chart has lready dealt with the abs of value
			// prettify the benchmark name, as in the Y axis:
			let a = datum.item.split(":");
			a[0] = `<b>${a[0].toUpperCase()}</b>`;
			datum.item = a.join(":");
			// limit the width of the tooltip, 
			datum.style = "width:600px; white-space:normal";

		}
	}

	class Component extends ExamDashboardComponent implements ng.IComponentOptions {

		constructor() {
			super(Controller, `resultsByBenchmark`);
			angular.merge(this.bindings, {
				staMirrors: "<"				// for mirror charts
			});
		}
	}

	angular
		.module("pineapples")
		.component("examResultsByBenchmark", new Component());
}