﻿namespace Pineapples.Dashboards {

	let palettes = {
		default: "Category10",
		sta4_1: ["#FF0000", "#FFC000", "#92D050", "#00B050"]
	}

	// this is the fields that come back in the source data table;
	// ie this directly matches the column names of the underlying warehouse view

	interface IXfData {
		examID,
		examCode,
		examYear,
		examName,
		StateID,
		State,
		Gender,
		standardID,
		standardCode,
		standardDesc,
		benchmarkID,
		benchmarkCode,
		benchmarkDesc,
		achievementLevel,
		achievementDesc,
		Candidates,
		DistrictCode,
		District
	}

	export class ExamsDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {
		// xf is defined in the parent class CrossfilterDashboard, but not typed
		// redfining it based on the type that describes the import data gives more intellisense and 
		// compile errors to catch typos.
		public xf: CrossFilter.CrossFilter<IXfData>;

		public xFilter: Sw.xFilter.XFilter;
		public options: examOptions;

		public table: any[][];								// the two recordsets are $q'ed together
		public examsDistrictResults: any;			// the legacy dataset
		public districtResults: any[];				// the exam district Results unnormalised

		public palettes = {
			default: ['#1010FF', '#223344', '#096723', '#123456'],
			// for STA, levels 1 2 3 4
			sta: ["#FF0000", "#FFC000", "#92D050", "#00B050"]
		}

		// dimensions and groups based on exam data
		// rather than create these individually, make an indexed collection of dimensions
		public dimensions: { [key: string]: CrossFilter.Dimension<IXfData, any> } = {};
		public groups: { [key: string]: CrossFilter.Group<IXfData, any, any>; } = {};

		public dimYear: CrossFilter.Dimension<IXfData, number>;
		public dimDistrictCode: CrossFilter.Dimension<IXfData, string>;

		// experiment...
		public dimYearDistrict: CrossFilter.Dimension<IXfData, any[]>;
		public dimExamCode: CrossFilter.Dimension<any, string>;
		public dimStandardCode: CrossFilter.Dimension<any, string>;
		public dimBenchmarkCode: CrossFilter.Dimension<any, any[]>;

		public grpYear: CrossFilter.Group<any, string, any>;
		public grpExamCode: CrossFilter.Group<any, string, any>;
		public grpBenchmarkCode: CrossFilter.Group<any, string, any>;
		public grpYearDistrict: CrossFilter.Group<any, any, any>;

		public standardsByExamYear;
		public benchmarksByExamYear;
		public achievementLevelsByExamYear;

		public examXf: Pineapples.Exams.ExamDashboardXf;

		//----------------------- Life Cycle hooks ----------------------------------
		public $onInit() {
			this.districtResults = this.table[0];
			this.examsDistrictResults = this.table[1];		// transitional support for legacy code


			this.examXf = new Pineapples.Exams.ExamDashboardXf(this.table[0]);

			this.makeCrossfilter();
			// set some defaults in the options box - 
			// year should be the most recent year in the loaded exams
			let g = this.examXf.examXf.xf.dimension(v => v.examYear).top(1);
			this.options.selectedYear = g.length ? g[0].examYear : null;
			this.options.selectedExamCode = g.length ? g[0].examCode : null;

			this.examXf.examXf.filterYear(this.options.selectedYear);
			this.examXf.examXf.dimensions.examCode.filter(this.options.selectedExamCode);

			// ready
			this.dashboard = this;
		}

		//----------------------- Life Cycle hooks ----------------------------------
		public $onChanges(changes) {
		}

		// ------------------------------ Crossfilter support ----------------------------
		public makeCrossfilter() {

			// create the crossfilter
			this.xf = crossfilter<IXfData>(this.districtResults);

			// obvious dimensions come from the aggregating fields in the source data
			//	examID,
			//  examCode,
			//	examYear,
			//	examName,
			//	StateID,
			//	State,
			//	Gender,
			//	standardID,
			//	standardCode,
			//	standardDesc,
			//	benchmarkID,
			//	benchmarkCode,
			//	benchmarkDesc,
			//	achievementLevel,
			//	achievementDesc,
			//	Candidates,
			//	DistrictCode,
			//	District
			this.dimensions["examYear"] = this.xf.dimension(d => d.examYear);
			this.dimensions["achievementLevel"] = this.xf.dimension(d => d.achievementLevel);

			this.groups["achievementLevel"] = this.xFilter.xReduce(this.dimensions.achievementLevel
				, "achievementLevel"
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));

			this.dimYear = this.xf.dimension(d => d.examYear);
			this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);

			this.dimExamCode = this.xf.dimension(d => d.examCode);
			this.dimStandardCode = this.xf.dimension(d => d.standardCode);
			this.dimBenchmarkCode = this.xf.dimension(d => [d.benchmarkID, d.benchmarkCode, d.benchmarkDesc]);

			this.grpYear = this.xFilter.xReduce(this.dimYear
				, "achievementLevel", this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));

			this.grpExamCode = this.xFilter.xReduce(this.dimExamCode
				, "achievementLevel", this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));
			this.grpBenchmarkCode = this.xFilter.xReduce(this.dimBenchmarkCode
				, "achievementLevel", this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));

			this.makeLookups();

		}
		// crossfilter dashboards will need to implement this, so that the options are applied as filters to the 
		// cross tab dimensions 
		// any implementation should call super implementation
		public onOptionChange(data: IOptionChangeData, sender) {
			// the update of the current dc obects happens here
			if (data.selectedYear) {
				this.dimYear.filter(this.options.selectedYear);
				// in the examChart crossfilters, the dimension keys are complex objectsthat include the recordType etc
				// this more-than-usually complicated filtering ensures we can filter on such dimensions, and get the expected behaviour
				// that filtering on a dimension is ignored for charts built on that dimension
				// this filtering is managed in the ExamXf obect which knows about the structure of these keys
				this.examXf.examXf.filterYear(this.options.selectedYear);
			}

			if (data.selectedDistrict) {
				this.dimDistrictCode.filter(this.options.selectedDistrict);
				this.examXf.examXf.filterDistrict(this.options.selectedDistrict);
			}

			if (data.selectedExamCode) {
				this.dimExamCode.filter(this.options.selectedExamCode);
				this.examXf.examXf.dimensions.examCode.filter(this.options.selectedExamCode);
			}

			if (data.selectedIsland) {
				this.examXf.examXf.filterIsland(this.options.selectedIsland);
			}

			if (data.selectedRegion) {
				this.examXf.examXf.filterRegion(this.options.selectedRegion);
			}

			if (data.selectedAuthorityGovt) {
				this.examXf.examXf.filterAuthorityGovt(this.options.selectedAuthorityGovt);
			}

			if (data.selectedAuthority) {
				this.examXf.examXf.dimensions.authorityCode.filter(this.options.selectedAuthority);
			}
			super.onOptionChange(data, sender);
		}




		// ------------------------------  ----------------------------

		//' lookups derived from the input data, that may depend on exam code / year
		// the memoized functions are reated here, rather than static, to give an easy way to regenerate them
		// (ie kill the caches and start over) if required
		public makeLookups() {

			// Return an array of the standards defined for an exam in a given year
			// the returned results is an array of {I,C,D, N }; similar to lookups
			// replaces standardForExam going forward
			let _standardsByExamYear = _.memoize(
				(exam, year) => {
					return _.uniqBy(
						this.districtResults
							.filter(({ examYear, examCode }) => (examYear === year && examCode == exam))
							.map(({ standardID, standardCode, standardDesc }) => {
								return {
									I: standardID,
									C: standardCode,
									D: standardDesc,
									N: `${standardCode}: ${standardDesc}`
								};
							})
						// _uniqBy : supply an iteratee to determine sameness of the object
						// since we know that standardCode and Description are derived from StandardID
						// we only need to check standardID (I) so we can use the property shorthand
						, "I"
					);
				}, // use a resolver becuase we have 2 arguments
				// this makes the key an array of the argument values
				(exam, year) => [exam, year]
				// or generally and mindlessly we can do this
				//(...args) => _.values(args).join("_")

			);
			let _benchmarksByExamYear = _.memoize(
				(exam, year) => {
					return _.uniqBy(
						this.districtResults
							.filter(({ examYear, examCode }) => (examYear === year && examCode == exam))
							.map(({ benchmarkID, benchmarkCode, benchmarkDesc, standardID }) => ({
								I: benchmarkID,
								C: benchmarkCode,
								D: benchmarkDesc,
								N: `${benchmarkCode}: ${benchmarkDesc}`,
								S: standardID						// benchamrks belong to a standard
							})
							)
						// _uniqBy : supply an iteratee to determine sameness of the object
						// since we know that benchamrkCode and Description are derived from BenchmarkID
						// we only need to check benchmarkID (I) so we can use the property shorthand
						, "I"
					).sort((a, b) => a.C < b.C ? -1 : 1);
				}, // use a resolver becuase we have 2 arguments
				(...args) => _.values(args).join("_")
			);

			let _achievementLevelsByExamYear = _.memoize(
				(exam, year) => {
					return _.uniqBy(
						this.districtResults
							.filter(({ examYear, examCode }) => (examYear === year && examCode == exam))
							.map(({ achievementLevel, achievementDesc }) => {
								return {
									C: achievementLevel,
									D: achievementDesc,
									N: `${achievementLevel}: ${achievementLevel}`
								};
							})
						// _uniqBy : supply an iteratee to determine sameness of the object
						, "C"
					).sort((a, b) => a.C - b.C);
				}, // use a resolver becuase we have 2 arguments
				(...args) => _.values(args).join("_")
			);

			this.standardsByExamYear = (exam, year) => {
				exam = exam || this.options.selectedExamCode;
				year = year || this.options.selectedYear;
				return _standardsByExamYear(exam, year);
			}
			this.benchmarksByExamYear = (exam, year) => {
				exam = exam || this.options.selectedExamCode;
				year = year || this.options.selectedYear;
				return _benchmarksByExamYear(exam, year);
			}

			this.achievementLevelsByExamYear = (exam, year) => {
				exam = exam || this.options.selectedExamCode;
				year = year || this.options.selectedYear;
				return _achievementLevelsByExamYear(exam, year);
			}
		}

		// -- utility to retrieve a property from a collection
		// -- see LookupSErvice.getByCode
		public lookup(collection: any[], searchValue, resultProperty?, searchProperty?) {
			searchProperty = searchProperty || "C";
			let r = _.find(collection, d => d[searchProperty] == searchValue);
			if (!r) {
				return null;
			};
			if (resultProperty) {
				return r[resultProperty];
			}
			return r;
		}

		//-----------------------------------------------------------------------------------------
		public exams = (year) => _.uniq(
			this.examsDistrictResults
				.filter(({ ExamYear }) => ExamYear === year)
				.map(({ Exam }) => Exam)
		);

		public standardsForExam = _.memoize(
			(exam, year) => _.uniq(
				this.examsDistrictResults
					.filter(({ ExamYear }) => ExamYear === year)
					.filter(({ Exam }) => Exam === exam)
					.map(({ ExamStandard }) => ExamStandard)
			),
			(year, examStandard) => [year, examStandard]);


		public dataExamResultsByStandardDistrict = _.memoize((year, exam) => {
			const mPercent = (level, sign) => x => ({
				ExamStandardState: x['ExamStandardState'] + ' - M',
				percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
			})
			const fPercent = (level, sign) => x => ({
				ExamStandardState: x['ExamStandardState'] + ' - F',
				percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
			})
			const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

			return _(this.examsDistrictResults)
				.filter(({ ExamYear }) => ExamYear === year)
				.filter(_.matchesProperty('Exam', exam))
				.map(x => _.assign({}, x, { ExamStandardState: `${x.ExamStandard} - ${x.DistrictCode}` }))

				.groupBy(_.property('ExamStandardState'))
				.mapValues((x: any[]) => x.reduce((acc, val) => _.assign({}, val,
					{
						CompetentM: acc.CompetentM + val.CompetentM || 0,
						CompetentF: acc.CompetentF + val.CompetentF || 0,
						MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
						MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
						ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
						ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
						WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
						WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
						CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
						CandidatesF: acc.CandidatesF + val.CandidatesF || 0
					}
				), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
				))
				.values()
				.map(data =>
					[
						{ meta: 'Approaching competence', color: "#FFC000", data: _.flatten(genderPercent('ApproachingCompetence', -1)(data)) },
						{ meta: 'Well below competent', color: "#FF0000", data: _.flatten(genderPercent('WellBelowCompetent', -1)(data)) },
						{ meta: 'Minimally competent', color: "#92D050", data: _.flatten(genderPercent('MinimallyCompetent', 1)(data)) },
						{ meta: 'Competent', color: "#00B050", data: _.flatten(genderPercent('Competent', 1)(data)) },
					])
				.flatten()
				.groupBy(_.property('meta'))
				.mapValues(x => _.assign({}, x[0], { data: _(x).map(_.property('data')).flatten().sortBy('ExamStandardState').value() })).values()
				.value()

		},
			(year, exam) => [year, exam]);


		public dataExamResultsByStandardThreeYears = _.memoize((year, exam) => {
			const startYear = year - 2
			const mPercent = (level, sign) => x => ({
				StandardYear: x['StandardYear'] + ' - M',
				percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
			})
			const fPercent = (level, sign) => x => ({
				StandardYear: x['StandardYear'] + ' - F',
				percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
			})
			const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

			return _(this.examsDistrictResults)
				.filter(({ ExamYear }) => ExamYear <= year)
				.filter(({ ExamYear }) => ExamYear >= startYear)
				.filter(_.matchesProperty('Exam', exam))
				.map(x => _.assign({}, x, { StandardYear: `${x.ExamStandard} - ${x.ExamYear} ` }))

				.groupBy(_.property('StandardYear'))
				.mapValues((x: any[]) => x.reduce((acc, val) => _.assign({}, val,
					{
						CompetentM: acc.CompetentM + val.CompetentM || 0,
						CompetentF: acc.CompetentF + val.CompetentF || 0,
						MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
						MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
						ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
						ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
						WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
						WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
						CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
						CandidatesF: acc.CandidatesF + val.CandidatesF || 0
					}
				), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
				))
				.values()
				.map(data =>
					[
						{ meta: 'Approaching competence', color: "#FFC000", data: _.flatten(genderPercent('ApproachingCompetence', -1)(data)) },
						{ meta: 'Well below competent', color: "#FF0000", data: _.flatten(genderPercent('WellBelowCompetent', -1)(data)) },
						{ meta: 'Minimally competent', color: "#92D050", data: _.flatten(genderPercent('MinimallyCompetent', 1)(data)) },
						{ meta: 'Competent', color: "#00B050", data: _.flatten(genderPercent('Competent', 1)(data)) },
					])
				.flatten()
				.groupBy(_.property('meta'))
				.mapValues(x => _.assign({}, x[0], { data: _(x).map(_.property('data')).flatten().sortBy('StandardYear').value() })).values()
				.value()

		},
			(year, exam) => [year, exam]);

		public dataExamResultsByStandardThreeYearsGenderColors = _.memoize((year, exam) => {
			const startYear = year - 2
			const mPercent = (level, sign) => x => ({
				StandardYear: x['StandardYear'] + ' - M',
				percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
			})
			const fPercent = (level, sign) => x => ({
				StandardYear: x['StandardYear'] + ' - F',
				percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
			})
			const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

			return _(this.examsDistrictResults)
				.filter(({ ExamYear }) => ExamYear <= year)
				.filter(({ ExamYear }) => ExamYear >= startYear)
				.filter(_.matchesProperty('Exam', exam))
				.map(x => _.assign({}, x, { StandardYear: `${x.ExamStandard} - ${x.ExamYear} ` }))

				.groupBy(_.property('StandardYear'))
				.mapValues((x: any[]) => x.reduce((acc, val) => _.assign({}, val,
					{
						CompetentM: acc.CompetentM + val.CompetentM || 0,
						CompetentF: acc.CompetentF + val.CompetentF || 0,
						MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
						MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
						ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
						ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
						WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
						WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
						CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
						CandidatesF: acc.CandidatesF + val.CandidatesF || 0
					}
				), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
				))
				.values()
				.map(data =>
					[
						{ meta: 'Approaching competence-F', color: "#FFC000", data: fPercent('ApproachingCompetence', -1)(data) },
						{ meta: 'Well below competent-F', color: "#FF0000", data: fPercent('WellBelowCompetent', -1)(data) },
						{ meta: 'Minimally competent-F', color: "#92D050", data: fPercent('MinimallyCompetent', 1)(data) },
						{ meta: 'Competent-F', color: "#00B050", data: fPercent('Competent', 1)(data) },
						{ meta: 'Approaching competence-M', color: "#8000ff", data: mPercent('ApproachingCompetence', -1)(data) },
						{ meta: 'Well below competent-M', color: "#00ff00", data: mPercent('WellBelowCompetent', -1)(data) },
						{ meta: 'Minimally competent-M', color: "#d05092", data: mPercent('MinimallyCompetent', 1)(data) },
						{ meta: 'Competent-M', color: "#b01500", data: mPercent('Competent', 1)(data) },
					])
				.flatten()
				.groupBy(_.property('meta'))
				.mapValues(x => _.assign({}, x[0], { data: _(x).map(_.property('data')).flatten().sortBy('StandardYear').value() })).values()
				.value()
		},
			(year, exam) => [year, exam]);

		public dataExamResultsByBenchmarkForYear = _.memoize((year, exam) => {

			const mPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - M',
				percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
			})
			const fPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - F',
				percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
			})
			const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

			return _(this.examsDistrictResults)
				.filter(_.matchesProperty('ExamYear', year))
				.filter(_.matchesProperty('Exam', exam))
				.groupBy(_.property('ExamBenchmark'))
				.mapValues((x: any) => x.reduce((acc, val: any) => _.assign({}, val,
					{
						CompetentM: acc.CompetentM + val.CompetentM || 0,
						CompetentF: acc.CompetentF + val.CompetentF || 0,
						MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
						MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
						ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
						ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
						WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
						WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
						CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
						CandidatesF: acc.CandidatesF + val.CandidatesF || 0
					}
				), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
				))
				.values()
				.groupBy(_.property('ExamStandard'))
				.mapValues((d: any) => d.map(data =>
					[
						{
							data: _.flatten(genderPercent('ApproachingCompetence', -1)(data)),
							color: '#FFC000',
							meta: 'Approaching competence'
						},
						{
							data: _.flatten(genderPercent('WellBelowCompetent', -1)(data)),
							color: '#FF0000',
							meta: 'Well below competent'
						},
						{
							data: _.flatten(genderPercent('MinimallyCompetent', 1)(data)),
							color: '#92D050',
							meta: 'Minimally competent'
						},
						{
							data: _.flatten(genderPercent('Competent', 1)(data)),
							color: '#00B050',
							meta: 'Competent'
						}
					]))
				.mapValues((x: object[]) => [{ data: _(x).flatten().filter(_.matchesProperty('meta', "Approaching competence")).map(_.property('data')).flatten().value(), color: "#FFC000", meta: "Approaching competence" },
				{ data: _(x).flatten().filter(_.matchesProperty('meta', "Well below competent")).map(_.property('data')).flatten().value(), color: "#FF0000", meta: "Well below competent" },
				{ data: _(x).flatten().filter(_.matchesProperty('meta', "Minimally competent")).map(_.property('data')).flatten().value(), color: "#92D050", meta: "Minimally competent" },
				{ data: _(x).flatten().filter(_.matchesProperty('meta', "Competent")).map(_.property('data')).flatten().value(), color: "#00B050", meta: "Competent" }]).value()

		},
			(year, exam) => [year, exam]);


	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			table: "<",
			options: "<",			// get the options
			// user configurable
			userPalette: "<colors",
			userTitle: "@headingTitle",
			mirrors: "<"				// for mirror charts

		};
		public controller: any = ExamsDashboard;
		public controllerAs: string = "vm";
		public templateUrl: string = `exam/Dashboard`;
	}

	angular
		.module("pineapples")
		.component("examsDashboard", new Component());


	// component options for dashboard children
	export class ExamDashboardComponent extends DashboardChildComponent implements ng.IComponentOptions {
		public get feature() {
			return "exam";
		}

		constructor(public controller: any, templateUrl) {
			super(controller, templateUrl);
		}
	}
}

