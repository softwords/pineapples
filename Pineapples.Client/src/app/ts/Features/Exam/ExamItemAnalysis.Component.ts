﻿namespace Pineapples.Exams {

	class Controller {
		public colorbrewer: ColorBrewer.Base = colorbrewer;

		public exam: Exam;
		public item;
		public palette;
		public answercolor = 'goldenrod';

		public indicator;
		public benchmark;
		public standard;

		public expectedDifficulty;
		public assessedDifficulty;

		public itemNo;
		static $inject = ["$state", "$http"];
		public constructor(public state: ng.ui.IStateService, public http: ng.IHttpService, public restangular: restangular.IService) { }

		public $onInit() {
			let pal = this.colorbrewer.Pastel2[6];
			this.palette = [
				this.item.iaAnswer == 'A' ? this.answercolor : pal[0]
				, this.item.iaAnswer == 'B' ? this.answercolor : pal[1]
				, this.item.iaAnswer == 'C' ? this.answercolor : pal[2]
				, this.item.iaAnswer == 'D' ? this.answercolor : pal[3]
				, this.item.iaAnswer == 'E' ? this.answercolor : pal[4]
				, this.item.iaAnswer == 'F' ? this.answercolor : pal[5]
				, '#FFFFFF'
			];
			this.indicator = _.find(this.exam.ExamFrame, ef => ef.Code == this.item.IndicatorKey);
			this.benchmark = _.find(this.exam.ExamFrame, ef => ef.Code == this.item.BenchmarkKey);
			this.standard = _.find(this.exam.ExamFrame, ef => ef.Code == this.item.StandardKey);

			if (this.item.iaCorrectPerc < 33.33)
				this.assessedDifficulty = "Hard";
			else if (this.item.CorrectPerc > 66.66)
				this.assessedDifficulty = "Easy";
			else
				this.assessedDifficulty = "Moderate";



			switch (<string>(this.item.exItemCode).substr(21, 1).toLowerCase()) {
				case "E":
				case "e":
					this.expectedDifficulty = "Easy";
					break;
				case "M":
				case "m":
					this.expectedDifficulty = "Moderate";
					break;
				case "H":
				case "h":
				case "D":
				case "d":
					this.expectedDifficulty = "Hard";
					break;
			}

			this.itemNo = +(this.item.exItemCode.substr(5, 3));

		}

		public renderResponseChart(option) {
			Sw.Charts.chartOps(option)
				.showLegend(false);
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				exam: "<"		// exam
				, item: "<"		// item record
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "exam/examItemAnalysis";
		}
	}
	angular
		.module("pineapples")
		.component("examItemAnalysis", new Component());
}