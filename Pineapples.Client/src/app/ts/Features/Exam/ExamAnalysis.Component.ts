﻿namespace Pineapples.Exams {

	class Controller {

		public model: Exam;

		static $inject = ["$state", "$http"];
		public constructor(public state: ng.ui.IStateService, public http: ng.IHttpService, public restangular: restangular.IService) { }

		public numItems: number;

		public sumItemVarianceP: number
		public totalScoreVarianceP: number

		public alpha: number;
		public totalScoreStdDev: number;
		public standardErrorOfMeasurement: number

		public $onInit() {
			this.numItems = this.model.ItemAnalysis.length;
			this.sumItemVarianceP = _.sumBy(this.model.ItemAnalysis, "iaVarianceP");
			this.totalScoreVarianceP = this.model["exVarianceP"];
			this.alpha = (this.numItems) / (this.numItems - 1) * (1 - (this.sumItemVarianceP / this.totalScoreVarianceP));
			// standard error of measurement derived from alpha
			this.totalScoreStdDev = Math.sqrt(this.totalScoreVarianceP);
			this.standardErrorOfMeasurement = this.totalScoreStdDev * Math.sqrt(1 - this.alpha);
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				model: "<"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "exam/examanalysis";
		}
	}
	angular
		.module("pineapples")
		.component("examAnalysis", new Component());
}