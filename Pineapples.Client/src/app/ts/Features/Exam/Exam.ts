﻿namespace Pineapples.Exams {


	export enum ExamContextAggregation {
		school = "School",
		district = "District",
		nation = "Nation"
	}

	export enum ExamContextRecordType {
		indicator = "Indicator",
		benchmark = "Benchmark",
		standard = "Standard",
		exam = "Exam"

	}
	/**
	 * Data returned by Conext Data method, used for production of 
	 * 'version 2' exams analysis 
	 */
	export type ExamContextData = {
		examID: number,
		examCode: string,
		examYear: number,
		Gender: string,
		Aggregation: ExamContextAggregation,
		AggregationKey: string,
		AggregationDesc: string,
		RecordType: ExamContextRecordType,
		Key: string,
		Description: string,
		achievementLevel: number,
		achievementDesc: string,
		indicatorCount: number,
		weight: number,
		candidateCount: number,

		// used in dashboard versions
		DistrictCode?: string,
		IslandCode?: string,
		RegionCode?: string,
		AuthorityCode?: string,
		AuthorityGovtCode?: string
	}


	export type ExamXfDimensions = {
		year: CrossFilter.Dimension<ExamContextData, string>,
		recordType: CrossFilter.Dimension<ExamContextData, ExamContextRecordType>,
		key: CrossFilter.Dimension<ExamContextData, string>,
		aggregationKey: CrossFilter.Dimension<ExamContextData, string>,
		examCode: CrossFilter.Dimension<ExamContextData, string>,
		districtCode: CrossFilter.Dimension<ExamContextData, string>,
		islandCode: CrossFilter.Dimension<ExamContextData, string>,
		regionCode: CrossFilter.Dimension<ExamContextData, string>,
		authorityCode: CrossFilter.Dimension<ExamContextData, string>,
		authorityGovtCode: CrossFilter.Dimension<ExamContextData, string>
	};

	export type ExamXfGroups = {
		year: CrossFilter.Group<ExamContextData, number, any>,
		key: CrossFilter.Group<ExamContextData, string, any>,
		district?: CrossFilter.Group<ExamContextData, string, any>,
		island: CrossFilter.Group<ExamContextData, string, any>,
		region: CrossFilter.Group<ExamContextData, string, any>,
		authorityGovt: CrossFilter.Group<ExamContextData, string, any>
	};

	/**
	 * The composite key in the 'dimKey' dimension in ExamXf
	 * 
	 */
	export type ExamXfKey = {
		key: string,
		description: string,
		aggregation: string,
		aggregationKey: string,
		aggregationDesc: string,
		recordType: string
	}

	export type ExamAchievementLevelData = {
		C: number,
		D: string,
		N: string
	}

	export class ExamAggregationLevels {
		public first = () => this.data?.[0];
		public last = () => this.data?.[this.data.length - 1];
		/**
		 * return available values for achivement levels, as an array
		 */
		public values = () => this.data;
		constructor(public data: string[]) { }

	}
	export class ExamAchievementLevels {

		public description = (achievementLevel: number) => this.find(achievementLevel)?.D;
		public max = () => this.data?.[this.data.length - 1].C;
		public find = (achievementLevel: number) => _.find(this.data, al => +achievementLevel == al.C);
		/**
		 * return available values for achivement levels, as an array
		 */
		public values = () => this.data.map(al => al.C);
		constructor(public data: ExamAchievementLevelData[]) { }
	}

	export class ExamXf {
		public xf: CrossFilter.CrossFilter<ExamContextData>;
		public dimensions: ExamXfDimensions;
		public groups: ExamXfGroups;
		public xFilter: Sw.xFilter.IXFilter;
		public contextData = () => this.xf.all();
		public achievementLevels: ExamAchievementLevels;
		public aggregationLevels: ExamAggregationLevels;


		// functions to allow filtering on dimensions with complex keys
		public static aggregationkeyFilter = (aggregationKey: string) => {
			return (v) => aggregationKey ? (ExamXf.getExamDataXfKey(v).aggregationKey == aggregationKey) : true;
		}

		public filterYear(selectedYear: number) {
			this.dimensions.year.filterFunction(ExamXf.aggregationkeyFilter(selectedYear.toString()));
		}

		public filterDistrict(selectedDistrict: string) {
			this.dimensions.districtCode.filterFunction(ExamXf.aggregationkeyFilter(selectedDistrict));
		}

		public filterIsland(selectedIsland: string) {
			this.dimensions.islandCode.filterFunction(ExamXf.aggregationkeyFilter(selectedIsland));
		}

		public filterRegion(selectedRegion: string) {
			this.dimensions.regionCode.filterFunction(ExamXf.aggregationkeyFilter(selectedRegion));
		}

		public filterAuthorityGovt(selectedAuthGovt: string) {
			this.dimensions.authorityGovtCode.filterFunction(ExamXf.aggregationkeyFilter(selectedAuthGovt));
		}

		constructor(contextdata: ExamContextData[]) {
			let xf = crossfilter<ExamContextData>(contextdata);
			this.xFilter = new Sw.xFilter.XFilter();
			let dimYear = xf.dimension(ExamXf.makeExamDataXfYearKey);
			let dimAggregationKey = xf.dimension(v => v.AggregationKey);
			let dimRecordType = xf.dimension(v => v.RecordType);
			let dimExamCode = xf.dimension(v => v.examCode);
			let dimKey = xf.dimension(ExamXf.makeExamDataXfKey);
			let dimDistrict = xf.dimension(ExamXf.makeExamDataXfDistrictKey);
			let dimIsland = xf.dimension(ExamXf.makeExamDataXfIslandKey);
			let dimRegion = xf.dimension(ExamXf.makeExamDataXfRegionKey);
			let dimAuthority = xf.dimension(d => d.AuthorityCode);
			let dimAuthorityGovt = xf.dimension(ExamXf.makeExamDataXfAuthGovtKey);
			let dimAchievement = xf.dimension(d => d.achievementLevel);


			let valueAccessor = this.xFilter.getFlattenedGenderAccessor("Gender", ["candidateCount", "indicatorCount", "weight"]);


			this.xf = xf;
			this.dimensions = {
				year: dimYear,
				recordType: dimRecordType,
				key: dimKey,
				aggregationKey: dimAggregationKey,
				examCode: dimExamCode,
				districtCode: dimDistrict,
				islandCode: dimIsland,
				regionCode: dimRegion,
				authorityCode: dimAuthority,
				authorityGovtCode: dimAuthorityGovt


			};
			this.groups = {
				year: this.xFilter.xReduce(dimYear, "achievementLevel", valueAccessor),
				district: this.xFilter.xReduce(dimDistrict, "achievementLevel", valueAccessor),
				island: this.xFilter.xReduce(dimIsland, "achievementLevel", valueAccessor),
				region: this.xFilter.xReduce(dimRegion, "achievementLevel", valueAccessor),
				authorityGovt: this.xFilter.xReduce(dimAuthorityGovt, "achievementLevel", valueAccessor),
				key: this.xFilter.xReduce(dimKey, "achievementLevel", valueAccessor)
			};


			// build the achievement levels
			let al =
				_.uniqBy(
					contextdata
						.map(({ achievementLevel, achievementDesc }) => {
							return {
								C: achievementLevel,
								D: achievementDesc,
								N: `${achievementLevel}: ${achievementLevel}`
							};
						})
					// _uniqBy : supply an iteratee to determine sameness of the object
					, "C"
				).sort((a: any, b: any) => a.C - b.C);
			this.achievementLevels = new ExamAchievementLevels(al);

			// build the aggregation levels, sorted in correct order
			let aggregationSorter = (agg1, agg2): number => {
				let aggregationSortMap = {
					School: 0,
					District: 1,
					Nation: 2
				};
				return Sw.Charts.sortComparator(0)(aggregationSortMap[agg1], aggregationSortMap[agg2]);
			}
			let ag =
				_.uniq(
					contextdata
						.map(({ Aggregation }) => Aggregation)
				).sort(aggregationSorter);
			this.aggregationLevels = new ExamAggregationLevels(ag);
		}

		private static KEY_JOIN = String.fromCharCode(1);
		private static makeExamDataXfKey(v: ExamContextData) {
			return [v.Key, v.Description, v.Aggregation, v.AggregationKey, v.AggregationDesc, v.RecordType].join(ExamXf.KEY_JOIN);
		}

		private static makeExamDataXfYearKey(v: ExamContextData) {
			return [v.Key, v.Description, "Year", v.examYear, v.examYear, v.RecordType].join(ExamXf.KEY_JOIN);
		}
		private static makeExamDataXfDistrictKey(v: ExamContextData) {
			return [v.Key, v.Description, "District", v.DistrictCode, v.DistrictCode, v.RecordType].join(ExamXf.KEY_JOIN);
		}
		private static makeExamDataXfIslandKey(v: ExamContextData) {
			return [v.Key, v.Description, "Island", v.IslandCode, v.IslandCode, v.RecordType].join(ExamXf.KEY_JOIN);
		}
		private static makeExamDataXfRegionKey(v: ExamContextData) {
			return [v.Key, v.Description, "Region", v.RegionCode, v.RegionCode, v.RecordType].join(ExamXf.KEY_JOIN);
		}
		private static makeExamDataXfAuthGovtKey(v: ExamContextData) {
			return [v.Key, v.Description, "AuthorityGovt", v.AuthorityGovtCode, v.AuthorityGovtCode, v.RecordType].join(ExamXf.KEY_JOIN);
		}
		/**
		 * Return an object parsing the string key in grpKey
		 * @param string value of key in Xf group
		 */
		public static getExamDataXfKey(contextstr: string): ExamXfKey {
			let arr = contextstr.split(ExamXf.KEY_JOIN);
			return {
				key: arr[0],
				description: arr[1],
				aggregation: arr[2],
				aggregationKey: arr[3],
				aggregationDesc: arr[4],
				recordType: arr[5]
			}
		}
	}


	export class ExamDashboardXf {
		public examXf: ExamXf;
		public get xf() {
			return <CrossFilter.CrossFilter<ExamContextData>>(this.examXf.xf);
		}
		// filtering dimension
		constructor(contextdata: ExamContextData[]) {
			this.examXf = new ExamXf(contextdata);

		}
	}

	export class Exam extends Pineapples.Api.Editable implements Sw.Api.IEditable {

		static $inject = ["Restangular", "data"];
		constructor(restangular: restangular.IService, examData) {
			super();
			this._transform(examData);
			angular.extend(this, examData);
			// bit of a kludge here: restangular will add the property 'route' (='exam')
			// when the item is restangularized, but that hasn;t happened yet
			// so set the route manually here so it is bound into the collection as well
			this.route = "exams";
			// this way we get url in the school types collection like
			// api/census/2021/schooltypes/PS
			// otherwise its api/undefined/2021/schooltypes/PS
			restangular.restangularizeCollection(this, this.ExamFrame, "examframes");
		}

		// create static method returns the object constructed from the resultset object from the server

		public static create(resultSet: any, injector: ng.auto.IInjectorService) {
			let app = injector.instantiate(Exam, { data: resultSet });
			return app;
		};

		public exID: number;
		public exCode: string;
		public exYear: number;
		public Results: ExamContextData[];
		public ExamFrame: any[];			// the standards, benchmarks, indicators
		public ItemAnalysis: any[];
		// IEditable implementation
		public _name() {
			return this.exCode;
		}
		public _type() {
			return "exam";
		}
		public _id() {
			return this.exID
		}

		protected route;

		public _transform(newData) {
			// convert these incoming data values
			// transformDates is deprecated: http interceptor now converts dates on load
			//this._transformDates(newData, ["exDate"]);
			return super._transform(newData);
		}
	}
}