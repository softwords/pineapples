﻿namespace Pineapples.Exams {

	class Controller implements Sw.Component.ISuperGridParent {

		public srcrows: any[];		// input unsorted
		public rowset: any[];			// bound to the supergrid

		static $inject = ["$state", "$http"];
		public constructor(public state: ng.ui.IStateService, public http: ng.IHttpService, public restangular: restangular.IService) { }

		public $onInit() {
			
			this.http.get(`api/examframes/info/viewmode`)
				.then(result => {
					this.frameViewMode = result.data;
					this.rowset = this.srcrows;
				});
		}

		private _table: string;
		public get table() {
			return this._table;
		}
		public set table(value) {
			this._table = value;
		};


		public frameViewMode;

		public registerGridApi(gridApi: uiGrid.IGridApi) { }
		public onSortClear(): void {
			this.rowset = this.srcrows;
		};
		public onSort(column: string, direction: string) {
			this.rowset = _.orderBy(this.srcrows, column);
			if (direction == "desc") {
				this.rowset = this.rowset.reverse();
			}
		}
		public onShowPage(newPage: number, pageSize: number) { }
		public onHighlightChanged(rowEntity: any, isSelected: boolean) { }
		public onAction(actionName: string, columnField: string, rowEntity: any) { }

	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				tableList: "<",
				srcrows: "<rowset",
				viewMode: "<",
				isEditing: "<"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "exam/examframe";
		}
	}
	angular
		.module("pineapples")
		.component("examFrame", new Component());
}