﻿module Pineapples.Api {

	type ExamContextData = Exams.ExamContextData;
	type ExamXf = Exams.ExamXf;

	export interface IExamsApi {
		/**
		 * Read a single exam
		 * @param id
		 */
		read(id: number): ng.IPromise<any>;
		/**
		 * create a new empty exam
		 * unlkely to be used, since exams are imported from an external system
		 */
		newExam(): ng.IPromise<Exams.Exam>;

		filterPaged(fltr): ng.IPromise<any>;
		/**
		 * Return 'contextdata', showing the school, its district and nation
		 * @param schoolNo
		 * @param examCode
		 * @param examYear
		 */
		contextData(schoolNo: string, examCode: string, examYear: number): ng.IPromise<ExamContextData[]>;

		/**
		 * Return an ExamXf object, holding various crosstab data for a contextData recordset
		 * @param schoolNo
		 * @param examCode
		 * @param examYear
		 */
		contextDataXf(schoolNo: string, examCode: string, examYear: number): ng.IPromise<ExamXf>;
	}
	const ENTITY = "exams";
	class apiService implements IExamsApi {

		static $inject = ["$q", "$http", "Restangular", "xFilter", "reflator"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService
			, public xFilter: Sw.xFilter.IXFilter
			, public reflator: Sw.Api.IReflator) {
		}
		public read(id) {
			return this.restangular.one(ENTITY, id).get();
		}

		public newExam(): ng.IPromise<Exams.Exam> {
			//return this.restangular.one(ENTITY, 0).get();
			let e = new Pineapples.Exams.Exam(this.restangular, {
				exID: null, ExamFrame: null
			});
			this.restangular.restangularizeElement(null, e, "exams");
			return this.$q.resolve<Exams.Exam>(e);
		}

		public filterPaged(fltr) {
			return this.http.post(`api/${ENTITY}/collection/filter`, fltr)
				.then(response => (response.data));
		}

		public contextData(schoolNo: string, examCode: string, examYear: number): ng.IPromise<ExamContextData[]> {
			let params = {
				schoolNo: schoolNo,
				examCode: examCode,
				examYear: examYear
			}
			let querystring = Object.keys(params).map(key => key + '=' + params[key]).join('&');
			return this.reflator.get<ExamContextData[]>(`api/${ENTITY}/contextdata?${querystring}`)
				.then(response => (response.data));
				
		}

		public contextDataXf(schoolNo: string, examCode: string, examYear: number) {
			return this.contextData(schoolNo, examCode, examYear)
				.then(data => new Exams.ExamXf(data));
		}
	}

angular.module("pineapplesAPI")
	.service("examsAPI", apiService);
}