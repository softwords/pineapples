﻿namespace Pineapples.Exams {

	angular
		.module("pineapples")
		.component("examRenderFrame", new Sw.Component.RenderFrameComponent("exam"));
}