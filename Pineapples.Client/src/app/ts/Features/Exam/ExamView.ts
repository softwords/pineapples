﻿namespace Pineapples.Exams {

	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'exID',
				name: 'Exam ID',
				displayName: 'Exam ID',
				editable: false,
				pinnedLeft: true,
				width: 70,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm")
			},
			{
				field: 'exCode',
				name: 'Exam Code',
				displayName: 'Exam Name',
				pinnedLeft: true,
				width: 240,
				lookup: 'examTypes',
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'exYear',
				name: 'Exam Year',
				displayName: 'Exam Year',
				pinnedLeft: true,
				width: 100,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'exUser',
				name: 'Uploaded By',
				displayName: 'Uploaded By',
				pinnedLeft: true,
				width: 200,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'exDate',
				name: 'Uploaded Date',
				displayName: 'Uploaded Date',
				cellFilter: "date:'d-MMM-yyyy'",
				pinnedLeft: true,
				width: 130,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
		]
	};


	let modes = [
		{
			key: "Results",
			gridOptions: {
				columnDefs: [
					{
						field: 'exCandidates',
						name: 'Candidates',
						displayName: 'Candidates',
						cellClass: 'gdAlignRight',
					},
					{
						field: 'exMarks',
						name: 'Marks',
						displayName: 'Marks',
						cellClass: 'gdAlignRight',
					},
					{
						field: 'docID',
						name: 'ExamSource',
						displayName: 'Data',
						width: 70,
						cellClass: 'gdAlignRight',
						cellTemplate: `
							<div class="ui-grid-cell-contents gdAction" >
								<span permission permission-only="'ExamOps'" >
									<document-image document = "row.entity.docID" height = "10" > </document-image>
								</span>
							</div>`
					},
				]
			}
		},
	];

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};

	angular
		.module('pineapples')
		.run(['ExamFilter', pushModes]);
}
