﻿namespace Pineapples.Exams {

  interface IBindings {
    model: Exam;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: Exam;
    public examXf: ExamXf;       // cross filter of the exam results

    static $inject = ["ApiUi", "examsAPI"];
    constructor(apiui: Sw.Api.IApiUi, private api: Api.IExamsApi) {
      super(apiui);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
      if (changes.model) {
        this.examXf = new ExamXf(this.model.Results);
			}
		}

		protected getNew = () => {
			return this.api.newExam();
    }
  }

  angular
    .module("pineapples")
    .component("componentExam", new Sw.Component.ItemComponentOptions("exam", Controller));
}