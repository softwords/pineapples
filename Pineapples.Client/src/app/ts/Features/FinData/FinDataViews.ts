namespace Pineapples.FinData {

	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'fnmID',
				name: 'ID',
				displayName: 'ID',
				editable: false,
				width: 60,
				//pinnedLeft: true,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item","vm")
			},
			{
				field: 'fnmYear',
				name: 'fnmYear',
				displayName: 'Year',
				editable: false,
				width: 60,
				//pinnedLeft: true,
			},
			{
				field: 'fnmDistrict',
				name: 'fnmDistrict',
				displayName: 'District',
				headerCellFilter: 'vocab',
				editable: false,
				width: 120,
				//pinnedLeft: true,
				lookup: 'expenditurePoints'
			}
			

		]
	};
  let modes = [
    {
      key: "Default",
      columnSet: 0,
			gridOptions: {
        columnDefs: [
					{
						field: 'fnmExpTotA',
						name: 'fnmExpTotA',
						displayName: 'Actual Exp',
						cellClass: "gdAlignRight",
						editable: false,
						width: 150
					},
					{
						field: 'fnmExpTotB',
						name: 'fnmExpTotB',
						displayName: 'Budgeted Exp',
						cellClass: "gdAlignRight",
						editable: false,
						width: 150
					},
					{
						field: 'fnmGNP',
						name: 'fnmGNP',
						displayName: 'GNI',
						cellClass: "gdAlignRight",
						editable: false,
						width: 150
					},
					{
						field: 'fnmGNPCapita',
						name: 'fnmGNPCapita',
						displayName: 'per Capita',
						cellClass: "gdAlignRight",
						editable: false,
						width: 150
					},
					{
						field: 'fnmGNPCurrency',
						name: 'fnmGNPCurrency',
						displayName: 'Curr',
						editable: false,
						width: 80
					},
        ]
      }
    },
  ]; // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};


  angular
    .module('pineapples')
    .run(['FinDataFilter', pushModes]);
}
