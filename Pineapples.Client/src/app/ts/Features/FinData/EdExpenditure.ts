﻿namespace Pineapples.FinData {

  // this needs to be maintained if more resultsets are added to the stored proc
	export class EdExpenditure extends Pineapples.Api.Editable implements Sw.Api.IEditable{

    // constructor just expects an object that is the school data, not the related data
    // this can be called from new school
    constructor(data) {
      super();
      angular.extend(this, data);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
			return new EdExpenditure(resultSet)
    }

    
    public _name() {
			return `${this.xedCostCentre} ${this.xedItem||""}` ;
    }
    public _type() {
      return "edexpenditure";
    }
    public _id() {
			return this.xedID;
		}

		public xedID: number;

		public fnmID: number;			// parent Id of findata
		public xedCostCentre: string;
		public xedItem: string;
		public sofCode: string;
		public xedTotA: number;
		public xedTotB: number;

		public xedCurrentA: number;
		public xedCurrentB: number;
		



  }
}
