namespace Pineapples.FinData {

	let routes = function ($stateProvider) {
		var featurename = 'findata';
		var filtername = 'FinDataFilter';
		var templatepath = "finData";
		var url = "findata";
		var usersettings = null;
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath);


		state.resolve = state.resolve || {};
		state.resolve["api"] = "finDataAPI";
		state.resolve["pageTitle"] = Sw.Utils.RouteHelper.featureTitle("Finance");

		state.data = state.data || {};
		state.data.frameTitle = "Education Financing";
		state.data.icon = "attach_money";
		state.data.permissions = {
			only: "FinanceRead"
		};
		// initialise the findata lookups when findata is first used
		// this is a pattern for loading on-demand, specialised codesets
		state.resolve["customLookups"] = ["Lookups", (lookups: Pineapples.Lookups.LookupService) => lookups.findata()]

		let basestate= "site.findata";
		$stateProvider.state(basestate, state);

		// List State
		state = Sw.Utils.RouteHelper.frameListState("findata", "fnmID");
		let statename = `${basestate}.list`;
		$stateProvider.state(statename, state);

		// state for a high level dashboard
		state = {
			url: "^/findata/dashboard",
			data: {
				permissions: {
					only: 'FinanceRead'
				},
				rendermode: "Dashboard"
			},
			views: {
				"renderarea": {
					component: "finDataDashboard"
				},
				"searcher": "findataOptionsEditor"
			},
			resolve: {
				options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
					return new Pineapples.Dashboards.coreOptions(lookups, scope.hub);
				}],
				tables: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
					// overall level 
					let p = reflator.get("api/warehouse/finance").then(response => (<any>response.data));
					// level by standard
					let p2 = reflator.get("api/warehouse/finance/district").then(response => (<any>response.data));
					// use $q.all to return both sets in an array
					return q.all([p, p2]);
				}],
				pageTitle: Sw.Utils.RouteHelper.simpleTitle("Dashboard")
			}
		};
		$stateProvider.state("site.findata.dashboard", state);


		// new - state with a custom url route
		state = {
			url: "^/findata/new",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'FinanceWrite'
				}
			},
			views: {
				"actionpane@site.findata": {
					component: "componentFinData"
				}

			},
			resolve: {
				model: ['finDataAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}],
				pageTitle: Sw.Utils.RouteHelper.simpleTitle("(new)")
			}
		};
		$stateProvider.state("site.findata.list.new", state);

		state = {
			url: "^/findata/reports",
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			data: {
				permissions: {
					only: 'FinanceRead'
				}
			},
			resolve: {
				folder: () => "School_inspections",  // actually School inspection folder in Jasper but URI is underscore
				promptForParams: () => "always",
				pageTitle: ["pageTitle", (title) => `{title} Reports`]
			}
		}
		$stateProvider.state("site.findata.reports", state);

		// item state
		state = {
			url: "^/findata/{id}",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'FinanceRead'
				}
			},
			views: {
				"actionpane@site.findata": {
					component: "componentFinData"
				}
			},
			resolve: {
				model: ['finDataAPI', '$stateParams', (api, $stateParams) => api.read($stateParams.id)],
				pageTitle: ["model", "rootPageTitle", (model, title) => `${title} * Finance: ${model.fnmDistrict} ${model.fnmYear}`]
			}
		};
		$stateProvider.state("site.findata.list.item", state);

		// utility route for resetting templates
		state = {
			url: '^/findata/reload',
			onEnter: ["$state", "$templateCache", function ($state: ng.ui.IStateService, $templateCache) {
				$templateCache.removeAll();
				$state.reload();
			}]
		};
		statename = "site.findata.reload";
		$stateProvider.state(statename, state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}
