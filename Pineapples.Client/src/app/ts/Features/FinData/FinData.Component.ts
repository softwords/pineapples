﻿namespace Pineapples.FinData {

  class Controller extends Sw.Component.ComponentEditController {
    public model: FinData;

    static $inject = ["ApiUi", "finDataAPI","Lookups"];
    constructor(apiui: Sw.Api.IApiUi, private api: any, public lookups:Sw.Lookups.LookupService) {
      super(apiui);
    }

		public get formTitle() {
			return (this.isNew)?
				"New Govt Expenditure History":
				`Govt Expenditure ${this.model.fnmYear}: ${this.lookups.byCode("expenditurePoints", this.model.fnmDistrict, "N")}`;
		}

		protected getNew = () => {
			return this.api.new();
		}

    public $onChanges(changes) {
      super.$onChanges(changes);
		}


		public get GNPCurrency() {
			return this.model.fnmGNPCurrency;
		}
		public set GNPCurrency(curr) {
			if (curr) {
				curr = curr.toUpperCase()
			}
			this.model.fnmGNPCurrency = curr;
			if (curr == "USD") {
				this.model.fnmGNPXchange = 1;
			}
		}

  }

  angular
    .module("pineapples")
    .component("componentFinData", new Sw.Component.ItemComponentOptions("findata", Controller));
}