﻿namespace Pineapples.Dashboards {
	export namespace FinData {
		export interface IxfEdExpenditure {
			SurveyYear: number
			DistrictCode: string;
			SectorCode: string;
			CostCentreCode: string;
		}

	}

	class edExpAccumulator {
		constructor(d, o) {
			this.EdExpA = d.EdExpA;
			this.EdExpB = d.EdExpB;
			this.EdRecurrentExpA = d.EdRecurrentExpA;
			this.EdRecurrentExpB = d.EdRecurrentExpB;			
			this.enrolment = d.Enrolment;
			this.enrolmentNation = d.EnrolmentNation;
			this._districtCode = d.DistrictCode;
			this._options = () => o;
		}
		_districtCode: string;
		_options: any;
		enrolment: number;
		enrolmentNation: number;
		EdExpA: number;
		EdExpB: number;
		EdRecurrentExpA: number;
		EdRecurrentExpB: number;
		// thi is the enrolment to use when reporting enrolments - only returns for national if we have filterd
		// on national
		get Enrolment() {
			return (this._options().selectedDistrict == 'NATIONAL' ? this.enrolmentNation : this.enrolment);
		}
		// this is the enrolment to use when calculating exp per head
		// it is different for the NATIONAL expenditure point becuase it is across all enrolments
		// , not just the district ones
		get EnrolmentApplies() {
			return (this._districtCode == 'NATIONAL' ? this.enrolmentNation : this.enrolment);
		}

		get EdExpAPerHead() {
			return this.EdExpA / this.EnrolmentApplies;
		}
		get EdExpBPerHead() {
			return this.EdExpB / this.EnrolmentApplies;
		}
	}

	class nationAccumulator {
		constructor(d) {
			this.EdExpA = d.EdExpA;
			this.EdExpB = d.EdExpB;
			this.EdRecurrentExpA = d.EdRecurrentExpA;
			this.EdRecurrentExpB = d.EdRecurrentExpB;		
			this.GovtExpA = d.GovtExpA;
			this.GovtExpB = d.GovtExpB;

			this.GNP = d.GNPLocal;
			this.enrolment = d.Enrolment;
			this.enrolmentNation = d.EnrolmentNation;
			this._districtCode = d.DistrictCode;
		}
		_districtCode: string;
		_options: any;
		enrolment: number;
		enrolmentNation: number;
		EdExpA: number;
		EdExpB: number;
		EdRecurrentExpA: number;
		EdRecurrentExpB: number;
		GovtExpA: number;
		GovtExpB: number;
		GNP: number;
		// thi is the enrolment to use when reporting enrolments - only returns for national if we have filterd
		// on national
		get EdExpAGovtPerc() {
			return 100 * this.EdExpA / this.GovtExpA;
		}
		get EdExpBGovtPerc() {
			return 100 * this.EdExpB / this.GovtExpA;
		}

		get EdExpAGNPPerc() {
			return 100 * this.EdExpA / this.GNP;
		}
		get EdExpBGNPPerc() {
			return 100 * this.EdExpB / this.GNP;
		}


		get Enrolment() {
			return (this._options.selectedDistrict == 'NATIONAL' ? this.enrolmentNation : this.enrolment);
		}

		// this is the enrolment to use when calculating exp per head
		// it is different for the NATIONAL expenditure point becuase it is across all enrolments
		// , not just the district ones
		get EnrolmentApplies() {
			return (this._districtCode == 'NATIONAL' ? this.enrolmentNation : this.enrolment);
		}

		get EdExpAPerHead() {
			return this.EdExpA / this.EnrolmentApplies;
		}
		get EdExpBPerHead() {
			return this.EdExpB / this.EnrolmentApplies;
		}
	}

	export class FinDataDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

		// additional data

		private tables: any[];
		// override with strongly typed crossfilter
		public xf: CrossFilter.CrossFilter<FinData.IxfEdExpenditure>;
		// use a completely separate cross tab for the nation results. We won't slice this by district
		public xfNation: CrossFilter.CrossFilter<FinData.IxfEdExpenditure>;

		// dimensions and groups based on education expenditure

		public dimSurveyYear: CrossFilter.Dimension<FinData.IxfEdExpenditure, number>;
		public dimDistrictCode: CrossFilter.Dimension<FinData.IxfEdExpenditure, string>;
		public dimSectorCode: CrossFilter.Dimension<FinData.IxfEdExpenditure, string>;

		public grpDistrictCode: CrossFilter.Group<FinData.IxfEdExpenditure, string, any>;

		public xtabYearSector: CrossFilter.Group<FinData.IxfEdExpenditure, number, any>;
		public xtabDistrictSector: CrossFilter.Group<FinData.IxfEdExpenditure, string, any>;
		public xtabYearDistrict: CrossFilter.Group<FinData.IxfEdExpenditure, string, any>;
		public xtabSector: CrossFilter.Group<FinData.IxfEdExpenditure, string, any>;

		public dimSurveyYearNation: CrossFilter.Dimension<FinData.IxfEdExpenditure, number>;
		public xtabYearNation: CrossFilter.Group<FinData.IxfEdExpenditure, string, any>;


		public allSectors: string[];
	

		private layers: any[];


		public $onInit() {
			this.xf = crossfilter(this.tables[0]);
			this.xfNation = crossfilter(this.tables[0]);
			this.createDimensions();
			this.dashboard = this;
		}

		public onOptionChange(data:IOptionChangeData, sender) {
			if (data.selectedYear) {
				this.dimSurveyYear.filter(this.options.selectedYear);
			}
			if (data.selectedDistrict) {

				this.dimDistrictCode.filter(this.options.selectedDistrict);
			}
			// always call the super version so that the clients get to know about the option change as well
			super.onOptionChange(data, sender);
		}

		private elSelector = ({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel)["L"];
		private classLevelSelector = this.xFilter.getPropAccessor("ClassLevel")

		public createDimensions() {
			// create the dimensions

			// this.dimAuthorityGovtCode = this.xf.dimension(d => d.AuthorityGovtCode || "--");

			// dimension returned on Expenditure data
			this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
			this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode || "--");
			this.dimSectorCode = this.xf.dimension(d => d.SectorCode || "--");

			this.dimSurveyYearNation = this.xfNation.dimension(d => d.SurveyYear);

			//this.dimCostCentreCode = this.xf.dimension(d => d.CostCentreCode);

			// accumulator function to accumulate ed spending data 
			let edExpValueAccessor = d => {
				return new edExpAccumulator(d, this.options);
			}

			// accumulator function to accumulate ed spending data 
			let nationValueAccessor = d => {
				return new nationAccumulator(d);
			}


			// grouping by year, summint to sector
			// for year-bysector chart
			this.xtabYearSector = this.xFilter.xReduce(this.dimSurveyYear, "SectorCode", edExpValueAccessor);
			this.xtabYearDistrict = this.xFilter.xReduce(this.dimSurveyYear, "DistrictCode", edExpValueAccessor);


			this.xtabDistrictSector = this.xFilter.xReduce(this.dimDistrictCode, "SectorCode", edExpValueAccessor);
			this.xtabSector = this.xFilter.xReduceTotal(this.dimDistrictCode, "SectorCode", edExpValueAccessor);

			this.xtabYearNation = this.xFilter.xReduce(this.dimSurveyYearNation, "DistrictCode", nationValueAccessor);

			this.allSectors = this.xf.dimension(d => d.SectorCode || "--").group().all().map(r => r.key);		// omits null
			console.log("FinDataDashboard all sectors", this.allSectors);
			// or _(this.tables[0]).map(r=>r.SectorCode).uniq().value() which will include null

			// function to shape the value on each row of these reductions

			let sectorReducer = d => {
				let isAccredited = ['Level 2', 'Level 3', 'Level 4'].indexOf(d.InspectionResult) >= 0;
				return {
					Num: d.Num, NumThisYear: d.NumThisYear
					, Accredited: (isAccredited ? d.Num : null)
					, AccreditedThisYear: (isAccredited ? d.NumThisYear : null)
				};
			};

			// set the surveyYear to be the most recent in the warehouse data, rather than the most recent surveyYear in the lookups
			// we dont want to get empty data on the initial display if the warehouse is not yet generated for the current year
			if (!this.options.selectedYear) {
				this.options.selectedYear = this.dimSurveyYear.top(1)[0].SurveyYear;
			}

			//this.xtabDistrictCode = this.xFilter.xReduce(this.dimDistrictCode, "InspectionResult", levelReducer);
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			tables: "<"	,			// array of data tables
			options: "<",			// get the options
			title: "<"
		};
		public controller = FinDataDashboard;
		public controllerAs = "vm"
		public templateUrl: string = "findata/dashboard";
	}

	angular
		.module("pineapples")
		.component("finDataDashboard", new Component());

	// component options for dashboard children
	export class FinDataDashboardComponent extends DashboardChildComponent implements ng.IComponentOptions {
		public get feature() {
			return "findata";
		}

		constructor(public controller: any, templateUrl) {
			super(controller, templateUrl);
		}
	}
}


