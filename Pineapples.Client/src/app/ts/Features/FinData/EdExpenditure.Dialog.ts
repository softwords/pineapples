﻿namespace Pineapples.FinData {

  export interface IEdExpenditureDialogLocals {
		model: EdExpenditure;
		getNew: () => ng.IPromise<EdExpenditure>;				
  }

  class DialogController extends Sw.Component.ComponentEditController {

		public model: EdExpenditure;
		/**
		 * generate a new empty EdExpenditure
		 * This function supplies the parent (finData) Id
		 * and is supplied by the parent list
		 */
		public getNew: () => ng.IPromise<EdExpenditure>;	

		static $inject = ["$mdDialog", "ApiUi"];
		constructor(public mdDialog: ng.material.IDialogService, apiUi: any) {
      super(apiUi);
      if (!this.model._id()) {
        this.isNew = true;
        this.isEditing = true;
      }
    }

    public closeDialog() {
      this.mdDialog.cancel();
    }

    public onModelUpdated(newData) {
      if (newData.plain) {
        // its a restangular object
        //        angular.extend(this.source, newData.plain());
        angular.extend(this.model, newData.plain());
      } else {
        //      angular.extend(this.source, newData);
        angular.extend(this.model, newData);
      }
		}

  }

  export class EdExpenditureDialog {

    public static options(locals: IEdExpenditureDialogLocals): ng.material.IDialogOptions {
      let o = {
        clickOutsideToClose: true,
        templateUrl: "findata/edexpdialog",
        controller: DialogController,
        controllerAs: "vm",
        bindToController: true,
        locals: locals
      }
      return o;
    }

  }
}