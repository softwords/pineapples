﻿namespace Pineapples.FinData {

  // this needs to be maintained if more resultsets are added to the stored proc
	export class FinData extends Pineapples.Api.Editable implements Sw.Api.IEditable{

    // constructor just expects an object that is the school data, not the related data
    // this can be called from new school
    constructor(data) {
      super();
      angular.extend(this, data);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
			return new FinData(resultSet)
    }

    
    public _name() {
      return `${this.fnmYear} ${this.fnmDistrict || "Central"}` ;
    }
    public _type() {
      return "findata";
    }
    public _id() {
			return this.fnmID;
		}

		public fnmID: number = null;
		public fnmYear: number;
		public fnmDistrict: string;
		public fnmGNPCurrency: string;
		public fnmGNPXchange: number;

		public fnmExpTotA: number;		// total expenditure actual
		public fnmExpTotB: number;		// total expenditure budget

		public educationSpending: EdExpenditure[];

		public get educationTotals() {
			return _.reduce(this.educationSpending, (tot, edx) => {
				tot.totalActual += edx.xedTotA;
				tot.totalBudget += edx.xedTotB;
				tot.currentActual += edx.xedCurrentA;
				tot.currentBudget += edx.xedCurrentB;
				return tot;
			}, { totalActual: 0, totalBudget: 0 , currentActual: 0, currentBudget: 0})
			
		}

		public get educationPercs() {
			return {
				actual: (this.fnmExpTotA)?
					Math.floor((1000 * this.educationTotals.totalActual) / this.fnmExpTotA) / 10
					:0 , 
				budget: (this.fnmExpTotB) ?
					Math.floor((1000 * this.educationTotals.totalBudget) / this.fnmExpTotB) / 10 
					:0
			};
		}
  }
}
