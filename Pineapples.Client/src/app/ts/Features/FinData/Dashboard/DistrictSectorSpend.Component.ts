﻿namespace Pineapples.Dashboards.FinData {

	class Controller extends EdSpendingChildBase implements IDashboardChild {
		public onViewOptionChange() { };
	}

	angular
		.module("pineapples")
		.component("findataDistrictSectorSpend", new FinDataDashboardComponent(Controller, "districtSectorSpend"));
}