﻿namespace Pineapples.Dashboards.FinData {

	class Controller extends EdSpendingChildBase implements IDashboardChild {

		// overrideable
		public defaultTitle = "Spending By Sector";

		public rowColData: Sw.Charts.RowColumnCollection;

		public onViewOptionChange() {
			switch (this.selectedViewOption) {
				case 4:
				case 5:
					// these are the ratios of Ed spending per head
					// it doesn't make sense to add the values by sector
					this.format = "vc";
					break;
				default:
					this.format = "v";
			}
			this.makeChartData();
		}

		public format = "v";

		public group: CrossFilter.Group<any, any, any>;
		public chartData;
		public $onChanges(changes) {
			this.makeChartData();
		}

		public makeChartData() {
			let data = this.group.all();
			let rc = [];
			data.forEach(kv => {
				this.dashboard.allSectors.forEach((sector, i) => {
					if (kv.value[sector] && kv.value[sector][this.selectedViewField]) {
						let n = kv.value[sector][this.selectedViewField];
						let p = Math.pow(10, this.selectedViewPrecision);
						n = Math.round(n * p) / p;
						rc.push({

							R: kv.key,			// the year
							RC: kv.key,
							RName: "Year",
							C: this.lookups.byCode("educationSectors", sector, "N"),
							CC: sector,
							CName: "Sector",
							Num: n
						});
					}
				});
			})
			this.rowColData = rc;
		}
		public clickHandler(event) {
			// when clicking on any sectin of this chart, set the selectedYear to the 'R' value of the datum
			let d = event.target.__data__;
			this.options.selectedYear = d.R;
			console.log(d);
		}

		// deselect the bars that are not the selectedYear
		public classHandler(source) {
			if (this.options.selectedYear && !this.options.isSelectedYear(source.R)) {
				return "pale";  // see dashboard.css for use of deseleced or fade
			}
			return "";
		}

		public onOptionChange(optionchange) {
			// if we have only changed the selected year, we don;t need to redraw
			this.makeChartData();
		}
		public onChildSelected() {
			if (!this.isSelected()) {
				this.drillfltr = null;
				this._selectedcell = "";
			}
		}


	}

  angular
		.module("pineapples")
		.component("yearSectorSpendChart", new FinDataDashboardComponent(Controller, "yearSectorSpendChart"))
}
