﻿namespace Pineapples.Dashboards.FinData {

	class Controller extends EdSpendingChildBase implements IDashboardChild {

		// overrideable
		public defaultTitle = "Spending By District";

		public group;
		public chartData;
		public rowColData: Sw.Charts.RowColumnCollection;

		public $onChanges(changes) {
			this.makeChartData();
		}

		public onViewOptionChange() {
			this.makeChartData();
		}

		public makeChartData() {
			let data = this.group.all();
			let rc = [];
			data.forEach(kv => {
				this.lookups.cache["expenditurePoints"].forEach((exppt, i) => {
					if (kv.value[exppt.C]) {
						let n = kv.value[exppt.C][this.selectedViewField];
						if (n) {
							let p = Math.pow(10, this.selectedViewPrecision);
							n = Math.round(n * p) / p;
						}
						rc.push({
							R: kv.key,			// the year
							RC: kv.key,
							RName: "Year",
							C: exppt.N,
							CC: exppt.C,
							CName: "Expenditure Point",
							Num: n
						});
					};
				});
			})
			this.rowColData = rc;
		}

		// overridables
		
		public XAccessor = (datum, index, dataset) => {
			let d = datum.value[dataset.metadata()].Actual_Total;
			return d;
		};
		public YAccessor = (datum, index, dataset) => {
			// this is just the year
			return datum.key;
		}

		private getDataValueAccessor(datavalue) {
			return (datum, index, dataset) => 
				datum.value[dataset.metadata()[datavalue]];
		}

		private getYear(datum, index, dataset) {
			return datum.key
		}

		public clickHandler(event) {
			// when clicking on any sectin of this chart, set the selectedYear to the 'R' value of the datum
			let d = event.target.__data__;
			this.options.selectedYear = d.R;
			console.log(d);
		}

		// deselect the bars that are not the selectedYear
		public classHandler(source) {
			if (this.options.selectedYear && !this.options.isSelectedYear(source.R)) {
				return "pale";  // see dashboard.css for use of deseleced or fade
			}
			return "";
		}

		public onOptionChange(optionchange) {
			// something has been changed in the dashboard options
			// but only refresh the chart if something other than selectedYear got changed
			if (_(optionchange).keys().without("selectedYear").value().length) {
				this.makeChartData();
			};
		}

		public onChildSelected() {
			if (!this.isSelected()) {
				this.drillfltr = null;
				this._selectedcell = "";
			}
		}
	}

  angular
		.module("pineapples")
		.component("yearDistrictSpendChart", new FinDataDashboardComponent(Controller, "yearDistrictSpendChart"))
}
