﻿namespace Pineapples.Dashboards.FinData {

	class Controller extends EdSpendingChildBase implements IDashboardChild {
		public onViewOptionChange() { };
		private _spOptions = {
			type: 'pie',
			offset: 90,
			sliceColors: ['rgba(0,0,255,1)', 'rgba(255, 255, 255, 0)'],
			composite: false,
			disableTooltips: true
		};

		public sparklineOptions(overrides) {
			if (overrides) {
				angular.merge(this._spOptions, overrides)
			}
			return this._spOptions;
		}
	}

	angular
		.module("pineapples")
		.component("findataYearNation", new FinDataDashboardComponent(Controller, "yearNation"));
}