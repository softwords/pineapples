﻿namespace Pineapples.Dashboards.FinData {

	export abstract class EdSpendingChildBase extends DashboardChild implements IDashboardChild {

		// bindings
		public options: finDataOptions;
		public dashboard;

		static $inject = ["$timeout", "$mdDialog", "$state", "Lookups", "reportManager", "$http"];
		constructor(
			protected timeout: ng.ITimeoutService
			, mdDialog: ng.material.IDialogService
			, protected $state: ng.ui.IStateService
			, public lookups: Sw.Lookups.LookupService
			, rmgr: Pineapples.Reporting.IReportManagerService
			, public http: ng.IHttpService) {
			super(timeout, mdDialog, $state, lookups, rmgr, http);
		}
		
		//--------------------------------------------------------------------------------------//
		// View Option Management
		//
		// View options for EdExpenditure data - charts may choose which of these values to chart
		// note they cannot be added together
		//--------------------------------------------------------------------------------------//
		public get viewOptions() {
			return [
				"Actual Expenditure",
				"Budgeted Expenditure",
				"Actual Recurrent Expenditure",
				"Budgeted Recurrent Expenditure",
				"Actual Expenditure per Head",
				"Budget Expenditure per Head",
				"Enrolment"
			]
		}

		// field name and precision
		protected get viewOptionFields() {
			return [
				["EdExpA",0],
				["EdExpB", 0],
				["EdRecurrentExpA", 0],
				["EdRecurrentExpB", 0],
				["EdExpAPerHead", 2],
				["EdExpBPerHead", 2],
				["Enrolment", 0]
			]
		}
		protected get selectedViewField() {
			return this.viewOptionFields[this.selectedViewOption][0];
		}
		protected get selectedViewPrecision():number {
			return <number>this.viewOptionFields[this.selectedViewOption][1];
		}


		private _selectedViewOption = 0;
		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.onViewOptionChange();
			}
		}

		//
		abstract onViewOptionChange();

		//--------------------------------------------------------------------------------------//
		// Drill down support
		
		// common base to support drill down into education expenditure data
		// this will show the (re)current and total expenditure
		//--------------------------------------------------------------------------------------//
		public drillData: any[]; // lowest level data for drill down

		public onOptionChange(optionchange) {
			this.redrill();
		}
		public onChildSelected() {
			if (!this.isSelected()) {
				this.drillfltr = null;
				this._selectedcell = "";
			}
		}
		protected drillfltr;
		protected redrill() {
			if (this.drillfltr && this.isSelected()) {
				angular.merge(this.drillfltr, {
					District: this.options.selectedDistrict,
					Year: this.options.selectedYear
				});
				this.getDrillData();
			}
		}
		public drilldown(districtCode: string, sectorCode: string, costCentreCode) {
			this.drillfltr = {
				District: this.options.selectedDistrict,
				SurveyYear: this.options.selectedYear,
			};
			this._selectedcell = `${districtCode}|${sectorCode}|${costCentreCode}`;
			this.getDrillData()
				.then(() => {
					this.setSelected();
				});;
		}
		private getDrillData() {
			return this.http.post("api/warehouse/accreditations/filter", this.drillfltr)
				.then(result => {
					this.drillData = <any[]>result.data;
				});
		}

		protected _selectedcell: string;
		protected isSelectedCell(districtCode: string, sectorCode: string, costCentreCode) {
			return this.isSelected() && this._selectedcell == `${districtCode}|${sectorCode}|${costCentreCode}`;
		}
	}
}