﻿module Pineapples.Api {
	const ENTITY = "findata";
	class apiService {
		
		static $inject = ["$q", "$http", "Restangular"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService) {
		}

		public read(id) {
			return this.restangular.one(ENTITY, id).get();
		}

		public new() {
			return this.restangular.one(ENTITY, 0).get();
		}

		// try withut an update method, becuase we'll get that directly from the 
		// there will never be more than a couple of dozen records in here, its not worth filtering or paging them
		// as well, we'll reread an individual from the list if we want to edit it.
		// point being we'll just tread lightly and use http for this method - but to be consistent with restangular
		// return just the data payload on success, not the entire object
		public list() { 
			return this.http.get("api/findata")
				.then(response => Pineapples.FinData.FinData.create(response.data));
		}

		public filterPaged(fltr) {
			return this.http.post("api/findata/collection/filter", fltr)
				.then(response => (response.data));
		}
	}
	
	angular.module("pineapplesAPI")
		.service("finDataAPI", apiService);

}
