﻿namespace Pineapples.FinData {

	angular
		.module("pineapples")
		.component("finDataRenderFrame", new Sw.Component.RenderFrameComponent("findata"));
}