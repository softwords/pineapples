﻿namespace Pineapples.FinData {

	class Controller extends Sw.Component.ListMonitor {
		public edexps: any[];
		public findata: Pineapples.FinData.FinData;
		public parentkeyname = "fnmID";

		public isWaiting: boolean;

		static $inject = ["$scope", "$mdDialog", "$location", "$state", "Restangular", "ApiUi", "$q", "Lookups"];
		constructor(
			private scope: Sw.IScopeEx
			, public mdDialog: ng.material.IDialogService
			, private $location: ng.ILocationService
			, public $state: ng.ui.IStateService
			, public restangular: restangular.IService
			, private apiUi: Sw.Api.IApiUi
			, private q: ng.IQService
			, public lookups: Sw.Lookups.LookupService) {
			super();
			this.monitor(scope, "edexpenditures", "xedID");
			this.multiSelect = false;
		}

		public monitoredList() {
			return this.edexps;
		}
		public isListMember(newData) {
			return (this.findata && this.findata._id() == newData[this.parentkeyname]);
		}

		public $onChanges(changes) {
			if (changes.qualifications) {
				//        console.log(this.surveys);
			}
		}

		/**
		 * Show a single ed exp record in a modal, for editing
		 * @param edexp
		 */
		public showEdExp(edexp) {
			let locals: IEdExpenditureDialogLocals = {
				model: this.clone(edexp),
				getNew: this.getNew
			}
			this.mdDialog.show(
				EdExpenditureDialog.options(locals)
			);
		}
		private deleteConfirm(q) {
			return this.mdDialog.confirm()
				.title("Delete Item")
				.content("Delete this expenditure item?")
				.clickOutsideToClose(true)
				.ok("Delete")
				.cancel("Cancel");
		}
		public delete(q) {
			this.mdDialog.show(this.deleteConfirm(q))
				.then(() => {
					this.isWaiting = true;
					// clone is a bit of overkill here
					// but it does allow restangular to now handle the deletion
					let tq = this.clone(q);
					tq.remove()
						.then(() => {
							this.isWaiting = false;
						})
						.catch((error) => {
							this.isWaiting = false;
							this.apiUi.showErrorResponse(q, error);
						})
				});
		}

		// record a new qualification for this teacher
		public new() {
			// set up the defaults for the new ed expenditure record
			let model = {
				fnmID: this.findata._id()
			}
			this.showEdExp(model);
		}

		/**
		 * Passed to the ed exp dialog to generate new when needed
		 * its a function returning a promise
		 */
		private getNew = () => {
			let model = {
				fnmID: this.findata._id()
			}
			return this.q.resolve(this.clone(model));
		}

		// see the comments in TeacherQualificationList.Component
		// on this appraoch
		private clone(edx) : EdExpenditure {
			let clone: EdExpenditure;
			// drop any reqstangular stuff
			let modeldata = (edx.plain ? edx.plain() : edx);
			// create the new teacher object
			clone = EdExpenditure.create(modeldata);
			//restangularize it
			this.restangular.restangularizeElement(null, clone, "edexpenditures");
			return clone;
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			edexps: "<"
			, findata: "<"
		};
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "findata/edexplist";
	}

	// popup dialog for audit log
	class DialogController extends Sw.Component.MdDialogController {
		static $inject = ["$mdDialog", "edexp"];
		constructor(mdDialog: ng.material.IDialogService, public payslip) {
			super(mdDialog);
		}
	}

	angular
		.module("pineapples")
		.component("componentEdExpList", new Component());
}
