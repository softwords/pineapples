﻿namespace Pineapples.Teachers {

	angular
		.module("pineapples")
		.component("teacherdeduperRenderFrame", new Sw.Component.RenderFrameComponent("teacherdeduper"));
}