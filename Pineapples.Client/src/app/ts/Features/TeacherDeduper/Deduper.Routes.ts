﻿// Teachers Routes
namespace Pineappples.Teachers {

	let routes = function ($stateProvider) {
		var featurename = 'TeacherDeduper';
		var filtername = 'TeacherDeduperFilter';
		var templatepath = "teacherdeduper";
		var tableOptions = "teacherFieldOptions";
		var url = "teacherdeduper";
		var usersettings = null;
		//var mapview = 'TeacherMapView';

		// root state for 'teachers' feature
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath, url, usersettings, tableOptions);

		// default 'api' in this feature is teachersAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "teacherDeduperAPI";
		state.data = state.data || {};
		state.data["frameTitle"] = "Teacher Deduplicate";
		state.data["permissions"] = {
			only: 'TeacherOps'
		};

		let basestate = "site.teacherdeduper";
		$stateProvider.state(basestate, state);

		// takes a list state
		// doesn't need monitor?
		state = Sw.Utils.RouteHelper.frameListState("teacher", "tID");
		let statename = `${basestate}.list`;
		state.url = `^/${url}/list`;

		$stateProvider.state(statename, state);

		state = {
			url: '^/teacherdeduper/reload',
			onEnter: ["$state", "$templateCache", function ($state: ng.ui.IStateService, $templateCache) {
				$templateCache.remove("teacher/item");
				$templateCache.remove("teacher/searcher");
				$state.go($state.current);
			}]
		};
		statename = "site.teacherdeduper.reload";
		$stateProvider.state(statename, state);

		// poup view of teacher is cloned from teachers.list.item
		state = {
			url: "^/teacherdeduper/list/teacher/{id}",
			views: {
				"actionpane@site.teacherdeduper": "componentTeacher"
			}
		};
		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.teachers.list.item", state);
		$stateProvider.state("site.teacherdeduper.list.teacher", state);

		// poup view of comparitve surveys
		state = {
			url: "^/teacherdeduper/list/surveys/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.teacherdeduper": "deduperSurveys"
			},
			resolve: {
				surveys: ["teacherDeduperAPI", "$stateParams", (api, $stateParams: ng.ui.IStateParamsService) => {
					let ids = (<string>$stateParams.id).split("_");
					return api.surveys(ids[0], ids[1]).then(
						response => (<any>response.ResultSet)
					);
				}]
			}
		};

		$stateProvider.state("site.teacherdeduper.list.surveys", state);

		// poup window to do the data merge and save
		state = {
			url: "^/teacherdeduper/list/merge/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.teacherdeduper": "deduperMerge"
			},
			resolve: {

				// use the existing interfaces to read
				model: ['teachersAPI', '$stateParams', (api, $stateParams: ng.ui.IStateParamsService) => {
					let ids = (<string>$stateParams.id).split("_");
					return api.read(ids[0]);
				}],
				dup: ['teachersAPI', '$stateParams', (api, $stateParams: ng.ui.IStateParamsService) => {
					let ids = (<string>$stateParams.id).split("_");
					return api.read(ids[1]);
				}]
			}
		};

		$stateProvider.state("site.teacherdeduper.list.merge", state);
	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}