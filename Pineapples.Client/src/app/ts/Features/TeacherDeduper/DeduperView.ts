﻿namespace Pineapples.Teachers {
	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			//{
			//  field: 'tID',
			//  name: 'Teacher ID',
			//  displayName: 'Teacher ID',
			//  editable: false,
			//  pinnedLeft: true,
			//  width: 90,
			//  cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item")
			//},
			{
				field: 'tID',
				name: 'colID',
				displayName: 'ID',
				pinnedLeft: true,
				width: 60,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div><a  ng-click="grid.appScope.vm.action('teacher','tID', row.entity);">{{row.entity.tID}}</a></div>
            <div><a  ng-click="grid.appScope.vm.action('teacher','dupID', row.entity);">{{row.entity.dupID}}</a></div>
           </a></div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'tSurname',
				name: 'colFamilyName',
				displayName: 'Family Name',
				pinnedLeft: true,
				width: 120,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.tSurname}}</div>
            <div>{{row.entity.dupSurname}}</div>
           </a></div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'tGiven',
				name: 'colGiven',
				displayName: 'Given Name',
				pinnedLeft: true,
				width: 120,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.tGiven}}</div>
            <div>{{row.entity.dupGiven}}</div>
           </a></div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				name: 'svyButton',
				displayName: '',
				pinnedLeft: true,
				width: 80,
				cellTemplate:
					`<div  layout="row" layout-align="center center" class="ui-grid-cell-contents gdAction">
							<md-icon ng-click="grid.appScope.vm.action('surveys',['tID', 'dupID'], row.entity);">
								assignment_turned_in
								<md-tooltip>Compare surveys</md-tooltip>
							</md-icon>
							<md-icon ng-click="grid.appScope.vm.action('merge',['tID', 'dupID'], row.entity);">
								call_merge
								<md-tooltip>Merge these two records</md-tooltip>
							</md-button>

           </div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'tSex',
				name: 'colGender',
				displayName: 'Gender',
				pinnedLeft: true,
				width: 60,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.tSex}}</div>
            <div>{{row.entity.dupSex}}</div>
           </a></div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'tDoB',
				name: 'colDoB',
				displayName: 'DoB',
				pinnedLeft: true,
				width: 100,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div >{{row.entity.tDoB|date:'d-MMM-yyyy'}}</div>
            <div>{{row.entity.dupDoB|date:'d-MMM-yyyy'}}</div>
           </a></div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]

			}
		]
	};

	let modes = [
		{
			key: "Dups",
			gridOptions: {
				rowHeight: 60,
				columnDefs: [
					{
						field: 'tProvident',
						name: 'colProvident',
						displayName: 'Teacher Provident No',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 160,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.tProvident}}</div>
            <div>{{row.entity.dupProvident}}</div>
           </a></div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					},
					{
						field: 'tRegister',
						name: 'colRegister',
						displayName: 'Teacher Registration No',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 160,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.tRegister}}</div>
            <div>{{row.entity.dupRegister}}</div>
           </a></div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					},
					{
						field: 'tPayroll',
						name: 'colPayroll',
						displayName: 'Teacher Payroll No',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 160,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.tPayroll}}</div>
            <div>{{row.entity.dupPayroll}}</div>
           </a></div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					}
				]

			}       // gridoptions
		}           //mode
	];              // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};


	angular
		.module('pineapples')
		.run(['TeacherDeduperFilter', pushModes]);
}
