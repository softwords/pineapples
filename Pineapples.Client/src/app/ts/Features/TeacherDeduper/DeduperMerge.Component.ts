﻿namespace Pineapples.Teachers {

	class Controller extends Sw.Component.ComponentEditController {
		public model: Teacher;
		public primary: Teacher;
		public dup: Teacher;

		// holds booleans showing which propertiese are copied from dup
		public copy: {}

		static $inject = ["ApiUi","teacherDeduperAPI", "$mdDialog","$state","Lookups"];
		constructor(apiUi: Sw.Api.IApiUi, public api: Pineapples.Api.teacherDeduperApi
			, public mdDialog: ng.material.IDialogService
			, public $state: ng.ui.IStateService
			, public lookups: Pineapples.Lookups.LookupService) {
			super(apiUi);
		}

		public $onInit() {
			// copy the primary into the working model
			this.primary = angular.copy( (<any>this.model).plain());
			this.fillAllBlanks();
			this.setEditing(true);
		}

		public allProps = {
			tNamePrefix: "Title",
			tGiven: "Given Name",
			tMiddleNames: "Middle Names",
			tSurname: "Family Name",
			tNameSuffix: "Name Suffix",
			tSex: "Gender",
			tDOB: "DoB",
			tPayroll: "EmployNo",
			tProvident: "Provident Fund No",
			tDatePSAppointed: "Date PS Appointed",
			tDatePSClosed: "Date PS Closed",

			tRegister: "Registration No",
			tDateRegister: "Date Registered",
			tDateRegisterEnd: "Registration End"
		}


		public nameProps = {
			tNamePrefix: "Title",
			tGiven: "Given Name",
			tMiddleNames: "Middle Names",
			tSurname: "Family Name",
			tNameSuffix: "Name Suffix"
		}

		public tnumberProps = {
			tPayroll: "EmployNo",
			tProvident: "ProvidentNo"
		}

		public fillAllBlanks() {
			Object.keys(this.allProps).forEach(propname => { this.fillBlank(propname); });
		}

		public fillBlank(propname) {
			if (this.primary[propname] == null && this.dup[propname]) {
				this.copy[propname] = true;
				this.primary[propname] = this.dup[propname];
			}
		}

		public Merge() {
			// save the model in the usual way (restangular) 
			// then run the merge dedup.
			this.save()
		}

		public onModelUpdated(newData) {
			// we have saved so let's dedup
			this.api.merge(this.primary._id(), this.dup._id())
				.then(response => {
					var confirm = this.mdDialog.confirm()
						.title('Merge Complete')
						.textContent('Display the combined record?')
						.ariaLabel('Merge')
						.ok('OK')
						.cancel('Cancel');

					this.mdDialog.show(confirm).then( () => {
						this.$state.go("site.teacherdeduper.list.teacher", { id: this.model._id() });
					});
				},
				// error callback
					errorResponse => {
						console.error(errorResponse);
					})
		}

		// put a confirm dialog in here so we know what is going to happen
		public save() {
				// Appending dialog to document.body to cover sidenav in docs app
				var confirm = this.mdDialog.confirm()
					.title('Merge Teacher Records')
					.textContent('This action will save your changes and merge the teacher records.')
					.ariaLabel('Merge')
					.ok('Continue to Merge')
					.cancel('Cancel');

				this.mdDialog.show(confirm).then(() => {
					super.save();
				});
		}

	}
	class Component implements ng.IComponentOptions {
		public bindings = {
			model: "<",
			dup: "<"
		};
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "teacherdeduper/merge";
	}

	angular
		.module("pineapples")
		.component("deduperMerge", new Component());
}