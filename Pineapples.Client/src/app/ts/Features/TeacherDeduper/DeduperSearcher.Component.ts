﻿namespace Pineapples.Teachers {


	class Controller extends Sw.Components.FindConfigOperations {

		public cycleOption(optionName) {
			switch (this.params[optionName]) {
				case 1:
					this.params[optionName] = 0
					break;
				case 0:
					this.params[optionName] = null;		// <> = null
					break;
				default:
					this.params[optionName] = 1
			}
		}

		public buttonClass(matchOption) {
			switch (matchOption) {
				case 1:
					return "md-accent";
				case 0:
					return "md-warn";
				default:
					return "md-primary";
			}
		}

		public buttonTooltip(matchOption) {
			switch (matchOption) {
				case 1:
					return `Matching`;
				case 0:
					return `Different`;
				default:
					return `Ignore`;
			}
		}

		public showOption(param) {
			switch (param) {
				case 0:
					return "<>";
				case 1:
					return "=";
			}
			return null;
		}

	}

  angular
    .module("pineapples")
		.component("teacherdeduperSearcherComponent"
			, new Sw.Components.FindConfigComponent("teacherdeduper/SearcherComponent", Controller));
}