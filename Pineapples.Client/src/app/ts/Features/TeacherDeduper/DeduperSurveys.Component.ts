﻿namespace Pineapples.Teachers {

	class Controller {
		public surveys: any[];
		public listdata;

		public $onInit() {
			// get the earliest and latest year represented
			let minYear = _.minBy(this.surveys, r => r.svyYear).svyYear;
			let maxYear = _.maxBy(this.surveys, r => r.svyYear).svyYear;

			let left = _.minBy(this.surveys, r => r.tID).tID;
			let right = _.maxBy(this.surveys, r => r.tID).tID;

			let lst = {};
			for (var j = minYear; j <= maxYear; j++) {
				lst[j] = { left: null, right: null };
			}
			this.surveys.forEach(svy => {
				if (svy.tID == left) {
					lst[svy.svyYear].left = svy;
				} else if (svy.tID == right) {
					lst[svy.svyYear].right = svy;
				}

			});
			this.listdata = lst;
		}


	}
	class Component implements ng.IComponentOptions {
		public bindings = {
			surveys: "<"
		};
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "teacherdeduper/surveys";
	}

	angular
		.module("pineapples")
		.component("deduperSurveys", new Component());
}