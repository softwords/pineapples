﻿namespace Pineapples.CensusWorkbook {

	
	class Controller {
		public year;
		public schoolNo;
		public district;


		public get censusPath() {
			if (this.schoolNo) {
				return `api/censusworkbook/create/${this.year}?schoolNo=${this.schoolNo}`;
			}
			if (this.district) {
				return `api/censusworkbook/create/${this.year}?district=${this.district}`;
			}
			return `api/censusworkbook/create/${this.year}`;
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "censusworkbook/create";
		}
	}
	angular
		.module("pineapples")
		.component("censusWorkbookCreateComponent", new ComponentOptions());
}