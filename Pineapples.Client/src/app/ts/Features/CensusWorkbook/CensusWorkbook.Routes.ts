﻿// Teachers Routes
namespace Pineappples.CensusWorkbook {

	let RouteHelper = Sw.Utils.RouteHelper;

	let routes = function ($stateProvider) {
		//var mapview = 'TeacherMapView';

		// root state for 'teachers' feature
		let state: ng.ui.IState;
		let statename: string;

		// base censusworkbook state
		state = {
			url: "^/censusworkbook",
			data: {
				permissions: {
					only: "SurveyOps"
				}
			},
			resolve: {
				featureTitle: RouteHelper.featureTitle("Census Workbook")
			},
			abstract: true
		};
		$stateProvider.state("site.censusworkbook", state);

		// upload state
		state = {
			url: "^/censusworkbook/upload",
			views: {
				"@": {
					component: "censusWorkbookUploadComponent"
				}
			},
			resolve: {
				pageTitle: RouteHelper.simpleTitle("Upload")
			}
		};
		$stateProvider.state("site.censusworkbook.upload", state);



		// upload state
		state = {
			url: "^/censusworkbook/create",
			views: {
				"@": {
					component: "censusWorkbookCreateComponent"
				}
			},
			resolve: {
				pageTitle: RouteHelper.simpleTitle("Rollover")
			}
		};
		$stateProvider.state("site.censusworkbook.create", state);

		// reload state for testing
		state = {
			url: '^/censusworkbook/reload',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.remove("censusworkbook/upload");
				$state.go("site.censusworkbook.upload");
			}]
		};
		statename = "site.censusworkbook.reload";
		$stateProvider.state(statename, state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}