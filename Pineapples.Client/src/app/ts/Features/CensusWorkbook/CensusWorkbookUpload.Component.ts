﻿namespace Pineapples.CensusWorkbook {

	const HUB_NAME = "workbookprocess";

	interface ICensusWorkbookProcessClient {
		connectFile(fileId: string);
		disconnectFile(fileId: string);
	}
	class censusWorkbookProgress {
		public schools: string;
		public students: string;
		public staff: string;
		public wash: string;
	}

	interface IUploadResponse {
		/**
		 * districrt code from the workbook settings
		 */
		district: string;
		/**
		 * file id assigned to the workbook in the file db
		 */
		id: string;
		/**
		 * Year of data, formatted in local style ( e.g. SY2019-2020)
		 */
		schoolYear: string;
		schools: number;
		staff: number;
		students: number;
		washInfo: number;

		validations: any[];
		staffValidations: any[];
		studentValidations: any[];
		washValidations: any[];

		studentWarnings: any[];
	}
	/**
	 * the response on upload identifies the data, and also validates that school nos are OK
	 * across each sheet. Bad or missing schoool numbers are returned in the vaidations field
	 */
	class censusWorkbookUploadResponse implements IUploadResponse {

		constructor(responseData: IUploadResponse) {
			angular.extend(this, responseData);
		}

		district: string;
		id: string;
		schoolYear: string;
		schools: number;
		staff: number;
		students: number;
		washInfo: number;

		validations: any[];
		staffValidations: any[];
		studentValidations: any[];
		washValidations: any[];

		studentWarnings: any[];

		public get hasValidations(): boolean {
			return (this.validations && this.validations.length > 0);
		}
		public get hasStaffValidations(): boolean {
			return (this.staffValidations && this.staffValidations.length > 0);
		}
		public get hasStudentValidations(): boolean {
			return (this.studentValidations && this.studentValidations.length > 0);
		}
		public get hasWashValidations(): boolean {
			return (this.washValidations && this.washValidations.length > 0);
		}

		public get hasAnyValidations(): boolean {
			return this.hasValidations || this.hasStaffValidations || this.hasStudentValidations
				|| this.hasWashValidations;
		}
	}

	/**
	 * the first recordset return is the validations array
	 * if this is empty, then the upload will take place and the
	 * pupil table is delivered in the last 4 recordsets
	 * the second recordset is Warnings related to conflicts of National ID number.
	 * These will not prevent the upload if present.
	 * see the stored proc pSurveyOps.censusLoadStudents 
	 */
	interface IProcessResponse {
		validations: any[];
		warnings: any[];
		schoolList: any[];
		levelList: any[];
		ageList: any[];
		enrolmentTableData: any[];
		repeaterTableData: any[];
		psaTableData: any[];
		disabCodes: any[];
		disabTableData: any[];
		trinTableData: any[];
		troutTableData: any[];
		dropoutColumns: any[];
		dropoutTableData: any[];
		staffInfo: any[];
		staffSchoolList: any[];
		tamList: any[];
		tamTableData: any[];

		pCreateUser: string;
		pCreateDateTime: any;
		fileID: string;
		surveyYear: number;







	}

	enum processResponseTables {
		validations,
		warnings,

	}
	class censusWorkbookProcessResponse implements IProcessResponse {

		validations: any[];
		warnings: any[];
		schoolList: any[];
		levelList: any[];
		ageList: any[];
		enrolmentTableData: any[];
		repeaterTableData: any[];
		psaTableData: any[];
		disabCodes: any[];
		disabTableData: any[];
		trinTableData: any[];
		troutTableData: any[];
		dropoutColumns: any[];
		dropoutTableData: any[];
		staffInfo: any[];
		staffSchoolList: any[];
		tamList: any[];
		tamTableData: any[];


		pCreateUser: string;
		pCreateDateTime: any;
		fileID: string;
		surveyYear: number;

		constructor(private responseData: any) {
			angular.extend(this, responseData);
		}
		public get hasValidations(): boolean {
			return (this.validations?.length > 0);
		}

		public get hasWarnings(): boolean {
			return (this.warnings?.length > 0);
		}
		public get hasEnrolmentPupilTable(): boolean {
			return (this.enrolmentTableData?.length >= 6);
		}
		public get enrolmentPupilTable(): Pineapples.PupilTable {
			if (!this.hasEnrolmentPupilTable) {
				return null;
			}
			let base = { tableName: "Pre-schoolattenders" };
			return new Pineapples.PupilTable(base,
				this.schoolList,
				this.levelList,
				this.enrolmentTableData);
		}
		public get hasRepeaterPupilTable(): boolean {
			return (this.repeaterTableData?.length > 0);
		}

		public get repeaterPupilTable(): Pineapples.PupilTable {
			if (!this.hasRepeaterPupilTable) {
				return null;
			}

			let base = { tableName: "Pre-schoolattenders" };
			return new Pineapples.PupilTable(base,
				this.schoolList, // reusing the rows (schools)
				this.levelList, // resuing the columns (levels)
				this.repeaterTableData);
		}

		public get hasPSAPupilTable(): boolean {
			return (this.psaTableData?.length > 0);
		}

		public get psaPupilTable(): Pineapples.PupilTable {
			if (!this.hasPSAPupilTable) {
				return null;
			}

			let base = { tableName: "Pre-schoolattenders" };
			return new Pineapples.PupilTable(base,
				this.schoolList, // reusing the rows (schools)
				this.ageList, // age in the columns
				this.psaTableData);
		}

		public get hasDisabPupilTable(): boolean {
			return (this.disabTableData?.length > 0);
		}

		public get disabPupilTable(): Pineapples.PupilTable {
			if (!this.hasDisabPupilTable) {
				return null;
			}

			let base = { tableName: "Disability" };
			return new Pineapples.PupilTable(base,
				this.schoolList, // reusing the rows (schools)
				this.disabCodes, // columns
				this.psaTableData);
		}
		public get hasTrinPupilTable(): boolean {
			return (this.trinTableData?.length > 0);
		}

		public get trinPupilTable(): Pineapples.PupilTable {
			if (!this.hasTrinPupilTable) {
				return null;
			}

			let base = { tableName: "Transfers In" };
			return new Pineapples.PupilTable(base,
				this.schoolList, // reusing the rows (schools)
				this.levelList, // columns
				this.trinTableData);
		}

		public get hasTroutPupilTable(): boolean {
			return (this.troutTableData?.length > 0);
		}

		public get troutPupilTable(): Pineapples.PupilTable {
			if (!this.hasTroutPupilTable) {
				return null;
			}

			let base = { tableName: "Transfers In" };
			return new Pineapples.PupilTable(base,
				this.schoolList, // reusing the rows (schools)
				this.levelList, // columns
				this.troutTableData);
		}

		public get hasDropoutPupilTable(): boolean {
			return (this.dropoutTableData?.length > 0);
		}

		public get dropoutPupilTable(): Pineapples.PupilTable {
			if (!this.hasDropoutPupilTable) {
				return null;
			}

			let base = { tableName: "Dropouts" };

			// Trim trailing spaces from all entries in dropoutColumns
			// there is a bug that results in a mismatch between the survey summary and the excel pivot table.
			// In particular, the 'Self Dropped ' columns has no data as the returned resultsets all came in with
			// trimmed trailing space.
			let trimmedDropoutColumns = this.dropoutColumns.map(column => ({
				codeCode: column.codeCode ? column.codeCode.trim() : null,
				codeDescription: column.codeDescription ? column.codeDescription.trim() : null
			}));

			return new Pineapples.PupilTable(
				base,
				this.schoolList, // reusing the rows (schools)
				trimmedDropoutColumns, // pass trimmed columns
				this.dropoutTableData
			);
		}
		public get hasTamPupilTable(): boolean {
			return (this.tamTableData?.length > 0);
		}

		public get tamPupilTable(): Pineapples.PupilTable {
			if (!this.hasTamPupilTable) {
				return null;
			}

			let base = { tableName: "Teachers" };
			return new Pineapples.PupilTable(base,
				this.staffSchoolList, // reusing the rows (schools)
				this.tamList, // columns
				this.tamTableData);
		}
		public enrolmentChartData(xfilter: Sw.xFilter.XFilter) {
			let xf = crossfilter(this.enrolmentTableData);
			let dim = xf.dimension((data) => data.row);
			let grp = xfilter.xReduce(dim, "row", ({ M, F }) => ({ M, F }));
			return grp;
		}
		public enrolmentChartData2(xfilter: Sw.xFilter.XFilter) {
			let xf = crossfilter(this.enrolmentTableData);
			let dim = xf.dimension((data) => data.col);
			let grp = xfilter.xReduce(dim, "col", ({ M, F }) => ({ M, F }));
			return grp;
		}
		public tamChartData(xfilter: Sw.xFilter.XFilter) {
			let xf = crossfilter(this.tamTableData);
			let dim = xf.dimension((data) => data.row);
			let grp = xfilter.xReduce(dim, "row", ({ M, F }) => ({ M, F }));
			return grp;
		}
		public tamChartData2(xfilter: Sw.xFilter.XFilter) {
			let xf = crossfilter(this.tamTableData);
			let dim = xf.dimension((data) => data.row);
			let grp = xfilter.xReduce(dim, "col", ({ M, F }) => (M ? M : 0) + (F ? F : 0));
			return grp;
		}
	}

	class Controller extends Pineapples.Documents.DocumentUploader {

		public doc: any;      // the document record representing the photo

		public model: any;      // teacher link, bound from the ui

		public imageHeight: number;

		public document: any;     // this is the document object
		public allowUpload: boolean;

		public docPath: string;

		public isProcessing: boolean;
		public uploadResponseData: censusWorkbookUploadResponse;
		public processResponseData: censusWorkbookProcessResponse;

		public progressData: censusWorkbookProgress;
		public enrolmentSummary: Pineapples.PupilTable;
		public repeaterSummary: Pineapples.PupilTable;
		public psaSummary: Pineapples.PupilTable;
		public dropoutSummary: Pineapples.PupilTable;
		public trinSummary: Pineapples.PupilTable;
		public troutSummary: Pineapples.PupilTable;
		public disabSummary: Pineapples.PupilTable;
		public tamSummary: Pineapples.PupilTable;

		// some chart objects
		public enrolChartData;
		public enrolChartData2;
		public tamChartData;
		public tamChartData2;


		public hub: ICensusWorkbookProcessClient;          // the signalr hub

		static $inject = ["identity", "documentsAPI", "FileUploader", "$mdDialog", "Hub"
			, "$state", "$http", "$timeout", "documentManager", "xFilter"];
		constructor(public identity: Sw.Auth.IIdentity, docApi: any
			, FileUploader: angularFileUpload.UploaderContructor
			, mdDialog: ng.material.IDialogService
			, public Hub: ngSignalr.HubFactory
			, state: ng.ui.IStateService
			, public http: ng.IHttpService
			, private timeout: ng.ITimeoutService
			, renderer: Pineapples.Documents.DocumentManager
			, private XFilter: Sw.xFilter.XFilter) {
			super(identity, docApi, FileUploader, mdDialog, state, renderer);
			this.uploader.url = "api/censusworkbook/upload";

			this.uploader.filters.push({
				name: "openxml", fn: (file) => this.renderer.isXmlExcelOpenXml(file.name),
				msg: "The Census Workbook must be an Excel 2007 Workbook with extension .xslm, or a census workbook in xml format, or an xml workbook contained in a zip file"
			});
			let options: ngSignalr.HubOptions;
			options = {
				listeners: {
					"progress": (fileId: string, state: censusWorkbookProgress) => {
						this.timeout(() => {
							console.log(state);
							this.progressData = state;
						}, 0, true);
					}
				},
				methods: ["connectFile", "disconnectFile"],
				errorHandler: (error: string) => {
				},
				logging: true,
				useSharedConnection: false,
				rootPath: "./signalR",        // needs to be relative to the site path in general
				stateChanged: (state: SignalR.StateChanged) => {
					switch (state.newState) {
						case SignalR.ConnectionState.Connected:
							break;
						case SignalR.ConnectionState.Connecting:
							break;
						case SignalR.ConnectionState.Disconnected:
							this.hub = <any>(new Hub(HUB_NAME, options));
							break;
						case SignalR.ConnectionState.Reconnecting:
							break;
					}
				}
			};
			this.hub = <any>(new Hub(HUB_NAME, options));
		};


		protected onAfterAddingFile(fileItem: angularFileUpload.FileItem) {
			super.onAfterAddingFile(fileItem);
			// if we have started to process a new file, clear all the state
			this.uploadResponseData = null;
			this.progressData = null;
			this.processResponseData = null;
			this.enrolmentSummary = null;
			this.repeaterSummary = null;

			this.psaSummary = null;
			this.dropoutSummary = null;
			this.trinSummary = null;
			this.troutSummary = null;
			this.disabSummary = null;
			this.isProcessing = false; // probably unnecessary, anticipating a missed change of state through an error?

		};

		protected onSuccessItem(fileItem: angularFileUpload.FileItem, response, status, headers) {
			this.uploadResponseData = new censusWorkbookUploadResponse(response);    // note slightly different format - not an array item

			// show the dislog for confirmation to continue;
			this._confirmationDialog(this.uploadResponseData).then(() => {
				// connect to signalR
				let fileId = response.id;
				this.hub.connectFile(fileId);
				this.isProcessing = true;


				this.http.get("api/censusworkbook/process/" + fileId).then((processResponse) => {
					this.hub.disconnectFile(fileId);
					console.log(processResponse);
					this.isProcessing = false;
					this.processResponseData = new censusWorkbookProcessResponse(<any[]>(<any>processResponse.data).ResultSet);
					this.enrolmentSummary = this.processResponseData.enrolmentPupilTable;
					this.repeaterSummary = this.processResponseData.repeaterPupilTable;
					this.psaSummary = this.processResponseData.psaPupilTable;
					this.dropoutSummary = this.processResponseData.dropoutPupilTable;
					this.trinSummary = this.processResponseData.trinPupilTable;
					this.troutSummary = this.processResponseData.troutPupilTable;
					this.disabSummary = this.processResponseData.disabPupilTable;
					this.tamSummary = this.processResponseData.tamPupilTable;

					this.enrolChartData = this.processResponseData.enrolmentChartData(this.XFilter);
					this.enrolChartData2 = this.processResponseData.enrolmentChartData2(this.XFilter);
					this.tamChartData = this.processResponseData.tamChartData(this.XFilter);
					this.tamChartData2 = this.processResponseData.tamChartData2(this.XFilter);



					this.uploader.queue.forEach((item) => {
						item.remove();
					});
				},
					// an error response has come back from the processing
					this.processError
				);
			}, () => {
				// cancel the upload from the confirm dialog
				// you may not be given the choice to continue if there are validation errors
				this.progressData = null;
				this.enrolmentSummary = null;
				this.repeaterSummary = null;
				this.psaSummary = null;
				this.isProcessing = false;
				this.uploader.queue.forEach((item) => {
					item.remove();
				});
				// go back to the server to delete the uploaded file - otherwise they just hang around
				this.http.get("api/censusworkbook/remove/" + response.id);
			});

		};

		protected onCompleteItem(fileItem: angularFileUpload.FileItem, response, status, headers) {
			// default is to remove from queue, override that by doing nothing
			// if there was an 
		}

		protected onBeforeUploadItem(item) {
			super.onBeforeUploadItem(item);
		}

		private _confirmationDialog(info) {
			let options: ng.material.IDialogOptions = {
				locals: { info: info },
				controller: confirmController,
				controllerAs: "vm",
				bindToController: true,
				templateUrl: "censusworkbook/uploadconfirm"

			}
			return this.mdDialog.show(options);
		}
		// life cycyle hooks
		public $onChanges(changes) {
		}

		public $onInit() {
		}

		// promise error handlers
		// this is a callback - so use lambda format to bind to this
		// (otherwise, this is 'Window' )
		private processError = (errorResponse) => {
			//this.hub.disconnectFile(fileId);
			this.isProcessing = false;
			let title = "Error Processing File";
			let msg = errorResponse.data.message;
			let ariaLabel = "Processing error";
			this.mdDialog.show(
				this.mdDialog.alert()
					.clickOutsideToClose(true)
					.title(title)
					.textContent(msg)
					.ariaLabel(ariaLabel)
					.multiple(true)
					.ok('Close')
			);
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "censusworkbook/Upload";
		}
	}
	angular
		.module("pineapples")
		.component("censusWorkbookUploadComponent", new ComponentOptions());

	class confirmController extends Sw.Component.MdDialogController {
		public info: censusWorkbookUploadResponse;
	}
}