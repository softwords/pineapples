﻿namespace Pineapples.Dashboards.Accreditations {

	export class LevelController extends DashboardChild implements IDashboardChild {

		// overrideable
		public defaultTitle = "District Status";

		public get viewOptions() {
			return [
				`Evaluated in ${this.options.selectedYear}`
				, `Cumulative to ${this.options.selectedYear}`
			]
		}

		// reference to the host dashboard is from the 'require' on the componentOptions
		public dashboard;
		public levels = ["Level 1", "Level 2", "Level 3", "Level 4"];
		public accreditations: any[];

		public group;
		public chartData;
		public $onChanges(changes) {
			this.makeChartData();
		}

		public makeChartData() {
			let data = this.group.all();
			this.chartData = [];
			this.levels.forEach((level, i) => {
				this.chartData.push({
					data: data,
					meta: level,
					color: ["#FF0000", "#FFC000", "#92D050", "#00B050"][i]
				});
			});
		}

		// overridables
		public XAccessor = (datum, index, dataset) => {
			let d = datum.value[dataset.metadata()];
			if (this._selectedViewOption == 0) {
				return d ? (dataset.metadata() == "Level 1" ? d.NumThisYear * -1 : d.NumThisYear) : null;
			}
			return d ? (dataset.metadata() == "Level 1" ? d.Num * -1: d.Num):null;
		};
		public YAccessor = (datum, index, dataset) => {
			return this.lookups.byCode("districts", datum.key,"N");
		}
		
		public clickHandler(event) {
			let d = event.target.__data__;
			this.options.toggleSelectedDistrict(d.key);
			console.log("Click");
			console.log(d);
		}

		public classHandler(source) {
			if (this.options.selectedDistrict && !this.options.isSelectedDistrict(source.key)) {
				return "deselected";
			}
			return "";
		}
		public onOptionChange(optionchange) {
			this.makeChartData();
		}
		public onChildSelected() {
			if (!this.isSelected()) {
				this.drillfltr = null;
				this._selectedcell = "";
			}
		}

		private _selectedViewOption = 1;

		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.makeChartData();

			}
		}

		private drillfltr;
		private redrill() {
			if (this.drillfltr && this.isSelected()) {
				angular.merge(this.drillfltr, {
					Authority: this.options.selectedAuthority,
					AuthorityGroup: this.options.selectedAuthorityGovt,
					SchoolType: this.options.selectedSchoolType,
					BestUpTo: this.options.selectedYear
				});
				if (this.drillfltr.InspYear) {
						this.drillfltr.InspYear = this.options.selectedYear;
				}
				this.getDrillData();
			}
		}
		public drilldown(level, districtCode, cumulative = "") {

			this.drillfltr = {
				InspResult: level,
				District: districtCode,
				Authority: this.options.selectedAuthority,
				AuthorityGroup: this.options.selectedAuthorityGovt,
				SchoolType: this.options.selectedSchoolType,
				InspYear: cumulative ? null : this.options.selectedYear,
				BestUpTo: this.options.selectedYear
			};
			this._selectedcell = `${level}|${districtCode}|${cumulative}`;
			this.getDrillData()
				.then(() => {
					this.setSelected();
			});;
		}
		private getDrillData() {
			return this.http.post("api/warehouse/accreditations/filter", this.drillfltr)
				.then(result => {
					this.accreditations = result.data["ResultSet"];
				});

		}

		private _selectedcell: string;
		public isSelectedCell(level: string, districtCode: string, cumulative = "") {
			return this.isSelected() && this._selectedcell == `${level}|${districtCode}|${cumulative}`;
		}
	}

	/*
	 * 
	 **/
	class YearLevelController extends LevelController implements IDashboardChild {
		public defaultTitle = "Accreditation Progress";

		public YAccessor = (datum, index, dataset) => {
			return (datum.key).toString();
		}

		public get viewOptions() {
			return [
				`Evaluated in year`
				, `Cumulative to year`
			]
		}

		/**
		 * Change the year by clicking bar
		 * Note that there should alwatys be a selected year, so don;t toggle, just set
		 * @param event
		 */
		public clickHandler(event) {
			let d = event.target.__data__;
			this.options.selectedYear = d.key;
		}

		// we don;t want to deselect in this chart, because there is always a year selected
		public classHandler = undefined;
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			dashboard: "<",
			group: "<",
			options: "<",			
			selectedChild: "<",
			reportPath: "@"
		};
		public templateUrl: string;
		public controllerAs: string = 'vm';

		constructor(public controller: any, templateUrl) {
			this.templateUrl = "schoolaccreditation/dashboard/component/" + templateUrl;
		}
	}

  angular
		.module("pineapples")
		.component("accreditationStandardChart", new Component(LevelController, "standardChart"))
}
