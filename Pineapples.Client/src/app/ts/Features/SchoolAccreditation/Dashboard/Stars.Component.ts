﻿namespace Pineapples.Dashboards {

	class Controller {
		// allow the star widget to respond to either a numeric (number of stars)
		// or a Level string 

		private _level: any;
		private val: string;
		private className: string;
		public levelName: string;
		public size: string = "";
		public set level(lvl) {
			this._level = lvl;			// save the input value;
			if (lvl == null || lvl == undefined) {
				this.className = null;
				this.levelName = null;
				return;
			}
			if (!isNaN(lvl)) {
				this.val = lvl;
			}
			else {
				let lvls:string = lvl;
				this.val = lvls.substr(lvls.length - 1);
			}
			this.className = "star" + this.val;
			this.levelName = "Level " + this.val;
		};

	}


	class Component implements ng.IComponentOptions {
		public bindings: any = {
			level: "<",
			size: "@"			// strin gvalue is more likely here than a calculation

		};
		public template: string = "<span ng-class='vm.className' class='stars {{vm.size}}' title='{{vm.levelName}}'</span>";
		public controllerAs: string = 'vm';
		public controller = Controller;
	}

	angular
		.module("pineapples")
		.component("stars", new Component());
}
