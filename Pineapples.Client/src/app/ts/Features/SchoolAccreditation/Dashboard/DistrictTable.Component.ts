﻿namespace Pineapples.Dashboards {

	class Controller extends DashboardChild implements IDashboardChild {

		// reference to the host dashboard is from the 'require' on the componentOptions
		public dashboard: any;
		public levels = ["Level 1", "Level 2", "Level 3", "Level 4"];
		public accreditations: any[];

		public onOptionChange(optionchange) {
			this.redrill();
		}
		public onChildSelected() {
			if (!this.isSelected()) {
				this.drillfltr = null;
				this._selectedcell = "";
			}
		}
		private drillfltr;
		private redrill() {
			if (this.drillfltr && this.isSelected()) {
				angular.merge(this.drillfltr, {
					Authority: this.options.selectedAuthority,
					AuthorityGroup: this.options.selectedAuthorityGovt,
					SchoolType: this.options.selectedSchoolType,
					BestUpTo: this.options.selectedYear
				});
				if (this.drillfltr.InspYear) {
						this.drillfltr.InspYear = this.options.selectedYear;
				}
				this.getDrillData();
			}
		}
		public drilldown(level, districtCode, cumulative = "") {

			this.drillfltr = {
				InspResult: level,
				District: districtCode,
				Authority: this.options.selectedAuthority,
				AuthorityGroup: this.options.selectedAuthorityGovt,
				SchoolType: this.options.selectedSchoolType,
				InspYear: cumulative ? null : this.options.selectedYear,
				BestUpTo: this.options.selectedYear
			};
			this._selectedcell = `${level}|${districtCode}|${cumulative}`;
			this.getDrillData()
				.then(() => {
					this.setSelected();
			});;
		}
		private getDrillData() {
			return this.http.post("api/warehouse/accreditations/filter", this.drillfltr)
				.then(result => {
					this.accreditations = <any[]>result.data;
				});

		}

		private _selectedcell: string;
		public isSelectedCell(level: string, districtCode: string, cumulative = "") {
			return this.isSelected() && this._selectedcell == `${level}|${districtCode}|${cumulative}`;
		}
	}
	class Component implements ng.IComponentOptions {
		public bindings: any = {
			dashboard: "<",
			group: "<",
			options: "<",			
			selectedChild: "<",
			optionChange: "<",
			reportPath: "@"
		};
		public templateUrl: string;
		public controllerAs: string = 'vm';

		constructor(public controller: any, templateUrl) {
			this.templateUrl = "schoolaccreditation/dashboard/component/" + templateUrl;
		}
	}

  angular
    .module("pineapples")
    .component("accreditationDistrictTable", new Component(Controller, "districtTable"));
}
