﻿namespace Pineapples.Dashboards.Accreditations {

	export class LevelController extends DashboardChild implements IDashboardChild {

		// overrideable
		protected defaultTitle = "Accreditation Progress";
		protected defaultPalette = ["#FF0000", "#FFC000", "#92D050", "#00B050"];

		public get viewOptions() {
			return [
				`Evaluated in ${this.options.selectedYear}`
				, `Cumulative to ${this.options.selectedYear}`
			]
		}

		// reference to the host dashboard is from the 'require' on the componentOptions
		public dashboard;
		public levels = ["Level 1", "Level 2", "Level 3", "Level 4"];
		
		public sortBy = "id";
		public sortDescending = true;
		public accreditations: any[];

		// highlight manages reflecting the selected item in the dimension on the chart
		// 3 ways you can do this:
		// 1) bind directly to rowcolchart in the component's html
		//				highlight="vm.options.selectedDistrict"
		//		this is the simplest in most cases
		// 2) override the getter to return the value - this means it gets dynamically recalculated
		//		html looks like this:
		//				highlight="vm.highlight"
		//		as in this case this works when different component share the same html 
		// - they can each set up their own getter for highlight
		// 3) the the highlight in option change. Don;t override the getter
		//    onOptionChange event

		public get highlight() {
			return this.options.selectedDistrict;
		};


		public get filters() {
			return this.options.descriptionExcluding("Year", "District");
		}
		public touch: number;

		public group: CrossFilter.Group<any,any,any>;
		public chartData: Sw.Charts.AnnotatedGroup;
		public charttype ="h";

		public $onInit() {
			// note this is only called from $onInit, becuase this object never changes
			// to notify the chart that the data has changed (ie change of option, or change of selected view)
			// we just use 'touch' binding on-row-col-chart to force a data transformation and redraw
			this.makeChartData();

		}

		public $onChanges(changes) {
			super.$onChanges(changes);
			this.touch = Date.now();
		}
		

		public makeChartData() {
			/** chartData format is 'AnnotatedGroup'
			 * ie it is a crossfilter group, with some extra info to describe how it is presented in the chart:
			*  group : the crossfilter group itself
			*  columns: an array of column names, or a function returning an array of column names
			*     if a function, it is evaluated on each row of group.all() - each of these is a key-value pair
			*		 , but the value may be a complex object, especially if created by xReduce
		  *	 valueAccessor: thisfunction extracts the column value from the key value pair:
		  *	 arguments row - the key value object
		  *	           columnName - taken from 'columns'
		  *	           columnIndex - index in columns array
		  *	 EchartTransformer can explicitly support this format, so it can be passed directly to row-col-chart as 
		  *	 the dataset binding
			*/
			this.chartData = {
				group: this.group,
				columns: this.levels, 
				valueAccessor: (row, columnName, columnIdx) => row.value[columnName] ?
						row.value[columnName][["NumThisYear","Num"][this.selectedViewOption]] * (columnIdx == 0?-1:1) : 0
			};
		}

		public tooltipper(datum) {
			// update the datum and return to the caller....
			// expand the district name
			datum.item = this.lookups.byCode('districts', datum.item, "N") || datum.item;
			// show the absolute value, not the negative value in the tooltip
			datum.value = Math.abs(datum.value);
		}

		public onChartRender(option, echart) {
			angular.merge(option, {
				xAxis: {
					axisLabel: {
						formatter: (value) => Math.abs(value) // the xAxis tick labels should be absolute values, not negatives
					}
				},
				yAxis: {
					axisLabel: {
						formatter: (value) => this.lookups.byCode('districts', value, "N") || value // the xAxis tick labels should be name
					}
				}
			});
		}

		////////////////////////////////////////////////////////////////
		// Crossfilter interaction
		////////////////////////////////////////////////////////////////
		public clickHandler(params) {
			/**
			 * the click handler is bound to the rowcolchart to receive click events 
			 * from the echart
			 * where the data item (ie the row id) is related to a selection option on the dashboard, 
			 * we can toggle that option. ie if its current value is the value clicked on, turn off
			 * if the current value is not set or different, set to the clicked-on value
			 * In the dashboard, the option change is detected and applied to the filter on the corresponding 
			 * crossfilter dimension. Hence, all the artefacts (tables as well as charts) that are built on the same 
			 * crossfilter are refreshed with updated data.
			 * Note that when a filter is applied to a dimension, all the OTHER dimensional objects change,
			 * not those built on the dimension that got the filter
			 */
			this.options.toggleSelectedDistrict(params.name);
		}

		public onChildSelected() {
			if (!this.isSelected()) {
				this.drillfltr = null;
				this._selectedcell = "";
			}
		}

		private _selectedViewOption = 1;

		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.touch = Date.now();

			}
		}

		private drillfltr;
		private redrill() {
			if (this.drillfltr && this.isSelected()) {
				angular.merge(this.drillfltr, {
					Authority: this.options.selectedAuthority,
					AuthorityGroup: this.options.selectedAuthorityGovt,
					SchoolType: this.options.selectedSchoolType,
					BestUpTo: this.options.selectedYear
				});
				if (this.drillfltr.InspYear) {
						this.drillfltr.InspYear = this.options.selectedYear;
				}
				this.getDrillData();
			}
		}
		public drilldown(level, districtCode, cumulative = "") {

			this.drillfltr = {
				InspResult: level,
				District: districtCode,
				Authority: this.options.selectedAuthority,
				AuthorityGroup: this.options.selectedAuthorityGovt,
				SchoolType: this.options.selectedSchoolType,
				InspYear: cumulative ? null : this.options.selectedYear,
				BestUpTo: this.options.selectedYear
			};
			this._selectedcell = `${level}|${districtCode}|${cumulative}`;
			this.getDrillData()
				.then(() => {
					this.setSelected();
			});;
		}
		private getDrillData() {
			return this.http.post("api/warehouse/accreditations/filter", this.drillfltr)
				.then(result => {
					this.accreditations = result.data["ResultSet"];
				});

		}

		private _selectedcell: string;
		public isSelectedCell(level: string, districtCode: string, cumulative = "") {
			return this.isSelected() && this._selectedcell == `${level}|${districtCode}|${cumulative}`;
		}
	}

	/*
	 * 
	 **/
	class YearLevelController extends LevelController implements IDashboardChild {
		public defaultTitle = "Accreditation Progress";

		// becuae there is always a selected year, don't highlight it
		public get highlight() {
			return null;
		}

		public get filters() {
			return this.options.descriptionExcluding("Year");
		}

		public get viewOptions() {
			return [
				`Evaluated in year`
				, `Cumulative to year`
			]
		}

		/**
		 * Change the year by clicking bar
		 * Note that there should alwatys be a selected year, so don't toggle, just set
		 * Also we don't highlight the selected year, otherwise we would neve see this chart in full color
		 * @param event
		 */
		public clickHandler(params) {
			this.options.selectedYear = params.name;
		}
		// show the absolute value, not the negative value in the tooltip
		public tooltipper(datum) {
			datum.value = Math.abs(datum.value);
		}

	}


	class PieController extends LevelController implements IDashboardChild {
		public defaultTitle = "Accreditation Progress";

		public get highlight() {
			return null;
		}

		public get filters() {
			return this.options.descriptionExcluding("Year");
		}

		public charttype="pieh"
	

		/**
		 * Change the year by clicking bar
		 * Note that there should alwatys be a selected year, so don;t toggle, just set
		 * @param event
		 */
		public clickHandler(params) {
			//we have a pie by accreditation level - it is therefore not related to any filter option
			// click goes unanswered....
		}

		public makeChartData() {
			/** 
			 *  The small variation here is that we do not need to make level 1 negative -
			 *  in fact it breaks the pie chart to do so
			*/
			this.chartData = {
				group: this.group,
				columns: this.levels,
				valueAccessor: (row, columnName, columnIdx) => row.value[columnName] ?
					row.value[columnName][["NumThisYear", "Num"][this.selectedViewOption]] : 0
			};
		}
		// override the default tooltip format for the pie chart
		// Since this chart is filter by district, show the relevant district in the tip
		// makes it a bit clearer what is going on in this pie chart
		public tooltipper(datum) {
			
			// show the selected district, or national in the tooltip
			let district = this.options.selectedDistrict ?
				this.lookups.byCode('districts', this.options.selectedDistrict, "N") : 'National';
			// return a value for a non-standard layout
			return `${district}
<br/>${datum.marker} ${datum.item}: <b>${datum.value}</b>
<br/>${datum.percent}%`;
		}

		public onChartRender(option, echart) {
			// turn off the legend
			angular.merge(option, {
				legend: {
					show: false
				}
			});
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			dashboard: "<",
			group: "<",
			options: "<",			
			optionChange: "<",
			selectedChild: "<",
			reportPath: "@",
			// user configurable ie specific dashboard.html for different places
			// can change set the title or palette according to local needs/standards
			userPalette: "<colors",
			userTitle: "@headingTitle"
		};
		public templateUrl: string;
		public controllerAs: string = 'vm';

		constructor(public controller: any, templateUrl) {
			this.templateUrl = "schoolaccreditation/dashboard/component/" + templateUrl;
		}
	}

  angular
		.module("pineapples")
		.component("accreditationLevelChart", new Component(LevelController, "nationChart"))
		.component("accreditationNationChart", new Component(LevelController, "nationChart"))
		.component("accreditationPieChart", new Component(PieController, "nationChart"))
	  .component("accreditationYearChart", new Component(YearLevelController, "nationChart"));
}
