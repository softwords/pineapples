﻿namespace Pineapples.Dashboards {

	class Controller {
		// allow the star widget to respond to either a numeric (number of stars)
		// or a Level string 

	
		private _value: any;
		private valn: number;
		private 
		private className: string;
		public levelName: string;
		public size: string = "";
		public set value(newValue) {
			this._value = newValue;
			if (!isNaN(newValue)) {
				this.valn = parseInt(newValue) != 0 ? 1 : 0;
			}
			else {
				switch ((<string>newValue).toLowerCase()) {
					case "y":
					case "yes":
					case "true":
					case "positive":
						this.valn = 1;
						break;
					case "n":
					case "no":
					case "false":
					case "negative":
						this.valn = 0;
						break;
					default:
						this.valn = -1;
				}
			}
		};

		public get yes() {
			return this.valn == 1 ? "checked" : "unchecked";
		}

		public get no() {
			return this.valn == 1 || this.valn == -1 ? "unchecked" : "checked";
		}

	}



	class Component implements ng.IComponentOptions {
		public bindings: any = {
			value: "<",
			size: "@"			// strin gvalue is more likely here than a calculation

		};
		public template: string = `<div>
					<div class="radiobutton" ng-class="vm.yes"> Yes</div>
					<div class="radiobutton" ng-class="vm.no"> No</div>
				</div>`;
		public controllerAs: string = 'vm';
		public controller = Controller;
	}

	angular
		.module("pineapples")
		.component("yesNo", new Component());
}
