﻿namespace Pineapples {

	interface INamedId {
		id: string,
		name: string
	}

	interface IAnswer {
		state: string
	}

	interface ISubCriteria extends INamedId {
		answer: IAnswer,
		hint: string,
		interviewQuestions: string
	}

	interface ICriteria extends INamedId {
		subcriteria: ISubCriteria[]
	}

	interface IStandard extends INamedId {
		criteria: ICriteria[]
	}

	interface ICategory {
		type: string,
		name: string,
		standard: IStandard[]
	}

	interface IEvaluation {
		category: ICategory[],
		date: Date,
		region: string,
		schoolId: string,
		schoolName: string,
		type: string,
		version: number
	}

	// for now this is a read-only object
	export class Evaluation implements IEvaluation {

		constructor(data:any) {
			angular.extend(this, data.survey);
		}

		public category: ICategory[];
		public date: Date;
		public region: string;
		public schoolId: string;
		public schoolName: string;
		public type: string;
		public version: number;
	}
}