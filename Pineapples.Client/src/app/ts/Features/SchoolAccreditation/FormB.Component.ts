﻿namespace Pineapples.SchoolAccreditations {
	class Controller  {
		model: Pineapples.Cloudfiles.ISchoolStandardAssessment;
		calc: ISSACalc;
		metadata: any;
		public id: string;

		// crossfilter object and dimensions
		//private xFilter: Sw.xFilter.XFilter;
		//public xf: CrossFilter.CrossFilter<ISSAData>;
		//public dimStandard: CrossFilter.Dimension<any, string>;
		//public dimCategoryType: CrossFilter.Dimension<ISSAData, [string, string]>;
		//public grpStandard: CrossFilter.Group<any, string, number>;
		//public grpCategoryType: CrossFilter.Group<any, string, number>;

		static $inject = ["$scope", "$http", "$stateParams", "Lookups"];
		constructor(public scope: Sw.IScopeEx, public http: ng.IHttpService
			, public stateParams: ng.ui.IStateParamsService
			, public lookups: Sw.Lookups.LookupService) {
			this.id = stateParams["id"];
			//this.xFilter = new Sw.xFilter.XFilter();
		}

		public $onInit() {
			// create the crossfilter if rowset data is available
			// the model is the survey
			// get the relevant calculator
			this.calc = SSACalculatorFactory.create(this.model);

			this.template = this.model.version > 0 ?
				'schoolaccreditation/formb/v1' : 'schoolaccreditation/formb/legacy';
		}

		// template - get the relevant version of this form

		public template: string;
		// Form B implementation

	
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			model: "<"
		};
		public controller = Controller
		public controllerAs: string = 'vm';
		public template = `<ng-include src="vm.template"></ng-include>`;
}

	angular
		.module("pineapples")
		.component("formB", new Component());
}