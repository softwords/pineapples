namespace Pineapples.SchoolAccreditations {

	export interface ISSACalc {

		prepareData(survey);
		// methods to get a score and a 'result'

		/**
		 * get a subcriteria score
		 * @param id - id of the subcriteria
		 * @param catname if supplied restrict to the category of this name
		 */
		subcritScore(id: string, catname?: string);

		score(std: string, criteriaSeq: string, subcritSeq: string, catname?: string);

		/**
		 * get a criteria score
		 * @param id - id of the criteria
		 * @param catname if supplied restrict to the category of this name
		 */
		critScore(id: string, catname?: string);

		/**
		 * get a standard score
		 * @param id - id of the standard
		 * @param catname if supplied restrict to the category of this name
		 */
		standardScore(id: string, catname?: string);

		/**
		 * get a standard maxscore
		 * @param id - id of the standard
		 * @param catname if supplied restrict to the category of this name
		 */
		standardScoreMax(id: string, catname?: string);

		/**
		 * get a category type score
		 * @param type - id of the category
		 * @param catname if supplied restrict to the category of this name
		 */
		categorytypeScore(type: string, catname?: string);

		/**
		 * get a category  score
		 * @param catname category name
		 */
		categoryScore(catname: string);
		/**
		 * get a criteria Result (ie 'Level 1', 2 etc)
		 * @param id - id of the criteria
		 * @param catname if supplied restrict to the category of this name
		 */
		critLevel(id: string, catname?: string, maxscore?: number);

		/**
		 * get a standard result
		 * @param id - id of the standard
		 * @param catname if supplied restrict to the category of this name
		 */
		standardLevel(id: string, catname?: string, maxScore?: number);

		/**
		 * get a category type result
		 * @param type - id of the category
		 * @param catname if supplied restrict to the category of this name
		 */
		categorytypeLevel(type: string, catname?: string, maxscore?: number);

		/**
		 * Calculate or retrieve the overall result of the inspection
		 * 
		 */
		assessedLevel();

		/**
		 * Tally of the number of items with this level
		 * In legacy applications this will tally across criteria, and classroom observations
		 * @param level
		 */
		tally(level: string);

		/**
		 *  Partition to convert score to result for criteria
		 */
		criteriaPartition();

		/**
		 *  Partition to convert score to result for categoryType/category ie this is for Classroom observations
		 */
		categorytypePartition();


	}

	export class SSACalculatorFactory {
		public static create(survey) {
			switch (survey.version) {
				case "0":
					return new SSACalculatorLegacy(survey);
			}
			switch (survey.country) {
				case "FSM":
					return new SSACalculator(survey);
				case "RMI":
					return new SSACalculatorRMI(survey);
			}
			return new SSACalculator(survey);
		}
	}

	class SSACalculator implements ISSACalc {

		protected rowset: any[];
		protected critrowset: any[];

		public criteriaPartition() {
			return this.ptSSACrit;
		}

		public categorytypePartition() {
			return this.ptSSACrit;
		}

		constructor(survey) {
			this.prepareData(survey);
		}

		public prepareData(survey) {
			this.rowset = [];
			//this.subcriteriaScores = {};
			survey.category.forEach(category => {
				category.standard.forEach(standard => {
					standard.criteria.forEach(criteria => {
						criteria.maxscore = criteria.subcriteria.length;
						criteria.score = _.filter(criteria.subcriteria, sc => sc.answer.state == 'POSITIVE').length;
						criteria.subcriteria.forEach(sub => {
							let a: Pineapples.Cloudfiles.ISSAData = {
								schoolNo: survey.schoolId,
								category: category.name,
								categoryType: category.type,
								standard: standard.id,
								criteria: criteria.id,
								criteriaSeq: criteria.id.replace(standard.id, "C"),
								subcriteria: sub.id,
								subSeq: sub.id.replace(criteria.id, "S"),
								score: sub.score,
								maxscore: 1
							}
							this.rowset.push(a);
							//							this.subcriteriaScores[a.subcriteria] = a.score;
						})
					})
				})
			});
			console.log(this.rowset);
			let r = _(this.rowset)
				.filter(r => r.categoryType == 'SCHOOL_EVALUATION')		// just add the school evaluations in here
				.groupBy(r => r.criteria)
				.map((grp, key) => {
					let s = _.sumBy(grp, "score");
					let m = _.sumBy(grp, "maxscore");
					let pt = this.lookupPartitionPt(s, m, 100, this.criteriaPartition())
					return {
						criteria: key
						, categoryType: "SCHOOL_EVALUATION"
						, category: null
						, score: s
						, maxscore: m
						, result: pt ? pt.ptName : null
						, resultvalue: pt ? pt.ptValue : null
					}
				})
				.value();

			// now we manually add the two records describing the classroom observations onto this array
			let s = this.categorytypeScore("CLASSROOM_OBSERVATION", "Classroom Observation 1");
			let m = 60;
			let pt = this.lookupPartitionPt(s, m, 100, this.categorytypePartition());
			r.push({
				criteria: null
				, categoryType: "CLASSROOM_OBSERVATION"
				, category: "Classroom Observation 1"
				, score: s
				, maxscore: m
				, result: pt ? pt.ptName : null
				, resultvalue: pt ? pt.ptValue : null
			});
			s = this.categorytypeScore("CLASSROOM_OBSERVATION", "Classroom Observation 2");
			m = 60;
			pt = this.lookupPartitionPt(s, m, 100, this.categorytypePartition());
			r.push({
				criteria: null
				, categoryType: "CLASSROOM_OBSERVATION"
				, category: "Classroom Observation 2"
				, score: s
				, maxscore: m
				, result: pt ? pt.ptName : null
				, resultvalue: pt ? pt.ptValue : null
			});
			this.critrowset = r;


			console.log(r);




		}
		// Scores
		/**
		 * get a subcriteria score
		 * @param id - id of the subcriteria
		 * @param catname if supplied restrict to the category of this name
		 */
		subcritScore(id: string, catname?: string) {
			let fcn = a => (a.subcriteria == id
				&& catname ? catname == a.category : true);
			return this.rowsetReduce(fcn);

		}

		public score(std: string, criteriaSeq: string, subcritSeq: string, catname?: string) {
			let sc = std + '.' + criteriaSeq + '.' + subcritSeq;
			let fcn = row => (row.subcriteria == sc
				&& (catname ? row.category == catname : true));
			let r = _.find(this.rowset, fcn);
			return r ? r.score : null;
		}


		/**
		 * get a criteria score
		 * @param id - id of the criteria
		 * @param catname if supplied restrict to the category of this name
		 */
		critScore(id: string, catname?: string) {
			let fcn = a => (a.criteria == id
				&& (catname ? catname == a.category : true));
			return this.rowsetReduce(fcn);
		}

		/**
		 * get a standard score
		 * @param id - id of the standard
		 * @param catname if supplied restrict to the category of this name
		 */
		public standardScore(std, catname?: string) {
			let fcn = a => (a.standard == std
				&& (catname ? catname == a.category : true));
			return this.rowsetReduce(fcn);
		}

		/**
		 * get a standard maxscore
		 * @param id - id of the standard
		 * @param catname if supplied restrict to the category of this name
		 */
		public standardScoreMax(std, catname?: string) {
			let fcn = a => (a.standard == std
				&& (catname ? catname == a.category : true));
			return this.rowsetReduceMaxscore(fcn);
		}

		/**
		 * get a category type score
		 * @param type - id of the category
		 * @param catname if supplied restrict to the category of this name
		 */
		categorytypeScore(type: string, catname?: string) {
			let fcn = row => (row.categoryType == type
				&& (catname ? row.category == catname : true));
			return this.rowsetReduce(fcn);
		}

		/**
		 * get a category  score
		 * @param catname category name
		 */
		categoryScore(catname: string) {
			let fcn = row => (row.category == catname);
			return this.rowsetReduce(fcn);
		}

		/**
		 * get a criteria Result (ie 'Level 1', 2 etc)
		 * @param id - id of the criteria
		 * @param catname if supplied restrict to the category of this name
		 */
		critLevel(id: string, catname?: string, maxscore: number = 5) {
			return this.SSACrit(this.standardLevel(id, catname), maxscore)
		}

		/**
		 * get a standard result
		 * @param id - id of the standard
		 * @param catname if supplied restrict to the category of this name
		 */
		standardLevel(id: string, catname?: string, maxscore: number = 16) {
			return this.SSA(this.standardScore(id, catname), maxscore);
		}

		/**
		 * get a category type result
		 * @param type - id of the category
		 * @param catname if supplied restrict to the category of this name
		 */
		categorytypeLevel(type: string, catname?: string, maxscore: number = 60) {
			let value = this.categorytypeScore(type, catname);
			return this.lookupPartition(value, maxscore, 100, this.categorytypePartition());
		}

		assessedLevel() {

		}

		tally(level: string) {
			let fcn = a => (a.result == level);
			return this.rowsetCount(fcn, this.critrowset);
		}

		public seTotal() {
			return _(this.rowset)
				.filter(row => (row.categoryType == "SCHOOL_EVALUATION"))
				.reduce((sum, a) => sum + a.score, 0);
		}

		// implementation

		protected rowsetFind(filterfunction: (ISSAData) => boolean, prop: string) {
			let r = _(this.rowset)
				.find(filterfunction);
			return r ? r[prop] : null;
		}

		protected rowsetReduce(filterfunction: (ISSAData) => boolean) {
			return _(this.rowset)
				.filter(filterfunction)
				.reduce((sum, a) => sum + a.score, 0);
		}

		protected rowsetReduceMaxscore(filterfunction: (ISSAData) => boolean) {
			return _(this.rowset)
				.filter(filterfunction)
				.reduce((sum, a) => sum + a.maxscore, 0);
		}

		protected rowsetCount(filterfunction: (ISSAData) => boolean, rowset: any[] = this.rowset) {
			return _(rowset)
				.filter(filterfunction)
				.reduce((sum, a) => sum + 1, 0);
		}



		public coLesser() {
			let c1 = this.categoryScore("Classroom Observation 1");
			let c2 = this.categoryScore("Classroom Observation 2");
			if (c1 <= c2) {
				return "Classroom Observation 1";
			}
			return "Classroom Observation 2";
		}


		protected ptSSACrit = [
			{
				ptName: "Level 1"
				, ptMax: 50
			},
			{
				ptName: "Level 2"
				, ptMax: 75
			},
			{
				ptName: "Level 3"
				, ptMax: 90
			},
			{
				ptName: "Level 4"
				, ptMax: 100
			}
		]

		// RMI do some things things in even 4s
		protected ptEven4 = [
			{
				ptName: "Level 1"
				, ptValue: 1
				, ptMax: 25
			},
			{
				ptName: "Level 2"
				, ptValue: 2
				, ptMax: 50
			},
			{
				ptName: "Level 3"
				, ptValue: 3
				, ptMax: 75
			},
			{
				ptName: "Level 4"
				, ptValue: 4
				, ptMax: 100
			}

		]

		protected ptSSA = [
			{
				ptName: "Level 1"
				, ptMax: 50
			},
			{
				ptName: "Level 2"
				, ptMax: 75
			},
			{
				ptName: "Level 3"
				, ptMax: 90
			},
			{
				ptName: "Level 4"
				, ptMax: 100
			}
		]
		public SSA(value, divisor) {
			// Before the lookupPartition the final score must be rounded
			// This does not affect standard levels since they are always
			// whole numbers without decimals. But the final score coming from
			// school evaluation (90%) and the lowest classroom observation (10%)
			// can have decimals and must be rounded (e.g. 90.2 => 90, 90.5 => 91)
			return this.lookupPartition(value.toFixed(0), divisor, 100, this.ptSSA);
		}

		public SSACrit(value, divisor) {
			return this.lookupPartition(value, divisor, 100, this.ptSSACrit);
		}

		/**
		 * Return the object from a partition ie the whole partition entry
		 * @param value
		 * @param divisor
		 * @param scale
		 * @param partition
		 */
		public lookupPartitionPt(value, divisor, scale, partition) {
			let searchValue = value * scale / divisor;
			let pt = _.find(partition, pt => (searchValue <= pt.ptMax));
			return pt;
		}

		public lookupPartition(value, divisor, scale, partition) {
			let searchValue = value * scale / divisor;
			let pt = _.find(partition, pt => (searchValue <= pt.ptMax));
			return pt ? pt.ptName : null;
		}
	}

	// legacy calculator
	//
	class SSACalculatorLegacy extends SSACalculator implements ISSACalc {

		public prepareData(survey) {
			this.rowset = [];
			survey.category.forEach(category => {
				// add the rows for totals at the category ( ie classroom observation) level
				if (category.result) {
					let c = {
						schoolNo: survey.schoolId,
						category: category.name,
						categoryType: category.type,
						score: category.score,
						maxscore: category.maxscore,
						resultvalue: category.resultvalue,
						result: category.result
					}
					this.rowset.push(c);
				}
				if (category.standard) {
					category.standard.forEach(standard => {
						standard.criteria.forEach(criteria => {
							criteria.score = Number(criteria.score);
							criteria.maxscore = Number(criteria.maxscore);
							criteria.resultvalue = Number(criteria.resultvalue);
							let a: Pineapples.Cloudfiles.ISSAData = {
								schoolNo: survey.schoolId,
								category: category.name,
								categoryType: category.type,
								standard: standard.id,
								criteria: criteria.id,
								criteriaSeq: criteria.id.replace(standard.id, "C"),
								score: criteria.score,
								maxscore: criteria.maxscore,
								resultvalue: criteria.resultvalue,
								result: criteria.result

							}
							this.rowset.push(a);
						})
					})	// standard
				}
			})
		}


		public critLevel(id, catname?, maxscore?) {
			let fcn = a => (a.criteria == id
				&& (catname ? catname == a.category : true));
			return this.rowsetFind(fcn, "result");
		}

		tally(level: string) {
			let fcn = a => (a.result == level);
			return this.rowsetCount(fcn);
		}

		/**
		* get a category type result
		* In legacy apps, we have just one number for the classrtoom observation level
		* @param type - id of the category
		* @param catname if supplied restrict to the category of this name
		*/
		categorytypeLevel(type: string, catname?: string, maxscore: number = 16) {
			let fcn = a => (a.categoryType == type
				&& (catname ? catname == a.category : true));
			return this.rowsetFind(fcn, "result");
		}

		/**
		 * The legacy formula is based on tally:
		 * if Tally1 >= 4 then result = Level 1 
		 * Otherwise it is the max of Tally4, Tally3, Tally2 
		 * ties are resolved upwards
		 * 
		 */
		assessedLevel() {
			if (this.tally('Level 1') >= 4)
				return 'Level 1';
			let t4 = this.tally('Level 4');
			let t3 = this.tally('Level 3');
			let t2 = this.tally('Level 2');

			let r = t4;
			let s = 'Level 4';
			if (t3 > r) {
				r = t3;
				s = 'Level 3';
			}
			if (t2 > r) {
				r = t2;
				s = 'Level 2';
			}
			return s;
		}
	}

	class SSACalculatorRMI extends SSACalculator implements ISSACalc {

		public criteriaPartition() {
			return this.ptEven4;
		}

		public categorytypePartition() {
			return this.ptEven4;
		}

		public SSA(value, divisor) {
			// In RMI, unlike FSM there is no final score that must be rounded
			// since it uses a tally formula to produce the final level.
			// This is merely for standard (and criteria) levels
			return this.lookupPartition(value, divisor, 100, this.ptEven4);
		}

		// RMI use the tally based formula
		assessedLevel() {
			if (this.tally('Level 1') >= 4)
				return 'Level 1';
			let t4 = this.tally('Level 4');
			let t3 = this.tally('Level 3');
			let t2 = this.tally('Level 2');

			let r = t4;
			let s = 'Level 4';
			if (t3 > r) {
				r = t3;
				s = 'Level 3';
			}
			if (t2 > r) {
				r = t2;
				s = 'Level 2';
			}
			return s;
		}

	}
}

