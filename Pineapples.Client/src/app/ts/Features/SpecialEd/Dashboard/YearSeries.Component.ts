﻿namespace Pineapples.Dashboards.SpecialEd {

	// Base class for components that want to split some lookup value (Environment, Ethnicity, english learner, disability)
	// the group passed as binding is assumed to have a array as 'key' [ <code>, <name> , <name of code>]

	// Viewoptions used in this class: field name, precision, palette, lookup
	type ViewOption = [string, number, PaletteDefinition, string];

	export class SpecialEdSeriesController extends DashboardChild implements IDashboardChild {

		public options: coreOptions;
		public chartData: CrossFilter.Group<any, any, any>;
		public dashboard: SpecialEdDashboard;

		//--------------------------------------------------------------------------------------//
		// View Option Management
		//
		// View options for EdExpenditure data - charts may choose which of these values to chart
		// note they cannot be added together
		//--------------------------------------------------------------------------------------//
		public get viewOptions() {
			return [
				"Environment",
				"Disability",
				"Ethnicity",
				"English Learner",

			]
		};


		// field name and precision
		protected get viewOptionFields():ViewOption[] {
			return [
				["Environment", 0, "category10","spEdEnvironment"],
				["Disability", 0, colorbrewer.Set1[9], "disabilities"],
				// use EthnicityCode - Ethnicity seems to be null ? to investigate...
				["EthnicityCode", 0, colorbrewer.Accent[8], "ethnicities"],
				["EnglishLearner", 0, colorbrewer.Paired[9],"spEdEnglishLearner"]
			]
		}
		protected get selectedViewField() {
			return this.viewOptionFields[this.selectedViewOption][0];
		}
		protected get selectedViewPrecision(): number {
			return <number>this.viewOptionFields[this.selectedViewOption][1];
		}

		protected get selectedViewPalette(): string {
			return <string>this.viewOptionFields[this.selectedViewOption][2];
		}

		protected get selectedViewLookup() {
			let lkp = <string>this.viewOptionFields[this.selectedViewOption][3];
			return this.lookups.cache[lkp];
		}


		private _selectedViewOption = 0;
		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.onViewOptionChange(); 
			}
		}

		public baseDimension(): CrossFilter.Dimension<any,any> {
			return this.dashboard.dimSurveyYear;
		}

		public onViewOptionChange() {
			// when the view option has changed, create a new crossfilter group
			// according to the selected disaggregation
			this.makeChartData();

			this.defaultPalette = this.viewOptionFields[this.selectedViewOption][2];
		};

		public $onInit() {
			//this.viewOptions = this.dashboard.dimStandardCode.group().all().map(d => d.key);
			// force the recalculation of the group here
			this.onViewOptionChange();
		}

		public onDashboardReady() {
			// EchartTransformer will recognie this as 'xReduced'
			// and will assign rows and columns accordingly
			// so we do not in this case need a more elaborate 'AnnnotatedGroup'
			this.makeChartData();
		}
		public onOptionChange() {

	
			this.refreshChart();
		};

		protected makeChartData() {
			let pa = this.dashboard.xFilter.getPropAccessor(<string>this.viewOptionFields[this.selectedViewOption][0], "-");
			this.chartData = this.dashboard.xFilter.xReduce(this.baseDimension(), pa, d => d.Num);
		}

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
		}

		public clickHandler(item) {
			this.options.selectedYear = item;
		}

		public filterDescription = () => this.options.descriptionExcluding("Year");
	}

	// component options for dashboard children
	class ComponentOptions extends SpecialEdDashboardComponent implements ng.IComponentOptions {

		constructor(public controller: any, templateUrl) {
			super(controller, templateUrl);
		}
	}

	angular
		.module("pineapples")
		.component("specialEdYearSeries", new ComponentOptions(SpecialEdSeriesController, "specialEdDemographic"));
}