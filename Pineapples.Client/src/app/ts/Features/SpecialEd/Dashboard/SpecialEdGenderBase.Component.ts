﻿namespace Pineapples.Dashboards.SpecialEd {

	// Base class for components that want to split some lookup value (Environment, Ethnicity, english learner, disability)
	// the group passed as binding is assumed to have a array as 'key' [ <code>, <name> , <name of code>]

	export class GenderBaseDashboardChild extends DashboardChild implements IDashboardChild {

		public group: CrossFilter.Group<any, any, any>;
		private groupData: any[]; // filtered all() array of keyvalue pairs
		public options: coreOptions;
		public chartData: Sw.Charts.AnnotatedGroup; 
		public dashboard: SpecialEdDashboard;

		public paletteName;
		public viewOptions;

		// needs to be overriden by the heading-title binding
		// since the contents are determind by the group passed in
		protected defaultTitle = "Special Ed By Gender";
		

		public $onInit() {
			this.chartData = {
				group: this.group,
				rowId: kv => kv.key[1],
				columns: ["F", "M"],
				valueAccessor: (kv, col) => {
					if (this.options.selectedYear) {
						// to avoid confusion don't show sums over multiple years
						return kv.value.Tot[col];
					}
					return 0;
				}
					
			}
		}

		public onOptionChange() {
			this.refreshChart();
		};

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			// default
		}
		public onChartRender(option: echarts.EChartOption, echarts: echarts.ECharts, renderedType: string) {
			Sw.Charts.chartOps(option)
				.showLegend(renderedType != "pie")

		}
	}

	angular
		.module("pineapples")
		.component("specialEdGender", new SpecialEdDashboardComponent(GenderBaseDashboardChild, "specialEdGender"));
}