﻿namespace Pineapples.Dashboards.SpecialEd {

	// Base class for components that want to split some lookup value (Environment, Ethnicity, english learner, disability)
	// the group passed as binding is assumed to have a array as 'key' [ <code>, <name> , <name of code>]

	class Controller extends SpecialEdSeriesController implements IDashboardChild {



		public baseDimension(): CrossFilter.Dimension<any, any> {
			return this.dashboard.dimDistrictCode;
		}

		public get highlight() {
			return this.options.selectedDistrict;
		}
		public clickHandler(item) {
			this.options.toggleSelectedDistrict(item);
		}

		public filterDescription = () => this.options.descriptionExcluding("District");
	}
	
	angular
		.module("pineapples")
		.component("specialEdDistrictSeries", new SpecialEdDashboardComponent(Controller, "specialEdDemographic"));
}