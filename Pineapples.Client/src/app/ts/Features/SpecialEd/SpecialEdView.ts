﻿namespace Pineapples.Students {
	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'stuCardID',
				name: 'Student Card ID',
				displayName: 'Student Card ID',
				editable: false,
				pinnedLeft: true,
				width: 140,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"],
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm", "stuID")
			},
			{
				field: 'stuGiven',
				name: 'Given Name',
				displayName: 'Given Name',
				pinnedLeft: true,
				width: 160,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'stuFamilyName',
				name: 'Family Name',
				displayName: 'Family Name',
				pinnedLeft: true,
				width: 160,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
		]
	};
  let modes = [
    {
      key: "Demographics",
      gridOptions: {
        columnDefs: [
          {
            field: 'stuDoB',
            name: 'Date of Birth',
            displayName: 'Date of Birth',
            cellFilter: "date:'d-MMM-yyyy'",
          },
          {
            field: 'stuGender',
            name: 'Gender',
            displayName: 'Gender',
            cellClass: 'gdAlignRight',
            lookup: 'gender',
          },
          {
            field: 'stuEthnicity',
            name: 'Ethnicity',
            displayName: 'Ethnicity',
            cellClass: 'gdAlignRight',
            lookup: 'ethnicities',
            enableSorting: true,
            sortDirectionCycle: ["asc", "desc"],
          },
          
        ]
      }
    },    
  ];

	var pushModes = function (filter) {
		filter.ViewDefaults = viewDefaults;
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['SpecialEdFilter', pushModes]);
}
