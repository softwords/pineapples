﻿namespace Pineapples.SpecialEd {

	angular
		.module("pineapples")
		.component("specialEdRenderFrame", new Sw.Component.RenderFrameComponent("specialed"));
}