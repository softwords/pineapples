﻿namespace Pineapples.SpecialEd {

  interface IBindings {
    model: SpecialEdStudent;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: SpecialEdStudent;

    static $inject = ["ApiUi", "specialEdAPI"];
    constructor(apiui: Sw.Api.IApiUi, private api: any) {
      super(apiui);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

		protected getNew = () => {
			return this.api.new();
		}

  }

  angular
    .module("pineapples")
    .component("componentSpecialEdStudent", new Sw.Component.ItemComponentOptions("specialed", Controller));
}