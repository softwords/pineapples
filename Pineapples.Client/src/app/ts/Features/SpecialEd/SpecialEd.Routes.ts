﻿// Students Routes
namespace Pineappples.SpecialEd {

	let RouteHelper = Sw.Utils.RouteHelper;

	let routes = function ($stateProvider: ng.ui.IStateProvider) {
		var featurename = 'SpecialEd';
		var filtername = 'SpecialEdFilter';
		var templatepath = "specialEd";
		var tableOptions = "studentFieldOptions";
		var url = "specialed";
		var usersettings = null;
		//var mapview = 'StudentMapView';

		// root state for 'specialed' feature
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath, url, usersettings, tableOptions);

		// default 'api' in this feature is studentsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "specialEdAPI";
		// load student/special ed codesets
		state.resolve["customLookups"] = ["Lookups", (lookups: Pineapples.Lookups.LookupService) => lookups.student()]

		state.data = state.data || {};
		state.data.frameTitle = "Special Education";
		state.data.icon = "people_outline";
		state.data.permissions = {
			only: 'SpecialEdRead'
		};

		let basestate = "site.specialed";
		$stateProvider.state(basestate, state);

		// List state
		state = Sw.Utils.RouteHelper.frameListState("specialed", "stuID");
		let statename = `${basestate}.list`;
		$stateProvider.state(statename, state);

		state = {
			url: `^/{url}/-`,
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.remove(`${templatepath}/item`);
				$templateCache.remove(`${templatepath}/searcher`);
				$state.go(`${basestate}.list`);
			}]
		};
		statename = `${basestate}.reload`;
		$stateProvider.state(statename, state);

		// chart, table and map
		Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addMapState($stateProvider, featurename); // , mapview

		// new - state with a custom url route
		state = {
			url: `^/${url}/new`,
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'SpecialEdWriteX'
				}
			},
			views: {
				"actionpane@site.students": {
					component: "componentSpecialEdStudent"
				}
			},
			resolve: {
				model: ['specialEdAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}],
				pageTitle: RouteHelper.simpleTitle("(new SpEd student)")
			}
		};
		$stateProvider.state(`${basestate}.list.new`, state);

		state = {
			url: "/reports",
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			data: {
				rendermode: "Reports"
			},
			resolve: {
				folder: () => "SpecialEd",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always",
				pageTitle: Sw.Utils.RouteHelper.simpleTitle("Reports")
			}
		}
		$stateProvider.state(`${basestate}.reports`, state);

		// item state
		//$resolve is intriduced in ui-route 0.3.1
		// it allows bindings from the resolve directly into the template
		// injections are replaced with bindings when using a component like this
		// the component definition takes care of its required injections - and its controller
		state = {
			url: "^/specialed/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.specialed": {
					component: "componentSpecialEdStudent"
				}
			},
			resolve: {
				model: ['specialEdAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}],
				titleId: ["model", (model) => `${model.stuGiven} ${model.stuFamilyName}`],
				pageTitle: RouteHelper.indirectTitle()
			}
		};
		$stateProvider.state(`${basestate}.list.item`, state);

		// Dashboard 
		//---------------------
		// state for a high level dashboard
		state = {
			url: "/dashboard",
			data: {
				rendermode: "Dashboard"
			},
			views: {
				"renderarea": "specialEdDashboard",
				"searcher": "coreOptionsEditor"
			},
			resolve: {
				options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
					return new Pineapples.Dashboards.coreOptions(lookups, scope.hub);
				}],

				tables: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
					let p = reflator.get("api/warehouse/specialeducation").then(response => (<any>response.data));
					return q.all([p]);		// an array of 1 promise
				}],
				pageTitle: RouteHelper.simpleTitle("Dashboard")
			}
		};
		$stateProvider.state(`${basestate}.dashboard`, state);

		state = {

			url: "^/spedtables/{lkp}",
			params: {
				lkp: { value: null, squash: true }
			},
			data: {
				rendermode: "Lookups"
			},
			views: {
				"@": {
					component: "componentTableSelector"
				}
			},
			resolve: {
				tableList: ["specialEdAPI", (api) => {
					return api.getEditableTables();
				}],
				rowset: ['$stateParams', 'Restangular', ($stateParams, restangular: restangular.IService) => {
					if ($stateParams.lkp) {
						return restangular.all($stateParams.lkp).getList();
					}
					return null;
				}],
				entityType: ['$stateParams', ($stateParams) => {
					return $stateParams.lkp;
				}],
				entityDescription: ['$stateParams', 'Restangular', ($stateParams, restangular: restangular.IService) => {
					if ($stateParams.lkp) {
						return restangular.all($stateParams.lkp).get("info/description");
					}
					return null;
				}],

				viewMode: ['$stateParams', "$http", ($stateParams, http: ng.IHttpService) => {
					if ($stateParams.lkp) {
						return http.get(`api/${$stateParams.lkp}/info/viewmode`).then(result => result.data);
					}
					return null;
				}],
				isEditing: () => true,
				pageTitle: RouteHelper.simpleTitle("Lookups")
			}
		};
		$stateProvider.state("site.specialed.lookups", state);

		$stateProvider
			.state('site.specialed.dataexport', {
				url: '^/specialed/dataexport',

				views: {
					"@": "specialEdDataExportComponent"
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Data Export")
				}
			});
	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}