﻿namespace Pineapples.SpecialEd {


	class SpecialEdFilter extends Pineapples.Students.StudentFilter implements Sw.Filter.IFilter {
		static $inject = ["$rootScope", "$state", "$q", "Lookups", "specialEdAPI", "identity"];
	}

	angular
		.module("pineapples")
		.service("SpecialEdFilter", SpecialEdFilter);
}