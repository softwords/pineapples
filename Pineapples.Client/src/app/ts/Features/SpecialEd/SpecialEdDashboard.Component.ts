﻿namespace Pineapples.Dashboards {

	export namespace SpecialEducation {

		// refer to the table warehouse.SpecialEd
		// which is accessed via api/warehouse/specialeducation
		export interface IxfData {
			Age: number;
			AuthorityCode: string;
			AuthorityGovtCode: string;
			AuthorityGovt: string;
			DistrictCode: string;
			SchoolTypeCode: string;
			SurveyYear: number;

			DisabilityCode: string;
			Disability: string;
			EthnicityCode: string;
			Ethnicity: string;
			EnvironmentCode: string;
			Environment: string;
			EnglishLearnerCode: string;
			EnglishLearner: string;

		}


	}
	export class SpecialEdDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {


		private xReduce = _.memoize((tableName, ...inputs) => this.xFilter.xReduce.bind(this.xFilter)(...inputs))
		private groupReduceSum = _.memoize((groupName, dim, va) => dim.group().reduceSum(va));

		// additional data

		private tables: any[];
		// override with strongly typed crossfilter
		public xf: CrossFilter.CrossFilter<SpecialEducation.IxfData>;

		// dimensions and groups based on Accreditations
		public dimSurveyYear: CrossFilter.Dimension<SpecialEducation.IxfData, number>;
		public dimDistrictCode: CrossFilter.Dimension<SpecialEducation.IxfData, string>;
		public dimAuthorityCode: CrossFilter.Dimension<SpecialEducation.IxfData, string>;
		public dimAuthorityGovtCode: CrossFilter.Dimension<SpecialEducation.IxfData, string>;
		public dimSchoolTypeCode: CrossFilter.Dimension<SpecialEducation.IxfData, string>;

		public dimEthnicity: CrossFilter.Dimension<SpecialEducation.IxfData, string[]>;
		public dimDisability: CrossFilter.Dimension<SpecialEducation.IxfData, string[]>;
		public dimEnglishLearner: CrossFilter.Dimension<SpecialEducation.IxfData, string[]>;
		public dimEnvironment: CrossFilter.Dimension<SpecialEducation.IxfData, string[]>;

		public grpDistrictCode: CrossFilter.Group<SpecialEducation.IxfData, string, number>;
		public grpAuthorityCode: CrossFilter.Group<SpecialEducation.IxfData, string, number>;
		public grpAuthorityGovtCode: CrossFilter.Group<SpecialEducation.IxfData, string, number>;

		public grpEthnicity: CrossFilter.Group<SpecialEducation.IxfData, string, number>;
		public grpDisability: CrossFilter.Group<SpecialEducation.IxfData, string, number>;
		public grpEnvironment: CrossFilter.Group<SpecialEducation.IxfData, string, number>;
		public grpEnglishLearner: CrossFilter.Group<SpecialEducation.IxfData, string, number>;

		public $onChanges(changes) {
			if (changes.tables && changes.tables.currentValue) {
				// data has been retrieved by the ui-router as a Resolve
				// and passed to the dashboard as a binding - process it into a crossfilter
				// create the crossfilter from the warehouse table
				this.xf = crossfilter(this.tables[0]);
				this.createDimensions();
			}
		}

		public $onInit() {

		}

		public onOptionChange(data:IOptionChangeData, sender) {
			if (data.selectedYear) {
				this.dimSurveyYear.filter(this.options.selectedYear);
			}
			if (data.selectedDistrict) {

				this.dimDistrictCode.filter(this.options.selectedDistrict);
			}
			if (data.selectedAuthorityGovt) {
				this.dimAuthorityGovtCode.filter(this.options.selectedAuthorityGovt);
			}
			if (data.selectedAuthority) {
				this.dimAuthorityCode.filter(this.options.selectedAuthority);
			}
			if (data.selectedSchoolType) {
				this.dimSchoolTypeCode.filter(this.options.selectedSchoolType);
			}
			// always call the super version so that the clients get to know about the option change as well
			super.onOptionChange(data, sender);
		}

		private elSelector = ({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel)["L"];
		private classLevelSelector = this.xFilter.getPropAccessor("ClassLevel")

		public createDimensions() {
			// create the dimensions

			// Lookup by code like others now...
			//this.dimAuthorityGovtCode = this.xf.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovtCode);
			//	return ag ? ag.N : null;
			//});

			this.dimAuthorityGovtCode = this.xf.dimension(d => d.AuthorityGovtCode);
			this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
			this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);
			this.dimAuthorityCode = this.xf.dimension(d => d.AuthorityCode);
			this.dimSchoolTypeCode = this.xf.dimension(d => d.SchoolTypeCode);

			// dimensions on lookups - the key is an array of code, name and code type description
			this.dimDisability = this.xf.dimension(d => [
				d.DisabilityCode || 'na',
				d.Disability || d.DisabilityCode || 'na',
				"Disability"]);
			this.dimEthnicity = this.xf.dimension(d => [
				d.EthnicityCode || 'na',
				d.Ethnicity || d.EthnicityCode || 'na',
				"Ethnicity"]);
			this.dimEnvironment = this.xf.dimension(d => [
				d.EnvironmentCode || 'na',
				d.Environment || d.EnvironmentCode || 'na',
				"Sp Ed Environment"]);
			this.dimEnglishLearner = this.xf.dimension(d => [
				d.EnglishLearnerCode || 'na',
				d.EnglishLearner || d.EnglishLearnerCode || 'na',
				"English Learner"]);


			this.grpDisability = this.xFilter.xReduce(this.dimDisability, d => d.DisabilityCode
				, this.xFilter.getFlattenedGenderAccessor("GenderCode", "Num"));
			this.grpEthnicity = this.xFilter.xReduce(this.dimEthnicity, d => d.DisabilityCode
				, this.xFilter.getFlattenedGenderAccessor("GenderCode", "Num"));
			this.grpEnvironment = this.xFilter.xReduce(this.dimEnvironment, d => d.DisabilityCode
				, this.xFilter.getFlattenedGenderAccessor("GenderCode", "Num"));

			this.grpEnglishLearner = this.xFilter.xReduce(this.dimEnglishLearner, d => d.DisabilityCode
				, this.xFilter.getFlattenedGenderAccessor("GenderCode", "Num"));


			// set the surveyYear to be the most recent in the warehouse data, rather than the most recent surveyYear in the lookups
			// we dont want to get empty data on the initial display if the warehouse is not yet generated for the current year
			if (!this.options.selectedYear) {
				this.options.selectedYear = this.dimSurveyYear.top(1)[0].SurveyYear;
			}
			this.dashboard = this;
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			tables: "<",			// array of data tables
			options: "<"			// get the options
		};
		public controller = SpecialEdDashboard;
		public controllerAs = "vm"
		public templateUrl: string = "specialed/dashboard";
	}



	angular
		.module("pineapples")
		.component("specialEdDashboard", new Component());

	// component options for dashboard children
	export class SpecialEdDashboardComponent extends DashboardChildComponent implements ng.IComponentOptions {
		public get feature() {
			return "specialed";
		}

		constructor(public controller: any, templateUrl) {
			super(controller, templateUrl);
		}
	}
}
