﻿namespace Pineapples.SpecialEd {

	export class SpecialEdStudent extends Pineapples.Students.Student implements Sw.Api.IEditable {

		constructor(studentData) {
			super(studentData);
		}

		// create static method returns the object constructed from the resultset object from the server
		public static create(resultSet: any) {
			let student = new SpecialEdStudent(resultSet);
			return student;
		}

		// IEditable implementation
		public _name() {
			return (<any>this).stuGiven;
		}
		public _type() {
			return "student";
		}
		public _id() {
			return (<any>this).stuID
		}

		public _transform(newData) {
			// convert these incoming data values
			// transformDates is deprecated: http interceptor now converts dates on load
			//this._transformDates(newData, ["stuDoB"]);
			return super._transform(newData);
		}


		public EvaluationEvents: any[];

	}
}