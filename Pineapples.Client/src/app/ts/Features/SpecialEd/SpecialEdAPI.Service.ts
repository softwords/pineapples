﻿namespace Pineapples.Api {
	const ENTITY = "specialedstudents";
	class apiService {

		static $inject = ["$q", "$http", "Restangular"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService) {
		}

		public read(id) {
			return this.restangular.one(ENTITY, id).get();
		}

		public new() {
			//return this.restangular.one(ENTITY, 0).get();
			let e = new Pineapples.Students.Student({
				stuID: null
			});
			this.restangular.restangularizeElement(null, e, ENTITY);
			return this.$q.resolve(e);
		}

		public filterPaged(fltr) {
			return this.http.post(`api/${ENTITY}/collection/filter`, fltr)
				.then(response => (response.data));
		}

		public getEditableTables() {
			return this.http.get("api/lookups/spededitabletables")
				.then(response => response.data);
		}
	}

	angular.module("pineapplesAPI")
		.service("specialEdAPI", apiService);
}