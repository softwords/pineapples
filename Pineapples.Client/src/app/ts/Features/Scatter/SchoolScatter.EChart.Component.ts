﻿namespace Pineapples.Dashboards {

	const NOT_DEFINED = "<>";

	class Controller {

		// bindings
		public datasets: any;
		public colorBy: string = "District";
		public colorByValue: string;
		public statsBy;

		public selected: string; // the selected school

		private _columnValue;
		public get columnValue() {
			return this._columnValue;
		};
		public set columnValue(newvalue) {
			this._columnValue = newvalue;
			switch (this._columnValue) {
				case "scatter":
					this.charttype = "scatter";
					break;
				case "xaccum":
				case "yaccum":
					this.charttype = "gini";
					break;
				case "xhist":
				case "yhist":
					this.charttype = "hist";
					break;

				default:
					this.charttype = "column";
					break;
			}
		}
		public onClick;
		public onDblClick;

		public charttype;
		public height = 800;
		public width = 800;

		private getDataValue() {
			switch (this.columnValue) {
				case "x":
					return "XValue";
				case "y":
					return "YValue";
				case "yonx":
					return "YonX";
				case "xony":
					return "XonY";
				case "xminusy":
					return "XminusY";
				case "yminusx":
					return "YminusX";
				case "xaccum":
					return "XValue";		// use the XValue in this series, and accum value in the other
				case "yaccum":
					return "YValue";

			}
		}
		private getAccumValue() {
			switch (this.columnValue) {

				case "xaccum":
					return "XAccum";
				case "yaccum":
					return "YAccum";

			}
		}
		// because the accumulated totals have already been set in the SQL
		// we use Xidx or Yidx to sort columns on 
		private getSortValue() {
			switch (this.columnValue) {
				case "x":
					return "XIdx";
				case "y":
					return "YIdx";
				case "yonx":
					return "YonX";
				case "xony":
					return "XonY";
				case "xminusy":
					return "XminusY";
				case "yminusx":
					return "YminusX";
				case "xaccum":
					return "XIdx";		// use the XValue in this series, and accum value in the other
				case "yaccum":
					return "YIdx";

			}
		}

		static $inject = ['$compile', "$scope", "$element", "$timeout"];
		constructor(private $compile: ng.ICompileService
			, public scope: ng.IScope
			, public element
			, public timeout: ng.ITimeoutService) { }

		public echart: echarts.ECharts;
		public eChartInit(echart) {
			this.echart = echart;
			// 'raw' mode
			this.initialiseChart();
			this.setDataset();
		}
		public initialiseChart() {
			this.echart.setOption(this.initialOptions(), true);
		}

		public clickHandler(params) {
			console.log("chartClick");
			console.log(params);
			this.timeout(this.onClick, 0, true, { params });
		}

		public dblClickHandler = (params) => {
			console.log("chartClick");
			console.log(params);
			// invoke the clickHandler & binding, use timeout to get back into angular-land
			this.timeout(this.onDblClick, 0, true, { params });
		}


		// LifeCycle methods

		public $onChanges(changes) {
			//if (this.echart) {
			//	let option = this.echart.getOption();
			//	let series = angular.copy(option.series);	// retrive the current series, and deep copy
			//	option = { series: series };
			//	this.echart.setOption(option);
			//}
			if (this.datasets) {
				this.setDataset();
				return;
			}
			if ((changes.datasets?.currentValue || changes.columnValue) && this.charttype == "column") {
				// we need to resort the data when moving from X Y YonX
				this.sortDataset(this.datasets[0])

			}
			if (changes.datasets?.currentValue || changes.columnValue) {
				this.setDataset();
				return;
			}

			if (changes.colorBy) {
				this.renderColorMap();
			}
		}

		//-----------------------------------------------------------------
		// Accessors into stats dataset (second dataset returned by the stored proc, 1 record only)

		public XAxisLabel() {
			return this.stats.XAxisLabel;
		}
		public YAxisLabel() {
			return this.stats.YAxisLabel;
		}

		public get statsAvg() {
			return this.statXY("avg");
		}
		public get stats() {
			return this.datasets[1][0];
		}

		// get the X or Y value of a parameter (min, max, avg etc) - these are symmetrical in the recordset
		public statXY(x: string, y?: string) {
			if (y === undefined) {
				y = "Y" + x;
				x = "X" + x;
			}
			switch (this.columnValue.toLowerCase()) {
				case "x":
				case "xaccum":
				case "xhist":
					return this.stats[x];
			}
			return this.stats[y];
		}

		////////////////////////////////////////////////////////////////////////////////
		// underlay
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * This sets up the shaded 'wedges' that represent bands of Y/X
		 * 
		 * In ECharts, these are drawn as line charts, with an areaStyle to shade the region
		 * However, you cannot stack lines on a chart without a category axis, 
		 * therefore, we have to create these areas overlapping each other using the z: parameter, biggest are has the 
		 * smallest z, and final white region has the largest z. All regions have opacity 1
		 * Drawing the regions with opacity 1 hides the gridlines associated to each axis, so the z value for the underlay 
		 * start at -20 and increase by 1 for each piece
		 * This means the underlay is behind everything else in the chart
		 * Further, when zoming we need to use filterMode: none; otherwise the areas disappear when the point is out of view
		 */

		/**
		 * Create the series representing the underlay wedges 
		 * @param option - an echarts option to hold the series
		 */
		public drawUnderlays(option) {
			let underlayRatios = [];
			underlayRatios.push([this.stats.Xmax * 2, this.stats.YonXQ3 * this.stats.Xmax * 2]);
			underlayRatios.push([this.stats.Xmax * 2, this.stats.YonXmedian * this.stats.Xmax * 2]);
			underlayRatios.push([this.stats.Xmax * 2, this.stats.YonXQ1 * this.stats.Xmax * 2]);
			let lastSlice = underlayRatios.length - 1; // index of the last slice - it will be white
			let midSlice = Math.floor(underlayRatios.length / 2); // this is where the ones below the midline start
			underlayRatios.forEach((pt, i) => {
				let colorIdx = i < midSlice ? midSlice - (i + 1) : i - midSlice;
				option.series.push({
					name: `slice${i}`,
					type: 'line',
					areaStyle: {
						color: i == lastSlice ? 'white' : ["#EEEEEE", "#F0F0F0", "#FEFEFE", "#FFFFFF"][colorIdx],
						opacity: 1

					},
					lineStyle: {
						width: i == midSlice ? 1 : 0,
						color: 'white',
					},
					encode: {
						x: 0,
						y: 1
					},
					data: [
						[0, 0],
						pt
					],
					animation: false,
					z: -20 + i
				});
			});
		}


		////////////////////////////////////////////////////////////////////////
		// column series
		public drawColumn(option) {
			// use schoolId here, and schNo in the VisualMap echarts bug they cannot be the same field on a bar chart
			let series = {
				name: "column",
				type: 'bar',
				stack: "a",
				datasetIndex: 0,
				encode: {
					y: this.getDataValue(),
					x: "schoolId",
					itemName: "schName",
					itemId: "schNo",

				},
				itemStyle: {
					opacity: (sch) => (sch.schNo == this.selected || this.selected == null) ? 1 : .3
				},
				tooltip: {
					formatter: this.getSchoolToolTipper()
				},
				animation: true,
				animationDuration: 2000,
				animationDelay: 0,
				animationDurationUpdate: 2000,
				colormap: true
			};
			option.series.push(series);
		}

		////////////////////////////////////////////////////////////////////////
		// Gini series
		// 4 series are drawn:
		// accumulation sum of all values strictly less than current value (column)
		// current - current value (column) stacked with accum
		// lorentz curve line joining [0,0] to the top right corner of each current ie this traces current + accum
		// diagonal - stright line joining [0,0] to the top, right
		// Conceptually the Gini coefficent is :
		// ratio of the(area between the diagonal and the lozentz curve) and the area between the diagonal and the XAxis
		// wikipedia offers a formula for a discrete probability distribution:
		// https://en.wikipedia.org/wiki/Gini_coefficient#Discrete_probability_distribution


		public initialOptionsGini() {
			let option: echarts.EChartOption = {

				color: ['#dd6b66', '#759aa0', '#e69d87', '#8dc1a9', '#ea7e53', '#eedd78', '#73a373', '#73b9bc', '#7289ab', '#91ca8c', '#f49f42'],
				title: {},
				tooltip: this.tooltipBase,

				xAxis: [
					{
						type: 'category',
						nameLocation: 'center',
						z: 0,
						splitLine: {
							show: false
						}

					},
					// value xAxis for drawing the diagonal and the lorenz curve
					{
						type: 'category',
						nameLocation: 'center',
						z: 0,
						splitLine: {
							show: false
						},
						min: 0,
						max: this.datasets[0].length,
						axisLabel: {
							show: false
						},
						boundaryGap:false			// make the lines go to the YAxis

					}
				],
				yAxis: {
					type: 'value',
					nameLocation: 'center',
					z: 0,
					splitLine: {
						show: true,
						lineStyle: {
							color: "blue",
							opacity: .2
						}
					}

				},

				series: [
					// accumulation series 
					{
						name: "accum",
						type: 'bar',
						stack: "a",
						XAxisIndex: 0,

						datasetIndex: 0,
						encode: {
							y: this.getAccumValue(),
							x: "schoolId",
							itemName: "schName",
							itemId: "schNo",

						},
						itemStyle: {
							color: "#aaaaaa"
						},
						tooltip: {
							formatter: this.tooltipperAccum
						},
					},

					// current school value
					<any>{
						id: "column",
						name: "column",
						type: 'bar',
						stack: "a",
						XAxisIndex: 0,
						datasetIndex: 0,
						encode: {
							y: this.getDataValue(),
							x: "schoolId",
							itemName: "schName",
							itemId: "schNo",

						},
						itemStyle: {
							opacity: (sch) => (sch.schNo == this.selected || this.selected == null) ? 1 : .3
						},
						tooltip: {
							formatter: this.getSchoolToolTipper()
						},
						colormap: true
					},
					//diagonal line
					{
						id: "diagonal",
						name: "diagonal",
						xAxisIndex: 1,
						type: "line",
						areaStyle: {
							color: "red",
							opacity: .1
						},
						itemStyle: {
							opacity: 0,			// no markers
						},
						datasetIndex: 2,
						encode: {
							y: 1,
							x: 0,
						},
						tooltip: {
							formatter: this.GiniToolTipper
						},
						"animation": false
					},
					// lorenz curve
					{
						"id": "lorenz",
						"name": "lorenz",
						"xAxisIndex": 1,
						"type": "line",
						"stack": "lorenz",
						"datasetIndex": 2,
						"lineStyle": {
							"width": 4,
							"color": "black"
						},
						"itemStyle": {
							"opacity": 0
						},
						"encode": {
							"y": "lorenz",
							"x": 0
						},
						tooltip: {
							formatter: this.GiniToolTipper
						}
					}

				],
				animation: true,
				animationDuration: 0,
				animationDelay: 0,
				animationDurationUpdate: 500,
				visualMap: []
			};
			return option;
		}

		public drawGini(option) {
			// no special handling required
		}


		// tooltipper for the diagonl line on the cumulative charts

		private GiniToolTipper = (params, ticket, callback) => {
			let id = "#schoolScatter_GiniTooltip";

			if (this.tipparams.data[0] != params.data[0]) {

				let tot = this.statXY("sum");
				let size = this.datasets[0].length;
				let accumtot = this.statXY("AccumIncTot")
				let gini = this.statXY("Gini")

				this.tipparams = params;
				this.tipparams.calc = {
					schNo: params.data.schNo,
					tot: tot,
					size: size,
					accumtot: accumtot,
					gini: gini
				}
				this.tipparams = params;
				this.scope.$apply();
			}

			this.timeout(0, false)
				.then(() => {
					let tt = angular.element(document.querySelector(id));
					callback(ticket, tt[0].outerHTML);
				});
			return "Loading...";
		}

		// End Gini chart processing

		private getHistogramData() {
			// get an approximate number of buckets from the FD rule https://en.wikipedia.org/wiki/Freedman%E2%80%93Diaconis_rule
			// 
			let IQR = this.statXY("Q3") - this.statXY("Q1");		// support X historrgram or Y historgram
			let width = 2 * IQR / Math.pow(this.datasets[0].length, 1 / 3);
			width = Math.ceil(width);

			let hist = [];
			let d = width;
			let mx = this.statXY("max");
			while (d < mx) {
				hist.push({ k: d, v: 0 })
				d += width;
			}
			hist.push({ k: d, v: 0 });

			this.datasets[0].forEach(dd => {
				let v = (this.columnValue == "xhist" ? dd.XValue : dd.YValue);
				if (v !== null) {
					let h = _.find(hist, kv => (this.columnValue == "xhist" ? dd.XValue : dd.YValue) <= kv.k);    // x or y options
					if (h) {
						h.v++;
						h[dd[this.colorBy]] = (h[dd[this.colorBy]] ?? 0) + 1;
					}
				};
			});

			let ss  = <string[]>_(this.datasets[0].map(d => d[this.colorBy])).uniq().value();
			let dims = ["k", "v"].concat(ss);
			return {
				dimensions: dims,
				source: hist
			};
		}

		////////////////////////////////////////////////////////////////////////
		// Histogram series
		public drawHistogram(option) {
			// use schoolId here, and schNo in the VisualMap: echarts bug they cannot be the same field on a bar chart

			let colormap = this.colorScaleManager.get(this.colorBy);
			let ss = _(this.datasets[0].map(d => d[this.colorBy])).uniq().value();
			colormap.mapValues(ss);

			ss.forEach(selector => {
				let series: echarts.EChartOption.SeriesPictorialBar = <any>{
					name: selector,
					type: 'bar',
					stack: "hist",
					datasetIndex: 0,
					encode: {
						y: selector,
						x: "k"
					},
					itemStyle: {
						color: colormap.pick(selector)
					},
					tooltip: {
						formatter: this.getHistToolTipper(),
						animation: true,
						animationDuration: 0,
						animationDelay: 0,
						animationDurationUpdate: 500
					}
				};
				option.series.push(series);
			})
		}
		///////////////////////////////////////////////////////////////////////////////
		// Scatter data
		///////////////////////////////////////////////////////////////////////////////
		/**
		 * 
		 */
		public XYSeriesIdx: number;
		public drawScatter(option) {


			let series: echarts.EChartOption.SeriesScatter = <any>{
				id: "xy",
				name: "XY",
				type: 'scatter',
				datasetIndex: 0,
				encode: {
					y: "YValue",
					x: "XValue",
					itemName: "schName",
					itemId: "schNo",

				},
				itemStyle: {
					opacity: (this.selected) ? .3 : 1
				},
				z: 0,
				tooltip: {
					formatter: this.getSchoolToolTipper()
				},
				// there is an item in the scatter chart for each school,
				// regardless of what data settings X Y are selected
				// inbound animation is purely visual - 
				// updates are significant
				animation: true,
				animationDuration: 200,
				animationDelay: 0,
				animationDurationUpdate: 500,
				// not defined by echarts
				colormap: true
			};
			option.series.push(series);
			this.XYSeriesIdx = option.series.length - 1;


			// create a new series if we have a selected school
			// is to account for the z axis, its the only way found to gaurantee the selection is on top

			if (this.selected) {
				series = <any>{
					name: "selected",
					type: 'scatter',
					datasetIndex: 1,
					encode: {
						y: "YValue",
						x: "XValue",
						itemName: "schName",
						itemId: "schNo",

					},
					itemStyle: {
						opacity: 1
					},
					z: 60,
					symbol: "star",
					symbolSize: 20,
					tooltip: {
						formatter: this.getSchoolToolTipper()
					},
					colormap: true
				};
				option.series.push(series);

			}
			// axes maxima:
			// if the X and Y are the same data item (ie different years)
			// then uses the same max on both axes. this is the maxof maxX, maxY
			// Otherwise, allow the axis max to be set independently
			let axis_config = {}
			if (this.stats.XSeries == this.stats.YSeries) {
				let m = Math.max(this.stats.Xmax, this.stats.Ymax);
				axis_config = {
					xAxis: {
						max: m
					},
					yAxis: {
						max: m
					}
				}
			} else {
				axis_config = {
					xAxis: {
						max: this.stats.Xmax

					},
					yAxis: {
						max: this.stats.Ymax
					}
				}
			}
			angular.merge(option, axis_config);

		}
		/**
		 * Marklines
		 * 
		 * The marklines are produced for both X and y data
		 * They have two modes:
		 * -- median mode : shows min, quartile 1, median, quartile 3, maximum value
		 * -- "bell curve mode" show min, 1 std dev below avg, avg, 1 std dev above average, max
		 * 
		 */
		public drawMarklines(option) {
			let marklines = {
				lineStyle: {
					type: 'solid'
				},
				emphasis: {
					lineStyle: {
						type: 'solid',
						color: "black"
					}
				},
				tooltip: {
					formatter: this.marklineToolTipper,
					position: (point, params) => {
						if (["yMin", "y1", "yMid", "y3", "yMax"].indexOf(params.data.id) >= 0) {
							return [0, 200];
						};
						return [200, 0];
					}
				},
				data: [
					{
						id: "xMin",
						xAxis: this.datasets[1][0].Xmin
					},
					{
						id: "x1",
						xAxis: this.datasets[1][0].XQ1
					},
					{
						id: "xMid",
						xAxis: this.datasets[1][0].Xmedian
					},
					{
						id: "x3",
						xAxis: this.datasets[1][0].XQ3
					},

					{
						id: "xMax",
						xAxis: this.datasets[1][0].Xmax
					},
					{
						id: "yMin",
						yAxis: this.datasets[1][0].Ymin
					},
					{
						id: "y1",
						yAxis: this.datasets[1][0].YQ1
					},
					{
						id: "yMid",
						yAxis: this.datasets[1][0].Ymedian
					},
					{
						id: "y3",
						yAxis: this.datasets[1][0].YQ3
					},

					{
						id: "yMax",
						yAxis: this.datasets[1][0].Ymax
					},
				]
			}
			this.getSeries(option, "XY").markLine = marklines;
		}

		private getSeries(option, name) {
			if (!option.series) {
				return undefined;
			}
			if (Array.isArray(option.series)) {
				return _.find(option.series, { name: name });
			}
			if (option.series.name == name) {
				return option.series;
			}
			return null;
		}

		// visual mappings
		//-----------------------------------------------
		/**
		 * As well as X and Y, color, shape and size may be mapped to data fields
		 * Color we treat in 2 ways 
		 * 1) partioning on a data field such as district or 
		 * 2) highlighting an individual school
		 * 
		 */
		////////////////////////////////////////////////////////////////////////////////////////////
		// Palette Management
		////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * A 'palette'; ie an array of color values, can be supplied by bindings in various ways as described 
		 * We want to define a permanent binding between a data item and a value in this array
		 * 
		 */


		public colorScaleManager = new ColorScaleManager();
		// this is d3.category10
		public palette: string[] = [
			"#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"
		];
		public colorItems: {
			[provider: string]: string[]
		} = {};

		/**
		 * Used for NULL value in Row or Column 
		 */

		public colormapTemplate = () => {
			return {
				id: "colormap",
				type: 'piecewise',
				show: true,
				dimension: this.colorBy,

				inRange: {
					color: this.palette,
				},
				outOfRange: {
					color: 'lightgrey'
				}
			}
		}

		public symbolmapTemplate = {
			id: "symbolmap",
			type: 'piecewise',
			show: false,
			dimension: "AuthorityGroup",
			inRange: {
				symbol: ["square", "circle", "triangle", "star"]
			},
			outOfRange: {
				symbol: "circle"
			}
		}

		public selectedMap: () => echarts.VisualMap.Piecewise = () => {
			return {
				id: "selectionmap",
				type: 'piecewise',
				show: false,
				inRange: {
					symbolSize: 20,
					opacity: 1
				},
				dimension: "schNo",
				categories: [this.selected],
				outOfRange: {
					opacity: .3
				}
			}
		}

		public setColorMap(newColorBy) {
			this.colorBy = newColorBy;
			this.renderColorMap();
		}

		public renderColorMap() {
			let option: echarts.EChartOption = {};
			this.colorMap(option);
			this.echart.setOption(option);
		}

		// apply the colormap as a visual map to all series
		public colorMap(option) {
			let idx = [];

			option.series.forEach((s, i) => {
				if (s.colormap == true) {
					idx.push(i);
				}
			});
			let template;
			if (idx.length) {
				template = { seriesIndex: idx };
			}
			let newItems = this.datasets[0].map(d => d[this.colorBy]);
			let vm = this.colorScaleManager
				.get(this.colorBy)
				.mapValues(newItems)
				.applyColorMap(option, template);
		}


		public axisLabels(option) {
			option.xAxis.name = this.XAxisLabel();
			option.yAxis.name = this.YAxisLabel();
		}

		//---------------------------------------------------------------------
		// tool tips
		// echarts lets you supply a function to render tooltips. te function is passed params object
		// containing all the current context, and constructs the tooltip html. This html is passed backed by calling the supplied
		// callback function.
		// Usually such a tooltip function uses conventional string manipulation to contruct the tooltip html.
		// Here we use a different technique:
		// To make it more readable and editable, the tooltip html is a <div> in the component's html (ng-show=false).
		// this component can use angular bindings to incorporate the pass parameters or other data.
		// The tooltipper function simply does $scope.apply to ensure that all the bindings in the tooltip <div> are up-to-date
		// then passes back the OuterHtml of that div as the tooltip string


		public tipparams = {
			data: { schNo: null },
			calc: {}
		};

		// generic tooltip from an Html Id - bound to vm.tipparams


		public getToolTipper = (tooltipDivId: string, tag?: string) => {
			let tt = (params, ticket, callback) => {

				// $hashKey can get added by ng-repeat, and will prevent isEqual from working as expected
				if (this.tipparams.hasOwnProperty('$$hashKey')) {  
					delete this.tipparams["$$hashKey"];
				}
				if (tag) {
					params.tag = tag;
				}
				if (!_.isEqual(this.tipparams ,params)) {
					this.tipparams = params;
					this.scope.$apply();
				}

				this.timeout(0, false)
					.then(() => {
						let tt = angular.element(document.querySelector('#' + tooltipDivId));
						callback(ticket, tt[0].outerHTML);
					});
				return "Loading";
			}
			return tt;				// returns the configured tooltipper function
		}
		// default School tooltip can use getToolTipper
		public getSchoolToolTipper = () => {
			return this.getToolTipper('schoolScatter_schoolTooltip','school');
		}

		// tooltip for accumulations
		private tooltipperAccum = (params, ticket, callback) => {

			let id = '#schoolScatter_AccumTooltip';
			if (this.tipparams.data.schNo != params.data.schNo) {


				// set up all the calculated values


				let tot = (this.columnValue == "xaccum") ? this.stats.Xsum : this.stats.Ysum;
				let size = this.datasets[0].length;
				let rank = size - params.data[this.getSortValue()] + 1;
				let accum = params.data[this.getAccumValue()];
				let bottompercent = (accum / tot);
				let toppercent = 1 - bottompercent;

				let rankpercent = rank / size;

				this.tipparams = params;
				this.tipparams.calc = {
					schNo: params.data.schNo,
					tot: tot,
					size: size,
					rank: rank,
					accum: accum,
					bottompercent: bottompercent,
					toppercent: toppercent,
					rankpercent: rankpercent
				}
				this.scope.$apply();
			}

			this.timeout(0, false)
				.then(() => {
					let tt = angular.element(document.querySelector(id));
					callback(ticket, tt[0].outerHTML);
				});
			return "Loading";
		}
		// make the tooltipper function for histogram
		// pass the tag 'hist'
		private getHistToolTipper = () => {
			return this.getToolTipper('schoolScatter_HistTooltip','hist')
	
		}

		// markline tooltipper

		private marklineToolTipper = (params, ticket, callback) => {
			let id = (["yMin", "y1", "yMid", "y3", "yMax"].indexOf(params.data.id) >= 0) ?
				"#schoolScatter_MarklineYTooltip" :
				"#schoolScatter_MarklineXTooltip"

			params.tag = 'markline';
			if (!_.isEqual(this.tipparams, params)) {
				this.tipparams = params;
				this.scope.$apply();
			}

			this.timeout(0, false)
				.then(() => {
					let tt = angular.element(document.querySelector(id));
					callback(ticket, tt[0].outerHTML);
				});
			return "Loading...";
		}

		// base tooltipper when initialising a chart type
		// the particular formatters are set in the tooltip property of each series
		// declared as any becuase the d,ts file has an incorrect definition for renderMode
		// css properties are applied to the 'floating layer'; ie a div that houses the tooltip
		private tooltipBase = {
			extraCssText: 'box-shadow: 0 0 8px rgba(0, 0, 0, 0.5);border-syle: none; padding: 0',
			borderColor: '#F0F0F0',
			borderWidth: 1,
			backgroundColor: 'white',
			//renderMode: 'html',
			textStyle: {
				fontFamily: 'Roboto',
				fontSize: 12
			},
			showDelay: 500,
			hideDelay: 1000,
			transitionDuration: 0,
			enterable: true,
		}

		/////////////////////////////////////////////////////////////////////////
		// Initialisation
		//////////////////
		public initialOptions() {
			switch (this.charttype) {
				case "scatter":
					return this.initialOptionsScatter();
				case "gini":
					return this.initialOptionsGini();
				default:
					return this.initialOptionsColumn();
			}
		}
		public initialOptionsScatter() {
			let option: echarts.EChartOption = {

				color: ['#dd6b66', '#759aa0', '#e69d87', '#8dc1a9', '#ea7e53', '#eedd78', '#73a373', '#73b9bc', '#7289ab', '#91ca8c', '#f49f42'],
				title: {},
				tooltip: this.tooltipBase,
				grid: [
					{ left: '7%', top: '10%', width: '85%', height: '80%' },		// leave room for zoom with 80% height
					{ right: '7%', top: '0%', width: '85%', height: '5%' }
				],
				xAxis: [{
					gridIndex: 0,
					type: "value",
					nameLocation: "center",
					nameGap: 20,
					z: 0,
					splitLine: {
						show: true,
						lineStyle: {
							color: "blue",
							opacity: 0.2
						}
					},
					axisLabel: {
						fontSize: 8
					},
					name: this.XAxisLabel(),

					max: this.stats.Xmax
				},
				{
					gridIndex: 1,
					"type": "value",
					"nameLocation": "end",
					"z": 0,
					"splitLine": {
						"show": false,
						"lineStyle": {
							"color": "blue",
							"opacity": 0.2
						}
					},
					axisLabel: { show: false },
					axisLine: {
						show: false
					},
					name: "Correlation",
					max: 1,
					min: -1
				}],
				yAxis: [{
					gridIndex: 0,
					type: "value",
					nameLocation: "center",
					nameGap: 40,
					"z": 0,
					"splitLine": {
						"show": true,
						"lineStyle": {
							"color": "blue",
							"opacity": 0.2
						}
					},
					axisLabel: {
						fontSize: 8
					},
					name: this.YAxisLabel(),
					max: this.stats.Ymax
				},
				{
					gridIndex: 1,
					"type": "category",
					"z": 0,
					"splitLine": {
						"show": false,
						"lineStyle": {
							"color": "blue",
							"opacity": 0.2
						}
					},
				}],
				// toolbox to provide zoom interaction
				toolbox: {
					show: true,
					right: 0,
					bottom: 0,
					feature: {
						dataZoom: {
							title: {
								zoom: "Zoom mode",
								back: "back"
							},
							xAxisIndex: 0,
							yAxisIndex: 0
						}
					}
				},
				// have to use an array - its an error in the .d.ts file
				dataZoom: [
					{
						type: "slider",

						xAxisIndex: 0,

						bottom: 0,
						borderColor: "#d0d0d0",
						backgroundColor: "#f0f0f0",

						fillerColor: "rgba(210, 240, 255)",
						handleSize: "90%",
						handleStyle: {
							color: "black",
							opacity: .5

						},
						showDataShadow: <any>false,			// definition problem
						filterMode: "none"
					},
					{
						type: "slider",

						yAxisIndex: 0,
						right: 0,
						borderColor: "#d0d0d0",
						backgroundColor: "#f0f0f0",

						fillerColor: "rgba(210, 240, 255)",
						handleSize: "90%",
						handleStyle: {
							color: "black",
							opacity: .5

						},
						showDataShadow: <any>false,
						filterMode: "none"

					}
				],
				series: [
					<any>{
						name: "correlation",
						type: "bar",
						xAxisIndex: 1,
						yAxisIndex: 1,
						"areaStyle": {
							"color": "#EEEEEE",
							"opacity": 1
						},
						itemLabel: {},
						lineStyle: {
							width: 0,
							color: "white"
						},
						data: [this.stats.correlation],
						showBackground: true,
						tooltip: {
							formatter: this.getToolTipper("schoolScatter_CorrelationTooltip")
						}
					}],
				animation: false,
				visualMap: []
			};
			return option;
		}

		public initialOptionsColumn() {
			let option: echarts.EChartOption = {

				color: ['#dd6b66', '#759aa0', '#e69d87', '#8dc1a9', '#ea7e53', '#eedd78', '#73a373', '#73b9bc', '#7289ab', '#91ca8c', '#f49f42'],
				title: {},
				tooltip: this.tooltipBase,
				xAxis: [
					{
						type: 'category',
						nameLocation: 'center',
						z: 0,
						splitLine: {
							show: true,
							lineStyle: {
								color: "blue",
								opacity: .2
							}
						}

					}],
				yAxis: {
					type: 'value',
					nameLocation: 'center',
					z: 0,
					splitLine: {
						show: true,
						lineStyle: {
							color: "blue",
							opacity: .2
						}
					}
				},

				series: [],
				animation: true,
				animationDuration: 0,
				animationDelay: 0,
				animationDurationUpdate: 500,
				visualMap: []
			};
			return option;
		}


		//////////////////////////////////////////////////////////////////////////////////////////////
		// Data Manipulations
		//////////////////////////////////////////////////////////////////////////////////////////////
		public setDataset() {
			//console.log(this.datasets);
			if (this.datasets == undefined) {
				return;
			}
			this.sortDataset(this.datasets[0]);
			let option: echarts.EChartOption = this.initialOptions();
			let d: Array<any> = this.datasets[0];
			let highlight = _.filter(d, ({ schNo }) => schNo == this.selected);

			switch (this.charttype) {

				case "scatter":
					option.dataset = [{
						source: d
					},
					{
						source: highlight
					}
					];
					this.drawUnderlays(option);
					this.axisLabels(option);
					this.drawScatter(option);
					this.drawMarklines(option);

					break;
				case "column":
					option.dataset = [{
						source: d
					},
					{
						source: highlight
					}
					];
					this.drawColumn(option);

					break;
				case "gini":
					// these are the points of the diagonal line and lorenz curve
					let diagincr = this.statsAvg;
					let idcol = (this.columnValue == "xaccum") ? "XIdx" : "YIdx";
					let inccol = (this.columnValue == "xaccum") ? "XAccumInc" : "YAccumInc";
					let diag = d.map((dt, idx) => {
						return [
							dt[idcol],
							dt[idcol] * diagincr,
							dt[inccol],			// duplicat all these becuase of echart bugs?
							dt[idcol] * diagincr - dt[inccol],
							dt[idcol]
						];
					});
					// initial point for 0 
					diag.sort((a, b) => a[0] - b[0]);
					diag = [[0, 0, 0, 0, 0]].concat(diag);

					option.dataset = [{
						source: d
					},
					{
						source: highlight
					},
					{
						dimensions: ["id", "diag", "lorenz","diff","id2"],
						source: diag
					}
					];
					this.drawGini(option);
					break;
				case "hist":
					option = this.initialOptionsColumn();
					option.dataset = [
						this.getHistogramData()
					];
					this.drawHistogram(option);
					break;

			}
			switch (this.charttype) {				case "hist":
					break;
				default:
					this.colorMap(option);
					if (this.selected) {
						option.visualMap.push(this.selectedMap());
					}
			}
			this.timeout(() => { this.echart.setOption(option, true); }, 0);
			this.tipparams = {
				data: { schNo: null },
				calc: {}
			};
		}

		//------------------------------------------------------------------
		// Sorting
		//------------------------------------------------------------------
		/**
		 * Support sorting ascending or descending, or either the data point name (ie the ROW_ID)
		 * or the value
		 */

		public sortDescending: boolean = false;
		public sortOn = "value";		// id or value

		private comparator = (a, b) => (a == b) ? 0 :
			(this.sortDescending ? (a < b ? 1 : -1) : (a < b ? -1 : 1));

		private idSort = (a, b) => {

			return this.comparator(a["schNo"], b["schNo"]);
		}

		/**
		 * Value sort
		 * 
		 */
		private valueSort = (a, b) => {

			return this.comparator(a[this.getSortValue()], b[this.getSortValue()]);
		}

		private selectedSort = (a, b) => {
			let a1 = (a.schNo == this.selected) ? 1 : 0;
			let b1 = (b.schNo == this.selected) ? 1 : 0;

			return this.comparator(a1, b1);
		}

		public sortDataset(out) {
			//	always sort scatter ascending on PtSelected
			let sortFunction = (this.charttype == "scatter") ? this.selectedSort :
				this.sortOn == "id" ? this.idSort : this.valueSort;

			out.sort(sortFunction);
		}

		public applySort() {
			this.sortDataset(this.datasets[0]);
			this.echart.setOption({
				dataset: {
					source: this.datasets[0]
				},
			});
		}


		public toggleSortDirection() {
			this.sortDescending = !this.sortDescending;
			this.applySort();
		}

		public toggleSortOn() {
			this.sortOn = this.sortOn == "id" ? "value" : "id";
			this.applySort();
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			datasets: "<",      // the 3 datasets returned by the scatterdata API
			colorBy: "<",
			colorByValue: "<",
			statsBy: "<",				// median or mode
			columnValue: "@",					  // scatter or x y yonx etc
			selected: "<",							// a selected school to highlight

			onClick: "&clickHandler",
			onDblClick: "&dblclickHandler",
		};
		public controller = Controller;
		public controllerAs = "vm";;
		public templateUrl = "schoolscatter/chartcore";

	}

	angular
		.module("pineapples")
		.component("schoolScatterChart", new Component());


	export class ColorScale {

		private mappings: any[];
		private palette: string | string[];
		constructor(public name: string) {
			this.mappings = [];
		}

		public setPalette(palette: string | string[]) {
			this.palette = palette;
			return this;
		}
		public mapValues(values: any[]) {
			this.mappings = _(this.mappings.concat(values)).uniq().value();
			return this;
		}


		public pick(value) {
			let idx = this.mappings.indexOf(value);
			return this.resolvePalette()[idx];
		}

		public colorMap(template?) {
			let defaults = {
				id: "colormap",
				type: 'piecewise',
				show: true,
				inRange: {
					color: this.resolvePalette(),
				},
				outOfRange: {
					color: 'lightgrey'
				}
			}
			if (template) {
				defaults = angular.merge(defaults, template);
			}
			return angular.merge(defaults, {
				dimension: this.name,
				categories: this.mappings,
			});
		}

		private resolvePalette() {
			if (Array.isArray(this.palette)) {
				return this.palette;
			}
			// d3 cases 
			switch (this.palette) {
				case "category10":
					return ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"];
			}
		}

		public applyColorMap(option, template?) {
			let colormap = this.colorMap(template);
			option.visualMap = option.visualMap || [];
			option.visualMap.push(colormap);

		}
	}

	export class ColorScaleManager {


		defaultPalette: string | string[] = "category10";

		constructor(defaultPalette?) {
			this.defaultPalette = defaultPalette || "category10";
		}
		private scales: {
			[provider: string]: ColorScale
		} = {};
		public get(scalename: string) {
			return this.scales[scalename] = this.scales[scalename] ||
				(new ColorScale(scalename)).setPalette(this.defaultPalette);
		}

	}
}
