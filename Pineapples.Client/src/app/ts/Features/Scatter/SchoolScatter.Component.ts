﻿namespace Pineapples.Dashboards {

	/* Corresponds to the legacy Pineapples X Y Charts */

	
	class Controller {
		public theData: any;    // the data that is used to build the scatter chart
		public resultset: any;
		public isWaiting: boolean;
		public colorBy: string = "District";
		public columnValue: string = "scatter";

		public search: any;

		public error;


		// data that is sent to the echart
		private _scatterData: (any[])[];
		public get scatterData(): (any[])[] {
			return this._scatterData;
		}
		public set scatterData(dt: (any[])[]) {
			// Recordset 1 holds the statistics;
			// in particular YMax and XMax
			// if either of these is null, we dont have a dataset to draw
			if (dt[1][0].Ymax == null || dt[1][0].Xmax == null) {
				this._scatterData = [];
				return;
			}
			this._scatterData = dt;
		}

		static $inject = ["$http","Lookups","$state"];
		constructor(public http: ng.IHttpService, public lookups: Sw.Lookups.LookupService, public state:ng.ui.IStateService) { }

		public dataItems = [
			{
				name: "Enrolment",
				argPrompt: "Level", argType: "levels", group: "", qualityItem: "Pupils", qualitySubItem: "Enrolment"
			},
			//{
			//	name: "Exam Average Result",
			//	argPrompt: "Exam", argType: "ExamType", group: "Exams"
			//},
			//{
			//	name: "Pupil Teacher Ratio",
			//	qualityItem: "Teachers"
			//},
			{
				name: "Repeaters",
				arg2: "", argPrompt: "Level", argType: "levels", group: "", qualityItem: "Pupils", qualitySubItem: "REP"
			},
			{
				name: "Preschool Attenders ",
				arg2: "", argPrompt: "Level", argType: "levels", group: "", qualityItem: "Pupils", qualitySubItem: "REP"
			},
			{
				name: "Enrolment (Male)",
				argPrompt: "Level", argType: "levels", group: "", qualityItem: "Pupils", qualitySubItem: "Enrolment"
			},
			{
				name: "Enrolment (Female)",
				argPrompt: "Level", argType: "levels", group: "", qualityItem: "Pupils", qualitySubItem: "Enrolment"
			},
			{
				name: "All Staff",
				arg2: "Q", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "All Staff Qualified",
				arg2: "NumStaffQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "All Staff Certified",
				arg2: "NumStaffC", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "All Staff Cert/Qual",
				arg2: "NumStaffCQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			//--teachers
			// ---- ftpt
			{
				name: "Teaching Staff (FTPT)",
				arg2: "TeachingFTPT", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTPT) Qualified",
				arg2: "TeachingFTPTQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTPT) Certified",
				arg2: "TeachingFTPTC", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTPT) Cert/Qual",
				arg2: "TeachingFTPTCQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTPT) Qualified %",
				arg2: "TeachingFTPTQ%", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTPT) Certified %",
				arg2: "TeachingFTPTC%", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTPT) Cert/Qual %",
				arg2: "TeachingFTPTCQ%", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTE)",
				arg2: "TeachingFTPT", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTE) Qualified",
				arg2: "TeachingFTPTQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTE) Certified",
				arg2: "TeachingFTPTC", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTE) Cert/Qual",
				arg2: "TeachingFTPTCQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTE) Qualified %",
				arg2: "TeachingFTPTQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTE) Certified %",
				arg2: "TeachingFTPTC", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Teaching Staff (FTE) Cert/Qual %",
				arg2: "TeachingFTPTCQ", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Non-Teaching Staff (FTPT)",
				arg2: "TeachingFTPT", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Non-Teaching Staff (FTE)",
				arg2: "TeachingFTPT", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Non-Teaching Staff (FTE) %",
				arg2: "TeachingFTPT%", argPrompt: "Level", argType: "levels",
				group: "Teachers", qualityItem: "Teachers"
			},
			{
				name: "Accreditation Level",
				arg2: "", argPrompt: null, argType: null,
				group: "Accreditation", qualityItem: ""
			},
			{
				name: "Exam Candidates At Risk",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			},
			{
				name: "Exam Candidates At Risk %",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			}
			,
			{
				name: "Exam Candidates Not Competent",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			},
			{
				name: "Exam Candidates Not Competent %",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			},
			{
				name: "Exam Candidates Competent",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			},
			{
				name: "Exam Candidates Competent %",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			},
			{
				name: "Exam Candidates Exceeding Standard",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			},
			{
				name: "Exam Candidates Exceeding Standard %",
				arg2: "", argPrompt: "Exam", argType: "examTypes",
				group: "Exams", qualityItem: ""
			},
			{
				name: "Toilets - Usable",
				arg2: "", argPrompt: null, argType: null,
				group: "WASH", qualityItem: ""
			},
			{
				name: "Toilets, Boys - Usable",
				arg2: "", argPrompt: null, argType: null,
				group: "WASH", qualityItem: ""
			},
			{
				name: "Toilets, Girls - Usable",
				arg2: "", argPrompt: null, argType: null,
				group: "WASH", qualityItem: ""
			},
			{
				name: "Toilets - Total",
				arg2: "", argPrompt: null, argType: null,
				group: "WASH", qualityItem: ""
			},
			{
				name: "Toilets, Boys - Total",
				arg2: "", argPrompt: null, argType: null,
				group: "WASH", qualityItem: ""
			},
			{
				name: "Toilets, Girls - Total",
				arg2: "", argPrompt: null, argType: null,
				group: "WASH", qualityItem: ""
			}
		];

		
		public yearOffsets = [
			{ C: -5, N: "5 Years Ago" },
			{ C: -4, N: "4 Years Ago" },
			{ C: -3, N: "3 Years Ago" },
			{ C: -2, N: "2 Years Ago" },
			{ C: -1, N: "1 Year Ago" },
			{ C: 0, N: "Base Year" },
			{ C: 1, N: "1 Year Later" },
			{ C: 2, N: "2 Years Later" },
			{ C: 3, N: "3 Years Later" },
			{ C: 4, N: "4 Years Later" },
			{ C: 5, N: "5 Years Later" },
		];

		public setXSeries() {
			var def = _.find(this.dataItems, ({ name }) => name == this.search.XSeries);
			this.XArgs = this.lookups.cache[def.argType];
		}

		public setYSeries() {
			var def = _.find(this.dataItems, ({ name }) => name == this.search.YSeries);
			this.YArgs = this.lookups.cache[def.argType];
		}

		public XArgs;
		public YArgs;

		// maybe we can get this from 
		public chartSpecs = [
			{
				name: "Enrolment Growth",
				search: {
					XSeries: "Enrolment",
					XSeriesOffset: -1,
					YSeries: "Enrolment",
					YSeriesOffset: 0
				}
			},
			{
				name: "Pupil Teacher Ratio",
				search: {
					XSeries: "Enrolment",
					XSeriesOffset: 0,
					YSeries: "Teaching Staff (FTE)",
					YSeriesOffset: 0
				}
			},
			{
				name: "Pupil / Qualified Teacher Ratio",
				search: {
					XSeries: "Enrolment",
					XSeriesOffset: 0,
					YSeries: "Teaching Staff (FTE) Qualified",
					YSeriesOffset: 0
				}
			}
		]
		public chartSpec: {
			name; search
		}
		public changeChartSpec() {
			angular.merge(this.search, this.chartSpec.search);  // Base year is preserved
			this.setXSeries();
			this.setYSeries();
			this.getData();
		}

		public flipXY() {
			let flip = angular.copy(this.search);
			this.search.XSeries = flip.YSeries;
			this.search.XSeriesOffset = flip.YSeriesOffset;
			this.search.XFilter = flip.YFilter;

			this.search.YSeries = flip.XSeries;
			this.search.YSeriesOffset = flip.XSeriesOffset;
			this.search.YFilter = flip.XFilter;
			this.getData();
		}
		public colorByOptions = [
			["District","District"],
			["AuthorityGroup","Govt / Non-govt"],
			["authName","Authority"],
			["stDescription", "School Type"],
			["Region","Region"]
		]

		public $onChanges(changes) {
			if (changes.search) {
				this.getData();
			}
		}

		private _selectedSchool;
		public set selectedSchool(value) {
			if (value == null) {
				this._selectedSchool = null;
				return;
			}
			if (this.lookups.byCode("schoolCodes", value)) {
				if (value != this._selectedSchool) {
					this._selectedSchool = value;
				}
			}
		}
		public get selectedSchool() {
			return this._selectedSchool;
		}

		public getData() {
			// we have to have values for base year, xSeries and YSeries
			//
			this.search.xArg2 = _.find(this.dataItems, { name: this.search.XSeries })["arg2"]||null;
			this.search.yArg2 = _.find(this.dataItems, { name: this.search.YSeries })["arg2"] || null;
			this.isWaiting = true;
			this.http.post("api/schoolscatter/getscatter", this.search)
				.then(response => {
					this.isWaiting = false;
					this.error = null;
					this.scatterData = response.data["ResultSet"];
				},
				// error response
					errorResp => {
						this.isWaiting = false;
						this._scatterData = [];
						this.error = errorResp.data;

					}
			);
		}

		//---------------------------------------------------------------------------
		// event handlers
		
		public dblClickHandler(params) {
			console.log(params);
		}

		public clickHandler(params) {
			console.log(params);
			let clickedSchool = params.data.schNo;
			if (clickedSchool == this.selectedSchool) {
				this.selectedSchool = null;
			} else {
				this.selectedSchool = clickedSchool;
			}
			
		}

		public showSelectedSchool() {
			if (this.selectedSchool) {
				this.showSchool(this.selectedSchool);
			}
		}

		public showSchool(schNo: string) {
			if (schNo) {
				this.isWaiting = true;
				this.state.go("site.schools.list.item", { id: schNo })
			}
		}

	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				search: "<"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "schoolscatter/chart";
		}
	}

	angular
		.module("pineapples")
		.component("componentSchoolScatter", new Component());

}