﻿namespace Pineapples.Dashboards {

    var routes = function ($stateProvider) {
        $stateProvider
          .state('site.xycharts', {
              url: '/xycharts',

              views: {
                  //"navbar@": {
                  //    templateUrl: "schoolscatter/searcher",
                  //    controller: "FilterController",
                  //    controllerAs: "vm"
                  //},
                  "@": "componentSchoolScatter"
              },
						resolve: {
							search: ["Lookups", (lookups: Sw.Lookups.LookupService) => ({
								BaseYear: lookups.cache["warehouseYears"][0].C,
								XSeries: "Enrolment",
								XSeriesOffset: 0,
								YSeries: "Enrolment",
								YSeriesOffset: -1
              })],
              pageTitle: Sw.Utils.RouteHelper.featureTitle("Scatter Chart")
            }

          });

    }

    angular
        .module('pineapples')
        .config(['$stateProvider', routes])

}