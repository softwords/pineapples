﻿namespace Pineapples.Dashboards {

	// TODO Can this be moved to a common place for use in various exams dashboards, etc.
	// this is the fields that come back in the source data table;
	// ie this directly matches the column names of the underlying warehouse view
	interface IXfData {
		examID,
		examCode,
		examYear,
		examName,
		StateID,
		State,
		Gender,
		standardID,
		standardCode,
		standardDesc,
		benchmarkID,
		benchmarkCode,
		benchmarkDesc,
		achievementLevel,
		achievementDesc,
		Candidates,
		DistrictCode,
		District
	}

	export class SchoolExamsDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

		static $inject = ["$rootScope", "$mdDialog", "$state", "$http", "$q",
			"$timeout", "$anchorScroll", "Lookups", "reflator"];
		constructor(rootScope: Sw.IRootScopeEx
			, public mdDialog: ng.material.IDialogService
			, protected $state: ng.ui.IStateService
			, public http: ng.IHttpService
			, public $q: ng.IQService
			, public $timeout: ng.ITimeoutService
			, public $anchorScroll: ng.IAnchorScrollService
			, public lookups: Sw.Lookups.LookupService
			, public reflator: Sw.Api.IReflator
		) {
			super(rootScope, mdDialog, $state, http, $q, $timeout, $anchorScroll, lookups, reflator);
			this.options = new examOptions(lookups, rootScope.hub);
		}

		// bindings
		public school: Pineapples.Schools.School;

		// options here is *not* a binding like with global dashboards, examOptions is
		// instead instantiated in the constructor with only year and exams properties
		// in use but gaining the onOptionChange mechanism that comes with it. This then
		// becomes important for all dashboard child components
		public options: examOptions;
		public examsSchoolResults: any;
		public examsSchoolResultsUnnormalized: any[];

		public palettes = {
			default: ['#1010FF', '#223344', '#096723', '#123456'],
			// for STA, note the order of the colors is changed, becuase the first 2 are 
			// multipplied by -1 to get them left of the origin
			sta: ["#FF0000", "#FFC000", "#92D050", "#00B050"]
		}

		// xf is defined in the parent class CrossfilterDashboard, but not typed
		// redfining it based on the type that describes the import data gives more intellisense and 
		// comppile errors to catch typos.
		public xf: CrossFilter.CrossFilter<IXfData>;
		public xFilter: Sw.xFilter.XFilter;

		public dimensions: { [key: string]: CrossFilter.Dimension<IXfData, any> } = {};
		public groups: { [key: string]: CrossFilter.Group<IXfData, any, any>; } = {};

		// dimensions and groups based on exam data
		public dimYear: CrossFilter.Dimension<any, number>;
		public dimExamCode: CrossFilter.Dimension<any, string>;
		public dimStandardCode: CrossFilter.Dimension<any, string>;
		public dimBenchmarkCode: CrossFilter.Dimension<any, any[]>;

		public grpYear: CrossFilter.Group<any, string, any>;
		public grpExamCode: CrossFilter.Group<any, string, any>;
		public grpBenchmarkCode: CrossFilter.Group<any, string, any>;

		public standardsByExamYear;
		public benchmarksByExamYear;
		public achievementLevelsByExamYear;

		public schoolNo() {
			return this.school._id();
		}

		public lastSurveyYear = () => this.school.LatestSurvey.svyYear;
		public lastWarehouseYear = () => this.school.LatestSurvey.svyYear;  // this will get updated when actual warehouse data is available

		public surveyYearArray = (numYears: number) => _.range(this.lastWarehouseYear() - numYears + 1, this.lastWarehouseYear() + 1);
		// flow data is always one year behind the current survey
		public lastExamYear = _.memoize(() => _(this.school.Exams)
			.map(({ ExamYear }) => ExamYear)
			.max()
		);

		public dataExamResultsByBenchmarkForYear = _.memoize((year, exam) => {

			const tPercent = (level, sign) => x => ({
				ExamBenchmark: x['benchmarkDesc'] + ' - T',
				percent: Math.round((x[`${level}F`] + x[`${level}M`]) * sign * 1000 / (x['CandidatesF'] + x['CandidatesM']) / 10)
			})

			const mPercent = (level, sign) => x => ({
				ExamBenchmark: x['benchmarkDesc'] + ' - M',
				percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
			})

			const fPercent = (level, sign) => x => ({
				ExamBenchmark: x['benchmarkDesc'] + ' - F',
				percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
			})

			const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

			const data = this.examsSchoolResults
				.filter(x => x.examYear === year)
				.filter(x => x.examName === exam);

			const groupedData = _.groupBy(data, ({ standardDesc }) => standardDesc)

			var examsData = _.mapValues(groupedData, data =>
				[
					{
						data: _.flatten(data.map(genderPercent('Approaching competence', -1))),
						color: '#FFC000',
						meta: 'Approaching competence'
					},
					{
						data: _.flatten(data.map(genderPercent('Well below competent', -1))),
						color: '#FF0000',
						meta: 'Well below competent'
					},
					{
						data: _.flatten(data.map(genderPercent('Minimally competent', 1))),
						color: '#92D050',
						meta: 'Minimally competent'
					},
					{
						data: _.flatten(data.map(genderPercent('Competent', 1))),
						color: '#00B050',
						meta: 'Competent'
					}
				])
			return examsData;
		},
			(year, exam) => [year, exam].toString());

		public exams;

		public standardsForExam;

		//----------------------- Life Cycle hooks ----------------------------------
		public $onInit() {
			// While we can collect data onInit we can not rely on data arriving in time
			// to be part of the whole life cycle of dashboard->dashboard child components
			// like we have with the global dashboards (e.g. Schools, Teachers, etc.)
			// Instead with individual entity dashboards (e.g. School, Teacher) we collect data and 
			// check for in in the onChanges of dashboard childs
			this._collectData(this.schoolNo());
		}

		public $onChanges(changes) {
			// a dashboard child should always call super in here,
			// or else onOptionChange will never fire
			super.$onChanges(changes);
		}
		//----------------------- Life Cycle hooks ----------------------------------

		// ------------------------------ Crossfilter support ----------------------------
		public makeCrossfilter() {
			this.xf = crossfilter(this.examsSchoolResultsUnnormalized);

			this.dimensions["examYear"] = this.xf.dimension(d => d.examYear);
			this.dimensions["achievementLevel"] = this.xf.dimension(d => d.achievementLevel);

			this.groups["achievementLevel"] = this.xFilter.xReduce(this.dimensions.achievementLevel
				, "achievementLevel"
				, this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));

			this.dimYear = this.xf.dimension(d => d.examYear);
			this.dimExamCode = this.xf.dimension(d => d.examCode);
			this.dimStandardCode = this.xf.dimension(d => d.standardCode);
			this.dimBenchmarkCode = this.xf.dimension(d => [d.benchmarkID, d.benchmarkCode, d.benchmarkDesc]);

			this.grpYear = this.xFilter.xReduce(this.dimYear
				, "achievementLevel", this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));
			this.grpExamCode = this.xFilter.xReduce(this.dimExamCode
				, "achievementLevel", this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));
			this.grpBenchmarkCode = this.xFilter.xReduce(this.dimBenchmarkCode
				, "achievementLevel", this.xFilter.getFlattenedGenderAccessor("Gender", "Candidates"));

			this.makeLookups();
		}

		// ------------------------------ Crossfilter support ----------------------------

		// crossfilter dashboards will need to implement this, so that the options are applied as filters to the 
		// cross tab dimensions 
		// any implementation should call super implementation
		public onOptionChange(data:IOptionChangeData, sender) {
			// the update of the current dc obects happens here
			if (data.selectedYear && this.dimYear) {
				this.dimYear.filter(this.options.selectedYear);
			}
			if (data.selectedExamCode && this.dimExamCode) {
				this.dimExamCode.filter(this.options.selectedExamCode);
			}

			super.onOptionChange(data, sender);
		}

		//' lookups derived from the input data, that may depend on exam code / year
		// the memoized functions are reated here, rather than static, to give an easy way to regenerate them
		// (ie kill the caches and start over) if required
		public makeLookups() {
			// Return an array of the standards defined for an exam in a given year
			// the returned results is an array of {I,C,D, N }; similar to lookups
			// replaces standardForExam going forward
			let _standardsByExamYear = _.memoize(
				(exam, year) => {
					return _.uniqBy(
						this.examsSchoolResultsUnnormalized
							.filter(({ examYear, examCode }) => (examYear === year && examCode == exam))
							.map(({ standardID, standardCode, standardDesc }) => {
								return {
									I: standardID,
									C: standardCode,
									D: standardDesc,
									N: `${standardCode}: ${standardDesc}`
								};
							})
						// _uniqBy : supply an iteratee to determine sameness of the object
						// since we know that standardCode and Description are derived from StandardID
						// we only need to check standardID (I) so we can use the property shorthand
						, "I"
					);
				}, // use a resolver becuase we have 2 arguments
				// this makes the key an array of the argument values
				(exam, year) => [exam, year]
				// or generally and mindlessly we can do this
				//(...args) => _.values(args).join("_")

			);
			let _benchmarksByExamYear = _.memoize(
				(exam, year) => {
					return _.uniqBy(
						this.examsSchoolResultsUnnormalized
							.filter(({ examYear, examCode }) => (examYear === year && examCode == exam))
							.map(({ benchmarkID, benchmarkCode, benchmarkDesc, standardID }) => ({
								I: benchmarkID,
								C: benchmarkCode,
								D: benchmarkDesc,
								N: `${benchmarkCode}: ${benchmarkDesc}`,
								S: standardID						// benchamrks belong to a standard
							})
							)
						// _uniqBy : supply an iteratee to determine sameness of the object
						// since we know that benchamrkCode and Description are derived from BenchmarkID
						// we only need to check benchmarkID (I) so we can use the property shorthand
						, "I"
					).sort((a, b) => a.C < b.C ? -1 : 1);
				}, // use a resolver becuase we have 2 arguments
				(...args) => _.values(args).join("_")
			);

			let _achievementLevelsByExamYear = _.memoize(
				(exam, year) => {
					return _.uniqBy(
						this.examsSchoolResultsUnnormalized
							.filter(({ examYear, examCode }) => (examYear === year && examCode == exam))
							.map(({ achievementLevel, achievementDesc }) => {
								return {
									C: achievementLevel,
									D: achievementDesc,
									N: `${achievementLevel}: ${achievementLevel}`
								};
							})
						// _uniqBy : supply an iteratee to determine sameness of the object
						, "C"
					).sort((a, b) => a.C - b.C);
				}, // use a resolver becuase we have 2 arguments
				(...args) => _.values(args).join("_")
			);

			this.standardsByExamYear = (exam, year) => {
				exam = exam || this.options.selectedExamCode;
				year = year || this.options.selectedYear;
				return _standardsByExamYear(exam, year);
			}
			this.benchmarksByExamYear = (exam, year) => {
				exam = exam || this.options.selectedExamCode;
				year = year || this.options.selectedYear;
				return _benchmarksByExamYear(exam, year);
			}

			this.achievementLevelsByExamYear = (exam, year) => {
				exam = exam || this.options.selectedExamCode;
				year = year || this.options.selectedYear;
				return _achievementLevelsByExamYear(exam, year);
			}
		}

		private _collectData(schNo) {
			let pExam = this.reflator.get(`api/warehouse/exams/school/${schNo}?report`);
			let pExamUnnormalized = this.reflator.get(`api/warehouse/exams/school/${schNo}`);
			// use $q.all so we only proceeed to construct the various data shapes 
			// when all the data is available
			this.$q.all({ pExam, pExamUnnormalized })
				.then(response => {
					this.examsSchoolResults = response.pExam.data;
					this.examsSchoolResultsUnnormalized = response.pExamUnnormalized.data;
					this.rebuild();
				});
		}
		// sequencing issues mean that functions bound to child components are called before the enrolments are populated
		// so these initialisations have to be moved so that they are called when enrolment changes
		private rebuild() {
			// START Crossfilter stuff here for similar reasons as sequencing issues commented above
			// Cause we need the data to create the xf
			this.makeCrossfilter();
			// Set some sensible defaults for crossfilter
			let g = this.dimYear.top(1);
			this.options.selectedYear = g.length ? g[0].examYear : null;
			g = this.dimExamCode.top(1);
			this.options.selectedExamCode = g.length ? g[0].examCode : null;
			this.dimYear.filter(this.options.selectedYear);
			this.dimExamCode.filter(this.options.selectedExamCode);
			// END Crossfilter stuff here for similar reasons as sequencing issues commented above

			this.exams = _.memoize(
				(year) => _.uniq(
					this.examsSchoolResults
						.filter(({ examYear }) => examYear === year)
						.map(({ examName }) => examName)
				));

			this.standardsForExam = _.memoize(
				(exam, year) => _.uniq(
					this.examsSchoolResults
						.filter(({ examYear }) => examYear === year)
						.filter(({ examName }) => examName === examName)
						.map(({ standardDesc }) => standardDesc)
				),
				// memoize cache logic changed in 4.x - array will not "deep" match - convert to string instead
				(exam, year) => [exam, year].toString());

			// NOW the dashboard is ready!
			this.dashboard = this;
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			school: "<",
			// user configurable
			userPalette: "<colors",
			userTitle: "@headingTitle"
		};
		public controller: any = SchoolExamsDashboard;
		public controllerAs: string = "vm";
		public templateUrl: string = `school/SchoolExamsDashboard`;
	}

	angular
		.module("pineapples")
		.component("schoolExamsDashboard", new Component());
}