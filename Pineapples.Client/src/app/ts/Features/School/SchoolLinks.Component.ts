﻿namespace Pineapples.Schools {

	interface IBindings {
		links: any[];
	}

	class Controller extends Sw.Component.ListMonitor implements IBindings {
		public links: any[];
		public school: School;
		public parentkeyname = "lnkID";

		static $inject = ["$scope", "$mdDialog", "Restangular"]
		constructor(
			scope: Sw.IScopeEx
			, private mdDialog: ng.material.IDialogService
			, private Restangular: restangular.IService
		) {
			super();
			this.monitor(scope, "schoollinks", "lnkID");
			this.multiSelect = false;
		}

		public monitoredList() {
			return this.links;
		}
		public isListMember(newData) {
			return (this.school?._id() == newData[this.parentkeyname]);
		}
		public $onChanges(changes) {
		}
		public newDoc() {
			this._uploadDialog().then(() => {
			}, (error) => {
			});
		}

		private _uploadDialog() {
			let options: ng.material.IDialogOptions = {
				locals: { school: this.school },
				controller: SchoolLinkUploadController,
				controllerAs: 'vm',
				bindToController: true,
				templateUrl: "schoollink/uploaddialog"
			}
			return this.mdDialog.show(options);
		}

		private doEdit(link, event) {
			// seed the edit with a reread from the server
			// the easiest way to keep track of currentphoto
			this.Restangular.one("schoollinks", link.lnkID).get().then(
				(link: Pineapples.Documents.DocumentLink) => {
					this._editDialog(link);
				});
		}

		private _editDialog(schlink: Pineapples.Documents.DocumentLink) {
			let options: ng.material.IDialogOptions = {
				locals: { model: schlink, school: this.school },
				controller: editController,
				controllerAs: 'vm',
				bindToController: true,
				templateUrl: "schoollink/editdialog"
			}
			return this.mdDialog.show(options);

		}

		// to do refactor this to somewhere else its duplicated at the moment with teacher links
		private doDelete(link, event) {
			let confirm = this.mdDialog.confirm()
				.title('Delete Document')
				.textContent('Delete the document ' + link.docTitle + ' from the document library?')
				.targetEvent(event)
				.ok('Delete')
				.cancel('Cancel');
			this.mdDialog.show(confirm).then(() => {
				let schlink = new SchoolLink(link);
				// this has cloned the data...
				this.Restangular.restangularizeElement(null, schlink, "schoollinks");
				schlink.remove().then(() => {
					// don't need to do anything becuase MonitoredList has removed the item from the array already
				},
					() => { }
					// remove failed
				);
			});
		}

	}

	/**
	 * Controller for the edit Dialog
	 */
	class editController extends Sw.Component.ComponentEditController {
		static $inject = ["$mdDialog", "ApiUi"];
		constructor(public mdDialog: ng.material.IDialogService
			, apiUi: Sw.Api.IApiUi) {
			super(apiUi);
			this.isEditing = true;
		}
		public school: School;

		public onModelUpdated(newData: any) {
			super.onModelUpdated(newData);
			// with the sql changes, schPhoto is now correctly calculated on the server
			// we only need to set it here, no logic is needed
			this.school.schPhoto = newData.schPhoto;
		}

		public closeDialog() {
			this.mdDialog.cancel();
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				links: "<",
				school: "<"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "school/linklist";
		}
	}
	angular
		.module("pineapples")
		.component("componentSchoolLinks", new Component());

}