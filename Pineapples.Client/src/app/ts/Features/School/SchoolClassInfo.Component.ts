namespace Pineapples.Schools {
  //
  // dependencies:
  // school - an object containing the school data - this is passed as a row {}
  // api the school api
  class Controller {

		public classInfo;
		public isWaiting;

    static $inject = ["$state", "schoolsAPI","Lookups"];
		constructor(
			private state: ng.ui.IStateService
			, private api
			, private lookups: Pineapples.Lookups.LookupService		) {
      
    }

		public $onChanges(changes) {
			console.log(this.classInfo);
		}

		public $onInit() {
			this._selectedSchool = this.classInfo.schNo;
			this._selectedYear = this.classInfo.svyYear;
		}

		public $onDestroy() {

		}

		// presentation of teacher list
		public teacherView = 0;				// 0 = names, 1 = detail

		public teacherAll = 1;				//0 = teachers in grade only 1 = all teachers
		public teacherListFilter = (tch) => (tch.InSelectedGrade || this.teacherAll);

		public setTeacherView = (view, all) => {
			this.teacherView = view;
			this.teacherAll = all;
		}
		public selectClassLevel(levelCode) {
			// use the current year and school, and the new level
			this.isWaiting = true;
			this.reload({
				id: this.classInfo.schNo
				, year: this.classInfo.svyYear
				, level: levelCode
			});
		}

		private _selectedSchool;
		public get selectedSchool() {
			return this._selectedSchool;
		}
		public set selectedSchool(value) {
			this._selectedSchool = value;
			if (value && this.lookups.byCode("schoolNames", value)) {
				
				this.reload( {
					id: this.selectedSchool
					, year: this.classInfo.svyYear
					, level: this.classInfo.ClassLevel
				});
			}
		}

		private _selectedYear: number;
		public get selectedYear() {
			return this._selectedYear;
		}

		public set selectedYear(value) {
			this._selectedYear = value;
			if (value && _.find(this.classInfo.Years, ({ surveyYear }) => surveyYear)) {

				this.reload({
					id: this.classInfo.schNo
					, year: value
					, level: this.classInfo.ClassLevel
				});
			}
		}

		public reload(params) {
			this.isWaiting = true;
			this.state.go(".", params)
				.catch((reason) => {
					this.isWaiting = false;
					console.log(reason);
				} );
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public controller = Controller;
		public controllerAs: string = "vm";
		public bindings = {
			classInfo: "<"
		};
		public templateUrl = "school/classinfo";
	}

	angular
		.module("pineapples")
		.component("schoolClassInfo", new ComponentOptions());

}

