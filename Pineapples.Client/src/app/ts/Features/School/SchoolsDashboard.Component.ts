﻿namespace Pineapples.Dashboards {

	export namespace Enrolments {
		export interface IxfData {
			Age: number;
			AuthorityCode: string;
			AuthorityGovtCode: string;
			ClassLevel: string;
			DistrictCode: string;
			IslandCode: string;
			RegionCode: string;
			Enrol: number;
			EnrolM: number;
			EnrolF: number;
			GenderCode: string;
			SchoolTypeCode: string;
			SurveyYear: number
		}
		// simplest value accessor based on this data shape:
		// just returns the sum M + F
		export function va(d: IxfData) {
			return d.EnrolM + d.EnrolF;
		};

		// this is a value accessor for building a cross tabulation
		// where each 'value' is an object containing M F and T values
		export function vaGendered(d: IxfData) {
			return { Male: d.EnrolM, Female: d.EnrolF, Total: d.EnrolM + d.EnrolF };
		};
	}
	export class SchoolsDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

		public va = Enrolments.va;
		public vaGendered = Enrolments.vaGendered;

		// the memoization is pointless here, becuase these objects are only created once.
		private xReduce = _.memoize((tableName, ...inputs) => this.xFilter.xReduce.bind(this.xFilter)(...inputs))
		private groupReduceSum = _.memoize((groupName, dim, va) => dim.group().reduceSum(va));

		// override with strongly typed crossfilter
		public xf: CrossFilter.CrossFilter<Enrolments.IxfData>;

		public dimSurveyYear: CrossFilter.Dimension<Enrolments.IxfData, number>;
		public dimDistrictCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimIslandCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimRegionCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimAuthorityCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimAuthorityGovtCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimSchoolTypeCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimEducationLevel: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimGender: CrossFilter.Dimension<Enrolments.IxfData, string>;
		public dimAge: CrossFilter.Dimension<Enrolments.IxfData, number>;
		public dimAgeGroup: CrossFilter.Dimension<Enrolments.IxfData, number>;

		// there is no point in memoizing these groups other than jit instantiation ....
		public grpDistrictCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public grpIslandCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public grpRegionCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public grpAuthorityCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public grpAuthorityGovtCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabAge_EdLevel: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabAge_Level: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabSchoolTypeByDistrict: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabDistrictBySchoolType: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabSchoolTypeByIsland: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabIslandBySchoolType: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabSchoolTypeByRegion: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabRegionBySchoolType: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabSchoolTypeByAuthorityGovt: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabAuthorityGovtBySchoolType: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabDistrict_EdLevel: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabEdLevel_District: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabIsland_EdLevel: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabEdLevel_Island: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabRegion_EdLevel: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabEdLevel_Region: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabAuthorityGovt_EdLevel: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public tabEdLevel_AuthorityGovt: CrossFilter.Group<Enrolments.IxfData, string, number>;

		public labelerAgeGroup: any;
		public rowLookup: any;


		public onOptionChange(data: IOptionChangeData, sender) {
			if (data.selectedYear) {
				this.dimSurveyYear.filter(this.options.selectedYear);
			}
			if (data.selectedDistrict) {
				this.dimDistrictCode.filter(this.options.selectedDistrict);
			}
			if (data.selectedIsland) {
				this.dimIslandCode.filter(this.options.selectedIsland);
			}
			if (data.selectedRegion) {
				this.dimRegionCode.filter(this.options.selectedRegion);
			}
			if (data.selectedAuthorityGovt) {
				this.dimAuthorityGovtCode.filter(this.options.selectedAuthorityGovt);
			}
			if (data.selectedAuthority) {
				this.dimAuthorityCode.filter(this.options.selectedAuthority);
			}
			if (data.selectedSchoolType) {
				this.dimSchoolTypeCode.filter(this.options.selectedSchoolType);
			}
			// always call the super version so that the clients get to know about the option change as well
			super.onOptionChange(data, sender);
		}

		private elSelector = ({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel)["L"];
		private classLevelSelector = this.xFilter.getPropAccessor("ClassLevel")

		public createDimensions() {
			// create the dimensions

			// Lookup by code like others now...
			//this.dimAuthorityGovtCode = this.xf.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovtCode);
			//	return ag ? ag.N : null;
			//});


			this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
			this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);
			this.dimIslandCode = this.xf.dimension(d => d.IslandCode);
			this.dimRegionCode = this.xf.dimension(d => d.RegionCode);
			//this.dimGender = this.xf.dimension(d => d.GenderCode);
			this.dimAuthorityCode = this.xf.dimension(d => d.AuthorityCode);
			this.dimAuthorityGovtCode = this.xf.dimension(d => d.AuthorityGovtCode);
			this.dimSchoolTypeCode = this.xf.dimension(d => d.SchoolTypeCode);
			this.dimAge = this.xf.dimension(d => d.Age);

			this.dimEducationLevel = this.xf.dimension(this.elSelector);
			this.dimAgeGroup = this.xf.dimension(this.ageGrouper(5));

			// now create some general purpose groups for dashboard-child use:

			// simplify this so these can be used for charts as well as tables
			// the chart needs to be passed the group, not the dimension, unless it has specific into on 
			// how to construct the group
			// there is no point in memoizing these groups other than jit instantiation ....
			this.grpDistrictCode = this.dimDistrictCode.group().reduceSum(Enrolments.va);
			this.grpIslandCode = this.dimIslandCode.group().reduceSum(Enrolments.va);
			this.grpRegionCode = this.dimRegionCode.group().reduceSum(Enrolments.va);
			this.grpAuthorityCode = this.dimAuthorityCode.group().reduceSum(Enrolments.va);
			this.grpAuthorityGovtCode = this.dimAuthorityGovtCode.group().reduceSum(Enrolments.va);

			// cross tabulation groups
			this.tabAge_EdLevel = this.xFilter.xReduce(this.dimAgeGroup, this.elSelector, Enrolments.vaGendered);
			this.tabAge_Level = this.xFilter.xReduce(this.dimAge, this.classLevelSelector, Enrolments.vaGendered);
			this.tabDistrict_EdLevel = this.xFilter.xReduce(
				this.dimDistrictCode,
				this.elSelector,
				Enrolments.vaGendered);

			this.tabSchoolTypeByDistrict = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.DistrictCode,
				Enrolments.vaGendered);

			this.tabDistrictBySchoolType = this.xFilter.xReduce(
				this.dimDistrictCode,
				x => x.SchoolTypeCode,
				Enrolments.vaGendered);

			this.tabSchoolTypeByIsland = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.IslandCode,
				Enrolments.vaGendered);

			this.tabIslandBySchoolType = this.xFilter.xReduce(
				this.dimIslandCode,
				x => x.SchoolTypeCode,
				Enrolments.vaGendered);

			this.tabSchoolTypeByRegion = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.RegionCode,
				Enrolments.vaGendered);

			this.tabRegionBySchoolType = this.xFilter.xReduce(
				this.dimRegionCode,
				x => x.SchoolTypeCode,
				Enrolments.vaGendered);

			this.tabSchoolTypeByAuthorityGovt = this.xFilter.xReduce(
				this.dimSchoolTypeCode,
				x => x.AuthorityGovtCode,
				Enrolments.vaGendered);

			this.tabAuthorityGovtBySchoolType = this.xFilter.xReduce(
				this.dimAuthorityGovtCode,
				x => x.SchoolTypeCode,
				Enrolments.vaGendered);

			this.tabEdLevel_District = this.xFilter.xReduce(
				this.dimEducationLevel,
				x => x.DistrictCode,
				Enrolments.vaGendered);			

			this.tabIsland_EdLevel = this.xFilter.xReduce(
				this.dimIslandCode,
				this.elSelector,
				Enrolments.vaGendered);

			this.tabEdLevel_Island = this.xFilter.xReduce(
				this.dimEducationLevel,
				x => x.IslandCode,
				Enrolments.vaGendered);

			this.tabRegion_EdLevel = this.xFilter.xReduce(
				this.dimRegionCode,
				this.elSelector,
				Enrolments.vaGendered);

			this.tabEdLevel_Region = this.xFilter.xReduce(
				this.dimEducationLevel,
				x => x.RegionCode,
				Enrolments.vaGendered);

			this.tabAuthorityGovt_EdLevel = this.xFilter.xReduce(
				this.dimAuthorityGovtCode,
				this.elSelector,
				Enrolments.vaGendered);

			this.tabEdLevel_AuthorityGovt = this.xFilter.xReduce(
				this.dimEducationLevel,
				x => x.AuthorityGovtCode,
				Enrolments.vaGendered);

			this.labelerAgeGroup = ((i) => this.ageGroupLabel(i, 5));
			// Hard coded for now as the version returning a function
			// was causing weird non obvious problems
			// TODO revise this 
			this.rowLookup = ((i) => this.lookups.cache["districts"].byCode(i).N);

			// set the surveyYear to be the most recent in the warehouse data, rather than the most recent surveyYear in the lookups
			// we dont want to get empty data on the initial display if the warehouse is not yet generated for the current year
			if (!this.options.selectedYear) {
				this.options.selectedYear = this.dimSurveyYear.top(1)[0].SurveyYear;;
			}
			this.dashboard = this;
		}

		private firstCheckX = fn => x => !x.Female || !x.Male || x.Female === 0 || x.Male === 0 ? '' : fn(x);

		public indicatorCalculations = {
			GPI: this.firstCheckX(x => (1.0 * x.Female / x.Male).toFixed(2)),
			"Female %": this.firstCheckX(x => (100.0 * x.Female / (x.Female + x.Male)).toFixed(1)),
			"Male %": this.firstCheckX(x => (100.0 * x.Male / (x.Female + x.Male)).toFixed(1)),
			All: this.firstCheckX(x => x.Female + x.Male)
		}

		public filterString = (...exclude) => `
       ${!_(exclude).includes('Year') && this.options.selectedYear || ''}
       ${!_(exclude).includes('District') && this.lookups.byCode('districts', this.options.selectedDistrict, 'N') || ''}
       ${!_(exclude).includes('Island') && this.lookups.byCode('islands', this.options.selectedIsland, 'N') || ''}
       ${!_(exclude).includes('Region') && this.lookups.byCode('regions', this.options.selectedRegion, 'N') || ''}
       ${!_(exclude).includes('Authority') && this.lookups.byCode('authorities', this.options.selectedAuthority, 'N') || ''}
       ${!_(exclude).includes('AuthorityGovt') && this.lookups.byCode('authorityGovts', this.options.selectedAuthorityGovt, 'N') || ''}
       ${!_(exclude).includes('SchoolType') && this.lookups.byCode('schoolTypes', this.options.selectedSchoolType, 'N') || ''}
			`


		/**
		 * Group a numeric value into bands
		 * 
		 * @param n the size of the band
		 */
		private ageGrouper = (n: number) =>
			({ Age }) => Age ? (Math.floor(Age / n) * n + 1) : 0;
		/**
		 * Display a label for an age band
		 * where the client uses an age grouped dimension, this function can be used to produce the row label
		 * Note that if the grouping is done on this value, rather than ageGrouper, the sorting wont be right.
		 * @param n starting value
		 * @param groupSize size of the band
		 */
		public ageGroupLabel = (n: number, groupSize: number) => {
			if (!n || n === 0) { return '-'; }
			return n ? n.toString() + "-" + (n + groupSize - 1).toString() : 0;
		}

		/**
		 * Display the label for a row in a crosstab group; ie this row is a key-value
		 * object, and the agegroup is the key
		 */
		public ageGroupAccessor = (row) => this.ageGroupLabel(row.key, 5);

		public valueAccessor = (row, col, gender) => {
			if (!gender) {
				return (row.value?.[col]?.Total === undefined ? row.value[col] : row.value[col].Total)
			} else {
				gender = {
					M: "Male",
					F: "Female",
					T: "Total"
				}[gender];
				return row.value?.[col]?.[gender];
			}
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;


		constructor() {
			this.bindings = {
				table: "<",
				options: "<"			// get the options
			};
			this.controller = SchoolsDashboard;
			this.controllerAs = "vm";
			this.templateUrl = `school/Dashboard`;
		}
	}

	angular
		.module("pineapples")
		.component("schoolsDashboard", new Component());


	export class EnrolComponent implements ng.IComponentOptions {
		public bindings: any = dashboardChildBindings;

		public templateUrl: string;
		public controllerAs: string = 'vm';

		constructor(public controller: any, templateUrl) {
			this.templateUrl = "school/dashboard/component/" + templateUrl;
		}
	}

}
