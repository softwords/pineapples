namespace Pineapples.Dashboards.School {

	class Controller extends DashboardChild implements IDashboardChild {

		// Bindings
		public dashboard: SchoolExamsDashboard;
		public group: CrossFilter.Group<any, any, any>;
		public dimension: CrossFilter.Dimension<any, any>;
		public options: any;

		private chartData;

		public paletteName;
		public colorbrewer: ColorBrewer.Base = colorbrewer;

		// these 2 properties supply defaults for 'palette' and 'title'
		// they may be overriden by bindings to 'color' and 'headingTitle'
		// see issue849
		protected defaultPalette = ['#1010FF', '#223344'];
		protected defaultTitle = "Achievement by Gender";

		public $onInit() {
			// While we can collect data onInit of parents we can not rely on data arriving in time
			// to be part of the whole life cycle of dashboard->dashboard child components
			// like we have with the global dashboards (e.g. Schools, Teachers, etc.)
			// Instead with individual entity dashboards (e.g. School, Teacher) we collect data and 
			// check for it in the onChanges of dashboard childs as done below
		}

		public $onChanges(changes) {
			// A change in the group (or dimension) binding in this case means data as arrived in parent dashboard
			// and was loaded into crossfilter, etc.
			if (changes.dimension && changes.dimension.currentValue) {
				// For extensive comments on code herein refer to ResultsGenderTree.Component.ts
				// The only difference is we have to call in onChanges when group is ready as this
				// individual entity dashboard is not like global dashboards
				this.group = this.dashboard.xFilter.xReduce(
					this.dimension,
					"achievementLevel",
					this.dashboard.xFilter.getFlattenedGenderAccessor("Gender", "Candidates")
				);

				let columns = ["F", "M"];

				let valueAccessor = (row, columnName, columnIndex) => (row.value.Tot) ?
					row.value.Tot[columnName] * (columnName == 'F' ? -1 : 1) : 0;

				this.chartData = {
					group: this.group,
					columns: columns,
					valueAccessor: valueAccessor
				};
			}

			// a dashboard child should always call super in here,
			// or else onOptionChange will never fire
			super.$onChanges(changes);
			// whatever is changed, we need a redraw
			// all we need to do is 'touch' the chart
			this.refreshChart();

		}

		////////////////////////////////////////////////////////////////////////////////
		// Chart presentation
		////////////////////////////////////////////////////////////////////////////////
		//
		// helper function to convert an achievement level 1 2 3 4 to text
		// for illustration only - in real world this needs to account for different exam structures
		private levelLookup = (achievementLevel) => {
			let n = [{ name: "Well below competent" }
				, { name: "Approaching competence" }
				, { name: "Minimally competent" }
				, { name: "Competent" }
			][achievementLevel - 1].name;
			return n;
		}

		/**
		 * callback set up in bindings to the rowcolchart. 
		 * on-chart-render='vm.onChartRender(option, echart)'
		 * This is invoked immediately before the chart is drawn. 
		 * Modify (but don't replace!) option to change aspects of the chart using echarts option API
		 * @param option - the option object
		 * @param echart - the echart instance
		 */
		public onChartRender(option: echarts.EChartOption, echart: echarts.ECharts) {
			// a typical strategy in many case is to create an option object containing the changes, and merge it
			let o = {
				xAxis: {
					axisLabel: {
						formatter: (value) => Math.abs(value)		// show the - values as + on the value axis
					}
				},
				yAxis: {
					axisLabel: {
						formatter: this.levelLookup				// formatter is a function taking a single value, being the axis label value
					}
				}
			}
			// angular.merge is a deep copy
			angular.merge(option, o);
		}

		/**
		 * callback to formt a tooltip
		 * There are lots of options for this, but this is perhaps the simplest:
		 * get the 'datum' format contiing the series name, item name marker and value and percent
		 * via the binding 
		 * tooltipper="vm.tooltipper(datum)"
		 * Change whaver parts of datum you want to change
		 * Don't return a value - then rowcolchart will format the modified datum in the standard tooltip format
		 * Note you can return a value - and that is used as the tooltip. Furthermore
		 * the returned value can be html
		 */
		public tooltipper(datum) {
			// replace the item name (1 2 3 4) with its lookup
			datum.item = this.levelLookup(datum.item);
			// change F M to Female or Male
			datum.series = (datum.series == "F") ? "Female" : "Male";
			// return the abs of the value
			datum.value = Math.abs(datum.value);
		}

	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			dashboard: "<",
			group: "<",
			options: "<",
			selectedChild: "<",
			optionChange: "<",
			componentTitle: "@",
			// allow a dimension to be passed to components that want to make their own group
			dimension: "<"
		};
		public templateUrl: string;
		public controllerAs: string = 'vm';

		constructor(public controller: any, templateUrl) {
			this.templateUrl = "school/SchoolDashboard/Component/" + templateUrl;
		}
	}

	angular
		.module("pineapples")
		.component("schoolExamResultsGenderTree", new Component(Controller, "SchoolExamResultsGenderTree"));
}