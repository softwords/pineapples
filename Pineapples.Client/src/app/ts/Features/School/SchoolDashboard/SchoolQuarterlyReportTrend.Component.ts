﻿namespace Pineapples.Schools {

	//TODO a lot of the strongly type code here was designed for the legacy plottable charts
	// rather then redoing all this a packetWgt adapter was used as shortcut

	/**
   * @interface IBindings
	 * @description What bindings are expected in the component's controller
	 * @see IChartMetric
	 */
	interface IBindings {
		quarterlyreports: any;
		title: string;
		meta: Array<IChartMetric>;
		ngDisabled: boolean;
	}

  /**
   * @interface IChartUnitData
   * @description A single atomic point on the chart. A Javascript item would look like
   * ```javascript
   * {
   *   svyYear: 2015,
   *   value: 0.98
   * }
   * ```
   */
	interface IChartUnitData {
		period: string;
		value: number;
	}

  /**
   * @interface IChartMetric
   * @description A description of the metric for a single dataset in a chart. A Javascript item would look like
   * ```javascript
   * {
   *   metric: 'AggDaysAbs',
   *   name: 'Agg. Days of Absence'
   * }
   * ```
   * where the metric is am actual metric coming from a School quaterly report list and
   * name simply a more verbose name for it.
   */
	interface IChartMetric {
		metric: string;
		name: string;
	}

  /**
   * @interface IChartDataset
   * @description A given dataset with associated metadata. It is is packaged in a way ready to be passed
   * to a Plottable.Dataset easily. A single item would look like
   * ```javascript
   * {
   *   ds: [{period: "2015/Q3", value: 0.98}, {period: "2015/Q4", value: 0.985}]
   *   meta: {metric: 'AvgDailyAtt', name: 'Avg. Daily Attendance'}
   * }
   * ```  
   * @see IChartUnitData, IChartMetric
   * @example new Plottable.Dataset(chartData.ds[0]) where chartData: IChartDataset
   */
	interface IChartDataset {
		ds: Array<IChartUnitData>;
		meta: IChartMetric;
	}

  /**
   * @interface IChartData
   * @description Contains the chart datasets and some other information for the chart
   * @see IChartDataset
   */
	interface IChartData {
		title: string;
		datasets: Array<IChartDataset>;
	}

	class Controller {

		// Currently not using the crossfilter goodies. This is merely a least amount
		// of effort to migrate the remaining plottable charts to echart

		// bindings
		public quarterlyreports: any;
		public title: string;
		public meta: any;
		public ngDisabled: boolean;

		private datasets: Array<IChartDataset>;
		private sortedQrs: any;

		// Convert to echart using the widgetPkg transit format
		public widgetPkg: Sw.Charts.IWidgetPkg;
		public palette: string[];
		public echart: echarts.ECharts;

		////////////////////////////////////////////////////////////////
		// LifeCycle events
		/////////////////////////////////////////////////////////////////
		public $onInit() {
		}

		public $onChanges(changes: any) {

		}

		public eChartInit(echart) {
			this.echart = echart;
		}

		public $postLink() {
			this.makeWidgetPkg();
		}

		public makeWidgetPkg() {
			// This still goes through the legacy interim data format before 
			// getting packages in widgetPkg

			// what sort of chart datasets we dealing with (but no data yet)
			this.datasets = _.map(this.meta, function (i) {
				return { ds: [], meta: i };
			});

			// Sort our quarterly reports by period
			this.sortedQrs = this.quarterlyreports.sort(function (a, b) {
				let aYearAndQuarter: [number, string] = a.InspQuarterlyReport.split("/");
				let bYearAndQuarter: [number, string] = b.InspQuarterlyReport.split("/");

				if (aYearAndQuarter[0] < bYearAndQuarter[0]) {
					return -1;
				}
				if (aYearAndQuarter[0] > bYearAndQuarter[0]) {
					return 1;
				}
				if (aYearAndQuarter[0] === bYearAndQuarter[0]) {
					let periodIndex = { 'Q3': 1, 'Q4': 2, 'Q1': 3, 'Q2': 4 };
					if (periodIndex[aYearAndQuarter[1]] < periodIndex[bYearAndQuarter[1]]) {
						return -1;
					}
					if (periodIndex[aYearAndQuarter[1]] > periodIndex[bYearAndQuarter[1]]) {
						return 1;
					}
				}

			});

			// For each school's quarterly reports extract data of interest into datasets
			for (let qr of this.sortedQrs) {
				for (let dataset of this.datasets) {
					// What metric is this? and get its quarterly report data
					let metric = dataset.meta.metric;
					let period = qr.InspQuarterlyReport;
					let value = qr[metric];
					dataset.ds.push({ period: period, value: value });
				}
			}

			// Package in ready format to be transmormed and processed by echart
			this.widgetPkg = {
				data: this.datasets.map(function (i) { return i.ds; }),
				X: 'period',
				Y: 'value',
				seriesNames: this.datasets.map(function (i) { return i.meta.name; })
			}
		}

		public onChartRender(option: echarts.EChartOption, echart) {
			//option['title'] = { text: this.title };
			option.series.forEach((series: echarts.EChartOption.SeriesLine) => {
				series.smooth = true;
			})
		}

	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			quarterlyreports: "<",
			title: "@",
			meta: "<",
			ngDisabled: "="
		};

		public controller: any = Controller;
		public controllerAs: string = "vm";
		public template: string = `
        <div class="dashboard-wrapper" style="padding: 5px;"> 
						<row-col-chart
									charttype = "l"
									dataset="vm.widgetPkg"
									on-chart-init="vm.eChartInit(echart)"
									on-chart-render="vm.onChartRender(option, echart)"
									colors="vm.palette"/>
        </div>`;
	}

	angular
		.module("pineapples")
		.component("quarterlyReportChart", new Component());

}