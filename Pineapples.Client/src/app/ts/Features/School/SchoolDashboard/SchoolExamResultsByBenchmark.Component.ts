﻿namespace Pineapples.Dashboards.School {

	class Controller extends DashboardChild implements IDashboardChild {

		// Bindings
		public dashboard: SchoolExamsDashboard;
		public group: CrossFilter.Group<any, any, any>;
		public options: any;

		public chartData;
		public groupData: any[]; // filtered all() array of keyvalue pairs	
		public touch: number;
		public columnSortBy: any[];

		protected defaultPalette;
		protected defaultTitle = "Achievement Results by Benchmark";
		public paletteName;
		public colorbrewer: ColorBrewer.Base = colorbrewer;
		public viewOptions;

		public onViewOptionChange() {
			this.refreshChart();				// this currently doesn;t work - viewOptions are not implemented
		};

		////////////////////////////////////////////////////////////////
		// LifeCycle events
		/////////////////////////////////////////////////////////////////
		public $onInit() {
			// While we can collect data onInit of parents we can not rely on data arriving in time
			// to be part of the whole life cycle of dashboard->dashboard child components
			// like we have with the global dashboards (e.g. Schools, Teachers, etc.)
			// Instead with individual entity dashboards (e.g. School, Teacher) we collect data and 
			// check for it in the onChanges of dashboard childs as done below
		}

		public $onChanges(changes) {
			// A change in the group binding in this case means data as arrived in parent dashboard
			// and was loaded into crossfilter, etc.
			// We also don't need to run this on other changes to bindings
			if (changes.group && changes.group.currentValue) {
				this.viewOptions = this.dashboard.dimStandardCode.group().all().map(d => d.key);
				this.configSupportingData();
				this.makeChartData();
				this.refreshChart();
			}
			// a dashboard child should always call super in here,
			// or else onOptionChange will never fire
			super.$onChanges(changes);
		}

		public onOptionChange(changes:IOptionChangeData) {
			// onOptionChange can get fired before there is a group and crossfilter setup (late promise of data)
			// it's pointless to do any action on option change without the initial setup of everything
			if (this.group) {
				if (changes.selectedYear || changes.selectedExamCode) {
					this.configSupportingData();
				}
				this.refreshChart();
			}
		};

		//////////////////////////////////////////////////////////////////////////////////////////////////
		// setup and configuration
		//////////////////////////////////////////////////////////////////////////////////////////////////

		/**
		 * series have to be reorganised to get them placed on the correct side of the axis in the correct order
		 * This routine can sort based on the multipliers, to set up this correct sequence.
		 * It is applied to the achivement Levels ( and sent to the chart through columnSortBy)
		 * and then to the palette, to apply the correct color for the series in the onChartRender callback
		 * @param arr - the array to sort according to multipliers
		 * @param multipliers array of -1 1 to determine positions
		 */
		private sortByMultipliers(arr: any[], multipliers: number[]): any[] {
			// create an array of the indexes of arr ie [0,1,2,.........arr.length - 1]
			let indexes = arr.map((value, index) => index);
			// create the comparator function to check the value of the multiplier
			let msort = (n1, n2) =>
				(multipliers[n1] != multipliers[n2]) ? multipliers[n1] - multipliers[n2]
					// if they are both -negative, the larger index is first (ie smaller)
					// if both positive, the smaller index is first
					: (n1 - n2) * multipliers[n1];
			// sort the indexes in place
			indexes.sort(msort);
			// reconstruct the array inthis order
			return arr.map((value, index) => arr[indexes[index]]);

		}

		public achievements;
		public multipliers: number[];

		/**
		 * The accessor functions in chartData reference the achievements list and multipliers
		 * As well, palette may change according to exam type,
		 * and the column sort order may change ( columnSortBy is bound)
		 * So these get recreated on an option chnage to selectedYear or selectedExamCode
		 */
		private configSupportingData() {
			if (this.options.selectedYear && this.options.selectedExamCode) {

				this.achievements = this.dashboard.achievementLevelsByExamYear();
				this.multipliers = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
				switch (this.achievements.length) {
					case 4:			// assume its an STA
						this.multipliers = [-1, -1, 1, 1];
						this.defaultPalette = this.sortByMultipliers(this.dashboard.palettes.sta, this.multipliers);
						break;
					default:
						this.defaultPalette = colorbrewer.Oranges[9];
				}
				this.columnSortBy = this.sortByMultipliers(this.achievements.map(a => a.C), this.multipliers);
				this.viewOptions = _.map(this.dashboard.standardsByExamYear(), "N");
			}

		}
		public makeChartData() {
			// this crosstab has an array for key, so we need a rowId function, normally this can be omitted
			// and allowed to 
			let rowId = (row) => `${row.key[1]}: ${row.key[2]}`;
			let columns = (row) => this.achievements.map(achievement => achievement.C);
			let valueAccessor = (row, col, colIdx) => row.value[col] ? (row.value[col].T * this.multipliers[colIdx]) || 0 : 0;

			this.chartData = {
				group: this.group,
				rowId: rowId,
				columns: columns,
				valueAccessor: valueAccessor
			}
		}

		public refreshChart() {
			// ensure that touch is bound onto the RowColChart 
			// note it is an @ binding ie use like this:  touch="{{vm.touch}}"
			this.touch = Date.now();
		}

		/////////////////////////////////////////////////////////////
		// Chart callbacks
		/////////////////////////////////////////////////////////////
		public tooltipper(datum) {
			// make the value abs
			datum.value = Math.abs(datum.value);
		}

		public onChartRender(option, echart) {
			option.series.forEach((series, idx) => {
				// change the series name to the achievement description ('Weel bleow competent'...)
				// or 'Level 1', 
				// Changing the series name will be reflected in tooltip, legend etc
				// although in this case, we still have to regenerate the legend data to get the sort order right
				series.name = _.find(this.achievements, { C: series.id })["D"];
				// apply the sorted palette to the sorted series
				series.color = this.palette[idx];
			});

			let o = {

				xAxis: {
					axisLabel: {
						formatter: (value) => Math.abs(value)
					}
				},
				yAxis: {
					axisLabel: {
						rich: {
							bold: {
								color: "darkgrey",
								fontWeight: "bold"
							}
						}
					}
				},
				legend: {
					// this restores the order in the legend
					data: this
						.achievements.map(a => ({ name: a.D }))
				},
				grid: {
					left: 0
				}
			}
			angular.merge(option, o);
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			dashboard: "<",
			group: "<",
			options: "<",
			selectedChild: "<",
			optionChange: "<",
			componentTitle: "@",
			// allow a dimension to be passed to components that want to make their own group
			dimension: "<"
		};
		public templateUrl: string;
		public controllerAs: string = 'vm';

		constructor(public controller: any, templateUrl) {
			this.templateUrl = "school/SchoolDashboard/Component/" + templateUrl;
		}
	}

	angular
		.module("pineapples")
		.component("schoolExamResultsByBenchmark", new Component(Controller, "SchoolExamResultsByBenchmark"));
}