﻿namespace Pineapples.Schools {

  class Controller {
    public school: Pineapples.Schools.School;

    public activityCols = ['TPK', 'T00', 'T01', 'T02', 'T03', 'T04', 'T05', 'T06', 'T07', 'T08', 'T09',
            'T10', 'T11', 'T12', 'T13', 'T14', 'T15', 'T', 'A', 'X'];
    static $inject = ["Lookups"]
    constructor(public lookups: Pineapples.Lookups.LookupService) { }

    public $onChanges(changes) {
      this.teacherCountsTable = this.school.Teachers.reduce((prev, row, idx) => {
        this.activityCols.forEach((col) => {
          prev.ftpt[col] = (prev.ftpt[col]??0) + row[col];
          prev.fte[col] = (prev.fte[col] ?? 0) + (row[col] / row.Activities);
        });
        prev.count++;
        return prev;
      }, { count: 0, ftpt: {}, fte: {}});
    }

    public rowAccessor = (row) => [row.tGiven, row.tSurname].join(" ");

    public valueAccessor = (row, col) => row[col];

    public solidblock = '\u25A0';
    public hollowblock = '\u25A1';

  

    public teacherCountsTable: {
      count: number,
      ftpt: any[],
      fte: any[]
    }

    public values(t) {
      return [t.T00, t.T01];
		}
  }

  class Component implements ng.IComponentOptions {
    public bindings = {
      school: '<'
    };
    public controller = Controller;
    public controllerAs = "vm";
    public templateUrl = "school/schoolstafflist";
  }

  angular
    .module("pineapples")
    .component("schoolStaffList", new Component());
}