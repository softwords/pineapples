﻿namespace Pineapples.Schools {

	class Controller {
    public school: School
	}


  class Component implements ng.IComponentOptions {
    public bindings = {
      school: '<'
    };
    public controller = Controller;
    public controllerAs = "vm";
    public templateUrl = "school/schoolexams";
  }

  angular
    .module("pineapples")
    .component("schoolExams", new Component());

}