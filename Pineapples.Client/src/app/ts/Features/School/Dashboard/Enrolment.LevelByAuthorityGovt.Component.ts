﻿namespace Pineapples.Dashboards {

	class Controller extends DashboardChild implements IDashboardChild {

		public dashboard: SchoolsDashboard;
		public grpAuthorityGovtCode: CrossFilter.Group<Enrolments.IxfData, string, number>;

		public onDashboardReady() {
			this.grpAuthorityGovtCode = this.dashboard.dimAuthorityGovtCode.group().reduceSum(d => d.EnrolM + d.EnrolF);
		}
	}

	angular
		.module("pineapples")
		.component("enrolmentLevelByAuthorityGovt", new EnrolComponent(Controller, "EnrolmentLevelByAuthorityGovt"));
}
