﻿namespace Pineapples.Dashboards {
	const ROW_LOOKUP = "authorityGovts";

	class Controller extends DashboardChild implements IDashboardChild {

		public dashboard: SchoolsDashboard;
		public chartData: Sw.Charts.AnnotatedGroup;
		public grpAuthorityGovtCode: CrossFilter.Group<Enrolments.IxfData, string, number>;

		public onDashboardReady() {
			let xFilter = this.dashboard.xFilter;
			this.grpAuthorityGovtCode = xFilter.xReduce(
				this.dashboard.dimAuthorityGovtCode, "AuthorityCode", Enrolments.vaGendered);

			this.chartData = {
				group: this.grpAuthorityGovtCode,
				columns: ["Female", "Male"],
				valueAccessor: (row, col) => row.value.Tot[col]
			};
		}

		// override
		public get highlight() {
			return this.options.selectedAuthorityGovt;
		}

		public clickHandler(params) {
			this.options.toggleSelectedAuthorityGovt(params.name);
		}

		public onChartRender(option: echarts.EChartOption, echarts, renderedType: string) {
			Sw.Charts.chartOps(option)
				.showLegend(renderedType != "pie" && (option.series.length > 1))
				.showPieLabels(renderedType == "pie")
				.minimalInk()
				.lookups(this.lookups)
				.translateLabels(ROW_LOOKUP, ["h", "hp", "hc"].indexOf(renderedType) >= 0);
		}

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			datum.item = this.lookups.byCode(ROW_LOOKUP, datum.item, "N");
		}

		private _allowedTypes = ["v", "vp", "h", "hp", "pie"];
		public get allowedTypes(): string[] {
			return this.isSelected() ? this._allowedTypes : undefined;
		}
	}

	angular
		.module("pineapples")
		.component("enrolmentAuthorityGovtChart", new EnrolComponent(Controller, "EnrolmentAuthorityGovtChart"));
}
