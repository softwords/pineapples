﻿namespace Pineapples.Dashboards {

	class Controller extends DashboardChild implements IDashboardChild {

		public dashboard: SchoolsDashboard;

		public grpAuthorityCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
		public chartData: Sw.Charts.AnnotatedGroup;
		public grpAuthorityGovtCode: CrossFilter.Group<Enrolments.IxfData, string, number>;

		public onDashboardReady() {

			this.grpAuthorityCode = this.dashboard.xFilter.xReduce(
				this.dashboard.dimAuthorityCode,
				x => x.DistrictCode,
				Enrolments.vaGendered);
			this.chartData = {
				group: this.grpAuthorityCode,
				columns: ["Female", "Male"],
				valueAccessor: (row, col) => row.value.Tot[col]
			};
			this.grpAuthorityGovtCode = this.dashboard.dimAuthorityGovtCode.group().reduceSum(d => d.EnrolM + d.EnrolF);
		}

		public get highlight() {
			return this.options.selectedAuthority;
		}

		public clickHandler(params) {
			this.options.toggleSelectedAuthority(params.name);
		}

		public onChartRender(option, echarts, renderedType) {
			Sw.Charts.chartOps(option)
				.showLegend(renderedType != "pie" && (option.series.length > 1))
				.showPieLabels(renderedType == "pie")
				.minimalInk()
				.lookups(this.lookups)
				.translateLabels("authorities", ["h", "hp", "hc"].indexOf(renderedType) >= 0);
		}

		public tooltipper(datum: Sw.Charts.TooltipDatum) {
			datum.item = this.lookups.byCode("authorities", datum.item, "N");
		}

		private _allowedTypes = ["v", "vp", "h", "hp", "pie"];
		public get allowedTypes(): string[] {
			return this.isSelected() ? this._allowedTypes : undefined;
		}
	}

	angular
		.module("pineapples")
		.component("enrolmentAuthorityChart", new EnrolComponent(Controller, "EnrolmentAuthorityChart"));
}
