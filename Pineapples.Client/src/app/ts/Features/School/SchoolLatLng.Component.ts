namespace Pineapples.Schools {
  //
  // dependencies:
  // school - an object containing the school data - this is passed as a row {}
  // api the school api
  class Controller {

    private lat;
    private lng;
    public marker: google.maps.Marker;
    public map: google.maps.Map;
    private _state = "";

		public model: School; // the school

    public get state() {
      return this._state;
    }
    public set state(newState) {
      this._state = newState;
      this._resetHandlers();
    }

    static $inject = ["$scope", "schoolsAPI"];
    constructor(private _scope: ng.IScope, private api) {
      _scope.$on('mapInitialized', this._mapInitializer);
    }

		public $onChanges(changes) {
			console.log(this.model);
		}

		public $onInit() {
		}

		public $onDestroy() {
			// seems that if the map is not destoyed moving between states,
			// we can accumulate orhpaned markers?
			if (this.marker) {
				this.marker.setMap(null);
				this.marker = null;
			}
		}
		public pointerChanged() {
			return !(this.lat == this.model.schLat && this.lng == this.model.schLong);
		}

    public enableKeyDragZoom = true;
    public addMarker = (event) => {
      this.lat = event.latLng.lat();
      this.lng = event.latLng.lng();

			// seems that if the map is not destoyed moving between states,
			// we can accumulate orhpaned markers?
			if (this.marker) {
				this.marker.setMap(null);
				this.marker = null;
			}

      this.marker = new google.maps.Marker({
        position: event.latLng,
        map: this.map,
        title: this.model.schName,
        draggable: true
      });

      this.state = 'Ready';
      this._scope.$apply();        // becuase this has been involded from the google map outside angular
    };

    public dropMarker = (event: google.maps.MouseEvent) => {
      this.lat = event.latLng.lat();
      this.lng = event.latLng.lng();
      this._scope.$apply();
    };

    // respond to a click on the map by moving the marker there
    public moveMarker = (event: google.maps.MouseEvent) => {
      this.lat = event.latLng.lat();
      this.lng = event.latLng.lng();
      this.marker.setPosition(event.latLng);
      this._scope.$apply();
    };

    // entry point when school is changed
    public moveMap = () => {
      this.state = '';        // indeterminate - force recalculate
      if (this.marker) {
        this.marker.setMap(null);
        this.marker = null;
      }
      // does the school have a lat long?
      // if so, go to it and zoom, if not, show an appropriate region
      if (this.model && this.model.schLat && this.model.schLong) {
        var posn = new google.maps.LatLng(this.model.schLat, this.model.schLong);
        // using global variable:
				this.lat = this.model.schLat;
				this.lng = this.model.schLong;
        this.marker = new google.maps.Marker({
          position: posn,
          map: this.map,
          title: this.model.schName,
          draggable: true
        });
        this.map.panTo(posn);
        this.map.setZoom(16);               // go in close if we have a point
        this.state = 'Ready';
      }
      else {
        var posn = new google.maps.LatLng(-8.312059183575627, 158.50799560546875);
        this.map.panTo(posn);
        this.map.setZoom(6);               // whole of solomons
        this.state = 'Add';
      }
    }

    public saveLatLong() {

      this.model.schLat = this.lat;
      this.model.schLong = this.lng;
      (<Sw.Api.IEditable>this.model).put().then( (newData) => {
				angular.extend(this.model, newData.plain());
				this.lat = this.model.schLat;
				this.lng = this.model.schLong;
      });

    };

    private _mapInitializer = (event, map: google.maps.Map) => {
      this.map = map;

      if (this.enableKeyDragZoom) {
				Sw.Maps.enableKeyDragZoom(map, {
					visualEnabled: true,
					visualPosition: google.maps.ControlPosition.LEFT_TOP,
					veilStyle: {
						webkitUserSelect: "none"
					}
				});
      }
      if (this.model) {
        this.moveMap();
      }
    };

    private _mapClickListener;
    private _markerClickListener;
    private _markerDropListener;

    private _resetHandlers = () => {
      if (this._mapClickListener) {
        google.maps.event.removeListener(this._mapClickListener);
      }
      if (this._markerDropListener) {
        google.maps.event.removeListener(this._markerDropListener);
      }
      switch (this.state) {
        case 'Add':
          // requires the click handler on the map surface
          this._mapClickListener = google.maps.event.addListener(this.map, "click", this.addMarker);
          break;
        case 'Ready':
          // add the handler for dragend
          this._markerDropListener = google.maps.event.addListener(this.marker, "dragend", this.dropMarker);
          this._mapClickListener = google.maps.event.addListener(this.map, "click", this.moveMarker);
      }
    };
  }
	class ComponentOptions implements ng.IComponentOptions {
		public controller = Controller;
		public controllerAs: string = "vm";
		public bindings = {
			model: "<"
		};
		public templateUrl = "school/latlng";
	}

	angular
		.module("sw.common")
		.component("schoolLatLng", new ComponentOptions());

}

