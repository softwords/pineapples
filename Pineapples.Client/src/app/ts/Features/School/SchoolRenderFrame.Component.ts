﻿namespace Pineapples.Schools {

	angular
		.module("pineapples")
		.component("schoolRenderFrame", new Sw.Component.RenderFrameComponent("school"));
}