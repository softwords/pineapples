﻿namespace Pineapples.School {


	class Controller {

		static $inject = ["$http", "$q","Lookups"]
		public constructor(private http: ng.IHttpService, private $q: ng.IQService
			, private lookups: Pineapples.Lookups.LookupService) {
			this.url = "api/selectors/school";
		}

		public $onInit() {
			this.ngModelCtlr.$render = () => {
				this.selectedItem = this.ngModelCtlr.$viewValue ?
					{
						C: this.ngModelCtlr.$viewValue,
						N: this.lookups.byCode("schoolNames", this.ngModelCtlr.$viewValue, "N")
					} : null;
			}
		}

		public url;
		public searchTextChange() {
		}
		public searchText;
		public selectedItem;

		public searchResults: any[];
		private _lastFetchString = "";
		private _fetchData: any[] = [];

		public ngModelCtlr: ng.INgModelController;

		//public doSearch(s: string) {
		//	if (s.length < 3) {
		//		// if we don't have 3 chars don;t do anything
		//		this.searchResults = [];
		//		return;
		//	}
		//	s = s.toLowerCase();
		//	let l = this._fetchData.length;
		//	// we don;t need to fetch:
		//	// the current search is the last fetch
		//	if (s === this._lastFetchString) {
		//		this.searchResults = this._filterLocal(s);
		//		return;
		//	}
		//	// the current search is backspaced and there is already a full list
		//	if (_.startsWith(this._lastFetchString, s) && l === 30) {
		//		this.searchResults = this._filterLocal(s);
		//		return;
		//	}
		//	// the current search string is longer than  the last search and that search returned < 30 records
		//	// ie we have everything
		//	if (_.startsWith(s, this._lastFetchString) && l > 0 && l < 30) {
		//		this.searchResults = this._filterLocal(s);
		//		return;
		//	}
		//	// return the promise with its resolution
		//	this.http.post(this.url, "\"" + s + "\"")
		//		.then((response) => {
		//			this._fetchData = response.data["ResultSet"][0];
		//			this._lastFetchString = s;
		//			this.searchResults = this._fetchData;
		//		});
		//};

		public doSearch(s: string) {
			if (s.length < 3) {
				// if we don't have 3 chars don;t do anything
				return [];
			}
			s = s.toLowerCase();
			let l = this._fetchData.length;
			// we don;t need to fetch:
			// the current search is the last fetch
			if (s === this._lastFetchString) {
				return this.searchResults = this._filterLocal(s);
			}
			// the current search is backspaced and there is already a full list
			if (_.startsWith(this._lastFetchString, s) && l === 30) {
				return this.searchResults = this._filterLocal(s);
			}
			// the current search string is longer than  the last search and that search returned < 30 records
			// ie we have everything
			if (_.startsWith(s, this._lastFetchString) && l > 0 && l < 30) {
				return this.searchResults = this._filterLocal(s);
			}
			// return the promise with its resolution
			return this.http.post(this.url, "\"" + s + "\"")
				.then((response) => {
					this._fetchData = response.data["ResultSet"][0];
					this._lastFetchString = s;
					return this.searchResults = this._fetchData;
				});
		};

		private _filterLocal = (s) => {
			return _.filter(this._fetchData, d => d.N.toLowerCase().indexOf(s) >= 0 ||
				d.C.toLowerCase().indexOf(s) >= 0);
		};

		public onSelected(item) {
			// now we have a unique element that should be returned to the paramChanged event handler
			if (this.ngModelCtlr) {
					this.ngModelCtlr.$setViewValue(item? item.C: null);
			}
		
		};

		public onKey($event) {
			if ($event.which === 13 && this.searchResults.length) {
					this.selectedItem = this.searchResults[0];
					this.onSelected(this.selectedItem);
			}
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "school/finder";
		public require = {
			ngModelCtlr: "ngModel"
		};
		public bindings = {
			ngDisabled: "<",
			ngRequired: "<"
		}
	}


	angular
		.module("pineapples")
		.component("schoolFinder", new ComponentOptions());
}