﻿namespace Pineapples.Schools {


	class Controller {
		public school: School;

		static $inject = ["$mdDialog", "Restangular", "documentsAPI"]
		constructor(

			public mdDialog: ng.material.IDialogService
			, public Restangular: restangular.IService
			, docAPI) {

		}

		public $onChanges(changes) {
		}
		public newDoc() {
			this._uploadDialog().then(() => {
			}, (error) => {
			});
		}

		private _uploadDialog() {
			let options: ng.material.IDialogOptions = {
				locals: { school: this.school },
				controller: SchoolLinkUploadController,
				controllerAs: 'vm',
				bindToController: true,
				templateUrl: "schoollink/uploaddialog"
			}
			return this.mdDialog.show(options);
		}

	}


	class Component implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
				school: "<",
				buttonText: "@"
			};
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "library/newlink";
		}
	}
	angular
		.module("pineapples")
		.component("schoolNewLink", new Component());

}
