﻿namespace Pineapples.Cloudfiles {

	
	class Controller extends Sw.Component.ListMonitor {
		public files: any[];
		public showFilter: boolean = false;

		private entity = "cloudfile";
		public isWaiting: boolean = false;
		public viewMode: string = "files";
		public mode: string = "";

		static $inject = ["ApiUi", "$http", "$q", '$scope', '$state', "$mdDialog"];
		constructor(apiui: Sw.Api.IApiUi
									, private http: ng.IHttpService
									, private q:ng.IQService
									, public scope: Sw.IScopeEx
									, public $state: ng.ui.IStateService
									, private mdDialog: ng.material.IDialogService 
		) {
			super();

			this.monitor(scope, "cloudfiles", "id");
			this.multiSelect = false;

			this.scope.hub.subscribe("SearchComplete", this.searchComplete);

			this.scope.hub.subscribe('SearchError', this.searchError);

			this.scope.hub.subscribe('FindNow', this.findNow);

			this.multiSelect = true;
			this.selectAs 
		}	
		// monitored list implementation
		public monitoredList() {
			return this.files;
		}

		/**
		 * If a new file is added, does it belniong to this list?
		 * In this scenario, redundant in that new files are not added theough the web app
		 * @param newData
		 */
		public isListMember(newData) {
			return true;
		}

		public showItem(id: string) {
			this.$state.go(".item", { id: id });
		}

		// when the cloudfiles has been loaded to an inspection, display that inspection
		public showInspection(id: string) {
			this.$state.go(".inspection", { id: id });
		}

		public $onInit() {
			console.log(this.files);
		}
		
		protected searchComplete = (resultpack) => {
			if (resultpack.entity === this.entity && resultpack.method === 'list') {
				this.files = resultpack.resultset;
				// and now we can get the findParams from the result, so we know if this was a search for photos
				//
				this.viewMode = (resultpack.findParams.filter.filesOrPhotos
					&& resultpack.findParams.filter.filesOrPhotos.toLowerCase() === "photos" ? "photos" : "files");
				this.unselectAll();
			}
		}
		
		protected searchError = (resultpack) => {
			if (resultpack.entity === this.entity) {
				// no point waiting around - this train aint comin'
				this.isWaiting = false;
			}
		};

		protected findNow = (data: Sw.Filter.IFindNowBroadcast) => {
			if (data.filter.entity === this.entity) {
				data.actions.push("list");
				this.isWaiting = true;
			}
		};


		private getFileStatus = file => {
			switch (file?.properties?.uploaded) {
				case "OK":
					return "Loaded";
				case "ERROR":
					return "Error";
				default:
					switch (file?.properties?.surveyCompleted) {
						case "POSITIVE":
							return "Ready";
						case "NEGATIVE":
							return "In Progress";
						case "MERGED":
							return "Merged";
						default:
							return file?.properties?.surveyCompleted;			// for future expansion?
					}
			}

		}
		public removeSelected() {
			// put together a summary of what is selected for deletion
			var selectedFiles = this.getSelectedFrom(this.files);
			var summary = _.countBy(selectedFiles, file => this.getFileStatus(file));
			confirmRemoveController.ShowDialog(this.mdDialog, this.selectionCount(), summary)
				.then(confirm => {
					switch (confirm) {
						case "safe":
							var list = this.files.filter(file => ['Ready', 'In Progress'].indexOf(this.getFileStatus(file)) == -1);
							this.trimSelectedTo(list);
							break;
					}
					if (this.selectionCount() == 0) {
						this.mdDialog.show(
							this.mdDialog.alert()
								.clickOutsideToClose(true)
								.title("Bulk Remove")
								.textContent("No files were selected to remove")
								.ariaLabel("Bulk Remove")
								.ok("Close")
						);
						return;
					}
					this.processRemoveSelected();

				}, error => {
			});
		}
		// fire off the remove api for each element in the seled list

		public processRemoveSelected() {

			var promiseArray = [];
			(<any[]>this.selected).forEach(cloudfile => {
				promiseArray.push(
					this.http.delete(`api/cloudfiles/${cloudfile.id}`)
						.then(result => {
							// bad data handling
							angular.merge(cloudfile, { properties: {} });
							cloudfile.properties.removeStatus = "Removed";
							return "Removed";
						}, error => {
							angular.merge(cloudfile, { properties: {} });
							cloudfile.properties.removeStatus = "Error";
							return "Error";
						})
				);
				
			});
			this.q.all(promiseArray).then(
				results => {
					this.showRemoveResults(results);
				},
				results => {
					console.log(results);
				}
			);
		}

		private showRemoveResults(results) {
			var summary:Object = _.countBy(results, r => r);			// by outcome
			var msg = "Results\n";
			Object.keys(summary).forEach(key => {
				msg = `${msg}\n${key}: ${summary[key]}\n`;
			})
			this.mdDialog.show(
				this.mdDialog.alert()
					.clickOutsideToClose(true)
					.title("Bulk Remove Completed")
					.textContent(msg)
					.ariaLabel("Bulk Remove Completed")
					.ok("Close")
			);
		}
		/**
		 * Return the string to access the file from google drive, via the server's imageprocessor implementation
		 * @param fileId
		 */
		public cloudPhotoUrl(fileId) {
			return `google/${fileId}/0/300`;	// ie rotate=0, size = 300
		}

		/**
		 * For display of createUser on a merged survey, put each name on a separate line
		 * stops the width of the column getting distorted
		 * @param userlist createUser or EditUser list, comma concatenated
		 */
		public splitUsers(userlist: string) {
			// user list should always be there but this accounts for some older bad data
			if (!userlist) {
				return null;
			}
			if (userlist.indexOf(",") == -1) {
				return userlist;
			}
			return userlist.split(",").join('\n');
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings = {
			files: "<"
		};
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "cloudfile/list";
	}

	angular
		.module("pineapples")
		.component("componentCloudfiles", new ComponentOptions());

	// Dialog controllers
	class confirmRemoveController extends Sw.Component.MdDialogController {
		public summary;
		public selectionCount;

		/**
		 * Are there files that are unsafe to delete?
		 */
		public unsafe() {
			return (this.summary.Ready > 0 || this.summary['In Progress'] > 0);
		}

		public static ShowDialog(mdDialog: ng.material.IDialogService, count, summary) {
			let options: ng.material.IDialogOptions = {
				locals: { selectionCount: count, summary: summary},
				controller: confirmRemoveController,
				controllerAs: "vm",
				bindToController: true,
				templateUrl: "cloudfile/dialog/confirmremoveselected"

			}
			return mdDialog.show(options);
		}
	}
}