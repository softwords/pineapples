﻿namespace Pineapples.Cloudfiles {

	//
	export class Cloudfile {

		/**
		 * Factory method to identify and create the appropriate survey object
		 * @param data the structured data, received either as a cloudfile, or from 
		 * the InspectionContent of a SchoolInspection object
		 */
		public static create(data) {
			let survey = data.survey;
			if (survey) {
				switch (survey.type) {
					case "SCHOOL_ACCREDITATION":
						return new Pineapples.Cloudfiles.SchoolStandardAssessment(data);
					case "WASH":
						return new Pineapples.Cloudfiles.Wash(data);
					default:
						return survey;		
				}
			}
			return data;
		}
	}
}