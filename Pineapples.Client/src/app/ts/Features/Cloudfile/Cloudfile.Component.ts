﻿namespace Pineapples.Cloudfiles {
	
	class Controller {
		model: ISchoolStandardAssessment;
		metadata: any;
		public id: string;

		static $inject = ["$scope", "$http", "$q", "$stateParams", "Lookups", "ApiUi", "$mdDialog", "$state"];
		constructor(public scope: Sw.IScopeEx, public http: ng.IHttpService
			, public q : ng.IQService
			, public stateParams: ng.ui.IStateParamsService
			, public lookups: Sw.Lookups.LookupService
			, public apiUi: Sw.Api.IApiUi
			, public mdDialog: ng.material.IDialogService
			, public state: ng.ui.IStateService) {
			this.id = stateParams["id"];
		}

		public trash() {

		}

		public $onInit() {
		}

		/*
		 * load the file as a new SchoolInspection of the appropriate type
		 */
		private approveAction = (data?) => {

			return this.http.get(`api/cloudfiles/${this.id}/approve`)
				.then(result => {
					this.metadata = result.data;
					this.scope.hub.publish("dataUpdated", { what: "cloudfiles", data: result.data });
					return "File approved";
				})
				// note that the process dialog expects its promise to be rejected on error
				// if we catch at this point, it will return reolved 
				// so we need to 'rethrow' the rejection to the process dialog
				// we can do some prehandling here
				// ie this as it standard will return a resolved promise, 
				/// .......     .catch((error) => `Error: ${error}`);;
				.catch((error => {
					console.log(error);
					this.apiUi.showErrorResponse(this.model, error);
					return this.q.reject("Error approving survey");
				}))
		};
		public approve() {
			let locals: Sw.Component.ProcessDialogOptions = {
				action: this.approveAction,
				title: "Approve File",
				confirmMessage: "Approve this survey?",
				inprogressMessage: "Approval in progress..."
			};
			let opts = Sw.Component.MdProcessDialogController.processDialogOptions(
				this.mdDialog
				, locals);
			this.mdDialog.show(opts)
				.then(result => {
					console.log(result);
				});
		}

		public download() {
			this.http.get(`api/cloudfiles/${this.id}/download`);
		};

		private deleteAction = (data?) => {
			return this.http.delete(`api/cloudfiles/${this.id}`)
				.then(() => {
					this.scope.hub.publish("dataDeleted", { what: "cloudfiles", data: this.metadata });
					return "File successfully deleted";
				})
				.catch((error) => `Error: ${error}`);
		};

		public delete()	{ 
			let locals:Sw.Component.ProcessDialogOptions = {
				action: this.deleteAction,
				title: "Delete File",
				confirmMessage: "Delete this survey?",
				inprogressMessage: "Deleting...",
			};
			let opts = Sw.Component.MdProcessDialogController.processDialogOptions(
				this.mdDialog
				, locals);
			this.mdDialog.show(opts)
					.then(result => {
						console.log(result);
						this.state.go("^");		//back to parent becuase we deleted this one
					});
		}
	}

	angular
		.module("pineapples")
		.component("componentCloudfile", new Sw.Component.ComponentOptions(
			"cloudfile/item", Controller, { model: "<", metadata: "<" }));
}