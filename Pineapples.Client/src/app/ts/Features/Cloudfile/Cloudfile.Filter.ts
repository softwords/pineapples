﻿namespace Pineapples.Cloudfiles {

  class CloudfileParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };

    public toFlashString(params: any) {
      var tt = ['SchoolType', 'District',
        'Authority', 'SchoolName'
      ];
      return this.flashStringBuilder(params, tt);
    }

    public fromFlashString(flashstring: string) {
      if (flashstring.trim().length === 0) {
        return;
      }
      let params: any = {};
      var parts = this.tokenise(flashstring);         // 8 3 2015 smarter Tokenise
      var parsed = [];
      for (var i = 0; i < parts.length; i++) {
        var s = parts[i].trim();
        var S = s.toUpperCase();

        if (this.cacheFind(S, params, "schoolTypes", "SchoolType")) {
          continue;
        }
        if (this.cacheFind(S + ' SCHOOL', params, "schoolTypes", "SchoolType")) {
          continue;
        }
        if (this.cacheFind(S, params, "districts", "District")) {
          continue;
        }
        if (this.cacheFind(S, params, "authorities", "Authority")) {
          continue;
        }
        if (s.indexOf('*') >= 0 || s.indexOf('?') >= 0 || s.indexOf('%') >= 0) {
          if (!params.SchoolName) {
            params.SchoolName = s;
            parsed.push(s);
            continue;
          }
        }
        if (parts.length === 1) {
          params.schNo = s;
          parsed.push(s);
        }
      }

      return params;
    }

    protected getParamString(name, value) {
      switch (name) {
        case 'AwardType':
          return this.lookups.findByID('awardTypes', value).C;
        default:
          return value.toString();
      }

    }
  }
  class Filter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "$http", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService
			, protected lookups: Sw.Lookups.LookupService
			, protected http: ng.IHttpService
			, protected identity: Sw.Auth.IIdentity) {
      super();
      this.entity = "cloudfile";
      this.ParamManager = new CloudfileParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // cycle through any filters in the identity, use them
      // to construct the identity filter
      for (var propertyName in this.identity.filters) {
        switch (propertyName) {
          case "Authority":
          case "District":
          case "ElectorateN":
          case "ElectorateL":
          case "SchoolNo":      // support access to a single school - for a principal?
            fltr[propertyName] = this.identity.filters[propertyName];
            break;
        }
      }
      return fltr;
    }

    public createTableCalculator() {
      return new SchoolTableCalculator();
    }
    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer<Sw.Filter.FindConfig>();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "SchoolNo";
			config.defaults.paging.sortDirection = "asc";

			config.defaults.filter = {
				filesOrPhotos: "files",
				status: "ready"
			};
     // config.defaults.viewMode = this.ViewModes[0].key;
			config.current = angular.copy(config.defaults);

      config.identity.filter = this.identityFilter();
      config.reset();

      d.resolve(config);
      return d.promise;
		}

		// custom data acquisition method - List is basically search without paging or view modes
		public list(findParams) {
			this.http.post("api/cloudfiles", findParams.filter)
				.then(result => {
					let data = {
						entity: this.entity
						, method: "list"
						, resultset: result.data
						, summary: { numResults: (<Array<any>>result.data).length }
						, findParams: findParams
					};
					this.broadcastData(data);
				});
		}

  }
  angular
    .module("pineapples")
    .service("CloudfileFilter", Filter);
}
