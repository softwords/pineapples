namespace Pineapples.Cloudfiles {

	let routes = function ($stateProvider) {
		var featurename = 'Cloudfiles';
		var filtername = 'CloudfileFilter';
		var templatepath = "cloudfile";

		// root state for 'schoolInspection' feature
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath);

		state.data = state.data || {};	// just mindlessly do this so we can do it everywhere
		state.data.icon = "cloud";
		state.data.permissions = {
			only: "InspectionOps"
		};

		$stateProvider.state("site.cloudfiles", state);

		state = {
			url: "^/cloudfiles/list/{fileType}",
			params: {
				fileType: { value: null, squash: true }
			},
			views: {
				"renderarea": "componentCloudfiles",
			},
			resolve: {
				findConfig: ["$stateParams", "findConfig",
					($stateParams, findConfig: Sw.Filter.FindConfig) => {
						let params = _.mapValues({ fileType: null }, (v, k) => $stateParams[k] ? $stateParams[k].toUpperCase() : null)
						findConfig.applyLocked(params);
						return findConfig;
					}],
				files: ['$http', "findConfig",
					(http: ng.IHttpService, findConfig: Sw.Filter.FindConfig) => {
						return http.post("api/cloudfiles", findConfig.current.filter)
							.then(response => response.data);
					}]
			}
		};
		state.data = state.data || {};	// just mindlessly do this so we can do it everywhere
		state.data.rendermode = "List";
		$stateProvider.state("site.cloudfiles.list", state);

		state = {
			url: "^/cloudfiles/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.cloudfiles": "componentCloudfile"
			},
			resolve: {
				model: ['$http', '$stateParams',
					(http: ng.IHttpService, $stateParams: ng.ui.IStateParamsService) =>
						http.get(`api/cloudfiles/${$stateParams.id}`)
							.then(response => {
								return Cloudfile.create(response.data);
							})
				],
				metadata: ['$http', '$stateParams',
					(http: ng.IHttpService, $stateParams: ng.ui.IStateParamsService) =>
						http.get(`api/cloudfiles/${$stateParams.id}/metadata`)
							.then(response => response.data)
				]
			}
		};
		$stateProvider.state("site.cloudfiles.list.item", state);

		// display a school inspection in the cloudfile action pane
		// this is a clone of the inspection display
		state = {
			url: "^/cloudfiles/inspection/{id}",
			views: {
				"actionpane@site.cloudfiles": {
					component: "componentSchoolInspection"						// uses school inspection now
				}
			},
		};
		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.schoolinspections.list.item", state);
		$stateProvider.state("site.cloudfiles.list.inspection", state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}
