﻿namespace Pineapples.Cloudfiles {

	angular
		.module("pineapples")
		.component("cloudfileSearcherComponent",
			new Sw.Components.FindConfigComponent("cloudfile/SearcherComponent"));
}