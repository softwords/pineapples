﻿namespace Pineapples.Cloudfiles {

	export interface INamedId {
		id: string,
		name: string
	}

	interface IPhoto {
		localPath: string,
		remotePath: string
	}

	interface IAnswer {
		state: string,
		photo: any[]
	}

	interface ISubCriteria extends INamedId {
		answer: IAnswer,
		hint: string,
		interviewQuestions: string,
		// modified on load
		score: number			// POSITIVE/NEGATIVE => 1/0
	}

	interface ICriteria extends INamedId {
		subcriteria: ISubCriteria[],
		maxscore: number,
		score: number,
		result: string,
		resultvalue: number

	}

	interface IStandard extends INamedId {
		criteria: ICriteria[]
	}

	interface ICategory {
		type: string,
		name: string,
		standard: IStandard[]
	}

	export interface ISchoolStandardAssessment {
		category: ICategory[],
		date: Date,
		region: string,
		schoolId: string,
		schoolName: string,
		type: string,
		version: number,
		rowset: ISSAData[],
		photos: any[]			// this is added on to the base object, it is constructed when the object is loaded
	}

	export interface ISSAData {
		schoolNo: string,
		category: string,
		categoryType: string,
		standard: string,
		criteria: string,
		criteriaSeq: string,
		subcriteria?: string, 
		subSeq?: string,
		score: number,
		maxscore: number,
		// criteria may supply their precalculated resut and resultvalue
		result?: string,
		resultvalue?: number

	}

	/**
	 * represents a Survey in the hierarchical standard=>criteria=>subcriteria format
	 * used by school accreditations
	 * 
	 * */
	export class SchoolStandardAssessment implements ISchoolStandardAssessment {

		constructor(data:any) {
			angular.extend(this, data.survey);
			// we have a problem from the server if there is only 1 criteria in a standard
			// the default conversion from Xml to Json creates an object property
			// not an array with 1 element
			// NewtonSoft Json has ways around this with attribute tagging
			// but then we have more hard-coded semantics about the file on the server
			// ( ie we'd need to create an SSA object to mark up with these JsonXXX attirbutes)
			// This may prove necessary later on ?, but for now, try to just fix it on the client.

			this.photos = [];
			this.category.forEach(category => {
				if (category.standard) {
					if (!Array.isArray(category.standard)) {
						category.standard = [category.standard];
					}
					category.standard.forEach(standard => {
						if (standard.criteria) {
							if (!Array.isArray(standard.criteria)) {
								standard.criteria = [standard.criteria];
							}
							standard.criteria.forEach(criteria => {
								if (criteria.subcriteria) {
									if (!Array.isArray(criteria.subcriteria)) {
										criteria.subcriteria = [criteria.subcriteria];
									}
									criteria.subcriteria.forEach(subcriteria => {
										subcriteria.score = (subcriteria.answer.state == 'POSITIVE' ? 1 : 0);
										if (subcriteria.answer.photo) {
											if (!Array.isArray(subcriteria.answer.photo)) {
												subcriteria.answer.photo = [subcriteria.answer.photo];
											}
											subcriteria.answer.photo.forEach(photo => {
												if (photo.remotePath) {
													this.photos.push({
														photoId: photo.remotePath,
														sourceId: subcriteria.id,
														caption: subcriteria.name
													});
												}
											})
										}
									})
								}
							})
						}
					})
				}
			});
			console.log(this);
		}


		public category: ICategory[];
		public date: Date;
		public region: string;
		public schoolId: string;
		public schoolName: string;
		public type: string;
		public version: number;
		public rowset: ISSAData[];
		public photos: any[];

		public subcriteriaScores: any;
	}
}