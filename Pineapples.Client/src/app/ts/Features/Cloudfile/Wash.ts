﻿namespace Pineapples.Cloudfiles {

	interface IWashAnswer {
		variant?: any;
		item?: any|any[];
	}
	interface IWashQuestion extends INamedId {
		answer: IWashAnswer,
		flags: string
	}


	interface ISubGroup extends INamedId {
		question: IWashQuestion[]
	}

	interface IGroup extends INamedId {
		subgroup: ISubGroup[]
	}


	export interface IWash {
		group: IGroup[],
		date: Date,
		region: string,
		schoolId: string,
		schoolName: string,
		type: string,
		version: number,
	}



	/**
	 * represents a Wash survey 
	 * the hierarchical arrangement is group->subgroup->question
	 * */
	export class Wash implements IWash {

		constructor(data: any) {
			angular.extend(this, data.survey);
			// we have a problem from the server if there is only 1 criteria in a standard
			// the default conversion from Xml to Json creates an object property
			// not an array with 1 element
			// NewtonSoft Json has ways around this with attribute tagging
			// but then we have more hard-coded semantics about the file on the server
			// ( ie we'd need to create an SSA object to mark up with these JsonXXX attirbutes)
			// This may prove necessary later on ?, but for now, try to just fix it on the client.
			this.group.forEach(group => {
				if (!Array.isArray(group.subgroup)) {
					group.subgroup = [group.subgroup];
				}
				group.subgroup.forEach(subgroup => {
					if (!Array.isArray(subgroup.question)) {
						subgroup.question = [subgroup.question];
					}
					subgroup.question.forEach(question => {
						if (question.answer) {
							switch (question.flags) {
								case "VAR|BINARY":
									if (question.answer.variant) {
										if (!Array.isArray(question.answer.variant)) {
											// there is only one answer; it should be an array
											question.answer.variant = [question.answer.variant];
										};
										// variant_item inside each variant should be an array too
										question.answer.variant.forEach(variant => {
											if (variant.variant_item && !Array.isArray(variant.variant_item)) {
												variant.variant_item = [variant.variant_item];
											};
										});
									};
									break;

								case "CHOOSE|MULTIPLE":
									if (question.answer.item && !Array.isArray(question.answer.item)) {
										// there is only one answer; it should be an array
										question.answer.item = [question.answer.item];
									}
							};	// switch on flags
						};		// question.answer
					});
				});
			});
			console.log(this);
		}

		public group: IGroup[];
		public date: Date;
		public region: string;
		public schoolId: string;
		public schoolName: string;
		public type: string;
		public version: number;
	}
}