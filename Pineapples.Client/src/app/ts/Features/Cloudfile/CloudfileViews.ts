namespace Pineapples.Cloudfiles {

	// not actually used, since the list doesn;t use a ui-grid
	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
		]
	};
  let modes = [
    {
      key: "Cloudfiles",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'L1',
            name: 'L1',
            displayName: 'L1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'L2',
            name: 'L2',
            displayName: 'L2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'L3',
            name: 'L3',
            displayName: 'L3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'L4',
            name: 'L4',
            displayName: 'L4',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
  ]; // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};


  angular
    .module('pineapples')
    .run(['CloudfileFilter', pushModes]);
}
