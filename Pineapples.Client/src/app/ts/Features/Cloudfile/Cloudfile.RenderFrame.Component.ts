﻿namespace Pineapples.Cloudfiles {

	angular
		.module("pineapples")
		.component("cloudfileRenderFrame", new Sw.Component.RenderFrameComponent("cloudfile"));
}