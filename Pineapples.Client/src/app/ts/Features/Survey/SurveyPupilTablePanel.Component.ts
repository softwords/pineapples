﻿namespace Pineapples.SurveyEntry {

  interface IBindings {
    schoolNo: string;       // the school no
    year: number;     // the survey year
    tableName: string;      // table name as in metaPupilTableDefs
    tableDescriptiveName: string; // full name (e.g. Enrolment)
  }

  class Controller implements IBindings {

    public gridForm: ng.IFormController;   // angular matches this to the name of the form in the view

    public schoolNo: string;
    public year: number;
    public tableName: string;
    public tableDescriptiveName: string;

    public isWaiting = false;
    public isEditing = false;

    public setEditing(editing: boolean) {
      this.isEditing = editing;
    }

    static $inject = ["schoolsAPI"];

    private _pt: PupilTable;
    private _ptInit: PupilTable;

    constructor(private _api) {
      //console.log("gridpage surveypupiltable");
    }

    public $onChanges(changes: any) {
      // The call to get the pupiltable data is the one generating
      // errors as it gets called without the schoolNo and year arguments
      // Leave this to get fixed when Brian works on the annual school survey data
      // polymorphic CRUD UI.
      this.getPupilTable();
    }

    public get pupilTable(): PupilTable {
      return this._pt;
    };


		public getPupilTable() {
			if (this.schoolNo && this.year && this.tableName) {
				this._api.pupilTable(this.schoolNo, this.year, this.tableName)
					.then((data: PupilTable) => {
						this._pt = data;
						this.schoolNo = data.schoolNo;
						this.tableDescriptiveName = data.schoolNo;
					});
			} else {
				this._pt = null;
			}
    }

    public save() {
      this.isWaiting = true;
      this._api.savePupilTable(this.pupilTable)
        .then((data: PupilTable) => {
          this.isWaiting = false;
          this._pt = data;
          this.schoolNo = data.schoolNo;
          this.pristine();
        });
    }

    public undo() {
      angular.extend(this._pt, this._ptInit);
      this.pristine();
    }

    public refresh() {
      this.getPupilTable();
    }

    private pristine() {
      if (this.gridForm) {
        this.gridForm.$setPristine();
        this._ptInit = angular.copy(this.pupilTable);      // if we want to implement Undo
        this.setEditing(false);
      }
    }
  }

  class PupilTablePanel implements ng.IComponentOptions {
    public bindings: any = {
      schoolNo: "<",
      year: "<",
      tableName: "@"

    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    public templateUrl = "survey/pupiltablepanel";
  }

  angular
    .module("pineapples")
    .component("pupilTablePanel", new PupilTablePanel());
}
