﻿namespace Pineapples.SurveyEntry {

  class Controller {
    // bindings
    public pupilTable: PupilTable;
    public ngDisabled: boolean
    constructor() {
      this.gridBlur();
    }

    public $onChanges(changes) {

    }

    public $onInit() {

		}
    public rowSum(data, rownum, gender) {
      if (data === undefined) {
        return null;
      }
      // get the row
      var row = data[rownum];
      return d3.sum(row, (cc: any) => {
        if (gender) {
          return (cc[gender] ? cc[gender] : null);
        }
        return (cc.M ? cc.M : null) + (cc.F ? cc.F : null);
      });
    };

    public colSum(data, colnum, gender) {
      if (data === undefined) {
        return null;
      }
      var cv = (row, colnum, gender) => {
        var cc = row[colnum];
        if (gender) {
          return (cc[gender] ? cc[gender] : null);
        } else {
          return (cc.M ? cc.M : null) + (cc.F ? cc.F : null);
        }
      };
      var tot = 0;
      data.forEach((row) => {
        tot += cv(row, colnum, gender);
      });
      return tot;
    };

    public gridSum(data, gender) {
      if (data === undefined) {
        return null;
      }
      // get the row
      var cv = (cc, gender) => {
        if (gender) {
          return (cc[gender] ? cc[gender] : null);
        } else {
          return (cc.M ? cc.M : null) + (cc.F ? cc.F : null);
        }
      };
      var tot = 0;
      data.forEach((row) => {
        row.forEach((col) => {
          tot += cv(col, gender);
        });

      });
      return tot;
    };

    // row/column tracking
    public selectedRow = -1;
    public selectedCol = -1;
    public selectedGender = '';

    public gridFocus = (row, col, gender) => {
      this.selectedRow = row;
      this.selectedCol = col;
      this.selectedGender = gender;
    };

    public gridBlur = () => {
      this.selectedRow = -1;
      this.selectedCol = -1;
      this.selectedGender = '';
    };

    // keyboard navigation
    public selectedRowNew = -1;
    public selectedColNew = -1;
    public selectedGenderNew = "";

    /**
     * grid key down handler
     * @param $event
     * @param row row index
     * @param col column index
     * @param gender gender of current selection
     * @returns {}
     */
    public gridkeydown = ($event, row, col, gender) => {
      // allow any numbers wihtout special handling,
      // eithr from keyboard or number pad
      if ($event.keyCode >= 48 && $event.keyCode <= 57) {
        return;
      }
      // number pad numerics
      if ($event.keyCode >= 96 && $event.keyCode <= 105) {
        return;
      }
      // process anything with alt or ctrl
      if ($event.altKey || $event.ctrlKey) {
        return;
      }

      switch ($event.keyCode) {
        case 8:          // backspace
        case 9:          // tab
        case 45:         // insert
        case 46:         // delete
          return;      // don;t prevent default

        case 33:         // page up
          this.selectedRowNew = 0;
          this.selectedColNew = col;
          this.selectedGenderNew = gender;
          break;
        case 34:         // page down
          this.selectedRowNew = this.pupilTable.rows.length - 1;
          this.selectedColNew = col;
          this.selectedGenderNew = gender;
          break;
        case 35:         // end
          this.selectedRowNew = this.pupilTable.rows.length - 1;
          this.selectedColNew = this.pupilTable.cols.length - 1;
          this.selectedGenderNew = 'F';
          break;

        case 36:         // home
          this.selectedRowNew = 0;
          this.selectedColNew = 0;
          this.selectedGenderNew = 'M';
          break;
        case 37:         // left
          this.selectedRowNew = row;
          if (gender === "M") {
            if ((this.selectedColNew = (col === 0 ? this.pupilTable.cols.length - 1 : --col)) === this.pupilTable.cols.length - 1) {
              this.selectedRowNew = (row === 0 ? this.pupilTable.rows.length - 1 : --row);
            } else {
              this.selectedColNew = col;
            }
          }
          this.selectedGenderNew = (gender === "M" ? "F" : "M");
          break;
        case 38:         // up

          this.selectedColNew = col;
          this.selectedGenderNew = gender;
          this.selectedRowNew = (row === 0 ? this.pupilTable.rows.length - 1 : --row);
          if (this.selectedRowNew === this.pupilTable.rows.length - 1) {
            this.selectedGenderNew = (gender === "M" ? "F" : "M");
            if (this.selectedGenderNew === "F") {
              // we wrapped a column
              this.selectedColNew = (col === 0 ? this.pupilTable.cols.length - 1 : --col);
            }
          }
          break;
        case 13:         // enter
        case 39:  // right
          this.selectedRowNew = row;
          if (gender === "F") {
            if ((this.selectedColNew = (++col === this.pupilTable.cols.length ? 0 : col)) === 0) {
              this.selectedRowNew = (++row === this.pupilTable.rows.length ? 0 : row);
            }
          } else {
            this.selectedColNew = col;
          }
          this.selectedGenderNew = (gender === "M" ? "F" : "M");
          break;
        case 40:

          this.selectedColNew = col;
          this.selectedGenderNew = gender;
          this.selectedRowNew = (++row === this.pupilTable.rows.length ? 0 : row);
          if (this.selectedRowNew === 0) {
            this.selectedGenderNew = (gender === "M" ? "F" : "M");
            if (this.selectedGenderNew === "M") {
              // we wrapped a column
              this.selectedColNew = (++col === this.pupilTable.cols.length ? 0 : col);
            }
          }
          break;

      }
      $event.preventDefault();
    };

    /**
     *
     * @param row
     * @param col
     * @param gender
     * @returns true if focus should move to the row/col/gender cell
     *  based on
     * https://www.emberex.com/programmatically-setting-focus-angularjs-way/
     */
    public focusRequested = (row, col, gender) => {
      var result = (row === this.selectedRowNew && col === this.selectedColNew && gender === this.selectedGenderNew);
      return result;
    };

    public gridkeypress($event) {
      if ($event.keyCode >= 48 && $event.keyCode <= 57) {
        return;
      }
      switch ($event.keyCode) {
        case 9:
        case 13:
        case 37:
        case 38:
        case 39:
        case 40:
          return;

      }
      $event.preventDefault();
    };
  }

 
  class Component implements ng.IComponentOptions {
    public bindings: any = {
      pupilTable: "<",
      ngDisabled: "<"

    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    public templateUrl = "survey/pupiltablegrid";
  }

  angular
    .module("pineapples")
    .component("pupilTableGrid", new Component())

    // this directive can shift the focus based on an evaluation
    // https://www.emberex.com/programmatically-setting-focus-angularjs-way/
    .directive('syncFocusWith', ["$timeout", "$rootScope", ($timeout, $rootScope) => {
      return {
        restrict: 'A',
        scope: {
          focusValue: "=syncFocusWith"
        },
        link: ($scope, $element, attrs) => {
          $scope.$watch("focusValue", (currentValue, previousValue) => {
            if (currentValue === true && !previousValue) {
              // $timeout prevents the event getting lost becuase a digest is already in progress
              $timeout(() => {
                $element[0].focus();
              });
            } else if (currentValue === false && previousValue) {
              $timeout(() => {
                $element[0].blur();
              });
            }
          });
        }
      };
    }]);
}
