﻿namespace Pineapples.Books {

	angular
		.module("pineapples")
		.component("bookRenderFrame", new Sw.Component.RenderFrameComponent("book"));
}