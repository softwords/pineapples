﻿namespace Pineapples.Books {

	// don;t need to subclass the controller 
	// optional controller argument is not supplied to FindConfigComponent, so the base is used
	//class Controller extends Sw.Components.FindConfigOperations {
	//}

	angular
		.module("pineapples")
		.component("bookSearcherComponent"
			, new Sw.Components.FindConfigComponent("book/SearcherComponent"));
}