﻿namespace Pineapples.QuarterlyReportsX {


  class QuarterlyReportXParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };
 
  }

  class QuarterlyReportXFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "quarterlyReportsXAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.entity = "quarterlyreportx";
      this.ParamManager = new QuarterlyReportXParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // not required
      return fltr;
    }

    //public createTableCalculator() {
    //  return new SchoolTableCalculator();
    //}

    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer<Sw.Filter.FindConfig>();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "qrID";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "School";
      config.defaults.table.col = "InspYear";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      //config.tableOptions = "schoolFieldOptions";
      //config.dataOptions = "schoolDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      d.resolve(config);
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("QuarterlyReportXFilter", QuarterlyReportXFilter);
}
