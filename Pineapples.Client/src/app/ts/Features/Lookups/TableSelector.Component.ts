﻿namespace Pineapples.Lookups {
  class Controller implements Sw.Component.ISuperGridParent{
		public tableList: string[];
		private entityType: string;
		private entityDescription: string;

		public srcrows: any[];		// input unsorted
		public rowset: any[];			// bound to the supergrid

    static $inject = ["$state"];
		public constructor(public state: ng.ui.IStateService) { }

		public $onInit() {
			if (this.entityType) {
				// we have to find case-insensive here
				this._table = _.find(this.tableList, d => d.toLowerCase() == this.entityType.toLowerCase());
			}
			this.rowset = this.srcrows;
		}

    private _table: string;
    public get table() {
      return this._table;
    }
    public set table(value) {
      this._table = value;
      this.state.go(this.state.current, { lkp: this.table });
		};

		public get heading() {
			let frameTitle = this.state.current.data["frameTitle"]
			let mode = this.state.current.data["rendermode"] || "Lookups";
			return (frameTitle ? (frameTitle + ": "):"") + mode;
		}

		public prettyName(tablename) {
			//https://stackoverflow.com/questions/5582228/insert-space-before-capital-letters
			return tablename.replace(/([A-Z]+)/g, ' $1').trim();
		}

		
		public registerGridApi(gridApi: uiGrid.IGridApi) { }
		public onSortClear(): void {
			this.rowset = this.srcrows;
		};
		public onSort(column: string, direction: string) {
			this.rowset = _.orderBy(this.srcrows, column);
			if (direction == "desc") {
				this.rowset = this.rowset.reverse();
			}
		}
		public onShowPage(newPage: number, pageSize: number) { }
		public onHighlightChanged(rowEntity: any, isSelected: boolean) { }
		public onAction(actionName: string, columnField: string, rowEntity: any) { }
	
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
				tableList: "<",
				entityType: "<",
				entityDescription: "<",
				srcrows: "<rowset",
				viewMode: "<",
				isEditing: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "lookup/tableselector";
    }
  }
  angular
    .module("pineapples")
    .component("componentTableSelector", new Component());
}