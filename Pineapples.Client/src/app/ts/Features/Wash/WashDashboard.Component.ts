﻿namespace Pineapples.Dashboards {
	export namespace Wash {
		export interface IxfData {
			SurveyYear: number,
			DistrictCode: string,
			District: string,
			AuthorityCode: string,
			Authority: string,
			AuthorityGovtCode: string,
			AuthorityGovt: string,
			SchoolTypeCode: string,
			SchoolType: string,
			Question: string,
			Response: string,
			Item: string,
			Answer: string,
			Num: number,
			NumThisYear: number
		}

		export interface IxfDataToilet {
			schNo: string,
			SurveyYear: number,
			inspID: number,
			InspectionYear: number,
			DistrictCode: string,
			District: string,
			AuthorityCode: string,
			Authority: string,
			AuthorityGovtCode: string,
			AuthorityGovt: string,
			SchoolTypeCode: string,
			SchoolType: string,
			TotalF: number,
			UsableF: number,
			EnrolF: number,
			TotalM: number,
			UsableM: number,
			EnrolM: number,
			TotalC: number,
			UsableC: number,
			Total: number,
			Usable: number,
			Enrol: number
		}

		export interface IxfDataWater {
			schNo: string,
			SurveyYear: number,
			inspID: number,
			InspectionYear: string,
			DistrictCode: string,
			District: string,
			AuthorityCode: string,
			Authority: string,
			AuthorityGovtCode: string,
			AuthorityGovt: string,
			SchoolTypeCode: string,
			SchoolType: string,
			PipedWaterSupplyCurrentlyAvailable: number,
			PipedWaterSupplyUsedForDrinking: number,
			ProtectedWellCurrentlyAvailable: number,
			ProtectedWellUsedForDrinking: number,
			UnprotectedWellSpringCurrentlyAvailable: number,
			UnprotectedWellSpringUsedForDrinking: number,
			RainwaterCurrentlyAvailable: number,
			RainwaterUsedForDrinking: number,
			BottledWaterCurrentlyAvailable: number,
			BottledWaterUsedForDrinking: number,
			TankerTruckCartCurrentlyAvailable: number,
			TankerTruckCartUsedForDrinking: number,
			SurfacedWaterCurrentlyAvailable: number,
			SurfacedWaterUsedForDrinking: number
		}

		export interface INumNumYear {
			Num: number, 
			NumYear: number
		}

		export interface ILevelNums {
			[level:string]: INumNumYear
		}

		export interface IQuestion {
			QID: string;
			QName: string;
			QFlags: string;
		}

		export enum WashQuestionTypes {
			BINARY = "BINARY",
			TERNARY = "TERNARY",
			SINGLE = "CHOOSE|SINGLE",
			MULTIPLE = "CHOOSE|MULTIPLE",
			NUMERIC = "INPUT|NUMERIC"
		}
	}

	export class WashDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {


		private xReduce = _.memoize((tableName, ...inputs) => this.xFilter.xReduce.bind(this.xFilter)(...inputs))
		private groupReduceSum = _.memoize((groupName, dim, va) => dim.group().reduceSum(va));

		// additional data
		private tables: any[];

		// override with strongly typed crossfilter
		public xf: CrossFilter.CrossFilter<Wash.IxfData>;
		public xf2: CrossFilter.CrossFilter<Wash.IxfDataToilet>;			// for toilets
		public xf3: CrossFilter.CrossFilter<Wash.IxfDataWater>;			// for water
		public questions: Wash.IQuestion[];
		public toiletData: any[];
		public waterData: any[];

		// dimensions and groups based on Accreditations
		public dimSurveyYear: CrossFilter.Dimension<Wash.IxfData, number>;
		public dimDistrictCode: CrossFilter.Dimension<Wash.IxfData, string>;
		public dimAuthorityCode: CrossFilter.Dimension<Wash.IxfData, string>;
		public dimAuthorityGovtCode: CrossFilter.Dimension<Wash.IxfData, string>;
		public dimSchoolTypeCode: CrossFilter.Dimension<Wash.IxfData, string>;

		// for toilets
		public dimSurveyYear2: CrossFilter.Dimension<Wash.IxfDataToilet, number>;
		public dimDistrictCode2: CrossFilter.Dimension<Wash.IxfDataToilet, string>;
		public dimAuthorityCode2: CrossFilter.Dimension<Wash.IxfDataToilet, string>;
		public dimAuthorityGovtCode2: CrossFilter.Dimension<Wash.IxfDataToilet, string>;
		public dimSchoolTypeCode2: CrossFilter.Dimension<Wash.IxfDataToilet, string>;
		// for water
		public dimSurveyYear3: CrossFilter.Dimension<Wash.IxfDataWater, number>;
		public dimDistrictCode3: CrossFilter.Dimension<Wash.IxfDataWater, string>;
		public dimAuthorityCode3: CrossFilter.Dimension<Wash.IxfDataWater, string>;
		public dimAuthorityGovtCode3: CrossFilter.Dimension<Wash.IxfDataWater, string>;
		public dimSchoolTypeCode3: CrossFilter.Dimension<Wash.IxfDataWater, string>;

		public dimQuestion: CrossFilter.Dimension<Wash.IxfData, string>;

		public grpDistrictCode: CrossFilter.Group<Wash.IxfData, string, number>;
		public grpAuthorityCode: CrossFilter.Group<Wash.IxfData, string, number>;
		public grpAuthorityGovtCode: CrossFilter.Group<Wash.IxfData, string, number>;

		public xtabDistrictCode: CrossFilter.Group<Wash.IxfData, string, Accreditations.ILevelNums>;
		public xtabAuthorityCode: CrossFilter.Group<Wash.IxfData, string, Accreditations.ILevelNums>;
		public xtabSurveyYear: CrossFilter.Group<Wash.IxfData, string, Accreditations.ILevelNums>;

		public layers: any[];

		public $onChanges(changes) {

		}

		public question(questionID): Wash.IQuestion {
			return <Wash.IQuestion>_.find(this.questions, { "QID": questionID });
		}

		public $onInit() {
			// data has been retrieved by the ui-router as a Resolve
			// and passed to the dashboard as a binding - process it into a crossfilter
			// create the crossfilter from the warehouse table
			this.xf = crossfilter(this.tables[0]);
			this.xf2 = crossfilter(this.tables[2]);
			this.xf3 = crossfilter(this.tables[3]);
			this.createDimensions();
			this.questions = this.tables[1];
			this.toiletData = this.tables[2];
			this.waterData = this.tables[3];
			this.dashboard = this;
		}

		public onOptionChange(data:IOptionChangeData, sender) {
			if (data.selectedYear) {
				this.dimSurveyYear.filter(this.options.selectedYear);
				this.dimSurveyYear2.filter(this.options.selectedYear);
				this.dimSurveyYear3.filter(this.options.selectedYear);
			}
			if (data.selectedDistrict) {

				this.dimDistrictCode.filter(this.options.selectedDistrict);
				this.dimDistrictCode2.filter(this.options.selectedDistrict);
				this.dimDistrictCode3.filter(this.options.selectedDistrict);
			}
			if (data.selectedAuthorityGovt) {
				this.dimAuthorityGovtCode.filter(this.options.selectedAuthorityGovt);
				this.dimAuthorityGovtCode2.filter(this.options.selectedAuthorityGovt);
				this.dimAuthorityGovtCode3.filter(this.options.selectedAuthorityGovt);
			}
			if (data.selectedAuthority) {
				this.dimAuthorityCode.filter(this.options.selectedAuthority);
				this.dimAuthorityCode2.filter(this.options.selectedAuthority);
				this.dimAuthorityCode3.filter(this.options.selectedAuthority);
			}
			if (data.selectedSchoolType) {
				this.dimSchoolTypeCode.filter(this.options.selectedSchoolType);
				this.dimSchoolTypeCode2.filter(this.options.selectedSchoolType);
				this.dimSchoolTypeCode3.filter(this.options.selectedSchoolType);

			}

			if (data.selectedQuestion) {
				this.dimQuestion.filter(this.options.selectedQuestion);
			}

			// can now use crossfilter to filter these lists
			this.toiletData = this.xf2.allFiltered();
			this.waterData = this.xf3.allFiltered();

			// always call the super version so that the clients get to know about the option change as well
			super.onOptionChange(data, sender);
		}

		private elSelector = ({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel)["L"];
		private classLevelSelector = this.xFilter.getPropAccessor("ClassLevel")

		public numYearReducer = d => {
		return {
			NumThisYear: d.NumThisYear,
			Num: d.Num
			//NumThisYear: d.NumThisYear
		};
	};
		public createDimensions() {
			// create the dimensions

			// Lookup by code like others now...
			//this.dimAuthorityGovtCode = this.xf.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovtCode);
			//	return ag ? ag.N : null;
			//});
			this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
			this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);
			this.dimAuthorityCode = this.xf.dimension(d => d.AuthorityCode);
			this.dimAuthorityGovtCode = this.xf.dimension(d => d.AuthorityGovtCode);
			this.dimSchoolTypeCode = this.xf.dimension(d => d.SchoolTypeCode);
			this.dimQuestion = this.xf.dimension(d => d.Question);

			// function to shape the value on each row of these reductions

			this.xtabDistrictCode = this.xFilter.xReduce(this.dimDistrictCode, "Answer",this.numYearReducer);
			this.xtabSurveyYear = this.xFilter.xReduce(this.dimSurveyYear, "InspectionResult", d => d.Num);
			this.xtabAuthorityCode = this.xFilter.xReduce(this.dimAuthorityCode, "InspectionResult", d => d.Num);

			//console.log("xtabDistrictCode.all(): ", this.xtabDistrictCode.all());
			//console.log("xtabSurveyYear.all()", this.xtabSurveyYear.all());

			// crossfilters 2 and 3 have dimensions to allow us to filter the raw rows
			// Lookup by code like others now...
			//this.dimAuthorityGovtCode2 = this.xf2.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovtCode);
			//	return ag ? ag.N : null;
			//});
			//this.dimAuthorityGovtCode2 = this.xf2.dimension(d => d.AuthorityGovtCode);
			this.dimSurveyYear2 = this.xf2.dimension(d => d.SurveyYear);
			this.dimDistrictCode2 = this.xf2.dimension(d => d.DistrictCode);
			this.dimAuthorityCode2 = this.xf2.dimension(d => d.AuthorityCode);
			this.dimAuthorityGovtCode2 = this.xf2.dimension(d => d.AuthorityGovtCode);
			this.dimSchoolTypeCode2 = this.xf2.dimension(d => d.SchoolTypeCode);

			// Lookup by code like others now...
			//this.dimAuthorityGovtCode3 = this.xf3.dimension(d => {
			//	let ag = this.lookups.cache["authorityGovts"].byCode(d.AuthorityGovtCode);
			//	return ag ? ag.N : null;
			//});
			//this.dimAuthorityGovtCode3 = this.xf3.dimension(d => d.AuthorityGovtCode);
			this.dimSurveyYear3 = this.xf3.dimension(d => d.SurveyYear);
			this.dimDistrictCode3 = this.xf3.dimension(d => d.DistrictCode);
			this.dimAuthorityCode3 = this.xf3.dimension(d => d.AuthorityCode);
			this.dimAuthorityGovtCode3 = this.xf3.dimension(d => d.AuthorityGovtCode);
			this.dimSchoolTypeCode3 = this.xf3.dimension(d => d.SchoolTypeCode);

			this.dashboard = this;
			// set the surveyYear to be the most recent in the warehouse data, rather than the most recent surveyYear in the lookups
			// we dont want to get empty data on the initial display if the warehouse is not yet generated for the current year
			if (!this.options.selectedYear) {
				this.options.selectedYear = this.dimSurveyYear.top(1)[0].SurveyYear;
			}

		}

	}


	class Component implements ng.IComponentOptions {
		public bindings = {
			tables: "<"	,			// array of data tables
			options: "<"			// get the options
		};
		public controller = WashDashboard;
		public controllerAs = "vm"
		public templateUrl: string = "wash/dashboard";
	}

	angular
		.module("pineapples")
		.component("washDashboard", new Component());

	// component options for dashboard children
	export class WashDashboardComponent extends DashboardChildComponent implements ng.IComponentOptions {
		public get feature() {
			return "wash";
		}
	}
}
