namespace Pineapples.Dashboards.Wash {

	class Controller extends DashboardChild implements IDashboardChild {

		public defaultTitle = "Toilets";

		// bindings
		public options: any;
		public dashboard: WashDashboard;
		public chartData: Sw.Charts.AnnotatedCrossfilter

		public currentQuestion: IQuestion;
		public group;

		static $inject = ["$timeout", "$mdDialog", "$state", "Lookups", "reportManager", "$http"];
		constructor(
			protected timeout: ng.ITimeoutService
			, mdDialog: ng.material.IDialogService
			, protected $state: ng.ui.IStateService
			, public lookups: Sw.Lookups.LookupService
			, rmgr: Pineapples.Reporting.IReportManagerService
			, public http: ng.IHttpService) {
			super(timeout, mdDialog, $state, lookups, rmgr, http);
		}

		//--------------------------------------------------------------------------------------//
		// View Option Management
		//
		// Select between Toilet Numbers, Usable Toilets Numbers
		// Enrolment (raw enrolment)
		// Pupils / Toilet
		// Pupils / Usable Toilet

		//
		//--------------------------------------------------------------------------------------//
		public get viewOptions() {
			return [
				"Toilets (Total)"
				, "Usable Toilets"
				, "% Usable"
				, "% Usable (gender)"
				, "Pupils / toilet"
				, "Pupils / toilet (gender)"
				, "Pupils / usable toilet"
				, "Pupils / usable toilet (gender)"
				, "Pupils"
				, "Pupils (mirror format)"
			]
		}


		private _selectedViewOption = 0;
		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.onViewOptionChange();
			}
		}

		public onViewOptionChange() {
			this.viewOptionMirror();
			this.refreshChart();
		};

		// when calculating percentages for females/males, it is incorrect to stack them
		// but, there is not space to do columns
		// solution is to mirror

		private MIRROR = [0];

		public mirrors;

		private viewOptionMirror() {
			switch (this.selectedViewOption) {
				case 3:
				case 5:
				case 7:
				case 9:
					this.mirrors = this.MIRROR;
					break;
				default:
					this.mirrors = null;
			}
		}

		//--------------------------------------------------------------------------------------//
		// Drill down support

		// common base to support drill down into education expenditure data
		// this will show the current and total expenditure
		//--------------------------------------------------------------------------------------//
		public drillData: any[]; // lowest level data for drill down
		public drillX: any[];		// cross tabbed version made by crossfilter
		public drillItems: any[];
		public drillRow(inspID) {
			return _.find(this.drillData, { inspID: inspID });
		}

		///////////////////////////////////////////////////////////
		// Accessors for 'annotated' data
		///////////////////////////////////////////////////////////
		private valueAccessor: Sw.Charts.ValueAccessor = (row, column, colIdx) => {
			switch (this.selectedViewOption) {
				case 0:			// Total toilets
					return row[["TotalF", "TotalM", "TotalC"][colIdx]];
				case 1:			// usable toilets
					return row[["UsableF", "UsableM", "UsableC"][colIdx]];

				case 2: // % usable of total
					// can;t stack percentages, and by school, would be too wide to have separate bars
					return (row.Total ? Math.round(1000 * row.Usable / row.Total) / 10 : 0)

				case 3: // % usable of total for boys and girls
					switch (column) {
						case "Female":
							return (row.TotalF ? Math.round(10 * row.UsableF / row.TotalF) / 10 : 0)

						case "Male":
							(row.TotalM ? Math.round(10 * row.UsableM / row.TotalM) / 10 : 0)
					}

				case 4:			//  Pupils per total toilet 
					return (row.Total ? Math.round(10 * row.Enrol / row.Total) / 10 : 0)
				case 5:			//  Pupils per total toilet (gendered)
					switch (column) {
						case "Female":
							return (row.TotalF ? Math.round(10 * row.EnrolF / row.TotalF) / 10 : 0)
							
						case "Male":
							return (row.TotalM ? Math.round(10 * row.EnrolM / row.TotalM) / 10 : 0)
					}
				case 6:			//  Pupils per usable toilet
					return (row.Usable ? Math.round(10 * row.Enrol / row.Usable) / 10 : 0)
				case 7:			//  Pupils per usable toilet (gendered)
					switch (column) {
						case "Female":
							return (row.UsableF ? Math.round(10 * row.EnrolF / row.UsableF) / 10 : 0)

						case "Male":
							return (row.UsableM ? Math.round(10 * row.EnrolM / row.UsableM) / 10 : 0)
					}
				case 8:			//  Pupil numbers
				case 9:			// mirror
					return row[["EnrolF", "EnrolM"][colIdx]];
			}
		}

		private columnCollector: Sw.Charts.ColumnCollector = () => {
			switch (this.selectedViewOption) {
				case 0:			// Total toilets
				case 1:			// usable toilets
					return ["Girls", "Boys", "Common"];
	
				case 2: // % usable of total
					// can;t stack percentages, and by school, would be too wide to have separate bars
					return ["Usable %"];
					
				case 3: // % usable of total for boys and girls
					return ["Girls", "Boys"];
				case 4:			//  Pupils per total toilet 
					return ["Pupils"];
				case 5:			//  Pupils per total toilet (gendered)
					return ["Female", "Male"];
				case 6:			//  Pupils per usable toilet
					return ["Pupils"]
				case 7:			//  Pupils per usable toilet (gendered)
				case 8:			//  Pupil numbers
				case 9:			// pupil mirrored
					return ["Female", "Male"];
			}
		}

		public onDashboardReady() {
			// this class needs to build its chart from toiletData - a simple collection
			// that is continuously refiltered. toiletData is produced from a crossfilter
			// using crossfilter.allFiltered()
			// We do this declaratively, we create an AnnotatedCrossfilter object as the data source
			this.chartData = {
				crossfilter: this.dashboard.xf2,
				rowId: "schNo",
				columns: this.columnCollector,
				valueAccessor: this.valueAccessor
			}
		}

		public onOptionChange(optionchange) {
			if (optionchange.selectedQuestion) {
				// if the question changes, we should destoy the palette, becuase the 
				// assignment of colours from the series to the palette needs to start again
				// otherwise we get old items (ie allocated palette members) showing up in the legend
				this.defaultPalette = "category10";

			}
			this.redrill();
			this.refreshChart();
			//this.makeChartData();
		}

		public onChildSelected() {
			if (this.isSelected()) {
				this.redrill();
				return;
			}
			this.drillfltr = null;
			this._selectedcell = "";
		}
		protected drillfltr;

		protected redrill() {
			if (this.drillfltr && this.isSelected()) {
				angular.merge(this.drillfltr, {
					District: this.options.selectedDistrict,
					Year: this.options.selectedYear
				});
				this.getDrillData();
			}
		}
		public drilldown(districtCode: string, sectorCode: string, costCentreCode) {
			this.drillfltr = {
				District: this.options.selectedDistrict,
				SurveyYear: this.options.selectedYear,
				Question: this.options.selectedQuestion
			};
			this._selectedcell = `${districtCode}|${sectorCode}|${costCentreCode}`;
			this.getDrillData()
				.then(() => {
					this.setSelected();
				});;
		}
		private getDrillData() {
			return this.http.post("api/warehouse/wash/responses", this.drillfltr)
				.then(result => {
					// result is 2 data sets:
					// the data filtered by question, for individual schools (schools are filtered by options)
					// the set of possible values ( e.g. '<item>' nodes for SINGLE or MULTIPLE)
					this.drillData = result.data[0];
					let xf = crossfilter(this.drillData);
					let dx = xf.dimension(d => d["inspID"]);
					this.drillX = this.dashboard.xFilter.xReduce(dx, "Response", d => d.Num).all();
					//this.drillData = <any[]>result.data[0];
					this.drillItems = <any[]>result.data[1];
				});
		}

		protected _selectedcell: string;
		protected isSelectedCell(districtCode: string, sectorCode: string, costCentreCode) {
			return this.isSelected() && this._selectedcell == `${districtCode}|${sectorCode}|${costCentreCode}`;
		}

		////////////////////////////////////////////////////////////////////////////////////
		// Chart callbacks
		////////////////////////////////////////////////////////////////////////////////////
		public clickHandler(params) {
			console.log("Click", params);
			this.drilldown(null, null, null);
		}
		public tooltipper(datum) {
		}

		public onChartRender(option: echarts.EChartOption, echart: echarts.ECharts, renderedType: string) {
			option.series.forEach(s => {
				angular.merge(s, {
					animation: true,
					animationDuration: 0,
					animationDurationUpdate: 10,
					animationDelayUpdate: 0,
				})
			})
			option.animation = false;
		}
	}

	angular
		.module("pineapples")
		.component("washToiletsBySchool", new WashDashboardComponent(Controller, "ToiletsBySchool"))
}