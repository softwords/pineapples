﻿namespace Pineapples.Dashboards.Wash {

	type WashDrillFilter = {
		SurveyYear?: number,
		Question?: string,
		District?: string,
		Authority?: string
	}
	class Controller extends DashboardChild implements IDashboardChild {

		// bindings
		public options: WashOptions;
		public dashboard: WashDashboard;
		public chartData: Sw.Charts.AnnotatedGroup;

		private binaryData: Sw.Charts.AnnotatedGroup;
		private choiceData: Sw.Charts.AnnotatedGroup;

		public currentQuestion: IQuestion; 

		public defaultTitle= "District Totals";

		static $inject = ["$timeout", "$mdDialog", "$state", "Lookups", "reportManager", "$http","$q"];
		constructor(
			protected timeout: ng.ITimeoutService
			, mdDialog: ng.material.IDialogService
			, protected $state: ng.ui.IStateService
			, public lookups: Sw.Lookups.LookupService
			, rmgr: Pineapples.Reporting.IReportManagerService
			, public http: ng.IHttpService
			, private q: ng.IQService) {
			super(timeout, mdDialog, $state, lookups, rmgr, http);
		}

		//--------------------------------------------------------------------------------------//
		// View Option Management
		//
		//--------------------------------------------------------------------------------------//
		public get viewOptions() {
			return [
				`Evaluated in ${this.options.selectedYear}`
				, `Cumulative to ${this.options.selectedYear}`
			]
		}

		private _selectedViewOption = 1;
		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.onViewOptionChange();
			}
		}

		public get selectedViewField() {
			return ["NumThisYear", "Num"][this.selectedViewOption];
		}

		//
		public onViewOptionChange() {
			this.setChartData();
		};

		public onDashboardReady() {
			// set up the crossfilter groups related to questions
			// there are 2 - because of the differences between BINARY/TERNARY 
			// and SINGLE/MULTIPLE question types
			// The xtab in these groups is ROW=District, COL= question ID
			// The structure below each question ID is defined by these reducers:
			let responseReducer = (d) => ({
				[d.Response]: {
					NumThisYear: d.NumThisYear,
					Num: d.Num
				}
			});
			let answerReducer = (d) => ({
				[d.Answer]: {
					NumThisYear: d.NumThisYear,
					Num: d.Num
				}
			});
			// These are the accessor functions for the AnnotatedGroups - these are the same in the 2 cases
			// Note that both functions depend on this.currentQuestion and this.selectedViewField - so 
			// they never need to be changed. 
			let columnCollector = (kv) => Object.keys(kv.value[this.currentQuestion.QID]);
			let valueAccessor = (kv, col) => {
				let q = kv.value[this.currentQuestion.QID];
				if (!q) {
					return 0;
				}
				let c = q[col];
				if (!c) {
					return 0;
				}
				return c[this.selectedViewField] || 0;
			}
			// make the group and AnnotatedGRoup to support binary ad ternary questions:
			let localgroup = this.dashboard.xFilter.xReduce(this.dashboard.dimDistrictCode, "Question"
				, answerReducer);

			this.binaryData = {
				group: localgroup,
				columns: columnCollector,
				valueAccessor: valueAccessor
			};
			// make the group and AnnotatedGroup to support single and multiple choice:
			localgroup = this.dashboard.xFilter.xReduce(this.dashboard.dimDistrictCode, "Question"
				, responseReducer);

			this.choiceData = {
				group: localgroup,
				columns: columnCollector,
				valueAccessor: valueAccessor
			};

		}

		public onOptionChange(optionchange) {
			if (optionchange.selectedQuestion) {

			}
			this.setChartData();
			if (this.isSelected()) {
				this.drilldown();
				return;
			} 

		}

		public onChildSelected() {
			if (this.isSelected()) {
				this.drilldown();
				return;
			} 
		}

		//--------------------------------------------------------------------------------------//
		// Drill down support
		//--------------------------------------------------------------------------------------//
		// Drilldown data is required when the component is 'Selected'.
		// we have to read it when :
		// - the compponent becomes selected and the 
		// - the component is Selected, and an option is changed or cleared
		// - we must have a question... and a year

		protected drillfltr: WashDrillFilter = {};
		public drillData: any[]; // the drill down (ie school level) data
		public drillX: any[];		// cross tabbed version made by crossfilter
		public drillItems: any[];
		public drillRow(inspID) {
			return _.find(this.drillData, { inspID: inspID });
		}

		private drillIsCurrent(): boolean {
			return (this.options.selectedYear == this.drillfltr.SurveyYear
				&& this.options.selectedQuestion == this.drillfltr.Question
				&& this.options.selectedDistrict == this.drillfltr.District
				&& this.options.selectedAuthority == this.drillfltr.Authority
			);
		}

		public drilldown() {
			this.getDrillData()
				.then(() => {

					let xf = crossfilter(this.drillData);
					xf.dimension(d => "")
					let dx = xf.dimension(d => d["inspID"]);
					switch (this.currentQuestion.QFlags) {
						case WashQuestionTypes.BINARY:
						case WashQuestionTypes.TERNARY:
							// output the disaggregation (e.g district as ROW, the answer as COL, the num as value)
							this.drillX = this.dashboard.xFilter.xReduce(dx, "Question", d => d.Num).all();
							break;
						case WashQuestionTypes.SINGLE:
						case WashQuestionTypes.MULTIPLE:
							this.drillX = this.dashboard.xFilter.xReduce(dx, "Response", d => d.Num).all();
							break;
					}
					

					this.setSelected();
				});;
		}
		private getDrillData() {
			if (this.drillIsCurrent()) {
				return this.q.resolve();
			}
			this.drillfltr = {
				SurveyYear: this.options.selectedYear,
				Question: this.options.selectedQuestion,
				District: this.options.selectedDistrict

			};
			return this.http.post("api/warehouse/wash/responses", this.drillfltr)
				.then(result => {
					// result is 2 data sets:
					// the data filtered by question, for individual schools (schools are filtered by options)
					// the set of possible values ( e.g. '<item>' nodes for SINGLE or MULTIPLE)
					this.drillData = result.data[0];
					this.drillItems = <any[]>result.data[1];
				});
		}

		//
		public setChartData() {
			// we can only get something if the selectedYear and selectedExam are both defined

			if (this.options.selectedYear && this.options.selectedQuestion) {
				this.currentQuestion = this.dashboard.question(this.options.selectedQuestion);

				switch (this.currentQuestion.QFlags) {
					case WashQuestionTypes.BINARY:
					case WashQuestionTypes.TERNARY:
						// output the disaggregation (e.g district as ROW, the answer as COL, the num as value)
						this.chartData = this.binaryData;
						break;
					case WashQuestionTypes.SINGLE:
					case WashQuestionTypes.MULTIPLE:
						this.chartData = this.choiceData;
						break;
				}
				this.refreshChart();
			}
		}

		/////////////////////////////////////////////////////////
		// crossfilter interaction
		//////////////////////////////////////////////////////////
		public clickHandler(item) {
			this.options.toggleSelectedDistrict(item);
		}

		public get highlight() {
			return this.options.selectedDistrict;
		}

		//////////////////////////////////////////////////////////
		// Chart callbacks
		//////////////////////////////////////////////////////////

		public tooltipper(datum: Sw.Charts.TooltipDatum) {

			datum.item = this.lookups.byCode("districts", datum.item, "N");
		}

		public onChartRender(option: echarts.EChartOption, echarts: echarts.ECharts) {
			Sw.Charts.chartOps(option)
				.lookups(this.lookups)
				.translateLabels("districts");
		}
	}

	angular
		.module("pineapples")
		.component("washQuestionDistrictChart", new WashDashboardComponent(Controller, "QuestionDistrictChart"))
}