namespace Pineapples.Dashboards.Wash {

	class Controller extends DashboardChild implements IDashboardChild {

		public defaultTitle = "Water Sources";

		// bindings
		public options: any;
		public dashboard: WashDashboard;
		public chartData: Sw.Charts.AnnotatedCrossfilter

		public currentQuestion: IQuestion;
		public group;

		static $inject = ["$timeout", "$mdDialog", "$state", "Lookups", "reportManager", "$http"];
		constructor(
			protected timeout: ng.ITimeoutService
			, mdDialog: ng.material.IDialogService
			, protected $state: ng.ui.IStateService
			, public lookups: Sw.Lookups.LookupService
			, rmgr: Pineapples.Reporting.IReportManagerService
			, public http: ng.IHttpService) {
			super(timeout, mdDialog, $state, lookups, rmgr, http);
		}

		//--------------------------------------------------------------------------------------//
		// View Option Management
		//
		// Select between Currently available and used for drinking options
		//
		//--------------------------------------------------------------------------------------//
		public get viewOptions() {
			return [
				"Used for Drinking"
				, "Currently Available"
			]
		}


		private _selectedViewOption = 1;
		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.onViewOptionChange();
			}
		}

		public onViewOptionChange() {
			//this.viewOptionMirror();
			this.refreshChart();
		};

		//--------------------------------------------------------------------------------------//
		// Drill down support

		// common base to support drill down into education expenditure data
		// this will show the current and total expenditure
		//--------------------------------------------------------------------------------------//
		public drillData: any[]; // lowest level data for drill down
		public drillX: any[];		// cross tabbed version made by crossfilter
		public drillItems: any[];
		public drillRow(inspID) {
			return _.find(this.drillData, { inspID: inspID });
		}

		///////////////////////////////////////////////////////////
		// Accessors for 'annotated' data
		///////////////////////////////////////////////////////////
		private valueAccessor: Sw.Charts.ValueAccessor = (row, column, colIdx) => {
			switch (this.selectedViewOption) {
				case 0:			// Used for Drinking
					switch (column) {
						case "Piped water supply":
							return (row.PipedWaterSupplyUsedForDrinking == "Yes" ? 1 : 0)
						case "Protected well / spring":
							return (row.ProtectedWellUsedForDrinking == "Yes" ? 1 : 0)
						case "Unprotected well / spring":
							return (row.UnprotectedWellSpringUsedForDrinking == "Yes" ? 1 : 0)
						case "Rainwater":
							return (row.RainwaterUsedForDrinking == "Yes" ? 1 : 0)
						case "Bottled water":
							return (row.BottledWaterUsedForDrinking == "Yes" ? 1 : 0)
						case "Tanker / Truck or cart":
							return (row.TankerTruckCartUsedForDrinking == "Yes" ? 1 : 0)
						case "Surfaced water(Lake, River, Stream)":
							return (row.SurfacedWaterUsedForDrinking == "Yes" ? 1 : 0)
					}
				case 1:			// Currently Available
					switch (column) {
						case "Piped water supply":
							return (row.PipedWaterSupplyCurrentlyAvailable == "Yes" ? 1 : 0)
						case "Protected well/spring":
							return (row.ProtectedWellCurrentlyAvailable == "Yes" ? 1 : 0)
						case "Unprotected well / spring":
							return (row.UnprotectedWellSpringCurrentlyAvailable == "Yes" ? 1 : 0)
						case "Rainwater":
							return (row.RainwaterCurrentlyAvailable == "Yes" ? 1 : 0)
						case "Bottled water":
							return (row.BottledWaterCurrentlyAvailable == "Yes" ? 1 : 0)
						case "Tanker / Truck or cart":
							return (row.TankerTruckCartCurrentlyAvailable == "Yes" ? 1 : 0)
						case "Surfaced water(Lake, River, Stream)":
							return (row.SurfacedWaterCurrentlyAvailable == "Yes" ? 1 : 0)
					}

			}
		}

		private columnCollector: Sw.Charts.ColumnCollector = () => {
			switch (this.selectedViewOption) {
				case 0:			// Used for Drinking
					return [
						"Piped water supply",
						"Protected well / spring",
						"Unprotected well / spring",
						"Rainwater",
						"Bottled water",
						"Tanker / Truck or cart",
						"Surfaced water(Lake, River, Stream)"
					];
				case 1:			// Currently Available
					return [
						"Piped water supply",
						"Protected well / spring",
						"Unprotected well / spring",
						"Rainwater",
						"Bottled water",
						"Tanker / Truck or cart",
						"Surfaced water(Lake, River, Stream)"
					];
			}
		}

		public onDashboardReady() {
			// this class needs to build its chart from waterData - a simple collection
			// that is continuously refiltered. waterData is produced from a crossfilter
			// using crossfilter.allFiltered()
			// We do this declaratively, we create an AnnotatedCrossfilter object as the data source
			this.chartData = {
				crossfilter: this.dashboard.xf3,
				rowId: "schNo",
				columns: this.columnCollector,
				valueAccessor: this.valueAccessor
			}
		}

		public onOptionChange(optionchange) {
			if (optionchange.selectedQuestion) {
				// if the question changes, we should destoy the palette, becuase the 
				// assignment of colours from the series to the palette needs to start again
				// otherwise we get old items (ie allocated palette members) showing up in the legend
				this.defaultPalette = "category10";

			}
			this.redrill();
			this.refreshChart();
		}

		public onChildSelected() {
			if (this.isSelected()) {
				this.redrill();
				return;
			}
			this.drillfltr = null;
			this._selectedcell = "";
		}
		protected drillfltr;

		protected redrill() {
			if (this.drillfltr && this.isSelected()) {
				angular.merge(this.drillfltr, {
					District: this.options.selectedDistrict,
					Year: this.options.selectedYear
				});
				this.getDrillData();
			}
		}
		public drilldown(districtCode: string, sectorCode: string, costCentreCode) {
			this.drillfltr = {
				District: this.options.selectedDistrict,
				SurveyYear: this.options.selectedYear,
				Question: this.options.selectedQuestion
			};
			this._selectedcell = `${districtCode}|${sectorCode}|${costCentreCode}`;
			this.getDrillData()
				.then(() => {
					this.setSelected();
				});;
		}
		private getDrillData() {
			return this.http.post("api/warehouse/wash/responses", this.drillfltr)
				.then(result => {
					// result is 2 data sets:
					// the data filtered by question, for individual schools (schools are filtered by options)
					// the set of possible values ( e.g. '<item>' nodes for SINGLE or MULTIPLE)
					this.drillData = result.data[0];
					let xf = crossfilter(this.drillData);
					let dx = xf.dimension(d => d["inspID"]);
					this.drillX = this.dashboard.xFilter.xReduce(dx, "Response", d => d.Num).all();
					//this.drillData = <any[]>result.data[0];
					this.drillItems = <any[]>result.data[1];
				});
		}

		protected _selectedcell: string;
		protected isSelectedCell(districtCode: string, sectorCode: string, costCentreCode) {
			return this.isSelected() && this._selectedcell == `${districtCode}|${sectorCode}|${costCentreCode}`;
		}

		////////////////////////////////////////////////////////////////////////////////////
		// Chart callbacks
		////////////////////////////////////////////////////////////////////////////////////
		public clickHandler(params) {
			console.log("Click", params);
			this.drilldown(null, null, null);
		}

		public tooltipper(datum) {
		}

		public onChartRender(option: echarts.EChartOption, echart: echarts.ECharts, renderedType: string) {
			option.series.forEach(s => {
				angular.merge(s, {
					animation: true,
					animationDuration: 0,
					animationDurationUpdate: 10,
					animationDelayUpdate: 0,
				})
			})
			option.animation = false;
		}

	}

	angular
		.module("pineapples")
		.component("washWaterBySchool", new WashDashboardComponent(Controller, "WaterBySchool"))
}