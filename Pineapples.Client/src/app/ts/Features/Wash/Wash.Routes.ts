namespace Pineappples.Wash {

	let RouteHelper = Sw.Utils.RouteHelper;
	let routes = function ($stateProvider: ng.ui.IStateProvider) {
		var featurename = 'Wash';
		var filtername = 'SchoolInspectionFilter';
		var templatepath = "schoolAccreditation";
		var tableOptions = "schoolaccreditationFieldOptions";
		var url = "schoolaccreditations";
		var usersettings = null;
		//var mapview = 'SchoolMapView';


		// state for a high level dashboard
		let state: ng.ui.IState = {
			url: "^/wash/dashboard",
			data: {
				//permissions: {
				//	only: 'SchoolReadX'
				//},
				rendermode: "Dashboard"
			},
			views: {
				"renderarea": {
					component: "washDashboard"
				},
				"searcher": "washOptionsEditor"

			},
			resolve: {
				options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
					return new Pineapples.Dashboards.WashOptions(lookups, scope.hub);
				}],
				questions: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
					return reflator.get("api/warehouse/wash/questions").then(response => (<any>response.data));
				}],
				// use $q.all to return both sets in an array
				tables: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
					// overall level 
					let p = reflator.get("api/warehouse/wash").then(response => (<any>response.data));
					// level by standard
					let p2 = reflator.get("api/warehouse/wash/questions").then(response => (<any>response.data));
					// use $q.all to return both sets in an array
					let p3 = reflator.get("api/warehouse/wash/toilets").then(response => (<any>response.data));
					// use $q.all to return both sets in an array
					let p4 = reflator.get("api/warehouse/wash/water").then(response => (<any>response.data));

					return q.all([p, p2, p3, p4]);
				}],
				pageTitle: RouteHelper.simpleTitle("Dashboard")
			}
		};
		$stateProvider.state("site.schoolinspections.washdashboard", state);

		// allow access to the school and inspection from within the wash dashboard -

		// item state

		state = {
			url: "^/wash/dashboard/{id}",
			views: {
				"actionpane@site.schoolinspections": "componentSchoolInspection"
			}
		};
		state = RouteHelper.clone($stateProvider, "site.schoolaccreditations.dashboard.item", state);
		$stateProvider.state("site.schoolinspections.washdashboard.item", state);
		// ----------------- school state : allows a school to be displayed from the list of inspections

		state = {
			url: "^/wash/dashboard/school/{id}",
			views: {
				"actionpane@site.schoolinspections": "componentSchool"
			}
		};
		state = RouteHelper.clone($stateProvider, "site.schools.list.item", state);
		$stateProvider.state("site.schoolinspections.washdashboard.school", state);

		$stateProvider
			.state('site.schoolinspections.washdataexport', {
				url: '^/wash/dataexport',
				views: {
					"@": "washDataExportComponent"
				},
				resolve: {
					pageTitle: RouteHelper.simpleTitle("Data Export")
				}
			});

	}
	angular
		.module('pineapples')
		.config(['$stateProvider', routes])
}
