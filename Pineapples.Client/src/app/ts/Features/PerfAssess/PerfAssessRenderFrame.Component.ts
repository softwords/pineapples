﻿namespace Pineapples.Students {

	angular
		.module("pineapples")
		.component("perfAssessRenderFrame", new Sw.Component.RenderFrameComponent("perfAssess"));
}