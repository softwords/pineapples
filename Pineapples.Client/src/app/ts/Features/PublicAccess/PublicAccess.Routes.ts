﻿// Teachers Routes
namespace Pineapples.CensusAdmin {

	let routes = function ($stateProvider: ng.ui.IStateProvider) {
		//var mapview = 'TeacherMapView';

		// root state for 'teachers' feature
		let state: ng.ui.IState;
		let statename: string;

		// base censusworkbook state
		state = {
			url: "^/public",
			abstract: true,
			resolve: {

				lookupsResolver: ['Lookups', function (Lookups) {
					return Lookups.init();
				}],
				allowPublic: ["$q", "lookupsResolver", "Lookups", (q: ng.IQService, lookupsResolver, lookups: Pineapples.Lookups.LookupService) => {
					if (lookups.byCode("sysParams", "allowPublicAccess")?.["I"] == 1) {
						return q.resolve();
					}
					return q.reject("Public access not supported");
				}],
				rootPageTitle: ["lookupsResolver", "Lookups", (lookupsResolver, lookups: Sw.Lookups.LookupService) => {
					return lookups.byCode("sysParams", "APP_NAME", "N") || "Pacific EMIS";
				}],
				pageTitle: ["rootPageTitle", (rootPageTitle) => rootPageTitle]

			}
		};
		$stateProvider.state("public", state);


		state = {
			url: "^/public/home",
			views: {
				"@": {
					component: "publicAccessHome"
				}
			},
			resolve: {
			}
		};
		$stateProvider.state("public.home", state);

		state = {
			url: "^/public/dashboards",
			data: null,
			views: <{ [n: string]: ng.ui.IState }>{
				"@": `publicDashboardsRenderFrame`
			},
			resolve: {
				pageTitle: () => "Dashboards",
				palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
				mapColorScale: function () { return new Sw.Maps.MapColorScale(); },
			}
		};
		$stateProvider.state("public.dashboards", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.findata.dashboard",
			{
				url: "^/public/dashboards/findata",
			});

		state.resolve = state.resolve || {};
		state.resolve['featureTitle'] = () => "Budgets";

		state.data = {
			permissions: {},
			rendermode: "Findata"
		};

		$stateProvider.state("public.dashboards.findata", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.schools.dashboard",
			{
				url: "^/public/dashboards/schools",
			});

		state.resolve = state.resolve || {};
		state.resolve['featureTitle'] = () => "Schools";

		state.data = {
			permissions: {},
			rendermode: "Schools"
		};

		$stateProvider.state("public.dashboards.schools", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.teachers.dashboard",
			{
				url: "^/public/dashboards/teachers",
			});

		state.resolve = state.resolve || {};
		state.resolve['featureTitle'] = () => "Teachers";

		state.data = {
			permissions: {},
			rendermode: "Teachers"
		};

		$stateProvider.state("public.dashboards.teachers", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.exams.dashboard",
			{
				url: "^/public/dashboards/exams",
			});

		state.resolve = state.resolve || {};
		state.resolve['featureTitle'] = () => "Exams";

		state.data = {
			permissions: {},
			rendermode: "Exams"
		};

		$stateProvider.state("public.dashboards.exams", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.specialed.dashboard",
			{
				url: "^/public/dashboards/specialed",
			});

		state.resolve = state.resolve || {};
		state.resolve['featureTitle'] = () => "Special Education";

		state.data = {
			permissions: {},
			rendermode: "Special Education"
		};

		$stateProvider.state("public.dashboards.specialed", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.schoolaccreditations.dashboard",
			{
				url: "^/public/dashboards/schoolaccreditations",
			});

		state.resolve = state.resolve || {};
		state.resolve['featureTitle'] = () => "School Accreditation";

		state.data = {
			permissions: {},
			rendermode: "School Accreditation"
		};

		$stateProvider.state("public.dashboards.schoolaccreditations", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.schoolinspections.washdashboard",
			{
				url: "^/public/dashboards/wash",
			});

		state.resolve = state.resolve || {};
		state.resolve['featureTitle'] = () => "WASH Surveys";

		state.data = {
			permissions: {},
			rendermode: "WASH Surveys"
		};

		$stateProvider.state("public.dashboards.wash", state);

		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.indicators",
			{
				url: "^/public/indicators",
				views: <{ [n: string]: ng.ui.IState }>{
					"@": `publicIndicatorsRenderFrame`
				}
			});

		$stateProvider.state("public.indicators", state);


		// clone any states that have been established under site.indicators
		// change the url and place the modified state under public.indicators
		let statesList = (<any>$stateProvider).stateRegistry.states;
		Object.keys(statesList).forEach(statename => {

			if (statename.indexOf("site.indicators.") == 0) {
				let state = statesList[statename];
				let publicstatename = statename.replace("site.indicators.", "public.indicators.");
				let publicurl = `^/${publicstatename.split(".").join("/")}`;			// ie replace all . with /
				let publicstate = Sw.Utils.RouteHelper.clone($stateProvider, statename,
					{
						url: publicurl
					});
				$stateProvider.state(publicstatename, publicstate);
			}
		})
	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}