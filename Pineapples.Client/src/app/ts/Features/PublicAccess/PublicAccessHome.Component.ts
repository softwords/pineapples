﻿namespace Pineapples.Public {


	class Controller {

		static $inject = ["Lookups"];
		constructor(private lookups: Pineapples.Lookups.LookupService) { };

		public $onChanges(changes) {

		}


	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any = {
		};
		public controllerAs: string = "vm";
		public templateUrl = "publicaccess/home";
		public controller = Controller

	}
	angular
		.module("pineapples")
		.component("publicAccessHome", new ComponentOptions());
}