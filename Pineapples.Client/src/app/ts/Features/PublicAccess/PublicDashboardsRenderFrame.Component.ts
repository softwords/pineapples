﻿namespace Pineapples.Schools {

	angular
		.module("pineapples")
		.component("publicDashboardsRenderFrame", new Sw.Component.RenderFrameComponent("publicdashboards"))
		.component("publicIndicatorsRenderFrame", new Sw.Component.RenderFrameComponent("publicindicators"));
}