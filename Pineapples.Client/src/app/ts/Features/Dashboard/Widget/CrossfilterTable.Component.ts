﻿namespace Pineapples.Dashboards {

	/*
	 * CrossfilterTable
	 * A table generator for crosstabs, especially crosstabs with 
	 * gender disaggregation, built using xFilter techniques
	 * 
	 * Bindings
	 *    source: "<",              Crossfilter group or array of groups, array or array of arrays
				columns: "<",             array of the available columns
				columnTranslator: "<",    Translator object for rendering column header expects 1 arg the raw col value
				rowAccessor: "<?",        takes a row (ie key value pair) and group, returns the row label
				rowFilter: "<?",          takes a row, returns false if the row should be hidden
				valueAccessor: "<",       function taking a (row, column, gender ) returns value
				valueFormatter: "<"				function taking a (value, row, column, gender ) returns formatted value
				hasRowTotals: "<",        true if there is a total at the end of each row
				footerGroups: "<",        an array of crossfilter groups used in the footer
  
				genderTotals: "<",        displays subheaders for gender, and gender values
																	expects the value of the crosstab to look like value.<item>.{ M, F, T}
																	(this format can be created by xFilter.xReduce, using xFilter.getFlattendedGenderValue)
				colDescriptor: "@",       Description of the values in columns
				rowDescriptor: "@",       Description of the values in rows
  
	** selection
	 *    select    "@"             Mode of selection - either 'row' or 'cell'
				onSelection: "&"          callback when a selection is made - arguments row, group, selectedRowKey, selectedColKey
	  
	 
	 * Classes
	 * Theses classes can be applied to the component element; 
	 * e.g. <crossfilter-table class="thin table-hover" ....></crossfilter-table>
	 * 
	 * thin           reduced padding in rows
	 * thick          increased padding in rows
	 * striped        odd/even striping
	 * 
	 * Customising css 
	 * further non-standard stylings can be applied to the table by adding an identifying class to the
	 * component element, then defining a css select based on that e.g.
	 * in cshtml:
		 <style>
		 crossfilter-table.blue_stripe tbody > tr:nth-child(odd) {
				background-color: blue;
		 }
  
		 </style>
		 <crossfilter-table class="blue_stripe"  ....>
	 * 
	 * 
	*/

	class Controller {
		public source: any[] | CrossFilter.Group<any, any, any>;
		public columns;
		public columnTranslator;
		public rowAccessor;
		public valueAccessor;
		public valueFormatter;

		// structure
		public hasRowTotals;
		public genderTotals;
		public rowFilter: (row: any, rows) => boolean;
		public rowSort: (rowa: any, rowb: any) => number;
		public showDetail: (row: any, rows) => boolean;

		// callbacks
		private onSelection;

		public selectedRowKey;
		public selectedColKey;
		public isSelectedRow = (row, grp) => (row.key === this.selectedRowKey) ? "selected" : "";
		public isSelectedCol = (col, grp) => (col === this.selectedColKey) ? "selectedCol" : "";

		// presentation
		public select: string;
		public tableClass: string;
		public rowClass: ((row: any, source: any[], index: number) => string[]) | string | string[];
		public rowLabelClass: ((row: any, source: any[], index: number) => string[]) | string | string[];

		public colClass: ((col: string, colIndex: number) => string[]) | string | string[];
		public cellClass: ((row: any, col: string, source: any[], rowIndex: number, colIndex: number) => string[]) | string | string[];

		public footerSource: any[];

		static $inject = ["$transclude", "Lookups"];

		constructor(public transclude, public lookups: Pineapples.Lookups.LookupService) {
		};

		public columnHeader = (col) => col;
		public $onChanges(changes) {
			if (changes.columnTranslator) {
				if (this.columnTranslator) {
					this.columnHeader = new Sw.Charts.ChartOps(null)
						.lookups(this.lookups)
						.translator(this.columnTranslator);
				} else {
					this.columnHeader = (col) => col;
				}
			}
			if (changes.footerSource) {
				// four options
				// 1 - its a single crossfilter group
				if (this.footerSource == null) {
					// do nothing
				} else if ((this.footerSource as any).all) {
					this.footerSource = [(this.footerSource as any)];
				} else if (Array.isArray(this.footerSource)) {
					//2 its an array where the element is not an array or group
					let arr: any[] = this.footerSource;
					if (arr.length == 0) {

					} else if (Array.isArray(this.footerSource[0]) || (this.footerSource[0] as any).all) {
						// its an array of arrays or groups - do nothing
					} else {
						// its a single source array, put it in an array
						this.footerSource = [this.footerSource];
					}
				}
			}
			if (changes.source) {
				// four options
				// 1 - its a single crossfilter group
				if (this.source == null) {
					// do nothing
				} else if ((this.source as any).all) {
					this.source = [this.source];
				} else if (Array.isArray(this.source)) {
					//2 its an array where the element is not an array or group

					let arr: any[] = this.source;
					if (arr.length == 0) {

					} else if (Array.isArray(this.source[0]) || (this.source[0] as any).all) {
						// its an array of arrays or groups - do nothing
					} else {
						// its a single source array, put it in an array
						this.source = [this.source];
					}
				}
			}
			//if (changes.selectedRowKey) {
			//  this.onSelection({ selectedRowKey: this.selectedRowKey, selectedColKey: this.selectedColKey })
			//}
		}
		private hideZeroCount = (row, index: number, arr: any[]) => !(row?.value?.Count === 0);
		public _rows(sourceobj) {
			let arr: any[] = (Array.isArray(sourceobj) ? sourceobj : sourceobj.all());
			arr = arr.filter(this.rowFilter ?? this.hideZeroCount, this);
			if (this.rowSort) {
				arr.sort(this.rowSort);
			}
			return arr;
		}

		public _cols(sourceobj) {
			if (this.columns) {
				return this.columns;
			}

			let root: any;
			if (sourceobj.all) {
				// the source is a group, so the default columns are proeprtys of row.value
				let arr = sourceobj.all();
				if (arr.length) {
					root = arr[0].value;
				} else {
					return [];
				}
			} else {
				root = sourceobj[0];
			}
			let keys = Object.keys(root).reduce((keys, n, i) => {
				switch (n) {
					case "$$hashKey":
					case "keyAccessor":
					case "valueAccessor":
					case "Count":
					case "Tot":
						break;
					default:
						keys.push(n);
				};
				return keys;
			}, <string[]>[]);
			return keys;
		}

		/**
		 * Return a value from the row of the source data
		 * delegates to user-supplied valueAccessor binding
		 * If valueAccessor is not given; the defaults apply:
		 * For a key-value object, assumes 'col' is a property of 'value'
		 * For a simple object, assumes 'col' is a property of 'row'
		 * @param row in the array - for a crossfilter source, this is a key-value object
		 * @param column header for the value (string)
		 * @param gender variant - M - F - T or null
		 */
		public value(row, col, gender) {
			try {
				if (gender == "NA") {
					let v = this.value(row, col, "T") - this.value(row, col, "M") - this.value(row, col, "F");
					if (isNaN(v) || v == 0) {
						return null;
					}
					return v;
				}
				let val = null;
				if (!this.valueAccessor) {
					// check if this 'row' is a key-value object
					if (Sw.Charts.EChartTransformer.isKeyValuePair(row)) {
						if (!gender) {
							val = (row.value?.[col]?.T === undefined ? row.value[col] : row.value[col].T)
						} else {
							val = row.value?.[col]?.[gender];
						}
					} else {
						// if not a key-value pair then the default value accessor assumes col is a property of row
						// or, in gendered case col + Gender is a property of row ie row.EnrolM row.EnrolF
						if (!gender) {
							val = (row[col]?.T === undefined ? row[col] : row[col].T);
						} else {
							if (row[col][gender] !== undefined) {
								val = row[col][gender];
							} else if (row[col + gender] !== undefined) {
								val = row[col + gender];
							} else {
								switch (gender) {
									case "T":
									case "Total":			// support for these legacy versions
										val = this.value(row, col, "M") + this.value(row, col, "F");
										break;
									case "Male": // legacy support
										val = this.value(row, col, "M");
										break;
									case "Female":
										val = this.value(row, col, "F");
										break;
								}
							}
						}
					}
				} else {
					val = this.valueAccessor(row, col, gender);
				}
				if (this.valueFormatter) {
					val = this.valueFormatter(val, row, col, gender)
				}
				return val ? val : null;
			}
			catch {
				console.log("Value error", row, col, gender);
				return "##";
			}
		}

		public rowLabel(row, group) {
			let label = null
			if (!this.rowAccessor) {
				return row.key;
			}
			// if its a string, assume it is a column name 
			if (typeof (this.rowAccessor) == "string") {
				return row[this.rowAccessor];
			}
			return this.rowAccessor(row, group);
		}

		public columnLabel(col) {
			let label = null
			if (!this.columnTranslator) {
				return col;
			}

			return this.columnHeader(col);

		}

		// click handers 
		/* 
		 * The click handlers implement selection of a row and/or column, and fire the 
		 * callback onSelection when the selection is changed. 
		 * The handlers respect the 'select' binding - row, col, cell any - to decide what can be 
		 * clicked on and selected
		 */
		public cellClickHandler(row, col, source, $event: Event) {
			switch (this.select) {
				case "cell":
				case "any":
					if (row.key === this.selectedRowKey && col == this.selectedColKey) {
						this.selectedRowKey = undefined;
						this.selectedColKey = undefined;
					} else {
						this.selectedRowKey = row.key;
						this.selectedColKey = col;
					}
					$event.stopPropagation();
					this.onSelection({
						row, source, selectedRowKey: this.selectedRowKey, selectedRowLabel: this.rowLabel(row, source),
						selectedColKey: this.selectedColKey, selectedColLabel: this.columnLabel(col)
					})
					break;
			}
		}

		public colClickHandler(col: string, $event: Event) {
			switch (this.select) {
				case "col":
				case "any":

					if (col === this.selectedColKey && this.selectedRowKey === undefined) {
						this.selectedColKey = undefined;
					} else {
						this.selectedColKey = col;
					}
					this.selectedRowKey = undefined;
					$event.stopPropagation();
					this.onSelection({
						selectedRowKey: this.selectedRowKey,
						selectedColKey: this.selectedColKey, selectedColLabel: this.columnLabel(this.selectedColKey)
					})
					break;
			};
		}

		public rowClickHandler(row, source, $event: Event) {
			switch (this.select) {
				case "row":
				case "any":
					if (row.key === this.selectedRowKey && this.selectedColKey === undefined) {
						this.selectedRowKey = undefined;
					} else {
						this.selectedRowKey = row.key;
					}
					this.selectedColKey = undefined;
					$event.stopPropagation();
					this.onSelection({
						row, source, selectedRowKey: this.selectedRowKey, selectedRowLabel: this.rowLabel(row, source)
					})
					break;
			}
		}

		// class handlers
		/* 
		 * The class handlers are invoked through ng-class at 4 points
		 * table - row - column ( colgroup) - cell
		 * 
		 */
		private tableClassEval(classSource) {
			if (typeof (classSource) == "string") {
				return [classSource];
			} else if (Array.isArray(classSource)) {
				// assume key value pair
				return classSource;
			}
			let f: Function = classSource;
			let result = f.call(this);
			if (Array.isArray(result)) {
				return result;
			}
			return result ? [result] : [];
		}

		public tableClassHandler() {
			let s: string[] = [];
			if (this.tableClass) {
				let u = this.tableClassEval(this.tableClass);
				if (Array.isArray(u)) {
					s = u;
				} else {
					s = u ? [u] : [];
				}
			}
			switch (this.select) {
				case "row":
					s.push('row-hover');
					break;
				case "col":
					s.push('col-hover');
					break;
				case "cell":
					s.push('cell-hover');
					break;
				case "rowcol":
					s.push('row-hover');
					s.push('col-hover');
					break;
				case "any":
					s.push('any-hover');
					break;
			}
			return s;
		}

		private rowClassEval(classSource, row, index: number, source: any[], section: string) {
			if (typeof (classSource) == "string") {
				return [classSource];
			} else if (Array.isArray(classSource)) {
				// assume key value pair
				return classSource;
			}
			let f: Function = classSource;
			let result = f.call(this, row, index, source, section);
			if (Array.isArray(result)) {
				return result;
			}
			return result ? [result] : [];
		}

		public rowClassHandler(row, index: number, source: any[], section: string) {
			let s: string[] = [];
			if (this.rowClass) {
				let u = this.rowClassEval(this.rowClass, row, index, source, section);
				if (Array.isArray(u)) {
					s = u;
				} else {
					s = u ? [u] : [];
				}
			}
			if (row.key === this.selectedRowKey && !this.selectedColKey) {
				// the entire row is selected
				s.push("selected")
			}
			if (index % 2) {
				s.push("stripe")
			}
			return s;
		}

		private colClassEval(classSource, col, index: number) {
			if (typeof (classSource) == "string") {
				return [classSource];
			} else if (Array.isArray(classSource)) {
				// assume key value pair
				return classSource;
			}
			let f: Function = classSource;
			let result = f.call(this, col, index);
			if (Array.isArray(result)) {
				return result;
			}
			return result ? [result] : [];
		}
		public colClassHandler(col: string, colIndex: number) {
			let s: string[] = [];
			if (this.colClass) {
				let u = this.colClassEval(this.colClass, col, colIndex);
				if (Array.isArray(u)) {
					s = u;
				} else {
					s = u ? [u] : [];
				}
			}
			if (col === this.selectedColKey && !this.selectedRowKey) {
				// the entire col is selected
				s.push('selected');
			}
			return s;
		}


		private cellClassEval(classSource, row, col: string, rowIndex: number, colIndex: number, source: any[]) {
			if (typeof (classSource) == "string") {
				return [classSource];
			} else if (Array.isArray(classSource)) {
				// assume key value pair
				return classSource;
			}
			let f: Function = classSource;
			let result = f.call(this, row, col, rowIndex, colIndex, source);
			if (Array.isArray(result)) {
				return result;
			}
			return result ? [result] : [];
		}
		public cellClassHandler(row, col, rowIndex, colIndex, source) {
			let s: string[] = [];
			if (this.cellClass) {
				let u = this.cellClassEval(this.cellClass, row, col, rowIndex, colIndex, source);
				if (Array.isArray(u)) {
					s = u;
				} else {
					s = u ? [u] : [];
				}
			}
			if (row.key === this.selectedRowKey && col === this.selectedColKey) {
				s.push('selected');
			}
			return s;
		}

		private rowLabelClassEval(classSource, row, rowIndex: number, source: any[], section) {
			if (typeof (classSource) == "string") {
				return [classSource];
			} else if (Array.isArray(classSource)) {
				// assume key value pair
				return classSource;
			}
			let f: Function = classSource;
			let result = f.call(this, row, rowIndex, source, section);
			if (Array.isArray(result)) {
				return result;
			}
			return result ? [result] : [];
		}
		public rowLabelClassHandler(row, index, source,section) {
			let s: string[] = [];
			if (this.rowLabelClass) {
				let u = this.rowLabelClassEval(this.rowLabelClass, row, index, source, section);
				if (Array.isArray(u)) {
					s = u;
				} else {
					s = u ? [u] : [];
				}
			}
			if (row?.key === this.selectedRowKey && !this.selectedColKey) {
				// the entire row is selected
				s.push("selected");
			}
			return s;
		}

		public showRow(row, grp) {
			if (!this.rowFilter) {
				// be default, filter out any rows where value.Count exists and = 0
				return !(row?.value?.Count === 0);
			}
			return this.rowFilter(row, grp);
		}

		public _showDetail(row, source) {
			if (this.showDetail) {
				return this.showDetail(row, source);
			}
			return (row.key === this.selectedRowKey);
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			source: "<",   // Crossfilter group
			footerSource: "<",     // an array of crossfilter groups used in the footer

			columns: "<",  // array of the available columns
			columnTranslator: "<",   // Translator object for rendering column header expects 1 arg the raw col value
			rowAccessor: "<?",    // takes a row (ie key value pair) returns the row label
			rowFilter: "<?",      // takes a row, returns false if the row should be hidden
			rowSort: "<?",				// a sort comparator function
			valueAccessor: "<",   // function taking a (row, column, gender ) returns value 
			valueFormatter: "<",	// function taking a (value, row, column, gender ) returns formatted value (called after value is computed)
			// default row.value[col].T or row.value[col]  (no gender) 
			// or if gender supplied, row.value[col][gender]

			hasRowTotals: "<",    // true if there is a total at the end of each row
			genderTotals: "<",    // displays subheaders for gender, and gender values
			showDetail: "<",      // function takes row, source returns boolean 
			// is not supplied, showDetail is true when rowAccessor is selectedRowKey
			// expects the value of the crosstab to look like value.<item>.{ M, F, T}
			colDescriptor: "@",
			rowDescriptor: "@",

			select: "@",             // 'row' 'col' 'cell' or 'any'

			// dynamic classes
			// each binding can be a function, a string (a class name), or a string[] - an array of classes
			// if a function, the return value is a class name or an array of class names.
			// Function arguments:
			// row : object in the array source
			// rowIndex : index of the current row in source
			// col : string identifier of column
			// colIndex : index of column in columns array
			// source : source data array
			// section: for row, may be 'body' or 'foot' for rowLabel, 'body', 'foot' or 'head'
			
			tableClass: "<",				// tableClass()
			rowClass: "<",					// rowClass(row, rowIndex, source, section) applied on <tr> elements in <tbody> <tfoot>
			colClass: "<",					// colClass(col, colIndex)  applied on <colgroup> elements. 
			rowLabelClass: "<",			// rowLabelClass(row, rowIndex, source, section)  applied on <th> element containing the row label (section='body' or 'foot')
															//   and <th> element in <thead> containing the row label header (section = 'head')
			cellClass: "<",					// cellClass(row, col, rowIndex, colIndex, source) applied on <td> element in the table body

			selectedRowKey: "<",
			selectedColKey: "<",
			onSelection: "&"       // accepts {row, col, group, key, label}

		};;
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = `dashboard/widget/crossfiltertable`;
		public transclude = {
			'caption': "?captionContent",       // a caption for the table
			'detail': "?detailContent",         // content when 'drilling down' on a row. opens below that row
			'toolbar': "?toolbarContent",      // a toolbar of controls, below the caption (if present)
			'actions': "?actionsContent"      // if present, added to the end of a row
		}

	}

	angular
		.module("pineapples")
		.component("crossfilterTable", new Component());

}