﻿/* <EChartLineChart />
 * Legacy Line Chart Component
 * TODO Revise this
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * chartTitle: Title to display on chart
 * datasets: data for chart - list of objects containing { dataset, meta, color }
        i.e. 
          [
            {
              "meta": "GPK",
              "color": "#FF6666",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 0
                }
              ]
            },
            {
              "meta": "GK",
              "color": "#FFB266",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 0
                }
              ]
            },
            {
              "meta": "G1",
              "color": "#FFFF66",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 137.5
                }
              ]
            },
            {
              "meta": "G2",
              "color": "#B2FF66",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 150
                }
              ]
            },
            {
              "meta": "G3",
              "color": "#66FF66",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 206.3
                }
              ]
            },
            {
              "meta": "G4",
              "color": "#66FFB2",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 114.3
                },
                {
                  "year": 2016,
                  "value": 76.9
                },
                {
                  "year": 2017,
                  "value": 255.6
                }
              ]
            },
            {
              "meta": "G5",
              "color": "#66FFFF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 145.5
                },
                {
                  "year": 2016,
                  "value": 157.1
                },
                {
                  "year": 2017,
                  "value": 110
                }
              ]
            },
            {
              "meta": "G6",
              "color": "#66B2FF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 78.6
                },
                {
                  "year": 2017,
                  "value": 440
                }
              ]
            },
            {
              "meta": "G7",
              "color": "#6666FF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 1200
                }
              ]
            },
            {
              "meta": "G8",
              "color": "#B266FF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 0
                }
              ]
            }
          ]

 * datasetsX: key for objects in dataset attribute for x axis.  i.e. "year"
 * datasetsY: key for objects in dataset attribute for y axis.  i.e. "value"
 */

namespace Pineapples.Dashboards {

  class Controller {
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;

    public headingFilters: string;
    public headingTitle: string;
    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsX: any;
    public datasetsY: any;

		public widgetPkg: Sw.Charts.IWidgetPkg;
		public palette: string[];
		public echart: echarts.ECharts;

    constructor() {
      this.componentId = uniqueId();
    }

    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
		anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

		public eChartInit(echart) {
			this.echart = echart;
		}
		public $postLink() {
			// this will create an Echarts dataset
			// first collect all the X values
			this.makeWidgetPkg();
		}

		public $onChanges() {
			this.makeWidgetPkg();
		}


		public makeWidgetPkg() {
			if (!this.datasets) {
				return;
			}

			this.widgetPkg = {
				data: this.datasets.map(series => series.dataset),
				X: this.datasetsX,
				Y: this.datasetsY,
				seriesNames: this.datasets.map(series => series.meta)
			}
			// collect the palette too...
			this.palette = this.datasets.map( series => series.color)

		}

		public onChartRender(option:echarts.EChartOption, echart) {
			console.log("Line Chart onChartRender");
			console.log(option);
			option.series.forEach((series:echarts.EChartOption.SeriesLine) => {
				series.smooth = true;
			})
		}
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `
        <dashboard-child class="dashboard-wrapper" style="padding: 5px;"
                         ng-class="vm.dimensions"
                         report-path="vm.reportPath"
                         toggle-selected="vm.toggleSelected"
                         is-selected="vm.isSelected()"
                         another-component-selected="vm.anotherComponentSelected()"
                         component-id="vm.componentId">

          <heading-title>{{vm.headingTitle}}</heading-title>
          <heading-filters>{{vm.headingFilters}}</heading-filters>
          <heading-options>
            <md-select ng-if="vm.headingOptions" ng-model="vm.selectedViewOption">
              <md-option ng-value="opt" ng-repeat="opt in vm.viewOptions()">{{ opt }}</md-option>
            </md-select>
          </heading-options>  

          <child-body>
						<row-col-chart
									charttype = "l"
									dataset="vm.widgetPkg"
									on-chart-init="vm.eChartInit(echart)"
									on-chart-render="vm.onChartRender(option, echart)"
									colors="vm.palette"
								
							/>

          </child-body>
        </dashboard-child>
`;
  }

  angular
    .module("pineapples")
    .component("echartLineChart", new Component());
}
