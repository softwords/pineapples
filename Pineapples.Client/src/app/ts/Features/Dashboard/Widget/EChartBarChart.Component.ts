﻿/* <EChartBarChart />
 * Legacy Bar Chart Component
 * TODO Revise this
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * headingOptions: js object, keys of which are displayed in dropdown in component header.  Values corrosponding to selected option are available to the component to customise the view.
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * chartTitle: Title to display on chart
 * datasets: data for chart - js Array of Arrays of Objects, one Array per dataset, one object per datapoint
        i.e. [[ {
                  "ClassLevel": "G5",
                  "Value": "110"
                },
                {
                  "ClassLevel": "G6",
                  "Value": "440"
                },
                {
                  "ClassLevel": "G7",
                  "Value": "1200"
                } ]]

 * datasetsX: key for objects in dataset attribute for x axis.  i.e. "ClassLevel"
 * datasetsY: key for objects in dataset attribute for y axis.  i.e. "Value"by 
 * 
 * datasetsX and datasetsY are string, not accessor functions.
 * Note that this data is 'transposed' from the way echarts will need it - the X values are horizontal, not vertical
 * 
 * Further, this format cannot guarantee that the same X value (ie ClassLevel) will not occur more than once in any series
 * 
 * These legacy 'plottable' formats are all minor variants of each other; we standardise on transforming 
 * a new interface called 'widgetPkg' (widget package)
 * data - an array of arrays as shown above
 * X - the rowProvider
 * Y - the value provider
 * seriesNames - a separate array contains the series names and should must match the number of rows in data
 * 
 * Each of these wrapper classes constructs a widgetPkg, which it can submit as the 'dataset' binding directly
 * to RowColChart. RowColChart use the new EChartTransformer object to do this data transformation
 * 
 * 
 */

namespace Pineapples.Dashboards {

  class Controller {
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;
    public headingOptions: any;
    public chartTitle: string;
    public datasets: any;
    public datasetsX: any;
		public datasetsY: any;
		public datasetsMetadata: any;
		public datasetsColors: any;

		public widgetPkg;
		public echart: echarts.ECharts;

    constructor() {
      this.componentId = uniqueId();
    }

    // This is ugly, but is required when components define their componentIds - Componenent Design recommends pushing state (like this) up. 
    public componentId: string;
    public isSelected = () => this.componentId == this.selectedChild;
		public anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

		public eChartInit(echart) {
			this.echart = echart;
		}
		public $postLink() {
			// this will create an Echarts dataset
			// first collect all the X values
			this.makeWidgetPkg();
		}

		public $onChanges() {
			this.makeWidgetPkg();
		}


		public makeWidgetPkg() {
			if (!this.datasets) {
				return;
			}
			this.widgetPkg = {
				data: this.datasets,
				X: this.datasetsX,
				Y: this.datasetsY,
				seriesNames: this.datasetsMetadata
			}

		}

		public onChartRender(option, echart) {
			/* 
			 *	if this chart is a bar with only a single series, then the description of that series 
			 *	is in the component header.
			 *	So the Legend in the chart is redundant, and, we don;t have a series name anyway for the one series.
			 *	Just remove the legend
			 *	We also need to adjust the tooltip
			 */
			Sw.Charts.chartOps(option)
				.showLegend(option.series.length > 1)
				.minimalInk();
				// tooltip - we can play with the tooltip string template, or provide
				// a formatter function
				//see  https://echarts.apache.org/en/option.html#tooltip.formatter for the 'String template'
				// But the easiest way to get the series name out of the tooltip is
				// to set the series name = '' or delete it
			if (option.series.length == 1) {
				option.series[0].name = "";
			}
		}
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      headingOptions: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      chartTitle: '@',
      datasets: "<",
      datasetsX: "@",
			datasetsY: "@",
			// uses of this component in schooldashboard.cshtml refer to these 2 bindings,
			// not previously defined
			datasetsMetadata: "<",
			datasetsColors: "<"
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `
        <dashboard-child class="dashboard-wrapper" style="padding: 5px;"
                         ng-class="vm.dimensions"
                         report-path="vm.reportPath"
                         toggle-selected="vm.toggleSelected"
                         is-selected="vm.isSelected()"
                         another-component-selected="vm.anotherComponentSelected()"
                         component-id="vm.componentId">

          <heading-title>{{vm.headingTitle}}</heading-title>
          <heading-filters>{{vm.headingFilters}}</heading-filters>
          <heading-options>
            <md-select ng-if="vm.headingOptions" ng-model="vm.selectedViewOption">
              <md-option ng-value="opt" ng-repeat="opt in vm.viewOptions()">{{ opt }}</md-option>
            </md-select>
          </heading-options>  

          <child-body>
            <row-col-chart
								charttype = 'v'
                dataset="vm.widgetPkg"
								on-chart-init="vm.eChartInit(echart)"
								on-chart-render="vm.onChartRender(option, echart)"
								colors="vm.datasetColors"
                allowed-types="['v','h','pie']"
						/>

          </child-body>
        </dashboard-child>
        `;
  }

  angular
    .module("pineapples")
    .component("echartBarChart", new Component());
}


