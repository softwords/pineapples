﻿/* <ECharteHorizontalBarChart />
 * Legacy Horizontal Bar Chart Component
 * TODO Revise this
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * headingOptions: js Array, items of which are displayed in dropdown in component header.  Selected item index selects dataset from datasets attribute for display
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * chartTitle: Title to display on chart
 * datasets: Top level selected by selected headingOption above
        i.e. {
              "ELA.2: Reading": [
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - F",
                      "percent": -33
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - F",
                      "percent": -50
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - M",
                      "percent": -33
                    }
                  ],
                  "color": "#FFC000",
                  "meta": "Approaching competence"
                },
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - F",
                      "percent": -17
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - F",
                      "percent": -50
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - M",
                      "percent": -67
                    }
                  ],
                  "color": "#FF0000",
                  "meta": "Well below competent"
                },
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - F",
                      "percent": 17
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - M",
                      "percent": 33
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - F",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - M",
                      "percent": 0
                    }
                  ],
                  "color": "#92D050",
                  "meta": "Minimally competent"
                },
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - F",
                      "percent": 33
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.1: Identify and use a variety of strategies to extend word meaning.  - M",
                      "percent": 67
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - F",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.2.8.2: Build comprehension of texts. - M",
                      "percent": 0
                    }
                  ],
                  "color": "#00B050",
                  "meta": "Competent"
                }
              ],
              "ELA.4: Literature": [
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - F",
                      "percent": -33
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - F",
                      "percent": -33
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - M",
                      "percent": -33
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - F",
                      "percent": -33
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - F",
                      "percent": -17
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - M",
                      "percent": 0
                    }
                  ],
                  "color": "#FFC000",
                  "meta": "Approaching competence"
                },
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - F",
                      "percent": -50
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - M",
                      "percent": -33
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - F",
                      "percent": -67
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - M",
                      "percent": -67
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - F",
                      "percent": -67
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - M",
                      "percent": -67
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - F",
                      "percent": -17
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - M",
                      "percent": -67
                    }
                  ],
                  "color": "#FF0000",
                  "meta": "Well below competent"
                },
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - F",
                      "percent": 17
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - M",
                      "percent": 67
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - F",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - F",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - M",
                      "percent": 33
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - F",
                      "percent": 50
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - M",
                      "percent": 0
                    }
                  ],
                  "color": "#92D050",
                  "meta": "Minimally competent"
                },
                {
                  "data": [
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - F",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.1: Listen to, read or view and respond to a narrative or poem. - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - F",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.2: Recognize and identify the complex elements of plot.  - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - F",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.3: Dramatize, record and write about the effects of culture and historical periods on literature and vice-versa. - M",
                      "percent": 0
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - F",
                      "percent": 17
                    },
                    {
                      "ExamBenchmark": "ELA.4.8.5: Apply knowledge of literal and figurative meanings to build vocabulary. - M",
                      "percent": 33
                    }
                  ],
                  "color": "#00B050",
                  "meta": "Competent"
                }
              ]
            }
 * datasetsX: key for objects in dataset attribute for x axis.  i.e. "percent"
 * datasetsY: key for objects in dataset attribute for y axis.  i.e. "ExamBenchmark"
 */

namespace Pineapples.Dashboards {

  class Controller {
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;

    public headingTitle: string;
    public headingFilters: string;

    public headingOptions: any;
   

    public chartTitle: string;
    public datasets: any;
    public datasetsX: any;
		public datasetsY: any;
    public datasetsMetadata: any;
    public datasetsColors: any;

    public chartData: any;			// this is the data that is relayed 

    public widgetPkg;
    public echart: echarts.ECharts;

		$onInit() {
			this.selectedViewOption = this.headingOptions ? this.headingOptions[0] : "";
    }

		private _selectedViewOption = '';
		public get selectedViewOption() {
			return this._selectedViewOption;
		}


		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.setChartData();
			}
		}

		private setChartData() {
			this.timeout(() => {
				this.chartData = this.headingOptions ? this.datasets[this.selectedViewOption] : this.datasets;
			}, 0, true);
		}

		static $inject = ["$timeout"]
    constructor(public timeout: ng.ITimeoutService) {
      this.componentId = uniqueId();
    }

    // This is ugly, but is required when components define their componentIds - Componenent Design recommends pushing state (like this) up. 
    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

    public eChartInit(echart) {
      this.echart = echart;
    }
    public $postLink() {
      // this will create an Echarts dataset
      // first collect all the X values
      this.makeWidgetPkg();
    }

    public $onChanges() {
      this.makeWidgetPkg();
    }


    public makeWidgetPkg() {
      if (!this.datasets) {
        return;
      }
      this.widgetPkg = {
        data: this.datasets,
        X: this.datasetsX,
        Y: this.datasetsY,
        seriesNames: this.datasetsMetadata
      }

    }

    public onChartRender(option, echart) {
			// nothing fancy here yet
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      headingOptions: "<?",

      chartTitle: '@',
      datasets: "<",
      datasetsX: "@",
      datasetsY: "@",
      datasetsMetadata: "<",
      datasetsColors: "<"
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `
					<dashboard-child class="dashboard-wrapper" style="padding: 5px;"
                         ng-class="vm.dimensions"
                         report-path="vm.reportPath"
                         toggle-selected="vm.toggleSelected"
                         is-selected="vm.isSelected()"
                         another-component-selected="vm.anotherComponentSelected()"
                         component-id="vm.componentId">

          <heading-title>{{vm.headingTitle}}</heading-title>
          <heading-filters>{{vm.headingFilters}}</heading-filters>
				  <heading-options>
            <md-select ng-if="vm.headingOptions" ng-model="vm.selectedViewOption" aria-label="Select">
              <md-option ng-value="opt" ng-repeat="opt in vm.headingOptions">{{ opt }}</md-option>
            </md-select>
          </heading-options>  
       
          <child-body>
            <row-col-chart
								charttype = 'h'
                dataset="vm.widgetPkg"
								on-chart-init="vm.eChartInit(echart)"
								on-chart-render="vm.onChartRender(option, echart)"
								colors="vm.datasetColors"/>
          </child-body>
        </dashboard-child>
        `;
  }

  angular
    .module("pineapples")
    .component("echartHorizontalBarChart", new Component());
}


