﻿/* <BarChart />
 * Bar Chart Component
 * 
 * Attributes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 
 * dim:    CrossFilter.Dimension
 * selectedKey:  Value to filter Dimension
 * onClick: dcCharts callback function
 * valueAccessors: the value accessors may be applied when building the chart data from the crossfilter group
 *  Each value accessor specifies 'columns' and valueAccessor, as required by the 'AnotatedGroup' format.
 */

namespace Pineapples.Dashboards {

  
  class Controller {

    headingTitle: string = 'You forgot to pass heading-title';
    headingFilters: string = 'You forgot to pass heading-filters';
    selectedChild: any;
    toggleSelected: any;
    dimensions: string;
    reportPath: string;
	

		public group: CrossFilter.Group<any, any, any>;					// now a binding
		public chartData;					//'AnnotatedGroup'
		public touch;																	
		selectedKey: any;
		public charttype;					// binding to the charttype
		public allowedTypes: string[];

	
		private componentId: string;
		isSelected = () => this.componentId == this.selectedChild;
		anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

		static $inject = ['Lookups'];
		constructor(public lookups) {
			this.componentId = uniqueId();
		}

		//////////////////////////////////////////////////////////////////////
		// Life Cycle events
		//////////////////////////////////////////////////////////////////////
    $onInit() {
			if (!this.valueAccessors) {
				// simplest case - we just have a group
				this.chartData = this.group;
				return;
			}
			if (Sw.Charts.EChartTransformer.hasAllProps(this.valueAccessors, ["columns", "valueAccessor"])) {
				// its just a simple, unnamed object with the columns and valueAccessor
				this.chartData = {
					group: this.group,
					columns: this.valueAccessors.columns,
					valueAccessor: this.valueAccessors.valueAccessor
				}
				return;			
			}
			// if the valueAccessors are named, allow the UI for them
			this.viewOptions = _.keys(this.valueAccessors)
			this.selectedViewOption = this.viewOptions[0];

			// build the 'annotated group'
			// refer to the columns and valueAccessor indirectly - the chartData is only set once
			this.chartData = {
				group: this.group,
				columns: (r,c,i) => this.valueAccessors[this.selectedViewOption].columns,
				valueAccessor: (r,c,i) => this.valueAccessors[this.selectedViewOption].valueAccessor(r,c,i)

			}
 		}

		public $postLink() {
			this.touch = Date.now();
		}
		public $onChanges(changes) {
			if (changes.group && changes.group.isFirstChange()) {
				return;
			}
			if (changes.optionChange) {
				this.touch = Date.now();
			}
		}
		///////////////////////////////////////////////////////////////////////
		// View options
		///////////////////////////////////////////////////////////////////////

		public valueAccessors;
		public viewOptions;

		private _selectedViewOption;

		public get selectedViewOption() {
			return this._selectedViewOption;
		}

		public set selectedViewOption(newValue) {
			if (this._selectedViewOption != newValue) {
				this._selectedViewOption = newValue;
				this.touch = Date.now();

			}
		}
		//////////////////////////////////////////////////////////////////////
		// eChart event handlers
		//////////////////////////////////////////////////////////////////////
		private onClick: any;			// binding from caller - assumed to be a toggle function for options
		public clickHandler(params) {
			this.onClick({ key: params.name });
		}

		//////////////////////////////////////////////////////////////////////
		// echart callbacks
		// key formatter => xAxis.axisLabel.formatter
		private keyFormatter: any;
		private doKeyFormatter = (key) => {
			return this.keyFormatter({ key }) || key;
		}
		//////////////////////////////////////////////////////////////////////

		public onChartRender(option: echarts.EChartOption, echart: echarts.ECharts) {
			if (option.series.length == 1) {
				option.legend.show=false;
				// tooltip - we can play with the tooltip string template, or provide
				// a formatter function
				//see  https://echarts.apache.org/en/option.html#tooltip.formatter for the 'String template'
				// But the easiest way to get the series name out of the tooltip is
				// to set the series name = '' or delete it
				(<echarts.EChartOption.SeriesBar>option.series[0]).name = "";
			}
			// otherwise 
			switch (this.charttype) {
				case "pie":
					break;
				case "v":
				case "vc":
				case "vp":
					angular.merge(option, {
						xAxis: {
							axisLabel: {
								formatter: this.doKeyFormatter
							}
						}
					});
					break;
			}
		}
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        // For <dashboard-child>
        dimensions: "@",
        selectedChild: "<",
        headingTitle: "@?",
        headingFilters: "<?",
        reportPath: "@",
        toggleSelected: "<",

				valueAccessors: "<", 
        group: "<",

				selectedKey: "<",

				charttype: "@",						// most likely a literal value here'
				allowedTypes: "<",				// if supplied, this is an array of the chart types that will be 
																	// available to select in the rowColChart UI
				
				// somehow we need to know that the chart must be redrawn
				// using dc, we could call dc.redraw in the dashboard
				// without dc, we have to get notified of the change...
				optionChange: "<",		

				onClick: "&",				//	make a callback using &
				keyFormatter: "&"		// callback to format the row identifier
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/DashboardChildChart`;
    }
  }

  angular
    .module("pineapples")
    .component("dashboardChildChart", new Component());
}