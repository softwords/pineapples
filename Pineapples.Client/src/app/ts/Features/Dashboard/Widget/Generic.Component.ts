﻿/*
 * <Generic />
 * This is a Wrapper over DashboardChild, 
 * that creates a dashboard component that displays transcluded child markup.
 * Used to create simple components i.e. Single Values, simple tables
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * headingOptions: js object, keys of which are displayed in dropdown in component header.  Values corrosponding to selected option are available to the component to customise the view.
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
*/

namespace Pineapples.Dashboards {
  const memoizeKeys = _.memoize(_.keys);
  
  class Controller {

    public headingTitle: string = 'You forgot to pass heading-title';
    public headingFilters: string = 'You forgot to pass heading-filters';
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;

    public headingOptions: any;

    // This is a bit ugly, but fits with Dashboards not defining componentIds - Componenent Design recommends pushing state (like this) up. 
    public componentId: string;
    public isSelected = () => this.componentId == this.selectedChild;
    public anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

    static $inject = ['Lookups'];
    constructor(public lookups) {
      this.componentId = uniqueId();
    }
  }

  class Component implements ng.IComponentOptions {
      public bindings: any = {
        dimensions: "@",
        selectedChild: "<",
        headingTitle: "@?",
        headingFilters: "<?",
        headingOptions: "<?",
        reportPath: "@",
        toggleSelected: "<",
      };
    public controller: any = Controller;
    public controllerAs: string = "vm";
    public templateUrl: string = `dashboard/widget/Generic`;
    public transclude: boolean = true;
    }

  angular
    .module("pineapples")
    .component("generic", new Component());
}