﻿/* <SimpleTable />
 * Simple Table Component
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * 
 * group:  CrossFilter.group
 * selectedKey: value to filter Dimension
 * rowLabelsLookup: lookup name (using lookups service)
 * onClick: dcCharts callback function
 * colHeadings: Array of column headings
 */

namespace Pineapples.Dashboards {

 
  class Controller {

    headingTitle: string = 'You forgot to pass heading-title';
    headingFilters: string = 'You forgot to pass heading-filters';
    selectedChild: any;
    toggleSelected: any;
    dimensions: string;
    reportPath: string;
    colHeadings: any;

    group: any;
    selectedKey: any;
    onClick: any;

    public componentId: string;
		public isSelected = () => this.componentId == this.selectedChild;

    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

    static $inject = ['Lookups'];
    constructor(public lookups) {
      this.componentId = uniqueId();
    }

		public clickHandler(key) {
			this.onClick({ key });
		}
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        // For <dashboard-child>
        dimensions: "@",
        selectedChild: "<",
        headingTitle: "@?",
        headingFilters: "<?",
        reportPath: "@",
        toggleSelected: "<",			

        group: "<",
        selectedKey: "<",
        onClick: "&",						// change to & binding for consistency
        rowLabelsLookup: "@",
        colHeadings: "<",

      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/SimpleTable`;
    }
  }

  angular
    .module("pineapples")
    .component("simpleTable", new Component());
}