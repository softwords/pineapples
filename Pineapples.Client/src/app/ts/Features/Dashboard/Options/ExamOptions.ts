﻿namespace Pineapples.Dashboards {


	export class examOptions extends coreOptions {

		//-------------------------------------------------------------------------------------
		// Cost Centre
		//-------------------------------------------------------------------------------------

		public get selectedExamCode() {
			return this.getSelected("ExamCode");
		};

		public set selectedExamCode(newValue) {
			this.setSelected("ExamCode", newValue);
		};

    /**
     * helper function to check if the supplied value is the currently selectedExamCode
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedExamCode ( ie 'all' are selected) return true
     */
		public isSelectedExamCode(testValue) {
			return this.isSelected("ExamCode", testValue);
		}

		public isIncludedExamCode(testValue) {
			return this.isIncluded("ExamCode", testValue);
		}

    /**
     * Return true if there is no selected ExamCode
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedExamCode() {
			return !this.selectedExamCode;
		}

    /**
     * if ExamCode is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedExamCode(newValue) {
			if (!newValue) {
				return;
			}
			if (newValue == this.selectedExamCode) {
				this.selectedExamCode = null;
			} else {
				this.selectedExamCode = newValue;
			}
		}

    /**
     * Locked the ExamCode so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed ExamCode value becomes the value of selectedExamCode
     */
		public lockExamCode(fixedValue) {
			this.lock("ExamCode", fixedValue);
		}

    /**
     * Return true if district is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedExamCode() {
			return this.isLocked("ExamCode");
		}

		public resetAll() {
			// keep only the year and exam code
			for (const key in this._selected) {
				switch (key) {
					case "Year":
					case "ExamCode":
						break;
					default:
						if (this._selected[key]) {
							this.setSelected(key, null);		// this will fire the notification
						}
				}

			};
		}

		public descriptionExcluding(...exclude) {

			return `
       ${!_(exclude).includes('Year') && this.selectedYear || ''}
			 ${!_(exclude).includes('ExamCode') && this.lookups.byCode('examTypes', this.selectedExamCode, 'N') || ''}
       ${!_(exclude).includes('District') && this.lookups.byCode('districts', this.selectedDistrict, 'N') || ''}
       ${!_(exclude).includes('Authority') && this.lookups.byCode('authorities', this.selectedAuthority, 'N') || ''}
       ${!_(exclude).includes('AuthorityGovt') && this.lookups.byCode('authorityGovts', this.selectedAuthorityGovt, 'N') || ''}
       ${!_(exclude).includes('SchoolType') && this.lookups.byCode('schoolTypes', this.selectedSchoolType, 'N') || ''}
      `
		}
	}
}

