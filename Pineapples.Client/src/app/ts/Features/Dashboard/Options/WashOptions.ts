﻿namespace Pineapples.Dashboards {


	export class WashOptions extends coreOptions {

		//-------------------------------------------------------------------------------------
		// Wash Question
		//-------------------------------------------------------------------------------------

		public get selectedQuestion() {
			return this.getSelected("Question");
		};

		public set selectedQuestion(newValue) {
			this.setSelected("Question", newValue);
		};

    /**
     * helper function to check if the supplied value is the currently selectedQuestion
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedQuestion ( ie 'all' are selected) return true
     */
		public isSelectedQuestion(testValue) {
			return this.isSelected("Question", testValue);
		}

		public isIncludedQuestion(testValue) {
			return this.isIncluded("Question", testValue);
		}

    /**
     * Return true if there is no selected Question
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedQuestion() {
			return !this.selectedQuestion;
		}

    /**
     * if Question is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedQuestion(newValue) {
			if (!newValue) {
				return;
			}
			if (newValue == this.selectedQuestion) {
				this.selectedQuestion = null;
			} else {
				this.selectedQuestion = newValue;
			}
		}

    /**
     * Locked the Question so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed Question value becomes the value of selectedQuestion
     */
		public lockQuestion(fixedValue) {
			this.lock("Question", fixedValue);
		}

    /**
     * Return true if district is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedQuestion() {
			return this.isLocked("Question");
		}
	}
}

