﻿namespace Pineapples.Dashboards {


	export class scholarshipOptions extends coreOptions {

		/**
		 * override resetall - clear the year
		 * it makes sense to accumulate scholarships awarded across years
		 */
		public resetAll = () => {
			super.resetAll();
			this.setSelected("Year", null);	
		};

		// year can be toggled 
		public toggleSelectedYear(newValue) {
			this.toggleSelected("Year", newValue);
		}
		//-------------------------------------------------------------------------------------
		// ScholarshipType 
		//-------------------------------------------------------------------------------------

		public get selectedScholarshipType() {
			return this.getSelected("ScholarshipType");
		};

		public set selectedScholarshipType(newValue) {
			this.setSelected("ScholarshipType", newValue);
		};
		public get selectedScholarshipTypeName() {
			return this.lookups.cache['scholarshipTypes'].byCode(this.selectedScholarshipType, "N");
		}
    /**
     * helper function to check if the supplied value is the currently selectedScholarshipType
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedScholarshipType ( ie 'all' are selected) return true
     */
		public isSelectedScholarshipType(testValue) {
			return this.isSelected("ScholarshipType", testValue);
		}

		public isIncludedScholarshipType(testValue) {
			return this.isIncluded("ScholarshipType", testValue);
		}

    /**
     * Return true if there is no selected ScholarshipType
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedScholarshipType() {
			return !this.selectedScholarshipType;
		}

    /**
     * if ScholarshipType is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedScholarshipType(newValue) {
			if (!newValue) {
				return;
			} else {
				newValue = this.lookups.cache["scholarshipTypes"].search(newValue, 'C');
			}
			if (newValue == this.selectedScholarshipType) {
				this.selectedScholarshipType = null;
			} else {
				this.selectedScholarshipType = newValue;
			}
		}

    /**
     * Locked the ScholarshipType so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed ScholarshipType value becomes the value of selectedScholarshipType
     */
		public lockScholarshipType(fixedValue) {
			this.lock("ScholarshipType", fixedValue);
		}

    /**
     * Return true if district is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedScholarshipType() {
			return this.isLocked("ScholarshipType");
		}


		//-------------------------------------------------------------------------------------
		// Major 
		//-------------------------------------------------------------------------------------

		public get selectedMajor() {
			return this.getSelected("Major");
		};

		public set selectedMajor(newValue) {
			this.setSelected("Major", newValue);
		};

		public get selectedMajorName() {
			return this.lookups.cache['fieldsOfStudy'].byCode(this.selectedMajor,"N");
		}
    /**
     * helper function to check if the supplied value is the currently selectedMajor
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedMajor ( ie 'all' are selected) return true
     */
		public isSelectedMajor(testValue) {
			return this.isSelected("Major", testValue);
		}

		public isIncludedMajor(testValue) {
			return this.isIncluded("Major", testValue);
		}

    /**
     * Return true if there is no selected Major
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedMajor() {
			return !this.selectedMajor;
		}

    /**
     * if Major is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedMajor(newValue) {
			if (!newValue) {
				return;
			}
			if (newValue == 'na') {
				newValue = null;
			} else {
				newValue = this.lookups.cache["fieldsOfStudy"].search(newValue, 'C');
			}
			if (newValue == this.selectedMajor) {
				this.selectedMajor = null;
			} else {
				this.selectedMajor = newValue;
			}
		}

    /**
     * Locked the Major so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed Major value becomes the value of selectedMajor
     */
		public lockMajor(fixedValue) {
			this.lock("Major", fixedValue);
		}

    /**
     * Useful for the UI to set disabled status on a control
     */
		public isLockedMajor() {
			return this.isLocked("Major");
		}


		//-------------------------------------------------------------------------------------
		// Status 
		//-------------------------------------------------------------------------------------

		public get selectedStatus() {
			return this.getSelected("Status");
		};

		public set selectedStatus(newValue) {
			this.setSelected("Status", newValue);
		};

		public get selectedStatusName() {
			return this.lookups.cache['scholarshipStatus'].byCode(this.selectedStatus, "N");
		}
		/**
		 * helper function to check if the supplied value is the currently selectedStatus
		 * @param testValue value to test
		 * @param trueWhenAll, if there is no selectedStatus ( ie 'all' are selected) return true
		 */
		public isSelectedStatus(testValue) {
			return this.isSelected("Status", testValue);
		}

		public isIncludedStatus(testValue) {
			return this.isIncluded("Status", testValue);
		}

		/**
		 * Return true if there is no selected Status
		 * Useful for the client to show or hide totals, rather than a selected value
		 */
		public noSelectedStatus() {
			return !this.selectedStatus;
		}

		/**
		 * if Status is selected, turn off; turn on
		 * useful for responding to click events on table rows, chart elements in the Ui
		 */
		public toggleSelectedStatus(newValue) {
			if (!newValue) {
				return;
			}
			newValue = this.lookups.cache['scholarshipStatus'].search(newValue, 'C');
			if (newValue == this.selectedStatus) {
				this.selectedStatus = null;
			} else {
				this.selectedStatus = newValue;
			}
		}

		/**
		 * Locked the Status so it cannot be changed
		 * Useful for implementing some 'security' in the dashboard to restrict a view
		 * @param fixedValue the fixed Status value becomes the value of selectedStatus
		 */
		public lockStatus(fixedValue) {
			this.lock("Status", fixedValue);
		}

		/**
		 * Useful for the UI to set disabled status on a control
		 */
		public isLockedStatus() {
			return this.isLocked("Status");
		}

		public descriptionExcluding(...exclude) {

			return `
				${!_(exclude).includes('Year') && this.selectedYear || ''}
				${!_(exclude).includes('ScholarshipType') && this.lookups.byCode('scholarshipTypes', this.selectedScholarshipType, 'N') || ''}
				${!_(exclude).includes('Major') && this.lookups.byCode('fieldsOfStudy', this.selectedMajor, 'N') || ''}
				${!_(exclude).includes('Status') && this.lookups.byCode('scholarshipStatus', this.selectedStatus, 'N') || ''}
				${!_(exclude).includes('District') && this.lookups.byCode('districts', this.selectedDistrict, 'N') || ''}
				${!_(exclude).includes('Authority') && this.lookups.byCode('authorities', this.selectedAuthority, 'N') || ''}
				${!_(exclude).includes('AuthorityGovt') && this.lookups.byCode('authorityGovts', this.selectedAuthorityGovt, 'N') || ''}
				${!_(exclude).includes('SchoolType') && this.lookups.byCode('schoolTypes', this.selectedSchoolType, 'N') || ''}
      `
		}

	}
}

