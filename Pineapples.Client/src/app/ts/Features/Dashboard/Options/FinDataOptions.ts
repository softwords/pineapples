﻿namespace Pineapples.Dashboards {


	export class finDataOptions extends coreOptions {

		//-------------------------------------------------------------------------------------
		// Cost Centre
		//-------------------------------------------------------------------------------------

		public get selectedCostCentre() {
			return this.getSelected("CostCentre");
		};

		public set selectedCostCentre(newValue) {
			this.setSelected("CostCentre", newValue);
		};

    /**
     * helper function to check if the supplied value is the currently selectedCostCentre
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedCostCentre ( ie 'all' are selected) return true
     */
		public isSelectedCostCentre(testValue) {
			return this.isSelected("CostCentre", testValue);
		}

		public isIncludedCostCentre(testValue) {
			return this.isIncluded("CostCentre", testValue);
		}

    /**
     * Return true if there is no selected CostCentre
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedCostCentre() {
			return !this.selectedCostCentre;
		}

    /**
     * if CostCentre is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedCostCentre(newValue) {
			if (!newValue) {
				return;
			}
			if (newValue == this.selectedCostCentre) {
				this.selectedCostCentre = null;
			} else {
				this.selectedCostCentre = newValue;
			}
		}

    /**
     * Locked the CostCentre so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed CostCentre value becomes the value of selectedCostCentre
     */
		public lockCostCentre(fixedValue) {
			this.lock("CostCentre", fixedValue);
		}

    /**
     * Return true if district is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedCostCentre() {
			return this.isLocked("CostCentre");
		}
	}
}

