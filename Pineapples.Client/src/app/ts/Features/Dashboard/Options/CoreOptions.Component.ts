﻿namespace Pineapples.Dashboards {


  class Controller {
    public options: any

    static $inject = ["Lookups"]
		constructor(public lookups: Sw.Lookups.LookupService) { }

		public $onInit() {
			
		}

		public $onChanges(changes) {

		}

    public resetAll = () => this.options.resetAll();

  }

	class WashController extends Controller {
		public searchQText;
		public questions: Wash.IQuestion[];
		public options: WashOptions;

		public $onInit() {
			super.$onInit();
			this.questions = _.filter(this.questions
				, q => ['CHOOSE|SINGLE', 'CHOOSE|MULTIPLE', 'BINARY', 'TERNARY'].indexOf(q.QFlags) >= 0);
		}

		public searchQuestions(searchText) {
			if (!searchText) {
				return this.questions;
			}
			let lower = searchText.toLowerCase();
			return this.questions.filter(q => {
				return (q.QID.toLowerCase().indexOf(lower) >= 0) || (q.QName.toLowerCase().indexOf(lower) >= 0)
			});
		}

		private _selectedQuestionItem: Wash.IQuestion;
		public get selectedQuestionItem() {
			return this._selectedQuestionItem;
		}

		public applyQuestion(newValue: Wash.IQuestion) {
			console.log("ApplyQuestion");
			console.log(newValue);
		}

		public set selectedQuestionItem(newValue: Wash.IQuestion) {
			if (this._selectedQuestionItem && newValue &&
				this._selectedQuestionItem.QID == newValue.QID) {
				return;
			}
			this._selectedQuestionItem = newValue;
			// newValue can be null when selected item is cleared
			if (newValue && newValue.QID) {  
				this.options.selectedQuestion = newValue.QID;
			}
		}


		public resetAll = () => {
			this.options.resetAll();
			this._selectedQuestionItem = null;
		}
	}
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor(templateUrl) {
      this.bindings = {
        options: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = templateUrl;
    }
  }

	class WashComponent implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor(templateUrl) {
			this.bindings = {
				options: "<",
				questions: "<"
			};
			this.controller = WashController;
			this.controllerAs = "vm";
			this.templateUrl = templateUrl;
		}


	}
  angular
    .module("pineapples")
    .component("coreOptionsEditor", new Component("dashboard/options/core"))
		.component("indicatorOptionsEditor", new Component("dashboard/options/indicator"))
		.component("findataOptionsEditor", new Component("dashboard/options/findata"))
		.component("examOptionsEditor", new Component("dashboard/options/exam"))
		.component("scholarshipOptionsEditor", new Component("dashboard/options/scholarship"))
		.component("washOptionsEditor", new WashComponent("dashboard/options/wash"));
}