﻿namespace Pineapples.Dashboards {

  /**
   * CoreOptions provides selectors for the key "dimensions" of a dashboard:
   *
   *  Year - the survey year or reporting year
   *  District - aka state, province
	 *  Island - 
	 *  Region - 
   *  SchoolType - school type
   *  SchoolClass - adhoc code
   *  Authority - managing authority
   *  AuthorityGov - authorityGovNonGov aggregation
   *
   *  EducationLevel - level of education
   *  Sector         - education sector
   *
   *  Gender - of pupils, or teachers
   *
   * for 'at a glance' or indicator pages we may compare two years -
   *  BaseYear - the base year for comparison, the comparison year is Year

   * This class can be used by a dashboard system to manage filtering of data 
   */
	export class coreOptions {
		public eventName = ":option_change";
		constructor(protected lookups: Sw.Lookups.LookupService, private hub: Sw.IHub) {
			// to do: make a unique id for this instance of the class,
			// so we can in theory have two instances running and they will not conflict
			this.eventName = "coreoptions:option_change";
		}

		public resetAll() {
			// keep only the year
			for (const key in this._selected) {
				switch (key) {
					case "Year":
						break;
					default:
						if (this._selected[key]) {
							this.setSelected(key, null);		// this will fire the notification
						}
				}

			};
		}

		protected _locked: { [param: string]: boolean } = {};
		protected _selected: { [param: string]: string } = {};


		protected getSelected(param: string) {
			return this._selected[param];
		}

		protected setSelected(param: string, newValue) {
			// treat all falsy values as null
			// crossfilter treats filter(null) as FilterAll() so this is convenient upstream
			if (this._locked[param]) {
				return;
			}
			if (!newValue) {
				newValue = null;
			}
			if (newValue !== this._selected[param]) {
				let publishData = {};
				publishData[`selected${param}`] = {
					new: newValue,
					old: this._selected[param]
				}
				this._selected[param] = newValue;
				this.hub.publish(this.eventName, publishData, this);
			}
		}

		protected toggleSelected(param:string, newValue) {
			if (!newValue) {
				return;
			}
			newValue = this.codeFromName(param, newValue);
			newValue = (newValue == this._selected[param] ? null : newValue);
			this.setSelected(param, newValue);;
		}

		protected isSelected(param, testValue) {
			return (this._selected[param] == testValue);
		}

		protected isIncluded(param, testValue) {
			if (!this._selected[param]) {
				return true;
			}
			return (this._selected[param] == testValue);
		}

		protected lock(param: string, fixedValue) {
			this._selected[param] = fixedValue;
			this._locked[param] = true;
		}

		protected isLocked(param: string) {
			return this._locked[param];
		}

		public codeFromName(param: string, value) {
			let lst: Sw.Lookups.ILookupList;
			switch (param) {
				case "District":
					lst = this.lookups.cache["districts"];
					break;
				default:
					return value;
			}
			let lkp = _.find(lst, l => l.N == value);
			return (lkp ? lkp.C : value);
		}

		// -----------------------------------------------------------------------------
		// selectedYear
		// -----------------------------------------------------------------------------
		// getter and setter for selectedYear
		public get selectedYear() {
			return Number(this._selected["Year"]);
		};

		public set selectedYear(newValue) {
			this.setSelected("Year", newValue);
		}

    /**
     * helper function to check if a year value is the currently selected value
     * user interface can use this to apply a style to a table row, or to a chart bar
     * @param testValue value to test
		 * isIncludedYear is true if this is the selected year, or there is no selected year
		 * Use for displays when you only want to show the selected
     * ng-show="vm.options.isIncludedYear(r.key)"
     * ng-class="{'selected': vm.options.isSelectedYear(r.key)}"
     */
		public isSelectedYear(testValue, trueWhenAll?: boolean) {
			return this.isSelected("Year", testValue);
		}

		public isIncludedYear(testValue, trueWhenAll?: boolean) {
			return this.isIncluded("Year", testValue);
		}
		
		//-------------------------------------------------------------------------------------
		//selectedDistrict

		public get selectedDistrict() {
			return this.getSelected("District");
		};

		public set selectedDistrict(newValue) {
			// treat all falsy values as null
			// crossfilter treats filter(null) as FilterAll() so this is convenient upstream
			this.setSelected("District", newValue);
		}
		/**
		 * helper function to check if the supplied value is the currently selectedDistrict
		 * @param testValue value to test
		 */
		public isSelectedDistrict(testValue) {
			return this.isSelected("District", testValue);
		}

		/**
		 * return true if the parameter is either the selected district, or no district is selected
		 * @param testValue
		 */
		public isIncludedDistrict(testValue) {
			return this.isIncluded("District", testValue);
		}
		/**
		 * Return true if there is no selected district
		 * Useful for the client to show or hide totals, rather than a selected value
		 */
		public noSelectedDistrict() {
			return !this.selectedDistrict;
		}

		/**
		 * if district is selected, turn off; turn on
		 * useful for responding to click events on table rows, chart elements in the Ui
		 */
		public toggleSelectedDistrict(newValue) {
			this.toggleSelected("District", newValue);
		}

		/**
		 * Locked the district so it cannot be changed
		 * Useful for implementing some 'security' in the dashboard to restrict a view
		 * @param fixedValue the fixed district value becomes the value of selectedDistrict
		 */
		public lockDistrict(fixedValue) {
			this.lock("District", fixedValue);
		}

		/**
		 * Return true if district is locked
		 * Useful for the UI to set disabled status on a control
		 */
		public isLockedDistrict() {
			return this.isLocked("District");
		}

		//-------------------------------------------------------------------------------------
		//selectedIsland

		public get selectedIsland() {
			return this.getSelected("Island");
		};

		public set selectedIsland(newValue) {
			// treat all falsy values as null
			// crossfilter treats filter(null) as FilterAll() so this is convenient upstream
			this.setSelected("Island", newValue);
		}
		/**
		 * helper function to check if the supplied value is the currently selectedIsland
		 * @param testValue value to test
		 */
		public isSelectedIsland(testValue) {
			return this.isSelected("Island", testValue);
		}

		/**
		 * return true if the parameter is either the selected island, or no island is selected
		 * @param testValue
		 */
		public isIncludedIsland(testValue) {
			return this.isIncluded("Island", testValue);
		}
		/**
		 * Return true if there is no selected island
		 * Useful for the client to show or hide totals, rather than a selected value
		 */
		public noSelectedIsland() {
			return !this.selectedIsland;
		}

		/**
		 * if island is selected, turn off; turn on
		 * useful for responding to click events on table rows, chart elements in the Ui
		 */
		public toggleSelectedIsland(newValue) {
			this.toggleSelected("Island", newValue);
		}

		/**
		 * Locked the Island so it cannot be changed
		 * Useful for implementing some 'security' in the dashboard to restrict a view
		 * @param fixedValue the fixed island value becomes the value of selectedIsland
		 */
		public lockIsland(fixedValue) {
			this.lock("Island", fixedValue);
		}

		/**
		 * Return true if island is locked
		 * Useful for the UI to set disabled status on a control
		 */
		public isLockedIsland() {
			return this.isLocked("Island");
		}


		//-------------------------------------------------------------------------------------
		//selectedRegion

		public get selectedRegion() {
			return this.getSelected("Region");
		};

		public set selectedRegion(newValue) {
			// treat all falsy values as null
			// crossfilter treats filter(null) as FilterAll() so this is convenient upstream
			this.setSelected("Region", newValue);
		}
		/**
		 * helper function to check if the supplied value is the currently selectedRegion
		 * @param testValue value to test
		 */
		public isSelectedRegion(testValue) {
			return this.isSelected("Region", testValue);
		}

		/**
		 * return true if the parameter is either the selected region, or no region is selected
		 * @param testValue
		 */
		public isIncludedRegion(testValue) {
			return this.isIncluded("Region", testValue);
		}
		/**
		 * Return true if there is no selected region
		 * Useful for the client to show or hide totals, rather than a selected value
		 */
		public noSelectedRegion() {
			return !this.selectedRegion;
		}

		/**
		 * if region is selected, turn off; turn on
		 * useful for responding to click events on table rows, chart elements in the Ui
		 */
		public toggleSelectedRegion(newValue) {
			this.toggleSelected("Region", newValue);
		}

		/**
		 * Locked the region so it cannot be changed
		 * Useful for implementing some 'security' in the dashboard to restrict a view
		 * @param fixedValue the fixed region value becomes the value of selectedRegion
		 */
		public lockRegion(fixedValue) {
			this.lock("Region", fixedValue);
		}

		/**
		 * Return true if region is locked
		 * Useful for the UI to set disabled status on a control
		 */
		public isLockedRegion() {
			return this.isLocked("Region");
		}

		//-------------------------------------------------------------------------------------
		//selectedAuthority

		public get selectedAuthority() {
			return this._selected["Authority"];
		};

		public set selectedAuthority(newValue) {
			this.setSelected("Authority", newValue);
		}

    /**
     * helper function to check if the supplied value is the currently selectedAuthority
     * @param testValue value to test
     */
		public isSelectedAuthority(testValue) {
			return this.isSelected("Authority", testValue);
		}

		public isIncludedAuthority(testValue) {
			return this.isIncluded("Authority", testValue);
		}

    /**
     * Return true if there is no selected Authority
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedAuthority() {
			return !this.selectedAuthority;
		}

    /**
     * if Authority is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedAuthority(newValue) {
			this.toggleSelected("Authority", newValue);
		}

    /**
     * Locked the Authority so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed Authority value becomes the value of selectedAuthority
     */
		public lockAuthority(fixedValue) {
			this.lock("Authority", fixedValue);
		}

    /**
     * Return true if Authority is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedAuthority() {
			return this.isLocked("Authority");
		}

		//-------------------------------------------------------------------------------------
		// SchoolType

		public get selectedSchoolType() {
			return this._selected["SchoolType"];
		};

		public set selectedSchoolType(newValue) {
			this.setSelected("SchoolType", newValue);
		};

    /**
     * helper function to check if the supplied value is the currently selectedSchoolType
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedSchoolType ( ie 'all' are selected) return true
     */
		public isSelectedSchoolType(testValue) {
			return this.isSelected("SchoolType", testValue);
		}

		public isIncludedSchoolType(testValue) {
			return this.isIncluded("SchoolType", testValue);
		}

    /**
     * Return true if there is no selected SchoolType
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedSchoolType() {
			return !this.selectedSchoolType;
		}

    /**
     * if SchoolType is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedSchoolType(newValue) {
			if (!newValue) {
				return;
			}
			if (newValue == this.selectedSchoolType) {
				this.selectedSchoolType = null;
			} else {
				this.selectedSchoolType = newValue;
			}
		}

    /**
     * Locked the SchoolType so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed SchoolType value becomes the value of selectedSchoolType
     */
		public lockSchoolType(fixedValue) {
			this.lock("SchoolType", fixedValue);
		}

    /**
     * Return true if school type is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedSchoolType() {
			return this.isLocked("SchoolType");
		}

		//-------------------------------------------------------------------------------------
		// AuthorityGovt
		// this is derived from Authority and is typically used to group authorities
		// - and by extension, schools and enrolments - into Govt/Non govt sectors.

		public get selectedAuthorityGovt() {
			return this._selected["AuthorityGovt"];
		};

		public set selectedAuthorityGovt(newValue) {
			this.setSelected("AuthorityGovt", newValue);
		}
    /**
     * helper function to check if the supplied value is the currently selectedAuthorityGovt
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedAuthorityGovt( ie 'all' are selected) return true
     */
		public isSelectedAuthorityGovt(testValue) {
			return this.isSelected("AuthorityGovt", testValue);
		}

		public isIncludedAuthorityGovt(testValue) {
			return this.isIncluded("AuthorityGovt", testValue);
		}

    /**
     * Return true if there is no selected AuthorityGovt
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedAuthorityGovt() {
			return !this.selectedAuthorityGovt;
		}

    /**
     * if AuthorityGovt is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedAuthorityGovt(newValue) {
			this.toggleSelected("AuthorityGovt", newValue);
		}

    /**
     * Locked the AuthorityGovt so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed AuthorityGovt value becomes the value of selectedAuthorityGovt
     */
		public lockAuthorityGovt(fixedValue) {
			this.lock("AuthorityGovt", fixedValue);
		}

    /**
     * Return true if AuthorityGovt is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedAuthorityGovt() {
			return this.isLocked("AuthorityGovt");
		}

		// use this version of subscribe so that the coreOptions can register its unique message tag
		public subscribe(func: (data, sender) => void) {
			this.hub.subscribe(this.eventName, func);
		}

		//-------------------------------------------------------------------------------------
		//selectedEducationLevel
		public get selectedEducationLevel() {
			return this._selected["EducationLevel"];
		};

		public set selectedEducationLevel(newValue) {
			this.setSelected("EducationLevel", newValue);
		}
    /**
     * helper function to check if the supplied value is the currently selectedEducationLevel
     * @param testValue value to test
     */
		public isSelectedEducationLevel(testValue) {
			return this.isSelected("EducationLevel", testValue);
		}

		public isIncludedEducationLevel(testValue) {
			return this.isIncluded("EducationLevel", testValue);
		}

    /**
     * Return true if there is no selected EducationLevel
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedEducationLevel() {
			return !this.selectedEducationLevel;
		}

    /**
     * if EducationLevel is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedEducationLevel(newValue) {
			this.toggleSelected("EducationLevel", newValue);
		}

    /**
     * Locked the EducationLevel so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed EducationLevel value becomes the value of selectedEducationLevel
     */
		public lockEducationLevel(fixedValue) {
			this.lock("EducationLevel", fixedValue);
		}

    /**
     * Return true if EducationLevel is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedEducationLevel() {
			return this.isLocked("EducationLevel");
		}

		//-------------------------------------------------------------------------------------
		//selectedSchoolClass

		public get selectedSchoolClass() {
			return this._selected["SchoolClass"];
		};

		public set selectedSchoolClass(newValue) {
			this.setSelected("SchoolClass", newValue);
		}

    /**
     * helper function to check if the supplied value is the currently selectedSchoolClass
     * @param testValue value to test
     */
		public isSelectedSchoolClass(testValue) {
			return this.isSelected("SchoolClass", testValue);
		}

		public isIncludedSchoolClass(testValue) {
			return this.isIncluded("SchoolClass", testValue);
		}

    /**
     * Return true if there is no selected SchoolClass
     * Useful for the client to show or hide totals, rather than a selected value
     */
		public noSelectedSchoolClass() {
			return !this.selectedSchoolClass;
		}

    /**
     * if SchoolClass is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
		public toggleSelectedSchoolClass(newValue) {
			this.toggleSelected("SchoolClass", newValue);
		}

    /**
     * Locked the SchoolClass so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed SchoolClass value becomes the value of selectedSchoolClass
     */
		public lockSchoolClass(fixedValue) {
			this.lock("SchoolClass", fixedValue);
		}

    /**
     * Return true if SchoolClass is locked
     * Useful for the UI to set disabled status on a control
     */
		public isLockedSchoolClass() {
			return this.isLocked("SchoolClass");
		}

		public descriptionExcluding(...exclude) {

			return `
       ${!_(exclude).includes('Year') && this.selectedYear || ''}
       ${!_(exclude).includes('District') && this.lookups.byCode('districts', this.selectedDistrict, 'N') || ''}
       ${!_(exclude).includes('Island') && this.lookups.byCode('islands', this.selectedIsland, 'N') || ''}
       ${!_(exclude).includes('Region') && this.lookups.byCode('regions', this.selectedRegion, 'N') || ''}
       ${!_(exclude).includes('Authority') && this.lookups.byCode('authorities', this.selectedAuthority, 'N') || ''}
       ${!_(exclude).includes('AuthorityGovt') && this.lookups.byCode('authorityGovts', this.selectedAuthorityGovt, 'N') || ''}
       ${!_(exclude).includes('SchoolType') && this.lookups.byCode('schoolTypes', this.selectedSchoolType, 'N') || ''}
      `
		}
	}
}