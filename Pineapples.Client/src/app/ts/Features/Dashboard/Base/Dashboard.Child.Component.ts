﻿namespace Pineapples.Dashboards {

	// a plaette definition may be the palette name (e.g. category10)
	// or an array of colours 
	export type PaletteDefinition = string | string[];
	/**
	 * An object that implements some particular view or exploration of
	 * data provided by a dashboard
	 */
	export interface IDashboardChild {
 
    // bindings
    //----------------------------------------------

		/**
		 * a reference to the parent dashboard
		 */
		dashboard: IDashboard;

		/** 
		 *  The options object used in this dashboard
		 */
		options: any;

		/**
		 * One of the children can mark itself as 'selected' ie given prominence in the dashboard
		 * the dashboard notifies all it children of this change of status
		 * selectedChild is the unique component id of the currently selected child
		 */
    selectedChild: string;


		/**
		 * a unique id to identify the instance of the component
		 */
		componentId: string;

		title: string;		
		
    // check if the current component is the 'selected' component
    // these delegate to the relevant members of the dashboard
    isSelected(): boolean;
    isUnselected(): boolean;
    setSelected();
    toggleSelected();

    // reporting
    // 
    // true if there is an assoicated report
    hasReport(): boolean
    // generate the associated report
    runReport(): void

    /**
		 * Called when dashboard binding is initialised. The dashboard 
		 * signals this by setting its 'dashboard' property to itself
		 */
    onDashboardReady(): void; 

    /** 
		 * Function to call when the selected child changes.
		 * The currently selected child has been transmitted via bindings to selectedChild - this is the 
		 * componentId of that child
		 */
    onChildSelected(): void; 

		/**
		 * Called when the options associated to the dashboard have changed
		 * this is implemented with the binding (option-change)
		 * the data is the change description packed by the options object
		 * Note that most clients will not need to access this becuase:
		 * bindings to optionvalues in the html will update anyway e.g.
		 * {{vm.dashboard.options.selectedYear}}
		 * will automatically refresh
		 * As well, a Crossfilter dashboard should respond to the option changes by applying
		 * the appropriate filters to its dimensions. Any clients built using groups on these dimensions 
		 * will respond appropriately. e.g.
		 * <tr ng-repeat="row in vm.group.all()">
		 * will update to reflect the new filters
		 * Charts
		 * --------------
		 * Child objects that build charts do need to regenerate their charts when an option changes.
		 * The base implementation of onOptionChange will do this.
		 * Note that if the child implements $onChanges, it must call super.$onChanges in order
		 * for onOptionChange to be invoked.
		 * 
		 * @param optionchange
		 */
    onOptionChange(optionchange): void;
 
  }

  // Controller for a dashboard child component
  // extends the ReportRunner class to allow the dashboard object to rpint reports
  export class DashboardChild extends Pineapples.Reporting.ReportRunner implements IDashboardChild {
    // injections and constructor
    static $inject = ["$timeout", "$mdDialog", "$state", "Lookups", "reportManager","$http"];
    constructor(
      protected timeout: ng.ITimeoutService
      , mdDialog: ng.material.IDialogService
      , protected $state: ng.ui.IStateService
      , public lookups: Sw.Lookups.LookupService
			, rmgr: Pineapples.Reporting.IReportManagerService
			, public http: ng.IHttpService) {
      super(mdDialog, rmgr);
      this.componentId = this.uniqueId();
    }

		// --- bindings ---

    // binding to the parent dashboard
		// derived class must override this to strongly type it
		dashboard: IDashboard;      

		/**
		 * component id of the currently selected child
		 */
    selectedChild: string;

		/** 
		 *  title for the instance 
		 */
		protected userTitle;				// collected from a binding to title

		/**
		 * Title of the component, which binding may override
		 */
		protected defaultTitle = "Dashboard Component";

		/**
		 * Title for the component, using any value supplied in bindings,
		 * or the default value in the component
		 */
		public get title() {
			return this.userTitle || this.defaultTitle;
		}
		
		// palette for the instance
		protected userPalette: PaletteDefinition;			// this is bound to colors
		protected defaultPalette: PaletteDefinition = "category10";
		public get palette(): PaletteDefinition {
			return this.userPalette || this.defaultPalette;
		}

		// manage highlight - ie selected item in chart or table
		/**
		 * Highlight is usually associated to a selected value in the current options
		 * To set the 'highlight', via clicking on a table row or chart item:
		 * -- respond to the click event to toggle the the relevant option
		 * this.option.toggleSelectedDistrict(districtCode);
		 * -- override highlight get to return
		 * public get highlight() {
		 *	return this.options.selectedDistrict;
		 * }
		 */
		protected _highlight;
		public get highlight() {
			return this._highlight;
		}
		public set highlight(value) {
			this._highlight = value;
		}

		// manage filter - the displayed description of the current filter string
		// based on crossfilter logic, this may omit the dimensions on which the component is based
		protected _filters;
		public get filters() {
			return this._filters;
		}
		public set filters(value) {
			this._filters = value;
		}

		// the options object - passed via a binding
		options: any;

    // path in the jasper report hierarchy to the associated report
    // this can be set in code to provide a default
    // otherwise can be overwritten by binding report-path
    protected defaultReportPath: string

    protected get reportPath(): string {
      return this.defaultReportPath;
    }

    protected set reportPath(newValue) {
      if (newValue) {
        this.defaultReportPath = newValue;
      }
    }


    public $onChanges(changes) {
			if (changes?.dashboard?.currentValue) {
        this.onDashboardReady();
      }
      if (changes.selectedChild) {
        this.onChildSelected();
			}
			if (changes.optionChange?.currentValue) {
				// we don't want the changes object, we want changes.currentValue
				this.onOptionChange(changes.optionChange.currentValue as IOptionChangeData);
			}

    }

		public $onInit() {
		}

    // ------------------------------------
    // reporting
    //-------------------------------------
   
    // true if there is a report
    public hasReport(): boolean {
      return (this.reportPath != null);
    }

    // for example...
    public getReportContext() {
      return {
        surveyYear: this.dashboard.options.selectedYear,
        district: this.dashboard.options.selectedDistrict
      }
    }
    // setParams is noop - construct reportContext as the parameters
    public setParams = () => { }


    // execute the associated report
    public runReport() {
      // first step is to get a Report object from the definition
      this.report = new Pineapples.Reporting.Report();
      this.report.path = this.reportPath;
      this.report.promptForParams = "always";
      this.report.canRenderPdf = true;
      this.report.canRenderExcel = true;
      this.report.format = "PDF";
      this.reportContext = this.getReportContext();
      this.run();
    }


		// called when the dashboard is initialised
    public onDashboardReady() { }

    // called whenever the selectedChild in the group changes
    // often it wont be necessary to do anything in here,
    // because UI effects will be manage by use of vm.isSelected(...)
    // e.g. in ng-show or ng-class
    public onChildSelected() { }

		/**
		 * called when the dashboard has recorded a change in the options
		 * Default behaviour is to refresh any attached chart
		 * @param optionchange describes the option that was changed
		 */
		public onOptionChange(optionchange: IOptionChangeData) {
			this.refreshChart();
		}

		protected touch: number;		// bind to a chart to signal that it needs to be refreshed

		protected refreshChart() {
			this.touch = Date.now();
		}

   /**
    * unique identifier for this instance of the component
    **/
    public componentId: string

    //-----------------------------------------------
    // selected child impementation
    // this delegates to the dashboard
    //-----------------------------------------------
    public isSelected(): boolean {
      return (this.componentId === this.selectedChild);
    }
    public isUnselected(): boolean {
      return (this.selectedChild != this.componentId && this.selectedChild !== "");
    }

    // these methods just pass through to the dashboard
    // but it is convenient to have them here so that markup in the ui can refer to them 
    // directly: e.g. ng-click="vm.toggleSelected()"
    // this will change the value in the dashboard, which will get relayed to every child
    // via the binding, forcing the call to onChildSelected()
    public setSelected() {
      this.dashboard.setSelected(this.componentId);
    }
    public toggleSelected() {
      this.dashboard.toggleSelected(this.componentId);
    }

    /** 
		 *  generate a  random, unique Id for this instance 
		 */
    private uniqueId(): string {
      // https://gist.github.com/gordonbrander/2230317
      // Math.random should be unique because of its seeding algorithm.
      // Convert it to base 36 (numbers + letters), and grab the first 9 characters
      // after the decimal.
      return '_' + Math.random().toString(36).substr(2, 9);
    };
	}

	/**
	 * Bindings supported by dashboard child objects 
	 */
	export const dashboardChildBindings = {
			dashboard: "<",
			options: "<",
			optionChange: "<",

			selectedChild: "<",
			reportPath: "@",

			userTitle: "@headingTitle",
			userPalette: "<colors",
			/**
			 * a cross filter group that suppplies the data used by this group
			 */
			group: "<",
			// allow a dimension to be passed to components that want to make their own group
			dimension: "<"
	};

	export class DashboardChildComponent implements ng.IComponentOptions {
	
		public bindings = dashboardChildBindings;
		public templateUrl: string;
		public controllerAs: string = 'vm';

		public get feature() {
			return "<define feature in DashboardchildComponent>";
		}

		constructor(public controller: any, templateUrl) {
			this.templateUrl = `${this.feature}/dashboard/component/${templateUrl}`;
		}
	}
}