﻿namespace Pineapples.Dashboards {


	export interface IOptionChangeData {
		[optionName: string]: { new: string, old: string }
	}
	

  export interface IDashboard {
    options: any;             // a core options object 
    hub: Sw.IHub;

    // child selection support

   /**
    * Make the specifed child the selectedChild
    * return the selected Child
    **/
    setSelected(childId: string): string;

   /**
    * if the specified child is the selected child, unselect it; otherwise, select it
    * return the selected Child
    **/
    toggleSelected(childId: string): string;

   /**
    * return true if the specified child is the selected child
    **/
    isSelected(childId: string): boolean;

    /**
     * return true if the specified child is Unselected; ie some other child is selected
     **/
    isUnselected(childId: string): boolean;

   /**
    * return the currently selected child, empty strng if no selection
    **/
    readonly selectedChild: string;

  }

  // a dashboard that exposes a crossfilter object for its children to display and manipulate
  export interface ICrossfilterDashboard extends IDashboard{
    // a data table managed by the dashboard
    table: any[];
    // crossfilter object and dimensions
    xf: CrossFilter.CrossFilter<any>;

   /**
    * when data is available, create the required crossfilter dimensions
    **/
    createDimensions(): void;
  }

  /**
   * Base implementation of Dashboard
   */
  export class Dashboard implements IDashboard {


    public options: any;
    public hub: Sw.IHub;

    /**
		 * this self-reference can be the dashboard that is passed to the child;
		 * ie instead of dashboard="vm"
		 * dashboard="vm.dashboard"
		 * The dashboard can withhold setting this until it is 'ready'
		 */ 
		public dashboard;


    // utlity class for cross filters but made available here for access to reflate
    public xFilter: Sw.xFilter.XFilter;  

    //-----------------------------------------------------
    // child Selection Implementation
    //-----------------------------------------------------
    private _selectedChild: string = "";

    public isSelected(childId: string): boolean {
      return (childId === this._selectedChild);
    }

    public isUnselected(childId: string): boolean {
      return (this._selectedChild != childId && this._selectedChild !== "");
    }

    public setSelected(childId: string): string {
			if (childId == "" && this.selectedChild != "") {
				// we are toggling the selected child off
				// scroll to its rest location
				this.scrollTo(this.selectedChild);
			}
			this._selectedChild = childId;
			if (childId != "") {
				this.scrollTo(childId);
			}
      return this._selectedChild;
    }

		private scrollTo(id) {
			this.$timeout( () => {
					//this.$location.hash("#" + id);
				this.$anchorScroll(id);
				});
		}

    public toggleSelected = (childId: string) => {
      if (childId === this._selectedChild) {
					childId = "";
      } 
      return this.setSelected(childId);
    }

    public get selectedChild() {
      return this._selectedChild;
    }

    //-------------------------------------------------------------
    // Constructor and injections
    //------------------------------------------------------------

		static $inject = ["$rootScope", "$mdDialog", "$state", "$http", "$q",
		"$timeout", "$anchorScroll",  "Lookups", "reflator"];
    constructor(rootScope: Sw.IRootScopeEx
      , public mdDialog: ng.material.IDialogService
      , protected $state: ng.ui.IStateService
      , public http: ng.IHttpService
			, public $q: ng.IQService
			, public $timeout: ng.ITimeoutService
			, public $anchorScroll:ng.IAnchorScrollService
			, public lookups: Sw.Lookups.LookupService
			, public reflator: Sw.Api.IReflator
        ) {
      this.hub = rootScope.hub;
      this.options = new coreOptions(lookups, this.hub);
      this.xFilter = new Sw.xFilter.XFilter();

      this.options.subscribe(this.onOptionChange.bind(this));
    }

		/**
		 * Bound to the child to notify of a change in the dashboard options
		 */
    public optionChange: IOptionChangeData;
    // this is the data relayed from the options object if something has changed

		public onOptionChange(data: IOptionChangeData, sender) {
      this.optionChange = data;
    }

    /** Utility method to return a function that will "toggle" the value of a nominated option
     * @option the name of the option to manage e.g. selectedDistrict
     * In the generated function, newValue is the (scalar) value to assign to this property
     * earlier usages had a key/value object.
     * See the comments in DsChart on binding options for onChartClick 
    **/
    public optionToggler = _.memoize(
      (option: string) => (newValue) => {
        if (!newValue) { return; }
        this.options[option] = (newValue === this.options[option])? null : newValue;
      }
		);    

		//////////////////////////////////////////////////////////
		// Life Cycle events
		//////////////////////////////////////////////////////////

		public $onDestroy() {
			this.dashboard = undefined;
		}
  }

  export class CrossfilterDashboard extends Dashboard implements ICrossfilterDashboard {
    public table: any[];      // the raw data 

    // crossfilter object and dimensions
		// specific dashboards may wish to redfine this as strongly typed
    public xf: CrossFilter.CrossFilter<any>;
     
    // the dashboard can supply some precreated dimensions from its crossfilter
    // these should be declared as public members, and constructed when data is available
    // e.g.
    //public dimSurveyYear: CrossFilter.Dimension<IxfData, number>;


    public $onChanges(changes) {
      if (changes.table && changes.table.currentValue) {
        // data has been retrieved by the ui-router as a Resolve
        // and passed to the dashboard as a binding - process it into a crossfilter
        this.makeCrossfilter();
      }
    }


    public $onInit() {
      // if we get to onInit, without having the data injected, 
      // then assume we go fetch it
      if (!this.table) {
        this.fetchData().then((table) => {
          this.table = table;
          this.makeCrossfilter();
        })
          .catch((error) => {
          })
          .finally(() => {
          });
      }
    }

    // virtual Members

		/**
		 * the Dashboard may get its own data, or expect that data to be injected by the routing
		 * in either case it must call makeCrossfilter to generate the crossfilter object
		 * from the deflated table
		 * The dashboard sets its dashboard property when everything is ready
		 */
    protected makeCrossfilter() {
      
      // create the crossfilter from the warehouse table
      this.xf = crossfilter(this.table);
			this.createDimensions();
			// signal open for business 
      this.dashboard = this;			
    }

    /** 
		 *  create the dimensions and groups that child components may use
		 */
    public createDimensions() {
    }


		public fetchData(): ng.IPromise<any[]> {
      return null;
		}

		/// some useful utilities for working with crossfilters
		///
		/// return an array containing a single element from the key-value pairs
		/// use this to get a single value from a crossfilter group.all()
		public byKey(data: Sw.Charts.KeyValuePair<any, any>[], key: any) {
			return [_.find(data, r => r.key == key)];
		}
  }
}
