﻿namespace Pineapples.TeacherExams {

	/**
	 * Object returned from upload process
	 */
	export type TeacherExamData = {
		ScorePercentage: number,
		texcFamilyName: string,
		texcGiven: string,
		texcID: number,
		texCode: string,
		texDate: number,
		texName: string,
	}

	/**
	 * The object returned from process has 3 properties each of whcih is a recordset
	 * the data is ExamContextData used to build a chart
	 */
	type AuditRecord = {
		clock: Date,
		message: string,
		elapsed: number,
		cumulative: number,
	}

	type processResponseData = {
		validationErrors: any[],
		validationWarnings: any[],
		data: TeacherExamData[]
		audit: AuditRecord[];
	}

	class teacherexamProcessResponse {

		constructor(public responseData: processResponseData) {
			let start = this.responseData.audit[0].clock.getTime();
			this.responseData.audit.forEach((audit, i, arr) => {
				if (i > 0) {
					audit.elapsed = audit.clock.getTime() - arr[i - 1].clock.getTime();
					audit.cumulative = audit.clock.getTime() - start;
				}
			});
		}
		public get hasValidations(): boolean {
			return (this?.responseData?.validationErrors.length > 0);
		}
		public get validations() {
			return this?.responseData?.validationErrors;
		}

		public get hasWarnings(): boolean {
			return (this?.responseData?.validationWarnings.length > 0);
		}
		public get warnings() {
			return this?.responseData?.validationWarnings;
		}
		//public get hasData(): boolean {
		//  return (this?.responseData?.data.length > 0);
		//}
		//public get data() {
		//  return this?.responseData?.data;
		//}
		public get hasAudit(): boolean {
			return (this?.responseData?.audit.length > 0);
		}
		public get audit() {
			return this?.responseData?.audit;
		}

	}

	class Controller extends Pineapples.Documents.DocumentUploader {


		public allowUpload: boolean

		public processResponseData: teacherexamProcessResponse;
		public uploadResponseData: any;
		public isWaiting: boolean;

		public summary: any;
		//private teacherexamXf: TeacherExamXf;

		static $inject = ["identity", "documentsAPI", "FileUploader", "$mdDialog"
			, "$state", "Hub", "$rootScope", "$http", "documentManager", "examsAPI"];
		constructor(public identity: Sw.Auth.IIdentity, docApi: Pineapples.Documents.IDocumentApi, FileUploader
			, mdDialog: ng.material.IDialogService
			, state: ng.ui.IStateService
			, public Hub: ngSignalr.HubFactory
			, public rootScope: ng.IRootScopeService
			, public http: ng.IHttpService
			, renderer: Pineapples.Documents.DocumentManager
			, private api: Pineapples.Api.ITeacherExamsApi
		) {
			super(identity, docApi, FileUploader, mdDialog, state, renderer);
			this.uploader.url = "api/teacherexams/upload";

			this.uploader.filters.push({
				name: "openxml", fn: (file) => (this.renderer.isXml(file.name) || this.renderer.isExcelOpenXml(file.name)),
				msg: "The Exam summary file must be an Xml document with extension .xml, or an Excel workbook (*.xlsx/*.xlsm)"
			});

		};

		//-----------------------------------------------------------------------
		// Table support from Crossfilters
		//
		// these are reusable - to do - find a better home for them
		//
		public filterRecordType(row: any, recordtype: string) {
			return row.recordType == recordtype;
		}

		protected onAfterAddingFile(fileItem: angularFileUpload.FileItem) {
			super.onAfterAddingFile(fileItem);
			// if we have started to process a new file, clear all the state
			this.uploadResponseData = null;
			this.summary = null;
		};

		protected onSuccessItem(fileItem: angularFileUpload.FileItem, response, status, headers) {
			this.uploadResponseData = response;    // note slightly different format - not an array item
			this.processResponseData = null;    // kill existing results now
			//this.teacherexamXf = null;
			let fileId = response.id;
			// show the dialog for confirmation to continue;
			this._confirmationDialog(response).then(() => {
				this.isWaiting = true;
				this.http.get<Api.IDataResult<processResponseData>>("api/teacherexams/process/" + fileId).then((processResponse) => {
					this.processResponseData = new teacherexamProcessResponse(processResponse.data.ResultSet);
					// make the crossfilter from the exam detailed dataset
					//if (this.processResponseData.hasData) {

					//  this.teacherexamXf = new TeacherExamXf(this.processResponseData.data);
					//  this.teacherexamXf.dimensions.recordType.filter(TeacherExamContextRecordType.exam);
					//  this.teacherexamXf.dimensions.aggregationKey.filter(TeacherExamContextAggregation.nation);
					//}

					this.uploader.queue.forEach((item) => {
						item.remove();
					})
					this.isWaiting = false;
				}, (errorResponse: ng.IHttpPromiseCallbackArg<Sw.Api.ErrorInfo>) => {
					// error return from process
					this.isWaiting = false;
					let title = "Error Processing File";
					let ariaLabel = "ProcessError";
					let okLabel = "Close";
					this.mdDialog.show(
						this.mdDialog.alert()
							.clickOutsideToClose(true)
							.title(title)
							.textContent(errorResponse.data.message)
							.ariaLabel(ariaLabel)
							.multiple(true)
							.ok(okLabel)
					)
				})
			}, () => {
				// error returned from confirmation dialog - cancel the upload
				this.http.get("api/teacherexams/cancelprocess/" + fileId);
				this.uploadResponseData = null;
				this.summary = null;
				this.uploader.queue.forEach((item) => {
					item.remove();
				});
			});

		};

		protected onBeforeUploadItem(item) {
			super.onBeforeUploadItem(item);
		}


		private _confirmationDialog(info) {
			let options: ng.material.IDialogOptions = {
				locals: { info: info },
				controller: confirmController,
				controllerAs: "vm",
				bindToController: true,
				// no clickutsideToClose here - get an explicit response
				templateUrl: "teacherexam/uploadconfirm"

			}
			return this.mdDialog.show(options);
		}
		// life cycyle hooks
		public $onChanges(changes) {
		}

		public $onInit() {
		}
	}

	class ComponentOptions implements ng.IComponentOptions {
		public bindings: any;
		public controller: any;
		public controllerAs: string;
		public templateUrl: string;

		constructor() {
			this.bindings = {
			}; ``
			this.controller = Controller;
			this.controllerAs = "vm";
			this.templateUrl = "teacherexam/Upload";
		}
	}
	angular
		.module("pineapples")
		.component("teacherexamUploadComponent", new ComponentOptions());

	class confirmController extends Sw.Component.MdDialogController {
		public info: any;
	}
}