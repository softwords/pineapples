﻿module Pineapples.Api {

	//type TeacherExamContextData = TeacherExams.TeacherExamContextData;
	//type TeacherExamXf = TeacherExams.TeacherExamXf;

	export interface ITeacherExamsApi {

		/**
		 * Read a single teacherexam
		 * @param id
		 */
		read(id: number): ng.IPromise<any>;
		/**
		 * create a new empty teacherexam
		 * unlkely to be used, since teacher exams are imported from an external system
		 */
		newTeacherExam(): ng.IPromise<TeacherExams.TeacherExam>;

		filterPaged(fltr): ng.IPromise<any>;

		///**
		// * Return 'contextdata', showing the school, its district and nation
		// * @param schoolNo
		// * @param examCode
		// * @param examYear
		// */
		//contextData(schoolNo: string, examCode: string, examYear: number): ng.IPromise<TeacherExamContextData[]>;

		/**
		 * Return an ExamXf object, holding various crosstab data for a contextData recordset
		 * @param schoolNo
		 * @param examCode
		 * @param examYear
		 */
		//contextDataXf(schoolNo: string, examCode: string, examYear: number): ng.IPromise<ExamXf>;
	}
	const ENTITY = "teacherexams";
	class apiService implements ITeacherExamsApi {

		static $inject = ["$q", "$http", "Restangular", "xFilter", "reflator"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService
			, public xFilter: Sw.xFilter.IXFilter
			, public reflator: Sw.Api.IReflator) {
		}
		public read(id) {
			return this.restangular.one(ENTITY, id).get();
		}

		public newTeacherExam(): ng.IPromise<TeacherExams.TeacherExam> {
			//return this.restangular.one(ENTITY, 0).get();
			let e = new Pineapples.TeacherExams.TeacherExam(this.restangular, {
				texID: null, TeacherExamFrame: null
			});
			this.restangular.restangularizeElement(null, e, "teacherexams");
			return this.$q.resolve<TeacherExams.TeacherExam>(e);
		}

		public filterPaged(fltr) {
			return this.http.post(`api/${ENTITY}/collection/filter`, fltr)
				.then(response => (response.data));
		}

		//public contextData(schoolNo: string, examCode: string, examYear: number): ng.IPromise<TeacherExamContextData[]> {
		//	let params = {
		//		schoolNo: schoolNo,
		//		examCode: examCode,
		//		examYear: examYear
		//	}
		//	let querystring = Object.keys(params).map(key => key + '=' + params[key]).join('&');
		//	return this.reflator.get<TeacherExamContextData[]>(`api/${ENTITY}/contextdata?${querystring}`)
		//		.then(response => (response.data));

		//}

		//public contextDataXf(schoolNo: string, examCode: string, examYear: number) {
		//	return this.contextData(schoolNo, examCode, examYear)
		//		.then(data => new Exams.ExamXf(data));
		//}
	}

	angular.module("pineapplesAPI")
		.service("teacherexamsAPI", apiService);
}