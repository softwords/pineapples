﻿namespace Pineapples.TeacherExams {

	angular
		.module("pineapples")
		.component("teacherexamRenderFrame", new Sw.Component.RenderFrameComponent("teacherexam"));
}