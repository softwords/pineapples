﻿namespace Pineapples.TeacherExams {

	let viewDefaults = {
		columnSet: 0,
		columnDefs: [
			{
				field: 'texID',
				name: 'Teacher Exam ID',
				displayName: 'Teacher Exam ID',
				editable: false,
				pinnedLeft: true,
				width: 70,
				cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm")
			},
			{
				field: 'texCode',
				name: 'Teacher Exam Code',
				displayName: 'Teacher Exam Name',
				pinnedLeft: true,
				width: 240,
				lookup: 'teacherExamTypes',
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'texYear',
				name: 'Teacher Exam Year',
				displayName: 'Teacher Exam Year',
				pinnedLeft: true,
				width: 100,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'texUser',
				name: 'Uploaded By',
				displayName: 'Uploaded By',
				pinnedLeft: true,
				width: 200,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'texDate',
				name: 'Uploaded Date',
				displayName: 'Uploaded Date',
				cellFilter: "date:'d-MMM-yyyy'",
				pinnedLeft: true,
				width: 130,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
		]
	};


	let modes = [
		{
			key: "Results",
			gridOptions: {
				columnDefs: [
					{
						field: 'texCandidates',
						name: 'Candidates',
						displayName: 'Candidates',
						cellClass: 'gdAlignRight',
					},
					{
						field: 'texMarks',
						name: 'Marks',
						displayName: 'Marks',
						cellClass: 'gdAlignRight',
					},
					{
						field: 'docID',
						name: 'TeacherExamSource',
						displayName: 'Data',
						width: 70,
						cellClass: 'gdAlignRight',
						cellTemplate: `
							<div class="ui-grid-cell-contents gdAction" >
								<span permission permission-only="'ExamOps'" >
									<document-image document = "row.entity.docID" height = "10" > </document-image>
								</span>
							</div>`
					},
				]
			}
		},
	];

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};

	angular
		.module('pineapples')
		.run(['TeacherExamFilter', pushModes]);
}
