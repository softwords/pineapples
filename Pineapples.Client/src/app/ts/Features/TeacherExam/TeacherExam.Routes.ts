﻿// TeacherExams Routes
namespace Pineappples.TeacherExams {

	let routes = function ($stateProvider) {
		var featurename = 'TeacherExams';
		var filtername = 'TeacherExamFilter';
		var templatepath = "teacherexam";
		var tableOptions = "teacherexamFieldOptions";
		var url = "teacherexams";
		var usersettings = null;
		//var mapview = 'TeacherExamMapView';
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath);

		// default 'api' in this feature is examsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "teacherexamsAPI";

		state.data = state.data || {};
		state.data["frameTitle"] = "TeacherExams";
		state.data["icon"] = "assignment_turned_in";
		state.data["permissions"] = {
			only: 'ExamRead'
		};
		let basestate = "site.teacherexams";
		$stateProvider.state(basestate, state);

		// List State
		state = Sw.Utils.RouteHelper.frameListState("teacherexam", "texID");
		let statename = `${basestate}.list`;
		$stateProvider.state(statename, state);

		// Reload state
		state = {
			url: '^/teacherexams/-',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.removeAll();
				$state.reload();
			}]
		};
		statename = "site.teacherexams.reload";
		$stateProvider.state(statename, state);

		// chart, table and map
		Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
		Sw.Utils.RouteHelper.addMapState($stateProvider, featurename); // , mapview

		// new - state with a custom url route
		state = {
			url: "^/teacherexams/new",
			params: { id: null, columnField: null, rowData: {} },
			data: {
				permissions: {
					only: 'ExamWrite'
				}
			},
			views: {
				"actionpane@site.teacherexams": {
					component: "componentTeacherExam"
				}
			},
			resolve: {
				model: ['teacherexamsAPI', '$stateParams', function (api, $stateParams) {
					return api.new();
				}]
			}
		};
		$stateProvider.state("site.teacherexams.list.new", state);

		state = {
			url: "/reports",
			data: {
				permissions: {
					only: 'ExamRead'
				}
			},
			views: {
				"@": "reportPage"       // note this even more shorthand syntax for a component based view
			},
			resolve: {
				folder: () => "TeacherExams",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
				promptForParams: () => "always",
				pageTitle: Sw.Utils.RouteHelper.simpleTitle("Reports")
			}
		}
		$stateProvider.state("site.teacherexams.reports", state);

		// state for a high level exams dashboard
		//state = {
		//	url: "/dashboard",
		//	data: {
		//		permissions: {
		//			only: 'ExamRead'
		//		},
		//		rendermode: "Dashboard"
		//	},
		//	views: {
		//		"renderarea": {
		//			component: "teacherexamsDashboard"
		//		},
		//		"searcher": "teacherexamOptionsEditor"
		//	},
		//	resolve: {
		//		options: ["Lookups", "$rootScope", (lookups: Sw.Lookups.LookupService, scope: Sw.IRootScopeEx) => {
		//			return new Pineapples.Dashboards.teacherexamOptions(lookups, scope.hub);
		//		}],
		//		table: ['reflator', '$q', (reflator: Sw.Api.IReflator, q: ng.IQService) => {
		//			// overall level 
		//			let p = reflator.get("api/warehouse/teacherexams/tablex")
		//				.then(response => (<any>response.data));
		//			// legacy endpoint now removed?
		//			//let p2 = reflator.get("api/warehouse/examsdistrictresults").then(response => (<any>response.data));
		//			// use $q.all to return both sets in an array
		//			return q.all([p]);
		//		}],
		//		pageTitle: Sw.Utils.RouteHelper.simpleTitle("Dashboard")
		//	}
		//}
		//$stateProvider.state("site.teacherexams.dashboard", state);

		// item state
		//$resolve is intriduced in ui-route 0.3.1
		// it allows bindings from the resolve directly into the template
		// injections are replaced with bindings when using a component like this
		// the component definition takes care of its required injections - and its controller
		state = {
			url: "^/teacherexams/{id}",
			data: {
				permissions: {
					only: 'ExamAdmin'
				}
			},
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.teacherexams": {
					component: "componentTeacherExam"
				}
			},
			resolve: {
				model: ['teacherexamsAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}],
				titleId: ["model", (model) => `${model.texCode} ${model.texYear}`],
				pageTitle: Sw.Utils.RouteHelper.indirectTitle()
			}
		};
		$stateProvider.state("site.teacherexams.list.item", state);

		state = {
			url: "^/teacherexams/upload",
			data: {
				permissions: {
					only: 'ExamWrite'
				}
			},
			views: {
				"@": {
					component: "teacherexamUploadComponent"
				}
			},
			resolve: {
				pageTitle: Sw.Utils.RouteHelper.simpleTitle("Upload")
			}
		};
		$stateProvider.state("site.teacherexams.upload", state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}