﻿namespace Pineapples.TeacherExams {

	/**
	 * Results data of a particular exam
	 */
	export type TeacherExamResultsData = {
		ScorePercentage: number,
		texcFamilyName: string,
		texcGiven: string,
		texcID: number,
		texCode: string,
		texDate: number,
		texName: string,
		textYear: number,
	}

	export class TeacherExam extends Pineapples.Api.Editable implements Sw.Api.IEditable {

		static $inject = ["Restangular", "data"];
		constructor(restangular: restangular.IService, teacherexamData) {
			super();
			this._transform(teacherexamData);
			angular.extend(this, teacherexamData);
			// bit of a kludge here: restangular will add the property 'route' (='teacherexam')
			// when the item is restangularized, but that hasn;t happened yet
			// so set the route manually here so it is bound into the collection as well
			this.route = "teacherexams";
			// this way we get url in the school types collection like
			// api/census/2021/schooltypes/PS
			// otherwise its api/undefined/2021/schooltypes/PS
			//restangular.restangularizeCollection(this, this.TeacherExamFrame, "teacherexamframes");
		}

		// create static method returns the object constructed from the resultset object from the server

		public static create(resultSet: any, injector: ng.auto.IInjectorService) {
			let app = injector.instantiate(TeacherExam, { data: resultSet });
			return app;
		};

		public texID: number;
		public texCode: string;
		public texYear: number;
		public Results: TeacherExamResultsData[];
		//public ExamFrame: any[];			// the standards, benchmarks, indicators
		//public ItemAnalysis: any[];
		// IEditable implementation
		public _name() {
			return this.texCode;
		}
		public _type() {
			return "teacherexam";
		}
		public _id() {
			return this.texID
		}

		protected route;

		public _transform(newData) {
			// convert these incoming data values
			// transformDates is deprecated: http interceptor now converts dates on load
			//this._transformDates(newData, ["exDate"]);
			return super._transform(newData);
		}
	}
}