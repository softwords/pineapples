﻿namespace Pineapples.TeacherExams {

	interface IBindings {
		model: TeacherExam;
	}

	class Controller extends Sw.Component.ComponentEditController implements IBindings {
		public model: TeacherExam;
		//public teacherexamXf: TeacherExamXf;       // cross filter of the exam results

		static $inject = ["ApiUi", "teacherexamsAPI"];
		constructor(apiui: Sw.Api.IApiUi, private api: Api.ITeacherExamsApi) {
			super(apiui);
		}

		public $onChanges(changes) {
			super.$onChanges(changes);
			//   if (changes.model) {
			//     this.teacherexamXf = new TeacherExamXf(this.model.Results);
			//}
		}

		protected getNew = () => {
			return this.api.newTeacherExam();
		}
	}

	angular
		.module("pineapples")
		.component("componentTeacherExam", new Sw.Component.ItemComponentOptions("teacherexam", Controller));
}