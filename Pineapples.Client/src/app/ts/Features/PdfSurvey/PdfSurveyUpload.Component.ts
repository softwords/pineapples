﻿namespace Pineapples.PdfSurvey {

  interface IPdfSurveyProcessClient {
    connectFile(fileId: string);
    disconnectFile(fileId: string);
  }
  class PdfSurveyProgress { 
    public schools: string;
    public students: string;
    public staff: string;
    public wash: string;
  }

  /**
   * the response on uload identifies the data, and also validates that school nos are OK
   * across each sheet. Bad or missing schoool numbers are returned in the vaidations field
   */
  class PdfSurveyUploadResponse {

    constructor(responseData: any) {
      angular.extend(this, responseData);
    }

    public validations: any[];
    public get hasValidations(): boolean {
      return (this.validations && this.validations.length > 0);
    }
  }

  /**
   * the first recordset return is the validations array
   * if this is empty, then the upload will take place and the
   * pupil table is delivered in the last 4 recordsets
   * the second recordset is Warnings related to conflicts of National ID number.
   * These will not prevent the upload if present.
   * see the stored proc koboLoadStudents
   */
  class PdfSurveyProcessResponse {

    auditlog: any[];
    summary: any
    constructor(private responseData: any[]) {
      this.auditlog = responseData[0];
      this.summary = responseData[1][0];
    }

  }

  class Controller extends Pineapples.Documents.DocumentUploader {

    public doc: any;      // the document record representing the photo

    public model: any;      // teacher link, bound from the ui

    public imageHeight: number;

    public document: any;     // this is the document object
    public allowUpload: boolean;

    public docPath: string;

    public isProcessing: boolean;
    public uploadResponseData: PdfSurveyUploadResponse;
    public processResponseData: PdfSurveyProcessResponse;

    public progressData: PdfSurveyProgress;
    public enrolmentSummary: Pineapples.PupilTable;
    public repeaterSummary: Pineapples.PupilTable;
    public psaSummary: Pineapples.PupilTable;


    static $inject = ["identity", "documentsAPI", "FileUploader", "$mdDialog"
      , "$state", "$rootScope", "$http", "documentManager"];
    constructor(public identity: Sw.Auth.IIdentity, docApi: Pineapples.Documents.IDocumentApi
      , FileUploader: angularFileUpload.UploaderContructor
			, mdDialog: ng.material.IDialogService
			, state: ng.ui.IStateService
      , public rootScope: ng.IRootScopeService
			, public http: ng.IHttpService
			, renderer: Pineapples.Documents.DocumentManager) {
      super(identity, docApi, FileUploader, mdDialog, state, renderer);
      this.uploader.url = "api/pdfsurvey/upload";
      this.uploader.filters.push({
        name: "pdf", fn: (file) => this.renderer.isPdfXfdfZip(file.name),
        msg: "The Pdf Survey must be a Pdf document with extension .pdf or Pdf data in .xfdf format. The Pdf or Xfdf file may be zipped (.zip file)"
      });
    };


    protected onAfterAddingFile(fileItem: angularFileUpload.FileItem) {
      super.onAfterAddingFile(fileItem);
      // if we have started to process a new file, clear all the state
      this.uploadResponseData = null;
      this.progressData = null;
      this.processResponseData = null;
      this.isProcessing = false; // probably unnecessary, anticipating a missed change of state through an error?

    };

    protected onSuccessItem(fileItem: angularFileUpload.FileItem, response, status , headers) {
      this.uploadResponseData = new PdfSurveyUploadResponse(response);    // note slightly different format - not an array item
       let fileId = response.id;
      // show the dislog for confirmation to continue;
      this._confirmationDialog(this.uploadResponseData).then(() => {
        // connect to signalR
       
        this.isProcessing = true;

        
        this.http.get("api/pdfsurvey/process/" + fileId).then((processResponse) => {
          console.log(processResponse);
          this.isProcessing = false;
          this.processResponseData = new PdfSurveyProcessResponse(<any[]>(<any>processResponse.data).ResultSet);
          this.uploader.queue.forEach((item) => {
            item.remove();
          });
        },
          // an error response has come back from the processing
          this.processError
        );
      }, () => {
        // cancel the upload from the confirm dialog
        // you may not be given the choice to continue if there are validation errors
        this.http.get("api/pdfsurvey/cancelprocess/" + fileId);
        this.progressData = null;
        this.enrolmentSummary = null;
        this.repeaterSummary = null;
        this.psaSummary = null;
        this.isProcessing = false;
        this.uploader.queue.forEach((item) => {
          item.remove();
        });
      });
   
    };

    protected onBeforeUploadItem(item) {
      super.onBeforeUploadItem(item);
    }

    private _confirmationDialog(info) {
      let options: ng.material.IDialogOptions = {
        locals: { info: info },
        controller: confirmController,
        controllerAs: "vm",
        bindToController: true,
        templateUrl: "pdfsurvey/uploadconfirm"

      }
      return this.mdDialog.show(options);
    }
    // life cycyle hooks
    public $onChanges(changes) {
    }

    public $onInit() {
    }

    // promise error handlers
    // this is a callback - so use lambda format to bind to this
    // (otherwise, this is 'Window' )
    private processError = (errorResponse) => {
      let title = "Error Processing File";
      let msg = errorResponse.data.Message;
      let ariaLabel = "Processing error";
      this.mdDialog.show(
        this.mdDialog.alert()
          .clickOutsideToClose(true)
          .title(title)
          .textContent(msg)
          .ariaLabel(ariaLabel)
          .multiple(true)
          .ok('Close')
      );
    }
  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "pdfsurvey/Upload";
    }
  }
  angular
    .module("pineapples")
    .component("pdfSurveyUploadComponent", new ComponentOptions());

  class confirmController extends Sw.Component.MdDialogController {
    public info: PdfSurveyUploadResponse;
  }
}