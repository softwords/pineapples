﻿// PdfSurvey Routes
namespace Pineappples.PdfSurvey {

	let routes = function ($stateProvider) {

		let state: ng.ui.IState;
		let statename: string;

		// base pdfsurvey state
		state = {
			url: "^/pdfsurvey",
			data: {
				permissions: {
					only: "SurveyOps"
				}
			},
			abstract: true,
			resolve: {
				featureTitle: Sw.Utils.RouteHelper.featureTitle("Pdf Survey")
			}
		};
		$stateProvider.state("site.pdfsurvey", state);

		// upload state
		state = {
			url: "^/pdfsurvey/upload",

			views: {
				"@": {
					component: "pdfSurveyUploadComponent"
				}
			},
			resolve: {
				pageTitle: Sw.Utils.RouteHelper.simpleTitle("Upload")
			}
		};
		$stateProvider.state("site.pdfsurvey.upload", state);

		// -- allow view of an upload inspection from within the uploader
		// -- this state is copied from schoolinspections.list.item
		// ----------------- item state
		state = {
			url: "^/pdfsurvey/upload/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.pdfsurvey.upload": {
					component: "componentSchool"
				}
			},
			resolve: {
				model: ['schoolsAPI', '$stateParams', function (api, $stateParams) {
					return api.read($stateParams.id);
				}],
				pageTitle: ['model', 'rootPageTitle', (model, pageTitle) => `${pageTitle} * Upload School: ${model.schName}`]

			}
		};
		$stateProvider.state("site.pdfsurvey.upload.school", state);

		// reload state for testing
		state = {
			url: '^/pdfsurvey/reload',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.remove("pdfsurvey/upload");
				$state.go("site.pdfsurvey.upload");
			}]
		};
		statename = "site.pdfsurvey.reload";
		$stateProvider.state(statename, state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}