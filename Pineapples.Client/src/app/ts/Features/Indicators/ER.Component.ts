﻿namespace Pineapples.Indicators {
	/**
	 * Component to support drill downs into GER, NER
	 * 
	 */
	class Controller {

		static $inject = ["Lookups", "xFilter", "$state","xfHelper"]
		constructor(public lookups: Pineapples.Lookups.LookupService
			, public xFilter: Sw.xFilter.XFilter, private $state: ng.ui.IStateService
			, private xfHelper: xfHelper) {

		}

		// the variables are the essence of the calculation of NER
		private indicatorCalc: Pineapples.Indicators.IndicatorCalc;

		private _surveyYear: number;
		public get surveyYear() {
			return this._surveyYear;
		}
		public set surveyYear(newValue) {
			this._surveyYear = newValue;
		}

		public edLevelCode: string;
		public districtCode: string;
		public tables: any[];
		public previousState: any;

		public yearsInEdLevel: number;

		public yearsOfEd: number[];
		public minYoE: number;
		public maxYoE: number;

		public officialAges: number[];
		public minOfficialAge: number;
		public maxOfficialAge: number;
		public isOfficialAge = (age) => ((age >= this.minOfficialAge && age <= this.maxOfficialAge) || age === null);

		public unknownAgeTotal: number;

		public kvPop: Sw.Charts.KeyValueCollection;
		public kvPopChart: Sw.Charts.AnnotatedKeyValueCollection;

		public enrol: number[];
		public nEnrol: number[];

		public popT: any;

		public enrolT: any;
		public nEnrolT: any;

		public edLevelClasses: string[];			// the classes in the education level


		public grpAgeA: CrossFilter.Group<any, any, any>;
		public grpTotalA: CrossFilter.Group<any, any, any>;
		public grpTotalOA: CrossFilter.Group<any, any, any>;			// official age totals

		public chartData: Sw.Charts.AnnotatedGroup;
		public touch;
		public model;
		//to do: organise the school drilldown in this class to use XfEnrolment; as per IR.Component
		// interactivity
		public selectedAge;
		public selectedGrade;

		public col_stripe = (col, index) => index % 3 == 1 ? "stripe" : "";
		public row_pairstripe = (row, col, index) => index % 3 == 1 ? "stripe" : "";

		public $onChanges(changes) {
			if (changes.tables && changes.tables.currentValue) {
				// data has been retrieved by the ui-router as a Resolve
				// and passed to the dashboard as a binding - process it into a crossfilter
				// create the crossfilter from the warehouse table
				this.createDimensions();
			}

		}

		public createDimensions() {

			let classLevelsForEdLevel = (edLevelCode) => {
				return (classLevelCode) =>
					this.lookups.byCode("levels", classLevelCode, "L") == edLevelCode;
			}

			// establish the class levels that belong to the selected Ed Level
			this.edLevelClasses = this.lookups.edLevelClasses(this.edLevelCode);

			// grouping by age and class, for nation - used for main table on er page
			//use XfEnrolment class here:

			let xfE = this.xfHelper.createXfEnrolment(this.tables[0]);
			let xf = xfE.xf;
			// only a single year - permanently filtered
			xfE.xf.dimension(d => d.SurveyYear).filter(this.surveyYear);
			// only primary classes
			xf.dimension(d=>d.ClassLevel).filter(classLevelsForEdLevel(this.edLevelCode));
			this.grpAgeA = xfE.AgeClassGroup();

			// get a total dimensionn - this will respect the filterings already applied
			let dimTotalA = xf.dimension(d => "Total");

			this.grpTotalA = this.xFilter.xReduceGrouped(dimTotalA, "ClassLevel",
				this.xFilter.getFlattenedGenderAccessor("GenderCode", ["Enrol","Rep"]),
				(row) => "Total Enrol");

			// filter for official age totals by class level
			let officialAgeTot = (d) => {
				if (this.isOfficialAge(d.Age)) {
					return {
						M: {
							Enrol: d.GenderCode == "M" ? d.Enrol : null,
							Rep: d.GenderCode == "M" ? d.Rep : null
						},
						F: {
							Enrol: d.GenderCode == "F" ? d.Enrol : null,
							Rep: d.GenderCode == "F" ? d.Rep : null
						},
						T: {
							Enrol: d.Enrol,
							Rep: d.Rep
						}
					};
				}
				return {
					M: {
						Enrol: null,
						Rep:  null
					},
					F: {
						Enrol: null,
						Rep: null
					},
					T: {
						Enrol: null,
						Rep: null
					}
				};

			};

			this.grpTotalOA = this.xFilter.xReduceGrouped(dimTotalA, "ClassLevel",
				officialAgeTot, (row) => "Official Age Enrol");

			let r = this.xFilter.find(this.grpAgeA, { key: null });
			this.unknownAgeTotal = r?.Tot.T;



			this.chartData = {
				group: this.grpAgeA,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value?.Tot?.[col]?.Enrol
				// to do dynamic on column click?
				// valueAccessor: (kv, col) => kv.value?.[this.selectedGrade]?.[col]
			}
	
		}

		public $onInit() {


			let ix: IndicatorCalc = this.indicatorCalc;

			// class level and age ranges for the Education Level - from the 'vermpaf'
			this.minYoE = this.indicatorCalc.edLevelFirstYoE(this.surveyYear, this.edLevelCode);
			this.maxYoE = this.indicatorCalc.edLevelLastYoE(this.surveyYear, this.edLevelCode);
			this.minOfficialAge = this.indicatorCalc.edLevelStartAge(this.surveyYear, this.edLevelCode);
			this.maxOfficialAge = this.minOfficialAge + (this.maxYoE - this.minYoE);

			// enrolment totals for the education level - from the vermpaf - 
			// can crosscheck against grpTotal, grpTotalA
			this.enrolT = {
				M: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "M"),
				F: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "F"),
				T: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "")
			}
			// official age (net) enrolments from vermpaf - check against grpTotalOA
			this.nEnrolT = {
				M: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "M"),
				F: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "F"),
				T: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "")
			}
			// number of years in the education level is sourced from the vermpaf
			this.yearsInEdLevel = ix.edLevelHeader(this.surveyYear, this.edLevelCode, "numYears")

			this.kvPop = [];

			// make an array of these
			let e: Array<any> = [];

			let p: Array<any> = [];
			let dt: Array<any> = [];
			for (let i = this.minYoE; i <= this.maxYoE; i++) {
				let d = {
					key: i,
					value: { enrol: null, pop: null, nEnrol: null }
				}
				let enrol: any = {};

				enrol.T = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "");
				enrol.M = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "M");
				enrol.F = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "F");
				d.value.enrol = enrol;

				let pop: any = {};
				pop.age = this.minOfficialAge + i - this.minYoE;
				pop.T = this.indicatorCalc.yoePop(this.surveyYear, i, "");
				pop.M = this.indicatorCalc.yoePop(this.surveyYear, i, "M");
				pop.F = this.indicatorCalc.yoePop(this.surveyYear, i, "F");
				d.value.pop = pop;

				this.kvPop.push({ key: pop.age, value: { pop } });

				enrol = {};
				enrol.T = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "");
				enrol.M = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "M");
				enrol.F = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "F");
				d.value.nEnrol = enrol;

				dt.push(d);

			};
			this.kvPopChart = {
				keyValues: this.kvPop,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value.pop[col]
			}
			this.model = {
				data: dt,
				total: null
			};

			let total = {
				key: "Total",
				value: { enrol: null, pop: null }
			}

			total.value.enrol = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.enrol[p] : e.value.enrol[p];
				});
				return tot;
			}, {})
			total.value.pop = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.pop[p] : e.value.pop[p];
				});
				return tot;
			}, {})

			this.model.total = [total];
		}

		//public valueAccessor(row, col, gender?) {
		//	if (gender) {
		//		return row.value[col]?.[gender];
		//	}
		//	return row.value[col]?.T;
		//}
		public valueAccessor = (row, col, gender?) => XfEnrolment.valueAccessor(row, col, "Enrol", gender);

		public rowAccessor(row, source) {
			if (row.key == "Total") {
				return source.name;
			}
			return row.key;
		}

		public hideZeros(row, grp) {
			return this.valueAccessor(row, "Tot") > 0;
		}

		public onSelection(row, grp, selectedRowKey, selectedColKey) {
			this.selectedAge = selectedRowKey;
			this.selectedGrade = selectedColKey;
			this.touch = Date.now();
		}

		// dynamic classes

		// apply the class "official_age" to rows of official age
		// make sure this is bound to this class
		public officialAgeClass = (row) => this.isOfficialAge(row.key) ? "official_age" : null;
	}



	class Component implements ng.IComponentOptions {
		public bindings = {
			surveyYear: "<",
			edLevelCode: "<",
			districtCode: "<",
			indicatorCalc: "<",
			tables: "<",
			previousState: "<"
		};
		public templateUrl = "indicators/drill/ner";
		public controller = Controller;
		public controllerAs = "vm";

		constructor(ratio: string) {
			this.templateUrl = `indicators/drill/${ratio}`;
		}
	}

	angular
		.module("pineapples")
		.component("indicatorsNerExplore", new Component("ner"))
		.component("indicatorsGerExplore", new Component("ger"));
}