﻿namespace Pineapples.Indicators {

  class Controller {

    public isWaiting = false;
    public isComplete = false;
    public isError = false;
    public from: string;
    public mode: string;      // xl or pq
    public caption;
    public progress: 'linear' | 'circular';
    static $inject = ["$http", "Lookups"];

    constructor(public http: ng.IHttpService, public lookups: Sw.Lookups.LookupService) { }


    public $onInit() {
      if (angular.isUndefined(this.progress)) {
        this.progress = 'linear';
      }
    }

    public download() {
      this.isComplete = false;
      this.isError = false;
      this.isWaiting = true;
      // pass this header to indicate we want base64encoded representation of the workbook
      // this will be decided by HttpFileDownload interceptor
      // see pineapplesApiController.cs SendExcelPackage for how this is identified 
      // and handled on the server
      var config = {
        headers: {
          "Accept": "binary/base64encoded"}
      };
      let tempBase = "https://temp/"; //URL contructor needs a valid URL
      let target = new URL(this.from, tempBase);
      // remove any switch already there
      target.searchParams.delete("xl");
      target.searchParams.delete("pq");
      if (this.mode) {
        target.searchParams.append(this.mode, "");
      }
      

      let dest = target.toString().replace(tempBase, "");
      this.http.get(dest, config)
        .then(response => {
          // nothing needs to happen here because the filedownloadInterceptor has already dealt with it
          this.isComplete = true;
        }, errorResponse => {
            this.isError = true;
        })
        .finally(() => {
          this.isWaiting = false;
				})
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings = {
      from: "@",
      caption: "@",
      mode: "@",      // pq or xl
      drawButton: "@",    // 0 or 1, draw in button
      ngDisabled: "<",
      progress: "@"
    };
    transclude = true;
    public controller = Controller;
    public controllerAs = "vm";
    public templateUrl = `indicators/exceldownload`;
  }

  angular
    .module("pineapples")
    .component("excelDownloadComponent", new Component());
}
