﻿namespace Pineapples.Indicators {
	/**
	 * Component to support drill downs into GER, NER
	 * 
	 */
	class Controller {

		static $inject = ["Lookups", "xFilter", "$state","xfHelper"]
		constructor(public lookups: Pineapples.Lookups.LookupService
			, public xFilter: Sw.xFilter.XFilter
			, private $state: ng.ui.IStateService
			, private xfHelper: xfHelper) {

		}

		// the variables are the essence of the calculation of NER
		private indicatorCalc: Pineapples.Indicators.IndicatorCalc;

		private _surveyYear: number;
		public get surveyYear() {
			return this._surveyYear;
		}
		public set surveyYear(newValue) {
			this._surveyYear = newValue;
			////if (this.dimYearA) { //surveyYear is fixed?
			////	this.dimYearA.filter(this.surveyYear);
			////}
		}
		public edLevelCode: string;
		public districtCode: string;
		public tables: any[];
		public previousState: any;

		public model;

		public yearsInEdLevel: number;

		public yearsOfEd: number[];
		public minYoE: number;
		public maxYoE: number;

		public officialAges: number[];
		public minOfficialAge: number;
		public maxOfficialAge: number;
		public isOfficialAge = (age) => ((age >= this.minOfficialAge && age <= this.maxOfficialAge) || age === null);

		public unknownAgeTotal: number;

		public kvPop: Sw.Charts.KeyValueCollection;

		public kvPopChart: Sw.Charts.AnnotatedKeyValueCollection;

		public enrol: number[];
		public nEnrol: number[];

		public popT: any;

		// values for text, sourced from IndicatorCalc 
		public startYearEnrol: any;
		public startYearNEnrol: any;
		public startYearRep: any;
		public startYearNRep: any;
		public startYearNewEnrol: any; // ie intake
		public startYearNewNEnrol: any;  // net intake
		public startYearPop: any;


		public edLevelClasses: string[];			// the classes in the education level
		public intakeClass: string;			// the lowest class level in the edlevel

		// built from enrolments by age
		public grpAgeA: CrossFilter.Group<any, any, any>;			// group used to draw table of Enrol Rep intake

		public grpTotalA: CrossFilter.Group<any, any, any>;
		public grpTotalOA: CrossFilter.Group<any, any, any>;			// official age totals

		// built from school enrolments
		public dimAgeSchool: CrossFilter.Dimension<any, any>;
		public grpSchool: CrossFilter.Group<any, any, any>;


		public chartData: Sw.Charts.AnnotatedGroup;
		public chartRepData: Sw.Charts.AnnotatedGroup;

		public touch;
		// interactivity
		public selectedAge;
		public selectedGrade;

		public col_stripe = (col, index) => index % 3 == 1 ? "stripe" : "";
		public row_pairstripe = (row, col, index) => index % 3 == 1 ? "stripe" : "";

		public $onChanges(changes) {
			if (changes.tables && changes.tables.currentValue) {
				// data has been retrieved by the ui-router as a Resolve
				// and passed to the dashboard as a binding - process it into a crossfilter

				this.createDimensions();
			}

		}

		
		public createDimensions() {
			//this.dimYear = this.xf.dimension(d => d.SurveyYear);
			//this.dimEdLevel = this.xf.dimension(d => d.EdLevel);

			//this.dimYear.filter(this.surveyYear);
			//this.dimEdLevel.filter("Primary");		// to do

			//this.dimYoE = this.xf.dimension(d => d.yearOfEd);
			//this.dimTotal = this.xf.dimension(d => "Total");
			//this.grpYoE = this.xFilter.xReduce(this.dimYoE, "EdLevel", d => ({
			//	Enrol: d.Enrol,
			//	OfficialAge: d.OfficialAge,
			//}))
			//this.grpTotal = this.xFilter.xReduce(this.dimTotal, "EdLevel", d => ({
			//	Enrol: d.Enrol,
			//	OfficialAge: d.OfficialAge,
			//}))

			// establish what class levels we need
			this.edLevelClasses = this.lookups.edLevelClasses(this.edLevelCode);
			this.intakeClass = this.edLevelClasses[0];

			// First crossfilter - intake class level enrolments, by age
			let xfE = this.xfHelper.createXfEnrolment(this.tables[0]);

			// only a single year - permanently filtered
			xfE.xf.dimension(d => d.SurveyYear).filter(this.surveyYear);
			// only first class level of the selected ed level
			xfE.xf.dimension(d => d.ClassLevel).filter(this.intakeClass);

			this.grpAgeA = xfE.AgeClassGroup();

			// construct group for totals across Ages
			let dimTotalA = xfE.xf.dimension(d => "Total");
			this.grpTotalA = xfE.AgeClassGroup(dimTotalA);

			let r = this.xFilter.find(this.grpAgeA, { key: null });
			this.unknownAgeTotal = r?.Tot.T;

			// for enrolment chart
			this.chartData = {
				group: this.grpAgeA,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value?.[this.intakeClass]?.[col]?.Enrol
				// to do dynamic on column click?
			}

			// data for repeater chart
			this.chartRepData = {
				group: this.grpAgeA,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value?.[this.intakeClass]?.[col]?.Rep
				// to do dynamic on column click?
			}

			// school drill down - this group of crossfilter objects manage the drill down into school
			xfE = this.xfHelper.createXfEnrolment(this.tables[1]);
			let xf = <CrossFilter.CrossFilter<ISchoolEnrolData>>xfE.xf;
			// dimension on age - we need to be able to filter on this so it is saved
			this.dimAgeSchool = <CrossFilter.Dimension<ISchoolEnrolData, number>>xf.dimension(d => d.Age);
			// filter permanently on class level of intake class
			xfE.xf.dimension(d => d.ClassLevel).filter(this.intakeClass);
			// get the group that will be rendered in the crossfilter-table
			this.grpSchool = xfE.YearEnrolGroup("SchoolNo");

		}

		public $onInit() {


			let ix: IndicatorCalc = this.indicatorCalc;

			// class level and age ranges for the Education Level - from the 'vermpaf'
			this.minYoE = this.indicatorCalc.edLevelFirstYoE(this.surveyYear, this.edLevelCode);
			this.maxYoE = this.indicatorCalc.edLevelLastYoE(this.surveyYear, this.edLevelCode);
			this.minOfficialAge = this.indicatorCalc.edLevelStartAge(this.surveyYear, this.edLevelCode);
			this.maxOfficialAge = this.minOfficialAge + (this.maxYoE - this.minYoE);

			// enrolment totals for the education level - from the vermpaf - 
			// can crosscheck against grpTotal, grpTotalA
			this.startYearRep = {
				M: ix.yoeRep(this.surveyYear, this.minYoE,"M"),
				F: ix.yoeRep(this.surveyYear, this.minYoE, "F"),
				T: ix.yoeRep(this.surveyYear, this.minYoE, ""),
			}
			this.startYearEnrol = {
				M: ix.yoeEnrol(this.surveyYear, this.minYoE, "M"),
				F: ix.yoeEnrol(this.surveyYear, this.minYoE, "F"),
				T: ix.yoeEnrol(this.surveyYear, this.minYoE, ""),
			}
			// official age (net) enrolments from vermpaf - check against grpTotalOA
			this.startYearNEnrol = {
				M: ix.yoeNetEnrol(this.surveyYear, this.minYoE, "M"),
				F: ix.yoeNetEnrol(this.surveyYear, this.minYoE, "F"),
				T: ix.yoeNetEnrol(this.surveyYear, this.minYoE, ""),
			}

			this.startYearNRep = {
				M: ix.yoeNetRep(this.surveyYear, this.minYoE, "M"),
				F: ix.yoeNetRep(this.surveyYear, this.minYoE, "F"),
				T: ix.yoeNetRep(this.surveyYear, this.minYoE, ""),
			}

			this.startYearNewEnrol = {
				M: this.startYearEnrol.M - this.startYearRep.M,
				F: this.startYearEnrol.F - this.startYearRep.F,
				T: this.startYearEnrol.T - this.startYearRep.T
			}

			this.startYearNewNEnrol = {
				M: this.startYearNEnrol.M - this.startYearNRep.M,
				F: this.startYearNEnrol.F - this.startYearNRep.F,
				T: this.startYearNEnrol.T - this.startYearNRep.T
			}

			this.startYearPop = {
				M: ix.yoePop(this.surveyYear, this.minYoE, "M"),
				F: ix.yoePop(this.surveyYear, this.minYoE, "F"),
				T: ix.yoePop(this.surveyYear, this.minYoE, ""),
			}
			// number of years in the education level is sourced from the vermpaf
			this.yearsInEdLevel = ix.edLevelHeader(this.surveyYear, this.edLevelCode, "numYears")

			this.kvPop = [];

			// make an array of these
			let e: Array<any> = [];

			let p: Array<any> = [];
			let dt: Array<any> = [];
			for (let i = this.minYoE; i <= this.maxYoE; i++) {
				let d = {
					key: i,
					value: { enrol: null, pop: null, nEnrol: null }
				}
				let enrol: any = {};

				enrol.T = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "");
				enrol.M = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "M");
				enrol.F = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "F");
				d.value.enrol = enrol;

				let pop: any = {};
				pop.age = this.minOfficialAge + i - this.minYoE;
				pop.T = this.indicatorCalc.yoePop(this.surveyYear, i, "");
				pop.M = this.indicatorCalc.yoePop(this.surveyYear, i, "M");
				pop.F = this.indicatorCalc.yoePop(this.surveyYear, i, "F");
				d.value.pop = pop;

				this.kvPop.push({ key: pop.age, value: { pop } });

				enrol = {};
				enrol.T = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "");
				enrol.M = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "M");
				enrol.F = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "F");
				d.value.nEnrol = enrol;

				dt.push(d);

			};
			this.kvPopChart = {
				keyValues: this.kvPop,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value.pop[col]
			}
			this.model = {
				data: dt,
				total: null
			};

			let total = {
				key: "Total",
				value: { enrol: null, pop: null }
			}

			total.value.enrol = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.enrol[p] : e.value.enrol[p];
				});
				return tot;
			}, {})
			total.value.pop = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.pop[p] : e.value.pop[p];
				});
				return tot;
			}, {})

			this.model.total = [total];
		}

		public valueAccessor = (row, col, gender?) => XfEnrolment.valueAccessor(row, this.intakeClass, col, gender);


		public columnTranslator = (col) => {
			let s = XfEnrolment.columnTranslator(this.surveyYear, col);
			return `${this.lookups.byCode("levels", this.intakeClass, "N")} ${s}`;
		}

		public rowAccessor = (row, source) => {
			if (row.key == "Total") {
				//return source.name;
			}
			return row.key;
		}

		//public hideZeros(row, grp) {
		//	return this.valueAccessor(row, "Tot") > 0;
		//}

		public onSelection(row, grp, selectedRowKey, selectedColKey) {
			this.selectedAge = selectedRowKey;
			this.selectedGrade = selectedColKey;

			// usually we treat null as filterAll(), but here null is a valid key
			this.dimAgeSchool.filter(age => ((this.selectedAge === undefined) || age === this.selectedAge));
			//this.dimAgeSchool.filter(this.selectedAge);
			this.touch = Date.now();
		}

		// dynamic classes

		// apply the class "official_age" to rows of official age
		// make sure this is bound to this class
		// public officialAgeClass = (row) => this.isOfficialAge(row.key) ? "official_age" : null;

		public officialAgeClass = (row) => (row.key == this.minOfficialAge) ? "official_age" : null;
	}



	class Component implements ng.IComponentOptions {
		public bindings = {
			surveyYear: "<",
			edLevelCode: "<",
			districtCode: "<",
			indicatorCalc: "<",
			tables: "<"
		};
		public templateUrl = "indicators/drill/ner";
		public controller = Controller;
		public controllerAs = "vm";

		constructor(ratio: string) {
			this.templateUrl = `indicators/drill/${ratio}`;
		}
	}

	angular
		.module("pineapples")
		.component("indicatorsNirExplore", new Component("nir"))
		.component("indicatorsGirExplore", new Component("gir"));
}