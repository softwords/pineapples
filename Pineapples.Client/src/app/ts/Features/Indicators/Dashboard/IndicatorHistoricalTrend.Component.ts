﻿namespace Pineapples.Dashboards.Indicators {

	class Controller {

		// Currently not using the crossfilter goodies. This is merely a least amount
		// of effort to migrate the remaining plottable charts to echart

		// bindings
		public indicators: any;
		public title: string;
		public chartType: string;
		public indicator: string;
		public valueFormat: string;
		public meta: Array<string>;
		public ngDisabled: boolean;

		public echartOption: any;

		// Data retrieval and manipulation variables
		private indicatorsMgr: Pineapples.Indicators.IndicatorsMgr;
		private indicatorCalc: Pineapples.Indicators.IndicatorCalc;
		private selectedEdLevel: string;
		private selectedSector: string;
		private selectedDistrict: string;
		private years: Array<number>;

		// Convert to echart using the widgetPkg transit format
		public widgetPkg: Sw.Charts.IWidgetPkg;
		public palette: string[];
		public echart: echarts.ECharts;

		////////////////////////////////////////////////////////////////
		// LifeCycle events
		/////////////////////////////////////////////////////////////////
		public $onInit() {
		}

		public $onChanges(changes: any) {
		}

		public eChartInit(echart) {
			this.echart = echart;
		}

		public $postLink() {
			this.makeWidgetPkg();
		}

		public makeWidgetPkg() {
			this.indicatorsMgr = this.indicators.indicatorsMgr;
			this.indicatorCalc = this.indicators.indicatorCalc;
			this.selectedEdLevel = this.indicators.selectedEdLevel;
			this.selectedSector = this.indicators.selectedSector;
			this.selectedDistrict = this.indicators.selectedDistrict;
			this.years = this.indicatorCalc.getYearsWithData();

			let chartData: any[][] = [[], [], []];
			let indicatorsData: any = {};

			// For each year and the current selectedEdLevel/selectedSector retrieve the data and fill up
			// the chartData
			this.years.forEach((year) => {
				let yearData = this.indicatorsMgr.get(this.indicatorCalc, year, this.selectedEdLevel, this.selectedSector, this.selectedDistrict);
				indicatorsData[year] = yearData;

				chartData[0].push({ year: year, value: yearData[this.indicator].M });
				chartData[1].push({ year: year, value: yearData[this.indicator].F });
				chartData[2].push({ year: year, value: yearData[this.indicator].T });
			});

			// Remove empty years (i.e. NaN or 0 returned from indicatorsMgr.get) from datasets. 
			chartData.forEach((serie) => {
				_.remove(serie, function (i) {
					return _.isNaN(i.value) || i.value == 0;
				});
			});

			// Package in ready format to be transmormed and processed by echart
			this.widgetPkg = {
				data: chartData,
				X: 'year',
				Y: 'value',
				seriesNames: this.meta
			}
		}

		public onChartRender(option: echarts.EChartOption, echart) {
			console.log("Line Chart onChartRender");
			console.log(option);
			//option['title'] = { text: this.title };
			option.series.forEach((series: echarts.EChartOption.SeriesLine) => {
				series.smooth = true;
			})
		}

	}

	class Component implements ng.IComponentOptions {
		public bindings: any = {
			indicators: "<",
			title: "@",
			chartType: "@",
			indicator: "@",
			meta: "<",
			ngDisabled: "=",
			valueFormat: "@"
		};

		public controller: any = Controller;
		public controllerAs: string = "vm";
		public template: string = `
        <div class="dashboard-wrapper" style="padding: 5px;"> 
						<row-col-chart
									charttype = "l"
									dataset="vm.widgetPkg"
									on-chart-init="vm.eChartInit(echart)"
									on-chart-render="vm.onChartRender(option, echart)"
									colors="vm.palette"/>
        </div>`;
	}

	angular
		.module("pineapples")
		.component("echart", new Component());
}