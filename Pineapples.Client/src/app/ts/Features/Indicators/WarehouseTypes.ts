﻿namespace Pineapples.Indicators {

	export interface IEnrolData {
		SurveyYear: number,
		GenderCode: string,
		ClassLevel?: string,
		Age?: number,
		// quanitites
		Enrol: number,
		Rep: number
	}
	export interface ISchoolEnrolData extends IEnrolData {
		// aggregations that may or may not be present
		SchoolNo: string,
		"School Name"?: string
	}

}