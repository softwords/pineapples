﻿namespace Pineapples.Api {

  export interface IIndicatorsApi {
    indicators(districtCode?: string): ng.IPromise<any>;
    refreshWarehouse(startFromYear): ng.IPromise<any>;
    refreshIndicators(districtCode?: string): ng.IPromise<any>;
  }

	class IndicatorsApi implements IIndicatorsApi {

		static $inject = ["$q", "$http", "Restangular"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService) {
		}

		public indicators(districtCode?: string) {
			let endpoint = (districtCode)?
				`api/indicators/${districtCode}`
				: "api/indicators"
			return this.http.get(endpoint).then((response) => {
				return response.data;       // return the xml payload
			});
		}

		// continue to use restangular for now 
		public refreshWarehouse(startFromYear) {
			return this.restangular.all("indicators").customGET("makewarehouse", { year: startFromYear });
		}

		public refreshIndicators(districtCode: string){
			let endpoint = (districtCode) ?
				`api/indicators/refresh/${districtCode}`
				: "api/indicators/refresh"
			return this.http.get(endpoint).then((response) => {
				return response.data;       // return nothing
			});
		}

	}
 
  angular.module("pineapplesAPI")
    .service("IndicatorsAPI", IndicatorsApi);
}
