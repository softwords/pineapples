﻿namespace Pineapples.Indicators {
	
	class Controller {

		public surveyYear: number;
		static $inject = ["Lookups", "xFilter"]
		constructor(public lookups: Pineapples.Lookups.LookupService, public xFilter: Sw.xFilter.XFilter) {

		}

		// drill down school table
		// 
		public valueAccessor = (row: EnrolGroupRow, col, gender) => {
			// values group by year - we can extract the enrol from previous year, and repeaters from current year
			switch (col) {
				case "SchoolName":
					// if used as a column, assumes row.key is school no
					// only display on total
					if ((!gender) || gender == "T") {
						return this.lookups.byCode("schoolNames", row.key, "N");
					}
					return "";
				default:
					return XfEnrolment.valueAccessor(row, this.surveyYear, col, gender);
			}
		}
		public valueFormatter(value, row, col: string) {
			return XfEnrolment.valueFormatter(value, row, col);
		}

		public columnTranslator = (col) => {
			switch (col) {
				default:
					return XfEnrolment.columnTranslator(this.surveyYear, col)
			}
		}

		public rowAccessor = (row, group) => {
			//return `${row.key}: ${this.lookups.byCode("schoolNames", row.key, "N")}`
			return row.key;
		}

		public cellClassHandler = (row, col) => {
			switch (col) {
				case "SchoolName":
					return "left-align"

			}
		}

		public rowFilterRepRate = (row) => this.valueAccessor(row, 'EnrolLY', 'T') > 0 || this.valueAccessor(row, 'Rep', 'T') > 0;
		public rowFilterIntake = (row) => this.valueAccessor(row, 'Enrol', 'T') > 0 || this.valueAccessor(row, 'Rep', 'T') > 0;
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			source: "<",
			classes: "<",
			genderTotals: "<",
			surveyYear: "<",
			selectedClass: "<"
		};
		public templateUrl: string;
		public controller = Controller;
		public controllerAs = "vm";
		constructor(template: string) {
			this.templateUrl = template;
		}

	}

	angular
		.module("pineapples")
		.component("schoolGradeRepRateTable", new Component("indicators/table/schoolGradeRepRateTable"))
		.component("schoolIntakeTable", new Component("indicators/table/schoolIntakeTable"))
}