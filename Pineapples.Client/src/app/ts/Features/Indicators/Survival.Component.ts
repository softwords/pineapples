﻿namespace Pineapples.Indicators {
	/**
	 * Component to support drill downs into GER, NER
	 * 
	 */
	class Controller {

		static $inject = ["Lookups", "xFilter", "$state"]
		constructor(public lookups: Pineapples.Lookups.LookupService, public xFilter: Sw.xFilter.XFilter, private $state: ng.ui.IStateService,) {

		}

		// the variables are the essence of the calculation of NER
		private indicatorCalc: Pineapples.Indicators.IndicatorCalc;

		private _surveyYear: number;
		public get surveyYear() {
			return this._surveyYear;
		}
		public set surveyYear(newValue) {
			this._surveyYear = newValue;
			if (this.dimYearA) {
				this.dimYearA.filter(this.surveyYear);
			}
		}
		public edLevelCode: string;
		public tables: any[];
		public previousState: any;

		public model;

		public yearsInEdLevel: number;

		public yearsOfEd: number[];
		public minYoE: number;
		public maxYoE: number;

		public officialAges: number[];
		public minOfficialAge: number;
		public maxOfficialAge: number;
		public isOfficialAge = (age) => ((age >= this.minOfficialAge && age <= this.maxOfficialAge) || age === null);

		public unknownAgeTotal: number;

		public kvPop: Sw.Charts.KeyValueCollection;

		public kvPopChart: Sw.Charts.AnnotatedKeyValueCollection;

		public enrol: number[];
		public nEnrol: number[];

		public popT: any;

		public startYearEnrol: any;
		public startYearNEnrol: any;
		public startYearRep: any;
		public startYearNewEnrol: any;
		public startYearPop: any;

		public edLevelClasses: string[];			// the classes in the education level


		public xf: CrossFilter.CrossFilter<any>;
		public dimYoE: CrossFilter.Dimension<any, any>;
		public dimYear: CrossFilter.Dimension<any, any>;
		public dimEdLevel: CrossFilter.Dimension<any, any>;
		public grpYoE: CrossFilter.Group<any, any, any>;
		public dimTotal: CrossFilter.Dimension<any, any>;
		public grpTotal: CrossFilter.Group<any, any, any>;

		// built from enrolments by age
		public xfA: CrossFilter.CrossFilter<any>;
		public dimYearA: CrossFilter.Dimension<any, any>;
		public dimEdLevelA: CrossFilter.Dimension<any, any>;
		public dimAgeA: CrossFilter.Dimension<any, any>;
		public grpAgeA: CrossFilter.Group<any, any, any>;
		public dimTotalA: CrossFilter.Dimension<any, any>;
		public grpTotalA: CrossFilter.Group<any, any, any>;
		public grpTotalOA: CrossFilter.Group<any, any, any>;			// official age totals

		// built from school enrolments
		public xfSchool: CrossFilter.CrossFilter<any>;
		public dimAgeSchool: CrossFilter.Dimension<any, any>;
		public dimSchool: CrossFilter.Dimension<any, any>;
		public grpSchool: CrossFilter.Group<any, any, any>;


		public chartData: Sw.Charts.AnnotatedGroup;
		public touch;
		// interactivity
		public selectedAge;
		public selectedGrade;

		public col_stripe = (col, index) => index % 3 == 1 ? "stripe" : "";
		public row_pairstripe = (row, col, index) => index % 3 == 1 ? "stripe" : "";

		public $onChanges(changes) {
			if (changes.tables && changes.tables.currentValue) {
				// data has been retrieved by the ui-router as a Resolve
				// and passed to the dashboard as a binding - process it into a crossfilter
				// create the crossfilter from the warehouse table
				this.xf = crossfilter(this.tables[0]);
				this.xfA = crossfilter(this.tables[1]);
				this.xfSchool = crossfilter(this.tables[2]);
				this.createDimensions();
			}

		}

		public createDimensions() {
			this.dimYear = this.xf.dimension(d => d.SurveyYear);
			this.dimEdLevel = this.xf.dimension(d => d.EdLevel);

			this.dimYear.filter(this.surveyYear);
			this.dimEdLevel.filter("Primary");		// to do

			this.dimYoE = this.xf.dimension(d => d.yearOfEd);
			this.dimTotal = this.xf.dimension(d => "Total");
			this.grpYoE = this.xFilter.xReduce(this.dimYoE, "EdLevel", d => ({
				Enrol: d.Enrol,
				OfficialAge: d.OfficialAge,
			}))
			this.grpTotal = this.xFilter.xReduce(this.dimTotal, "EdLevel", d => ({
				Enrol: d.Enrol,
				OfficialAge: d.OfficialAge,
			}))


			this.dimYearA = this.xfA.dimension(d => d.SurveyYear);
			this.dimEdLevelA = this.xfA.dimension(d => d.ClassLevel);

			this.dimYearA.filter(this.surveyYear);

			let r: any = this.lookups.byCode("educationLevels", this.edLevelCode);
			let minYear = r.MinY;
			let maxYear = r.MaxY;

			let classLevelsForEdLevel = (edLevelCode) => {
				return (classLevelCode) =>
					this.lookups.byCode("levels", classLevelCode, "L") == edLevelCode;
			}

			this.edLevelClasses = this.lookups.cache["levels"].filter((classrow: any) => {
				return (classrow.L == this.edLevelCode);
			}).sort((a: any, b: any) => a.YoEd - b.YoEd)
				.map((classrow: any) => classrow.C);

			this.dimEdLevelA.filter(classLevelsForEdLevel(this.edLevelCode));		// to do

			this.dimAgeA = this.xfA.dimension(d => d.Age);
			this.dimTotalA = this.xfA.dimension(d => "Total");
			this.grpAgeA = this.xFilter.xReduce(this.dimAgeA, "ClassLevel",
				this.xFilter.getFlattenedGenderAccessor("GenderCode", "Enrol"));

			this.grpTotalA = this.xFilter.xReduceGrouped(this.dimTotalA, "ClassLevel",
				this.xFilter.getFlattenedGenderAccessor("GenderCode", "Enrol"),
				(row) => "Total Enrol");

			// filter for official age totals by class level
			let officialAgeTot = (d) => {
				if (this.isOfficialAge(d.Age)) {
					return {
						M: d.GenderCode == "M" ? d.Enrol : null,
						F: d.GenderCode == "F" ? d.Enrol : null,
						T: d.Enrol
					};
				}
				return { M: null, F: null, T: null }
			}

			this.grpTotalOA = this.xFilter.xReduceGrouped(this.dimTotalA, "ClassLevel",
				officialAgeTot, (row) => "Official Age Enrol");

			r = this.xFilter.find(this.grpAgeA, { key: null });
			this.unknownAgeTotal = r?.Tot.T;



			this.chartData = {
				group: this.grpAgeA,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value?.Tot?.[col]
				// to do dynamic on column click?
				// valueAccessor: (kv, col) => kv.value?.[this.selectedGrade]?.[col]
			}

			// drill down data into school
			this.dimAgeSchool = this.xfSchool.dimension(d => d.Age);
			this.dimSchool = this.xfSchool.dimension(d => d.SchNo);
			this.grpSchool = this.xFilter.xReduce(this.dimSchool, d => d.ClassLevel,
				d => ({
					M: d.Tot_M,
					F: d.Tot_F,
					T: d.Tot
				})
			);
		}

		public $onInit() {


			let ix: IndicatorCalc = this.indicatorCalc;

			// class level and age ranges for the Education Level - from the 'vermpaf'
			this.minYoE = this.indicatorCalc.edLevelFirstYoE(this.surveyYear, this.edLevelCode);
			this.maxYoE = this.indicatorCalc.edLevelLastYoE(this.surveyYear, this.edLevelCode);
			this.minOfficialAge = this.indicatorCalc.edLevelStartAge(this.surveyYear, this.edLevelCode);
			this.maxOfficialAge = this.minOfficialAge + (this.maxYoE - this.minYoE);

			// enrolment totals for the education level - from the vermpaf - 
			// can crosscheck against grpTotal, grpTotalA
			this.startYearRep = {
				M: ix.yoeRep(this.surveyYear, this.minYoE,"M"),
				F: ix.yoeRep(this.surveyYear, this.minYoE, "F"),
				T: ix.yoeRep(this.surveyYear, this.minYoE, ""),
			}
			this.startYearEnrol = {
				M: ix.yoeEnrol(this.surveyYear, this.minYoE, "M"),
				F: ix.yoeEnrol(this.surveyYear, this.minYoE, "F"),
				T: ix.yoeEnrol(this.surveyYear, this.minYoE, ""),
			}
			// official age (net) enrolments from vermpaf - check against grpTotalOA
			this.startYearNEnrol = {
				M: ix.yoeNetEnrol(this.surveyYear, this.minYoE, "M"),
				F: ix.yoeNetEnrol(this.surveyYear, this.minYoE, "F"),
				T: ix.yoeNetEnrol(this.surveyYear, this.minYoE, ""),
			}

			this.startYearNewEnrol = {
				M: this.startYearEnrol.M - this.startYearRep.M,
				F: this.startYearEnrol.F - this.startYearRep.F,
				T: this.startYearEnrol.T - this.startYearRep.T
			}

			this.startYearPop = {
				M: ix.yoePop(this.surveyYear, this.minYoE, "M"),
				F: ix.yoePop(this.surveyYear, this.minYoE, "F"),
				T: ix.yoePop(this.surveyYear, this.minYoE, ""),
			}
			// number of years in the education level is sourced from the vermpaf
			this.yearsInEdLevel = ix.edLevelHeader(this.surveyYear, this.edLevelCode, "numYears")

			this.kvPop = [];

			// make an array of these
			let e: Array<any> = [];

			let p: Array<any> = [];
			let dt: Array<any> = [];
			for (let i = this.minYoE; i <= this.maxYoE; i++) {
				let d = {
					key: i,
					value: { enrol: null, pop: null, nEnrol: null }
				}
				let enrol: any = {};

				enrol.T = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "");
				enrol.M = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "M");
				enrol.F = this.indicatorCalc.yoeEnrol(this.surveyYear, i, "F");
				d.value.enrol = enrol;

				let pop: any = {};
				pop.age = this.minOfficialAge + i - this.minYoE;
				pop.T = this.indicatorCalc.yoePop(this.surveyYear, i, "");
				pop.M = this.indicatorCalc.yoePop(this.surveyYear, i, "M");
				pop.F = this.indicatorCalc.yoePop(this.surveyYear, i, "F");
				d.value.pop = pop;

				this.kvPop.push({ key: pop.age, value: { pop } });

				enrol = {};
				enrol.T = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "");
				enrol.M = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "M");
				enrol.F = this.indicatorCalc.yoeNetEnrol(this.surveyYear, i, "F");
				d.value.nEnrol = enrol;

				dt.push(d);

			};
			this.kvPopChart = {
				keyValues: this.kvPop,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value.pop[col]
			}
			this.model = {
				data: dt,
				total: null
			};

			let total = {
				key: "Total",
				value: { enrol: null, pop: null }
			}

			total.value.enrol = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.enrol[p] : e.value.enrol[p];
				});
				return tot;
			}, {})
			total.value.pop = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.pop[p] : e.value.pop[p];
				});
				return tot;
			}, {})

			this.model.total = [total];
		}

		public valueAccessor(row, col, gender?) {
			if (gender) {
				return row.value[col]?.[gender];
			}
			return row.value[col]?.T;
		}

		public rowAccessor(row, source) {
			if (row.key == "Total") {
				return source.name;
			}
			return row.key;
		}

		public hideZeros(row, grp) {
			return this.valueAccessor(row, "Tot") > 0;
		}

		public onSelection(row, grp, selectedRowKey, selectedColKey) {
			this.selectedAge = selectedRowKey;
			this.selectedGrade = selectedColKey;

			// usually we treat null as filterAll(), but here null is a valid key
			this.dimAgeSchool.filter(age => (this.selectedAge === undefined || age === this.selectedAge));
			this.touch = Date.now();
		}

		// dynamic classes

		// apply the class "official_age" to rows of official age
		// make sure this is bound to this class
		public officialAgeClass = (row) => this.isOfficialAge(row.key) ? "official_age" : null;
	}



	class Component implements ng.IComponentOptions {
		public bindings = {
			surveyYear: "<",
			edLevelCode: "<",
			indicatorCalc: "<",
			tables: "<"
		};
		public templateUrl = "indicators/drill/survival";
		public controller = Controller;
		public controllerAs = "vm";

	}

	angular
		.module("pineapples")
		.component("indicatorsSurvivalExplore", new Component());
}