﻿namespace Pineapples.Indicators {

	/**
	 * Component to support drill downs into Teacher count
	 * 
	 */
	class Controller {

		static $inject = ["Lookups", "xFilter", "$state", "xfHelper"]
		constructor(public lookups: Pineapples.Lookups.LookupService
			, public xFilter: Sw.xFilter.XFilter, private $state: ng.ui.IStateService
			, private xfHelper: xfHelper) {
		}

		private indicatorCalc: Pineapples.Indicators.IndicatorCalc;

		private _surveyYear: number;
		public get surveyYear() {
			return this._surveyYear;
		}
		public set surveyYear(newValue) {
			this._surveyYear = newValue;
		}
		public edLevelCode: string;
		public districtCode: string;
		public tables: any[];
		public previousState: any;

		public yearsInEdLevel: number;

		public yearsOfEd: number[];
		public minYoE: number;
		public maxYoE: number;

		public enrolT: any;
		public repT: any;
		public enrolTly: any;

		public nEnrolT: any;

		// crossfilter object for main crossfilter table 
		public grpLevel;
		public grpLevelTotal;

		// built from school enrolments
		public dimSchoolLevel: CrossFilter.Dimension<ISchoolEnrolData, string>;
		public grpSchool: CrossFilter.Group<ISchoolEnrolData, string, any>;

		public $onInit() {
			let ix: IndicatorCalc = this.indicatorCalc;

			// class level and age ranges for the Education Level - from the 'vermpaf'
			this.minYoE = this.indicatorCalc.edLevelFirstYoE(this.surveyYear, this.edLevelCode);
			this.maxYoE = this.indicatorCalc.edLevelLastYoE(this.surveyYear, this.edLevelCode);

			// set up class level crossfilter
			let xfE = this.xfHelper.createXfEnrolment(this.tables[0]);
			// filter all levels permanentnly on ed level
			//xfE.FilterClassesByYearofEd(this.minYoE, this.maxYoE);
			xfE.FilterByEdLevel(this.edLevelCode);
	
			this.grpLevel = xfE.YearEnrolGroup("ClassLevel");
			// make the totals
			this.grpLevelTotal = xfE.YearEnrolGroup(xfE.xf.dimension(d => "Total"));

			// school drill down - this group of crossfilter objects manage the drill down into school
			xfE = this.xfHelper.createXfEnrolment(this.tables[1]);

			let xf = <CrossFilter.CrossFilter<ISchoolEnrolData>>xfE.xf;
			// dimension on level - we need to be able to filter on this as it is saved
			this.dimSchoolLevel = <CrossFilter.Dimension<ISchoolEnrolData, string>>xf.dimension(d => d.ClassLevel);
			// filter permanently on year of ed
			xfE.FilterByYearofEd(this.minYoE, this.maxYoE);
			// 
			// get the group that will be rendered in the detail crossfilter-table
			this.grpSchool = xfE.YearEnrolGroup("SchoolNo");


			// enrolment totals for the education level - from the vermpaf - 
			// can crosscheck against grpTotal, grpTotalA
			this.enrolT = {
				M: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "M"),
				F: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "F"),
				T: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "")
			}

			this.repT = {
				M: ix.edLevelRep(this.surveyYear, this.edLevelCode, "M"),
				F: ix.edLevelRep(this.surveyYear, this.edLevelCode, "F"),
				T: ix.edLevelRep(this.surveyYear, this.edLevelCode, "")
			}

			this.enrolTly = {
				M: ix.edLevelEnrol(this.surveyYear - 1, this.edLevelCode, "M"),
				F: ix.edLevelEnrol(this.surveyYear - 1, this.edLevelCode, "F"),
				T: ix.edLevelEnrol(this.surveyYear - 1, this.edLevelCode, "")
			}
			// official age (net) enrolments from vermpaf - check against grpTotalOA
			this.nEnrolT = {
				M: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "M"),
				F: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "F"),
				T: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "")
			}
			// number of years in the education level is sourced from the vermpaf
			this.yearsInEdLevel = ix.edLevelHeader(this.surveyYear, this.edLevelCode, "numYears")

		}

		//*******************************************
		// Accessor functions for tables, charts
		//*******************************************

		public valueAccessor = (row, col, gender) => {
			return XfEnrolment.valueAccessor(row, this.surveyYear, col, gender);
		}

		public valueFormatter(value, row, col: string) {
			return XfEnrolment.valueFormatter(value, row, col);
		}

		public rowAccessor = (row, group) => 
			this.lookups.byCode("levels", row.key, "N")??row.key;


		public orderLevels = (rowa, rowb) => {
			let ya = this.lookups.byCode("levels", rowa.key, "YoEd");
			let yb = this.lookups.byCode("levels", rowb.key, "YoEd");
			return Sw.Charts.sortComparator(0)(ya, yb);
		}
		public columnTranslator = (col) => {
			return XfEnrolment.columnTranslator(this.surveyYear, col);
		}

		// handling of drilldown

		public selectedGrade;
		public touch: number;

		public onSelection(row, grp, selectedRowKey, selectedColKey) {
			this.selectedGrade = selectedRowKey;

			// usually we treat null as filterAll(), but here null is a valid key
			this.dimSchoolLevel.filter(this.selectedGrade);
			this.touch = Date.now();
		}

	}



	class Component implements ng.IComponentOptions {
		public bindings = {
			surveyYear: "<",
			edLevelCode: "<",
			districtCode: "<",
			indicatorCalc: "<",
			tables: "<"
		};
		public templateUrl = "indicators/drill/repeater";
		public controller = Controller;
		public controllerAs = "vm";

	}

	angular
		.module("pineapples")
		.component("indicatorsRepeaterExplore", new Component())
}