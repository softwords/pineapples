﻿namespace Pineapples.Indicators {
	/**
	 * Component to support drill downs into intake at the grade level
	 * This component differs from SchoolRepRateTable in that enrolment and repeaters
	 * are both taken from the current survey year. (For RepRate, enrolment is from the previous year)
	 * This means we can get the Intake simply as 
	 */
	class Controller {

		public surveyYear: number;
		static $inject = ["Lookups", "xFilter"]
		constructor(public lookups: Pineapples.Lookups.LookupService, public xFilter: Sw.xFilter.XFilter) {

		}

		// drill down school table
		// 
		public yvalueAccessor = (row, col, gender?) => {
			console.log(row, col, gender);
			switch (col) {
				case "SchoolName":
					// if used as a column, assumes row.key is school no
					// only display on total
					if ((!gender) || gender == "T") {
						return this.lookups.byCode("schoolNames", row.key, "N");
					}
					return "";
			}
			return XfEnrolment.valueAccessor(row, col, "Enrol", gender);
		}

		public valueAccessor = (row, col, gender) => {
			return 6;
			// values group by year - we can extract the enrol from previous year, and repeaters from current year
			switch (col) {
				case "Enrol":
					return row.value?.[(this.surveyYear - 1).toString()]?.[(gender || "T")]?.[col];
					break;
				case "Rep":
					return row.value?.[(this.surveyYear).toString()]?.[(gender || "T")]?.[col];
					break;
				case "RepRate":
					let e = row.value?.[(this.surveyYear - 1).toString()]?.[(gender || "T")]?.["Enrol"];
					let r = row.value?.[(this.surveyYear).toString()]?.[(gender || "T")]?.["Rep"];
					return (e == 0? "": `${Math.round(r * 10000 / e) / 100}%`);
					break;
				case "SchoolName":
					return this.lookups.byCode("schoolNames",row.key,"N")
			}
			if (!gender) {
				return row.value.Tot?.T?.[col];
			}
			return row.value.Tot?.[gender]?.[col];
		}

		public columnTranslator = (col) => {
			switch (col) {
				case "Enrol":
					return `Enrolment ${(this.surveyYear - 1).toString()}`;
					break;
				case "Rep":
					return `Repeaters ${(this.surveyYear).toString()}`;
					break;
				case "RepRate":
					return `Repetition Rate ${(this.surveyYear - 1).toString()} -> ${(this.surveyYear).toString()}`;
			}
		}

		public colclassHandler = (col) => {
			switch (col) {
				case "SchoolName":
					return "left-align"
					
			}
		}


		public rowFilter = (row) => this.valueAccessor(row, 'Enrol','T') > 0;
	}



	class Component implements ng.IComponentOptions {
		public bindings = {
			source: "<",
			classes: "<",
			genderTotals: "<",
			surveyYear: "<",
			selectedClass: "<"
		};
		public templateUrl = "indicators/table/schoolGradeRepRateTable";
		public controller = Controller;
		public controllerAs = "vm";

	}

	angular
		.module("pineapples")
		//.component("schoolIntakeTable", new Component())

}