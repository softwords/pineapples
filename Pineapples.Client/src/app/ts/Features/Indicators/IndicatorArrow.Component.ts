﻿namespace Pineapples.Indicators {

  class Controller {
    public v1: number;
    public v2: number;
    public change: number;      // rounded change

    public roundTo: number;
    public improve: string;

    public iconName: string;

    public color: string;

    private colors = ["green", "red", "grey"];    // increase, decline, same

    updown: number; // 

    public $onChanges(changes) {
      let roundFactor = Math.pow(10, this.roundTo ?? 0);
      let a1 = Math.round(this.v1 * roundFactor) / roundFactor;
      let a2 = Math.round(this.v2 * roundFactor) / roundFactor;
      this.change = a2 - a1;

      this.updown = this.change / Math.abs(this.change);
      this.iconName = this.updown == 1 ? "trending_up" :
        this.updown == -1 ? "trending_down" :
          "trending_flat";

      this.iconName = this.updown == 1 ? "arrow_drop_up" :
        this.updown == -1 ? "arrow_drop_down" : "";      //"horizontal_rule"

      let colorindex = (this.improve ?? "up" == "up" ? 1 : -1) * this.updown;
      colorindex = colorindex == 1 ? 0 : colorindex == -1 ? 1 : 2;      // index into colors array
      this.color = this.colors[colorindex]
    }



	}

  class Component implements ng.IComponentOptions {
    public bindings = {
      v1: "<",
      v2: "<",
      roundTo: "<",
      improve: "@"
    }
    public controller = Controller;
    public controllerAs = "vm";
    public templateUrl = "indicators/arrow";
  }

  // popup dialog for info log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog"];
    constructor(mdDialog: ng.material.IDialogService) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("indicatorArrow", new Component());
}