﻿namespace Pineapples.Indicators {
	/**
	 * Component to support drill downs into school class levels, 
	 * for GER and NER drill downs
	 */

	// input frpom api/warehouse/enrol/list/school
	type InputData = {
		SurveyYear: number;
		Age: number;
		SchNo: string;
		"School Name": string;
		ClassLevel: string;
		Tot_F: number;
		Tot_M: number;
		Tot: number;
		Est: number;
	}

	type ValueShape = {
		[ClassLevel: string]: {
			M: number;
			F: number;
			T: number;
		}
	}

	class Controller {

		static $inject = ["Lookups", "xFilter","$state","xfHelper"]
		constructor(public lookups: Pineapples.Lookups.LookupService
			, public xFilter: Sw.xFilter.XFilter
			, public state: ng.ui.IStateService
			, private xfHelper: xfHelper		) {

		}
		public source: IEnrolData[];
		public selectedAge: number;

		// built from school enrolments
		public dimAgeSchool: CrossFilter.Dimension<IEnrolData, any>;
		public dimSchool: CrossFilter.Dimension<IEnrolData, any>;
		public grpSchool: CrossFilter.Group<IEnrolData, any, any>;


		public $onInit() {
			let xfE = this.xfHelper.createXfEnrolment(this.source);
			this.dimAgeSchool = xfE.xf.dimension(d => d.Age);
			this.grpSchool = xfE.RowColGroup("SchoolNo", "ClassLevel")
			console.log(this.grpSchool.all());
			this.filterAge();
		}

		public $onChanges(changes) {
			if (changes.selectedAge?.isFirstChange()) {
				return;
			}
			if (changes.selectedAge) {
				this.filterAge();
			}
		}

		private filterAge() {
			// usually we treat null as filterAll(), but here null is a valid key
			this.dimAgeSchool.filter(age => (this.selectedAge === undefined || age === this.selectedAge));

		}

		// drill down school table 
		// 
		public valueAccessor = (row, col, gender?) => {
			switch (col) {
				case "SchoolName":
					// if used as a column, assumes row.key is school no
					// only display on total
					if ((!gender) || gender == "T") {
						return this.lookups.byCode("schoolNames", row.key, "N");
					}
					return "";
			}
			return XfEnrolment.valueAccessor(row, col, "Enrol", gender);
		}

		public onSelection(row, grp, selectedRowKey, selectedColKey) {
			this.state.go("site.schools.list.item", { id: selectedRowKey });
		}

	}



	class Component implements ng.IComponentOptions {
		public bindings = {
			source: "<",
			classes: "<",
			genderTotals: "<",
			selectedAge: "<"
		};
		public templateUrl = "indicators/table/schoolClassTable";
		public controller = Controller;
		public controllerAs = "vm";

	}

	angular
		.module("pineapples")
		.component("schoolClassTable", new Component())

}