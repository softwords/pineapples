﻿namespace Pineapples.Indicators {

  class Controller {

    static $inject = ["$http", "Lookups"];

    constructor(public http: ng.IHttpService, public lookups: Sw.Lookups.LookupService) { }

    public year: number;

    public getSurvey() {
      this.http.get(this.surveyPath);
    }

    public get surveyPath() {
      return `api/uis/${this.year}`;
		}

    public getNationEnrolmentData() {
      this.http.get(`api/warehouse/enrol/nation?byAge&xl`);
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings = {
 
    };
    public controller = Controller;
    public controllerAs = "vm";
    public templateUrl = `indicators/uissurvey`;
  }

  angular
    .module("pineapples")
    .component("uisSurveyComponent", new Component());
}
