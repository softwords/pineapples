﻿namespace Pineapples.Indicators {


	export class xfHelper {
		static $inject = ["xFilter", "Lookups"];
		constructor(public xFilter 
			, public lookups) { }

		public createXfEnrolment(sourcedata: IEnrolData[]) {
			return new XfEnrolment(sourcedata, this.xFilter, this.lookups);
		}
	}
	angular
		.module('pineapples')
		.service('xfHelper', xfHelper);


	export type EnrolGroupValue = {
		// what is actually used here will be values from a selected field; ie may be classLevels, years, DistrictCodes
		// These will typically appear in the column headers of a crossfilterTable
		[columnAggregation: string]: {
			[genderCode: string]: {
				Enrol: number,
				Rep: number,
			}
		}
	}
	export type EnrolGroupRow = {
		key: string, // school no
		value: EnrolGroupValue
	}

	export class XfEnrolment {
		public xf: CrossFilter.CrossFilter<IEnrolData>
		constructor(sourcedata: IEnrolData[]
			, private xFilter: Sw.xFilter.IXFilter
			, private lookups: Pineapples.Lookups.LookupService		) {
			this.xf = crossfilter<IEnrolData>(sourcedata);

		}

		/**
		 * Extract a value from group created using xReduce
		 * ie returns a value from row.value[<column>][<gender>][<dataItem>]
		 * Also incorporates some ability to get data from previous year where dataItem is EnrolLY etc
		 * This requires that column is surveyYear
		 * @param row - the key value object from grp.all()
		 * @param column	the disaggregation of row.value (as specified in xReduce)
		 * @param dataItem split of gender node (ie Enrol, Rep), or calculated or redirected value (eg EnrolLY)
		 * @param gender
		 */
		public static valueAccessor = (row: EnrolGroupRow, column, dataItem, gender) => {
			let e;
			let r;
			switch (dataItem) {
				case "EnrolLY":
					return row.value?.[(column - 1).toString()]?.[(gender || "T")]?.Enrol;
					break;
				case "Enrol":
				case "EnrolTY":
					return row.value?.[(column).toString()]?.[(gender || "T")]?.Enrol;
					break;
				case "RepLY":
					return row.value?.[(column - 1).toString()]?.[(gender || "T")]?.Rep;
					break;
				case "RepTY":
				case "Rep":
					return row.value?.[(column).toString()]?.[(gender || "T")]?.["Rep"];
					break;
				case "Intake":
				case "IntakeTY":
					e = XfEnrolment.valueAccessor(row, column, "Enrol", gender);
					r = XfEnrolment.valueAccessor(row, column, "Rep", gender);
					return e - r;
					break;
				case "IntakeLY":
					e = XfEnrolment.valueAccessor(row, column, "EnrolLY", gender);
					r = XfEnrolment.valueAccessor(row, column, "RepLY", gender);
					break;
				case "RepRate":
					e = XfEnrolment.valueAccessor(row, column, "EnrolLY", gender);
					r = XfEnrolment.valueAccessor(row, column, "Rep", gender);
					return (e == 0 ? undefined : r / e);
			}
			if (dataItem) {
				if (!gender) {
					return row.value.Tot?.T?.[dataItem];
				}
				return row.value.Tot?.[gender]?.[dataItem];
			}
			if (!gender) {
				return row.value.Tot?.T;
			}
			return row.value.Tot?.[gender];

		}

		public static valueFormatter(value, row, dataItem: string, gender?: string) {
			if (value === undefined) {
				return null;
			}
			let locale = Intl.NumberFormat().resolvedOptions().locale;
			switch (dataItem) {
				case "RepRate":
					if (value == 0) {
						return null;
					}
					return parseFloat(value).toLocaleString(locale, { style: "percent", minimumFractionDigits: 2 });
			}
			return value;
		}
		public static columnTranslator = (surveyYear: number, dataItem) => {
			switch (dataItem) {
				case "Enrol":
					return `Enrolment`;
				case "Rep":
					return `Repeaters`;
				case "RepRate":
					return `Repetition Rate ${(surveyYear - 1).toString()} -> ${(surveyYear).toString()}`;
				case "EnrolLY":
					return `Enrolment ${(surveyYear - 1).toString()}`;
				case "EnrolTY":
					return `Enrolment ${(surveyYear).toString()}`;
				case "RepLY":
					return `Repeaters ${(surveyYear - 1).toString()}`;
				case "RepTY":
					return `Repeaters ${(surveyYear).toString()}`;
				case "Intake":
					return "Intake";
				case "IntakeLY":
					return `Intake ${(surveyYear - 1).toString()}`;
				case "IntakeTY":
					return `Intake ${(surveyYear).toString()}`;
				case "SchoolName":
					return null;
				default:
					return dataItem;
			}
		}
		public isText(data: any): data is string {
			return typeof data === 'string';
		};
		/**
		 * Create a crosstab group where columns are years. This can be used for year-on-year comparisons
		 * or else to present values from different years ie in calculation of repetition rate.
		 * @param dimension dimension from which to build the grup. This specifies the row keys of the group
		 */
		public YearEnrolGroup(dimension: CrossFilter.Dimension<IEnrolData, string>);
		/**
		 * 
		 * @param rowKey string value for the row key. A dimension is built from this
		 */
		public YearEnrolGroup(rowKey: string);
		public YearEnrolGroup(arg: CrossFilter.Dimension<IEnrolData, string> | string) {
			return this.RowColGroup(arg, "SurveyYear");
		}

		/**
		 * Build a group for presentation of class 'Age' x 'ClassLevel' grid
		 * @param agedimension exisitng dimension based on Age
		 * If no dimension is supplied, one is created
		 */
		public AgeClassGroup(agedimension?: CrossFilter.Dimension<IEnrolData, string>) {
			if (agedimension == undefined) {
				return this.RowColGroup("Age", "ClassLevel");
			}
			return this.RowColGroup(agedimension, "ClassLevel");
		}

		/**
		 * Build a group that crosstabulates specified 'row' and 'column' values in the input enrolment data
		 * @param dimension crossfilter dimension built on the 'row' value ie d -> d[row]
		 * @param column string name of 'column' value
		 */
		public RowColGroup(dimension: CrossFilter.Dimension<IEnrolData, string>, column: string): CrossFilter.Group<IEnrolData, string, EnrolGroupValue>;
		/**
		 * 
		 * @param rowKey string name of 'row' value. A dimension is built from this rowKey, d -> d[row], 
		 * then the group is derived from that dimension
		 * @param column string name of 'column value
		 */
		public RowColGroup(rowKey: string, column: string);
		public RowColGroup(arg: CrossFilter.Dimension<any, string> | string, column: string);
		RowColGroup(arg: CrossFilter.Dimension<IEnrolData, string> | string, column: string) {
			let dim: CrossFilter.Dimension<any, string> = (this.isText(arg)) ? this.xf.dimension(d => d[arg]) : arg;
			return this.xFilter.xReduce(dim, column
				, this.xFilter.getFlattenedGenderAccessor("GenderCode", ["Enrol", "Rep"]));
		}

		public FilterByYearofEd(minYoE, maxYoE) {
			let dimYoEd = this.xf.dimension(d => this.lookups.byCode("levels", d.ClassLevel, "YoEd"));
			dimYoEd.filterFunction(yoe => yoe <= maxYoE && yoe >= minYoE);
			return dimYoEd;
		}


		public FilterByEdLevel(edLevelCode: string) {
			return this.xf.dimension(d => d.ClassLevel)
				.filter((classLevelCode) =>
						this.lookups.byCode("levels", classLevelCode, "L") == edLevelCode);
		}
	}
	
	
}
	