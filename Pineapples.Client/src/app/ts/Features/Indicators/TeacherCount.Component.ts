﻿namespace Pineapples.Indicators {

	export type TeacherCountData = {
		SurveyYear: number,
		edLevelCode: string,
		GenderCode: string,
		NumTeachers: number,
		Qualified: number,
		Certified: number,
		QualCert: number,

		fte: number,
		fteQ: number,
		fteC: number,
		fteQC: number,

		ftpt: number,
		ftptQ: number,
		ftptC: number,
		ftptQC: number,


	}
	export type TeacherCountXfDimensions = {
		year: CrossFilter.Dimension<TeacherCountData, number>,
		edLevel: CrossFilter.Dimension<TeacherCountData, string>,

	};

	export type TeacherCountXfGroups = {
		year: CrossFilter.Group<TeacherCountData, number, any>,
		edLevel: CrossFilter.Group<TeacherCountData, string, any>
	};

	
	export class TeacherCountXf {
		public xf: CrossFilter.CrossFilter<TeacherCountData>;
		public dimensions: TeacherCountXfDimensions;
		public groups: TeacherCountXfGroups;
		public xFilter: Sw.xFilter.IXFilter;
		public teacherCountData = () => this.xf.all();

		public grpValue = (row, col, gender) => {
			let edLevel = (<string>this.dimensions.edLevel.currentFilter());
			if (!gender) {
				gender = "T";
			}
			return row.value[edLevel]?.[gender]?.[col];
		}
		public grpValueFormatter(val, row, col, gender) {
			return Math.round(val);
		}


		constructor(data: TeacherCountData[], private surveyYear: number, private edLevelCode: string) {

			this.xFilter = new Sw.xFilter.XFilter();
			let xf = crossfilter<TeacherCountData>(data);

			let dimYear = xf.dimension(d => d.SurveyYear);
			let dimEdLevel = xf.dimension(d => d.edLevelCode);


			dimYear.filter(this.surveyYear);
			dimEdLevel.filter(this.edLevelCode);
			this.dimensions = {
				year: dimYear,
				edLevel: dimEdLevel
			};

			let valueAccessor = this.xFilter.getFlattenedGenderAccessor("GenderCode", ["ftpt", "NumTeachers", "fte"]);

			this.xf = xf;
		
			this.groups = {
				year: this.xFilter.xReduce(dimYear, "edLevelCode", valueAccessor),
				edLevel: this.xFilter.xReduce(dimEdLevel, "edLevelCode", valueAccessor)
			};
		}

		private static KEY_JOIN = String.fromCharCode(1);
	}


	/**
	 * Component to support drill downs into Teacher count
	 * 
	 */
	class Controller {

		static $inject = ["Lookups", "xFilter", "$state"]
		constructor(public lookups: Pineapples.Lookups.LookupService, public xFilter: Sw.xFilter.XFilter, private $state: ng.ui.IStateService,) {

		}

		// the variables are the essence of the calculation of NER
		private indicatorCalc: Pineapples.Indicators.IndicatorCalc;

		private _surveyYear: number;
		public get surveyYear() {
			return this._surveyYear;
		}
		public set surveyYear(newValue) {
			this._surveyYear = newValue;
			if (this.teacherXf) {
				this.teacherXf.dimensions.year.filter(this.surveyYear);
			}
		}
		public edLevelCode: string;
		public tables: any[];
		public previousState: any;

		public model;

		public yearsInEdLevel: number;

		public yearsOfEd: number[];


		public kvPop: Sw.Charts.KeyValueCollection;

		public kvPopChart: Sw.Charts.AnnotatedKeyValueCollection;

		public enrol: number[];
		public nEnrol: number[];

		public popT: any;

		public enrolT: any;
		public nEnrolT: any;

		public edLevelClasses: string[];			// the classes in the education level


		public chartData: Sw.Charts.AnnotatedGroup;
		public touch;
		// interactivity

		public col_stripe = (col, index) => index % 3 == 1 ? "stripe" : "";
		public row_pairstripe = (row, col, index) => index % 3 == 1 ? "stripe" : "";

		public teacherXf: TeacherCountXf;

		public $onChanges(changes) {


		}

		public $onInit() {

			//if (this.tables) {
			//	// data has been retrieved by the ui-router as a Resolve
			//	// and passed to the dashboard as a binding - process it into a crossfilter
			//	// create the crossfilter from the warehouse table
			//	this.teacherXf = new TeacherCountXf(this.tables[0], this.surveyYear, this.edLevelCode);
			//}
			let coll = this.indicatorCalc.collectionBase("Teachers > EdLevel"
				, { edLevelCode: this.edLevelCode }
				, ["year", "edLevelCode"]
				, ["teachers", "ftpt", "fte"]
				, "MF"
			);
			let keyMappings = {
				year: "SurveyYear",
				teachers: "NumTeachers",
				genderCode: "GenderCode"
			}
			coll = coll.map(row => _.mapKeys(row, (value, key) => keyMappings[key] || key));
			this.teacherXf = new TeacherCountXf(coll, this.surveyYear, this.edLevelCode);

			let ix: IndicatorCalc = this.indicatorCalc;

			// enrolment totals for the education level - from the vermpaf - 
			// can crosscheck against grpTotal, grpTotalA
			this.enrolT = {
				M: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "M"),
				F: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "F"),
				T: ix.edLevelEnrol(this.surveyYear, this.edLevelCode, "")
			}
			// official age (net) enrolments from vermpaf - check against grpTotalOA
			this.nEnrolT = {
				M: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "M"),
				F: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "F"),
				T: ix.edLevelNetEnrol(this.surveyYear, this.edLevelCode, "")
			}
			// number of years in the education level is sourced from the vermpaf
			this.yearsInEdLevel = ix.edLevelHeader(this.surveyYear, this.edLevelCode, "numYears")

			this.kvPop = [];

			// make an array of these
			let e: Array<any> = [];

			let p: Array<any> = [];
			let dt: Array<any> = [];

			this.kvPopChart = {
				keyValues: this.kvPop,
				columns: ["M", "F"],
				valueAccessor: (kv, col) => kv.value.pop[col]
			}
			this.model = {
				data: dt,
				total: null
			};

			let total = {
				key: "Total",
				value: { enrol: null, pop: null }
			}

			total.value.enrol = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.enrol[p] : e.value.enrol[p];
				});
				return tot;
			}, {})
			total.value.pop = _.reduce(this.model.data, (tot, e) => {
				['T', 'M', 'F'].forEach(p => {
					tot[p] = tot[p] ? tot[p] + e.value.pop[p] : e.value.pop[p];
				});
				return tot;
			}, {})

			this.model.total = [total];
		}

		public valueAccessor(row, col, gender?) {
			if (gender) {
				return row.value[col]?.[gender];
			}
			return row.value[col]?.T;
		}

		public rowAccessor(row, source) {
			if (row.key == "Total") {
				return source.name;
			}
			return row.key;
		}

		public hideZeros(row, grp) {
			return this.valueAccessor(row, "Tot") > 0;
		}

		public onSelection(row, grp, selectedRowKey, selectedColKey) {
			// to do?
			this.touch = Date.now();
		}
	}

	class Component implements ng.IComponentOptions {
		public bindings = {
			surveyYear: "<",
			edLevelCode: "<",
			districtCode:"<",
			indicatorCalc: "<",
			tables: "<"
		};
		public templateUrl = "indicators/drill/teacher";
		public controller = Controller;
		public controllerAs = "vm";

	}

	angular
		.module("pineapples")
		.component("indicatorsTeacherExplore", new Component())
}