﻿// Teachers Routes
namespace Pineappples.Kobo {

	let RouteHelper = Sw.Utils.RouteHelper;

	let routes = function ($stateProvider) {
		//var mapview = 'TeacherMapView';

		// root state for 'teachers' feature
		let state: ng.ui.IState;
		let statename: string;

		// base kobo state
		state = {
			url: "^/kobo",
			resolve: {
				featureTitle: RouteHelper.featureTitle("SchoolInspections")
			},
			abstract: true
		};
		$stateProvider.state("site.kobo", state);

		// upload state
		state = {
			url: "^/kobo/upload",

			views: {
				"@": {
					component: "koboUploadComponent"
				},
			},
			resolve: {
				pageTitle: RouteHelper.simpleTitle("Load from Kobo")
			}
		};
		$stateProvider.state("site.kobo.upload", state);

		// -- allow view of an upload inspection from within the uploader
		// -- this state is copied from schoolinspections.list.item
		// ----------------- item state
		state = {
			url: "^/kobo/upload/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.kobo.upload": {
					component: "componentSchoolInspection"
				}
			},
			resolve: {
				model: ['schoolInspectionsAPI', '$stateParams', (api, $stateParams) => api.read($stateParams.id)],
				pageTitle: ['model', 'rootPageTitle', (model, title) => `${title} * ${model.InspectionType}: ${model.schName}`]

			}
		};
		$stateProvider.state("site.kobo.upload.item", state);

		// reload state for testing
		state = {
			url: '^/kobo/reload',
			onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
				$templateCache.remove("kobo/upload");
				$state.go("site.kobo.upload");
			}]
		};
		statename = "site.kobo.reload";
		$stateProvider.state(statename, state);

	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}