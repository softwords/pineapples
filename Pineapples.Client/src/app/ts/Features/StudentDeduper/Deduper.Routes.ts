﻿// Students Routes
namespace Pineappples.Students {

	let routes = function ($stateProvider) {
		var featurename = 'StudentDeduper';
		var filtername = 'StudentDeduperFilter';
		var templatepath = "studentdeduper";
		var tableOptions = "studentFieldOptions";
		var url = "studentdeduper";
		var usersettings = null;

		// root state for 'students' feature
		let state: ng.ui.IState = Sw.Utils.RouteHelper.frameState(featurename, filtername, templatepath, url, usersettings, tableOptions);

		// default 'api' in this feature is studentsAPI
		state.resolve = state.resolve || {};
		state.resolve["api"] = "studentDeduperAPI";
		state.data = state.data || {};
		state.data["frameTitle"] = "Student Deduplicate";
		state.data["permissions"] = {
			only: 'StudentOps'
		};

		let basestate = "site.studentdeduper";
		$stateProvider.state(basestate, state);

		// takes a list state
		// doesn't need monitor?
		state = Sw.Utils.RouteHelper.frameListState("student", "stuID");
		let statename = `${basestate}.list`;
		state.url = `^/${url}/list`;

		$stateProvider.state(statename, state);

		state = {
			url: '^/studentdeduper/reload',
			onEnter: ["$state", "$templateCache", function ($state: ng.ui.IStateService, $templateCache) {
				$templateCache.remove("student/item");
				$templateCache.remove("student/searcher");
				$state.go($state.current);
			}]
		};
		statename = "site.studentdeduper.reload";
		$stateProvider.state(statename, state);

		// poup view of student is cloned from students.list.item
		state = {
			url: "^/studentdeduper/list/student/{id}",
			views: {
				"actionpane@site.studentdeduper": "componentStudent"
			}
		};
		state = Sw.Utils.RouteHelper.clone($stateProvider, "site.students.list.item", state);
		$stateProvider.state("site.studentdeduper.list.student", state);

		// poup view of comparitve surveys
		state = {
			url: "^/studentdeduper/list/surveys/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.studentdeduper": "studentDeduperSurveys"
			},
			resolve: {
				surveys: ["studentDeduperAPI", "$stateParams", (api, $stateParams: ng.ui.IStateParamsService) => {
					let ids = (<string>$stateParams.id).split("_");
					return api.surveys(ids[0], ids[1]).then(
						response => (<any>response.ResultSet)
					);
				}]
			}
		};

		$stateProvider.state("site.studentdeduper.list.surveys", state);

		// poup window to do the data merge and save
		state = {
			url: "^/studentdeduper/list/merge/{id}",
			params: { id: null, columnField: null, rowData: {} },
			views: {
				"actionpane@site.studentdeduper": "studentDeduperMerge"
			},
			resolve: {

				// use the existing interfaces to read
				model: ['studentsAPI', '$stateParams', (api, $stateParams: ng.ui.IStateParamsService) => {
					let ids = (<string>$stateParams.id).split("_");
					return api.read(ids[0]);
				}],
				dup: ['studentsAPI', '$stateParams', (api, $stateParams: ng.ui.IStateParamsService) => {
					let ids = (<string>$stateParams.id).split("_");
					return api.read(ids[1]);
				}]
			}
		};

		$stateProvider.state("site.studentdeduper.list.merge", state);
	}

	angular
		.module('pineapples')
		.config(['$stateProvider', routes])

}