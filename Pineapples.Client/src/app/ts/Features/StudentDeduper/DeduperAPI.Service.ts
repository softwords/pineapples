﻿namespace Pineapples.Api {
	const ENTITY = "studentdeduper";
	export class studentDeduperApi {

		static $inject = ["$q", "$http", "Restangular"]
		constructor(public $q: ng.IQService
			, public http: ng.IHttpService
			, public restangular: restangular.IService) {
		}

		//public read(id) {
		//	return this.restangular.one(ENTITY, id).get();
		//}
	
		public filterPaged(fltr) {
			return this.http.post(`api/${ENTITY}/collection/filter`, fltr)
				.then(response => (response.data));
		}

		public surveys(id1: number, id2: number) {
			return this.http.get(`api/${ENTITY}/surveys/${id1}/${id2}`)
				.then(response => (response.data));
		}

		public merge(target: number, source: number) {
			return this.http.get(`api/${ENTITY}/merge/${target}/${source}`)
				.then(response => (response.data));
		}

	}

	angular.module("pineapplesAPI")
		.service("studentDeduperAPI", studentDeduperApi);
}