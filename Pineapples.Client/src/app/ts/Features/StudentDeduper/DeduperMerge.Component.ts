﻿namespace Pineapples.Students {

	class Controller extends Sw.Component.ComponentEditController {
		public model: Student;
		public primary: Student;
		public dup: Student;

		// holds booleans showing which propertiese are copied from dup
		public copy: {}

		static $inject = ["ApiUi","studentDeduperAPI", "$mdDialog","$state","Lookups"];
		constructor(apiUi: Sw.Api.IApiUi, public api: Pineapples.Api.studentDeduperApi
			, public mdDialog: ng.material.IDialogService
			, public $state: ng.ui.IStateService
			, public lookups: Pineapples.Lookups.LookupService) {
			super(apiUi);
		}

		public $onInit() {
			// copy the primary into the working model
			this.primary = angular.copy( (<any>this.model).plain());
			this.fillAllBlanks();
			this.setEditing(true);
		}

		public allProps = {
			stuNamePrefix: "Title",
			stuGiven: "Given Name",
			stuMiddleNames: "Middle Names",
			stuFamilyName: "Family Name",
			stuNameSuffix: "Name Suffix",
			stuGender: "Gender",
			stuDoB: "DoB",
			stuCardID: "Student Card ID",

		}


		public nameProps = {
			stuNamePrefix: "Title",
			stuGiven: "Given Name",
			stuMiddleNames: "Middle Names",
			stuFamilyName: "Family Name",
			stuNameSuffix: "Name Suffix"
		}

		public tnumberProps = {
			stuCardID: "Student Card ID"
		}

		public fillAllBlanks() {
			Object.keys(this.allProps).forEach(propname => { this.fillBlank(propname); });
		}

		public fillBlank(propname) {
			if (this.primary[propname] == null && this.dup[propname]) {
				this.copy[propname] = true;
				this.primary[propname] = this.dup[propname];
			}
		}

		public Merge() {
			// save the model in the usual way (restangular) 
			// then run the merge dedup.
			this.save();
		}

		public onModelUpdated(newData) {
			// we have saved so let's dedup
			this.api.merge(this.primary._id(), this.dup._id())
				.then(response => {
					var confirm = this.mdDialog.confirm()
						.title('Merge Complete')
						.textContent('Display the combined record?')
						.ariaLabel('Merge')
						.ok('OK')
						.cancel('Cancel');

					this.mdDialog.show(confirm).then( () => {
						this.$state.go("site.studentdeduper.list.student", { id: this.model._id() });
					});
				})
		}

		// put a confirm dialog in here so we know what is going to happen
		public save() {
				// Appending dialog to document.body to cover sidenav in docs app
				var confirm = this.mdDialog.confirm()
					.title('Merge Student Records')
					.textContent('This action will save your changes and merge the student records.')
					.ariaLabel('Merge')
					.ok('Continue to Merge')
					.cancel('Cancel');

				this.mdDialog.show(confirm).then(() => {
					super.save();
				});
		}

	}
	class Component implements ng.IComponentOptions {
		public bindings = {
			model: "<",
			dup: "<"
		};
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "studentdeduper/merge";
	}

	angular
		.module("pineapples")
		.component("studentDeduperMerge", new Component());
}