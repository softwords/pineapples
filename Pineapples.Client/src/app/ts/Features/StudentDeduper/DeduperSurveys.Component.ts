﻿namespace Pineapples.Students {

	class Controller {
		public surveys: any[];
		public listdata;
		public leftID;
		public rightID;

		public $onInit() {
			// get the earliest and latest year represented
			let minYear = _.minBy(this.surveys, r => r.stueYear).stueYear;
			let maxYear = _.maxBy(this.surveys, r => r.stueYear).stueYear;

			let left = _.minBy(this.surveys, r => r.stuID).stuID;
			let right = _.maxBy(this.surveys, r => r.stuID).stuID;
			if (right == left) {
				right == null;
			}

			let lst = {};
			for (var j = minYear; j <= maxYear; j++) {
				lst[j] = { left: null, right: null };
			}
			this.surveys.forEach(svy => {
				if (svy.stuID == left) {
					lst[svy.stueYear].left = svy;
				} else if (svy.stuID == right) {
					lst[svy.stueYear].right = svy;
				}

			});
			this.listdata = lst;
			this.leftID = left;
			this.rightID = right;
		}


	}
	class Component implements ng.IComponentOptions {
		public bindings = {
			surveys: "<"
		};
		public controller = Controller;
		public controllerAs = "vm";
		public templateUrl = "studentdeduper/surveys";
	}

	angular
		.module("pineapples")
		.component("studentDeduperSurveys", new Component());
}