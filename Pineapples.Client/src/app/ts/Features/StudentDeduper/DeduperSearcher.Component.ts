﻿namespace Pineapples.Students {


	class Controller extends Sw.Components.FindConfigOperations {

		public cycleOption(optionName) {
			switch (this.params[optionName]) {
				case 1:
					this.params[optionName] = 0
					break;
				case 0:
					this.params[optionName] = null;		// <> = null
					break;
				default:
					this.params[optionName] = 1
			}
		}
		// allow the option 2 in the sequence, to indicate a loose match
		public cycleFuzzyOption(optionName) {
			switch (this.params[optionName]) {
				case 1:
					this.params[optionName] = 2
					break;
				case 2:
					this.params[optionName] = 0;		// <> = null
					break;
				case 0:
					this.params[optionName] = null;		// <> = null
					break;
				default:
					this.params[optionName] = 1
			}
		}
		public buttonClass(matchOption) {
			switch (matchOption) {
				case 1:
					return "md-accent";
				case 0:
					return "md-warn";
				case 2:
					return "md-accent md-hue-1";
				default:
					return "md-primary";
			}
		}

		public buttonTooltip(matchOption) {
			switch (matchOption) {
				case 1:
					return `Matching`;
				case 0:
					return `Different`;
				case 2:
					return `Fuzzy match on`;
				default:
					return `Ignore`;
			}
		}

		public showOption(param) {
			switch (param) {
				case 0:
					return "<>";
				case 1:
					return "=";
				case 2:
					return "?";
			}
			return null;
		}

	}

  angular
    .module("pineapples")
		.component("studentdeduperSearcherComponent"
			, new Sw.Components.FindConfigComponent("studentdeduper/SearcherComponent", Controller));
}