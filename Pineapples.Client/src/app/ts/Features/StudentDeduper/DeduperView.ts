﻿namespace Pineapples.Students {
	let viewDefaults = {
		columnDefs: [
			//{
			//  field: 'tID',
			//  name: 'Teacher ID',
			//  displayName: 'Teacher ID',
			//  editable: false,
			//  pinnedLeft: true,
			//  width: 90,
			//  cellTemplate: Sw.Utils.UiGridUtils.actionColumn("item", "vm")
			//},
			{
				field: 'tID',
				name: 'colID',
				displayName: 'ID',
				pinnedLeft: true,
				width: 60,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div><a  ng-click="grid.appScope.vm.action('student','stuID', row.entity);">{{row.entity.stuID}}</a></div>
            <div><a  ng-click="grid.appScope.vm.action('student','dupID', row.entity);">{{row.entity.dupID}}</a></div>
           </a></div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'stuFamilyName',
				name: 'colFamilyName',
				displayName: 'Family Name',
				pinnedLeft: true,
				width: 120,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.stuFamilyName}}</div>
            <div>{{row.entity.dupFamilyName}}</div>
           </div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'stuGiven',
				name: 'colGiven',
				displayName: 'Given Name',
				pinnedLeft: true,
				width: 120,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.stuGiven}}</div>
            <div>{{row.entity.dupGiven}}</div>
           </div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				name: 'svyButton',
				displayName: '',
				pinnedLeft: true,
				width: 80,
				cellTemplate:
					`<div  layout="row" layout-align="center center" class="ui-grid-cell-contents gdAction">
							<md-icon ng-click="grid.appScope.vm.action('surveys',['stuID', 'dupID'], row.entity);">
								assignment_turned_in
								<md-tooltip>Compare surveys</md-tooltip>
							</md-icon>
							<md-icon ng-click="grid.appScope.vm.action('merge',['stuID', 'dupID'], row.entity);">
								call_merge
								<md-tooltip>Merge these two records</md-tooltip>
							</md-button>

           </div>`,
				enableSorting: false
			},
			{
				field: 'stuGender',
				name: 'colGender',
				displayName: 'Gender',
				pinnedLeft: true,
				width: 60,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div>{{row.entity.stuGender}}</div>
            <div>{{row.entity.dupGender}}</div>
           </div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]
			},
			{
				field: 'stuDoB',
				name: 'colDoB',
				displayName: 'DoB',
				pinnedLeft: true,
				width: 100,
				cellTemplate:
					`<div class="ui-grid-cell-contents gdAction">
            <div >{{row.entity.stuDoB|date:'d-MMM-yyyy'}}</div>
            <div>{{row.entity.dupDoB|date:'d-MMM-yyyy'}}</div>
           </div>`,
				enableSorting: true,
				sortDirectionCycle: ["asc", "desc"]

			}
		]
	};

	let modes = [
		{
			key: "Student Info",
			columnSet: 0,
			gridOptions: {
				rowHeight: 60,
				columnDefs: [
					{
						field: 'stuCardID',
						name: 'colCardID',
						displayName: 'Student Card ID',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 160,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
								<div>{{row.entity.stuCardID}}</div>
								<div>{{row.entity.dupCardID}}</div>
							</div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					},
					{
						field: 'stuEthnicity',
						name: 'colEthnicity',
						displayName: 'Ethnicity',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 160,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
								<div>{{row.entity.stuEthnicity|lookup:"ethnicities"}}</div>
								<div>{{row.entity.dupEthnicity|lookup:"ethnicities"}}</div>
							</div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					}
				]

			}       // gridoptions
		},          //mode
		{
			key: "Enrollments",
			columnSet: 1,
			gridOptions: {
				rowHeight: 60,
				columnDefs: [
					{
						field: 'stuMinSchool',
						name: 'colMinSchNo',
						displayName: 'School(s)',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 250,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
            <div>
							<span>{{row.entity.stuMinSchNo}}
									<md-tooltip>{{row.entity.stuMinSchNo|lookup:"schoolNames"}}</md-tooltip>
							</span>
							<span ng-show="row.entity.stuMinSchNo != row.entity.stuMaxSchNo">, {{row.entity.stuMaxSchNo}}
									<md-tooltip>{{row.entity.stuMaxSchNo|lookup:"schoolNames"}}</md-tooltip>
							</span>
						</div>
            <div>
							<span>{{row.entity.dupMinSchNo}}
									<md-tooltip>{{row.entity.dupMinSchNo|lookup:"schoolNames"}} </md-tooltip>
							</span>
							<span ng-show="row.entity.dupMinSchNo != row.entity.dupMaxSchNo">, {{row.entity.dupMaxSchNo}}
								<md-tooltip>{{row.entity.dupMaxSchNo|lookup:"schoolNames"}} </md-tooltip>
							</span>
						</div>
           </div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					},
					{
						field: 'stuMinYear',
						name: 'colminYear',
						displayName: 'Year(s)',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 120,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
								<div>{{row.entity.stuMinYear}}
									<span ng-show="row.entity.stuMinYear != row.entity.stuMaxYear"> - {{row.entity.stuMaxYear}}</span>
								</div>
								<div>{{row.entity.dupMinYear}}
									<span ng-show="row.entity.dupMinYear != row.entity.dupMaxYear"> - {{row.entity.dupMaxYear}}</span>
								</div>
							</div>`,
						enableSorting: false
					},
					{
						field: 'stuMinClassLevel',
						name: 'colminClassLevel',
						displayName: 'Grade(s)',
						headerCellFilter: 'vocab',
						pinnedLeft: false,
						width: 160,
						cellTemplate:
							`<div class="ui-grid-cell-contents gdAction">
								<div>{{row.entity.stuMinClassLevel|lookup:"levels"}}
									<span ng-show="row.entity.stuMinClassLevel != row.entity.stuMaxClassLevel">
										- {{row.entity.stuMaxClassLevel|lookup:"levels"}}
									</span>
								</div>
								<div>{{row.entity.dupMinClassLevel|lookup:"levels"}}
									<span ng-show="row.entity.dupMinClassLevel != row.entity.dupMaxClassLevel">
									- {{row.entity.dupMaxClassLevel|lookup:"levels"}}
									</span>
								</div>
							</div>`,
						enableSorting: false
					},
					{
						name: 'conflicts',
						displayName: '',
						pinnedLeft: false,
						width: 20,
						cellTemplate:
							`<div  layout="row" layout-align="center center" class="ui-grid-cell-contents gdAction">
									{{row.entity.countConflictYear|truefalse:"!"}}
           </div>`,
						enableSorting: true,
						sortDirectionCycle: ["asc", "desc"]
					}
				]
			}					// gridoptions
		}						// enrollment view mode
	];              // modes

	let pushModes = (filter: Sw.Filter.IFilter) => {
		filter.ViewDefaults = viewDefaults;
		filter.PushViewModes(modes);
	};


	angular
		.module('pineapples')
		.run(['StudentDeduperFilter', pushModes]);
}
