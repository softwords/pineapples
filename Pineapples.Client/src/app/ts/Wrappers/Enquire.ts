﻿/* service to return Enquire as a service, and thus avaiable in Angular code
Note this is a rare instance where we use factory, rather than service, in typescript angular
This is becuase the object we wish to use as a service already exists
*/
namespace Pineapples {
  let factory = (window) => window.enquire;
  (<any>factory).$inject = ["$window"];       // test demonstrating this alternative way of injecting dependencies

  angular
    .module("pineapples")
    .factory("enquire", factory);

}