﻿namespace Pineapples {

	class Interceptor implements ng.IHttpInterceptor {

		constructor(private _q: ng.IQService, private _rootscope: ng.IRootScopeService 
			, private fileSaver: any) { }

		// turns out to be complicated to get xlsx downloaded and correctly decoded
		//https://stackoverflow.com/questions/34993292/how-to-save-xlsx-data-to-file-as-a-blob
		// approach is to base64encode as a string on the server, then decode that on the client
		private s2ab = (s) => {
			var buf = new ArrayBuffer(s.length);
			var view = new Uint8Array(buf);
			for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
			return buf;
		}

		public response = (data) => {
			// shows how to get the default name sent by the server, and the mime type
			if (data.headers("Content-Disposition") == undefined) {
				return this._q.when(data); // wrap in a promise
			}
			let mimetype = data.headers("Content-Type");
			let filename: string = data.headers("Content-Disposition");
			// http://stackoverflow.com/questions/23054475/javascript-regex-for-extracting-filename-from-content-disposition-header/23054920
			let fileregex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
			// this pattern will return the name (possibly enclosed in quotes) as capture group 1
			// and any quoting char as capture group 2
			let regResult: RegExpExecArray = fileregex.exec(filename);
			// the resulting array has the input string at [0], then the 'match groups'
			filename = regResult[1];
			if (regResult.length === 3) {
				// remove the quote
				filename = filename.replace(regResult[2], "");
				filename = filename.replace(regResult[2], "");      // 
			}
			let blob;
			switch (mimetype) {
				case "binary/base64encoded":			// a custom token to trigger this preprocessing
					var ab = this.s2ab(atob(data.data)); // from example above
					blob = new Blob([ab], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
					break;
				default:
					blob = new Blob([data.data], { type: mimetype });
					break;
			}
			// this seems the preferred way to get the link to open in a new window, 
			// but the user would have to change their popupblocker settings for this to work....
			// don't bother for now - open from the command line
			//let fileURL = URL.createObjectURL(blob);
			//var link = document.createElement('a');
			//link.href = fileURL;
			//link.target = "_blank";
			//link.click();
			//setTimeout(() => {
			//  // For Firefox it is necessary to delay revoking the ObjectURL
			//  window.URL.revokeObjectURL(fileURL);
			//}, 100);
			this.fileSaver.saveAs(blob, filename);
			return data; // don't wrap in a promise - it's been dealt with
		}

		// the same strategy to construct a "factory method" that is used for typescript directives
		public static factory() {
			let interceptor = ($q, $rootScope, fileSaver) => {
				return new Interceptor($q, $rootScope, fileSaver);
			};
			interceptor["$inject"] = ["$q", "$rootScope", "FileSaver"];
			return interceptor;
		}
	}

	angular
			.module("sw.common")
	    .config(["$httpProvider", function (provider:ng.IHttpProvider) {
				provider.interceptors.push(<any>Interceptor.factory());
			}])
}