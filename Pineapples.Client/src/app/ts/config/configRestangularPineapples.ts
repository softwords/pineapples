﻿module Pineapples.Api {

	// ----------------------
	// Restangular config
	// ------------------
	// the base Url 'api' confirms to the web api convention that web api calls
	// are routed under 'api'
	// This allows web api calls to be distinguished from mvc calls

	function restangularConfig(restangularProvider: restangular.IProvider
		, $rootScope: Sw.IRootScopeEx) {
		restangularProvider.setBaseUrl("api");
		restangularProvider.setDefaultHeaders({ 'Content-Type': 'application/json' });

		// the save interceptor
		let interceptorBeforeSave = (element: any, operation: string, route: string, url: string) => {
			// if the object has a _beforeSave method, invoke it
			if (element && element._beforeSave)
				return element._beforeSave();

			return element;
		}

		let errorInterceptor = (response, deferred, responseHandler) => {
			switch (response.status) {
				// errors are caught lower down (http Interceptor for 401)
				// or higher up (process in ApiUi for client action)
				// possibly don;t need anything here?
			}
			return true;
		};

		// 
		let onElemR = (changedElem, isCollection, route, restangular: restangular.IService) => {
			if (Array.isArray(changedElem) && !isCollection) {
				// changedElem has been 'restangularized'
				return restangular.restangularizeCollection(null, changedElem, (<any>changedElem).route);
			}
			return changedElem;
		};
		restangularProvider.addRequestInterceptor(interceptorBeforeSave);

		//restangularProvider.addResponseInterceptor(interceptorPaged);

		restangularProvider.setOnElemRestangularized(onElemR);
		restangularProvider.setErrorInterceptor(<any>errorInterceptor);

		// point to the row version field as the default etag
		restangularProvider.setRestangularFields({ etag: "_rowversion" });
	};

	angular
		.module('pineapplesAPI')
		.config(['RestangularProvider', restangularConfig]);

	// ----------------------
	// Restangular config at run - the restangular service is available
	// ------------------

	function restangularRunConfig(restangular
		, $state: ng.ui.IStateService
		, $injector: ng.auto.IInjectorService
		, $rootScope: Sw.IRootScopeEx) {

		// specify the id field for each type of entity
		// this is how restabgular knows how to construct a rest end point for reading/writing that entity
		restangular.configuration.getIdFromElem = (elem) => {

			let id = null;
			if (elem._id) {
				// if element supports the standard IEditable interface it will have this
				return elem._id();
			}
			if (elem.hasOwnProperty("id")) {
				id = elem["id"];
			}
			if (elem.hasOwnProperty("ID") && id == null) {
				// should we change this in lookup queries?
				id = elem["ID"];
			}
			if (elem.hasOwnProperty("Id") && id == null) {
				// should we change this in lookup queries?
				id = elem["Id"];
			}
			if (elem.hasOwnProperty("C") && id == null) {
				// used by lookup lists that have a string key
				id = elem["C"];
			}
			if (elem.hasOwnProperty("Code") && id == null) {
				// used by most lookup tables that have a string key
				id = elem["Code"];
			}

			if (id == null) {
				let idField = "id";

				switch (elem.route) {
					case "schools":
						idField = "schNo";
						break;
					case "teachers":
						idField = "tID";
						break;
					case "books":
						idField = "bkCode";
						break;
					case "quarterlyreports":
					case "quarterlyreportsx":
						idField = "qrID";
						break;
					case "schoolaccreditations":
						idField = "saID";
						break;
					case "censusschooltypes":
						idField = "stCode";
						break;
					case "censusteacherquals":
						idField = "ytqID";
						break;

					case "students":
						idField = "stuID";
						break;
					case "exams":
						idField = "exID";
						break;
					case "teacherexams":
						idField = "texID";
						break;
					case "teacherlinks":
					case "schoollinks":
					case "studentlinks":
					case "documentlinks":
						idField = "lnkID";
						break;
				};

				if (elem.hasOwnProperty(idField)) {
					id = elem[idField];
				};
			}
			// this is to get around the issue that if the saved string code contains a .
			// then web api won't see it as an "extensionless Url"
			// ie api/people/BRIAN.LEWIS will fail
			// api/people/BRIAN.LEWIS/ will succeed
			if (id && id.indexOf && id.indexOf(".") >= 0) {
				id = id + "/";
			}
			return id;
		};

		/**
		 * Instantiate an object fromm a json payload
		 * The type of the objeft may be indentified from the payload in two ways:
		 * 1) ( old way) there is a Tag property in the data, and the object itselect is in data.Resultset
		 * 2) (newer way) objects returned from EF may not have this IDataResult wrapping
		 * in this case Tag is delevered as a response header 'tag'
		 * @param data
		 * @param operation
		 * @param what
		 * @param url
		 * @param response
		 * @param deferred
		 */
		let interceptorTagged = (data: any, operation: string, what: string, url: string, response: restangular.IResponse, deferred) => {
			let tag = ((data?.Tag) ? data.Tag : response.headers("X-Tag") || response.headers("tag"));
			switch (tag) {
				case "pupiltable":
					return new Pineapples.PupilTable(data.ResultSet[0][0],
						data.ResultSet[1],
						data.ResultSet[2],
						data.ResultSet[3]
					);
				case "exam":
					// favour direct use of constructor now over static 'create' method?
					return Pineapples.Exams.Exam.create(data.ResultSet, $injector);
				case "teacherexam":
					// favour direct use of constructor now over static 'create' method?
					return Pineapples.TeacherExams.TeacherExam.create(data.ResultSet, $injector);
				case "school":
					return Pineapples.Schools.School.create(data.ResultSet);
				case "resourcelist":
					return new Pineapples.Survey.ResourceList(data.ResultSet); // TODO: refactor like this one
				case "teacher":
					// favour direct use of constructor now over static 'create' method?
					return Pineapples.Teachers.Teacher.create(data.ResultSet);
				case "student":
					return Pineapples.Students.Student.create(data.ResultSet);
				case "scholarship":
					return Pineapples.Scholarships.Scholarship.create(data.ResultSet);
				case "scholarshipstudy":
					return Pineapples.Scholarships.ScholarshipStudy.create(data);


				case "book":
					return Pineapples.Books.Book.create(data.ResultSet);
				case "schoolinspection":
					// this factory method may create a particular kind of inspection object 
					// eg; schoolaccreditation or wash
					return Pineapples.SchoolInspections.SchoolInspection.create(data.ResultSet);
				case "quarterlyreportx":
					return Pineapples.QuarterlyReportsX.QuarterlyReportX.create(data.ResultSet);
				case "quarterlyreport":
					return Pineapples.QuarterlyReports.QuarterlyReport.create(data.ResultSet);
				case "schoolaccreditation":
					return Pineapples.SchoolAccreditations.SchoolAccreditation.create(data.ResultSet);
				case "auditlog":
					return Pineapples.Survey.AuditLog.create(data.ResultSet);
				case "perfassess":
					return Pineapples.PerfAssess.PerfAssess.create(data.ResultSet);
				case "user":
					return Sw.Auth.User.create(data.ResultSet, $injector);
				case "findata":
					return Pineapples.FinData.FinData.create(data.ResultSet || data);
				case "schoollink":
					return Pineapples.Schools.SchoolLink.create(data.ResultSet);
				case "teacherlink":
					return Pineapples.Teachers.TeacherLink.create(data.ResultSet);
				case "studentlink":
					return Pineapples.Students.StudentLink.create(data.ResultSet);
				case "census":
					return Pineapples.CensusAdmin.Census.create(data.ResultSet, $injector);

			};
			return data;
		};

		/**
		 * Restangular interceptor to broadcast changes in data
		 * Classes deriving from ListMonitor (MonitoredList.Component.ts)
		 * receive these notifications and implement handlers to action them
		 * Typically, these add a new item to a collection (dataInserted)
		 * or update an element of a collection with new field values (dataUpdated)
		 * See restangular documentation for more on parameters
		 * 
		 * @param data the new data
		 * @param operation restangular operation. Same as http operation except for remove
		 * @param what object type according to the restangular url
		 * @param url
		 * @param response the entire reponse object
		 * @param deferred deferred that owns the promise being resolved
		 */
		let interceptorDataUpdate = (data: any, operation: string, what: string, url: string, response: restangular.IResponse, deferred) => {
			if (response.status === 200) {
				if (operation == "put") {
					//$rootScope.$broadcast("dataUpdated", what, data);
					$rootScope.hub.publish("dataUpdated", { what: what, data: data });
				} else if (operation == "remove") {           // this is restangular's operation not the http method
					//$rootScope.$broadcast("dataUpdated", what, data);
					$rootScope.hub.publish("dataDeleted", { what: what, data: data });
				} else if (operation == "post") {
					$rootScope.hub.publish("dataInserted", { what: what, data: data });
				};
			}
			return data;
		};

		restangular.addResponseInterceptor(interceptorTagged);
		restangular.addResponseInterceptor(interceptorDataUpdate);

	};

	restangularRunConfig.$inject = ["Restangular", "$state", "$injector", "$rootScope"];

	// move the interceptorPaged from Restangular to http
	// this allows clients to call paged lists using http if restangularisation is not required
	// ( which it often is not)

	let config = ($httpProvider) => {
		let interceptorPaged = (data) => {
			if (isPagedDataResult(data)) {
				return notateResponse<any>(data);
			};
			return data;
		};

		$httpProvider.defaults.transformResponse.push(interceptorPaged);
	}

	angular
		.module("pineapplesAPI")
		.config(["$httpProvider", config])
		.run(restangularRunConfig);

}
