﻿namespace Pineapples {

	class Interceptor implements ng.IHttpInterceptor {

		constructor(private _q: ng.IQService) { }

		public response = (response) => {
			// shows how to get the default name sent by the server, and the mime type
			if (response.headers("X-Deprecated")) {
				console.warn(`WARNING: ${response.config.url} is Deprecated - use ${response.headers("X-Deprecated")}`);
			}
			return response;
		}

		public responseError = (response) => {
			// shows how to get the default name sent by the server, and the mime type
			if (response.headers("X-Deprecated")) {
				console.error(`ERROR: ${response.config.url} is Deprecated - use ${response.headers("X-Deprecated")}`);
			}
			return this._q.reject(response);
		}

		// the same strategy to construct a "factory method" that is used for typescript directives
		public static factory() {
			let interceptor = ($q) => {
				return new Interceptor($q);
			};
			interceptor["$inject"] = ["$q"];
			return interceptor;
		}
	}

	angular
			.module("sw.common")
	    .config(["$httpProvider", function (provider:ng.IHttpProvider) {
				provider.interceptors.push(<any>Interceptor.factory());
			}])
}
