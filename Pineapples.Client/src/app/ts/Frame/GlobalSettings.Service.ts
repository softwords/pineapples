﻿namespace Pineapples {

  export class GlobalSettingsService {
    public settings = {
      navbarCollapsed: false,
      navbarShown: false,
      fullscreen: false,
      smallscreen: false
    };

    static $inject = ['$rootScope', '$document', '$timeout'];
    constructor(public rootScope: ng.IRootScopeService
      , public document
      , public timeout: ng.ITimeoutService) { }

    public get(key) {
      return this.settings[key];
    };
    public set(key, value) {
      this.settings[key] = value;
      this.rootScope.$broadcast('globalStyles:changed', { key: key, value: this.settings[key] });
      this.rootScope.$broadcast('globalStyles:changed:' + key, this.settings[key]);
      this.timeout(); // try to force an asynchronous digest so that layout can be recalculated
    }

    public toggle(key) {
      this.settings[key] = !this.settings[key];
    }

    public values() { return this.settings; };
  }

  let fcn = (document, timeout, enquire, global: GlobalSettingsService) => {
    document.ready(() => {
      enquire.register("screen and (max-width: 959px)", {
        match: function () {
          global.set("smallscreen", true);
          timeout(() => {
            global.set('navbarShown', false);    // the md-savenav is open
            global.set('navbarCollapsed', false);
          });
        },
        unmatch: function () {
          global.set("smallscreen", false);
          timeout(() => {
            global.set('navbarShown', false);
          });
        }
      });
    });
  }
  (<any>fcn).$inject = ["$document", "$timeout", "enquire","GlobalSettings"];

  angular
    .module("pineapples")
    .service("GlobalSettings", GlobalSettingsService)
    .run(fcn);

}