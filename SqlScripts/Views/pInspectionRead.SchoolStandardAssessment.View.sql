SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Shraddha Tambe
-- Create date: Dec 2019
-- Description:	Normalised view of SchoolStandardAssessment, with subcriteria/criteria descriptions
-- ==================================================
-- -- 3 views are available of 'Accreditation' objects:
-- 1) the XMl representation of the object - contans the full text of each criteria description etc
--    this may be loaded from the survey tool, or else fabricated from earlier SchoolAccreditation objects
--
-- 2) A normalised view showing 1 subcriteria per row, or, if subcriteria are not avilable
--		1 criteria per row. This forms the basis of aggregations going to the warehouse.
--		This view provides the underlying parse of the Xml object.
--		It is not dependent on any particular values for criteria or subcriteria numbers.
--		It requires only that the Xml is structured in the category/standard/criteria/subcriteria hierarchy.
--		We name assessment instruments in this hierarchy 'SchoolStandardAssessment' or SSA for short
--		pInspectionRead.SchoolStandardAssessment
--		also pInspectionRead.SchoolStandardAssessmentEx - which includes descriptions at each level as well
--
-- 3) a denormalised view, with 1 column per criteria. this is equivalent to the original School Accredtion object that was stored in
--		its own table. This makes specific assumptions about the naming of criteria in the Xml file
--		e.g. SE.1.2 School Evaluation Standard `1 Criteria 2
--
--	The Stored Procedure pInspectionREad.ReadInspection returns a record whose structure is dependent
--	on the inspection type of the inspection. It does this by executing a query
--  pInspectionRead.<inspectiontype> if such a query exists (Otherwise it executes pInspectionRead.SchoolInspections)
--  for example, to read a SCHOOL_ACCREDITATION the procedure uses pInspectionRead.SCHOOL_ACCREDITATION
--  Such procedures are intended to shred the Xml stored in inspXml on SchoolInpection to generate the returned record.
--
-- =============================================
CREATE VIEW [pInspectionRead].[SchoolStandardAssessment]
AS
-- totals by subcriteria
Select U.*
, I.InspectionYear
, I.schNo
, BSE.SurveyDimensionssID SurveyDimensionID
FROM
(
Select inspID

, v.value('(../../../../version)[1]', 'int') Version
, v.value('(../../../name)[1]', 'nvarchar(50)') Category
, v.value('(../../../type)[1]', 'nvarchar(50)') CategoryType
, v.value('(../../id)[1]', 'nvarchar(50)') Standard
, v.value('(../../name)[1]', 'nvarchar(max)') StandardName
, v.value('(../id)[1]', 'nvarchar(50)') Criteria
, v.value('(../name)[1]', 'nvarchar(max)') CriteriaName
, v.value('id[1]', 'nvarchar(50)') SubCriteria
, v.value('name[1]', 'nvarchar(max)') SubCriteriaName
, v.value('interviewQuestions[1]', 'nvarchar(max)') SubCriteriaQuestion
, v.value('(answer/state)[1]', 'nvarchar(50)') Answer
, convert(decimal(5,1)
    , case v.value('(answer/state)[1]', 'nvarchar(50)')
        when 'POSITIVE' then 1
        when 'NEGATIVE' then 0
        else null
        end
    ) Score
, 1 MaxScore
from dbo.SchoolInspection SI
CROSS APPLY SI.inspXml.nodes('/survey/category/standard/criteria/subcriteria') as V(v)

UNION ALL
-- totals by criteria
Select inspID
, v.value('(../../../../version)[1]', 'int') Version
, v.value('(../../../name)[1]', 'nvarchar(50)') Category
, v.value('(../../../type)[1]', 'nvarchar(50)') CategoryType
, v.value('(../../id)[1]', 'nvarchar(50)') Standard
, v.value('(../../name)[1]', 'nvarchar(max)') StandardName
, v.value('(../id)[1]', 'nvarchar(50)') Criteria
, v.value('(../name)[1]', 'nvarchar(max)') CriteriaName
, null SubCriteria
, null SubCriteriaName
, null SubCriteriaQuestion
, null Answer
, v.value('.', 'decimal(4,1)') Score
, v.value('(../maxscore)[1]', 'int') MaxScore
from dbo.SchoolInspection SI
CROSS APPLY SI.inspXml.nodes('/survey/category/standard/criteria/score') as V(v)

UNION ALL
-- totals by standard
Select inspID
, v.value('(../../../version)[1]', 'int') Version
, v.value('(../../name)[1]', 'nvarchar(50)') Category
, v.value('(../../type)[1]', 'nvarchar(50)') CategoryType
, v.value('(../id)[1]', 'nvarchar(50)') Standard
, v.value('(../name)[1]', 'nvarchar(max)') StandardName
, null Criteria
, null CriteriaName
, null SubCriteria
, null SubCriteriaName
, null SubCriteriaQuestion
, null Answer
, v.value('(.)', 'decimal(5,1)') Score
, v.value('(../maxscore)[1]', 'int') MaxScore
from dbo.SchoolInspection SI
CROSS APPLY SI.inspXml.nodes('/survey/category/standard/score') as V(v)

UNION ALL
-- totals by category
Select inspID
, v.value('(../../version)[1]', 'int') Version
, v.value('(../name)[1]', 'nvarchar(50)') Category
, v.value('(../type)[1]', 'nvarchar(50)') CategoryType
, null Standard
, null StandardName
, null Criteria
, null CriteriaName
, null SubCriteria
, null SubCriteriaName
, null SubCriteriaQuestion
, null Answer
, v.value('(.)[1]', 'decimal(4,1)') Score
, v.value('(../maxscore)[1]', 'int') MaxScore
from dbo.SchoolInspection SI
CROSS APPLY SI.inspXml.nodes('/survey/category/score') as V(v)
) U
INNER JOIN pInspectionRead.SchoolInspections I
	ON U.inspID = I.inspID
LEFT JOIN  tfnESTIMATE_BestSurveyEnrolments() BSE
	ON I.schNo = BSE.SchNo
	AND I.InspectionYear = BSE.LifeYear
GO

