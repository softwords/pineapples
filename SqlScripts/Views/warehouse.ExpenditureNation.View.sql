SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 14 10 2019
-- Description:	Nation level financial indicators
-- ==================================================
-- Reports on indicators involving GNP which are only avialble at the national level
-- ie GNP data is not collected at the state level, is unlikely to be avilable and not especially meaningful.
--
-- Built from warehouse.Expenditure, which consolidates the data available in GovtExpenditureState and
-- EdExpenditureState
-- ==================================================
CREATE VIEW [warehouse].[ExpenditureNation]
WITH VIEW_METADATA
AS
Select SurveyYear
, sum(GNP) GNP
, sum(GNPCapita) GNPCapita
, max(GNPCurrency) GNPCurrency
, sum(GNPLocal) GNPLocal
, sum(GNPCapitaLocal) GNPCapitaLocal
, sum(GovtExpA) GovtExpA
, sum(GovtExpB) GovtExpB
, convert(decimal(6,2),100) * sum(GovtExpA) /sum(GNPLocal)		GovtExpAGNPPerc
, convert(decimal(6,2),100) * sum(GovtExpB) /sum(GNPLocal)		GovtExpBGNPPerc
, sum(EdExpA) EdExpA
, sum(EdExpB) EdExpB
, convert(decimal(6,2),100 * sum(EdExpA) / sum(GovtExpA))		EdGovtExpAPerc
, convert(decimal(6,2),100 * sum(EdExpB) / sum(GovtExpB))		EdGovtExpBPerc

, convert(decimal(6,2),100 * sum(EdExpA) /sum(GNPLocal))		EdExpAGNPPerc
, convert(decimal(6,2),100 * sum(EdExpB) /sum(GNPLocal))		EdExpBGNPPerc

, sum(EdExpA) / sum(Enrolment) EdExpAPerHead
, sum(EdExpB) / sum(Enrolment) EdExpBPerHead

, convert(decimal(6,2),100 * sum(EdExpA) / (sum(Enrolment) * sum(GNPCapitaLocal))) EdExpAPerHeadGNPCapitaPerc
, convert(decimal(6,2),100 * sum(EdExpB) / (sum(Enrolment) * sum(GNPCapitaLocal))) EdExpBPerHeadGNPCapitaPerc

, sum(Enrolment) Enrolment
from warehouse.Expenditure
GROUP BY SurveyYear
--Select SUB.*
--, EdExpA / EnrolmentApplies EdExpAPerHead
--, EdExpB / EnrolmentApplies EdExpBPerHead
--FROM
--(
--Select E.SurveyYear
--, DistrictCode
--, SectorCode
--, sum(EdExpA) EdExpA
--, sum(EdExpB) EdExpB

--, sum(Enrolment) Enrolment
--, sum(case when DistrictCode = 'NATIONAL' then EnrolmentNation else Enrolment end) EnrolmentApplies


--from warehouse.Expenditure E
--WHERE E.DistrictCode is not null
--GROUP BY E.SurveyYear
--, DistrictCode
--, SectorCode
--) SUB
--WHERE coalesce(EdExpA, EdExpB, EnrolmentApplies) is not null
GO

