SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 9 2021
-- Description:	Alternate presentation of CandidateLevel results
-- usng a record type to distinguish between Exam, Standard Benchmark and Indcator table
--
-- History Log:
--   * 27/07/2022, Ghislain Hachey, use the exam name as the Description for RecordType of Exams
-- =============================================
CREATE VIEW [pExamRead].[CandidateLevelTyped]
AS
Select CandidateID
, schNo
, ExamID
, ExamCode
, ExamName
, ExamYear
, stuID
, Gender
, GivenName
, FamilyName
, case when examResult is not null then 'Exam'
	when StandardID is not null then 'Standard'
	when BenchmarkID is not null then 'Benchmark'
	when IndicatorID is not null then 'Indicator'
	end RecordType
, coalesce(case when examResult is not null then examID end, StandardID, BenchmarkID, IndicatorID) ID
, coalesce(ExamResult, StandardKey, BenchmarkKey, IndicatorKey) [Key]
, coalesce(case when examResult is not null then examName end, StandardDesc, BenchmarkDesc, IndicatorDesc) [Description]
, AchievementLevel
, AchievementDesc
, IndicatorCount
, Weight
, CandidateCount
FROM
[pExamRead].[CandidateLevel]
GO

