SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	District Cohort
--
-- A wrapper around warehouse.cohort table to aggregate to district level.
-- Cohort views contain on the fields needed to calculate flow indicators using the
-- reconstructed cohort method on a single row. Specifcially, they contain This Year and Next Data data on the record.
-- This makes it much easier to calculate the required ratios across 2 years' data.
-- Related warehouse objects:
-- warehouse.cohort : school level cohort data (table). All other cohort objects are derived from this.
-- warehouse.FlowSchool: flow indicators calculated at school level
-- warehouse.CohortSchool: school level cohort data
-- warehouse.CohortDistrict : district level cohort data
-- warehouse.FlowDistrict : flow indicator calculated at district level.
-- warehouse.CohortNation : nation level cohort data
-- warehouse.FlowNation : flow indicator calculated at nation level.
-- Cohortxxx version do not calculate the actual indicators - this is so you can do this calculation in
-- a cube (pivot table, tableau) where the ratio is based on an aggregate of rows
-- based on the cube configuration.
-- xxFlow versions do provide these ratios at the Row level. Use these for flat reporting (Jasper etc)
-- =============================================
CREATE VIEW [warehouse].[CohortDistrict]
WITH VIEW_METADATA
AS
Select *
	from
	(
	Select SurveyYear
	, YearOfEd
	, GenderCode
	, DistrictCode
	, dName District
	, sum(Enrol) Enrol
	, sum(Rep) Rep
	, sum(RepNY) RepNY
	, sum(TroutNY) TroutNY
	, sum(EnrolNYNextLevel) EnrolNYNextLevel
	, sum(RepNYNextLevel) RepNYNextLevel
	, sum(TrinNYNextLevel) TrinNYNextLevel
	From warehouse.Cohort C
		LEFT JOIN Districts D
			ON C.DistrictCode = D.dID
	GROUP BY SurveyYear
	, YearOfEd
	, GenderCode
	, DistrictCode
	, dName
	UNION ALL
	Select SurveyYear
	, YearOfEd
	, null GenderCode
	, DistrictCode
	, dName
	, sum(Enrol) Enrol
	, sum(Rep) Rep
	, sum(RepNY) RepNY
	, sum(TroutNY) TroutNY
	, sum(EnrolNYNextLevel) EnrolNYNextLevel
	, sum(RepNYNextLevel) RepNYNextLevel
	, sum(TrinNYNextLevel) TrinNYNextLevel
	From warehouse.Cohort C
		LEFT JOIN Districts D
			ON C.DistrictCode = D.dID
	GROUP BY SurveyYear
	, YearOfEd
	, DistrictCode
	, dName
	) U
GO

