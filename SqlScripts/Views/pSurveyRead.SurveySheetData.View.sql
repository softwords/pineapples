SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2021
-- Description:	Normalises the columns of the census workbook school and Wash pages
-- into name-value rows
-- =============================================
CREATE VIEW [pSurveyRead].[SurveySheetData]
AS

select ssID,  schNo, svyYear, ssSource, ssData rowData
,'School' sheet
, r.value('@Index','int') AS rowIndex
, a.value('local-name(.)','VARCHAR(50)') AS columnName  -- get attribute name
, a.value('.','nvarchar(max)') AS value  -- get attribute value

from SchoolSurvey
CROSS APPLY ssData.nodes('row') R(r)  -- access to the row element - attributes can be retrived by bame
CROSS APPLY ssData.nodes('row/@*') Att(a) -- access to the attibutes collection - can be enumerated to get name and value
WHERE ssData is not null

UNION ALL
select ssID,  schNo, svyYear, ssSource, ssWashData
, 'Wash' sheet
, r.value('@Index','int') AS RowIndex
, a.value('local-name(.)','VARCHAR(50)') AS ColumnName  -- get attribute name
, a.value('.','nvarchar(max)') AS Value  -- get attribute value

from SchoolSurvey
CROSS APPLY ssWashData.nodes('row') R(r)
CROSS APPLY ssWashData.nodes('row/@*') Att(a)
WHERE ssWashData is not null
GO

