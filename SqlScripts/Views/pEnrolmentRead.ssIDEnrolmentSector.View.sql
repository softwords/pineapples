SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2007
-- Description:	used by efa11 to grou-p enrolments by sector,
-- =============================================
CREATE VIEW [pEnrolmentRead].[ssIDEnrolmentSector]
as
SELECT
Enrollments.ssID,
lkpLevels.SecCode SectorCode,
Sum(Enrollments.enM) AS EnrolM,
Sum(Enrollments.enF) AS enrolF,
Sum(enSum) AS Enrol
FROM Enrollments
	INNER JOIN lkpLevels
	ON Enrollments.enLevel = lkpLevels.codeCode
GROUP BY Enrollments.ssID, lkpLevels.SecCode;
GO

