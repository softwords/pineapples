SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- View
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 01/06/2022
-- Description:	Warehouse - Exam Results (whole test) presented with minimum proficiency data
-- at School level (normalized version)
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
-- =============================================
CREATE VIEW [warehouse].[ExamResultsTestMinimumProficiencySchool]
AS
SELECT examCode
		, examName
		, examYear
		, ET.exNameShort examNameShort
		, ET.exLevel ExamClassLevel
		, L.lvlYear ExamClassLevelYear
		, EL.codeCode EdLevelCode
		, EL.edlMaxYear EdLevelMaxYear
		, EL.edlMinYear EdLevelMinYear
		, DSS.[School No] schNo
		, DSS.[School Name] schName
		, DSS.[District Code] DistrictCode
		, DSS.[District] District
		, SUM(ISNULL(CONVERT(float, [3]), 0)) + SUM(ISNULL(CONVERT(float, [4]), 0)) MinimumProficiencyC
		, SUM(Candidates) TotalCandidates
		, ROUND((SUM(ISNULL(CONVERT(float, [3]), 0)) + SUM(ISNULL(CONVERT(float, [4]), 0))) / SUM(Candidates), 2) [MinimumProficiencyP]
	FROM [warehouse].[ExamResultsTestSchoolX] ERTSX
	INNER JOIN DimensionSchoolSurvey DSS ON ERTSX.schNo = DSS.[School No]  AND (ERTSX.examYear = DSS.[Survey Data Year] OR DSS.[Survey Data Year] IS NULL) -- The IS NULL clause is a hack to pick up a school with exams data without any survey data in DimensionSchoolSurvey
	INNER JOIN [dbo].[lkpExamTypes] ET ON ERTSX.examCode = ET.exCode
	INNER JOIN [dbo].[lkpLevels] L ON ET.exLevel = L.codeCode
	CROSS JOIN [dbo].[lkpEducationLevels] EL
	WHERE (L.lvlYear BETWEEN EL.edlMinYear AND EL.edlMaxYear)
	GROUP BY
		examCode
		, examName
		, examYear
		, ET.exNameShort
		, ET.exLevel
		, L.lvlYear
		, EL.codeCode
		, EL.edlMaxYear
		, EL.edlMinYear
		, DSS.[School No]
		, DSS.[School Name]
		, DSS.[District Code]
		, DSS.[District]
GO

