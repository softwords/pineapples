SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 28 10 2021
-- Description:	TeacherLocationAndQualificationHighest is a view requested by Weison (and education advisors in FSM.)
-- The purpose of this view is to bring data on duties (which teacher teaches what grade in what schools), their
-- qualifications and subjects of study (with support for ISCED-F) all in one convenient place.
-- FSM education advisors are interested in exploring correlations there and eventually with student performance.

-- Caveat: This view is based on TeacherQualificationHighest and hence also come with the disadvantage
-- that annual surveys might be missing qualification data entered through the UI. And also only contains
-- teacher (and not other staff)

-- As usual, data is provided over the years with many supported disaggregations.
-- TBare in mind that the Certified/Qualified
-- boolean value takes into consideration the qualifications in TeacherQual (and not just what
-- is in the survey) so there could be mismatch if highest qualification is not reported in survey and
-- using the LEFT JOIN lkpTeacherQual...below.
--
-- History Log:
--   * 28 October 2021, Ghislain Hachey, First draft of view.
--   * 28 April 2022, Ghislain Hachey, changed to only include teachers
--   * 4 May 2022, Ghislain Hachey, Added salary field.
-- =============================================
CREATE VIEW [warehouse].[TeacherLocationAndQualificationsHighest]
WITH VIEW_METADATA
AS
SELECT TL.[TID]
	  ,[tGiven]
      ,[tMiddleNames]
      ,[tSurname]
	  ,TL.[SchNo]
	  ,S.schName
      ,TL.[SurveyYear]
      ,TL.[GenderCode]
      ,TL.[BirthYear]
      ,TL.[AgeGroup]
      ,[SurveySchNo]
      ,[SurveyRole]
      ,[SurveyDataYear]
      ,[Estimate]
      ,TL.[ISCEDSubClass]
      ,TL.[Certified]
      ,TL.[Qualified]
      ,[Tpk]
      ,[T00]
      ,[T01]
      ,[T02]
      ,[T03]
      ,[T04]
      ,[T05]
      ,[T06]
      ,[T07]
      ,[T08]
      ,[T09]
      ,[T10]
      ,[T11]
      ,[T12]
      ,[T13]
      ,[T14]
      ,[T15]
      ,[T]
      ,[A]
      ,[X]
	  ,[Tpk] / [Activities] [TpkW]
      ,[T00] / [Activities] [T00W]
      ,[T01] / [Activities] [T01W]
      ,[T02] / [Activities] [T02W]
      ,[T03] / [Activities] [T03W]
      ,[T04] / [Activities] [T04W]
      ,[T05] / [Activities] [T05W]
      ,[T06] / [Activities] [T06W]
      ,[T07] / [Activities] [T07W]
      ,[T08] / [Activities] [T08W]
      ,[T09] / [Activities] [T09W]
      ,[T10] / [Activities] [T10W]
      ,[T11] / [Activities] [T11W]
      ,[T12] / [Activities] [T12W]
      ,[T13] / [Activities] [T13W]
      ,[T14] / [Activities] [T14W]
      ,[T15] / [Activities] [T15W]
      ,[T] / [Activities] [TW]
      ,[A] / [Activities] [AW]
      ,[X] / [Activities] [XW]
      ,[Activities]
      ,[TAMX]
      ,[QualificationCode]
      ,[Qualification]
      ,[QualificationGroup]
	  ,[Institution]
	  ,[Year]
      ,[SubjectMajorCode]
      ,[SubjectMajor]
      ,[ISCEDDetailedFieldCode]
      ,[ISCEDDetailedField]
      ,[ISCEDNarrowFieldCode]
      ,[ISCEDNarrowField]
      ,[ISCEDBroadFieldCode]
      ,[ISCEDBroadField]
	  ,[Salary]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityGroupCode]
      ,[AuthorityGroup]
      ,[AuthorityTypeCode]
      ,[AuthorityType]
      ,[District Code]
      ,[District]
      ,[Region Code]
      ,[Region]
      ,[SchoolTypeCode]
      ,[SchoolType]
      ,[Island Code]
      ,[Island]
  FROM [warehouse].[TeacherLocation] TL
  LEFT OUTER JOIN [warehouse].[TeacherQualificationsHighest] TQH ON TL.TID = TQH.TID AND TL.SurveyYear = TQH.SurveyYear
  JOIN [dbo].[TeacherIdentity] TI ON TL.TID = TI.tID
  JOIN [dbo].Schools S ON TL.SchNo = S.schNo
  WHERE TL.TAMX IN ('T','M')
GO

