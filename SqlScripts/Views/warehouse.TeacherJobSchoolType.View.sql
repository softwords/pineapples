SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs school type totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobSchoolType]
WITH VIEW_METADATA
AS
Select SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType

, T.SchoolTypeCode
, stDescription SchoolType

, sum(NumStaff) NumStaff

FROM warehouse.TeacherJobTable T
	INNER JOIN  SchoolTypes S
		ON T.SchoolTypeCode = S.stCode
GROUP BY
SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType

, T.SchoolTypeCode
, stDescription
GO

