SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [commonInfrastructure].[TRBuildingValuationClass]
AS
Select bvcCode
, isnull(CASE dbo.userLanguage()
	WHEN 0 THEN bvcDescription
	WHEN 1 THEN bvcDescriptionL1
	WHEN 2 THEN bvcDescriptionL2

END, bvcDescription) AS bvcDescription
from lkpBuildingValuationClass;
GO

