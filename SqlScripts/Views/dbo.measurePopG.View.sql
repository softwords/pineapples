SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--      Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental list of population, by district if available
-- for the default population model. normalised by Gender
-- Used in the creation of data warehouse
-- =============================================
CREATE VIEW [dbo].[measurePopG]
AS
Select popYear
, dID
, popAge
, G.genderCode
, case G.genderCode
	when 'M' then popM
	when 'F' then popF
 end pop
FROM measurePop
	CROSS JOIN DimensionGender G
GO

