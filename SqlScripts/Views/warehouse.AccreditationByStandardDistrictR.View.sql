SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 21 August 2019
-- Description:	Aggregation of Level by Standard
-- ==================================================
-- Reports on the accedition level (according to SSA partition)
-- achieved by school for each School_Evaluation standard
-- Moved to specific views from construction of this SQL in the Web Portal data layer
-- to allow such views to be easily available to report writers

-- the 'Result' shown here is an abstract 'average' result for the standard
-- across all schools in the aggregation. this is made by aggregating the Score and Max Score, then putting
-- this percentage through the SSA partition to get a level.

-- Note that this view cross-tabulates Result ( ie Level x) horizontally.
-- This is the number of schools achieving each level.
-- PerformancewXXX views show Result vertically.
--
-- Related views:
-- AccreditationByStandardTable
-- AccreditationByStandardSchool
-- AccreditationByStandardDistrict
-- AccreditationByStandardAuthority
-- AccreditationByStandardSchoolType
-- AccreditationByStandardNation

--
-- ==================================================
CREATE VIEW [warehouse].[AccreditationByStandardDistrictR]
WITH VIEW_METADATA
AS
Select SurveyYear
, DistrictCode, District
--, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
--, SchoolTypeCode, SchoolType
, Standard
, Score
, MaxScore
, P.ptName Result
, NumSchools
, Level1
, Level2
, Level3
, Level4

, ScoreInYear
, MaxScoreInYear
, PInYear.ptName ResultinYear
, NumSchoolsInYear
, Level1InYear
, Level2InYear
, Level3InYear
, Level4InYear

FROM
(
SELECT SurveyYear
      ,DistrictCode
      ,District
      --,AuthorityCode
      --,Authority
      --,AuthorityGovtCode
      --,AuthorityGovt
      --,SchoolTypeCode
      --,SchoolType
      ,Standard
      ,sum(Score) Score
      ,sum(MaxScore) MaxScore
	  ,sum(Level1) Level1
	  ,sum(Level2) Level2
	  ,sum(Level3) Level3
	  ,sum(Level4) Level4
	  , sum(NumSchools) NumSchools
      ,sum(ScoreInYear) ScoreInYear
      ,sum(MaxScoreInYear) MaxScoreInYear
	  ,sum(Level1InYear) Level1InYear
	  ,sum(Level2InYear) Level2InYear
	  ,sum(Level3InYear) Level3InYear
	  ,sum(Level4InYear) Level4InYear
	  ,sum(NumSchoolsInYear) NumSchoolsInYear

  FROM warehouse.AccreditationByStandardSchool
  GROUP BY
	SurveyYear
      ,DistrictCode
      ,District
      --,AuthorityCode
      --,Authority
      --,AuthorityGovtCode
      --,AuthorityGovt
      --,SchoolTypeCode
      --,SchoolType
      ,Standard
) SUB
LEFT JOIN Partitions P
	ON P.PtSet = 'SSA'
		AND (convert(decimal(8,3), SUB.Score) * 100 )/  SUB.maxScore between P.ptMin and P.ptMax
LEFT JOIN Partitions PInYear
	ON PInYear.PtSet = 'SSA'
		AND (convert(decimal(8,3), SUB.ScoreInYear) * 100 )/  SUB.maxScoreInYear between PInYear.ptMin and PInYear.ptMax
GO

