SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRRandM]
AS
SELECT     randmCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN randmDescription
	WHEN 1 THEN randmDescriptionL1
	WHEN 2 THEN randmDescriptionL2

END,randmDescription) AS randmDescription
, randmSeq
FROM         dbo.lkpRandM
GO

