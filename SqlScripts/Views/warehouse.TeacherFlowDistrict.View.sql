SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2020
-- Description:	Warehouse - Teacher Flow District
--
-- Compares teacher status year on year to identify
-- -- teachers who leave the teaching service (Exiters)
-- -- teachers who enter the teaching service (Entrants)
-- -- teachers who move to a different district (TransfersInDistrict / TransfersOutDistrict)
-- -- teachers who change school (TransfersIn / TransfersOut)
-- Teacher Attrition is an output of this data, simply by taking
-- Exiters / NumStaff at any aggregation
--
-- Derived from warehouse.TeacherLocation table.
-- =============================================
CREATE VIEW [warehouse].[TeacherFlowDistrict]
AS
Select SurveyYear, Sector, GenderCode, TAMX
, DistrictCode, District
, count(*) NumStaff
, sum(case when SurveyYear = minYear then Null when PrevYear is null then 1 else 0 end) Entrant
, sum(case when SurveyYear = minYear then Null when PrevYear is null then C else 0 end) EntrantC
, sum(case when SurveyYear = minYear then Null when PrevYear is null then Q else 0 end) EntrantQ
, sum(case when SurveyYear = minYear then Null when PrevYear is null then C*Q else 0 end) EntrantCQ

, sum(case when SurveyYear = maxYear then Null when NextYear is null then 1 else 0 end) Exiter
, sum(case when SurveyYear = maxYear then Null when NextYear is null then C else 0 end) ExiterC
, sum(case when SurveyYear = maxYear then Null when NextYear is null then Q else 0 end) ExiterQ
, sum(case when SurveyYear = minYear then Null when PrevYear is null then C*Q else 0 end) ExiterCQ

, sum(case when SurveyYear = minYear then Null when PrevSchNo <> schNo then 1 else 0 end) TransferIn
, sum(case when SurveyYear = minYear then Null when PrevYear is not null and PrevDistrict <> District then 1 else 0 end) TransferInDistrict
, sum(case when SurveyYear = maxYear then Null when NextSchNo <> schNo then 1 else 0 end) TransferOut
, sum(case when SurveyYear = maxYear then Null when NextDistrict <> District then 1 else 0 end) TransferOutDistrict

from
(
Select TY.surveyYear, TY.Sector, TY.tID
, TY.GenderCode
, TY.AgeGroup
, TY.schNo
, TY.DistrictCode
, TY.District
, TY.TAMX
, TY.Certified C, TY.Qualified Q

, PREVY.surveyYear PrevYear, PREVY.schNo PrevSchNo, PREVY.TAMX PrevTAMX, PrevY.District PrevDistrict
, NY.surveyYear NextYear, NY.schNo NextSchNo, NY.TAMX NextTAMX, NY.District NextDistrict
, minYear, maxYear

from warehouse.TeacherLocationDistrict TY


	LEFT JOIN
	warehouse.TeacherLocationDistrict NY
		ON TY.tID = NY.tID
		AND TY.SurveyYear = NY.surveyYear -1

	LEFT JOIN
	warehouse.TeacherLocationDistrict PREVY
		ON TY.tID = PREVY.tID
		AND TY.SurveyYear = PREVY.surveyYear + 1
CROSS JOIN
(Select min(SurveyYear) minYear, max(SurveyYear) MaxYear from warehouse.TeacherLocation) MINMAX
) SUB
Group by surveyYear, Sector , GenderCode, DistrictCode, District, TAMX
GO

