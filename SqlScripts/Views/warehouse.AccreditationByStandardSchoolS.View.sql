SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Ghislain Hachey
-- Create date: 11 October 2022
-- Description:	Aggregation of Level by Standard (School Simpler version)
-- ==================================================
-- Reports on the accredition level (according to SSA partition)
-- achieved by school for each School_Evaluation standard
-- Moved to specific views from construction of this SQL in the Web Portal data layer
-- to allow such views to be easily available to report writers

--
-- Related views:
-- AccreditationByStandardTable
-- AccreditationByStandardSchool
-- AccreditationByStandardDistrict
-- AccreditationByStandardAuthority
-- AccreditationByStandardSchoolType
-- AccreditationByStandardNation
-- AccreditationByStandardRegion
-- AccreditationByStandardLocalElectorate
--
-- ==================================================
CREATE VIEW [warehouse].[AccreditationByStandardSchoolS]
WITH VIEW_METADATA
AS
Select SurveyYear
, schNo
, schName
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
, [Region Code]
, Region
, [Local Electorate No]
, [Local Electorate]
, [National Electorate No]
, [National Electorate]
, Standard
, Result
, NumSchools
, NumSchoolsInYear
FROM
  [warehouse].[AccreditationByStandardSchool]
GO

