SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2018
-- Description:	Warehouse - Teacher Count
--
-- Top level reporting view for teacher numbers:
-- Shows total number of teachers, number certified, number qualified, number both certified and qualified
-- This is a wrapper around warehouse.schoolTeacherCount, which has the same data by school
--
-- Aggregated up to school type, authority, district and sector
-- as well as teacher age bracket and gender
-- Includes names as well as codes for agregation fields,
-- and includes all Authority dimension; ie including Govt / Non-Govt (Public /Private)
--
-- When used in a cube, this allows aggregation on any of these dimensions.
-- =============================================
CREATE VIEW [warehouse].[TeacherCountTableR]
AS
Select SurveyYear
, DistrictCode
, District
, SchoolTypeCode
, SchoolType
, AuthorityCode
, Authority
, AuthorityTypeCode
, AuthorityType
, AuthorityGroupCode
, AuthorityGroup

, AgeGroup
, Sector

, sum(NumSupportStaff) NumSupportStaff
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual

, sum(case when GenderCode='M' then NumSupportStaff end) NumSupportStaffM
, sum(case when GenderCode='M' then NumTeachers end) NumTeachersM
, sum(case when GenderCode='M' then Certified end) CertifiedM
, sum(case when GenderCode='M' then Qualified end) QualifiedM
, sum(case when GenderCode='M' then CertQual end) CertQualM

, sum(case when GenderCode='F' then NumSupportStaff end) NumSupportStaffF
, sum(case when GenderCode='F' then NumTeachers end) NumTeachersF
, sum(case when GenderCode='F' then Certified end) CertifiedF
, sum(case when GenderCode='F' then Qualified end) QualifiedF
, sum(case when GenderCode='F' then CertQual end) CertQualF
from warehouse.TeacherCountTable
GROUP BY
SurveyYear
, DistrictCode
, District
, SchoolTypeCode
, SchoolType
, AuthorityCode
, Authority
, AuthorityTypeCode
, AuthorityType
, AuthorityGroupCode
, AuthorityGroup

, AgeGroup
, Sector
GO

