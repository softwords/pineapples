SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 7/3/2020
-- Description:	Same as [warehouse].[ClassLevelAge] for aggregated to nation
--
-- National enrolments divided by class level, showing whether the pupil is at, over or under
-- the EdLevel OfficialAge
-- The year of ed of the class level determines the EdLevel here, and the official age range
-- is determined in the usual way (from Offical Start Age and Range of years of Ed for the Ed Level)
-- Suggested Use: High Level analysis of over / under age enrolments.
-- Note this view uses only the principal education levels defined in lkpEducationLevels
-- It does not use the Alt or Alt2 groupings (to do?)
-- =============================================
CREATE VIEW [warehouse].[ClassLevelAgeNation]
AS
Select surveyYear
, ClassLevel
, yearOfEd
, EdLevel
, GenderCode
, sum(case when ClassLevelOfficialAge = 'UNDER' then Enrol else null end ) UnderAge
, sum(case when ClassLevelOfficialAge = '=' then Enrol else null end ) OfficialAge
, sum(case when ClassLevelOfficialAge = 'OVER' then Enrol else null end ) OverAge
, sum(case when ClassLevelOfficialAgeSDG = 'UNDER' then Enrol else null end ) UnderAgeSDG
, sum(case when ClassLevelOfficialAgeSDG = '=' then Enrol else null end ) OfficialAgeSDG
, sum(case when ClassLevelOfficialAgeSDG = 'OVER' then Enrol else null end ) OverAgeSDG
, sum(Enrol) Enrol
, sum(case when Estimate = 1 then Enrol else null end ) EstimatedEnrol
From Warehouse.enrolmentRatios
GROUP BY
surveyYear, ClassLevel, yearOfEd, EdLevel, GenderCode
GO

