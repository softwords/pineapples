SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2020
-- Description:	Warehouse - Student Attendance
--
-- The base view for student attendance down at the schools
-- level with all basic disaggregation
-- =============================================
CREATE VIEW [warehouse].[StudentAttendanceSchool]
AS
SELECT S.stuID AS stuID
	  ,S.stuGender AS Gender
      ,SE.[schNo]
      ,[stueYear] AS svyYear
      ,[stueSpEd]
      ,[stueDaysAbsent]
      ,[stueCompleted]
      ,[stueOutcome]
      ,[stueOutcomeReason]
	  , DSS.[School Name]
	  , DSS.[District Code]
	  , DSS.[District]
	  , DSS.[Region Code]
	  , DSS.[Region]
	  , DSS.[AuthorityCode]
	  , DSS.[Authority]
	  , DSS.[AuthorityGroupCode]
	  , DSS.[AuthorityGroup]
	  , DSS.[SchoolTypeCode]
	  , DSS.[SchoolType]
	  , DSS.[Island Code]
	  , DSS.[Island]
  FROM [dbo].[StudentEnrolment_] SE
  JOIN [dbo].[Student_] S ON SE.stuID = S.stuID
  LEFT JOIN DimensionSchoolSurvey DSS ON SE.schNo = DSS.[School No] AND SE.stueYear = DSS.[Survey Data Year]
GO

