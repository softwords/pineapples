SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:National Inspections with result
--
-- =============================================
CREATE VIEW [warehouse].[InspectionNation]
WITH VIEW_METADATA
AS
Select SurveyYear
, InspectionTypeCode
, InspectionResult
, sum(Num) Num
, sum(NumThisYear) NumThisYear

FROM warehouse.InspectionTable
GROUP BY SurveyYear
, InspectionTypeCode
, InspectionResult
GO

