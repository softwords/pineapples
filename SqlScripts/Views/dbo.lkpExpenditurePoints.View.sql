SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 21 August 2019
-- Description:	Funding points for budgets
-- ==================================================
--
-- Budgets may be funded at the state or national level
-- This view assembles this, using the hardcoded value 'NATIONAL' fornational expenditure.
-- The description (National? CEntral? Federal? )
-- can be set in sysvocab
--
-- ==================================================
CREATE VIEW [dbo].[lkpExpenditurePoints]
WITH VIEW_METADATA
AS

Select 'NATIONAL' C
, 'National' N
, '' S
WHERE not exists (Select vocabName from sysvocab WHERE vocabName = 'NATIONALGOVT')
UNION
Select 'NATIONAL', vocabName, ''
from sysvocab
WHERe vocabName = 'NATIONALGOVT'
UNION
Select dID, dName, dID
From Districts
GO

