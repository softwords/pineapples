SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: Jan 2020
-- Description:	Warehouse - Total Enrolments by school
--
-- This is a consolidation of warehouse.enrol to provide all the disaggregations
-- that are presented on the UIS survey sheets A2-A6
-- Data includes Enrol, Rep and PSA
-- UDF warehouse.EnrolISCEDAudit filter this view by SurveyYear
-- =============================================
CREATE VIEW [warehouse].[EnrolUis]
AS

Select E.SurveyYear
, E.SchNo
, DSS.[School Name]
, case DSS.AuthorityGovtCode when 'N' then 'Private' when 'G' then 'Public' end PublicPrivate
, Dl.Level ClassLevel
, DL.[ISCED SubClass], [ISCED Level]
, E.GenderCode
, E.Age
, case when U.Age =  -1 then 'Unknown'
when U.Age > 29 then '>29'
when U.Age >=25 then  '25-29'
when U.Age < 2 then '<2'
else convert(nvarchar(2), U.Age)
end A3AgeGroup
, case when U.Age =  -1 then 'Unknown'
when U.Age > 24 then '>24'
when U.Age < 4 then '<4'
else convert(nvarchar(2), U.Age)
end A5AgeGroup
, case when U.Age =  -1 then 'Unknown'
when U.Age > 24 then '>24'
when U.Age < 10 then '<10'
else convert(nvarchar(2), U.Age)
end A6AgeGroup
, E.Enrol
, E.Rep
, E.PSA
, E.Estimate

FROM
(Select isnull(Age, -1) Age from warehouse.tableEnrol
UNION
Select num from metaNumbers WHERE num between -1 and 30
) U
LEFT JOIN warehouse.enrol E
	ON (E.Enrol > 0 or E.Rep > 0)
	AND U.Age = E.Age
LEFT JOIN warehouse.DimensionSchoolSurvey DSS
ON E.SurveyDimensionID = Dss.[Survey ID]
LEFT JOIN DimensionLevel DL
on E.ClassLevel = DL.LevelCode
GO

