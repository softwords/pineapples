SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[schoolYearHasDataFurniture]
AS
SELECT DISTINCT dbo.SchoolSurvey.svyYear,
		dbo.SchoolSurvey.schNo, dbo.SchoolSurvey.ssID,ssqlevel

FROM   dbo.SchoolSurvey INNER JOIN dbo.Furniture ON SchoolSurvey.ssID = Furniture.ssID
             LEFT OUTER JOIN
                      dbo.tfnQualityIssues('', '') Q
					on schoolsurvey.ssID = q.ssID
GO

