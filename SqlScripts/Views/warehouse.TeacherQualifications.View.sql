SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- View
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 26 09 2020
-- Description:	TeacherQualification is a suggested base view (e.g. TeacherXXXTable, TeacherXXXSchool)
-- to produce statistics on Qualifications, Certifications and other trainings themselves. It provides
-- the ability to do it over the years with many disaggregations. Care should be taken has
-- direct analysis on this view will "over count" total teachers since teachers can have more than
-- one qualifications and thus more then one record.
--
-- History Log:
--   * 18 April 2022, Ghislain Hachey, Added support to include institution/year when possible.
-- =============================================
CREATE VIEW [warehouse].[TeacherQualifications]
WITH VIEW_METADATA
AS
SELECT TL.[TID]
      ,[SurveyYear]
      ,[GenderCode]
      ,[BirthYear]
      ,[AgeGroup]
      ,[ISCEDSubClass]
      ,[Sector]
      ,[Certified]
      ,[Qualified]
      ,[tchQual] AS QualificationCode
	  ,LTQ.codeDescription AS Qualification
	  ,LTQ.codeGroup AS QualificationGroup
	  ,TT.trInstitution Institution
	  ,TT.trYear [Year]
      ,TL.[SchNo]
	  ,DSS.AuthorityCode
	  ,DSS.Authority
	  ,DSS.AuthorityGroupCode
	  ,DSS.AuthorityGroup
	  ,DSS.AuthorityTypeCode
	  ,DSS.AuthorityType
	  ,DSS.[District Code]
	  ,DSS.District
	  ,DSS.[Region Code]
	  ,DSS.Region
	  ,DSS.SchoolTypeCode
	  ,DSS.SchoolType
	  ,DSS.[Island Code]
	  ,DSS.Island
  FROM [warehouse].[TeacherLocation] TL
  INNER JOIN [warehouse].[TeacherQual] TQ ON TL.TID = TQ.tID
  INNER JOIN lkpTeacherQual LTQ ON TQ.tchQual = LTQ.codeCode
  INNER JOIN [dbo].[DimensionSchoolSurvey] DSS ON TL.SchNo = DSS.[School No] AND TL.SurveyYear = DSS.[Survey Year]
  LEFT OUTER JOIN lkpTeacherRole LTR ON TL.Role = LTR.codeCode
  LEFT OUTER JOIN -- add support to include institution/year when possible
	(
	-- Possible cases to handle
	-- 1) We can have multiple certificate trainings recorded (in table dbo.TeacherTraining)
	-- 2) There could be duplicate trainings recorded (in table dbo.TeacherTraining)
	SELECT tID
		, max(isnull(trYear,0)) trYear -- 1) we'll just take the most recent one
		, trQual
		, max(trInstitution) trInstitution -- 2) we'll just take the one with an explicitly defined institutions. Unfortunately "as-is"
		-- it does not work if at least one of the dupes does not specify an institution (there is no such case in the data and should
		-- not be any since it is required data on entry). The ones with missing institution is likely old data manually entered
	FROM [dbo].[TeacherTraining]
	GROUP BY tID, trQual) TT ON TL.TID = TT.tID AND TQ.tchQual = TT.trQual
  WHERE (
  TAMX = 'T' OR
  TAMX = 'M'
  ) -- only teachers
GO

