SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 11/04/2022
-- Description:	Warehouse - Exam Results Level Count (whole test) at the candidate level
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- A convenience view for Candidate reports that is similar to [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsLevelCountTestCandidateX]
AS
Select CandidateID
, stuID
, Gender
, GivenName
, FamilyName
, examID
, examCode
, examYear
, examName
, schNo
, sum(IndicatorCount) IndicatorCount
, sum(case when AchievementLevel = 0 then IndicatorCount end) [0]
, sum(case when AchievementLevel = 1 then IndicatorCount end) [1]
, sum(case when AchievementLevel = 2 then IndicatorCount end) [2]
, sum(case when AchievementLevel = 3 then IndicatorCount end) [3]
, sum(case when AchievementLevel = 4 then IndicatorCount end) [4]
, sum(case when AchievementLevel = 5 then IndicatorCount end) [5]
, sum(case when AchievementLevel = 6 then IndicatorCount end) [6]
, sum(case when AchievementLevel = 7 then IndicatorCount end) [7]
, sum(case when AchievementLevel = 8 then IndicatorCount end) [8]
, sum(case when AchievementLevel = 9 then IndicatorCount end) [9]

From [pExamRead].[CandidateLevelTyped]
WHERE RecordType = 'Exam'
GROUP BY CandidateID
, stuID
, Gender
, GivenName
, FamilyName
, examID
, examCode
, examYear
, examName
, schNo
GO

