SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/04/2022
-- Description:	Warehouse - Exam Results (Standards) aggregated at the district level (normalized version)
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsStandardsDistrict]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, Gender
, [Key] [standardCode]
, Description [standardDesc]
, achievementLevel
, achievementDesc
, sum(candidateCount) Candidates

From warehouse.examSchoolResultsTyped
WHERE RecordType = 'Standard'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, Gender
, [Key]
, Description
, achievementLevel
, achievementDesc
GO

