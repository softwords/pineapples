SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblCommunications]
AS
SELECT
Resources.resID,
Resources.ssID,
Resources.resSplit,
Resources.resAvail,
Resources.resNumber,
Resources.resCondition
FROM dbo.Resources
WHERE (((Resources.resName)='Communications'))
GO

