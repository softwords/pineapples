SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 11/04/2022
-- Description:	Warehouse - Exam Results Level Count (whole test) at the candidate level (normalized version)
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- A convenience view for Candidate reports that is similar to [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsLevelCountTestCandidate]
AS
Select CandidateID
, stuID
, Gender
, GivenName
, FamilyName
, examID
, examCode
, examYear
, examName
, schNo
, achievementLevel
, achievementDesc
, sum(IndicatorCount) IndicatorCount
FROM [pExamRead].[CandidateLevelTyped]
WHERE RecordType = 'Exam'
GROUP BY CandidateID
, stuID
, Gender
, GivenName
, FamilyName
, examID
, examCode
, examYear
, examName
, schNo
, achievementLevel
, achievementDesc
GO

