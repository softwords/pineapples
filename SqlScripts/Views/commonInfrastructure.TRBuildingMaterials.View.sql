SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [commonInfrastructure].[TRBuildingMaterials]
AS
Select bmatCode
, isnull(CASE dbo.userLanguage()
	WHEN 0 THEN bmatDescription
	WHEN 1 THEN bmatDescriptionL1
	WHEN 2 THEN bmatDescriptionL2

END,bmatDescription) AS bmatDescription
, bmatFloor
, bmatWall
, bmatRoof
from lkpBuildingMAterials
GO

