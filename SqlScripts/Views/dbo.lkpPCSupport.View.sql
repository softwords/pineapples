SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[lkpPCSupport]
AS
SELECT     codeCode,
CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END AS codeType,CodeNum,codeDescription,codeDescriptionl1,CodeDescriptionL2

FROM     dbo.lkpCodesMisc
Where codeType='PCSupport'
GO
GRANT SELECT ON [dbo].[lkpPCSupport] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpPCSupport] TO [public] AS [dbo]
GO

