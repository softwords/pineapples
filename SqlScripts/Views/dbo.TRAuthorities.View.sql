SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRAuthorities]
AS
SELECT     authCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN authName
	WHEN 1 THEN authNameL1
	WHEN 2 THEN authNameL2
END, authName) AS authName,
authType
FROM         dbo.Authorities
GO
GRANT SELECT ON [dbo].[TRAuthorities] TO [public] AS [dbo]
GO

