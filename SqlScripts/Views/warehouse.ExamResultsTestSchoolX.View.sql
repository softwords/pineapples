SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 08/04/2022
-- Description:	Warehouse - Exam Results (whole test) at the candidate level
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- Similar
-- Exam results are based on the "standard-benchmark-indicator achievement levels" hierarchy.
-- Variants of presentation are
-- consolidation:
--   * national
--   * district (e.g. state)
--   * school level (e.g. this one)
--
-- structure:
--   * normalised: as the foundation tables warehouse.ExamSchoolResultsTyped, warehouse.ExamTableResultsTyped
--                 and [dbo].[ExamCandidateResults] for candidates
--   * "crosstabbed": In this cross tabbed versions up to 10 achievement levels are presented on a single record.
--                    This is more amenable to some chart presentations.
--   * "report": liked crosstabbed buut further packaged in the most useful way for band report software like Jaspersoft, Crystal Reports, etc.
--
-- The datasets are:
-- ExamResultsTestNation - national consolidation for the whole test
-- ExamResultsTestNationX - national consolidation for the whole test crosstabbed
-- ExamResultsStandardsNation - national consolidation by standard
-- ExamResultsStandardsNationX - national consolidation by standard - crosstabbed
-- ExamResultsBenchmarksNation - national consolidation by benchmark
-- ExamResultsBenchmarksNationX - national consolidation by benchmark - crosstabbed
-- ExamResultsIndicatorsNation - national consolidation by indicator
-- ExamResultsIndicatorsNationX - national consolidation by indicator - crosstabbed

-- ExamResultsTestDistrict - district consolidation for the whole test
-- ExamResultsTestDistrictX - district consolidation for the whole test crosstabbed
-- ExamResultsStandardsDistrict - district consolidation by standard
-- ExamResultsStandardsDistrictX - district consolidation by standard - crosstabbed
-- ExamResultsBenchmarksDistrict - district consolidation by benchmark
-- ExamResultsBenchmarksDistrictX - district consolidation by benchmark - crosstabbed
-- ExamResultsIndicatorsDistrict - district consolidation by indicator
-- ExamResultsIndicatorsDistrictX - district consolidation by indicator - crosstabbed

-- ExamResultsTestSchool - school consolidation for the whole test
-- ExamResultsTestSchoolX - school consolidation for the whole test crosstabbed
-- ExamResultsStandardsSchool - school consolidation by standard
-- ExamResultsStandardsSchoolX - school consolidation by standard - crosstabbed
-- ExamResultsBenchmarksSchool - school consolidation by benchmark
-- ExamResultsBenchmarksSchoolX - school consolidation by benchmark - crosstabbed
-- ExamResultsIndicatorsSchool - school consolidation by indicator
-- ExamResultsIndicatorsSchoolX - school consolidation by indicator - crosstabbed
--
-- As of Novemeber 2021 these are built from the new 'Typed' versions exam data (normalized), supporting the 'Soe'
-- model: ExamSchoolResultsTyped and ExamTableResultsTyped
-- These are created by the stored proc warehouse.buildExamResults
-- from Benchmark -> Standard = > Exam
-- Note that this SP is NOT called from the warehouse.buildWarehouse function,
-- but instead is invoked as part of the upload of each exam file.
--
-- Note: We no longer show standard and benchmark values, since in the new model, these cannot be correctly aggregated
-- =============================================
CREATE VIEW [warehouse].[ExamResultsTestSchoolX]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, sum(candidateCount) Candidates
, sum(case when achievementLevel = 0 then candidateCount end) [0]
, sum(case when achievementLevel = 1 then candidateCount end) [1]
, sum(case when achievementLevel = 2 then candidateCount end) [2]
, sum(case when achievementLevel = 3 then candidateCount end) [3]
, sum(case when achievementLevel = 4 then candidateCount end) [4]
, sum(case when achievementLevel = 5 then candidateCount end) [5]
, sum(case when achievementLevel = 6 then candidateCount end) [6]
, sum(case when achievementLevel = 7 then candidateCount end) [7]
, sum(case when achievementLevel = 8 then candidateCount end) [8]
, sum(case when achievementLevel = 9 then candidateCount end) [9]

From warehouse.examSchoolResultsTyped
WHERE RecordType = 'Exam'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
GO

