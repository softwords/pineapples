SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsPupilTablesGender]
AS
Select
ssID,
ptCode Category,
genderCode,
Gender,
case when genderCode = 'M' then ptM else ptF end Children
FROM PupilTables PT
	CROSS JOIN DimensionGender G
GO

