SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 20 July 2019
-- Description:	tabulation of accreditation rnak by criteria and standard
-- ==================================================
-- This view should not be used directly for reporting, it is used by warehouse.BuildInspections
-- to build the table warehouse.AccreditationPerformance
-- Use this warehouse table and it derived aggregation views
-- for reporting
-- ==================================================
CREATE VIEW [pInspectionRead].[SchoolAccreditationPerformance]
AS
Select SUB.*
, P.ptDescription ResultDescription
, P.ptName ResultLevel
from
(
Select inspID
, num ResultValue
, case when [SE.1.1] = NUM THEN 1 ELSE 0 END [SE.1.1_Count]
, case when [SE.1.2] = NUM THEN 1 ELSE 0 END [SE.1.2_Count]
, case when [SE.1.3] = NUM THEN 1 ELSE 0 END [SE.1.3_Count]
, case when [SE.1.4] = NUM THEN 1 ELSE 0 END [SE.1.4_Count]

, case when [SE.1] = NUM THEN 1 ELSE 0 END [SE.1_Count]

, case when [SE.2.1] = NUM THEN 1 ELSE 0 END [SE.2.1_Count]
, case when [SE.2.2] = NUM THEN 1 ELSE 0 END [SE.2.2_Count]
, case when [SE.2.3] = NUM THEN 1 ELSE 0 END [SE.2.3_Count]
, case when [SE.2.4] = NUM THEN 1 ELSE 0 END [SE.2.4_Count]

, case when [SE.2] = NUM THEN 1 ELSE 0 END [SE.2_Count]

, case when [SE.3.1] = NUM THEN 1 ELSE 0 END [SE.3.1_Count]
, case when [SE.3.2] = NUM THEN 1 ELSE 0 END [SE.3.2_Count]
, case when [SE.3.3] = NUM THEN 1 ELSE 0 END [SE.3.3_Count]
, case when [SE.3.4] = NUM THEN 1 ELSE 0 END [SE.3.4_Count]

, case when [SE.3] = NUM THEN 1 ELSE 0 END [SE.3_Count]

, case when [SE.4.1] = NUM THEN 1 ELSE 0 END [SE.4.1_Count]
, case when [SE.4.2] = NUM THEN 1 ELSE 0 END [SE.4.2_Count]
, case when [SE.4.3] = NUM THEN 1 ELSE 0 END [SE.4.3_Count]
, case when [SE.4.4] = NUM THEN 1 ELSE 0 END [SE.4.4_Count]

, case when [SE.4] = NUM THEN 1 ELSE 0 END [SE.4_Count]

, case when [SE.5.1] = NUM THEN 1 ELSE 0 END [SE.5.1_Count]
, case when [SE.5.2] = NUM THEN 1 ELSE 0 END [SE.5.2_Count]
, case when [SE.5.3] = NUM THEN 1 ELSE 0 END [SE.5.3_Count]
, case when [SE.5.4] = NUM THEN 1 ELSE 0 END [SE.5.4_Count]

, case when [SE.5] = NUM THEN 1 ELSE 0 END [SE.5_Count]

, case when [SE.6.1] = NUM THEN 1 ELSE 0 END [SE.6.1_Count]
, case when [SE.6.2] = NUM THEN 1 ELSE 0 END [SE.6.2_Count]
, case when [SE.6.3] = NUM THEN 1 ELSE 0 END [SE.6.3_Count]
, case when [SE.6.4] = NUM THEN 1 ELSE 0 END [SE.6.4_Count]

, case when [SE.6] = NUM THEN 1 ELSE 0 END [SE.6_Count]
-- classroom observations
, case when [CO.1] = NUM THEN 1 ELSE 0 END  [CO.1_Count]
, case when [CO.2] = NUM THEN 1 ELSE 0 END  [CO.2_Count]

FROM pInspectionRead.SchoolAccreditations A
	INNER JOIN MetaNumbers N
		ON N.num in (1,2,3,4)
) SUB
LEFT JOIN Partitions P
	ON P.ptSet = 'SSA'
	AND ptValue = SUB.ResultValue
GO

