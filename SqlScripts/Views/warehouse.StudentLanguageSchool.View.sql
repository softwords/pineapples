SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:    	Ghislain Hachey
-- Create date: 2020
-- Description:	Warehouse - Student Language
--
-- The base view for student language down at the schools enrolment
-- level with all basic disaggregation.
-- This is currently used at least for one SDG indicator
-- but perhaps should be used for any additional extra
-- student data as a more general view for student (Bullied, etc.)
-- =============================================
CREATE VIEW [warehouse].[StudentLanguageSchool]
AS
SELECT
    S.stuID AS stuID,
    S.stuGender AS Gender,
    SE.[schNo],
    [stueYear] AS svyYear,
    SE.stueClass,
    [stueSpEd],
    [stueOutcome],
    [stueOutcomeReason],
    S.[stuLangCode] [LanguageCode],
    DSS.[School Name],
    SS.ssLang [LanguageInstructionCode],
    DSS.[District Code],
    DSS.[District],
    DSS.[Region Code],
    DSS.[Region],
    DSS.[AuthorityCode],
    DSS.[Authority],
    DSS.[AuthorityGroupCode],
    DSS.[AuthorityGroup],
    DSS.[SchoolTypeCode],
    DSS.[SchoolType],
    DSS.[Island Code],
    DSS.[Island]
FROM [dbo].[StudentEnrolment_] SE
JOIN [dbo].[StudentEx] S
    ON SE.stuID = S.stuID
LEFT JOIN DimensionSchoolSurvey DSS
    ON SE.schNo = DSS.[School No]
    AND SE.stueYear = DSS.[Survey Data Year]
LEFT JOIN SchoolSurvey SS
    ON SE.schNo = SS.schNo
    AND SE.stueYear = SS.svyYear
GO

