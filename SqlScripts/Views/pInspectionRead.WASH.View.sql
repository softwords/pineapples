SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 20 July 2019
-- Description:	Base View of school accreditations
-- ==================================================
-- This list combines new format SchoolAccreditations and 'legacy' accreditations, presenting a unified output.
-- Note the for new surveys, the scores presented are converted to a 'level', rather than presenting the
-- number of subcriteria=POSITIVE. This is for consistency with the earlier model.

-- Thus, when working with this view, and its 'frozen' equivalent in the warehouse (warehouse.accreditations)
-- we can treat the numbers as the levels achieved for each criteria - NOT the number of positive answers.
-- We can get the number of positive answers from pInspectionRead.SchoolStandardAssessment, or by going directly to the Xml.
--
-- pInspectionRead.SCHOOL_ACCREDITATION
--			this view named by inspection will be used by proc pInspectionRead.Readinspection

-- pInspectionRead.SchoolAccreditations
--			for backward compatibility, mirrors ( except for column names) the earlier version of this view.
--			This is just a wrapper around pInspectionRead.SCHOOL_ACCREDITATION
--			Note that this view is no longer editable; ie trigger is gone.
-- =============================================
CREATE VIEW [pInspectionRead].[WASH]
AS

SELECT SI.*
, v.value('(//question[id="SW.S.1"]/answer/item)[1]','nvarchar(50)') ToiletType
, v.value('(//question[id="CW.H.2"]/answer/item)[1]','nvarchar(50)') Handwash

from pInspectionRead.SchoolInspections SI
	CROSS APPLY InspectionContent.nodes('/survey') as V(v)
	WHERE InspTypeCode = 'WASH'
	--AND InspectionContent.value('(/survey/version)[1]', 'int') > 0
GO

