SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblRepeatersEst]
AS
SELECT
ESTIMATE_PupilTables.[Survey Year],
ESTIMATE_PupilTables.schNo,
ESTIMATE_PupilTables.[Year of Data],
ESTIMATE_PupilTables.Estimate,
ESTIMATE_PupilTables.[Age of Data],
ESTIMATE_PupilTables.ptID,
ESTIMATE_PupilTables.ssID,
ESTIMATE_PupilTables.ptCode,
ESTIMATE_PupilTables.ptAge,
ESTIMATE_PupilTables.ptLevel,
ESTIMATE_PupilTables.ptM,
ESTIMATE_PupilTables.ptF
FROM
lkpLevels INNER JOIN ESTIMATE_PupilTables
ON lkpLevels.codeCode = ESTIMATE_PupilTables.ptLevel
WHERE (((ESTIMATE_PupilTables.ptCode)='REP' Or
(ESTIMATE_PupilTables.ptCode)='REP1') AND
((ESTIMATE_PupilTables.ptAge)<>0)) OR
(((ESTIMATE_PupilTables.ptCode)='REP' Or
(ESTIMATE_PupilTables.ptCode)='REP1') AND
((lkpLevels.lvlYear)<>1))
GO

