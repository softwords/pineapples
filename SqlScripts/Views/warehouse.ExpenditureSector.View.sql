SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 14 10 2019
-- Description:	District/Sector level financial indicators
-- ==================================================
-- Reports on indicators involving spending at the District level, by sector.
-- this incorporates district enrolments, to give expenditure per head.
-- Built from warehouse.Expenditure, which consolidates the data available in GovtExpenditureState and
-- EdExpenditureState
-- ==================================================
CREATE VIEW [warehouse].[ExpenditureSector]
WITH VIEW_METADATA
AS
Select SUB.*
, EdExpA / EnrolmentApplies EdExpAPerHead
, EdExpB / EnrolmentApplies EdExpBPerHead
FROM
(
Select E.SurveyYear
, DistrictCode
, SectorCode
, sum(EdExpA) EdExpA
, sum(EdExpB) EdExpB

, sum(Enrolment) Enrolment
, sum(case when DistrictCode = 'NATIONAL' then EnrolmentNation else Enrolment end) EnrolmentApplies


from warehouse.Expenditure E
WHERE E.DistrictCode is not null
GROUP BY E.SurveyYear
, DistrictCode
, SectorCode
) SUB
WHERE coalesce(EdExpA, EdExpB, EnrolmentApplies) is not null
GO

