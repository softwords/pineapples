SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paAssessmentLine]
WITH VIEW_METADATA
AS

select PAL.*
, L.paplValue
, L.paplCriterion
from paAssessmentLine_ PAL
	LEFT JOIN paIndicatorLevels_ L
	ON PAL.paindID = L.paindID
	AND PAL.paplCode = L.paplCode
GO

