SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInspectionRead].[SchoolInspections]
WITH VIEW_METADATA
AS
SELECT SI.inspID
	 , SI.schNo
	 , S.schName
	 , SI.inspPlanned as PlannedDate
	 , SI.inspStart AS StartDate
	 , SI.inspEnd AS EndDate
	 , SI.inspNote AS Note
	 , SI.inspBy AS InspectedBy
	 , SI.inspSetID as InspectionSetID
	 , SI.inspXML as InspectionContent
	 , SI.inspResult as InspectionResult
	 , SI.inspSource as SourceId
	 , ISET.inspsetName AS InspectionSetName
	 , ISET.inspsetType AS InspTypeCode
	 , ISET.inspsetYear InspectionYear
	 , IT.intyDesc InspectionType
	 , IT.intyClass as InspectionClass		-- holds SSA if this is a SchoolStandardAssessment
	 , IT.intyPartition as Partition		-- the partition that defines how levels are assigned to percentage scores
	 , SI.pCreateUser
	 , SI.pCreateDateTime
	 , SI.pEditUser
	 , SI.pEditDateTime
	 , SI.pRowversion
	 , SI.pCreateTag


FROM dbo.SchoolInspection SI
	INNER JOIN Schools S
		ON SI.schNo = S.schNo
	LEFT JOIN InspectionSet ISET
		ON SI.inspsetID = ISET.inspsetID
	LEFT JOIN dbo.lkpInspectionTypes IT
		ON ISET.inspsetType = IT.intyCode
-- RMI Quarterly Report is implemented as an Inspection type, but is not to be included in 'inspections'
WHERE ISET.inspsetType <> 'QUARTER'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 28 04 2017
-- Description:	Trigger on [pInspectionRead].[SchoolInspectionsUpdate]
-- =============================================
CREATE TRIGGER [pInspectionRead].[SchoolInspectionsUpdate]
   ON  [pInspectionRead].[SchoolInspections]
   INSTEAD OF INSERT, UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @FiscalYearStart date = isnull(common.sysParam('SchoolYear_StartDate'),'2000-01-01')

	-- populate any InspectionSets we may need
	INSERT INTO InspectionSet
	(
		inspsetName
		, inspsetType
		, inspsetYear
	)
	SELECT DISTINCT 
	coalesce(InspectionSetName, InspectionType + ':' + convert(nvarchar(4), coalesce(InspectionYear, FY.yr)))
	, InspTypeCode
	-- InspectionYear can be explicitly supplied, but if not, it is the FiscalYear of the start date
	, coalesce(InspectionYear, FY.yr)
	From INSERTED
	CROSS APPLY common.SchoolYear(StartDate, @FiscalYearStart) FY
	LEFT JOIN InspectionSet INSPSET
			ON (
				INSERTED.InspectionSetID = INSPSET.inspsetID
			)
			OR (
				INSERTED.InspectionSetID is null
				AND InspectionSetName =  INSPSET.inspsetName
			)
			OR (
				INSERTED.InspectionSetID is null
				AND InspectionSetName IS NULL
				and InspTypeCode = INSPSET.inspsetType
				and InspectionYear = INSPSET.inspSetYear
			)
			OR (
				INSERTED.InspectionSetID is null
				AND InspectionSetName is null
				and InspectionYear is null
				and InspTypeCode = INSPSET.inspsetType
				and INSPSET.inspsetYEar = FY.yr
			)	
			WHERE INSPSET.inspsetID is null

-- temp table to get the best inspSetID for each inspection in INSERTED.
-- we want exactly 1 per row, the above insert guarantees we have at least 1.

Declare @SetID TABLE
(
	createTag uniqueidentifier
	, SetId int
	, inspyear int
	, rowIdx int
)

InSERT INTO @setID
Select pCreateTag
, inspSetID
, inspSetYear
, row_number() OVER (PARTITION BY pCreateTag ORDER BY inspSetID DESC)
FROM INSERTED
CROSS APPLY common.SChoolYear(StartDate, @FiscalYearStart) FY
INNER JOIN InspectionSet INSPSET
			ON (
				INSERTED.InspectionSetID = INSPSET.inspsetID
			)
			OR (
				INSERTED.InspectionSetID is null
				AND InspectionSetName =  INSPSET.inspsetName
			)
			OR (
				INSERTED.InspectionSetID is null
				AND InspectionSetName IS NULL
				and InspTypeCode = INSPSET.inspsetType
				and InspectionYear = INSPSET.inspSetYear
			)
			OR (
				INSERTED.InspectionSetID is null
				AND InspectionSetName is null
				and InspectionYear is null
				and InspTypeCode = INSPSET.inspsetType
				and INSPSET.inspsetYEar = FY.yr
			)

	-- INSERT ------- 

	-- create the school inspection record

	INSERT INTO SchoolInspection
	 ( schNo
           ,inspsetID
		   , inspPlanned
		   ,inspStart
           ,inspEnd
           ,inspNote
           ,inspBy
		   , inspXml
		   , inspResult
		   , inspSource
		   ,pCreateUser
		   ,pCreateDateTime
		   ,pEditUser
           ,pEditDateTime
		   ,pCreateTag
	 )
	 SELECT 

		INSERTED.schNo
		,INSPSET.SetID
		,INSERTED.PlannedDate
		,INSERTED.StartDate
        ,INSERTED.EndDate
        ,INSERTED.Note
        ,INSERTED.InspectedBy
		, INSERTED.InspectionContent
		, INSERTED.InspectionResult
		, INSERTED.SourceId

		,INSERTED.pCreateUser
		,INSERTED.pCreateDateTime
		,INSERTED.pCreateUser		-- edit = create on insert
		,INSERTED.pCreateDateTime
		, INSERTED.pCreateTag

	From INSERTED
		INNER JOIN @setID INSPSET
		ON INSERTED.pCreateTag = INSPSET.createTag
		AND INSPSET.rowIdx = 1
	WHERE INSERTED.inspID is null		


 -- end insert

 -- update
 -- the only thing we can update on SchoolInspection is the inspsetID - if we change the SA to a different year ( a correction)
	UPDATE SchoolInspection
		SET inspsetID = INSPSET.setID
		,inspStart = INSERTED.StartDate
        ,inspEnd = INSERTED.EndDate
        ,inspNote = INSERTED.Note
        ,inspBy = INSERTED.InspectedBy
		, inspXml = INSERTED.InspectionContent
		, inspResult = INSERTED.InspectionResult
		, inspSource = INSERTED.SourceId
	    ,pEditUser = INSERTED.pEditUser
		,pEditDateTime = INSERTED.pEditDateTime

	FROM SchoolInspection

	INNER JOIN INSERTED
		ON SchoolInspection.inspID = INSERTED.inspID
	CROSS APPLY common.SchoolYear(INSERTED.StartDate, @FiscalYearStart) FY
	INNER JOIN DELETED	
		ON INSERTED.inspID = DELETED.inspID
	INNER JOIN @setID INSPSET
	ON INSERTED.pCreateTag = INSPSET.createTag
	AND INSPSET.rowIdx = 1


	-- end update

	-- delete


	DELETE FROM SchoolInspection
	FROM SchoolInspection
	INNER JOIN DELETED D
		ON SchoolInspection.inspID = D.inspID
	LEFT JOIN INSERTED I
		ON D.inspID = I.inspID
	WHERE I.inspID is null

	-- end delete

END

GO

