SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paAssessmentScore]
WITH VIEW_METADATA
AS
Select paID
, convert(decimal(4,1), avg(RawComAvg))paScore
FROM
paCompetencyAvg

GROUP BY paID
GO

