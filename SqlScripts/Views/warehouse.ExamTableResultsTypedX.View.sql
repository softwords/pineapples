SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 01/11/2023
-- Description:	Warehouse - Extended version of the [warehouse].[ExamTableResultsTyped] table
--
-- This version includes support for more disaggregation (e.g. Island, Region)
-- but has a result contains a lot more data record and will incur a penalty hit.
-- =============================================
CREATE VIEW [warehouse].[ExamTableResultsTypedX]
AS
SELECT [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[Gender]
      ,ESRT.[DistrictCode]
      ,ESRT.[District]
      ,ESRT.[SchoolTypeCode]
      ,ESRT.[SchoolType]
      ,ESRT.[AuthorityCode]
      ,ESRT.[Authority]
      ,ESRT.[AuthorityGovtCode]
      ,ESRT.[AuthorityGovt]
	  ,DSS.[Island Code] [IslandCode]
	  ,DSS.[Island]
	  ,DSS.[Region Code] [RegionCode]
	  ,DSS.[Region]
      ,[RecordType]
      ,[ID]
      ,[Key]
      ,[Description]
      ,[achievementLevel]
      ,[achievementDesc]
      ,SUM([indicatorCount]) [indicatorCount]
      ,SUM([weight]) [weight]
      ,SUM([candidateCount]) [candidateCount]
  FROM [warehouse].[ExamSchoolResultsTyped] ESRT
  INNER JOIN [warehouse].[dimensionSchoolSurvey] DSS ON ESRT.schNo = DSS.[School No] AND ESRT.examYear = DSS.[Survey Data Year]
  GROUP BY [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[Gender]
      ,ESRT.[DistrictCode]
      ,ESRT.[District]
      ,ESRT.[SchoolTypeCode]
      ,ESRT.[SchoolType]
      ,ESRT.[AuthorityCode]
      ,ESRT.[Authority]
      ,ESRT.[AuthorityGovtCode]
      ,ESRT.[AuthorityGovt]
	  ,DSS.[Island Code]
	  ,DSS.[Island]
	  ,DSS.[Region Code]
	  ,DSS.[Region]
      ,[RecordType]
      ,[ID]
      ,[Key]
      ,[Description]
      ,[achievementLevel]
      ,[achievementDesc]
GO

