SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 7/3/2020
-- Description:	Warehouse - Over and Under Age reporting
--
-- Two versions of = (at official age) and OVER age are supported:
--  - A standard version in which at age is strictly those of official age and OVER age are those with age > then official age
--  - A SDG 4.1.6 version where the at age is a little more liberal and includes those with age up to 1 year above official age and OVER at least 2 years over official age
--
-- Using enrolment data and definitions of age ranges for class levels,
-- Calculate the numbers:
-- OVER Age (ie > class level age range)
-- UNDER age (< class level age range)
-- = Age (at offical age range)
-- At Age is the same as nEnrol on Enrolment Ratio views.
-- Note that OVERS are over the class level range; a pupil may be OVERAGE for their class level,
-- but still within the age range for the education level (see warehouse.EdLevelAge)
--
-- =============================================
CREATE VIEW [warehouse].[ClassLevelAge]
AS
Select surveyYear
	, ClassLevel
	, districtCode
	, authorityCode
	, SchoolTypecode
	, yearOfEd
	, EdLevel
	, GenderCode
	, sum(case when ClassLevelOfficialAge = 'UNDER' then Enrol else null end ) UnderAge
	, sum(case when ClassLevelOfficialAge = '=' then Enrol else null end ) OfficialAge
	, sum(case when ClassLevelOfficialAge = 'OVER' then Enrol else null end ) OverAge
	, sum(case when ClassLevelOfficialAgeSDG = 'UNDER' then Enrol else null end ) UnderAgeSDG
	, sum(case when ClassLevelOfficialAgeSDG = '=' then Enrol else null end ) OfficialAgeSDG
	, sum(case when ClassLevelOfficialAgeSDG = 'OVER' then Enrol else null end ) OverAgeSDG
	, sum(Enrol) Enrol
	, sum(case when Estimate = 1 then Enrol else null end ) EstimatedEnrol
	From Warehouse.enrolmentRatios
	GROUP BY
	surveyYear, ClassLevel, districtCode, authorityCode, SchoolTypecode, yearOfEd, EdLevel, GenderCode
GO

