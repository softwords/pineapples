SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @T TABLE
--(
--	schoID int
--	, schoStatus nvarchar(10)
--	, scholCode nvarchar(10)
--	, ReqGPA decimal(3,2)
--	, CheckSemesters int
--	, ConsecutiveSemesters int
--	, NumGPARecorded int
--	, maxGPA decimal(3,2)
--	, lastGPA decimal(3,2)
--	, lastYear int
--	, lastSemester int
--	, newStatus nvarchar(10)

--)
--INSERT INTO @T
CREATE VIEW [pScholarshipRead].[StatusReview]
AS
Select
SCHOL.*
, reqGPA
, CheckSemesters
, ConsecutiveSemesters
, numGPARecorded
, maxGPA
, lastGPA
, lastYear
, lastSemester
, case when ConsecutiveSemesters = CheckSemesters
				and NumGPARecorded = CheckSemesters
				and maxGPA < ReqGPA then 'TERMINATED'
		when lastGPA < reqGPA then 'PROBATION'
	end newStatus
FROM
(
Select schoID, schoStatus, scholCode, reqGPA, CheckSemesters, max(RN) ConsecutiveSemesters, count(posGPA) numGPARecorded
, max(posGPA) maxGPA
, max(case when RN = 1 then posGPA else null end) lastGPA
, max(case when RN = 1 then posYear else null end) lastYear
, max(case when RN = 1 then posSemester else null end) lastSemester
from
(
	Select SUB.*
	, TT.ConsecutiveSemesters CheckSemesters
	, TT.ReqGPA from
	(
	Select S.schoID, posGPA, posYear, posSemester
	, S.schoStatus , S.scholCode
	, case when SS.schoID is null then 0
		else row_number() OVER (PARTITION BY S.schoID order by posYear DESC, posSemester DESC)
		end RN
	 from  Scholarships S
		LEFT JOIN ScholarshipStudy_ SS
			ON SS.schoID = S.schoID
	) SUB
	INNER JOIN ScholarshipTypes TT
		ON SUB.scholCode = TT.codeCode
	WHERE RN <= TT.ConsecutiveSemesters --@num
) SUB2
group by schoID, schoStatus, scholCode, reqGPA, CheckSemesters
) SUB3
INNER JOIN Scholarships SCHOL
ON SUB3.schoID = SCHOL.schoID
GO

