SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 08/04/2022
-- Description:	Warehouse - Exam Results (whole test) at the candidate level
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsTestCandidateX]
AS
Select excID
, studentID
, excGiven
, excMiddleNames
, excFamilyName
, examID
, examCode
, examYear
, examName
, schNo
, Gender
, sum(candidateCount) CandidateCount
, sum(case when achievementLevel = 0 then candidateCount end) [0]
, sum(case when achievementLevel = 1 then candidateCount end) [1]
, sum(case when achievementLevel = 2 then candidateCount end) [2]
, sum(case when achievementLevel = 3 then candidateCount end) [3]
, sum(case when achievementLevel = 4 then candidateCount end) [4]
, sum(case when achievementLevel = 5 then candidateCount end) [5]
, sum(case when achievementLevel = 6 then candidateCount end) [6]
, sum(case when achievementLevel = 7 then candidateCount end) [7]
, sum(case when achievementLevel = 8 then candidateCount end) [8]
, sum(case when achievementLevel = 9 then candidateCount end) [9]

From warehouse.ExamCandidateResultsTyped
WHERE RecordType = 'Exam'
GROUP BY excID
, studentID
, excGiven
, excMiddleNames
, excFamilyName
, examID
, examCode
, examYear
, examName
, schNo
, Gender
GO

