SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Teacher Counts
--
-- Teacher counts aggregated by district and sector.
-- Derived from warehouse.schoolTeacherCount table.
-- =============================================
CREATE VIEW [warehouse].[TeacherCountDistrictR]
AS
Select
 SurveyYear
, AgeGroup
, DistrictCode
, District
-- this logic assumes that the column 'Support' is a text field in SchoolStaffCount
-- and that records are disaggregated between 'Support' and (null) values - ie teaching staff
-- warehouse.schoolTeacherCount is a VIEW that 'denormalises' support numbers in the same way as this view
-- Certified, Qualified Certqual do NOT include non-teaching staff
-- see Issue #518

, sum(NumSupportStaff) NumSupportStaff
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual

, sum(case when GenderCode='M' then NumSupportStaff end) NumSupportStaffM
, sum(case when GenderCode='M' then NumTeachers end) NumTeachersM
, sum(case when GenderCode='M' then Certified end) CertifiedM
, sum(case when GenderCode='M' then Qualified end) QualifiedM
, sum(case when GenderCode='M' then CertQual end) CertQualM

, sum(case when GenderCode='F' then NumSupportStaff end) NumSupportStaffF
, sum(case when GenderCode='F' then NumTeachers end) NumTeachersF
, sum(case when GenderCode='F' then Certified end) CertifiedF
, sum(case when GenderCode='F' then Qualified end) QualifiedF
, sum(case when GenderCode='F' then CertQual end) CertQualF

--- TEMP FIX to CRITICAL FAILURE ---
--- REMOIVE THIS AFTER DEPLOYMENT OF FIX TO ISSUE 600 ---
---------------------------------------------------------
, null Support
---------------------------------------------------------

from warehouse.TeacherCountDistrict S
GROUP BY
SurveyYear
, AgeGroup
, DistrictCode
, District
GO

