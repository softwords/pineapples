SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- View
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 03 05 2022
-- Description:	TeacherPerformance is a view containing denormalized data from various sources requested by
-- Weison to fulfill a request by IQBE consultants in FSM.

/**
 There are a number of caveats with this data warehouse view and the data it offers.

 * It is currently in a format mostly for FSM (especially the exams performance data which is hard coded
   by exam name different in each country)
 * Teacher certification data entry needs improvement from the teacher certification division
   to be considered excellent.
 * Exams data for past years is missing because of a previous NDOE employee. Effort needs to be done to
   reclaim this data one way or the other to get all historical exams data into the FedEMIS.
 * Data for exams results is for the school at which the teacher was teaching (and thus contributing)
   and not the results of the students this teacher was teaching. While the former (and what is currently
   available) is not has good as the later it can still provide insight. Current work is being
   undertaken aiming to clean the exams data (candidates/teachers in particular) and this more precise
   data correlation will (hopefully) be available soon!
 * The offered Exams percentage data is not the same as the old SOE system. For example, for
   Math Grade 4 0% at Level 1, 15% at level 2, 60% at level 3 and 25% at level 4 the percentages are
   for the whole test and are the actual percentages of candidates performing at those levels. The old SOE
   system was not the percentage of candidates but the percentage of indicators (on which candidates
   are tested). They are similar but different. FedEMIS supports both the old and new simpler format
   most often requested in international key performance indicators.
 * As with most data in the system, most recent years have more complete and better data (which keeps
   on improving)
 * Data is packaged in the way it was requested by Weison/David and not necessarily the best format
   for flexibility, re-usability and advanced data analysis. This should be considered a draft version
   of a larger on-going vision of how integrated data can be used in advanced analysis. Consider exploring
   more the XY charts also.
 * Teacher IDs is a new thing that is still not perfect. Especially in older years it was common to get
   teachers entered into the system twice (or more) with slightly different names (or other data such as
   date of birth). This is better in later years but as a result it is possible for a single teacher to
   be in the FedEMIS with different teacher IDs. FedEMIS has a special tool to improve this called the
   Teacher De-duplication Tool that can be used to consolidate these "duplicates" and clean this teacher
   data. It does not affect total numbers from year to year but does becomes important when perfecting
   historical data.
 * The only teacher qualification taken into consideration is the highest. For example, a teacher with
   a known Bachelor and a known Master the Master is the one recorded for the teacher in here.
 * The school accreditation is currently linked to calendar years in the FedEMIS (as opposed to a
   School Year). This means a school accreditation inspection done in November 2021 is currently
   under year 2021 (while it would be under 2022 under a school year scheme). There is work this year to
   build support for both calendar and school year with the latter being the more commonly used in FSM.
   This does not have as huge an effect as it may sound for this data which makes use of cummulative to a year
   (and not accredited in year) snapshot of the schools accreditation status.
 * School Accreditation are not shown for ECE schools as they are accredited as part of the "sister" Primary school
   hence those school will have no record for school accreditation.

 That said, this warehouse table offers a really good starting point that is difficult to achieve
 in most environment. Most of these above is being improved on the short term roadmap (this year).

 Assumptions (the query code to package this data makes some assumptions that are listed below):

 * The years of experience is not data currently collected explicitly. To provide some best estimates
   with available data we assume the years of experience starts at the date of hire and go up to
   the school year analysed.
*/

-- History Log:
--   * DD MMM YYYY, Author Name, Short description of change.
-- =============================================
CREATE VIEW [warehouse].[TeacherPerformance]
WITH VIEW_METADATA
AS
WITH CTE AS (
SELECT SurveyYear [School Year]
	, T.schName [School Name]
	, T.SchNo
	, T.District
	, T.SchoolType
	, T.Region
	, T.tGiven + ' ' + T.tSurname Teacher
	, T.TID
	, T.BirthYear
	, TI.tDatePSAppointed [Date of Hire]
	, T.Qualification
	, CASE
		WHEN T.Qualification IS NULL THEN 0
		WHEN T.Qualification = 'High School Diploma' THEN 0
		WHEN T.Qualification = 'Associate of Arts' THEN 2
		WHEN T.Qualification = 'Associate of Applied Arts' THEN 2
		WHEN T.Qualification = 'Associate of Applied Science' THEN 2
		WHEN T.Qualification = 'Associate of Science' THEN 2
		WHEN T.Qualification = 'Bachelor of Arts' THEN 4
		WHEN T.Qualification = 'Bachelor of Science' THEN 4
		WHEN T.Qualification = 'Certificate' THEN 1
		WHEN T.Qualification = 'Early Childhood Education' THEN 2
		WHEN T.Qualification = 'Masters of Arts' THEN 5
		WHEN T.Qualification = 'Masters of Business Administration' THEN 5
		WHEN T.Qualification = 'Masters of Science' THEN 6
		WHEN T.Qualification = 'Masters of Education' THEN 5
		WHEN T.Qualification = 'Doctor of Philosophy' THEN 8
		WHEN T.Qualification = 'Masters of Public Administration' THEN 5
		WHEN T.Qualification = '3rd Year Certificate' THEN 3
		WHEN T.Qualification = 'High School Diploma (GED)' THEN 0
		WHEN T.Qualification = 'Bachelor of Business Administration' THEN 4
		ELSE 0
		END [Years of Education]
	, T.ISCEDBroadField [Field of Study (ISCEDBroadField)]
	, T.ISCEDNarrowField [Field of Study (ISCEDNarrowField)]
	, T.ISCEDDetailedField [Field of Study (ISCEDDetailedField)]
	, T.Institution
	, T.Salary
	, CASE
		WHEN T.Year = 0 THEN NULL
		ELSE T.Year
		END [Year]
	, CASE -- By no means great rules but good enough for now
		WHEN YEAR(TI.tDatePSAppointed) > SurveyYear THEN NULL -- Date of hire in the future, discard
		ELSE ([SurveyYear] - YEAR(TI.tDatePSAppointed)) -- Assume years of experience is school year minus date of hire into the school system
		END [Years of  Experience]
FROM [warehouse].[TeacherLocationAndQualificationsHighest] T
INNER JOIN [dbo].[TeacherIdentity] TI ON T.TID = TI.tID
)

SELECT CTE.[School Year]
	, CTE.[School Name]
	, CTE.SchNo
	, CTE.District
	, CTE.SchoolType
	, CTE.Region
	, Teacher
	, CTE.TID
	, BirthYear
	, [Date of Hire]
	, Qualification
	, [Years of Education]
	, [Field of Study (ISCEDBroadField)]
	, [Field of Study (ISCEDNarrowField)]
	, [Field of Study (ISCEDDetailedField)]
	, Institution
	, Year
	, [Years of  Experience]
	, [Years of  Experience] + [Years of Education] [Years of Experience (including years of Education)]
	, Salary

	-- Teacher certification data
	, CERTS.[Certificate Type]
	, CASE
		WHEN CERTS.[Certificate Year] = 0 THEN NULL
		ELSE CERTS.[Certificate Year]
		END [Certificate Year]

	-- School Accreditation data
	,[InspectionResult] [Accreditation Level]
      ,CASE [SE.1]
		WHEN 1 THEN 'Level 1'
		WHEN 2 THEN 'Level 2'
		WHEN 3 THEN 'Level 3'
		WHEN 4 THEN 'Level 4'
		END [Standard 1]
      ,CASE [SE.2]
		WHEN 1 THEN 'Level 1'
		WHEN 2 THEN 'Level 2'
		WHEN 3 THEN 'Level 3'
		WHEN 4 THEN 'Level 4'
		END [Standard 2]
      ,CASE [SE.3]
		WHEN 1 THEN 'Level 1'
		WHEN 2 THEN 'Level 2'
		WHEN 3 THEN 'Level 3'
		WHEN 4 THEN 'Level 4'
		END [Standard 3]
      ,CASE [SE.4]
		WHEN 1 THEN 'Level 1'
		WHEN 2 THEN 'Level 2'
		WHEN 3 THEN 'Level 3'
		WHEN 4 THEN 'Level 4'
		END [Standard 4]
      ,CASE [SE.5]
		WHEN 1 THEN 'Level 1'
		WHEN 2 THEN 'Level 2'
		WHEN 3 THEN 'Level 3'
		WHEN 4 THEN 'Level 4'
		END [Standard 5]
      ,CASE [SE.6]
		WHEN 1 THEN 'Level 1'
		WHEN 2 THEN 'Level 2'
		WHEN 3 THEN 'Level 3'
		WHEN 4 THEN 'Level 4'
		END [Standard 6]

	  -- Standard test of achievement student performance data
	  ,[Math G4 Percent Level 1]
	  ,[Math G4 Percent Level 2]
	  ,[Math G4 Percent Level 3]
	  ,[Math G4 Percent Level 4]
	  ,[Math G6 Percent Level 1]
	  ,[Math G6 Percent Level 2]
	  ,[Math G6 Percent Level 3]
	  ,[Math G6 Percent Level 4]
	  ,[Reading G6 Percent Level 1]
	  ,[Reading G6 Percent Level 2]
	  ,[Reading G6 Percent Level 3]
	  ,[Reading G6 Percent Level 4]
	  ,[Math G8 Percent Level 1]
	  ,[Math G8 Percent Level 2]
	  ,[Math G8 Percent Level 3]
	  ,[Math G8 Percent Level 4]
	  ,[Reading G8 Percent Level 1]
	  ,[Reading G8 Percent Level 2]
	  ,[Reading G8 Percent Level 3]
	  ,[Reading G8 Percent Level 4]
	  ,[Math G10 Percent Level 1]
	  ,[Math G10 Percent Level 2]
	  ,[Math G10 Percent Level 3]
	  ,[Math G10 Percent Level 4]
	  ,[Reading G10 Percent Level 1]
	  ,[Reading G10 Percent Level 2]
	  ,[Reading G10 Percent Level 3]
	  ,[Reading G10 Percent Level 4]
FROM CTE

LEFT OUTER JOIN -- Data for the latest certification of teachers
(
	SELECT TT.tID
		, CASE
			WHEN TQ.codeDescription = 'National Standardized Test for Teachers' THEN 'NSTT passed but no record of Certificate Type Recorded'
			ELSE TQ.codeDescription
			END [Certificate Type]
		, ISNULL(trYear,0) [Certificate Year]
	FROM TeacherTraining TT
	INNER JOIN lkpTeacherQual TQ ON TT.trQual = TQ.codeCode
	INNER JOIN -- Get latest (and also account for what seems like possible duplicate certification records
		(SELECT tID, MAX(ISNULL(trYear,0)) AS LatestCertYear, MAX(ISNULL(trEffectiveDate,0)) AS LatestCert
		FROM TeacherTraining TT2
		INNER JOIN lkpTeacherQual TQ ON TT2.trQual = TQ.codeCode
		WHERE codeCertified = 1
		GROUP BY tID) TT3 ON TT.tID = TT3.tID AND ISNULL(TT.trYear,0) = TT3.LatestCertYear AND ISNULL(TT.trEffectiveDate,0) = TT3.LatestCert
	WHERE codeCertified = 1
) CERTS ON CTE.TID = CERTS.tID

LEFT OUTER JOIN [warehouse].[AccreditationClassic] A -- Data for accreditation
ON CTE.SchNo = A.schNo AND CTE.[School Year] = A.SurveyYear

LEFT OUTER JOIN -- Data for the performance of students in the standardized test of achievement
(
	SELECT [School Year]
		,[schNo]
		,MAX([Math G4 Percent Level 1]) [Math G4 Percent Level 1]
		,MAX([Math G4 Percent Level 2]) [Math G4 Percent Level 2]
		,MAX([Math G4 Percent Level 3]) [Math G4 Percent Level 3]
		,MAX([Math G4 Percent Level 4]) [Math G4 Percent Level 4]

		,MAX([Math G6 Percent Level 1]) [Math G6 Percent Level 1]
		,MAX([Math G6 Percent Level 2]) [Math G6 Percent Level 2]
		,MAX([Math G6 Percent Level 3]) [Math G6 Percent Level 3]
		,MAX([Math G6 Percent Level 4]) [Math G6 Percent Level 4]

		,MAX([Reading G6 Percent Level 1]) [Reading G6 Percent Level 1]
		,MAX([Reading G6 Percent Level 2]) [Reading G6 Percent Level 2]
		,MAX([Reading G6 Percent Level 3]) [Reading G6 Percent Level 3]
		,MAX([Reading G6 Percent Level 4]) [Reading G6 Percent Level 4]

		,MAX([Math G8 Percent Level 1]) [Math G8 Percent Level 1]
		,MAX([Math G8 Percent Level 2]) [Math G8 Percent Level 2]
		,MAX([Math G8 Percent Level 3]) [Math G8 Percent Level 3]
		,MAX([Math G8 Percent Level 4]) [Math G8 Percent Level 4]

		,MAX([Reading G8 Percent Level 1]) [Reading G8 Percent Level 1]
		,MAX([Reading G8 Percent Level 2]) [Reading G8 Percent Level 2]
		,MAX([Reading G8 Percent Level 3]) [Reading G8 Percent Level 3]
		,MAX([Reading G8 Percent Level 4]) [Reading G8 Percent Level 4]

		,MAX([Math G10 Percent Level 1]) [Math G10 Percent Level 1]
		,MAX([Math G10 Percent Level 2]) [Math G10 Percent Level 2]
		,MAX([Math G10 Percent Level 3]) [Math G10 Percent Level 3]
		,MAX([Math G10 Percent Level 4]) [Math G10 Percent Level 4]

		,MAX([Reading G10 Percent Level 1]) [Reading G10 Percent Level 1]
		,MAX([Reading G10 Percent Level 2]) [Reading G10 Percent Level 2]
		,MAX([Reading G10 Percent Level 3]) [Reading G10 Percent Level 3]
		,MAX([Reading G10 Percent Level 4]) [Reading G10 Percent Level 4]
	FROM (
		SELECT [examYear] [School Year]
			,[schNo]
			,CASE
				WHEN examName = 'Year 4 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([1]) )/ SUM(Candidates), 0), 2)
				END [Math G4 Percent Level 1]
			,CASE
				WHEN examName = 'Year 4 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([2]) )/ SUM(Candidates), 0), 2)
				END [Math G4 Percent Level 2]
			,CASE
				WHEN examName = 'Year 4 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([3]) )/ SUM(Candidates), 0), 2)
				END [Math G4 Percent Level 3]
			,CASE
				WHEN examName = 'Year 4 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([4]) )/ SUM(Candidates), 0), 2)
				END [Math G4 Percent Level 4]
			,CASE
				WHEN examName = 'Year 6 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([1]) )/ SUM(Candidates), 0), 2)
				END [Math G6 Percent Level 1]
			,CASE
				WHEN examName = 'Year 6 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([2]) )/ SUM(Candidates), 0), 2)
				END [Math G6 Percent Level 2]
			,CASE
				WHEN examName = 'Year 6 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([3]) )/ SUM(Candidates), 0), 2)
				END [Math G6 Percent Level 3]
			,CASE
				WHEN examName = 'Year 6 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([4]) )/ SUM(Candidates), 0), 2)
				END [Math G6 Percent Level 4]
			,CASE
				WHEN examName = 'Year 6 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([1]) )/ SUM(Candidates), 0), 2)
				END [Reading G6 Percent Level 1]
			,CASE
				WHEN examName = 'Year 6 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([2]) )/ SUM(Candidates), 0), 2)
				END [Reading G6 Percent Level 2]
			,CASE
				WHEN examName = 'Year 6 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([3]) )/ SUM(Candidates), 0), 2)
				END [Reading G6 Percent Level 3]
			,CASE
				WHEN examName = 'Year 6 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([4]) )/ SUM(Candidates), 0), 2)
				END [Reading G6 Percent Level 4]
			,CASE
				WHEN examName = 'Year 8 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([1]) )/ SUM(Candidates), 0), 2)
				END [Math G8 Percent Level 1]
			,CASE
				WHEN examName = 'Year 8 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([2]) )/ SUM(Candidates), 0), 2)
				END [Math G8 Percent Level 2]
			,CASE
				WHEN examName = 'Year 8 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([3]) )/ SUM(Candidates), 0), 2)
				END [Math G8 Percent Level 3]
			,CASE
				WHEN examName = 'Year 8 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([4]) )/ SUM(Candidates), 0), 2)
				END [Math G8 Percent Level 4]
			,CASE
				WHEN examName = 'Year 8 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([1]) )/ SUM(Candidates), 0), 2)
				END [Reading G8 Percent Level 1]
			,CASE
				WHEN examName = 'Year 8 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([2]) )/ SUM(Candidates), 0), 2)
				END [Reading G8 Percent Level 2]
			,CASE
				WHEN examName = 'Year 8 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([3]) )/ SUM(Candidates), 0), 2)
				END [Reading G8 Percent Level 3]
			,CASE
				WHEN examName = 'Year 8 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([4]) )/ SUM(Candidates), 0), 2)
				END [Reading G8 Percent Level 4]
			,CASE
				WHEN examName = 'Year 10 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([1]) )/ SUM(Candidates), 0), 2)
				END [Math G10 Percent Level 1]
			,CASE
				WHEN examName = 'Year 10 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([2]) )/ SUM(Candidates), 0), 2)
				END [Math G10 Percent Level 2]
			,CASE
				WHEN examName = 'Year 10 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([3]) )/ SUM(Candidates), 0), 2)
				END [Math G10 Percent Level 3]
			,CASE
				WHEN examName = 'Year 10 Maths'
				THEN ROUND(ISNULL(convert(float, SUM([4]) )/ SUM(Candidates), 0), 2)
				END [Math G10 Percent Level 4]
			,CASE
				WHEN examName = 'Year 10 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([1]) )/ SUM(Candidates), 0), 2)
				END [Reading G10 Percent Level 1]
			,CASE
				WHEN examName = 'Year 10 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([2]) )/ SUM(Candidates), 0), 2)
				END [Reading G10 Percent Level 2]
			,CASE
				WHEN examName = 'Year 10 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([3]) )/ SUM(Candidates), 0), 2)
				END [Reading G10 Percent Level 3]
			,CASE
				WHEN examName = 'Year 10 Reading'
				THEN ROUND(ISNULL(convert(float, SUM([4]) )/ SUM(Candidates), 0), 2)
				END [Reading G10 Percent Level 4]
		FROM [warehouse].[ExamResultsTestSchoolX] AS ERTSX
		GROUP BY examName
			,[examYear]
			,[schNo]
	) T
	GROUP BY [School Year]
		,[schNo]
) EXAMS ON CTE.[School Year] = EXAMS.[School Year] AND CTE.SchNo = EXAMS.schNo
GO

