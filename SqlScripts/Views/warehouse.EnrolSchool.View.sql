SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: Jan 2020
-- Description:	Warehouse - Total Enrolments by school
--
-- This is a simple consolidation of warehouse.enrol to group by school and Gender
-- This or its 'R' version can produce simple column charts of enrolment across all schools
--
-- History Log:
--   * 28 October 2021, Ghislain Hachey, Add missing Rep, Trin, Trout, Boarders, Disab, Dropout, PSA.
-- =============================================
CREATE VIEW
[warehouse].[EnrolSchool]
AS
Select SurveyYear
, E.schNo
, DSS.[District Code] DistrictCode
, District
, [AuthorityCode]
, Authority
, AuthorityGovtCode
, AuthorityGovt
, [SchoolTypeCode]
, SchoolType
, [School Name] SchoolName
, E.SurveyDimensionID
, GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA
, sum(Completed) Completed
from warehouse.enrol E
LEFT JOIN warehouse.dimensionSchoolSurvey DSS
	ON E.SurveyDimensionID = DSS.[Survey ID]
group by
E.schNo, SurveyYear, GenderCode, SurveyDimensionID
, DSS.[District Code]
, District
, [AuthorityCode]
, Authority
, AuthorityGovtCode
, AuthorityGovt
, [SchoolTypeCode]
, SchoolType
, [School Name]
GO

