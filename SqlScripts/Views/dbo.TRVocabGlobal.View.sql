SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRVocabGlobal]
AS
SELECT     vxID,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN vxEng
	WHEN 1 THEN vxFr
	WHEN 2 THEN vxBis

END , vxEng) AS vxEng,vxFr,vxBis,vxuser,vxObjType,vxObjName

FROM         dbo.trVocab

WHERE vxObjName = 'Global'
GO
GRANT SELECT ON [dbo].[TRVocabGlobal] TO [public] AS [dbo]
GO

