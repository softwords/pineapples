SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/04/2022
-- Description:	Warehouse - Exam Results (whole test) aggregated at School level (normalized version)
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsTestSchool]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, achievementLevel
, achievementDesc
, sum(candidateCount) Candidates
FROM warehouse.examSchoolResultsTyped
WHERE RecordType = 'Exam'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, achievementLevel
, achievementDesc
GO

