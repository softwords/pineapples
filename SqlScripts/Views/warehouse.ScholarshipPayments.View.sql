SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from ScholarshipStudy_ SS
--INNER JOIN Scholarships S
-- ON SS.schoID = S.schoID
CREATE VIEW [warehouse].[ScholarshipPayments]
WITH VIEW_METADATA
AS

Select
S.scholCode ScholarshipType
, S.schrYear Round
, S.schoStatus StatusCode
, STAT.codeDescription Status
, stuGender Gender
, SS.fosCode MajorCode
, FOS.fosName Major
, FOS.fosGrpCode MajorGroupCode
, FOSG.codeDescription MajorGroup

, SS.posYear AcademicYear
, SS.posSemester AcademicSemester
, convert(nvarchar(4),posYear) + ':' + convert(nvarchar(2), posSemester) AcademicYearSemester
, SS.gyIndex TertiaryGradeCode
, TG.gyName TertiaryGrade
, instCode
, SCHYR.yr PaymentYear
, sum(posAmount) Amount
, count(*) Number
from Scholarships S
	LEFT JOIN ScholarshipStudy_ SS
		ON S.schoID = SS.schoID
	CROSS APPLY common.SchoolYear(SS.posPaid, common.SchoolYearStartDate()) SCHYR
	LEFT JOIN lkpScholarshipStatus STAT
		ON S.schoStatus = STAT.codeCode
	LEFT JOIN FieldOfStudy FOS
		ON SS.fosCode = FOS.fosCode
	LEFT JOIN dbo.ScholarshipRounds R
		ON S.schrYear = R.schrYear
		AND S.scholCode = R.scholCode
	LEFT JOIN FieldOfStudyGroups FOSG
		ON FOS.fosgrpCode = FOSG.codeCode
	LEFT JOIN lkpTertiaryGrades TG
		ON SS.gyIndex = TG.gyIndex
GROUP BY
S.scholCode
, S.schrYear
, S.stuGender
, S.schoStatus
, STAT.codeDescription
, SS.fosCode
, SS.posYear
, SS.posSemester
, SS.gyIndex
, TG.gyName
, FOS.fosName
, FOS.fosGrpCode
, FOSG.codeDescription

, STAT.codeDescription
, SCHYR.yr
, instCode
GO

