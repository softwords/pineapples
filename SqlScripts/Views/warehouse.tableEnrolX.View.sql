SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2023
-- Description:	An extended version of the warehouse.tableEnrol which includes more disaggregation such as Islands, Region, etc.
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation
--      warehouse.Enrol*

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
--      warehouse.Enrol*R
-- =============================================
CREATE VIEW
[warehouse].[tableEnrolX]
AS
Select [SurveyYear]
      ,[Estimate]
      ,[ClassLevel]
      ,[Age]
      ,[GenderCode]
      ,[DistrictCode]
	  ,[DSS].[District]
      ,[AuthorityCode]
	  ,[DSS].[Authority]
      ,[SchoolTypeCode]
	  ,[DSS].[SchoolType]
	  ,[DSS].[AuthorityGovtCode]
	  ,[DSS].[AuthorityGovt]
	  ,[DSS].[AuthorityGroupCode]
	  ,[DSS].[AuthorityGroup]
	  ,[DSS].[AuthorityTypeCode]
	  ,[DSS].[AuthorityType]
	  ,[DSS].[Island Code] [IslandCode]
	  ,[DSS].[Island]
	  ,[DSS].[Region Code] [RegionCode]
	  ,[DSS].[Region]
      ,sum(Enrol) Enrol
      ,sum(Rep) Rep
      ,sum(Trin) Trin
      ,sum(Trout) Trout
      ,sum(Boarders) Boarders
      ,sum(Disab) Disab
      ,sum(Dropout) Dropout
      ,sum(PSA) PSA
      ,sum(Expelled) Expelled
      ,sum(Completed) Completed

from warehouse.Enrol E
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON E.SurveyDimensionID = [DSS].[Survey ID]
GROUP BY [SurveyYear]
      ,[Estimate]
      ,[ClassLevel]
      ,[Age]
      ,[GenderCode]
      ,[DistrictCode]
	  ,[DSS].[District]
      ,[AuthorityCode]
	  ,[DSS].[Authority]
      ,[SchoolTypeCode]
	  ,[DSS].[SchoolType]
	  ,[DSS].[AuthorityGovtCode]
	  ,[DSS].[AuthorityGovt]
	  ,[DSS].[AuthorityGroupCode]
	  ,[DSS].[AuthorityGroup]
	  ,[DSS].[AuthorityTypeCode]
	  ,[DSS].[AuthorityType]
	  ,[DSS].[Island Code]
	  ,[DSS].[Island]
	  ,[DSS].[Region Code]
	  ,[DSS].[Region]
GO

