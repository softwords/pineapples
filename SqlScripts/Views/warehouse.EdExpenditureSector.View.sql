SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 14 10 2019
-- Description:	Nation level financial indicators
-- ==================================================
-- Reports on indicators available at the State level, but not further split by sector.
--
-- Built from warehouse.Expenditure, which consolidates the data available in GovtExpenditureState and
-- EdExpenditureState
-- This includes spending on education, as a percent of govt spending
-- spending per head ( across all sectors)
-- NATIONAL govt spending is stored using the 'expenditure point' NATIONAL.

-- The field EnrolmentApplies is used for calculating the spending per head, this is district enrolment
-- for expenditure points that rae real districts, and national enrolment for NATIONAL. This means you cannot meaningully add EnrolmentApplies
-- but you can total Enrolment and match the national enrolment returned elsewhere from the warehouse

-- ==================================================
CREATE VIEW [warehouse].[EdExpenditureSector]
WITH VIEW_METADATA
AS
Select SUB.*
, EdExpA / EnrolmentApplies EdExpAPerHead
, EdExpB / EnrolmentApplies EdExpBPerHead
FROM
(
Select E.SurveyYear
, DistrictCode
, SectorCode
, sum(EdExpA) EdExpA
, sum(EdExpB) EdExpB

, sum(Enrolment) Enrolment
, sum(case when DistrictCode = 'NATIONAL' then EnrolmentNation else Enrolment end) EnrolmentApplies


from warehouse.Expenditure E
WHERE E.DistrictCode is not null
GROUP BY E.SurveyYear
, DistrictCode
, SectorCode
) SUB
WHERE coalesce(EdExpA, EdExpB, EnrolmentApplies) is not null
GO

