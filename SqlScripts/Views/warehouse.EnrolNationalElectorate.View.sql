SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2019
-- Description:	Warehouse - Enrolment data by local electorate
--
-- This is a simple consoilidation of warehouse.enrol to group by local electorate
-- which is an attribute of schools.
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation
--		warehouse.EnrolIsland
--		warehouse.EnrolLocalElectorate
--		warehouse.EnrolNationalElectorate
--		warehouse.EnrolRegion
--		warehouse.EnrolSchoolType

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolIslandR
--		warehouse.EnrolRegionR
--		warehouse.EnrolLocalElectorateR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW [warehouse].[EnrolNationalElectorate]
AS
Select SurveyYear
, DSS.[National Electorate No] NationalElectorateNo
, [National Electorate]

, ClassLevel
, Age
, GenderCode

, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA
, sum(Completed) Completed

from warehouse.Enrol E
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON E.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, DSS.[National Electorate No]
, [National Electorate]
, ClassLevel
, Age
, GenderCode
GO

