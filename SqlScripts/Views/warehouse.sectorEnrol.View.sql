SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Sector Enrolment
--
-- Top level aggregate of enrolments by education Sector.
-- Pineapples data model allows enrolments to be classified chronologically
-- (by education level / year of education) and sectorially. This corresponds to the distrinctions made
-- in ISCED and elsewhere between General and Vocational education.
-- Each class level is assigned a sector code, as well as a year of education.
-- this view aggregates by these sector codes.
-- Note that Estimate are separated from Actuals.
-- =============================================
CREATE VIEW [warehouse].[sectorEnrol]
AS
Select SurveyYear
, Estimate
, GenderCode
, L.secCode SectorCode
, sum(Enrol) Enrol
, sum(Rep) Rep
From warehouse.tableEnrol E
INNER JOIN lkpLevels L
ON E.ClassLevel = L.codeCode
GROUP BY SurveyYear, Estimate, L.secCode, GenderCode
GO

