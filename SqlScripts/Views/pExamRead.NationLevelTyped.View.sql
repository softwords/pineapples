SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 9 2021
-- Description:	'Typed' ( ie exam, standard,  benchmark,  indicator) results
--				for exams aggregated to exam
-- ExamData is the Xml representation of the List object on the first sheet of the workbook
-- =============================================
CREATE VIEW [pExamRead].[NationLevelTyped]
AS
select examID, examCode, examYear, examName
, Gender

, RecordType
, ID
, [Key]
, Description

, achievementLevel
, achievementDesc

, sum(indicatorCount) indicatorCount
, sum(weight) weight
, sum(CandidateCount) candidateCount
from pExamRead.CandidateLevelTyped CL

GROUP BY examID, examCode, examYear, examName
, Gender
, RecordType
, ID
, [Key]
, Description

, achievementLevel
, achievementDesc
GO

