SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionTeacherAge]
AS
SELECT
metaNumbers.num,
Partitions.ptName
FROM metaNumbers, Partitions
WHERE
(((metaNumbers.num) Between [ptMin] And [ptMax])
AND ((Partitions.ptSet)='TeacherAge'))
GO
GRANT DELETE ON [dbo].[DimensionTeacherAge] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[DimensionTeacherAge] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[DimensionTeacherAge] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[DimensionTeacherAge] TO [public] AS [dbo]
GO

