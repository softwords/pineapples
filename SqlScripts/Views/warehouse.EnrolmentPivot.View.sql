SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2021
-- Query to generate pivot tables for enrolment on every conceivable disaggregation.
---------------------------------------------------------------------------------------
CREATE VIEW [warehouse].[EnrolmentPivot]
AS
SELECT E.surveyYear [Survey Year]
, E.schNo
, E.Estimate
, E.Enrol
, E.Age
, DL.*
, DSS.*
,SYRANK.rankDistrictSchoolType [District Rank]
,SYRANK.rankDistrictSchoolTypeDec [District Decile]
,SYRANK.rankDistrictSchoolTypeQtl [District Quartile]
,SYRANK.rankSchoolType [Rank]
,SYRANK.rankSchoolTypeDec Decile
,SYRANK.rankSchoolTypeQtl Quartile
, isnull(DAge.AtLevelAge,1) [At Level Age]
, isnull(DAge.AtedLevelAge,1) [At EdLevel Age]
, isnull(DAge.AtedLevelAltAge,1) [At EdLevelAlt Age]
, isnull(DAge.AtedLevelAlt2Age,1) [At EdLevelAlt2 Age]
, isnull(Dage.[Age Offset], 0) [Age Offset]
from warehouse.Enrol E
LEFT JOIN DimensionGender G
	ON E.GenderCode = G.GenderCode
LEFT JOIN warehouse.dimensionSchoolSurvey DSS
ON E.SurveyDimensionID = DSS.[Survey ID]
LEFT JOIN DimensionLevel DL
ON E.ClassLevel = DL.LevelCode
LEFT JOIN warehouse.SurveyYearRank SYRANK
ON E.surveyYear = SYRANK.SurveyYear
AND E.schNo = SYRANK.schNo
LEFT JOIN DimensionAge DAge
ON E.surveyYear = DAge.svyYear
AND E.Age = DAge.Age
AND DL.[Year of Education] = DAge.yearofEd

WHERE E.Enrol is not null
GO

