SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: ?
-- Description:	Warehouse - Enrolment and population data
--
-- This is a consolidation of warehouse.tableEnrol
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
--
-- History Log:
--   * 30 July 2021, Ghislain Hachey, Added Completed column since it is potentially available from end of year census
-- =============================================
CREATE VIEW [warehouse].[EnrolAndPop]
WITH VIEW_METADATA
AS
select SurveyYear
      ,Estimate
      ,ClassLevel
      ,Age
      ,GenderCode
      ,DistrictCode
      ,AuthorityCode
      ,SchoolTypeCode
      ,Enrol
      ,Rep
      ,TrIn
      ,TrOut
      ,Boarders
      ,Disab
      ,Dropout
      ,Expelled
      ,PSA
	  ,Completed
, null Pop
from warehouse.tableEnrol

UNION ALL

Select
popYear SurveyYear
, 0 Estimate
, null ClassLevel
, popAge Age
, genderCode
, dID DistrictCode
, null AuthorityCode
, null schoolTypeCode
, null Enrol
, null Rep
, null Trin
, null Trout
, null Boarders
, null Disab
, null Dropout
, null Expelled
, null PSA
, null Completed
, pop Pop
 from warehouse.measurePopG
GO

