SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/04/2022
-- Description:	Warehouse - Exam Results (Standards) (normalized version)
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsStandardsSchool]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, [Key] [standardCode]
, Description [standardDesc]
, achievementLevel
, achievementDesc
, sum(candidateCount) Candidates
FROM warehouse.examSchoolResultsTyped
WHERE RecordType = 'Standard'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, [Key]
, Description
, achievementLevel
, achievementDesc
GO

