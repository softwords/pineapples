SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Accreditations Table - 'report' format
-- The warehouse.AccreditionXXX views disaggregate by inspection result
-- The 'R' (=report) formats denormalise on InspectionResult,
-- creating columns for [Level 1] [Level 2], [Level 3], [Level 4]
-- This is consistent with naming and denormalising of Gender in
-- enrolment views in the warehouse.
-- Note that denormalising on InspectionResult in this way
-- makes hardcoded assumptions aboout the possible values for InspectinResult
-- and the 'business rule' that Level 2-4 are accredited, Level 1 is not
-- =============================================
CREATE VIEW [warehouse].[AccreditationTableR]
WITH VIEW_METADATA
AS
Select SurveyYear
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
, sum(NumSchools) NumSchools

-- cumulative
, sum(case when InspectionResult = 'Level 1' then Num end) [Level1]
, sum(case when InspectionResult = 'Level 2' then Num end) [Level2]
, sum(case when InspectionResult = 'Level 3' then Num end) [Level3]
, sum(case when InspectionResult = 'Level 4' then Num end) [Level4]
, sum(Num) Evaluated
, sum(case when InspectionResult in('Level 2', 'Level 3', 'Level 4') then Num end) Accredited

-- in year
, sum(case when InspectionResult = 'Level 1' then NumThisYear end) [Level1ThisYear]
, sum(case when InspectionResult = 'Level 2' then NumThisYear end) [Level2ThisYear]
, sum(case when InspectionResult = 'Level 3' then Num end) [Level3ThisYear]
, sum(case when InspectionResult = 'Level 4' then Num end) [Level4ThisYear]
, sum(NumThisYear) EvaluatedThisYear
, sum(case when InspectionResult in('Level 2', 'Level 3', 'Level 4') then NumThisYear end) AccreditedThisYear


FROM
(
SELECT
SurveyYear
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
, InspectionResult
, Num
, NumThisYear
, null NumSchools
FROM warehouse.AccreditationTable
UNION ALL
SELECT
SurveyYear
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
, null InspectionResult
, null Num
, null NumThisYear
, NumSchools
FROM warehouse.SchoolCountTable
) SUB
group by
SurveyYear
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
GO

