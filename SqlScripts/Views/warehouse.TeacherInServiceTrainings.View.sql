SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 14 09 2020
-- Description:	This view assumes there are no teachers that have passed more than
-- one FSM certification in a single year. If those occured they will
-- currently increase the percentage of teachers having received
-- in-service training in the year (last year for the SDG 4.c.7 indicator)
-- These are rare and don't matter.
--
-- This should serve as temporary view providing reasonable data for SDG 4.c.7
-- Going forward we'll need to decide whether we fine tune this query
-- or adapt warehouse.TeacherLocation to have CertInYear and QualInYear columns
--
-- Any qualification that is in the group 'FSM Certification' is currently
-- what we define as SDG' in-service training
-- =============================================
CREATE VIEW [warehouse].[TeacherInServiceTrainings]
AS
SELECT TL.[tID]
	  ,[SurveyYear]
      ,[GenderCode]
      ,[yr] AS QualificationYear
	  ,[ISCEDSubClass]
      ,[Role]
      ,[Sector]
      ,[Certified]
      ,[Qualified]
      ,[tchQual] AS QualificationCode
	  ,[codeDescription] AS Qualification
	  ,[InYear] AS CertifiedInYear
	  ,[Num] = 1
FROM (SELECT tID, yr, tchQual, InYear = 1 FROM [warehouse].[TeacherQual]) TQ
INNER JOIN (SELECT * FROM lkpTeacherQual WHERE codeGroup = 'FSM Certification') LTQ ON TQ.tchQual = LTQ.codeCode
RIGHT OUTER JOIN [warehouse].[TeacherLocation] TL ON TQ.tID = TL.TID AND TQ.yr = TL.SurveyYear
GO

