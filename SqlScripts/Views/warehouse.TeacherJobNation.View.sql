SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs nation totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobNation]
WITH VIEW_METADATA
AS
Select SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType

, sum(NumStaff) NumStaff

FROM warehouse.TeacherJobTable
GROUP BY
SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType
GO

