SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs by region
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobIsland]
WITH VIEW_METADATA
AS
Select SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType
, [Island Code] IslandCode
, Island
, [Region Code] RegionCode
, Region
, [District Code] DistrictCode
, District

, sum(NumStaff) NumStaff

FROM warehouse.TeacherJobSchool S
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON S.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType
, [Island Code]
, Island
, [Region Code]
, Region
, [District Code]
, District
GO

