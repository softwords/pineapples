SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: November 2019
-- Description:	Aggregate view of special ed data - denormalised on Gender
-- =============================================
CREATE VIEW [warehouse].[SpecialEdR]
WITH VIEW_METADATA AS

SELECT SurveyYear
      ,EdLevelCode
      ,EdLevel
      ,EnvironmentCode
      ,Environment
      ,DisabilityCode
      ,Disability
      ,EnglishLearnerCode
      ,EnglishLearner
      ,EthnicityCode
      ,Ethnicity
      ,Age
      ,AuthorityCode
      ,Authority
      ,DistrictCode
      ,District
      ,AuthorityGovtCode
      ,AuthorityGovt
      ,SchoolTypeCode
      ,SchoolType
      ,RegionCode
      ,Region
	  , sum(Num) Num
      ,sum(case when GenderCode = 'M' then Num end) M
	  ,sum(case when GenderCode = 'F' then Num end) F
  FROM warehouse.enrolSpEd
GROUP BY SurveyYear
      ,EdLevelCode
      ,EdLevel
      ,EnvironmentCode
      ,Environment
      ,DisabilityCode
      ,Disability
      ,EnglishLearnerCode
      ,EnglishLearner
      ,EthnicityCode
      ,Ethnicity
      ,Age
      ,AuthorityCode
      ,Authority
      ,DistrictCode
      ,District
      ,AuthorityGovtCode
      ,AuthorityGovt
      ,SchoolTypeCode
      ,SchoolType
      ,RegionCode
      ,Region
GO

