SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSchoolRead].[SchoolSurveyBuildingReviews]
AS
Select ssID, BuildingReview.*
from BuildingReview
INNER JOIN SchoolInspection
	ON BuildingReview.inspID =SchoolInspection.inspID
INNER JOIN SurveyInspectionSet SIS
	ON SchoolInspection.inspsetID = SIS.inspsetID
	AND SIS.intyCode = 'BLDG'
INNER JOIN SchoolSurvey SS
	ON SS.svyYear = SIS.svyYEar
	AND SchoolInspection.schNo = SS.schno
GO

