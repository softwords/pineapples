SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 5 Dec 2020
-- Description:	audit of imported teacher data
-- ==================================================
--Teacher data from the census workbook is stored 'verbatim' as xml in
--tcheData field. From there, the census load process writes into specific fields in TacherSurvey and TeacherIdentity.
--warehouse.buildTeacherLocation then uses these tables to create warehouse.TeacherLocation, which is the foundation  of
--all warehouse teacher number information.
--To audit the transformation of data from census workbook to warehouse, it is important to verify that the raw information
--from the census is accruately captured in tcheData. This query presents the xml as a recordset.
--see also common.buildTeacherSurveyAudit, whcih can construct this sql from the attribute names that are found in tcheData
-- allowing a simple way for this to be maintained in the future.
--
-- ==================================================
CREATE VIEW [pSurveyOps].[TeacherSurveyAudit]
AS

Select tchsID, tId, ssID, schNo, svyYear , tcheSource fileID
, tcheData.value('(//row/@Index)[1]', 'nvarchar(100)') [Index]
, tcheData.value('(//row/@SchoolYear)[1]', 'nvarchar(100)') [SchoolYear]
, tcheData.value('(//row/@State)[1]', 'nvarchar(100)') [State]
, tcheData.value('(//row/@District)[1]', 'nvarchar(100)') [District]
, tcheData.value('(//row/@School_Name)[1]', 'nvarchar(100)') [School_Name]
, tcheData.value('(//row/@School_No)[1]', 'nvarchar(100)') [School_No]
, tcheData.value('(//row/@Authority_Govt)[1]', 'nvarchar(100)') [Authority_Govt]
, tcheData.value('(//row/@Govt_or_Non-govt)[1]', 'nvarchar(100)') [Govt_or_Non-govt]
, tcheData.value('(//row/@School_Type)[1]', 'nvarchar(100)') [School_Type]
, tcheData.value('(//row/@First_Name)[1]', 'nvarchar(100)') [First_Name]
, tcheData.value('(//row/@Middle_Name)[1]', 'nvarchar(100)') [Middle_Name]
, tcheData.value('(//row/@Last_Name)[1]', 'nvarchar(100)') [Last_Name]
, tcheData.value('(//row/@Full_Name)[1]', 'nvarchar(100)') [Full_Name]
, tcheData.value('(//row/@Gender)[1]', 'nvarchar(100)') [Gender]
, tcheData.value('(//row/@Date_of_Birth)[1]', 'nvarchar(100)') [Date_of_Birth]
, tcheData.value('(//row/@Age)[1]', 'nvarchar(100)') [Age]
, tcheData.value('(//row/@Citizenship)[1]', 'nvarchar(100)') [Citizenship]
, tcheData.value('(//row/@Ethnicity)[1]', 'nvarchar(100)') [Ethnicity]
, tcheData.value('(//row/@OTHER_SSN)[1]', 'nvarchar(100)') [OTHER_SSN]
, tcheData.value('(//row/@FSM_SSN)[1]', 'nvarchar(100)') [FSM_SSN]
, tcheData.value('(//row/@Highest_Ed_Certification)[1]', 'nvarchar(100)') [Highest_Ed_Certification]
, tcheData.value('(//row/@Highest_Qualification)[1]', 'nvarchar(100)') [Highest_Qualification]
, tcheData.value('(//row/@Field_of_STudy)[1]', 'nvarchar(100)') [Field_of_STudy]
, tcheData.value('(//row/@Year_of_Completion)[1]', 'nvarchar(100)') [Year_of_Completion]
, tcheData.value('(//row/@Reason)[1]', 'nvarchar(100)') [Reason]
, tcheData.value('(//row/@Highest_Ed_Qualification)[1]', 'nvarchar(100)') [Highest_Ed_Qualification]
, tcheData.value('(//row/@Year_Of_Completion2)[1]', 'nvarchar(100)') [Year_Of_Completion2]
, tcheData.value('(//row/@Employment_Status)[1]', 'nvarchar(100)') [Employment_Status]
, tcheData.value('(//row/@Job_Title)[1]', 'nvarchar(100)') [Job_Title]
, tcheData.value('(//row/@Date_Of_Exit)[1]', 'nvarchar(100)') [Date_Of_Exit]
, tcheData.value('(//row/@Organization)[1]', 'nvarchar(100)') [Organization]
, tcheData.value('(//row/@Teacher_Type)[1]', 'nvarchar(100)') [Teacher_Type]
, tcheData.value('(//row/@Staff_Type)[1]', 'nvarchar(100)') [Staff_Type]
, tcheData.value('(//row/@Teacher-Type)[1]', 'nvarchar(100)') [Teacher-Type]
, tcheData.value('(//row/@Date_of_Hire_)[1]', 'nvarchar(100)') [Date_of_Hire_]
, tcheData.value('(//row/@Date_of_Hire)[1]', 'nvarchar(100)') [Date_of_Hire]
, tcheData.value('(//row/@Annual_Salary)[1]', 'nvarchar(100)') [Annual_Salary]
, tcheData.value('(//row/@Funding_Source)[1]', 'nvarchar(100)') [Funding_Source]
, tcheData.value('(//row/@ECE)[1]', 'nvarchar(100)') [ECE]
, tcheData.value('(//row/@Grade_1)[1]', 'nvarchar(100)') [Grade_1]
, tcheData.value('(//row/@Grade_2)[1]', 'nvarchar(100)') [Grade_2]
, tcheData.value('(//row/@Grade_3)[1]', 'nvarchar(100)') [Grade_3]
, tcheData.value('(//row/@Grade_4)[1]', 'nvarchar(100)') [Grade_4]
, tcheData.value('(//row/@Grade_5)[1]', 'nvarchar(100)') [Grade_5]
, tcheData.value('(//row/@Grade_6)[1]', 'nvarchar(100)') [Grade_6]
, tcheData.value('(//row/@Grade_7)[1]', 'nvarchar(100)') [Grade_7]
, tcheData.value('(//row/@Grade_8)[1]', 'nvarchar(100)') [Grade_8]
, tcheData.value('(//row/@Grade_9)[1]', 'nvarchar(100)') [Grade_9]
, tcheData.value('(//row/@Grade_10)[1]', 'nvarchar(100)') [Grade_10]
, tcheData.value('(//row/@Grade_11)[1]', 'nvarchar(100)') [Grade_11]
, tcheData.value('(//row/@Grade_12)[1]', 'nvarchar(100)') [Grade_12]
, tcheData.value('(//row/@Admin)[1]', 'nvarchar(100)') [Admin]
, tcheData.value('(//row/@Other)[1]', 'nvarchar(100)') [Other]
, tcheData.value('(//row/@Total_Days_Absence)[1]', 'nvarchar(100)') [Total_Days_Absence]
, tcheData.value('(//row/@Science)[1]', 'nvarchar(100)') [Science]
, tcheData.value('(//row/@Maths)[1]', 'nvarchar(100)') [Maths]
, tcheData.value('(//row/@Language)[1]', 'nvarchar(100)') [Language]
, tcheData.value('(//row/@Competency)[1]', 'nvarchar(100)') [Competency]
 FROM pTeacherRead.TeacherSurveyV
GO

