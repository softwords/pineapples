SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Exam Results
--
-- Exam results are based on the "standard-benchmark-achievement level" hierarchy.
-- Variants of presentation are
-- consolidation: national district (state) or school levelreporting by benchmark or consoilidated to standard
-- normalised or "crosstabbed"
-- In the cross tabbed versions up to 10 achievement levels are presented on a single record.
-- This is more amenable to some chart presentations.
--
-- The datasets are:
-- ExamNationResults - national consolidation
-- ExamNationResultsX - national consolidation crosstabbed
-- ExamNationStandards - national consolidation by standard
-- ExamNationStandardsX - national consolidation by standard - crosstabbed
-- ExamStateResults - district consolidation (table)
-- ExamStateResultsX - district consolidation crosstabbed
-- ExamStateStandards - district consolidation by standard
-- ExamStateStandardsX - district consolidation by standard - crosstabbed
-- ExamSchoolResults - school level reporting (table)
-- ExamSchoolResultsX - school level reporting crosstabbed
-- ExamSchooltandards - school level reporting by standard
-- ExamSchoolStandardsX - school level reporting by standard - crosstabbed
--
-- As of Novemeber 2021 these are bult from the new 'Typed' versions exam data, supporting the 'Soe'
-- model: ExamSchoolResultsTyped and ExamTableResultsTyped
-- These are created by the stored proc warehouse.buildExamResults
-- from Benchmark -> Standard = > Exam
-- Note that this SP is NOT called from the warehouse.buildWarehouse function,
-- but instead is involed as part of the upload of each exam file.
-- We no longer show standard and benchmark values, since in the new model, these cannot be correctly aggregated
-- =============================================
CREATE VIEW [warehouse].[ExamSchoolResultsX]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, sum(candidateCount) Candidates
, sum(case when achievementLevel = 0 then candidateCount end) [0]
, sum(case when achievementLevel = 1 then candidateCount end) [1]
, sum(case when achievementLevel = 2 then candidateCount end) [2]
, sum(case when achievementLevel = 3 then candidateCount end) [3]
, sum(case when achievementLevel = 4 then candidateCount end) [4]
, sum(case when achievementLevel = 5 then candidateCount end) [5]
, sum(case when achievementLevel = 6 then candidateCount end) [6]
, sum(case when achievementLevel = 7 then candidateCount end) [7]
, sum(case when achievementLevel = 8 then candidateCount end) [8]
, sum(case when achievementLevel = 9 then candidateCount end) [9]

From warehouse.examSchoolResultsTyped
WHERE RecordType = 'EXAM'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
GO

