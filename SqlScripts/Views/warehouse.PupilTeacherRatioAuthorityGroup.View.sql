SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 18/08/2021
-- Description:	Warehouse - Teacher Pupil Ratio by AuthorityGroup
--
-- Calculating Pupil Teacher Ratios requires having the Enrolment values on the same row as the
-- teacher numbers.
-- this view splits by sector and aggregates up to AuthorityGroup

-- =============================================
CREATE VIEW [warehouse].[PupilTeacherRatioAuthorityGroup]
WITH VIEW_METADATA
AS

SELECT [SurveyYear]
, [AuthorityGroupCode]
, AuthorityGroup
, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(certQual) CertQual
, sum(Enrol) Enrol
FROM [warehouse].[PupilTeacherRatioSchool] PTRS
  LEFT JOIN DimensionSchoolSurvey DSS ON PTRS.schNo = DSS.[School No] AND PTRS.SurveyYear = DSS.[Survey Data Year]
GROUP BY [SurveyYear]
, [AuthorityGroupCode]
, AuthorityGroup
, Sector
GO

