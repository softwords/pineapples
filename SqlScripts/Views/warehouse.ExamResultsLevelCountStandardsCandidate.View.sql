SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 11/04/2022
-- Description:	Warehouse - Exam Results Level Count (standards) at the candidate level (normalized version)
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- A convenience view for Candidate reports that is similar to [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsLevelCountStandardsCandidate]
AS
Select CandidateID
, stuID
, Gender
, GivenName
, FamilyName
, examID
, examCode
, examYear
, examName
, schNo
, [Key] [standardCode]
, Description [standardDesc]
, achievementLevel
, achievementDesc
, sum(IndicatorCount) IndicatorCount
FROM [pExamRead].[CandidateLevelTyped]
WHERE RecordType = 'Standard'
GROUP BY CandidateID
, stuID
, Gender
, GivenName
, FamilyName
, examID
, examCode
, examYear
, examName
, schNo
, [Key]
, Description
, achievementLevel
, achievementDesc
GO

