SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 24/11/2021
-- Description:	Warehouse - Extension version of TeacherFtptSchool containing all dimensions (i.e. possible disaggregations)
--
-- History log:
--   * 24/11/2021, Ghislain Hachey, Created view
-- =============================================
CREATE VIEW
[warehouse].[TeacherFtptSchoolDimension]
AS
SELECT
	TFS.schNo
	, SurveyYear
	, GenderCode
	, ISCEDSubClass
	, ISCEDClass
	, AgeGroup
	, surveyDimensionID
	, FTE
	, FTEQ
	, FTEC
	, FTEQC
	, FTPT
	, FTPTQ
	, FTPTC
	, FTPTQC
	, A
	, X
	, [Survey ID]
	, [Survey Data Year]
	, [School No]
	, [School Name]
	, [District Code]
	, [District Code] DistrictCode
	, District
	, [District Short]
	, [Island Code]
	, Island
	, [Region Code]
	, Region
	, Size
	, [Local Electorate No]
	, [Local Electorate]
	, [National Electorate No]
	, [National Electorate]
	, DSS.AuthorityCode
	, Authority
	, AuthorityTypeCode
	, AuthorityType
	, AuthorityGroupCode
	, AuthorityGroup
	, LanguageCode
	, Language
	, LanguageGroupCode
	, LanguageGroup
	, CurrentSchoolTypeCode
	, [Current School Type]
	, DSS.SchoolTypeCode
	, SchoolType
	, [School Class]
	, [Year Closed]
FROM warehouse.TeacherFtptSchool TFS
JOIN DimensionSchoolSurvey DSS ON TFS.schNo = DSS.[School No] AND TFS.SurveyYear = DSS.[Survey Data Year]
GO

