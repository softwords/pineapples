SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse -  Offical Age
--
-- This view shows the enrolments by school at each class level and age, and identifies
-- the percentage at, over , or under the EdcationLevel Official Age Range, as well as Class Level official age
-- Note the views
-- EdLevelAge, EdLevelAgeNation, EdLevelAgeDistrict are related, bu these can go further by incorporating population,
-- and therefore calculating Gross and Net Enrolment Ratios
-- =============================================
CREATE VIEW [warehouse].[EdLevelAgeSchool]
AS
 Select E.SurveyYear
 , E.GenderCode

 , E.schNo

 , sum(E.Enrol) Enrol
 , sum(case when Estimate = 1 then Enrol end) EstimatedEnrol
 , ELA.edLevelCode
 , ELA.edLevel
 , sum(case when edLevelOfficialAge = '=' then Enrol end) OfficialAge
 , sum(case when edLevelOfficialAge = 'UNDER' then Enrol end) UnderAge
 , sum(case when edLevelOfficialAge = 'OVER' then Enrol end) OverAge

 , E.ClassLevel
 , ELA.yearOfEd
 , sum(case when ClassLevelOfficialAge = '=' then Enrol end) OfficialClassAge
 , sum(case when ClassLevelOfficialAge = 'UNDER' then Enrol end) UnderClassAge
 , sum(case when ClassLevelOfficialAge = 'OVER' then Enrol end) OverClassAge


 from warehouse.Enrol E
	LEFT JOIN lkpLevels L
		ON E.ClassLevel = L.codeCode
	LEFT JOIN  dimensionEdLevelAge ELA
		ON E.SurveyYear = ELA.svyYear
		AND L.lvlYear = ELA.yearOfEd
		AND (
		-- how to account for missing age here?
		-- if age is unknown, assume at official age of class - note this row is unique for the class level
				E.Age = ELA.Age
				OR (E.Age is null and ClassLevelOfficialAge = '=' and edLevelClassification is null)
		)
		WHERE (Enrol is not null or Rep is not null or PSA is not null)
		 AND edLevelClassification is null
	GROUP BY
	E.SurveyYear
	, E.GenderCode
	, SchNo
	, ELA.edLevelCode
	, ELA.edLevel
	, E.ClassLevel
	, ELA.yearOfEd
GO

