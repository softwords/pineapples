SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs district totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobDistrictR]
WITH VIEW_METADATA
AS
Select SurveyYear
, JobTitle
, StaffType
, TeacherType

, DistrictCode
, dName District

, sum(NumStaff) NumStaff
, sum(case when GenderCode = 'M' then NumStaff else null end) NumStaffM
, sum(case when GenderCode = 'F' then NumStaff else null end) NumStaffF

FROM warehouse.TeacherJobTable T
	INNER JOIN Districts D
		ON T.DistrictCode = D.dID
GROUP BY
SurveyYear
, JobTitle
, StaffType
, TeacherType
, DistrictCode
, dName
GO

