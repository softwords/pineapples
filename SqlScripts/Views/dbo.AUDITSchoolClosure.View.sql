SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AUDITSchoolClosure]
AS
SELECT Schools.schNo, Schools.schName, SchoolSurvey.svyYear,
Schools.schClosed, Schools.schEst
FROM
dbo.Schools INNER JOIN dbo.SchoolSurvey
ON Schools.schNo = SchoolSurvey.schNo
WHERE (((Schools.schClosed)<>0 And (Schools.schClosed)<=[svyYear]))
OR (((Schools.schEst)>[svyYear] Or (Schools.schEst) Is Null));
GO
GRANT DELETE ON [dbo].[AUDITSchoolClosure] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[AUDITSchoolClosure] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[AUDITSchoolClosure] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[AUDITSchoolClosure] TO [public] AS [dbo]
GO

