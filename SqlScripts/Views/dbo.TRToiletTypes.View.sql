SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRToiletTypes]
AS
SELECT  TOP 100 PERCENT
 ttypName as ttypCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN ttypName
	WHEN 1 THEN ttypNameL1
	WHEN 2 THEN ttypNameL2

END, ttypName)  AS ttypName,
ttypSystem,ttypSort, ttypGroup

FROM         dbo.lkpToiletTypes
ORDER BY ttypSort ASC
GO
GRANT SELECT ON [dbo].[TRToiletTypes] TO [public] AS [dbo]
GO

