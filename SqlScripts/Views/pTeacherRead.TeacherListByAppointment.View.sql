SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * View to produce an annual list of teachers at a school, drawn from Appointments rather than
 * TeacherSurvey. An appointment extending over multiple years may appear multiple times in this list
 * So the list may be filtered by surveyYear.
*/
CREATE VIEW [pTeacherRead].[TeacherListByAppointment]
WITH VIEW_METADATA
AS
select TI.tID
, TI.tSex Gender
, TI.tDoB
, S.svyYear
, isnull(TA.schNo, EST.schNo) schNo
, SCH.schName
, TA.taDate
, Ta.taRole
, TA.taID
, TA.estpNo
, case when TS.tchsID is not null then 1 else null end SurveyConfirmed
-- dec 2017 should be max, not sum
, case max(convert(int,isnull(ytqQualified, codeQualified)))
	when 1 then 1
	else 0 end Qualified

, case max(convert(int,isnull(ytqCertified, codeCertified)))
	when 1 then 1
	else 0 end Certified

from TeacherIdentity TI
CROSS JOIN Survey S
INNER JOIN TeacherAppointment TA
	ON TI.tID = TA.tID
	AND TA.taDate <= S.svyCensusDate
	AND (TA.taEndDate > S.svyCensusDate or TA.taEndDate is null)


LEFT JOIN Establishment EST
	ON TA.estpNo = EST.estpNo
LEFT JOIN Schools SCH
-- join to Schools from either TeacherAppointment or Establishment, to correctly handle Supernumeraries
	ON isnull(TA.schNo, EST.schNo) = SCH.schNo
-- confirmation of appointment in school survey
LEFT JOIN SchoolSurvey SS
	ON SS.schNo = SCH.schNo
	AND SS.svyYEar = S.svyYear
-- add support for ISCED
LEFT JOIN SchoolTypeILSCode ILS
	ON SS.ssschType = ILS.stCode
LEFT JOIN TeacherSurvey TS
	ON TS.tID = TI.tID
	AND TS.ssID = SS.ssID
-- related to training
LEFT JOIN TeacherTraining TT
	ON TI.tID = TT.tID
	AND isnull(TT.trYear,0) <= S.svyYear
LEFT JOIN SurveyYearTeacherQual SQ
	ON TT.trQual = SQ.ytqQual
	AND SQ.svyYear = S.svyYEar
	AND isnull(ILS.UniquesecCode,TS.tchSector) = SQ.ytqSector
LEFT JOIN lkpTeacherQual TQ
	ON TT.trQual = TQ.codeCode

group by TI.tID,  S.svyYEar, ta.taDate, ta.taRole, TA.taID
, EST.schNo, Ta.schNo
, SCH.schName, TA.estpNo, TS.tchsID, tSex, tDoB
GO

