SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: Jan 2020
-- Description:	Warehouse - Total Enrolments by school
--
-- This is a simple consolidation of warehouse.enrol to group by school and Gender
-- This or its 'R' version can produce simple column charts of enrolment across all schools
--
-- History Log:
--   * 28 October 2021, Ghislain Hachey, Add missing Rep, Trin, Trout, Boarders, Disab, Dropout, PSA.
-- =============================================
CREATE VIEW
[warehouse].[EnrolSchoolR]
AS
Select SurveyYear
, schNo
, DistrictCode
, District
, [AuthorityCode]
, Authority
, AuthorityGovtCode
, AuthorityGovt
, [SchoolTypeCode]
, SchoolType
, SchoolName
, max(SurveyDimensionID) SurveyDimensionID
, sum(case when GenderCode = 'M' then Enrol end) EnrolM
, sum(case when GenderCode = 'F' then Enrol end) EnrolF
, sum(Enrol) Enrol

, sum(case when GenderCode = 'M' then Rep end) RepM
, sum(case when GenderCode = 'F' then Rep end) RepF
, sum(Rep) Rep

, sum(case when GenderCode = 'M' then Trin end) TrinM
, sum(case when GenderCode = 'F' then Trin end) TrinF
, sum(Trin) Trin

, sum(case when GenderCode = 'M' then Trout end) TroutM
, sum(case when GenderCode = 'F' then Trout end) TroutF
, sum(Trout) Trout

, sum(case when GenderCode = 'M' then Boarders end) BoardersM
, sum(case when GenderCode = 'F' then Boarders end) BoardersF
, sum(Boarders) Boarders

, sum(case when GenderCode = 'M' then Disab end) DisabM
, sum(case when GenderCode = 'F' then Disab end) DisabF
, sum(Disab) Disab

, sum(case when GenderCode = 'M' then Dropout end) DropoutM
, sum(case when GenderCode = 'F' then Dropout end) DropoutF
, sum(Dropout) Dropout

, sum(case when GenderCode = 'M' then PSA end) PSAM
, sum(case when GenderCode = 'F' then PSA end) PSAF
, sum(PSA) PSA

, sum(case when GenderCode = 'M' then Completed end) CompletedM
, sum(case when GenderCode = 'F' then Completed end) CompletedF
, sum(Completed) Completed
from warehouse.enrolSchool E
group by
schNo, SurveyYear		--, SurveyDimensionID may be multiples??
, DistrictCode
, District
, [AuthorityCode]
, Authority
, AuthorityGovtCode
, AuthorityGovt
, [SchoolTypeCode]
, SchoolType
, [SchoolName]
GO

