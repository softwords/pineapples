SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Authority Inspections with result
--
-- =============================================
CREATE VIEW [warehouse].[AccreditationAuthorityGovt]
WITH VIEW_METADATA
AS
Select SurveyYear
, AuthorityGovtCode
, AuthorityGovt
, InspectionResult
, sum(Num) Num
, sum(NumThisYear) NumThisYear

FROM warehouse.InspectionTable
WHERE InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GROUP BY SurveyYear
, AuthorityGovtCode
, AuthorityGovt
, InspectionResult
GO

