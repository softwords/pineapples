SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Enrolments
--
-- National enrolments divideded by class level, showing whether the pupil is at, over or under
-- the EdLevel OfficialAge
-- The year of ed of the class level determines the EdLevel here, and the official age range
-- is determined in the usual way (from Offical Start Age and Range of years of Ed for the Ed Level)
-- Suggested Use: High Level analysis of over / under age enrolments.
-- Note this view uses only the principal education levels defined in lkpEducationLevels
-- It does not use the Alt or Alt2 groupings (to do?)
-- =============================================
CREATE VIEW [warehouse].[EdLevelAgeTableR]
AS
Select SurveyYear
, ClassLevel
, yearOfEd
, EdLevel
, DistrictCode
, AuthorityCode
, AuthorityGovtCode
, SchoolTypeCode
, sum(Enrol) Enrol
, sum(case when GenderCode = 'M' then Enrol end) EnrolM
, sum(case when GenderCode = 'F' then Enrol end) EnrolF

, sum(UnderAge) UnderAge
, sum(case when GenderCode = 'M' then UnderAge end) UnderAgeM
, sum(case when GenderCode = 'F' then UnderAge end) UnderAgeF
, sum(OfficialAge) OfficialAge
, sum(case when GenderCode = 'M' then OfficialAge end) OfficialAgeM
, sum(case when GenderCode = 'F' then OfficialAge end) OfficialAgeF
, sum(OverAge) OverAge
, sum(case when GenderCode = 'M' then OverAge end) OverAgeM
, sum(case when GenderCode = 'F' then OverAge end) OverAgeF

, sum(UnderClassAge) UnderClassAge
, sum(case when GenderCode = 'M' then UnderClassAge end) UnderClassAgeM
, sum(case when GenderCode = 'F' then UnderClassAge end) UnderClassAgeF
, sum(OfficialClassAge) OfficialClassAge
, sum(case when GenderCode = 'M' then OfficialClassAge end) OfficialClassAgeM
, sum(case when GenderCode = 'F' then OfficialClassAge end) OfficialClassAgeF
, sum(OverClassAge) OverClassAge
, sum(case when GenderCode = 'M' then OverClassAge end) OverClassAgeM
, sum(case when GenderCode = 'F' then OverClassAge end) OverClassAgeF

FROM warehouse.EdLevelAgeTable
GROUP BY
surveyYear, ClassLevel, yearOfEd, EdLevel
, DistrictCode
, AuthorityCode
, AuthorityGovtCode
, SchoolTypeCode
GO

