SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	School Type Inspections with result
--
-- =============================================
CREATE VIEW [warehouse].[AccreditationNation]
WITH VIEW_METADATA
AS
Select SurveyYear
, InspectionResult
, sum(Num) Num
, sum(NumThisYear) NumThisYear

FROM warehouse.InspectionTable
WHERE InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GROUP BY SurveyYear
, InspectionResult
GO

