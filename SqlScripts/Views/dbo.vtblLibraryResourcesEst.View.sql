SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblLibraryResourcesEst]
AS
SELECT
vtblResourcesEst.*
FROM
vtblResourcesEst
WHERE (((vtblResourcesEst.resName)='Library Resources'))
GO
GRANT SELECT ON [dbo].[vtblLibraryResourcesEst] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[vtblLibraryResourcesEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[vtblLibraryResourcesEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[vtblLibraryResourcesEst] TO [pSchoolWrite] AS [dbo]
GO

