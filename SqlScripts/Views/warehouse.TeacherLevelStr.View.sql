SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 3 2021
-- Description:	A utility 'wrapper' around warehouse.TeacherLocation
-- represents activities as a single string of 0 and 1
-- This streamlines filter on Teacher activities
-- e.g., to find all teachers teaching in an ISCED Level or education level
-- we can use the level string like this:
-- substring(levelStr, minYr+2, maxYear - MinYear + 1) like '%1%'

-- TO DO?? eliminate this and place levelstr directly on warehouse.TeacherLocation?
-- =============================================
CREATE VIEW [warehouse].[TeacherLevelStr]
AS
Select
convert(nchar(1), tpk)
+ convert(nchar(1), t00)
+ convert(nchar(1), t01)
+ convert(nchar(1), t02)
+ convert(nchar(1), t03)
+ convert(nchar(1), t04)
+ convert(nchar(1), t05)
+ convert(nchar(1), t06)
+ convert(nchar(1), t07)
+ convert(nchar(1), t08)
+ convert(nchar(1), t09)
+ convert(nchar(1), t10)
+ convert(nchar(1), t11)
+ convert(nchar(1), t12)
+ convert(nchar(1), t13)
+ convert(nchar(1), t14)
+ convert(nchar(1), t15)
+ convert(nchar(1), T)
+ convert(nchar(1), A)
+ convert(nchar(1), X)
LevelStr
, TL.*
, BS.surveyDimensionID
, I.ilCode ISCEDLevel
from warehouse.TeacherLocation TL
	INNER JOIN warehouse.BestSurvey BS
		ON TL.SurveyYEar = BS.SurveyYEar
		AND TL.SchNo = BS.schNo
	LEFT JOIN ISCEDLevelSub I
		ON I.ilsCode = TL.ISCEDSubClass
GO

