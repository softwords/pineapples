SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 9 2021
-- Description:	The Level achieved for each Benchmark, by Candidate
-- This is determined by 'ItemCount' accumulating all items
-- in the benchmark, and apply the same bandings to that total as are
-- used to get the Indicator level. This is not used in the Soe system analysis.
-- =============================================
CREATE VIEW [pExamRead].[CandidateLevel]
AS

Select
EC.exID		examID
, EX.exCode examCode
, EX.exYear examYear
, LKPX.exName examName
, EC.excID candidateID
, EC.schNo
, EC.stuID
, EC.Gender gender
, EC.GivenName
, Ec.FamilyName

, ECR.exindID indicatorID
, XI.exindCode indicatorKey
, XI.exindDescription	indicatorDesc

, ECR.exbnchID benchmarkID
, XB.exbnchCode benchmarkKey
, XB.exbnchDescription	benchmarkDesc

, ECR.exstdID standardID
, XS.exstdCode standardKey
, XS.exstdDescription	standardDesc

, ECR.exCode		ExamResult
, ECR.excrLevel achievementLevel
, XAL.exalDescription achievementDesc
, ECR.excrLevelCount				indicatorCount
, ECR.excrWeight			weight
, ECR.excrCandidateCount	CandidateCount
from ExamCandidateResults ECR
INNER JOIN pExamRead.ExamCandidates EC
	ON ECR.excID = EC.excID
INNER JOIN Exams EX
	ON EC.exID = EX.exID
INNER JOIN lkpExamTypes LKPX
	ON EX.exCode = LKPX.exCode
LEFT JOIN ExamBenchmarks XB
	ON ECR.exbnchID = XB.exbnchID
	AND EC.exID = XB.exID
LEFT JOIN ExamStandards XS
	ON ECR.exstdID = XS.exstdID
	AND EC.exID = XS.exID
LEFT JOIN ExamIndicators XI
	ON ECR.exindID = XI.exindID
	AND EC.exID = XI.exID

INNER JOIN ExamAchievementLevels XAL
	ON ECR.excrLevel = XAL.exalVal
	AND EC.exID = XAL.exID
GO

