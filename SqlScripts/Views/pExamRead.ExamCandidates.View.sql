SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 9 2021
-- Detailed information about examination candidate.
-- If the candidate is linked to a student, then the deographic
-- information from the Student_ - name, gender, dob, ethnicity -
-- is taken from the student, overriding the ExamCandidate record
-- =============================================
CREATE VIEW [pExamRead].[ExamCandidates]
AS
Select excID
, exID
, schNo
, EC.stuID
, coalesce(stuCardID, studentID) StudentID
, coalesce(stuGiven, excGiven) GivenName
, coalesce(stuFamilyName, excFamilyName) FamilyName
, coalesce(stuGender, excGender) Gender
, stuDoB	DoB
, stuEthnicity Ethnicity
FROM dbo.ExamCandidates EC
LEFT JOIN STudent_ S
ON EC.stuID = S.stuID
GO

