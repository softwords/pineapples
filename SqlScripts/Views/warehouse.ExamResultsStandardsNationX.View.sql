SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/04/2022
-- Description:	Warehouse - Exam Results (Standards) aggregated at the National level
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsStandardsNationX]
AS
Select examID
, examCode
, examYear
, examName
, Gender
, [Key] [standardCode]
, Description [standardDesc]
, sum(candidateCount) Candidates
, sum(case when achievementLevel = 0 then candidateCount end) [0]
, sum(case when achievementLevel = 1 then candidateCount end) [1]
, sum(case when achievementLevel = 2 then candidateCount end) [2]
, sum(case when achievementLevel = 3 then candidateCount end) [3]
, sum(case when achievementLevel = 4 then candidateCount end) [4]
, sum(case when achievementLevel = 5 then candidateCount end) [5]
, sum(case when achievementLevel = 6 then candidateCount end) [6]
, sum(case when achievementLevel = 7 then candidateCount end) [7]
, sum(case when achievementLevel = 8 then candidateCount end) [8]
, sum(case when achievementLevel = 9 then candidateCount end) [9]

From warehouse.examSchoolResultsTyped
WHERE RecordType = 'Standard'
GROUP BY examID
, examCode
, examYear
, examName
, Gender
, [Key]
, Description
GO

