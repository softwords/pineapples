SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 07 2021
-- Description:	TeacherQualificationHighest is a view requested by Weison. TeacherQualification can
-- "over count" total teachers since teachers can have multiple qualifications which would have multiple records.
-- Weison wants the same table but with only the highest qualification. The easiest is to use the
-- annual surveys' highest qualification instead of building on warehouse.TeacherQual.

-- Caveat: It does come with the disadvantage that annual surveys might be missing data entered through the UI.

-- This is to produce statistics on Qualifications. It provides
-- the ability to do it over the years with many disaggregations. Bare in mind that the Certified/Qualified
-- boolean value takes into consideration the qualifications in TeacherQual (and not just what
-- is in the survey) so there could be mismatch if highest qualification is not reported in survey and
-- using the LEFT JOIN lkpTeacherQual...below.
--
-- History Log:
--   * 16 September 2021, Ghislain Hachey, Added support to provide subject of study and the ISCED-F equivalents.
--   * 18 April 2022, Ghislain Hachey, Added support to include institution/year when possible.
--   * 4 May 2022, Ghislain Hachey, Added salary field.
-- =============================================
CREATE VIEW [warehouse].[TeacherQualificationsHighest]
WITH VIEW_METADATA
AS
SELECT TL.[TID]
      ,[SurveyYear]
      ,[GenderCode]
      ,[BirthYear]
      ,[AgeGroup]
      ,[ISCEDSubClass]
      ,[Sector]
      ,[Certified]
      ,[Qualified]
      ,[tchQual] AS QualificationCode
	  ,LTQ.codeDescription AS Qualification
	  ,LTQ.codeGroup AS QualificationGroup
	  ,TT.trInstitution Institution
	  ,TT.trYear [Year]
	  ,TS.tchSubjectMajor AS SubjectMajorCode
	  ,S.subjName AS SubjectMajor
	  ,ISF.ifID AS ISCEDDetailedFieldCode
	  ,ISF.ifName AS ISCEDDetailedField
	  ,ISG.ifgID AS ISCEDNarrowFieldCode
	  ,ISG.ifgName AS ISCEDNarrowField
	  ,ISGB.ifgbID AS ISCEDBroadFieldCode
	  ,ISGB.ifgbName AS ISCEDBroadField
	  ,DSS.[School Name]
      ,TL.[SchNo]
	  ,TS.tchSalary [Salary]
	  ,DSS.AuthorityCode
	  ,DSS.Authority
	  ,DSS.AuthorityGroupCode
	  ,DSS.AuthorityGroup
	  ,DSS.AuthorityTypeCode
	  ,DSS.AuthorityType
	  ,DSS.[District Code]
	  ,DSS.District
	  ,DSS.[Region Code]
	  ,DSS.Region
	  ,DSS.SchoolTypeCode
	  ,DSS.SchoolType
	  ,DSS.[Island Code]
	  ,DSS.Island
  FROM [warehouse].[TeacherLocation] TL
  INNER JOIN [dbo].[DimensionSchoolSurvey] DSS ON TL.SchNo = DSS.[School No] AND TL.SurveyYear = DSS.[Survey Year]
  LEFT OUTER JOIN lkpTeacherRole LTR ON TL.Role = LTR.codeCode
  LEFT OUTER JOIN [dbo].[TeacherSurvey] TS ON TL.TID = TS.tID AND TS.ssID = DSS.[Survey ID] -- LEFT to include any estimate found on TL
  LEFT OUTER JOIN lkpTeacherQual LTQ ON TS.tchQual = LTQ.codeCode -- LEFT to include teachers without a highest qualification in the survey (for testing
  -- identifying list of teachers expected as qualified based on TeacherQual but without any HighestQual in the survey)
  LEFT OUTER JOIN lkpSubjects S ON TS.tchSubjectMajor = S.subjCode
  LEFT OUTER JOIN ISCEDField ISF ON S.ifID = ISF.ifID
  LEFT OUTER JOIN ISCEDFieldGroup ISG ON ISF.ifgID = ISG.ifgID
  LEFT OUTER JOIN ISCEDFieldGroupBroad ISGB ON ISG.ifgbID = ISGB.ifgbID
  LEFT OUTER JOIN -- add support to include institution/year when possible
	(
	-- Possible cases to handle
	-- 1) We can have multiple certificate trainings recorded (in table dbo.TeacherTraining)
	-- 2) There could be duplicate trainings recorded (in table dbo.TeacherTraining)
	SELECT tID
		, max(isnull(trYear,0)) trYear -- 1) we'll just take the most recent one
		, trQual
		, max(trInstitution) trInstitution -- 2) we'll just take the one with an explicitly defined institutions. Unfortunately "as-is"
		-- it does not work if at least one of the dupes does not specify an institution (there is no such case in the data and should
		-- not be any since it is required data on entry). The ones with missing institution is likely old data manually entered
	FROM [dbo].[TeacherTraining]
	GROUP BY tID, trQual) TT ON TL.TID = TT.tID AND TS.tchQual = TT.trQual
  WHERE (
  TAMX = 'T' OR
  TAMX = 'M'
  ) -- only teachers
GO

