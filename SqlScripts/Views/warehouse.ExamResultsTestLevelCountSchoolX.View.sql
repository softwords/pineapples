SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- View
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/06/2022
-- Description:	Warehouse - Exam Results (whole test) aggregated at School level
-- based on level count (aka. indicator count) (and not candidate count)
--
-- These views are similar to those documented in View [warehouse].[ExamResultsTestSchoolX]
-- but the analysis is based on the level count (aka. indicator count) and not the candidate counts
-- as in those views.
--
-- There should be less of those as they are only created ad-hoc depending on client's request for
-- some reports backwards compatible to the old SOE assessment system.
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- =============================================
CREATE VIEW [warehouse].[ExamResultsTestLevelCountSchoolX]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, sum(indicatorCount) LevelCount
, sum(case when achievementLevel = 0 then indicatorCount end) [0]
, sum(case when achievementLevel = 1 then indicatorCount end) [1]
, sum(case when achievementLevel = 2 then indicatorCount end) [2]
, sum(case when achievementLevel = 3 then indicatorCount end) [3]
, sum(case when achievementLevel = 4 then indicatorCount end) [4]
, sum(case when achievementLevel = 5 then indicatorCount end) [5]
, sum(case when achievementLevel = 6 then indicatorCount end) [6]
, sum(case when achievementLevel = 7 then indicatorCount end) [7]
, sum(case when achievementLevel = 8 then indicatorCount end) [8]
, sum(case when achievementLevel = 9 then indicatorCount end) [9]

From warehouse.examSchoolResultsTyped
WHERE RecordType = 'Exam'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
GO

