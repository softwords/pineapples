SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 29 July 2021
-- Description:	Cohort data disaggregated by region
--
-- =============================================
CREATE VIEW [warehouse].[CohortRegion]
AS
Select SurveyYear
, [Region Code] RegionCode
, Region
, YearOfEd
, GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.CohortSchool C
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON C.SchoolNo = DSS.[School No] AND C.SurveyYear = DSS.[Survey Data Year]

GROUP BY SurveyYear
, [Region Code]
, Region
, YearOfEd
, GenderCode
UNION ALL
Select SurveyYear
, [Region Code] RegionCode
, Region
, YearOfEd
, null GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.CohortSchool C
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON C.SchoolNo= DSS.[School No] AND C.SurveyYear = DSS.[Survey Data Year]
GROUP BY SurveyYear
, [Region Code]
, Region
, YearOfEd
GO

