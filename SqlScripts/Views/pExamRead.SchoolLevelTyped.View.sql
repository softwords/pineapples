SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pExamRead].[SchoolLevelTyped]
AS
select examID, examCode, examYear, examName
, CL.schNo
, S.schName  SchoolName
, Gender
, DSS.[District Code] DistrictCode
, DSS.District
, coalesce(DSS.SchoolTypeCode, S.schType) SchoolTypeCode
, DSS.SchoolType
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, RecordType
, ID
, [Key]
, Description

, achievementLevel
, achievementDesc

, indicatorCount
, weight
, candidateCount
from
(
	select examID, examCode, examYear, examName
	, CL.schNo
	, Gender
	, RecordType
	, ID
	, [Key]
	, Description
	, achievementLevel
	, achievementDesc
	, sum(indicatorCount) indicatorCount
	, sum(weight) weight
	, sum(CandidateCount) candidateCount
	from pExamRead.CandidateLevelTyped CL
	GROUP BY examID, examCode, examYear, examName
	, CL.schNo
	, Gender
	, RecordType
	, ID
	, [Key]
	, Description
	, achievementLevel
	, achievementDesc

) CL
	LEFT JOIN warehouse.BestSurvey BS
		ON CL.schNo = BS.schNo
		AND CL.ExamYear = BS.SurveyYear
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON BS.surveyDimensionID = DSS.[Survey ID]
	LEFT JOIN Schools S
		ON CL.schNo = S.schNo
GO

