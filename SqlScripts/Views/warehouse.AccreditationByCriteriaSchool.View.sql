SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 21 August 2019
-- Description:	tabulation of accreditation rnak by criteria and standard
-- ==================================================
-- Reports on the accedition level (according to SSA partition)
-- achieved by school for each School_Evaluation standard
-- Moved to specific views from construction of this SQL in the Web Portal data layer
-- to allow such views easily available to report writers to be available to report writers
--
--
-- ==================================================
CREATE VIEW [warehouse].[AccreditationByCriteriaSchool]
WITH VIEW_METADATA
AS
Select SurveyYear
, [School No] schNo
, [School Name] schName
, DistrictCode, District
, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
, SchoolTypeCode, SchoolType
, Standard
, Criteria
, Score
, MaxScore
, NumSchools
, P.ptName Result
, case P.ptName	when 'Level 1' then 1 end Level1
, case P.ptName when 'Level 2' then 1 end Level2
, case P.ptName when 'Level 3' then 1 end Level3
, case P.ptName when 'Level 4' then 1 end Level4
, ScoreInYear
, MaxScoreInYear
, NumSchoolsInYear
, PInYear.ptName ResultinYear
, case PInYear.ptName when 'Level 1' then 1 end Level1InYear
, case PInYear.ptName when 'Level 2' then 1 end Level2InYear
, case PInYear.ptName when 'Level 3' then 1 end Level3InYear
, case PInYear.ptName when 'Level 4' then 1 end Level4InYear


FROM
(
Select SurveyYear
, [School No], [School Name]
						, DistrictCode, District
						, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
						, SchoolTypeCode, SchoolType
, Standard
, Criteria
, isnull(sum(D.Score),0) Score				-- this isnull will turn a potential NULL result back to Level 1
											-- this is consistent with what happens elsewhere
, sum(D.MaxScore) MaxScore
, count(DISTINCT D.inspID) NumSchools

, sum(case when BI.InspectionYear = BI.SurveyYear then D.Score else null end) ScoreInYear
, sum(case when BI.InspectionYear = BI.SurveyYear then D.MaxScore else null end) MaxScoreInYear
, count(DISTINCT case when BI.InspectionYear = BI.SurveyYear then D.inspID else null end) NumSchoolsInYear

--, case when BI.InspectionYear = BI.SurveyYear then 1 else 0 end InYear

from warehouse.AccreditationDetail D
	INNER JOIN warehouse.BestInspection BI
		ON D.inspID = BI.inspID
	INNER JOIN warehouse.dimensionSchoolSurvey DSS
		ON Dss.[Survey ID] = BI.SurveyDimensionID

GROUP BY
SurveyYear
, [School No], [School Name]
						, DistrictCode, District
						, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
						, SchoolTypeCode, SchoolType
, Standard
, Criteria
) SUB
LEFT JOIN Partitions P
	ON P.PtSet = 'SSA'
		AND (convert(decimal(8,3), SUB.Score) * 100 )/  SUB.maxScore between P.ptMin and P.ptMax
LEFT JOIN Partitions PInYear
	ON PInYear.PtSet = 'SSA'
		AND (convert(decimal(8,3), SUB.ScoreInYear) * 100 )/  SUB.maxScoreInYear between PInYear.ptMin and PInYear.ptMax
GO

