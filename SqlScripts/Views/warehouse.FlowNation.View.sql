SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Nation Flow
--
-- Wrapper around warehouse.nationCohort to include flow calculations
-- ie PromoteRate, RepeatRate, SurvivalRate (single year survival aka "transition rate")
-- This version is intended to be used in reports, use nationCohort for "cubes"
-- (pivot tables, tableau) where ratios should be calculated based on current aggregations.
-- TRANSFER versions:
-- field named xxxxTR calculate the same ratios but attempt to take into account internal transfers
-- These should be used with caution as internal transfer data may not be very reliable
-- See related views:
-- warehouse.CohortDistrict
-- warehouse.FlowDistrict
-- warehouse.CohortNation
-- warehouse.cohort (table)
-- warehouse.FlowSchool
-- =============================================
CREATE VIEW [warehouse].[FlowNation]
WITH VIEW_METADATA
AS

Select S.*
, round(1 - RepeatRate - PromoteRate,3) DropoutRate
, round(1 - RepeatRateTR - PromoteRateTR,3) DropoutRateTR
, case when RepeatRate = 1 then null else round(PromoteRate / (1 - RepeatRate),3) end SurvivalRate
, case when RepeatRateTR = 1 then null else round(PromoteRateTR / (1 - RepeatRateTR),3) end SurvivalRateTR
FROM
(
Select C.*
	, case when Enrol is null then null
		when Enrol = 0 then null
		else
			round( convert(float,isnull(RepNY,0)) /Enrol , 3)
		end RepeatRate
	, case when Enrol is null then null
		when (Enrol - isnull(TroutNY,0)) = 0 then null
		else
			round(convert(float,isnull(RepNY,0)) / (Enrol - isnull(TroutNY,0)) , 3)
		end RepeatRateTR
	, case when Enrol is null then null
		when Enrol = 0 then null
		else
			round(( convert(float,EnrolNYNextLevel) - isnull(RepNYNextLevel,0))  / Enrol ,3)
		end PromoteRate
	, case when Enrol is null then null
		when ( Enrol - isnull(TroutNY,0)) = 0 then null
		else
			round(( convert(float,EnrolNYNextLevel) - isnull(RepNYNextLevel,0) - isnull(TrinNYNextLevel,0))  / ( Enrol - isnull(TroutNY,0)) ,3)
		end PromoteRateTR
	from warehouse.CohortNation C

) S
GO

