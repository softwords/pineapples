SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 10 2021
-- Description:	Assembles the data for the warehouse table
-- warehouse.TeacherActivityWeightsEdLevel
-- This is used in warehouse.BuildTeacherLocation to construct the warehouse table
-- this version works out TeacherActivityWeights by education level
-- this is a bit simpler than the ISCED case, becuase the ed level is determined only by the year of eduction
-- ================================================
CREATE VIEW [warehouse].[buildTeacherActivityWeightsISCED]
AS
Select
SUB.SchNo
, SUB.SurveyYear
, SUB.GenderCode
, DSS.AuthorityGovtCode
, II.[ISCED SubClass] IscedSubClass
, tID
, Qualified
, Certified
, Qualified * Certified CertQual
, Activities
, case when t=1 then 1			-- the 'weight' for an unActivityDetail teacher is 1
else
	len(replace( substring(LevelStr, II.MinYr + 2, II.NumYrs),'0','')) /  Activities
end W

-- WTeach - 'weight teaching' - is used in Uis reporting
-- it is the weight of this isced over teaching activities ie omitting Admin (A) and Other (X) activities
-- the sum for a single teacher across all ISCEDs is 1; therefore the sum across all teachers is equal to the total
-- number of teachers 'teaching'. These are also the same records in TeacherLocation where TAMX in ('T','M')
-- Note if TAMX = 'T' then WT = W

,  case when t=1 then 1			-- the 'weight' for an unActivityDetail teacher is 1
	when Activities - A - X = 0 then 0		-- the teacher has no activities other than A or X - so teaching weight is 0
	else
	len(replace( substring(LevelStr, II.MinYr + 2, II.NumYrs),'0','')) /  (Activities - A - X)
end WTeach

, case when t=1 then 0 else 1 end ActivityDetail
, SUB.LevelStr
, SurveyDimensionID
from
(
Select *
from warehouse.TeacherLevelStr
WHERE Source not in ('A','A?')
) SUB

INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON SUB.surveyDimensionID = DSS.[Survey ID]

--- Here we'll find the class level with biggest enrolment taught in the school
--- get the default isced subclass to use for Teacher Unpecified (T=1 on warehouse.TeacherLocation)
--- this replaces earlier use of iscedsubclass from warehouse.TeacherLocation
--- if no enrolments in any class level, use the minimum class Level of the school type

LEFT JOIN (
  Select *
  FROM
  (
  Select schNo, surveyYear, Src, DL.*
  , row_number() OVER (Partition by schNo, SurveyYear order by Enrol DESC, seq, M.lvlYear ) RN
  from warehouse.schoolYearLevelMap M
  INNER JOIN DimensionLevel DL
  on M.ClassLevel = DL.LevelCode
  ) SS
  WHERE RN = 1
) DEFLEVEL
ON DEFLEVEL.schNo = SUB.schNo AND DEFLEVEL.SurveyYear = SUB.SurveyYear


INNER JOIN (
 Select schNo, SurveyYear
 , DL.[ISCED SubClass]
 , min(M.lvlYear) minYr
 , max(M.lvlYear) MaxYr
,  max(M.lvlYear) - min(M.lvlYear) + 1 NumYrs
 from warehouse.schoolYearLevelMap M
 INNER JOIN DimensionLevel DL
 on M.ClassLevel = DL.levelCode
 GROUP BY schNo, SurveyYEar, DL.[ISCED SubClass]
) II
ON
	II.schNo = SUB.schNo and II.SurveyYear = SUB.SurveyYear
	AND
	( substring(LevelStr, II.MinYr + 2, II.NumYrs) like '%1%' -- this case gets those that have supplied grtade level data
		OR ( T=1 and II.[ISCED SubClass] = DEFLEVEL.[ISCED SubClass]) -- this case is for 'Teacher Unspecified'.
	)

---- now the second part is to add back the A and X teachers

UNION ALL

Select
SUB.SchNo
, SUB.SurveyYear
, SUB.GenderCode
, DSS.AuthorityGovtCode
, case num when 1 then 'A' when 2 then 'X' end
, tID
, Qualified
, Certified
, Qualified * Certified
, Activities
, 1 /  Activities W

-- WTeach - 'weight teaching' - is used in Uis reporting
-- it is the weight of this isced over teaching activities ie omitting Admin (A) and Other (X) activities
-- the sum for a single teacher across all ISCEDs is 1; therefore the sum across all teachers is equal to the total
-- number of teachers 'teaching'. These are also the same records in TeacherLocation where TAMX in ('T','M')
-- Note if TAMX = 'T' then WT = W

, 0 WTeach

, 1 ActivityDetail
, SUB.LevelStr
, SurveyDimensionID
from
(
Select *
from warehouse.TeacherLevelStr
	INNER JOIN metaNumbers
		ON (num =1 and A=1)
			OR (num=2 and X=1)
WHERE Source not in ('A','A?')
) SUB

INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON SUB.surveyDimensionID = DSS.[Survey ID]
GO

