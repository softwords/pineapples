SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	District Inspections with result
--
-- =============================================
CREATE VIEW [warehouse].[AccreditationSchool]
WITH VIEW_METADATA
AS
Select SurveyYear
, BI.schNo
, DSS.[School Name]
, BI.inspID InspectionID
, BI.InspectionYear
, DSS.DistrictCode
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType, InspectionResult
, count(*) Num
, sum(case when BI.SurveyYear = BI.InspectionYear then 1 end) NumThisYear

FROM warehouse.BestInspection BI
	LEFT JOIN warehouse.dimensionSchoolSurvey DSS
		ON Bi.SurveyDimensionID = DSS.[Survey ID]
WHERE BI.InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GROUP BY SurveyYear
, BI.schNo
, DSS.[School Name]
, BI.inspID
, BI.InspectionYear
, DSS.DistrictCode
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, InspectionResult
GO

