SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRLanguageGroup]
AS
SELECT     lngGrp,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN lnggrpName
	WHEN 1 THEN lnggrpNameL1
	WHEN 2 THEN lnggrpNameL2

END,lnggrpName) AS lnggrpName

FROM         dbo.lkpLanguageGroup
GO
GRANT SELECT ON [dbo].[TRLanguageGroup] TO [public] AS [dbo]
GO

