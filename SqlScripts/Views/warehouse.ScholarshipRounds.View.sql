SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[ScholarshipRounds]
AS

Select
S.scholCode ScholarshipType
, S.schrYear Round
, S.schoFos MajorCode
, FOS.fosName Major
, S.schoStatus StatusCode
, STAT.codeDescription Status
, S.stuGender Gender
, count (distinct S.schoID) Number
, sum(case when posPaid is not null then posAmount else null end) Paid
, max(convert(nvarchar(4), posYear) + ':' + convert(nvarchar(1), posSemester)) LastEnrol
from Scholarships S
	LEFT JOIN lkpScholarshipStatus STAT
		ON S.schoStatus = STAT.codeCode
	LEFT JOIN FieldOfStudy FOS
		ON schoFos = FOS.fosCode
	LEFT JOIN dbo.ScholarshipRounds R
		ON S.schrYear = R.schrYear
		AND S.scholCode = R.scholCode
	LEFT JOIN ScholarshipStudy_ SS
		ON S.schoID = SS.schoID
GROUP BY
	S.scholCode
	, S.schrYear
	, S.stuGender
	, S.schoStatus
	, S.schoFos
	, FOS.fosName
	, STAT.codeDescription
GO

