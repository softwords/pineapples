SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- View
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/06/2022
-- Description:	Warehouse - Exam Results (whole test) aggregated at School level (normalized version)
-- based on level count (aka. indicator count) (and not candidate count)
--
-- Refer to documentation in View [warehouse].[ExamResultsTestLevelCountSchoolX]
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
-- =============================================
CREATE VIEW [warehouse].[ExamResultsTestLevelCountSchool]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, achievementLevel
, achievementDesc
, sum(indicatorCount) LevelCount
FROM warehouse.examSchoolResultsTyped
WHERE RecordType = 'Exam'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, schNo
, Gender
, achievementLevel
, achievementDesc
GO

