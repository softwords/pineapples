SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[TeacherLocationDistrict]
AS
Select TY.*
, I.iGroup DistrictCode
, I.dName District
from  warehouse.TeacherLocation TY
	LEFT JOIN Schools S
		ON S.schNo = TY.schNo
	LEFT JOIN lkpIslands I
		ON I.iCode = S.iCode
GO

