SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date:  02/05/2023
-- Description:	An extended version of [warehouse].[TeacherFtptSchool] which contains
-- Teacher FTE and FTPT numbers by Activity and School also for new hires (newly recruited)
-- Refer to documentation of [warehouse].[TeacherFtptSchool] for further details.
-- =============================================
CREATE VIEW [warehouse].[TeacherFtptSchoolX]
AS
  Select W.schNo, W.SurveyYear, W.GenderCode, W.ISCEDSubClass, LSX.ilCode ISCEDClass
 , TL.AgeGroup, BSS.surveyDimensionID
 , DSS.AuthorityCode, DSS.[District Code] DistrictCode, DSS.SchoolTypeCode
      ,sum(W)	FTE	-- FTE
      ,sum(WQ)	FTEQ
      ,sum(WC)  FTEC
      ,sum(WQC) FTEQC
      ,sum(case when ilsCode in ('A','X') then null else WTeach end) FTPT -- => FTPT
      ,sum(WTeachQ)	FTPTQ
      ,sum(WTeachC) FTPTC
      ,sum(WTeachQC) FTPTQC
	  ,sum(case TAMX when 'A' then 1 end) A
	  ,sum(case TAMX when 'X' then 1 end) X
	  -- New hires
	  ,sum(case when NewHire = 1 then W else null end)	FTENew	-- FTE
      ,sum(case when NewHire = 1 then WQ else null end)	FTEQNew
      ,sum(case when NewHire = 1 then WC else null end)  FTECNew
      ,sum(case when NewHire = 1 then WQC else null end) FTEQCNew
      ,sum(case when ilsCode in ('A','X') or NewHire = 0 then null else WTeach end) FTPTNew -- => FTPT
      ,sum(case when NewHire = 1 then WTeachQ else null end)	FTPTQNew
      ,sum(case when NewHire = 1 then WTeachC else null end) FTPTCNew
      ,sum(case when NewHire = 1 then WTeachQC else null end) FTPTQCNew
	  ,sum(case when TAMX = 'A' and NewHire = 1 then 1 end) ANew
	  ,sum(case when TAMX = 'X' and NewHire = 1 then 1 end) XNew
	  -- In service
	  ,sum(case when Inservice = 1 then W else null end)	FTEInSvc	-- FTE
      ,sum(case when Inservice = 1 then WQ else null end)	FTEQInSvc
      ,sum(case when Inservice = 1 then WC else null end)  FTECInSvc
      ,sum(case when Inservice = 1 then WQC else null end) FTEQCInSvc
      ,sum(case when ilsCode in ('A','X') or Inservice = 0 then null else WTeach end) FTPTInSvc -- => FTPT
      ,sum(case when Inservice = 1 then WTeachQ else null end)	FTPTQInSvc
      ,sum(case when Inservice = 1 then WTeachC else null end) FTPTCInSvc
      ,sum(case when Inservice = 1 then WTeachQC else null end) FTPTQCInSvc
	  ,sum(case when TAMX = 'A' and Inservice = 1 then 1 end) AInSvc
	  ,sum(case when TAMX = 'X' and Inservice = 1 then 1 end) XInSvc
  from [warehouse].[TeacherActivityWeights] W
  INNER JOIN warehouse.TeacherLocation TL
	ON W.tID = TL.tID
	AND W.SurveyYear = TL.SurveyYear
  INNER JOIN warehouse.bestSurvey BSS
		ON W.schNo = BSS.schNo
		AND W.SurveyYear = BSS.SurveyYear
  INNER JOIN warehouse.DimensionSchoolSurvey DSS
	ON BSS.surveyDimensionID = DSS.[Survey ID]
  LEFT JOIN
  ( Select ilsCode, ilCode from ISCEDLevelSub LS
	UNION Select 'A', 'Support'
	UNION Select 'X', 'Support'
  ) LSX
  ON W.ISCEDSUbClass = LSX.ilsCode
  group by W.schNo, W.SurveyYear, W.GenderCode, W.ISCEDSubClass, TL.AgeGroup, BSS.surveyDimensionID
 , DSS.AuthorityCode, DSS.[District Code] , DSS.SchoolTypeCode, LSX.ilCode
GO

