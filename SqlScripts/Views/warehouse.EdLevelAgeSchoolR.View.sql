SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse -  Offical Age
--
-- This view shows the enrolments by school at each class level and age, and identifies
-- the percentage at, over , or under the EdcationLevel Official Age Range, as well as Class Level official age
-- Note the views
-- EdLevelAge, EdLevelAgeNation, EdLevelAgeDistrict are related, bu these can go further by incorporating population,
-- and therefore calculating Gross and Net Enrolment Ratios
-- =============================================
CREATE VIEW [warehouse].[EdLevelAgeSchoolR]
AS
 Select SurveyYear
 , schNo
 , ClassLevel
 , yearOfEd
 , edLevelCode
 , edLevel

 , sum(Enrol) Enrol
 , sum(case when GenderCode = 'M' then Enrol end) EnrolM
 , sum(case when GenderCode = 'F' then Enrol end) EnrolF
 , sum(EstimatedEnrol) EstimatedEnrol
 , sum(case when GenderCode = 'M' then EstimatedEnrol end) EstimatedEnrolM
 , sum(case when GenderCode = 'F' then EstimatedEnrol end) EstimatedEnrolF

, sum(UnderAge) UnderAge
, sum(case when GenderCode = 'M' then UnderAge end) UnderAgeM
, sum(case when GenderCode = 'F' then UnderAge end) UnderAgeF
, sum(OfficialAge) OfficialAge
, sum(case when GenderCode = 'M' then OfficialAge end) OfficialAgeM
, sum(case when GenderCode = 'F' then OfficialAge end) OfficialAgeF
, sum(OverAge) OverAge
, sum(case when GenderCode = 'M' then OverAge end) OverAgeM
, sum(case when GenderCode = 'F' then OverAge end) OverAgeF

, sum(UnderClassAge) UnderClassAge
, sum(case when GenderCode = 'M' then UnderClassAge end) UnderClassAgeM
, sum(case when GenderCode = 'F' then UnderClassAge end) UnderClassAgeF
, sum(OfficialClassAge) OfficialClassAge
, sum(case when GenderCode = 'M' then OfficialClassAge end) OfficialClassAgeM
, sum(case when GenderCode = 'F' then OfficialClassAge end) OfficialClassAgeF
, sum(OverClassAge) OverClassAge
, sum(case when GenderCode = 'M' then OverClassAge end) OverClassAgeM
, sum(case when GenderCode = 'F' then OverClassAge end) OverClassAgeF

from warehouse.EdLevelAgeSchool
GROUP BY
SurveyYear
 , schNo
 , ClassLevel
 , yearOfEd
 , edLevelCode
 , edLevel
GO

