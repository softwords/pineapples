SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - District Ed Level Age
--
-- District enrolments divideded by class level, showing whether the pupil is at, over or under
-- the EdLevel OfficialAge
-- The year of ed of the class level determines the EdLevel here, and the official age range
-- is determined in the usual way (from Offical Start Age and Range of years of Ed for the Ed Level)
-- Suggested Use: High Level analysis of over / under age enrolments.
-- Note this view uses only the principal education levels defined in lkpEducationLevels
-- It does not use the Alt or Alt2 groupings (to do?)
-- see also:
-- warehouse.EdLevelAgeNation
-- =============================================
CREATE VIEW [warehouse].[EdLevelAgeAuthorityGovt]
AS
Select SurveyYear
, GenderCode

, AuthorityGovtCode
, AuthorityGovt

, sum(Enrol) Enrol
, sum(EstimatedEnrol ) EstimatedEnrol

, EdLevel
, sum(UnderAge ) UnderAge
, sum(OfficialAge ) OfficialAge
, sum(OverAge ) OverAge

, ClassLevel
, yearOfEd
, sum(UnderClassAge ) UnderClassAge
, sum(OfficialClassAge ) OfficialClassAge
, sum(OverClassAge ) OverClassAge

From Warehouse.EdLevelAgeAuthority

GROUP BY
surveyYear, ClassLevel, yearOfEd, EdLevel, GenderCode
, AuthorityGovtCode
, AuthorityGovt
GO

