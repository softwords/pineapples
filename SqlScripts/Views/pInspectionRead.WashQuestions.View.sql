SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInspectionRead].[WashQuestions]
AS
Select v.value('id[1]', 'nvarchar(20)') QID
, v.value('name[1]', 'nvarchar(200)') QName

, v.value('flags[1]', 'nvarchar(20)') QFlags
from lkpInspectionTypes
CROSS APPLY intyTemplate.nodes('//question') V(v)
WHERE intyCode = 'WASH'
GO

