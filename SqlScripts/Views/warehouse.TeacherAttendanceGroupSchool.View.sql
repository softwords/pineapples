SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2020
-- Description:	Warehouse - Teacher Attendance Groups
--
-- A specialized view of attendance by groups teachers
-- with attendance groups
-- =============================================
CREATE VIEW [warehouse].[TeacherAttendanceGroupSchool]
AS
SELECT tchTAM
	  , Gender
	  , tchSector
	  , svyYear
	  , schNo
	  , [School Name]
	  , [District Code]
	  , [District]
	  , [Region Code]
	  , [Region]
	  , [AuthorityCode]
	  , [Authority]
	  , [AuthorityGroupCode]
	  , [AuthorityGroup]
	  , [SchoolTypeCode]
	  , [SchoolType]
	  , [Island Code]
	  , [Island]
, sum(case when isnull(TotalDaysOfAbsence,0) = 0 then Num end) 'None'
, sum (case when TotalDaysOfAbsence between 1 and 5 then Num end) '1-5'
, sum (case when TotalDaysOfAbsence between 6 and 10 then Num end) '6-10'
, sum (case when TotalDaysOfAbsence between 11 and 15 then Num end) '11-15'
, sum (case when TotalDaysOfAbsence > 20 then Num end) '>20'
FROM
(
Select [TotalDaysOfAbsence]
	  , tchTAM
	  , Gender
	  , tchSector
	  , svyYear
	  , schNo
	  , [School Name]
	  , [District Code]
	  , [District]
	  , [Region Code]
	  , [Region]
	  , [AuthorityCode]
	  , [Authority]
	  , [AuthorityGroupCode]
	  , [AuthorityGroup]
	  , [SchoolTypeCode]
	  , [SchoolType]
	  , [Island Code]
	  , [Island]
	  ,count(tID) Num
from warehouse.TeacherAttendanceSchool
group by [TotalDaysOfAbsence]
	  , tchTAM
	  , Gender
	  , tchSector
	  , svyYear
	  , schNo
	  , [School Name]
	  , [District Code]
	  , [District]
	  , [Region Code]
	  , [Region]
	  , [AuthorityCode]
	  , [Authority]
	  , [AuthorityGroupCode]
	  , [AuthorityGroup]
	  , [SchoolTypeCode]
	  , [SchoolType]
	  , [Island Code]
	  , [Island]
) SUB
group by tchTAM
	  , Gender
	  , tchSector
	  , svyYear
	  , schNo
	  , [School Name]
	  , [District Code]
	  , [District]
	  , [Region Code]
	  , [Region]
	  , [AuthorityCode]
	  , [Authority]
	  , [AuthorityGroupCode]
	  , [AuthorityGroup]
	  , [SchoolTypeCode]
	  , [SchoolType]
	  , [Island Code]
	  , [Island]
GO

