SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 20 July 2019
-- Description:	Base View of school accreditations
-- ==================================================
-- This list combines new format SchoolAccreditations and 'legacy' accreditations, presenting a unified output.
-- Note the for new surveys, the scores presented are converted to a 'level', rather than presenting the
-- number of subcriteria=POSITIVE. This is for consistency with the earlier model.

-- Thus, when working with this view, and its 'frozen' equivalent in the warehouse (warehouse.accreditations)
-- we can treat the numbers as the levels achieved for each criteria - NOT the number of positive answers.
-- We can get the number of positive answers from pInspectionRead.SchoolStandardAssessment, or by going directly to the Xml.
--
-- pInspectionRead.SCHOOL_ACCREDITATION
--			this view named by inspection will be used by proc pInspectionRead.Readinspection

-- pInspectionRead.SchoolAccreditations
--			for backward compatibility, mirrors ( except for column names) the earlier version of this view.
--			This is just a wrapper around pInspectionRead.SCHOOL_ACCREDITATION
--			Note that this view is no longer editable; ie trigger is gone.
--
-- History Log:
--   - Ghislain Hachey: fixed wrong partitioning at the standard level (SE.1, SE.2, etc)
-- =============================================
CREATE VIEW [pInspectionRead].[SCHOOL_ACCREDITATION]
AS
Select U.*
, common.tally(0, [SE.1.1], [SE.1.2], [SE.1.3], [SE.1.4]
, [SE.2.1], [SE.2.2], [SE.2.3], [SE.2.4]
, [SE.3.1], [SE.3.2], [SE.3.3], [SE.3.4]
, [SE.4.1], [SE.4.2], [SE.4.3], [SE.4.4]
, [SE.5.1], [SE.5.2], [SE.5.3], [SE.5.4]
, [SE.6.1], [SE.6.2], [SE.6.3], [SE.6.4]
, [CO.1], [CO.2]
) Level0

, common.tally(1, [SE.1.1], [SE.1.2], [SE.1.3], [SE.1.4]
, [SE.2.1], [SE.2.2], [SE.2.3], [SE.2.4]
, [SE.3.1], [SE.3.2], [SE.3.3], [SE.3.4]
, [SE.4.1], [SE.4.2], [SE.4.3], [SE.4.4]
, [SE.5.1], [SE.5.2], [SE.5.3], [SE.5.4]
, [SE.6.1], [SE.6.2], [SE.6.3], [SE.6.4]
, [CO.1], [CO.2]
) Level1
, common.tally(2, [SE.1.1], [SE.1.2], [SE.1.3], [SE.1.4]
, [SE.2.1], [SE.2.2], [SE.2.3], [SE.2.4]
, [SE.3.1], [SE.3.2], [SE.3.3], [SE.3.4]
, [SE.4.1], [SE.4.2], [SE.4.3], [SE.4.4]
, [SE.5.1], [SE.5.2], [SE.5.3], [SE.5.4]
, [SE.6.1], [SE.6.2], [SE.6.3], [SE.6.4]
, [CO.1], [CO.2]
) Level2
, common.tally(3, [SE.1.1], [SE.1.2], [SE.1.3], [SE.1.4]
, [SE.2.1], [SE.2.2], [SE.2.3], [SE.2.4]
, [SE.3.1], [SE.3.2], [SE.3.3], [SE.3.4]
, [SE.4.1], [SE.4.2], [SE.4.3], [SE.4.4]
, [SE.5.1], [SE.5.2], [SE.5.3], [SE.5.4]
, [SE.6.1], [SE.6.2], [SE.6.3], [SE.6.4]
, [CO.1], [CO.2]
) Level3
, common.tally(4, [SE.1.1], [SE.1.2], [SE.1.3], [SE.1.4]
, [SE.2.1], [SE.2.2], [SE.2.3], [SE.2.4]
, [SE.3.1], [SE.3.2], [SE.3.3], [SE.3.4]
, [SE.4.1], [SE.4.2], [SE.4.3], [SE.4.4]
, [SE.5.1], [SE.5.2], [SE.5.3], [SE.5.4]
, [SE.6.1], [SE.6.2], [SE.6.3], [SE.6.4]
, [CO.1], [CO.2]
) Level4
FROM
(
Select inspID
	, 'tablet' as Source
	, schNo
	, schName
	, StartDate
	, EndDate
	, InspectionYear
	, InspectedBy
	, inspTypeCode
	, InspectionType
	, InspectionResult
	, SourceId
	, InspectionClass
	, Partition
	, pCreateTag
	, pCreateUser
	, pCreateDateTime
	, pEditUser
	, pEditDateTime
	-- values based on tablet version of Xml, where subcriteria are available
	-- SSACRIT partition turns the number of positive subcriteria ('score') to a level ( the 'result')
	-- Level as so defined is not so important at the criteria level;
	-- for FSM SSACRIT maps 0,1,2 => Level 1; 3 => Level 2; 4 => Level 3
	-- If we used the SSA partitition 50/75/90/100 then 4 would map to Level 4 and there would be no 3
	--
	-- For RMI we map : 0,1 => 1 2=>2 3=>3 and 4=>4 ... however this will create some anomalies when calculating
	-- e.g a score of 3 in every criteria will give an overall Level of 3 overall (based on Tally); but Level 2 for every Standard!
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.1.1"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		-- 4 is a cheat should be the count of subcriteria
		, 4, 100) [SE.1.1]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.1.2"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.1.2]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.1.3"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.1.3]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.1.4"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.1.4]
	-- for standard, score using SSA partition: refer to FSM Accreditation manual and issue #1058
	, dbo.PartitionValue('SSA' -- Should this not be SSA defined partitions?
		, v.value('fn:count(./category/standard[id="SE.1"]/criteria/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		-- 4 is a cheat should be the count of subcriteria
		, 16, 100) [SE.1]


	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.2.1"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.2.1]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.2.2"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.2.2]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.2.3"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.2.3]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.2.4"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.2.4]
	-- for standard, score using SSA partition: refer to FSM Accreditation manual and issue #1058
	, dbo.PartitionValue('SSA'
		, v.value('fn:count(./category/standard[id="SE.2"]/criteria/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 16, 100) [SE.2]

	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.3.1"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.3.1]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.3.2"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.3.2]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.3.3"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.3.3]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.3.4"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.3.4]
	-- for standard, score using SSA partition: refer to FSM Accreditation manual and issue #1058
	, dbo.PartitionValue('SSA'
		, v.value('fn:count(./category/standard[id="SE.3"]/criteria/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 16, 100) [SE.3]

	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.4.1"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.4.1]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.4.2"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.4.2]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.4.3"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.4.3]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.4.4"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.4.4]
	-- for standard, score using SSA partition: refer to FSM Accreditation manual and issue #1058
	, dbo.PartitionValue('SSA'
		, v.value('fn:count(./category/standard[id="SE.4"]/criteria/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 16, 100) [SE.4]

	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.5.1"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.5.1]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.5.2"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.5.2]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.5.3"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.5.3]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.5.4"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.5.4]
	-- for standard, score using SSA partition: refer to FSM Accreditation manual and issue #1058
	, dbo.PartitionValue('SSA'
		, v.value('fn:count(./category/standard[id="SE.5"]/criteria/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 16, 100) [SE.5]

	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.6.1"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.6.1]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.6.2"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.6.2]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.6.3"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.6.3]
	, dbo.PartitionValue('SSACRIT'
		, v.value('fn:count(./category/standard/criteria[id="SE.6.4"]/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 4, 100) [SE.6.4]
	-- for standard, score using SSA partition: refer to FSM Accreditation manual and issue #1058
	, dbo.PartitionValue('SSA'
		, v.value('fn:count(./category/standard[id="SE.6"]/criteria/subcriteria/answer[state="POSITIVE"])[1]', 'numeric')
		, 16, 100) [SE.6]

		-- capturing the total for the classroom observation uses the SSA scale
	, dbo.PartitionValue('SSA'
		, v.value('fn:count(./category[name="Classroom Observation 1"]/standard/criteria/subcriteria/answer[state="POSITIVE"])', 'numeric')
		, v.value('fn:count(./category[name="Classroom Observation 1"]/standard/criteria/subcriteria)', 'numeric')
		, 100) [CO.1]
	, dbo.PartitionValue('SSA'
		, v.value('fn:count(./category[name="Classroom Observation 2"]/standard/criteria/subcriteria/answer[state="POSITIVE"])', 'numeric')
		, v.value('fn:count(./category[name="Classroom Observation 2"]/standard/criteria/subcriteria)', 'numeric')
		, 100) [CO.2]

from pInspectionRead.SchoolInspections SI
	CROSS APPLY InspectionContent.nodes('/survey') as V(v)
	WHERE InspTypeCode = 'SCHOOL_ACCREDITATION'
	AND SourceId is not null

UNION

Select inspID
	, 'legacy' as Source
	, schNo
	, schName
	, StartDate
	, EndDate
	, InspectionYear
	, InspectedBy
	, inspTypeCode
	, InspectionType
	, InspectionResult
	, SourceId
	, InspectionClass
	, Partition
	, pCreateTag
	, pCreateUser
	, pCreateDateTime
	, pEditUser
	, pEditDateTime

	-- values based on fabricated version of Xml, where:
	-- * criteria result (e.g. Level 2) is in result node
	-- * criteria resultvalue (e.g. 2) is in resultvalue node
	-- * criteria/score contains a calculated score value derived from the average
	-- * of the partition range in the SSA partitition
	-- * e.g. a resultValue of 3 is range 75-90 => avg is 82.5 out of 4 is 3.3
	-- * what goes back onto the SchoolAccreditation record is the result - ie resultvalue
	, v.value('fn:sum(./category/standard/criteria[id="SE.1.1"]/resultvalue)', 'float') [SE.1.1]
	, v.value('fn:sum(./category/standard/criteria[id="SE.1.2"]/resultvalue)', 'float') [SE.1.2]
	, v.value('fn:sum(./category/standard/criteria[id="SE.1.3"]/resultvalue)', 'float') [SE.1.3]
	, v.value('fn:sum(./category/standard/criteria[id="SE.1.4"]/resultvalue)', 'float') [SE.1.4]
	, dbo.PartitionValue('SSA'
		, v.value('fn:sum(./category/standard[id="SE.1"]/criteria/score)', 'float')
		, 16, 100) [SE.1]


	, v.value('fn:sum(./category/standard/criteria[id="SE.2.1"]/resultvalue)', 'float') [SE.2.1]
	, v.value('fn:sum(./category/standard/criteria[id="SE.2.2"]/resultvalue)', 'float') [SE.2.2]
	, v.value('fn:sum(./category/standard/criteria[id="SE.2.3"]/resultvalue)', 'float') [SE.2.3]
	, v.value('fn:sum(./category/standard/criteria[id="SE.2.4"]/resultvalue)', 'float') [SE.2.4]
	, dbo.PartitionValue('SSA'
		, v.value('fn:sum(./category/standard[id="SE.2"]/criteria/score)', 'float')
		, 16, 100) [SE.2]

	, v.value('fn:sum(./category/standard/criteria[id="SE.3.1"]/resultvalue)', 'float') [SE.3.1]
	, v.value('fn:sum(./category/standard/criteria[id="SE.3.2"]/resultvalue)', 'float') [SE.3.2]
	, v.value('fn:sum(./category/standard/criteria[id="SE.3.3"]/resultvalue)', 'float') [SE.3.3]
	, v.value('fn:sum(./category/standard/criteria[id="SE.3.4"]/resultvalue)', 'float') [SE.3.4]
	, dbo.PartitionValue('SSA'
		, v.value('fn:sum(./category/standard[id="SE.3"]/criteria/score)', 'float')
		, 16, 100) [SE.3]

	, v.value('fn:sum(./category/standard/criteria[id="SE.4.1"]/resultvalue)', 'float') [SE.4.1]
	, v.value('fn:sum(./category/standard/criteria[id="SE.4.2"]/resultvalue)', 'float') [SE.4.2]
	, v.value('fn:sum(./category/standard/criteria[id="SE.4.3"]/resultvalue)', 'float') [SE.4.3]
	, v.value('fn:sum(./category/standard/criteria[id="SE.4.4"]/resultvalue)', 'float') [SE.4.4]
	, dbo.PartitionValue('SSA'
		, v.value('fn:sum(./category/standard[id="SE.4"]/criteria/score)', 'float')
		, 16, 100) [SE.4]

	, v.value('fn:sum(./category/standard/criteria[id="SE.5.1"]/resultvalue)', 'float') [SE.5.1]
	, v.value('fn:sum(./category/standard/criteria[id="SE.5.2"]/resultvalue)', 'float') [SE.5.2]
	, v.value('fn:sum(./category/standard/criteria[id="SE.5.3"]/resultvalue)', 'float') [SE.5.3]
	, v.value('fn:sum(./category/standard/criteria[id="SE.5.4"]/resultvalue)', 'float') [SE.5.4]
	, dbo.PartitionValue('SSA'
		, v.value('fn:sum(./category/standard[id="SE.5"]/criteria/score)', 'float')
		, 16, 100) [SE.5]

	, v.value('fn:sum(./category/standard/criteria[id="SE.6.1"]/resultvalue)', 'float') [SE.6.1]
	, v.value('fn:sum(./category/standard/criteria[id="SE.6.2"]/resultvalue)', 'float') [SE.6.2]
	, v.value('fn:sum(./category/standard/criteria[id="SE.6.3"]/resultvalue)', 'float') [SE.6.3]
	, v.value('fn:sum(./category/standard/criteria[id="SE.6.4"]/resultvalue)', 'float') [SE.6.4]
	, dbo.PartitionValue('SSA'
		, v.value('fn:sum(./category/standard[id="SE.6"]/criteria/score)', 'float')
		, 16, 100) [SE.6]

	-- this case is olde assessments that do not even have classroom evaluation
	,case when v.value( '(./category[name="Classroom Observation 1"]/resultvalue)[1]', 'int') is null
		then null
		else v.value( 'fn:sum(./category[name="Classroom Observation 1"]/resultvalue)', 'float')
	end [CO.1]
	,case when v.value( '(./category[name="Classroom Observation 2"]/resultvalue)[1]', 'int') is null
		then null
		else v.value( 'fn:sum(./category[name="Classroom Observation 2"]/resultvalue)', 'float')
	end [CO.2]
from pInspectionRead.SchoolInspections SI
CROSS APPLY InspectionContent.nodes('/survey') as V(v)
WHERE InspTypeCode = 'SCHOOL_ACCREDITATION'
--AND InspectionContent.value('(/survey/version)[1]', 'int') = 0
AND SourceId is null
) U
WHERE InspTypeCode = 'SCHOOL_ACCREDITATION'
GO

