SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pExamRead].[DistrictLevelTyped]
AS
select examID, examCode, examYear, examName
, Gender
, DistrictCode
, District

, RecordType
, ID
, [Key]
, Description

, achievementLevel
, achievementDesc

, sum(indicatorCount) indicatorCount
, sum(weight) weight
, sum(CandidateCount) candidateCount
from pExamRead.SchoolLevelTyped CL

GROUP BY examID, examCode, examYear, examName
, Gender
, DistrictCode
, District
, RecordType
, ID
, [Key]
, Description

, achievementLevel
, achievementDesc
GO

