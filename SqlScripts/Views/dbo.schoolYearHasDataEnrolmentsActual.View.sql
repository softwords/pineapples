SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:August 2022
-- Description:	Report if enrolment data exists for a school in a given year

--			  : schoolYearHasDataEnrolments will include data created by "smoothing" or by "explicit" modification
--			  : By contrast, schoolYearHasDataEnrolmentsActual ignores smoothed values -
--            : This is so that 'estimated' data can be used for other enrolment related data; ie repeaters etc.
--            : That is to say, it is assumed that when SMOOTHING is applied to Enrolments it is NOT applied
--            : to Repeaters etc. If there is repeater data in the year of the SMOOTHED enrolments, it is ignored.
--            : If smoothed values have been explcitly supplied for pupil tables, set the enrolment data to EXPLICIT
--            : This is treated the same as 'real' data, expect that it is counted as Estimated for the purposes of reporting
--            : on Estimated %.
-- =============================================
CREATE VIEW [dbo].[schoolYearHasDataEnrolmentsActual]
AS
SELECT     S.schNo, S.svyYear,
			S.ssID EnrolssID,
			S.ssSchType ,
			S.ssEnrol SSenrol,
			Q.ssqLevel
FROM         dbo.SchoolSurvey AS S LEFT OUTER JOIN
                      dbo.tfnQualityIssues('Pupils', 'Enrolment') AS Q ON S.ssID = Q.ssID
WHERE     ssEnrol > 0
			and (S.ssID not in (Select DISTINCT ssID from Enrollments WHERE enOrigin = 'SMOOTHING'))

			and (q.ssqLevel is null or q.ssqLevel <2)
GO

