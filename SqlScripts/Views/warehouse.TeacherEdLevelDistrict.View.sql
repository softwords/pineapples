SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[TeacherEdLevelDistrict]
WITH VIEW_METADATA
AS
Select SurveyYear
, edLevelCode
, T.AuthorityGovt
, DistrictCode
, District

, GenderCode
, count(DISTINCT tID) NumTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when Qualified = 1 and Certified = 1 then tID end)  QualCert
, count(DISTINCT case when NewHire = 1 then tID end) NumNewTeachers
, count(DISTINCT case when InService = 1 then tID end)  InService

, sum(W) fte
, sum(WQ) fteQ
, sum(WC) fteC
, sum(WQC) fteQC
, sum(WTeach) ftpt
, sum(WTeachQ) ftptQ
, sum(WTeachC) ftptC
, sum(WTeachQC) ftptQC
, sum(WTeach * NewHire) ftptNew
, sum(WTeach * InService) ftptInSvc

from warehouse.TeacherActivityWeightsEdLevel T
	INNER JOIN warehouse.dimensionSchoolSurvey DSS
		on SurveyDimensionID = DSS.[Survey ID]
group by SurveyYear, edLevelCode, T.AuthorityGovt, DistrictCode, District, GenderCode
GO

