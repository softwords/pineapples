SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- View
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Warehouse - enrolment 'grid' by Authority
-- A standard presentation of enrolment data is a grid, with age in the rows, and
-- Level / Gender in the columns
-- To assist banded report writers to produce this layout it is convenient to
-- denormalise the enrolment data so that all levels x gender appear on a single row
--
--
-- see also
-- warehouse.EnrolGrid
-- warehouse.EnrolGridDistrict
-- warehouse.EnrolGridAuthority
-- for other aggregations
-- Note that this query produces columns by Year of Education
-- In systems hwere it is important to separate Class Levels that have the same year of ed,
-- you can use the stored proc warehouse.GridMaker
-- =============================================
CREATE VIEW [warehouse].[EnrolGridSchoolType]
AS
Select
EE.SurveyYear
, ee.Age
, SchoolTypeCode
, T.stDescription SchoolType
, SUM([Y-2F]) [Y-2F]
, SUM([Y-2M]) [Y-2M]
, SUM([Y-2])  [Y-2]

, SUM([Y-1F]) [Y-1F]
, SUM([Y-1M]) [Y-1M]
, SUM([Y-1])  [Y-1]

, SUM([Y00F]) [Y00F]
, SUM([Y00M]) [Y00M]
, SUM([Y00])  [Y00]

, SUM([Y01F]) [Y01F]
, SUM([Y01M]) [Y01M]
, SUM([Y01])  [Y01]

, SUM([Y02F]) [Y02F]
, SUM([Y02M]) [Y02M]
, SUM([Y02])  [Y02]

, SUM([Y03F]) [Y03F]
, SUM([Y03M]) [Y03M]
, SUM([Y03])  [Y03]

, SUM([Y04F]) [Y04F]
, SUM([Y04M]) [Y04M]
, SUM([Y04])  [Y04]

, SUM([Y05F]) [Y05F]
, SUM([Y05M]) [Y05M]
, SUM([Y05])  [Y05]

, SUM([Y06F]) [Y06F]
, SUM([Y06M]) [Y06M]
, SUM([Y06])  [Y06]

, SUM([Y07F]) [Y07F]
, SUM([Y07M]) [Y07M]
, SUM([Y07])  [Y07]

, SUM([Y08F]) [Y08F]
, SUM([Y08M]) [Y08M]
, SUM([Y08])  [Y08]

, SUM([Y09F]) [Y09F]
, SUM([Y09M]) [Y09M]
, SUM([Y09])  [Y09]

, SUM([Y10F]) [Y10F]
, SUM([Y10M]) [Y10M]
, SUM([Y10])  [Y10]

, SUM([Y11F]) [Y11F]
, SUM([Y11M]) [Y11M]
, SUM([Y11])  [Y11]

, SUM([Y12F]) [Y12F]
, SUM([Y12M]) [Y12M]
, SUM([Y12])  [Y12]

, SUM([Y13F]) [Y13F]
, SUM([Y13M]) [Y13M]
, SUM([Y13])  [Y13]

, SUM([Y14F]) [Y14F]
, SUM([Y14M]) [Y14M]
, SUM([Y14])  [Y14]

, SUM([TotF]) [TotF]
, SUM([TotM]) [TotM]
, SUM([Tot])  [Tot]

, SUM([Est])  [Est]

from warehouse.enrolGrid ee
	LEFT JOIN SchoolTypes T
		ON EE.SchoolTypeCode = T.stCode
GROUP BY EE.surveyYear
, EE.Age
, EE.SchoolTypeCode
, T.stDescription
GO

