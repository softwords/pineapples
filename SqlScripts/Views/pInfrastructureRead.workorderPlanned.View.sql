SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 2 2010
-- Description:	the workorders where there is a duplication of school and item
-- =============================================
CREATE VIEW [pInfrastructureRead].[workorderPlanned]
AS
SELECT WorkItems.[witmID]
      ,WorkItems.[schNo]
      ,Schools.[SchName]
      ,WorkItems.[bldID]
      , Buildings.bldgTitle
      ,WorkItems.[witmType]
      , WT.codeDescription [WorkItem Type]
      ,[woRef] [WO Ref]
      ,[woStatus] [W/O Status]
      ,[witmProgress] Progress
      ,[witmQty]
      ,[witmDesc]
      ,[witmEstCost]
      ,[witmSourceOfFunds]
      ,[witmCostCentre]
      ,[witmContractValue]
      ,[witmProgressDate]
      ,[witmActualCost]
      ,[witmInspectedBy]
      ,[witmEstBaseCost]
      ,[witmEstBaseUnit]
      ,[witmBldCreateType]
      ,[witmRoomsClass]
      ,[witmRoomsOHT]
      ,[witmRoomsStaff]
      ,[witmRoomsAdmin]
      ,[witmRoomsStorage]
      ,[witmRoomsDorm]
      ,[witmRoomsKitchen]
      ,[witmRoomsDining]
      ,[witmRoomsLibrary]
      ,[witmRoomsSpecialTuition]
      ,[witmRoomsHall]
      ,[witmRoomsOther]
      , WorkOrders.[woID]
      ,[woDesc]	  [W/O Desc]
      ,[woPlanned]
      ,[woBudget]
      ,[woWorkApproved]
      ,[woFinanceApproved]
      ,[woSourceOfFunds]
      ,[woDonorManaged]
      ,[woCostCentre]
      ,[woTenderClose]
      ,[woContracted]
      ,[supCode]
      ,[woContractValue]
      ,[woCommenced]
      ,[woPlannedCompletion]
      ,[woCompleted]
      ,[woInvoiceValue]
      ,[woSignoff]
      ,[woSignOffUser]


  FROM WorkItems
	INNER JOIN Schools
		ON WorkItems.schNo = Schools.schNo

  LEFT JOIN WorkOrders
	ON WorkOrders.woID = WorkITems.woID
  LEFT JOIN Buildings
	ON Buildings.bldID = WorkItems.bldID
  LEFT JOIN TRWorkItemType WT
	on WT.codeCode = WorkItems.witmType
WHERE woPlanned < getdate()
and woStatus is null
or woStatus not in ('Signed Off', 'Discontinued')
GO

