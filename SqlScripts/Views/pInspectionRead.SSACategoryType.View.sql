SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=============================================
-- Author:		Brian Lewis
-- Create date: 22 July 2019
-- Description:	For an inspection in the format of a
-- 'SchoolStandardAssessment' present totals by CategoryType
-- At this level we have both a score and a MaxScore,
-- so from that we get:
--    ScorePerc - the percentage of Score/MaxScore
--	  Level - a Level category mapped to a ScorePerc
--			using the partitition associated to the inspection type
--
-- =============================================
CREATE VIEW [pInspectionRead].[SSACategoryType]
WITH VIEW_METADATA
AS
Select SUB.*
, ptName Level
FROM
(
	SELECT SSA.inspID, CategoryType
	, sum(Score) Score
	, sum(maxScore) MaxScore
	, convert(decimal(3,0), sum(Score) * 100 / convert(float,sum(MaxScore))) ScorePerc
	from pInspectionRead.SchoolStandardAssessment SSA
	group by SSA.inspID, CategoryType
) SUB
left JOIN Partitions P
	ON SUB.ScorePerc between ptMin and ptMax
GO

