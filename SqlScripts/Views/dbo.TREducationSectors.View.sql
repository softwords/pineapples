SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TREducationSectors]
AS
SELECT     secCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN secDesc
	WHEN 1 THEN secDescL1
	WHEN 2 THEN secDescL2

END,secDesc) AS secDesc,
secYears,
secMinAge,
secSort

FROM         dbo.EducationSectors
GO
GRANT SELECT ON [dbo].[TREducationSectors] TO [public] AS [dbo]
GO

