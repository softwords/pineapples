SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: November 2019
-- Description:	Aggregate view of special ed data
-- =============================================
CREATE VIEW [warehouse].[SpecialEd]
WITH VIEW_METADATA AS

SELECT SurveyYear
      ,EdLevelCode
      ,EdLevel
      ,EnvironmentCode
      ,Environment
      ,DisabilityCode
      ,Disability
      ,EnglishLearnerCode
      ,EnglishLearner
      ,EthnicityCode
      ,Ethnicity
      ,GenderCode
      ,Gender
      ,Age
      ,AuthorityCode
      ,Authority
      ,DistrictCode
      ,District
      ,AuthorityGovtCode
      ,AuthorityGovt
      ,SchoolTypeCode
      ,SchoolType
      ,RegionCode
      ,Region
      ,sum(Num) Num
  FROM warehouse.enrolSpEd
GROUP BY SurveyYear
      ,EdLevelCode
      ,EdLevel
      ,EnvironmentCode
      ,Environment
      ,DisabilityCode
      ,Disability
      ,EnglishLearnerCode
      ,EnglishLearner
      ,EthnicityCode
      ,Ethnicity
      ,GenderCode
      ,Gender
      ,Age
      ,AuthorityCode
      ,Authority
      ,DistrictCode
      ,District
      ,AuthorityGovtCode
      ,AuthorityGovt
      ,SchoolTypeCode
      ,SchoolType
      ,RegionCode
      ,Region
GO

