SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 28/03/2024
-- Description:	Warehouse - Extended version of TeacherHousingTypesAndCondition containing all dimensions (i.e. possible disaggregations)
--
-- History log:
--   * 28/03/2024, Ghislain Hachey, Created view
-- =============================================
CREATE VIEW
[warehouse].[TeacherHousingTypesAndConditionX]
AS
SELECT CAST([Survey Data Year] AS INT) [SurveyYear]
	, VTH.Materials HousingMaterial
	, VTH.HousingType
	, VTH.resNumber Number
	, VTH.resName HousingTypeGroup
	, CASE
		WHEN VTH.HousingCondition = 'A' THEN 'Good'
		WHEN VTH.HousingCondition = 'B' THEN 'Fair'
		WHEN VTH.HousingCondition = 'C' THEN 'Poor'
		ELSE 'Unknown'
		END HousingCondition
	, [Survey ID]
	, [Survey Data Year]
	, [School No]
	, [School Name]
	, [District Code]
	, [District Code] DistrictCode
	, District
	, [District Short]
	, [Island Code]
	, Island
	, [Region Code]
	, Region
	, Size
	, [Local Electorate No]
	, [Local Electorate]
	, [National Electorate No]
	, [National Electorate]
	, DSS.AuthorityCode
	, Authority
	, AuthorityTypeCode
	, AuthorityType
	, AuthorityGroupCode
	, AuthorityGroup
	, LanguageCode
	, Language
	, LanguageGroupCode
	, LanguageGroup
	, CurrentSchoolTypeCode
	, [Current School Type]
	, DSS.SchoolTypeCode
	, SchoolType
	, [School Class]
	, [Year Closed]
FROM [dbo].[vtblTeacherHousing] VTH
JOIN DimensionSchoolSurvey DSS ON VTH.ssID = DSS.[Survey ID]
GO

