SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: October 2019
-- Description:	A view to select only special ed students from the student table
-- Special Ed status may be revised each year (held on StudentEnrolment_)
-- so this view finds any Student_ who has at least 1 SpEd record for some year.
-- Further, it returns the earliest and the most recent years in which the student is special ed
-- The special ed 'demographics', also held on StudentEnrolment_ are reported from the most recent enrolment record.
-- A trigger manages the updating of this view to kep the underlying tables updated.
-- =============================================
CREATE VIEW [dbo].[SpecialEdStudent_]
WITH VIEW_METADATA
AS

  --Select * from Student_
  --WHERE stuID in (Select stuID from StudentEnrolment_ WHERE stuespEd = 1)
   Select S.*
 , minYear
 , maxYEar
 , SE.stueSpEdDisability
 , SE.stueSpEdEnglish
 , SE.stueSpEdEnv
 , SE.stueSpEdIEP
 , SE.stueSpEdHasAccommodation
 , SE.stueSpEdAccommodation
 , SE.stueSpEdAssessment
 , SE.stueSpEdExit
 FROM Student_ S
 INNER JOIN (Select stuID
 , min(stueYear) minYear
 , min(stueYear) maxYear
 From StudentEnrolment_
 WHERE stueSpEd = 1
 GROUP BY stuID
 ) REG
 ON S.stuID = REG.stuID
 INNER JOIN ( Select *
 FROM StudentEnrolment_) SE
 ON REG.stuID = SE.stuID
 AND REG.maxYear = SE.stueYear
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: recreated April 2020
-- Description:	trigger for maintaining special ed
-- =============================================
CREATE TRIGGER [dbo].[SpecialEdUpdate]
   ON  [dbo].[SpecialEdStudent_]
   INSTEAD OF UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--- update on the student record
	UPDATE Student_
	SET stuCardID = I.stuCardID
	, stuNamePrefix = I.stuNamePrefix
	, stuGiven = I.stuGiven
	, stuFamilyName = I.stuFamilyName
	, stuNameSuffix = I.stuNameSuffix
	, stuDoB = I.stuDoB
	, stuDoBEst = I.stuDobEst
	, stuGender = I.stuGender
	, stuEthnicity = I.stuEthnicity
	, pEditDateTime = I.pEditDateTime
	, pEditUser = I.pEditUser
	FROM Student_
		INNER JOIN INSERTED I
			ON Student_.stuID = I.stuID
		INNER JOIN DELETED D
			ON I.stuID = D.stuID

	-- UPDATE on the enrolment record
	UPDATE StudentEnrolment_
		set stueSpEdDisability = I.stueSpEdDisability,
		stueSpEdEnglish = I.stueSpEdEnglish,
		stueSpEdEnv = I.stueSpEdEnv,
		stueSpEdIEP = I.stueSpEdIEP,
		stueSpEdHasAccommodation = I.stueSpEdHasAccommodation,
		stueSpEdAccommodation = I.stueSpEdAccommodation,
		stueSpEdAssessment = I.stueSpEdAssessment,
		stueSpEdExit = I.stueSpEdExit
	FROM StudentEnrolment_
		INNER JOIN INSERTED I
			ON StudentEnrolment_.stuID = I.stuID
			AND StudentEnrolment_.stueYear = I.maxYear
		INNER JOIN DELETED D
			ON I.stuID = D.stuID

	-- DELETE from SpecialEd just means that we have to turn the special ed flag off on the 
	-- enrolment record
	UPDATE StudentEnrolment_
		SET stueSpEd = null

	FROM StudentEnrolment_
		INNER JOIN DELETED D
			ON StudentEnrolment_.stuID = D.stuID
		LEFT JOIN INSERTED I
			ON I.stuID = D.stuID		
	WHERE I.stuID is null

END
GO

