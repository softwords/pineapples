SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2020
-- Description:	Warehouse - Teacher Attendance
--
-- The base view for teacher attendance down at the schools
-- level with all basic disaggregation
-- =============================================
CREATE VIEW [warehouse].[TeacherAttendanceSchool]
AS
SELECT TS.tID
	  , tchTAM
	  , TS.tchGender AS Gender
	  , tchSector
	  , svyYear
	  , SS.schNo
	  , DSS.[School Name]
	  , DSS.[District Code]
	  , DSS.[District]
	  , DSS.[Region Code]
	  , DSS.[Region]
	  , DSS.[AuthorityCode]
	  , DSS.[Authority]
	  , DSS.[AuthorityGroupCode]
	  , DSS.[AuthorityGroup]
	  , DSS.[SchoolTypeCode]
	  , DSS.[SchoolType]
	  , DSS.[Island Code]
	  , DSS.[Island]
	  , TS.tcheData.value('(/row/@Total_Days_Absence)[1]','float') TotalDaysOfAbsence
  FROM [dbo].[TeacherSurvey] TS
  INNER JOIN SchoolSurvey  SS ON TS.ssID = SS.ssID
  INNER JOIN warehouse.dimensionSchoolSurvey DSS ON SS.schNo = DSS.[School No] AND SS.svyYear = DSS.[Survey Data Year]
GO

