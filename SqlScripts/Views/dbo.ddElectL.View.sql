SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ddElectL]
AS
SELECT     TOP 100 PERCENT ElectL, SortOrder
FROM         (SELECT     '(all)' AS ElectL, '' AS SortOrder
                       FROM          dbo.metaNumbers
                       WHERE      (num = 0)
                       UNION
                       SELECT     codeDescription, codeSort AS SortOrder
                       FROM         dbo.lkpElectorateL) AS L
ORDER BY SortOrder
GO
GRANT DELETE ON [dbo].[ddElectL] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ddElectL] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ddElectL] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ddElectL] TO [public] AS [dbo]
GO

