SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVAnticipatedIntake]
AS
SELECT DimensionSchoolSurvey.*,
SchoolSurvey.ssPlan0 AS [Anticipated Intake NY Prep],
SchoolSurvey.ssPlan1 AS [Anticipated Intake NY Form 1],
SchoolSurvey.ssPlan2 AS [Anticipated Intake NY Form 4],
SchoolSurvey.ssPlan3 AS [Anticipated Intake NY Form 6],
SchoolSurvey.ssPlan4 AS [Anticipated Intake NY Form 7]
FROM DimensionSchoolSurvey INNER JOIN SchoolSurvey
ON DimensionSchoolSurvey.[Survey ID] = SchoolSurvey.ssID;
GO

