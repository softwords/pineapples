SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Nation Flow - report format
-- contents of warehouse.FlowSchool, but denormalised by Gender
-- =============================================
CREATE VIEW [warehouse].[FlowSchoolR]
AS
SELECT SurveyYear
	  , SchoolNo
	  , [School Name]
      ,YearOfEd

      ,sum(case when GenderCode is null then Enrol end) Enrol
	  ,sum(case when GenderCode = 'M' then Enrol end) EnrolM
	  ,sum(case when GenderCode = 'F' then Enrol end) EnrolF

      ,sum(case when GenderCode is null then Rep end) Rep
	  ,sum(case when GenderCode = 'M' then Rep end) RepM
	  ,sum(case when GenderCode = 'F' then Rep end) RepF

      ,sum(case when GenderCode is null then RepNY end) RepNY
	  ,sum(case when GenderCode = 'M' then RepNY end) RepNYM
	  ,sum(case when GenderCode = 'F' then RepNY end) RepNYF

      ,sum(case when GenderCode is null then TroutNY end) TroutNY
	  ,sum(case when GenderCode = 'M' then TroutNY end) TroutNYM
	  ,sum(case when GenderCode = 'F' then TroutNY end) TroutNYF

      ,sum(case when GenderCode is null then EnrolNYNextLevel end) EnrolNYNextLevel
	  ,sum(case when GenderCode = 'M' then EnrolNYNextLevel end) EnrolNYNextLevelM
	  ,sum(case when GenderCode = 'F' then EnrolNYNextLevel end) EnrolNYNextLevelF

      ,sum(case when GenderCode is null then RepNYNextLevel end) RepNYNextLevel
	  ,sum(case when GenderCode = 'M' then RepNYNextLevel end) RepNYNextLevelM
	  ,sum(case when GenderCode = 'F' then RepNYNextLevel end) RepNYNextLevelF

      ,sum(case when GenderCode is null then TrinNYNextLevel end) TrinNYNextLevel
	  ,sum(case when GenderCode = 'M' then TrinNYNextLevel end) TrinNYNextLevelM
	  ,sum(case when GenderCode = 'F' then TrinNYNextLevel end) TrinNYNextLevelF

      ,sum(case when GenderCode is null then RepeatRate end) RepeatRate
	  ,sum(case when GenderCode = 'M' then RepeatRate end) RepeatRateM
	  ,sum(case when GenderCode = 'F' then RepeatRate end) RepeatRateF

      ,sum(case when GenderCode is null then RepeatRateTR end) RepeatRateTR
	  ,sum(case when GenderCode = 'M' then RepeatRateTR end) RepeatRateTRM
	  ,sum(case when GenderCode = 'F' then RepeatRateTR end) RepeatRateTRF

      ,sum(case when GenderCode is null then PromoteRate end) PromoteRate
	  ,sum(case when GenderCode = 'M' then PromoteRate end) PromoteRateM
	  ,sum(case when GenderCode = 'F' then PromoteRate end) PromoteRateF

      ,sum(case when GenderCode is null then PromoteRateTR end) PromoteRateTR
	  ,sum(case when GenderCode = 'M' then PromoteRateTR end) PromoteRateTRM
	  ,sum(case when GenderCode = 'F' then PromoteRateTR end) PromoteRateTRF

      ,sum(case when GenderCode is null then DropoutRate end) DropoutRate
	  ,sum(case when GenderCode = 'M' then DropoutRate end) DropoutRateM
	  ,sum(case when GenderCode = 'F' then DropoutRate end) DropoutRateF

      ,sum(case when GenderCode is null then DropoutRateTR end) DropoutRateTR
	  ,sum(case when GenderCode = 'M' then DropoutRateTR end) DropoutRateTRM
	  ,sum(case when GenderCode = 'F' then DropoutRateTR end) DropoutRateTRF

      ,sum(case when GenderCode is null then SurvivalRate end) SurvivalRate
	  ,sum(case when GenderCode = 'M' then SurvivalRate end) SurvivalRateM
	  ,sum(case when GenderCode = 'F' then SurvivalRate end) SurvivalRateF

      ,sum(case when GenderCode is null then SurvivalRateTR end) SurvivalRateTR
	  ,sum(case when GenderCode = 'M' then SurvivalRateTR end) SurvivalRateTRM
	  ,sum(case when GenderCode = 'F' then SurvivalRateTR end) SurvivalRateTRF

  FROM [warehouse].[FlowSchool]
  GROUP BY SurveyYear
	, SchoolNo
	, [School Name]
	,YearOfEd
GO

