SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[scholarships]
WITH VIEW_METADATA
AS
SELECT schoID
      ,S.stuID
	  , SR.scholCode
	  , SR.schrYear
      , schoExpectedCompletion
      , schoStatus
      , schoStatusDate
	  , schoAppliedDate
	  , schoCompliantDate
	  , schoAwardedDate
	  , schoCompletedDate
	  , schoInst
	  , schoFos
	  , schoSubject
	  , schoGPA
	  , schoNote
      , S.pRowversion
      , S.pCreateDateTime
      , S.pCreateUser
      , S.pEditDateTime
      , S.pEditUser
	  , S.pCreateTag

	  -- from student_
	  ,stuCardID
      ,stuNamePrefix
      ,stuGiven
      ,stuMiddleNames
      ,stuFamilyName
      ,stuNameSuffix
      ,stuDoB
      ,stuDoBEst
      ,stuGender
	  ,stuMaritalStatus
      ,stuEthnicity

  FROM dbo.Scholarships_ S
	INNER JOIN Student_ STU
		ON S.stuID = STU.stuID
	INNER JOIN ScholarshipRounds SR
		ON S.schrID = SR.schrID
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2020
-- Description:	Write scholarship records
--
-- History Log:
--   * 09/05/2022, Ghislain Hachey, edit the action string in audit logs and add user that deletes record.
-- =============================================
CREATE TRIGGER [dbo].[ScholarshipsUpdate]
   ON  [dbo].[scholarships]
   INSTEAD OF INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- check the student table if we have a student ID, we can update it
	-- note we can update student even if we arae inserting into Scholarship

	DECLARE @user nvarchar(100) = original_login()
	DECLARE @dt datetime = getutcdate()

	UPDATE Student_
	SET stuCardID = I.stuCardID
	, stuNamePrefix = I.stuNamePrefix
	, stuGiven = I.stuGiven
	, stuMiddleNames = I.stuMiddleNames
	, stuFamilyName = I.stuFamilyName
	, stuNameSuffix = I.stuNameSuffix
	
		, stuDoB = I.stuDoB
		, stuDoBEst = I.stuDobEst
		, stuGender = I.stuGender
		, stuMaritalStatus = I.stuMaritalStatus
		, stuEthnicity = I.stuEthnicity
	FROM Student_
		INNER JOIN INSERTED I
			ON Student_.stuID = I.stuID

	-- if the stuID is null, create it; otherwise use the suplied stuID
	-- as the new key if it is not already defined

	INSERT INTO Student_
	(
		stuID	
		, stuCardID
		, stuNamePrefix
		, stuGiven
		, stuMiddleNames
		, stuFamilyName
		, stuNameSuffix
		, stuDoB
		, stuDoBEst
		, stuGender
		, stuMaritalStatus
		, stuEthnicity
	)
	SELECT isnull(I.stuID, pCreateTag)
	, I.stuCardID
	, I.stuNamePrefix
	, I.stuGiven
	, I.stuMiddleNames
	, I.stuFamilyName
	, I.stuNameSuffix
	, I.stuDoB
	, I.stuDobEst
	, I.stuGender
	, I.stuMaritalStatus
	, I.stuEthnicity

	FROM INSERTED I
	LEFT JOIN Student_
		ON I.stuID = Student_.stuID
		WHERE student_.stuID is null


-- update the scholarship record
	UPDATE Scholarships_
      SET [schrID] = SR.schrID
      ,schoExpectedCompletion = I.schoExpectedCompletion
	  , schoAppliedDate = case when I.schoStatus in ('APPLIED') then I.schoStatusDate
								else I.schoAppliedDate end
	  , schoCompliantDate = case when I.schoStatus in ('COMPLIANT') then I.schoStatusDate
								else I.schoCompliantDate end

	  , schoAwardedDate = case when I.schoStatus in ('AW', 'AWARDED') then I.schoStatusDate
								else I.schoAwardedDate end
	  , schoCompletedDate = case when I.schoStatus in ('COMPLETED') then I.schoStatusDate
								else I.schoCompletedDate end

      ,schoStatus = I.schoStatus
      ,schoStatusDate = I.schoStatusDate
	  , schoInst = I.schoInst
	  , schoFos = I.schoFos
	  , schoSubject = I.schoSubject
	  , schoGPA = I.schoGPA
	  , schoNote = I.schoNote
      ,pEditDateTime = I.pEditDateTime
      ,pEditUser = isnull(I.pEditUser, @user)
  
	FROM Scholarships_ S
		INNER JOIN INSERTED I
		ON S.schoID = I.schoID
		INNER JOIN ScholarshipRounds SR
			ON SR.scholCode = I.scholCode
			AND SR.schrYear = I.schrYEar

-- make sure the scholarship round is there?
INSERT INTO ScholarshipRounds
( schrYear, scholCode)
SELECT DISTINCT I.schrYear, I.scholCode
FROM INSERTED I
	LEFT JOIN ScholarshipRounds SR
		ON I.scholCode = Sr.scholCode
		AND I.schrYear = Sr.schrYear
WHERE SR.scholCode is null



-- insert new scholarship
	INSERT INTO Scholarships_
	(
	  stuID
	  , schrID
   	  ,schoExpectedCompletion
      ,schoStatus
      ,schoStatusDate
	  , schoAppliedDate
	  , schoCompliantDate
	  , schoAwardedDate
	  , schoCompletedDate
	  , schoInst
	  , schoFos
	  , schoSubject
	  , schoGPA
       ,pCreateDateTime
      ,pCreateUser
      ,pEditDateTime
      ,pEditUser
	  ,pCreateTag

	)
SELECT
		isnull(stuID, pCreateTag)
		, SR.schrID
   		,schoExpectedCompletion
      ,isnull(schoStatus, 'APPLIED')
      ,isnull(I.schoStatusDate, common.today())
	  , case when isnull(I.schoStatus,'APPLIED') in ('APPLIED') then isnull(I.schoStatusDate, common.today()) end
	   , case when I.schoStatus in ('COMPLIANT') then I.schoStatusDate end
	  , case when I.schoStatus in ('AW', 'AWARDED') then I.schoStatusDate end
	  , case when I.schoStatus in ('COMPLETED') then I.schoStatusDate end
	  , schoInst
	  , schoFos
	  , schoSubject
	  , schoGPA
      ,isnull(I.pCreateDateTime, @dt)
      ,isnull(I.pCreateUser, @user)
      ,isnull(I.pEditDateTime, @dt)
      ,isnull(I.pEditUser, @user)
	  , pCreateTag
	FROM INSERTED I
	INNER JOIN ScholarshipRounds SR
		ON SR.scholCode = I.scholCode
		AND SR.schrYear = I.schrYEar
	WHERE I.schoID is null

-- write aduits if the status changes

-- audit INSERT
 INSERT INTO ScholarshipAudit_
   (
    schoID
	, schoStatus
	, schoStatusDate
   , pCreateDateTime
   , pCreateUser
   , schoaNote
   )
   Select
   S.schoID
 	, I.schoStatus
	, I.schoStatusDate
   , I.pCreateDateTime
   , I.pCreateUser
   , 'Scholarship record created'
   FROM INSERTED I 
   LEFT JOIN DELETED D
		on D.schoID = I.schoID
	LEFT JOIN Scholarships_ S	-- get back the freshly created schoID identity
		ON I.pCreateTag = S.pCreateTag
	WHERE D.schoID is null

	INSERT INTO ScholarshipAudit_
   (
    schoID
	, schoStatus
	, schoStatusDate
   , pCreateDateTime
   , pCreateUser
   , schoaNote
   )
   Select
   I.schoID
	-- audit update
   , case when coalesce(I.schoStatus, D.schoStatus) is null then null
			when I.schoStatus = D.schoStatus then null
			else I.schoStatus end newschoStatus
    , case when coalesce(I.schoStatusDate, D.schoStatusDate) is null then null
			when I.schoStatusDate = D.schoStatusDate then null
			else I.schoStatusDate end newschoStatusDate

	, I.pEditDateTime
	, I.pEditUser
	, 'Scholarship record updated'
   FROM INSERTED I 
   INNER JOIN DELETED D
		on D.schoID = I.schoID

    INSERT INTO ScholarshipAudit_
   (
		schoID
		, schoaNote
		, pCreateDateTime
		, pCreateUser
	)
   Select
   D.schoID
   , 'Scholarship record deleted'
   , getUtcdate()
   , ISNULL(D.pEditUser, @user)
   FROM DELETED D
	LEFT JOIN INSERTED I 
		on D.schoID = I.schoID
	WHERE I.schoID is null



END
GO

