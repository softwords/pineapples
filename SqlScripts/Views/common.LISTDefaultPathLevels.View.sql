SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [common].[LISTDefaultPathLevels]
AS
/*
	The default path is used as a way of ensuring we have only
	one class level at each year of education
	Otherwise some joins return multiple records

	Levels may share a year of education in different sectors

*/
SELECT     TOP 100 PERCENT dbo.EducationPathLevels.levelCode, dbo.TRLevels.codeDescription AS [Level], dbo.TRLevels.lvlYear AS YearOfEd
FROM         dbo.EducationPaths INNER JOIN
                      dbo.TRLevels INNER JOIN
                      dbo.EducationPathLevels ON dbo.TRLevels.codeCode = dbo.EducationPathLevels.levelCode ON
                      dbo.EducationPaths.pathCode = dbo.EducationPathLevels.pathCode
WHERE     (dbo.EducationPaths.pathDefault = 1)
ORDER BY YearOfEd
GO

