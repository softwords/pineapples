SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pTeacherRead].[TeacherWideView]
AS

Select TI.*
, case when SURVEY.tID is null then 0 else 1 end HasSurvey
, SURVEY.svyYear		SurveyYear
, SURVEY.schNo			SurveySchool
, SURVEY.schName		SurveySchoolName
, SURVEY.tchRole		SurveyRole
, case when APPT.tID is null then 0 else 1 end HasAppointment
, APPT.taDate			ApptDate
, APPT.estpNo			PositionNo
, APPT.estpScope		PositionScope
, APPT.estpRoleGrade	PositionRole
, APPT.SchNo			PositionSchool
, APPT.schName			PositionSchoolName
, case when PSLIP.tpsPayroll is null then 0 else 1 end HasPayslip
, PSLIP.tpsPeriodStart  LastPayDate
, PSLIP.tpsPosition		LastPayPosition
, PSLIP.tpsPaypoint		LastPayPaypoint
, PSLIP.tpsSurname		PayslipSurname
, PSLIP.tpsGiven		PayslipGiven
, PSLIP.tpsSalaryPoint	PayslipSalaryPoint

FROM TEacherIdentity TI
LEFT JOIN
	(
	Select SS.ssID
	, svyYEar
	, SS.schNo
	, S.schName
	, tchsID
	, tchRole
	, tID
	, row_number() OVER ( PARTITION BY tID ORDER BY svyYear DESC) rown
	FROM SchoolSurvey SS
		INNER JOIN TeacherSurvey TS
			ON SS.ssID = TS.ssID
		INNER JOIN Schools S
			ON SS.schNo = S.schNo
	) SURVEY
ON Ti.tID = SURVEY.tID
	AND SURVEY.rown = 1

LEFT JOIN
(
Select tID
, taDate
, taEndDate
, taType
, TA.estpNo
, E.estpScope
, E.estpRoleGrade
, E.SchNo
, S.schName

, row_number() OVER( PARTITION BY tID ORDER BY taDate DESC) rown
from TeacherAppointment TA
	LEFT JOIN Establishment E
		ON TA.estpNo = E.estpNo
	LEFT JOIN Schools S
		ON E.schNo = S.schNo
) APPT
ON TI.tID = APPT.tID
	AND APPT.rown = 1
LEFT JOIN
(
	Select tpsPeriodStart
	, tpsPeriodEnd
	, tpsSalaryPoint
	, tpsPosition
	, tpsPayroll
	, tpsPaypoint
	, tpsSurname
	, tpsGiven
	, row_number() OVER (PARTITION BY tpsPayroll ORDER BY tpsPeriodStart DESC) rown
	FRom TeacherPayslips
) PSLIP
ON TI.tPayroll = PSLIP.tpsPayroll
	AND PSLIP.rown = 1
GO

