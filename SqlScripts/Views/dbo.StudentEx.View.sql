SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  	Brian Lewis
-- Create date: 27/08/2021
-- Description:	Student with extra fields typically coming from stueData XML
-- =============================================
CREATE VIEW [dbo].[StudentEx]
AS
SELECT
  S.[stuID],
  [stuCardID],
  [stuNamePrefix],
  [stuGiven],
  [stuMiddleNames],
  [stuFamilyName],
  [stuNameSuffix],
  [stuDoB],
  [stuDoBEst],
  [stuGender],
  [stuEthnicity],
  [stuCreateFileRef],
  [stuEditFileRef],
  S.[pCreateDateTime],
  S.[pCreateUser],
  S.[pEditDateTime],
  S.[pEditUser],
  S.[pRowversion],
  [stuGivenSoundex],
  [stuFamilySoundex],
  [stuRedirect],
  [stuMaritalStatus],
  --NULLIF(LTRIM(d.value('@Language', 'nvarchar(100)')), '') stuLangName
  d.value('@Language', 'nvarchar(100)') stuLangName,
  L.langCode [stuLangCode]
  -- Any other extra data can be added here
FROM Student_ S
INNER JOIN
(
SElECT *
, ROW_NUMBER() OVER (PARTITION BY stuID ORDER BY stueYear DESC) RN
FROM StudentEnrolment_
) SE ON S.stuID = SE.stuID
OUTER APPLY stueData.nodes('row') D(d)
LEFT JOIN lkpLanguage L -- Any extra joins for loading the codes instead or with the name
  ON d.value('@Language', 'nvarchar(100)') = L.langName
WHERE SE.RN = 1
GO

