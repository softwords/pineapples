SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2019
-- Description:	Warehouse - Enrolment data by Authority
--
-- This is a simple consoilidation of warehouse.enrol to group by Authority
-- in report format
-- since AuthorityType and AuthorityGroup are determined by Authority, these are included too
-- to facilitate groupings and subtotalling
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation
--		warehouse.EnrolAuthority

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.EnrolAuthorityR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW
[warehouse].[EnrolAuthorityR]
AS
Select SurveyYear
, E.AuthorityCode
, Authority
, AuthorityTypeCode
, Authoritytype
, AuthorityGroupCode
, AuthorityGroup

, ClassLevel
, Age

, sum(case when GenderCode = 'M' then Enrol end) EnrolM
, sum(case when GenderCode = 'F' then Enrol end) EnrolF
, sum(Enrol) Enrol

, sum(case when GenderCode = 'M' then Rep end) RepM
, sum(case when GenderCode = 'F' then Rep end) RepF
, sum(Rep) Rep

, sum(case when GenderCode = 'M' then Trin end) TrinM
, sum(case when GenderCode = 'F' then Trin end) TrinF
, sum(Trin) Trin

, sum(case when GenderCode = 'M' then Trout end) TroutM
, sum(case when GenderCode = 'F' then Trout end) TroutF
, sum(Trout) Trout

, sum(case when GenderCode = 'M' then Boarders end) BoardersM
, sum(case when GenderCode = 'F' then Boarders end) BoardersF
, sum(Boarders) Boarders

, sum(case when GenderCode = 'M' then Disab end) DisabM
, sum(case when GenderCode = 'F' then Disab end) DisabF
, sum(Disab) Disab

, sum(case when GenderCode = 'M' then Dropout end) DropoutM
, sum(case when GenderCode = 'F' then Dropout end) DropoutF
, sum(Dropout) Dropout

, sum(case when GenderCode = 'M' then PSA end) PSAM
, sum(case when GenderCode = 'F' then PSA end) PSAF
, sum(PSA) PSA

, sum(case when GenderCode = 'M' then Completed end) CompletedM
, sum(case when GenderCode = 'F' then Completed end) CompletedF
, sum(Completed) Completed

from warehouse.tableEnrol E
	LEFT JOIN DimensionAuthority DA
		ON E.AuthorityCode = DA.AuthorityCode
GROUP BY
SurveyYear
, E.AuthorityCode
, Authority
, AuthorityTypeCode
, Authoritytype
, AuthorityGroupCode
, AuthorityGroup
, ClassLevel
, Age
GO

