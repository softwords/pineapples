SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	School hohrt alternative name
-- Effectively this view is a "synonym" for
-- the table warehouse.SchoolCohort. Thi is to conform with revised naming standards in the warehouse
--
-- =============================================
CREATE VIEW [warehouse].[CohortSchool]
AS
Select SurveyYear
, C.schNo SchoolNo
, schName [School Name]
, YearOfEd
, GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.SchoolCohort C
	LEFT JOIN Schools S
		ON C.schNo = S.schNo

GROUP BY SurveyYear
, C.schNo
, schName
, YearOfEd
, GenderCode
UNION ALL
Select SurveyYear
, C.schNo SchoolNo
, schName
, YearOfEd
, null GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.SchoolCohort C
	LEFT JOIN Schools S
		ON C.schNo = S.schNo
GROUP BY SurveyYear
, C.schNo
, schName
, YearOfEd
GO

