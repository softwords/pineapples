SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolYearHasDataParentContributions]
AS
SELECT DISTINCT    dbo.SchoolSurvey.svyYear, dbo.SchoolSurvey.schNo,
		   dbo.SchoolSurvey.ssID, ssqLevel

FROM       dbo.SchoolSurvey INNER JOIN dbo.vtblParentContributions ON dbo.SchoolSurvey.ssID= dbo.vtblParentContributions.ssID
			LEFT OUTER JOIN
                      dbo.tfnQualityIssues('Finance', 'PCONT') Q
					on schoolsurvey.ssID = q.ssID
GO

