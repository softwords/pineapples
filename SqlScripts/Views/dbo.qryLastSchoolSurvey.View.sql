SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qryLastSchoolSurvey]
AS
SELECT     schNo, MAX(svyYear) AS MaxSvyYear
FROM         dbo.SchoolSurvey
GROUP BY schNo
GO

