SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Warehouse - Enrolment and population data by District , in report format
--
-- This is a consolidation of warehouse.enrol to group by District
-- including District Population for each Age, if available
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
--
-- History Log:
--   * 30 July 2021, Ghislain Hachey, Added Completed column since it is potentially available from end of year census
-- =============================================
CREATE VIEW
[warehouse].[EnrolPopDistrictR]
AS
Select SurveyYear
, Age
, DistrictCode
, dName District
, sum(case when GenderCode = 'M' then Enrol end) EnrolM
, sum(case when GenderCode = 'F' then Enrol end) EnrolF
, sum(Enrol) Enrol


, sum(case when GenderCode = 'M' then Rep end) RepM
, sum(case when GenderCode = 'F' then Rep end) RepF
, sum(Rep) Rep

, sum(case when GenderCode = 'M' then Trin end) TrinM
, sum(case when GenderCode = 'F' then Trin end) TrinF
, sum(Trin) Trin

, sum(case when GenderCode = 'M' then Trout end) TroutM
, sum(case when GenderCode = 'F' then Trout end) TroutF
, sum(Trout) Trout

, sum(case when GenderCode = 'M' then Boarders end) BoardersM
, sum(case when GenderCode = 'F' then Boarders end) BoardersF
, sum(Boarders) Boarders

, sum(case when GenderCode = 'M' then Disab end) DisabM
, sum(case when GenderCode = 'F' then Disab end) DisabF
, sum(Disab) Disab

, sum(case when GenderCode = 'M' then Dropout end) DropoutM
, sum(case when GenderCode = 'F' then Dropout end) DropoutF
, sum(Dropout) Dropout

, sum(case when GenderCode = 'M' then Expelled end) ExpelledM
, sum(case when GenderCode = 'F' then Expelled end) ExpelledF
, sum(Expelled) Expelled

, sum(case when GenderCode = 'M' then PSA end) PSAM
, sum(case when GenderCode = 'F' then PSA end) PSAF
, sum(PSA) PSA

, sum(case when GenderCode = 'M' then Completed end) CompletedM
, sum(case when GenderCode = 'F' then Completed end) CompletedF
, sum(Completed) Completed

, sum(case when GenderCode = 'M' then Pop end) PopM
, sum(case when GenderCode = 'F' then Pop end) PopF
, sum(Pop) Pop

from warehouse.EnrolAndPop E
	LEFT JOIN Districts D
		ON E.DistrictCode = D.dID
	INNER JOIN
	(
		select SurveyYear SY, max(age) MaxA, min(Age) MinA from warehouse.Enrol WHERE Enrol is not null GROUP BY SurveyYear
	) AgeRange
		ON E.SurveyYear = AgeRange.SY
		AND E.Age between AgeRange.MinA and AgeRange.MaxA
GROUP BY SurveyYear
, DistrictCode
, dName
, Age
GO

