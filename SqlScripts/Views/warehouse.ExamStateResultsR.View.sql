SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: May 2020
-- Description:	Warehouse - Exams State Results
--
-- This is the 'R' version of [warehouse].[ExamStateResults]
-- =============================================
CREATE VIEW [warehouse].[ExamStateResultsR]
AS
SELECT [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[StateID]
      ,[State]
      ,[standardID]
      ,[standardCode]
      ,[standardDesc]
      ,[benchmarkID]
      ,[benchmarkCode]
      ,[achievementLevel]
      ,[achievementDesc]
      ,[benchmarkDesc]
	  ,sum(case when [Gender] = 'M' then Candidates end) CandidatesM
	  ,sum(case when [Gender] = 'F' then Candidates end) CandidatesF
	  ,sum(Candidates) CandidatesT
  FROM [warehouse].[ExamStateResults]
  GROUP BY
       [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[StateID]
      ,[State]
      ,[standardID]
      ,[standardCode]
      ,[standardDesc]
      ,[benchmarkID]
      ,[benchmarkCode]
      ,[achievementLevel]
      ,[achievementDesc]
      ,[benchmarkDesc]
GO

