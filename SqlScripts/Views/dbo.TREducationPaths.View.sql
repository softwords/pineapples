SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TREducationPaths]
AS
SELECT     pathCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN pathName
	WHEN 1 THEN pathNameL1
	WHEN 2 THEN pathNameL2

END,pathName) AS pathName
FROM         dbo.EducationPaths
GO
GRANT SELECT ON [dbo].[TREducationPaths] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TREducationPaths] TO [public] AS [dbo]
GO

