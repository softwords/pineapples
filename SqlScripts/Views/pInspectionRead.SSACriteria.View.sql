SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=============================================
-- Author:		Brian Lewis
-- Create date: 22 July 2019
-- Description:	For an inspection in the format of a
-- 'SchoolStandardAssessment' present totals by Criteria
-- At this level we have both a score and a MaxScore,
-- so from that we get:
--    ScorePerc - the percentage of Score/MaxScore
--	  Level - a Level category mapped to a level
--			using the partitition associated to the inspection type
-- Note that a Criteria may be split between categories
-- so to produce Criteria totals this query does not aggregate on Category
-- =============================================
CREATE VIEW [pInspectionRead].[SSACriteria]
WITH VIEW_METADATA
AS
Select SUB.*
, ptName Level
FROM
(
	SELECT SSA.inspID, CategoryType, standard, criteria
	, isnull(SI.Partition, 'SSA') Partition
	, sum(Score) Score
	, sum(maxScore) MaxScore
	, convert(decimal(3,0), sum(Score) * 100 / convert(float,sum(MaxScore))) ScorePerc
	from pInspectionRead.SchoolStandardAssessment SSA
		INNER JOIN pInspectionRead.SchoolInspections SI
			ON SSa.inspID = SI.inspID
	group by SSA.inspID, CategoryType, standard, criteria, SI.Partition
) SUB
left JOIN Partitions P
	ON SUB.Partition = ptSet
	AND SUB.ScorePerc between ptMin and ptMax
GO

