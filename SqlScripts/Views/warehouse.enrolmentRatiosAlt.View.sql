SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Description:	Warehouse - Enrolment Ratios based on education level definitions in lkpEducationLevelsAlt
-- Create date: 2017
--
-- this view assembles data points needed for calculation of Enrolment Ratios.
-- In particular, it makes both Population and Enrolment available in one view,
-- although these are not on the same record.
-- consumers of this view will GROUP to get pop and enrol on the same output record.
-- this is an intermediate view used in the construction of the following warehouse items:
--   - [EdLevelAge]
--   - [EdLevelAgeNation]
--   - [EdLevelAgeDistrict]
--   - [ClassLevelAge]
--   - [ClassLevelAgeNation]
--   - [ClassLevelAgeDistrict]
--
-- History Log:
--   * 30/07/2021, Ghislain Hachey, Add support for SDG definition of OVER age (at least 2 years more then official age
--   * 30/07/2021, Ghislain Hachey, Port what looks like adjustments made to [warehouse].[enrolmentRatios]
-- =============================================
CREATE VIEW [warehouse].[enrolmentRatiosAlt]
 WITH VIEW_METADATA
 AS
 Select E.SurveyYear
 , E.Estimate
 , E.ClassLevel
 , E.Age
 , E.GenderCode
 , E.DistrictCode
 , E.AuthorityCode
 , E.SchoolTypeCode
 , E.Enrol
 , E.Rep
 , E.Trin
 , E.Trout
 , E.PSA
, null Pop
 , ELA.yearOfEd
 , ELA.edLevelCode
 , ELA.edLevel
 , ELA.EdLevelOfficialAge
 , ELA.classLevelOfficialAge
 , ELA.classLevelOfficialAgeSDG
 , case when edLevelOfficialAge = '=' then Enrol end EdLevelOfficialAgeEnrol
 , case when ClassLevelOfficialAge = '=' then Enrol end ClassLevelOfficialAgeEnrol
 , case when ClassLevelOfficialAgeSDG = '=' then Enrol end ClassLevelOfficialAgeEnrolSDG
 , case when ClassLevelOfficialAge = '=' then Rep end ClassLevelOfficialAgeRep

 from warehouse.tableEnrol E
	LEFT JOIN lkpLevels L
		ON E.ClassLevel = L.codeCode
	LEFT JOIN  dimensionEdLevelAge ELA
		ON E.SurveyYear = ELA.svyYear
		AND L.lvlYear = ELA.yearOfEd
		AND (
		-- how to account for missing age here?
		-- if age is unknown, assume at official age of class - note this row is unique for the class level
				E.Age = ELA.Age
				OR (E.Age is null and ClassLevelOfficialAge = '=' and edLevelClassification = 'Alt')
		)
		WHERE (Enrol is not null or Rep is not null or PSA is not null)
		 AND edLevelClassification = 'Alt' -- or ELA.svyYear is null) -- account for left join
UNION ALL
Select
popYear SurveyYear
, 0 Estimate
, DL.levelCode ClassLevel
, popAge Age
, genderCode
, dID DistrictCode
, null AuthorityCode
, null schoolTypeCode
, null Enrol
, null Rep
, null Trin
, null Trout
, null PSA
, pop Pop
 , ELA.yearOfEd
 , ELA.edLevelCode
 , ELA.edLevel
 , ELA.EdLevelOfficialAge
 , ELA.classLevelOfficialAge
 , ELA.classLevelOfficialAgeSDG
 , null EdLevelOfficialAgeEnrol
 , null ClassLevelOfficialAgeEnrol
 , null ClassLevelOfficialAgeEnrolSDG
 , null ClassLevelOfficialAgeRep
from warehouse.measurePopG	P
	INNER JOIN dimensionEdLevelAge ELA
		ON P.popYear = ELA.svyYear
		AND P.popAge = ELA.Age
		AND ELA.ClassLevelOfficialAge = '='
		AND ELA.ClassLevelOfficialAgeSDG = '='
		AND ELA.edLevelClassification = 'Alt'
	-- note it may be possible to have more than one class level at the same year of ed
	-- we do not want to duplicate population in this circumstance,
	-- so we select only one class level at each year of education,
	-- by using ListDefaultPathLevels ie those classes that are in the default "path"
	LEFT JOIN ListDefaultPathLevels DL
		ON ELA.yearOfEd = DL.YearOfEd
GO

