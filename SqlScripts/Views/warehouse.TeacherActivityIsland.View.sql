SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherActivity national totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherActivityIsland]
WITH VIEW_METADATA
AS
Select SurveyYear
, Activity
, [Island Code] IslandCode
, Island
, [Region Code] RegionCode
, Region
, [District Code] DistrictCode
, District

, sum(Staff) Staff
, sum(StaffQ) StaffQ
, sum(StaffC) StaffC
, sum(StaffQC) StaffQC

, sum(StaffW) StaffW
, sum(StaffQ_W) StaffQ_W
, sum(StaffC_W) StaffC_W
, sum(StaffQC_W) StaffQC_W

, sum(Enrol) Enrol
, case when isnull(sum(Staff),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(Staff))) end GradePTR
, case when isnull(sum(StaffW),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffW))) end GradePTRW

, case when isnull(sum(StaffQ),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffQ))) end GradePTRQ
, case when isnull(sum(StaffQ_W),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffQ_W))) end GradePTRQ_W

, case when isnull(sum(StaffC),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffC))) end GradePTRC
, case when isnull(sum(StaffC_W),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffC_W))) end GradePTRC_W

, case when isnull(sum(StaffQC),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffQC))) end GradePTRQC
, case when isnull(sum(StaffQC_W),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffQC_W))) end GradePTRQC_W


FROM warehouse.TeacherActivitySchool S
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON S.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, Activity
, [Island Code]
, Island
, [Region Code]
, Region
, [District Code]
, District
GO

