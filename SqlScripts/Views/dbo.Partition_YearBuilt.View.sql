SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Partition_YearBuilt]
AS
-- these views are to control specific permissions on different lookups
Select * from [Partitions]
WHERE ptSet = 'YearBuilt'
GO
GRANT DELETE ON [dbo].[Partition_YearBuilt] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[Partition_YearBuilt] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Partition_YearBuilt] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[Partition_YearBuilt] TO [public] AS [dbo]
GO

