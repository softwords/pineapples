SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2018
-- Description:	Warehouse - Teacher Pupil Ratio
--
-- Calculating Pupil Teacher Ratios requires having the Enrolment values on the same row as the
-- teacher numbers.
-- this view splits by sector and aggregates up to District

-- =============================================
CREATE VIEW [warehouse].[PupilTeacherRatioTable]
WITH VIEW_METADATA
AS

Select SurveyYear
, DistrictCode
, dName District
, PTR.AuthorityCode
, Authority
, AuthorityGroupCode AuthorityGovtCode
, AuthorityGroup     AuthorityGovt

, SchoolTypeCode
, stDescription SchoolType

, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(certQual) CertQual
, sum(Enrol) Enrol
from warehouse.PupilTeacherRatioSchool  PTR
	LEFT JOIN Districts D
		ON DistrictCode = D.dID
	LEFT JOIN SchoolTypes ST
		ON PTR.SchoolTypeCode = ST.stCode
	LEFT JOIN DimensionAuthority AA
		ON AA.AuthorityCode = PTR.AuthorityCode


GROUP BY SurveyYear
, DistrictCode
, dName
, PTR.AuthorityCode
, Authority
, AuthorityGroupCode
, AuthorityGroup

, SchoolTypeCode
, stDescription

, Sector
GO

