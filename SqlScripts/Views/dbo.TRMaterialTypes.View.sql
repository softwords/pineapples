SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRMaterialTypes]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END,codeDescription) AS codeDescription, codeSort

FROM         dbo.lkpMaterialTypes
GO
GRANT SELECT ON [dbo].[TRMaterialTypes] TO [public] AS [dbo]
GO

