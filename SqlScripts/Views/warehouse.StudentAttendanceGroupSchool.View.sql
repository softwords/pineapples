SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2020
-- Description:	Warehouse - Student Attendance Groups
--
-- A specialized view of attendance by groups teachers
-- with attendance groups
-- =============================================
CREATE VIEW [warehouse].[StudentAttendanceGroupSchool]
AS
SELECT [svyYear]
	  ,[stueSpEd]
      ,[stueCompleted]
      ,[stueOutcome]
      ,[stueOutcomeReason]
	  ,[School Name]
	  ,[District Code]
	  ,[District]
	  ,[Region Code]
	  ,[Region]
	  ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode]
	  ,[AuthorityGroup]
	  ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[Island Code]
	  ,[Island]
, sum(case when isnull(stueDaysAbsent,0) = 0 then Num end) 'None'
, sum (case when stueDaysAbsent between 1 and 5 then Num end) '1-5'
, sum (case when stueDaysAbsent between 6 and 10 then Num end) '6-10'
, sum (case when stueDaysAbsent between 11 and 15 then Num end) '11-15'
, sum (case when stueDaysAbsent > 20 then Num end) '>20'
FROM
(
Select [stueDaysAbsent]
      ,[svyYear]
      ,[stueSpEd]
      ,[stueCompleted]
      ,[stueOutcome]
      ,[stueOutcomeReason]
	  ,[School Name]
	  ,[District Code]
	  ,[District]
	  ,[Region Code]
	  ,[Region]
	  ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode]
	  ,[AuthorityGroup]
	  ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[Island Code]
	  ,[Island]
	  ,count(stuID) Num
from warehouse.StudentAttendanceSchool
group by [stueDaysAbsent]
      ,[svyYear]
      ,[stueSpEd]
      ,[stueCompleted]
      ,[stueOutcome]
      ,[stueOutcomeReason]
	  ,[School Name]
	  ,[District Code]
	  ,[District]
	  ,[Region Code]
	  ,[Region]
	  ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode]
	  ,[AuthorityGroup]
	  ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[Island Code]
	  ,[Island]
) SUB
group by [svyYear]
	  ,[stueSpEd]
      ,[stueCompleted]
      ,[stueOutcome]
      ,[stueOutcomeReason]
	  ,[School Name]
	  ,[District Code]
	  ,[District]
	  ,[Region Code]
	  ,[Region]
	  ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode]
	  ,[AuthorityGroup]
	  ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[Island Code]
	  ,[Island]
GO

