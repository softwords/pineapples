SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 23/08/2021
-- Description:	Warehouse - Teacher Pupil Ratio based on newer warehouse.TeacherFtptSchool
--
-- For backwards compatibility:
--   * FTPT is used to translate to NumTeachers (i.e. includes all teachers whether full-time (i.e. only assigned teaching grades)
--     or part-time (i.e. assigned teaching grades and other duties like admin)
--   * Add Sector column as before which is based on the mapping in lkpLevels (i.e. ilsCode -> secCode)
--
-- How this relates to warehouse.PupilTeacherRatioSchool:
--
-- warehouse.PupilTeacherRatioSchool is based on StaffCountSchool (TeacherCountSchool, etc.). The total numbers
-- by sector were whole numbers (no decimal) because the business rule was a teacher is assigned the sector
-- based on its highest grade taught. In warehouse.PupilTeacherRatioFtptSchool the total numbers by sector will be more precise but with decimal
-- This is because the new business rule is based on a weighted system as per UIS guidelines and is a net
-- improvement in data. If you were to compare totals across all sectors (or district, etc.) in a pivot table
-- warehouse.TeacherPupilRatioSchool with warehouse.TeacherPupilRatioFtptSchool they would all be the same!
-- For example, before you could have a school in a year with 1 teacher in ECE, 3 in Primary and 2 in Secondary.
-- While now you would have something like 1 teacher in ECE, 4.333 in Primary and 1.666 in Secondary.
-- The school still have a total of 6 teachers except now the distribution in sectors is more intelligent.

-- Calculating Pupil Teacher Ratios requires having the Enrolment values on the same row as the
-- teacher numbers. This view splits by sector and ISCED sub levels within school where applicable,
-- and allow aggregations up from school
--
-- History log:
--   * 23/08/2021, Ghislain Hachey, Created view
--   * 24/11/2021, Ghislain Hachey, Modified view to use [warehouse].[TeacherFtptSchoolDimension] to support all disaggregations
-- =============================================
CREATE VIEW [warehouse].[PupilTeacherRatioFtptSchool]
WITH VIEW_METADATA
AS

Select schNo
, SurveyYear
, Sector
, ISCEDSubClass

, District
, DistrictCode
, Island
, [Island Code]
, Region
, [Region Code]
, Authority
, AuthorityCode
, AuthorityType
, AuthorityTypeCode
, AuthorityGroup
, AuthorityGroupCode
, SchoolType
, SchoolTypeCode

, sum(FTPT) NumTeachers
, sum(FTPTC) Certified
, sum(FTPTQ) Qualified
, sum(FTPTQC) CertQual
, sum(Enrol) Enrol
FROM
(
Select schNo
, SurveyYear
, secCode Sector
, ISCEDSubClass

, District
, DistrictCode
, Island
, [Island Code]
, Region
, [Region Code]
, Authority
, AuthorityCode
, AuthorityType
, AuthorityTypeCode
, AuthorityGroup
, AuthorityGroupCode
, SchoolType
, SchoolTypeCode

, FTPT
, FTPTC
, FTPTQ
, FTPTQC
, convert(int, null) Enrol
From warehouse.TeacherFtptSchoolDimension TFS
LEFT JOIN
(
SELECT DISTINCT ilsCode, secCode FROM [dbo].[lkpLevels]
) L
ON TFS.ISCEDSubClass=L.ilsCode

-- IMPORTANT now excluding 'Admin' and 'Other' contributions to identify 'Academic' staff
-- or more precisely 'Teaching staff' only
WHERE ISCEDSubClass != 'A' AND ISCEDSubClass != 'X'

UNION ALL
	Select E.schNo
	, E.surveyYear
	, L.secCode Sector
	, L.ilsCode ISCEDSubClass

	, District
	, DistrictCode
	, Island
	, [Island Code]
	, Region
	, [Region Code]
	, Authority
	, AuthorityCode
	, AuthorityType
	, AuthorityTypeCode
	, AuthorityGroup
	, AuthorityGroupCode
	, SchoolType
	, SchoolTypeCode

	, null NumTeachers
	, null Certified
	, null Qualified
	, null CertQual
	, Enrol
	from warehouse.enrol E
		INNER JOIN lkpLevels L
			ON E.ClassLevel = L.codeCode
		INNER JOIN Warehouse.bestSurvey S
			ON S.schNo = E.schNo
			AND S.SurveyYear = E.surveyYear
		LEFT JOIN Warehouse.dimensionSchoolSurvey DSS
			ON Dss.[Survey ID] = S.surveyDimensionID
) U
GROUP BY
schNo
, SurveyYear
, Sector
, ISCEDSubClass

, District
, DistrictCode
, Island
, [Island Code]
, Region
, [Region Code]
, Authority
, AuthorityCode
, AuthorityType
, AuthorityTypeCode
, AuthorityGroup
, AuthorityGroupCode
, SchoolType
, SchoolTypeCode
GO

