SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- =============================================
CREATE VIEW [dbo].[Student]
AS
SELECT
	*
FROM
	dbo.Student_
	WHERE stuRedirect is null
GO

