SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Start].[tblTeachers]
AS
Select
'T' + replace(right('00000000000000000000' + convert(nvarchar(12), tID),12),'-','9') MoE_PF
, tGiven [First Name]
, tSurname [Family Name]
, tDOB     [DOB]
, tSex		[Gender]


From TeacherIdentity
GO

