SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[AccreditationPerformanceSchool]
AS

Select BI.SurveyYear
, BI.schNo
, DSS.[School Name]
, DSS.DistrictCode
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, AP.*
from warehouse.BestInspection BI
	INNER JOIN warehouse.AccreditationPerformance AP
		on BI.inspID = ap.inspID
	LEFT JOIN warehouse.dimensionSchoolSurvey DSS
		ON Bi.SurveyDimensionID = DSS.[Survey ID]
GO

