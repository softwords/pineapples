SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	District Inspections with result
--
-- =============================================
CREATE VIEW [warehouse].[InspectionAuthority]
WITH VIEW_METADATA
AS
Select SurveyYear
, InspectionTypeCode
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, InspectionResult
, sum(Num) Num
, sum(NumThisYear) NumThisYear

FROM warehouse.InspectionTable
GROUP BY SurveyYear
, InspectionTypeCode
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, InspectionResult
GO

