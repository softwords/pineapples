SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2019
-- Description:	Warehouse - Enrolment data by school with the whole set of possible disaggregations
--
-- This is a simple consoilidation of warehouse.enrol to group by school.
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation
--		warehouse.EnrolIsland
--		warehouse.EnrolRegion
--		warehouse.EnrolLocalElectorate

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolIslandR
--		warehouse.EnrolRegionR
--		warehouse.EnrolLocalElectorateR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW
[warehouse].[EnrolSchoolDimension]
AS
Select SurveyYear
, [School No]
, [School Name]
, [Local Electorate No] LocalElectorateNo
, [Local Electorate]
, [National Electorate No] NationalElectorateNo
, [National Electorate]
, [District Code] DistrictCode
, District
, [Island Code] IslandCode
, Island
, [Region Code] RegionCode
, Region
, AuthorityCode
, Authority
, AuthorityTypeCode
, AuthorityType
, AuthorityGroupCode
, AuthorityGroup
, SchoolTypeCode
, SchoolType

, ClassLevel
, Age
, GenderCode

, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA
, sum(Completed) Completed

from warehouse.Enrol E
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON E.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, [School No]
, [School Name]
, [Local Electorate No]
, [Local Electorate]
, [National Electorate No]
, [National Electorate]
, [District Code]
, District
, [Island Code]
, Island
, [Region Code]
, Region
, AuthorityCode
, Authority
, AuthorityCode
, Authority
, AuthorityTypeCode
, AuthorityType
, AuthorityGroupCode
, AuthorityGroup
, SchoolTypeCode
, SchoolType
, ClassLevel
, Age
, GenderCode
GO

