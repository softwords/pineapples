SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Warehouse - enrolment 'grid' by school
--
-- A standard presentation of enrolment data is a grid, with age in the rows, and
-- Level / Gender in the columns
-- To assist banded report writers to produce this layout it is convenient to
-- denormalise the enrolment data so that all levels x gender appear on a single row
--
-- This versatile view produces this layout, and includes the subtotals by level by age, by gender by age, and by age
-- this view is by School.
-- see also
-- warehouse.EnrolGridDistrict
-- warehouse.EnrolGridAuthority
-- warehouse.EnrolGridSchoolType
-- for higher level aggregations
-- Note that this query produces columns by Year of Education
-- In systems hwere it is important to separate Class Levels that have the same year of ed,
-- you can use the stored proc warehouse.GridMaker
-- =============================================
CREATE VIEW [warehouse].[EnrolGrid]
AS
Select
EE.schNo
, EE.SurveyYear
, ee.Age Age
, DSS.[District Code] DistrictCode
, DSS.AuthorityCode
, DSS.SchoolTypeCode
, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = -2 then enrol end) [Y-2F]
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = -2 then enrol end) [Y-2M]
, SUM(CASE WHEN L.lvlYear = -2 then enrol end) [Y-2]


, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = -1 then enrol end) [Y-1F]
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = -1 then enrol end) [Y-1M]
, SUM(CASE WHEN L.lvlYear = -1 then enrol end) [Y-1]


, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 0 then enrol end) Y00F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 0 then enrol end) Y00M
, SUM(CASE WHEN L.lvlYear = 0 then enrol end) Y00

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 1 then enrol end) Y01F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 1 then enrol end) Y01M
, SUM(CASE WHEN L.lvlYear = 1 then enrol end) Y01

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 2 then enrol end) Y02F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 2 then enrol end) Y02M
, SUM(CASE WHEN L.lvlYear = 2 then enrol end) Y02

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 3 then enrol end) Y03F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 3 then enrol end) Y03M
, SUM(CASE WHEN L.lvlYear = 3 then enrol end) Y03

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 4 then enrol end) Y04F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 4 then enrol end) Y04M
, SUM(CASE WHEN L.lvlYear = 4 then enrol end) Y04

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 5 then enrol end) Y05F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 5 then enrol end) Y05M
, SUM(CASE WHEN L.lvlYear = 5 then enrol end) Y05

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 6 then enrol end) Y06F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 6 then enrol end) Y06M
, SUM(CASE WHEN L.lvlYear = 6 then enrol end) Y06

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 7 then enrol end) Y07F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 7 then enrol end) Y07M
, SUM(CASE WHEN L.lvlYear = 7 then enrol end) Y07

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 8 then enrol end) Y08F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 8 then enrol end) Y08M
, SUM(CASE WHEN L.lvlYear = 8 then enrol end) Y08

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 9 then enrol end) Y09F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 9 then enrol end) Y09M
, SUM(CASE WHEN L.lvlYear = 9 then enrol end) Y09

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 10 then enrol end) Y10F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 10 then enrol end) Y10M
, SUM(CASE WHEN L.lvlYear = 10 then enrol end) Y10

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 11 then enrol end) Y11F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 11 then enrol end) Y11M
, SUM(CASE WHEN L.lvlYear = 11 then enrol end) Y11

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 12 then enrol end) Y12F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 12 then enrol end) Y12M
, SUM(CASE WHEN L.lvlYear = 12 then enrol end) Y12

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 13 then enrol end) Y13F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 13 then enrol end) Y13M
, SUM(CASE WHEN L.lvlYear = 13 then enrol end) Y13

, SUM(CASE WHEN GenderCode = 'F' and L.lvlYear = 14 then enrol end) Y14F
, SUM(CASE WHEN GenderCode = 'M' and L.lvlYear = 14 then enrol end) Y14M
, SUM(CASE WHEN L.lvlYear = 14 then enrol end) Y14

, SUM(CASE WHEN GenderCode = 'F' then enrol end) TotF
, SUM(CASE WHEN GenderCode = 'M' then enrol end) TotM

, SUM(enrol) Tot
, SUM(case when Estimate = 1 then enrol end) Est

from warehouse.enrol EE
 INNER JOIN lkpLevels L
	ON EE.ClassLevel = L.codeCode
 INNER JOIN warehouse.DimensionSchoolSurvey DSS
	ON EE.SurveyDimensionID = DSS.[Survey ID]
GROUP BY EE.schNo, EE.surveyYear, EE.Age
, DSS.[District Code], DSS.AuthorityCode, DSS.SchoolTypeCode
GO

