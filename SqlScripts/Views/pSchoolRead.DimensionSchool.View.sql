SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSchoolRead].[DimensionSchool]
As
	Select schNo
		, schName
		, IName
		, dName
		, schType

	FROM Schools
		INNER JOIN Islands
			ON Schools.iCode = Islands.iCode
		INNER JOIN Districts
			ON Islands.iGRoup = Districts.dID
GO

