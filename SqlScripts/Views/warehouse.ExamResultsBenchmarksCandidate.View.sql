SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- View
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 08/04/2022
-- Description:	Warehouse - Exam Results (benchmark) at the candidate level (normalized version)
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsBenchmarksCandidate]
AS
Select excID
, studentID
, excGiven
, excMiddleNames
, excFamilyName
, examID
, examCode
, examYear
, examName
, schNo
, Gender
, [Key] [benchmarkCode]
, Description [benchmarkDesc]
, achievementLevel
, achievementDesc
, sum(candidateCount) CandidateCount
FROM warehouse.ExamCandidateResultsTyped
WHERE RecordType = 'Benchmark'
GROUP BY excID
, studentID
, excGiven
, excMiddleNames
, excFamilyName
, examID
, examCode
, examYear
, examName
, schNo
, Gender
, [Key]
, Description
, achievementLevel
, achievementDesc
GO

