SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ddDistrict]
AS
SELECT     TOP 100 PERCENT District, SortOrder
FROM         (SELECT     '(all)' AS District, '' AS SortOrder
                       FROM          dbo.metaNumbers
                       WHERE      (num = 0)
                       UNION
                       SELECT     dName, dName AS SortOrder
                       FROM         dbo.Districts) AS L
ORDER BY SortOrder
GO
GRANT DELETE ON [dbo].[ddDistrict] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ddDistrict] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ddDistrict] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ddDistrict] TO [public] AS [dbo]
GO

