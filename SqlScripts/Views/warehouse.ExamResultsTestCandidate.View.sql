SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 08/04/2022
-- Description:	Warehouse - Exam Results (whole test) at the candidate level (normalized version)
--
-- History Log:
--  * DD/MM/YYYY, Author, Short description of change
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsTestCandidate]
AS
Select excID
, studentID
, excGiven
, excMiddleNames
, excFamilyName
, examID
, examCode
, examYear
, examName
, schNo
, Gender
, achievementLevel
, achievementDesc
, sum(candidateCount) CandidateCount
FROM warehouse.ExamCandidateResultsTyped
WHERE RecordType = 'Exam'
GROUP BY excID
, studentID
, excGiven
, excMiddleNames
, excFamilyName
, examID
, examCode
, examYear
, examName
, schNo
, Gender
, achievementLevel
, achievementDesc
GO

