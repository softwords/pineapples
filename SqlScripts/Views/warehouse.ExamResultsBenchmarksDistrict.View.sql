SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/04/2022
-- Description:	Warehouse - Exam Results (Benchmarks) aggregated at the districts level (normalized versioon)
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsBenchmarksDistrict]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, Gender
, [Key] [benchmarkCode]
, Description [benchmarkDesc]
, achievementLevel
, achievementDesc
, sum(candidateCount) Candidates
FROM warehouse.ExamSchoolResultsTyped ESRT
WHERE RecordType = 'Benchmark'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, Gender
, [Key]
, Description
, achievementLevel
, achievementDesc
GO

