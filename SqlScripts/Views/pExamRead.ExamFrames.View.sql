SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pExamRead].[ExamFrames]
AS

Select RowType + convert(nvarchar(10), rowID) ID
, * from
(
Select 'S' RowType
, exstdID RowID
, ExID ExamID
, exstdCode Code
, exstdDescription Description
, pCreateUser
, pCreateDateTime
, pEditUser
, pEditDateTime
, pRowversion
from ExamStandards
UNION ALL
Select 'B' Rtype
, exbnchID
, exID
, exbnchCode
, exbnchDescription
, pCreateUser
, pCreateDateTime
, pEditUser
, pEditDateTime
, pRowversion
FROM ExamBenchmarks
UNION ALL
Select 'I' Rtype
, exindID
, exID
, exindCode
, exindDescription
, pCreateUser
, pCreateDateTime
, pEditUser
, pEditDateTime
, pRowversion
FROM ExamIndicators
) SUB
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [pExamRead].[UpdateExamFrame]
   ON  [pExamRead].[ExamFrames]
   INSTEAD OF UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
-- update standards
UPDATE ExamStandards
SET exstdDescription = Description
, pEditUser = I.pEditUser
, pEditDateTime = I.pEditDateTime
FROM ExamStandards 
INNER JOIN INSERTED I
ON ExamStandards.exstdID = I.rowID
WHERE I.RowType = 'S'

UPDATE ExamBenchmarks
SET exbnchDescription = Description
, pEditUser = I.pEditUser
, pEditDateTime = I.pEditDateTime
FROM ExamBenchmarks
INNER JOIN INSERTED I
ON ExamBenchmarks.exbnchID = I.rowID
WHERE I.RowType = 'B'

UPDATE ExamIndicators
SET exindDescription = Description
, pEditUser = I.pEditUser
, pEditDateTime = I.pEditDateTime
FROM ExamIndicators
INNER JOIN INSERTED I
ON ExamIndicators.exindID = I.rowID
WHERE I.RowType = 'I'

END
GO

