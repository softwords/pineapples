SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description: denormalised 'convenience' view of StudentEnrolments
-- Change History:
--			2022 09 30 Include lvlYear, edLevelCode
-- =============================================
CREATE VIEW [dbo].[StudentEnrolment]
WITH VIEW_METADATA
AS
Select STUE.*
, STU.stuCardID
      ,stuNamePrefix
      ,stuGiven
      ,stuMiddleNames
      ,stuFamilyName
      ,stuNameSuffix
      ,stuGivenSoundex
      ,stuFamilySoundex
      ,stuDoB
      ,stuDoBEst
      ,stuGender
      ,stuEthnicity
, SS.ssID
, SVY.svyCensusDate
, case when svyCensusDate is null then STUE.stueYear - year(stuDob)
	else common.AgeAt(stuDoB, svyCensusDate) end Age
, LVL.[Year of Education] lvlYear
, LVL.edLevelCode
, LVL.[ISCED SubClass] ilsCode
FROM
StudentEnrolment_ STUE
	INNER JOIN Student_ STU
		ON STUE.stuID = STU.stuID
	LEFT JOIN SchoolSurvey SS
		ON STUE.stueYear = SS.svyYear
		AND STUE.schNo = SS.schNo
	LEFT JOIN Survey SVY
		ON SS.svyYEar = SVY.svyYear
	LEFT JOIN DimensionLevel LVL
		ON STUE.stueClass = LVL.levelCode
GO

