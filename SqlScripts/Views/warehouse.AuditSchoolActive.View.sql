SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: April 2021
-- Description:
-- A school should have enrolment data
-- if and only if it is active in that year
-- =============================================
CREATE VIEW [warehouse].[AuditSchoolActive]
AS
Select TOP 100 PERCENT U.*
, schEst, schClosed
, case when SVY.svyYear is null then 0 else 1 end SurveyYearDefined
, SurveyCount.NumSurveys
FROM
(
select S.schNo, S.SurveyYear, S.Active
, E.Enrol
from warehouse.SchoolActive S
left join warehouse.enrolSchoolR E
ON S.schNo = E.schNo
AND S.surveyYear = E.surveyYEar
WHERE Active = 'A'
and E.schNo is null
UNION ALL

select E.schNo, E.SurveyYear, S.Active
, E.Enrol
from warehouse.enrolSchoolR E
left join warehouse.SchoolActive S
ON S.schNo = E.schNo
AND S.Active = 'A'
AND S.surveyYear = E.surveyYEar
WHERE S.schNo is null
) U
LEFT JOIN Survey SVY
	ON U.SurveyYear = SVY.svyYear
LEFT JOIN (
	select svyYear, count(*) NumSurveys from SchoolSurvey
	GROUP BY svyYear
) SurveyCount
	ON U.SurveyYear = SurveyCount.svyYear
LEFT JOIN Schools S
	ON U.schNo = S.schNo

ORDER by surveyYear, schNo
GO

