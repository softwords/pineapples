SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRISTCourseGRoup] AS
SELECT istgCode,
isnull(CASE dbo.userLanguage()
    WHEN 0 THEN istgDescription
    WHEN 1 THEN istgDescriptionL1
    WHEN 2 THEN istgDescriptionL2
 END,istgDescription) AS istgDescription
 FROM dbo.ISTCourseGRoup
GO
GRANT SELECT ON [dbo].[TRISTCourseGRoup] TO [public] AS [dbo]
GO

