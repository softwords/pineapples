SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 24/08/2021
-- Description:	Warehouse - Teacher Pupil Ratio aggregated up to Nation
-- Based on warehouse.PupilTeacherRatioFtptSchool which is based on warehouse.TeacherFtptSchool (FTPT values)
-- Related to warehouse.PupilTeacherRatioNation and variants
-- =============================================
CREATE VIEW [warehouse].[PupilTeacherRatioFtptNation]
WITH VIEW_METADATA
AS

Select SurveyYear
, Sector
, ISCEDSubClass
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(certQual) CertQual
, sum(Enrol) Enrol
from warehouse.PupilTeacherRatioFtptSchool
GROUP BY SurveyYear
, Sector
, ISCEDSubClass
GO

