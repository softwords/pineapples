SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs by region
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobIslandR]
WITH VIEW_METADATA
AS
Select SurveyYear
, JobTitle
, StaffType
, TeacherType
, [Island Code] IslandCode
, Island
, [Region Code] RegionCode
, Region
, [District Code] DistrictCode
, District

, sum(NumStaff) NumStaff
, sum(case when GenderCode = 'M' then NumStaff else null end) NumStaffM
, sum(case when GenderCode = 'F' then NumStaff else null end) NumStaffF

FROM warehouse.TeacherJobSchool S
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON S.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, JobTitle
, StaffType
, TeacherType
, [Island Code]
, Island
, [Region Code]
, Region
, [District Code]
, District
GO

