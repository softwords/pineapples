SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 31 3 2019
-- Description:	Return details of exams for schools
--              Uses a union to include both "formal" exams,
--              and "standard test of achievement"
-- =============================================
CREATE VIEW [pExamRead].[SchoolExams]
WITH VIEW_METADATA
AS
-- a 'formal exam' has records in exam scores - this is the ancient approach for interface to Atlas
SELECT *
, case when isnull(Candidates,0) = 0 then null else convert(decimal(7, 2), Scores / Candidates) end AvgScore
, case when isnull(CandidatesM,0) = 0 then null else convert(decimal(7, 2), ScoresM / CandidatesM) end AvgScoreM
, case when isnull(CandidatesF,0) = 0 then null else convert(decimal(7, 2), ScoresF / CandidatesF) end AvgScoreF
FROM
(
Select DISTINCT
EXS.schNo
, schName
, X.exID
, X.exCode	ExamCode
, X.exYear	ExamYear
, sum(EXS.exsCandidates) Candidates
, sum(case when exsGender = 'M' then exsCandidates end) CandidatesM
, sum(case when exsGender = 'F' then exsCandidates end) CandidatesF
, sum(EXS.exsScore) Scores
, sum(case when exsGender = 'M' then exsScore end) ScoresM
, sum(case when exsGender = 'F' then exsScore end) ScoresF

FROM Exams X
	INNER JOIN ExamScores EXS
		ON X.exID = EXS.exID
	INNER JOIN Schools S
		ON EXS.schNo = S.schNo
GROUP BY EXS.schNo
, S.schName
, X.exID
, X.exCode
, X.exYear
) SUB

UNION
-- STA has records in examCandidateResults - this is supported by both exam uploads, including 2021 Soe upload
SELECT *
, case when isnull(Candidates,0) = 0 then null else convert(decimal(7, 2), Scores / Candidates) end AvgScore
, case when isnull(CandidatesM,0) = 0 then null else convert(decimal(7, 2), ScoresM / CandidatesM) end AvgScoreM
, case when isnull(CandidatesF,0) = 0 then null else convert(decimal(7, 2), ScoresF / CandidatesF) end AvgScoreF
FROM
(
Select
EXS.schNo
, schName
, X.exID
, X.exCode	+ ': ' + XT.exName ExamCode
, X.exYear	ExamYear
, sum(Candidates) Candidates
, sum(case when Gender = 'M' then Candidates end) CandidatesM
, sum(case when Gender = 'F' then Candidates end) CandidatesF
, null Scores
, null ScoresM
, null ScoresF

FROM Exams X
	INNER JOIN warehouse.ExamSchoolResultsX EXS
		ON X.exID = EXS.examID
	INNER JOIN lkpExamTypes XT
		ON X.exCode = XT.exCode
	INNER JOIN Schools S
		ON EXS.schNo = S.schNo
GROUP BY EXS.schNo
, S.schName
, X.exID
, X.exCode
, XT.exName
, X.exYear
) SUB
GO

