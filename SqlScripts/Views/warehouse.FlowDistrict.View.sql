SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	District Flow
--
-- Wrapper around warehouse.CohortDistrict to include flow calculations
-- ie PromoteRate, RepeatRate, SurvivalRate (single year survival aka "transition rate")
-- This version is intended to be used in reports, use districtCohort for "cubes"
-- (pivot tables, tableau) where ratios should be calculated based on current aggregations.
-- TRANSFER versions:
-- field named xxxxTR calculate the same ratios but attempt to take into account internal transfers
-- These should be used with caution as internal transfer data may not be very reliable
-- All rates are shown as ratios ( ie 0 to 1) rather than percentages.
-- =============================================
CREATE VIEW [warehouse].[FlowDistrict]
WITH VIEW_METADATA
AS

Select S.*
, round(1 - RepeatRate - PromoteRate,3) DropoutRate
, round(1 - RepeatRateTR - PromoteRateTR,3) DropoutRateTR
, case when RepeatRate = 1 then null else round(PromoteRate / (1 - RepeatRate),3) end SurvivalRate
, case when RepeatRateTR = 1 then null else round(PromoteRateTR / (1 - RepeatRateTR),3) end SurvivalRateTR
FROM
(
Select C.*
	, case when Enrol is null then null
		when Enrol = 0 then null
		else
			round( convert(float,isnull(RepNY,0)) /Enrol , 3)
		end RepeatRate
	, case when Enrol is null then null
		when (Enrol - isnull(TroutNY,0)) = 0 then null
		else
			round(convert(float,isnull(RepNY,0)) / (Enrol - isnull(TroutNY,0)) , 3)
		end RepeatRateTR
	, case when Enrol is null then null
		when Enrol = 0 then null
		else
			round(( convert(float,EnrolNYNextLevel) - isnull(RepNYNextLevel,0))  / Enrol ,3)
		end PromoteRate
	, case when Enrol is null then null
		when ( Enrol - isnull(TroutNY,0)) = 0 then null
		else
			round(( convert(float,EnrolNYNextLevel) - isnull(RepNYNextLevel,0) - isnull(TrinNYNextLevel,0))  / ( Enrol - isnull(TroutNY,0)) ,3)
		end PromoteRateTR
	from warehouse.CohortDistrict C

) S
GO

