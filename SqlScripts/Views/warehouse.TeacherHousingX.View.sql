SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 29/03/2024
-- Description:	Warehouse - Extended version of TeacherHousing containing all dimensions (i.e. possible disaggregations)
--
-- History log:
--   * 29/03/2024, Ghislain Hachey, Created view
-- =============================================
CREATE VIEW
[warehouse].[TeacherHousingX]
AS
SELECT CAST([Survey Data Year] AS INT) [SurveyYear]
	-- COALESCE to handle seemingly dame data put in different fields (depending on the country)
	--, COALESCE(SS.ssTotalClasses, SS.ssNumClassrooms) TotalClasses
	--, COALESCE(SS.ssTotalPupils, SS.ssEnrol) TotalPupils
	, COUNT(TS.tID) TotalTeachersReal
	, COALESCE(SS.ssTotalTeachers, SS.ssNumTeachers) TotalTeachers
	, ISNULL(SS.ssTchHouseOnSite,0) + ISNULL(SS.ssTchHouseOffSite,0) NumTeachersHouseProvidedTotal
	, SS.ssTchHouseOnSite NumTeachersHouseProvidedOnSite
	, SS.ssTchHouseOffSite NumTeachersHouseProvidedOffSite
	, SS.ssTchHouseM NumTeachersWithHouseRequiringMaintenance
	, SS.ssTchHouseN NumTeachersInHousingNotOfficiallyProvided
	, 5 TotalPossibleData
	, (
        CASE WHEN SS.ssNumTeachers IS NOT NULL THEN 1 ELSE 0 END +
        CASE WHEN SS.ssTchHouseOnSite IS NOT NULL THEN 1 ELSE 0 END +
        CASE WHEN SS.ssTchHouseOffSite IS NOT NULL THEN 1 ELSE 0 END +
		CASE WHEN SS.ssTchHouseM IS NOT NULL THEN 1 ELSE 0 END +
		CASE WHEN SS.ssTchHouseN IS NOT NULL THEN 1 ELSE 0 END
    ) AS TotalSubmittedData
	, [Survey ID]
	, [Survey Data Year]
	, [School No]
	, [School Name]
	, [District Code]
	, [District Code] DistrictCode
	, District
	, [District Short]
	, [Island Code]
	, Island
	, [Region Code]
	, Region
	, Size
	, [Local Electorate No]
	, [Local Electorate]
	, [National Electorate No]
	, [National Electorate]
	, DSS.AuthorityCode
	, Authority
	, AuthorityTypeCode
	, AuthorityType
	, AuthorityGroupCode
	, AuthorityGroup
	, LanguageCode
	, Language
	, LanguageGroupCode
	, LanguageGroup
	, CurrentSchoolTypeCode
	, [Current School Type]
	, DSS.SchoolTypeCode
	, SchoolType
	, [School Class]
	, [Year Closed]
FROM [dbo].[SchoolSurvey] SS
JOIN DimensionSchoolSurvey DSS ON SS.ssID = DSS.[Survey ID]
LEFT OUTER JOIN TeacherSurvey TS ON SS.ssID = TS.ssID
GROUP BY [Survey Data Year]
	, SS.ssTotalTeachers
	, SS.ssNumTeachers
	, SS.ssTchHouseOnSite
	, SS.ssTchHouseOffSite
	, SS.ssTchHouseM
	, SS.ssTchHouseN
	, [Survey ID]
	, [Survey Data Year]
	, [School No]
	, [School Name]
	, [District Code]
	, District
	, [District Short]
	, [Island Code]
	, Island
	, [Region Code]
	, Region
	, Size
	, [Local Electorate No]
	, [Local Electorate]
	, [National Electorate No]
	, [National Electorate]
	, DSS.AuthorityCode
	, Authority
	, AuthorityTypeCode
	, AuthorityType
	, AuthorityGroupCode
	, AuthorityGroup
	, LanguageCode
	, Language
	, LanguageGroupCode
	, LanguageGroup
	, CurrentSchoolTypeCode
	, [Current School Type]
	, DSS.SchoolTypeCode
	, SchoolType
	, [School Class]
	, [Year Closed]
GO

