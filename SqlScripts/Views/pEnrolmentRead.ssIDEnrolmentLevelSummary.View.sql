SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pEnrolmentRead].[ssIDEnrolmentLevelSummary]
AS
Select subQ.ssID,
sum(LevelCounter) as NumLevels,
right(max(lvlString),len(max(lvlString))-5) as HighestLevel,
right(min(lvlString),len(min(lvlString))-5) as LowestLevel,
max(lvlYear) as HighestYearOfEd,
min(lvlYear) as LowestYearOfEd,
max(Enrol) as MaxLevelEnrol,
right(max(EnrolString),len(max(EnrolString))-5)  as LevelOfMaxEnrol,
max(lvlYear) - min(lvlYear)+1 as RangeYearOfEd,
case when sum(LevelCounter) = numSchoolTypeLevels
	then 'Y'
	else 'N'
end FullRangeYN,
case when sum(LevelCounter) = numSchoolTypeLevels
	then 1
	else 0
end FullRange ,
case when sum(LevelCounter) = max(lvlYear) - min(lvlYear)+1
	then 'Y'
	else 'N'
end ContinuousRangeYN,
case when sum(LevelCounter) = max(lvlYear) - min(lvlYear)+1
	then 1
	else 0
end ContinuousRange

from
(SELECT EL.ssID,
	lvlYear,
	EL.LevelCode,
	case when Enrol <> 0
		then right('00000'+ convert(varchar(5),lvlYear),5)+ EL.LevelCode
		else null end  LvlString,

	case when Enrol <> 0 then 1 else 0 end LevelCounter,
	EL.Enrol,
	case when Enrol <> 0
		then right('00000'+ convert(varchar(5),EL.Enrol),5)+ EL.LevelCode
		else null end  EnrolString

FROM pEnrolmentRead.ssIDEnrolmentLevel EL
	INNER JOIN lkpLevels ON EL.LevelCode = lkpLevels.codeCode
) subQ
INNER JOIN
(Select ssID, ssSchType, count(tlmLevel)  numSchoolTypeLevels
	from metaSchoolTypelevelMap LM
	inner join SchoolSurvey SS
	on LM.stCode = SS.ssSchtype
	inner join lkpLevels L
	on LM.tlmLevel = L.codeCode
	group by ssID, ssSchType
) subQ2 on subQ.ssID = subQ2.ssID

GROUP BY subQ.ssID, subQ2.numSchoolTypeLevels
GO

