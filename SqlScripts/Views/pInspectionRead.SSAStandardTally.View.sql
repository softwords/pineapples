SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 July 2019
-- Description:	For an inspection in the format of a
-- 'SchoolStandardAssessment' (ie CategoryType/Standard/Criteria/SubCriteria hierarchy)
-- return the number of criteria reciving each score between 0 and 6
-- This query is calculated directly from the Xml on SchoolInspection
-- This allows these totals to be displayed in
--pInspectionRead.SchoolAccreditation, making this backward compatible with the legacy version
-- Since in general, there may be a variable MaxScore for Criteria,
-- As well, we can tally the number of criteria at each Level of achievement
-- =============================================
CREATE VIEW [pInspectionRead].[SSAStandardTally]
WITH VIEW_METADATA
AS
SELECT inspID
, sum(case when score = 0 then 1 end) ST0
, sum(case when score = 1 then 1 end) ST1
, sum(case when score = 2 then 1 end) ST2
, sum(case when score = 3 then 1 end) ST3
, sum(case when score = 4 then 1 end) ST4
, sum(case when score = 5 then 1 end) ST5
, sum(case when score = 6 then 1 end) ST6
, count(score) ST
, sum(case when Level = 'Level 0' then 1 end) Level0
, sum(case when Level = 'Level 1' then 1 end) Level1
, sum(case when Level = 'Level 2' then 1 end) Level2
, sum(case when Level = 'Level 3' then 1 end) Level3
, sum(case when Level = 'Level 4' then 1 end) Level4
FROM pInspectionRead.SSAStandard
group by inspID
GO

