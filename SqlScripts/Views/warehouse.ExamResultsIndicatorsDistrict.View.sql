SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 06/04/2022
-- Description:	Warehouse - Exam Results (Indicators) at the district level (normalized version)
--
-- Refer to documentation in View [warehouse].[ExamResultsTestSchoolX]
-- =============================================
CREATE VIEW [warehouse].[ExamResultsIndicatorsDistrict]
AS
Select examID
, examCode
, examYear
, examName
, DistrictCode
, District
, Gender
, [Key] [indicatorCode]
, Description [indicatorDesc]
, achievementLevel
, achievementDesc
, sum(candidateCount) Candidates
FROM warehouse.ExamSchoolResultsTyped ESRT
WHERE RecordType = 'Indicator'
GROUP BY examID
, examCode
, examYear
, examName
, DistrictCode
, District
, Gender
, [Key]
, Description
, achievementLevel
, achievementDesc
GO

