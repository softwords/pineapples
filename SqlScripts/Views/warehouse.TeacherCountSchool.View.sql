SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Teacher Counts
--
-- Teacher counts aggregated by district and sector.
-- Derived from warehouse.schoolTeacherCount table.
-- =============================================
CREATE VIEW [warehouse].[TeacherCountSchool]
AS
Select SchNo
, SurveyYear
, GenderCode
, AgeGroup
, DistrictCode
, S.AuthorityCode
, AU.AuthorityGroupCode AuthorityGovtCode
, SchoolTypeCode
, Sector
, ISCEDSubClass
-- UPDATE: 13 12 2020
-- this logic assumes that the column 'Support' is a text field in SchoolStaffCount, derived from TAMX on
-- warehouse.teacherlocation. ie this may be reflect some 'corrections' to inconsistent data recorded in the
-- workbook
-- and that records are now disaggregated between 'A' 'X' and (null) values - ie teaching staff Support is null when TAMX is T or M
-- warehouse.schoolTeacherCount is a VIEW that 'denormalises' support numbers in the same way as this view
-- Certified, Qualified Certqual do NOT include non-teaching staff
-- see Issue #518, #1003, #600

, sum(case when Support is not null then NumStaff else 0 end) NumSupportStaff
, sum(case when Support is null then NumStaff else null end) NumTeachers
, sum(case when Support is null then Certified else null end) Certified
, sum(case when Support is null then Qualified else null end) Qualified
, sum(case when Support is null then CertQual else null end) CertQual

--- TEMP FIX to CRITICAL FAILURE ---
--- REMOIVE THIS AFTER DEPLOYMENT OF FIX TO ISSUE 600 ---
---------------------------------------------------------
, null Support
---------------------------------------------------------

from warehouse.schoolStaffCount S
	LEFT JOIN DimensionAuthority AU
		 ON S.AuthorityCode = AU.AuthorityCode
GROUP BY SchNo
, SurveyYear
, GenderCode
, AgeGroup
, DistrictCode
, S.AuthorityCode
, AU.AuthorityGroupCode
, SchoolTypeCode
, Sector
, ISCEDSubClass
GO

