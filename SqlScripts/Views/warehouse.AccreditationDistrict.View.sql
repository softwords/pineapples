SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	District Inspections with result
--
-- =============================================
CREATE VIEW [warehouse].[AccreditationDistrict]
WITH VIEW_METADATA
AS
Select SurveyYear
, DistrictCode
, District
, InspectionResult
, sum(Num) Num
, sum(NumThisYear) NumThisYear

FROM warehouse.InspectionTable
WHERE InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GROUP BY SurveyYear
, DistrictCode
, District
, InspectionResult
GO

