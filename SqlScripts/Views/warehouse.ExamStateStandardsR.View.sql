SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: May 2020
-- Description:	Warehouse - Exams School standards
--
-- This is the 'R' version of [warehouse].[ExamStateStandards]
-- =============================================
CREATE VIEW [warehouse].[ExamStateStandardsR]
AS
SELECT [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[StateID]
      ,[State]
      ,[standardID]
      ,[standardCode]
      ,[standardDesc]
      ,[achievementLevel]
      ,[achievementDesc]
	  ,sum(case when [Gender] = 'M' then Candidates end) CandidatesM
	  ,sum(case when [Gender] = 'F' then Candidates end) CandidatesF
	  ,sum(Candidates) CandidatesT
  FROM [warehouse].[ExamStateStandards]
  GROUP BY
	   [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[StateID]
      ,[State]
      ,[standardID]
      ,[standardCode]
      ,[standardDesc]
      ,[achievementLevel]
      ,[achievementDesc]
GO

