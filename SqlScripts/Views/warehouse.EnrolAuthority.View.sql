SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2019
-- Description:	Warehouse - Enrolment data by Authority
--
-- This is a simple consolidation of warehouse.enrol to group by Authority
-- since AuthorityType and AuthorityGroup are determined by Authority, these are included too
-- to facilitate groupings and subtotalling
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation
--		warehouse.EnrolAuthority

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--      warehouse.EnrolAuthorityR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW
[warehouse].[EnrolAuthority]
AS
Select SurveyYear
, E.AuthorityCode
, Authority
, AuthorityTypeCode
, Authoritytype
, AuthorityGroupCode
, AuthorityGroup

, ClassLevel
, Age
, GenderCode

, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA
, sum(Completed) Completed

from warehouse.tableEnrol E
	LEFT JOIN DimensionAuthority DA
		ON E.AuthorityCode = DA.AuthorityCode
GROUP BY
SurveyYear
, E.AuthorityCode
, Authority
, AuthorityTypeCode
, Authoritytype
, AuthorityGroupCode
, AuthorityGroup
, ClassLevel
, Age
, GenderCode
GO

