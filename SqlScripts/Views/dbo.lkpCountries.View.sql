SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2020
-- Description:	Countries
--
-- lkpNationality is a table used for lookup of nationality
-- this is now (late 2020) standardised on ISO 2-char country codes
-- however, a number of 3-char codes are used to provide broader groupings
-- (e.g. 'Other European') for histoical compatibility, and in the interests
-- of simpler dropdowns. Such 3 char codes are in the ranges set asied for presonal use in
-- the ISO Standard 3166 (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
-- lkpCountries is just the Nationality list without these 3 char codes; ie it is ISO3166-2
-- this is more usable for addresses.
-- =============================================
CREATE VIEW lkpCountries
WITH VIEW_METADATA
AS

Select * from lkpNationality
WHERE len(codeCode) = 2
GO

