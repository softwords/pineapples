SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ddElectN]
AS
SELECT     TOP 100 PERCENT ElectN, SortOrder
FROM         (SELECT     '(all)' AS ElectN, '' AS SortOrder
                       FROM          dbo.metaNumbers
                       WHERE      (num = 0)
                       UNION
                       SELECT     codeDescription, codeSort AS SortOrder
                       FROM         dbo.lkpElectorateN) AS L
ORDER BY SortOrder
GO
GRANT DELETE ON [dbo].[ddElectN] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ddElectN] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ddElectN] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ddElectN] TO [public] AS [dbo]
GO

