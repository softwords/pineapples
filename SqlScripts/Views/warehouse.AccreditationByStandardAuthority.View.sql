SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 21 August 2019
-- Description:	Aggregation of Level by Standard
-- ==================================================
-- Reports on the accredition level (according to SSA partition)
-- achieved by school for each School_Evaluation standard
-- Moved to specific views from construction of this SQL in the Web Portal data layer
-- to allow such views to be easily available to report writers

--
-- Related views:
-- AccreditationByStandardTable
-- AccreditationByStandardSchool
-- AccreditationByStandardDistrict
-- AccreditationByStandardAuthority
-- AccreditationByStandardSchoolType
-- AccreditationByStandardNation

--
-- ==================================================
CREATE VIEW [warehouse].[AccreditationByStandardAuthority]
WITH VIEW_METADATA
AS
Select SurveyYear
--, DistrictCode, District
, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
--, SchoolTypeCode, SchoolType
, Standard
, Result
, sum(NumSchools) Num
, sum(NumSchoolsInYear) NumInYear

FROM
  warehouse.AccreditationByStandardSchool
GROUP BY SurveyYear
--, DistrictCode, District
, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
--, SchoolTypeCode, SchoolType

	, Standard, Result
GO

