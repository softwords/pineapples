SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 21 August 2019
-- Description:	Full drill-down into Accreditations scoring
-- ==================================================
-- This view is designed for pivot table use to provide maximum
-- analysis flexibiity.
-- Supports
--  * Disaggregation by school or key school attributes
--  * Calculation of percentages and 'nominal' Level at standard and criteria level
--  * analysis of results by subcriteria
--  * Disaggregation by overall inpsection result
--  * time series
--
-- History Log:
--   * Ghislain Hachey, 28 September 2021: add StandardName, CriteriaName, SubCriteriaName for easy access by end users
-- ==================================================
CREATE VIEW [warehouse].[AccreditationPivot]
AS
Select SurveyYear
, [School No], [School Name]
, DistrictCode, District
, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
, SchoolTypeCode, SchoolType
, DSS.[National Electorate No], DSS.[National Electorate]
, DSS.[Local Electorate No], DSS.[Local Electorate]
, DSS.[Region Code], DSS.Region
, BI.inspID InspectionID
, BI.InspectionResult
, BI.InspectionYear
, case when BI.InspectionYear = BI.SurveyYear then 1 else 0 end InYear
, Standard
, StandardName
, Criteria
, CriteriaName
, SubCriteria
, SubCriteriaName
, SubCriteriaQuestion
, sum(D.Score) Score
, sum(D.MaxScore) MaxScore
, count(DISTINCT D.inspID) NumSchools

, sum(case when BI.InspectionYear = BI.SurveyYear then D.Score else null end) ScoreInYear
, sum(case when BI.InspectionYear = BI.SurveyYear then D.MaxScore else null end) MaxScoreInYear
, count(DISTINCT case when BI.InspectionYear = BI.SurveyYear then D.inspID else null end) NumSchoolsInYear

--, case when BI.InspectionYear = BI.SurveyYear then 1 else 0 end InYear

from warehouse.AccreditationDetail D
	INNER JOIN warehouse.BestInspection BI
		ON D.inspID = BI.inspID
	INNER JOIN warehouse.dimensionSchoolSurvey DSS
		ON Dss.[Survey ID] = BI.SurveyDimensionID

GROUP BY
SurveyYear
, [School No], [School Name]
, DistrictCode, District
, AuthorityCode, Authority, AuthorityGovtCode, AuthorityGovt
, SchoolTypeCode, SchoolType
, DSS.[National Electorate No], DSS.[National Electorate]
, DSS.[Local Electorate No], DSS.[Local Electorate]
, DSS.[Region Code], DSS.Region
, BI.inspID
, BI.InspectionResult
, BI.InspectionYear
, Standard
, StandardName
, Criteria
, CriteriaName
, SubCriteria
, SubCriteriaName
, SubCriteriaQuestion
GO

