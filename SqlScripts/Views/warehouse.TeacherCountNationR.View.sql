SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Teacher Counts
-- Teacher counts aggregated to national level, split by sector age group and gender
-- USefuk for pivot table analysis or simple presentation of teacher demographics e.g.
-- Teacher supply and demand
-- 2 10 2018 now based on DistrictTeacherCount to isloate potential change to SchoolTeacherCount table
-- see Issue #518
-- =============================================
CREATE VIEW [warehouse].[TeacherCountNationR]
AS
Select SurveyYear
, AgeGroup
, Sector

, sum(NumSupportStaff) NumSupportStaff
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual

, sum(case when GenderCode='M' then NumSupportStaff end) NumSupportStaffM
, sum(case when GenderCode='M' then NumTeachers end) NumTeachersM
, sum(case when GenderCode='M' then Certified end) CertifiedM
, sum(case when GenderCode='M' then Qualified end) QualifiedM
, sum(case when GenderCode='M' then CertQual end) CertQualM

, sum(case when GenderCode='F' then NumSupportStaff end) NumSupportStaffF
, sum(case when GenderCode='F' then NumTeachers end) NumTeachersF
, sum(case when GenderCode='F' then Certified end) CertifiedF
, sum(case when GenderCode='F' then Qualified end) QualifiedF
, sum(case when GenderCode='F' then CertQual end) CertQualF
from warehouse.TeacherCountNation
GROUP BY SurveyYear,  Sector, AgeGroup
GO

