SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 11 2013
-- Description:	Report on curriculum name, from school survey
-- see PIVSurvey_Rubbish
-- =============================================
CREATE VIEW [dbo].[PIVColsCurriculum]
AS
Select ssID
, ssCurriculum Curriculum
, ssEdSystem    EdSystem
, ssMediumOfInstruction  MediumOfInstruction
FROM SchoolSurvey
GO

