SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[schoolYearLevelMap]
AS
Select SchNo, SurveyYear, ClassLevel, lvlYear,
case Seq
	when 0 then 'Enrolment'
	when 1 then 'SchoolType'
	when 2 then 'Default'
end Src,
Enrol,
Seq
from
(
Select *
, row_number() OVER (Partition by schNo, SurveyYear, lvlYear ORDER BY Seq) RN
from
(
Select
schNo
, SurveyYear
, Classlevel
, L.lvlYear
, 0 Seq
, sum(Enrol) Enrol
from warehouse.Enrol E
INNER JOIN lkpLevels L
ON E.ClassLevel = L.codeCode
GROUP BY schNo
, SurveyYear
, Classlevel
, L.lvlYear

UNION ALL

Select schNo
, svyYear
, tlmLevel
, L.lvlYear
,1
, null
from metaSchoolTypeLevelMap M
INNER JOIN SchoolSurvey SS
	ON M.stCode = SS.ssSchType
INNER JOIN lkpLevels L
ON M.tlmLevel = L.codeCode

UNION ALL
Select
schNo
, svyYear
, levelCode
, YearOfEd
,2
, null
from ListDefaultPathLevels P
CROSS JOIN SchoolSurvey SS
) U
) U2
WHERE RN = 1
GO

