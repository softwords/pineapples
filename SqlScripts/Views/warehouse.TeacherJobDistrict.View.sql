SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs district totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobDistrict]
WITH VIEW_METADATA
AS
Select SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType

, DistrictCode
, dName District

, sum(NumStaff) NumStaff

FROM warehouse.TeacherJobTable T
	INNER JOIN Districts D
		ON T.DistrictCode = D.dID
GROUP BY
SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType
, DistrictCode
, dName
GO

