SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	School Type Inspections with result
--
-- =============================================
CREATE VIEW [warehouse].[AccreditationSchoolType]
WITH VIEW_METADATA
AS
Select SurveyYear
, SchoolTypeCode
, SchoolType
, InspectionResult
, sum(Num) Num
, sum(NumThisYear) NumThisYear

FROM warehouse.InspectionTable
WHERE InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GROUP BY SurveyYear
, SchoolTypeCode
, SchoolType
, InspectionResult
GO

