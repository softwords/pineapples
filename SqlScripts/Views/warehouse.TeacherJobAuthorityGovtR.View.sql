SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs authority govttotals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobAuthorityGovtR]
WITH VIEW_METADATA
AS
Select SurveyYear
, JobTitle
, StaffType
, TeacherType

, AuthorityGroupCode AuthorityGovtCode
, AuthorityGroup AuthorityGovt

, sum(NumStaff) NumStaff
, sum(case when GenderCode = 'M' then NumStaff else null end) NumStaffM
, sum(case when GenderCode = 'F' then NumStaff else null end) NumStaffF

FROM warehouse.TeacherJobTable T
	INNER JOIN DimensionAuthority AA
		ON T.AuthorityCode = AA.AuthorityCode
GROUP BY
SurveyYear
, JobTitle
, StaffType
, TeacherType

, AuthorityGroupCode
, AuthorityGroup
GO

