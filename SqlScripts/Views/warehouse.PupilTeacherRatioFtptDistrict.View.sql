SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 24/08/2021
-- Description:	Warehouse - Teacher Pupil Ratio aggregated up to District
-- Based on warehouse.PupilTeacherRatioFtptSchool which is based on warehouse.TeacherFtptSchool (FTPT values)
-- Related to warehouse.PupilTeacherRatioDistrict and variants
-- =============================================
CREATE VIEW [warehouse].[PupilTeacherRatioFtptDistrict]
WITH VIEW_METADATA
AS

Select SurveyYear
, DistrictCode
, dName District
, Sector
, ISCEDSubClass
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(certQual) CertQual
, sum(Enrol) Enrol
from warehouse.PupilTeacherRatioFtptSchool
	LEFT JOIN Districts D
	ON DistrictCode = D.dID
GROUP BY SurveyYear
, DistrictCode
, dName
, Sector
, ISCEDSubClass
GO

