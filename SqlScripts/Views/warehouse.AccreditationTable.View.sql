SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	School Type Inspections with result
-- Other 'table' formats are actual tables - this is just a thin filter on the table
-- InspectionTable
-- =============================================
CREATE VIEW [warehouse].[AccreditationTable]
WITH VIEW_METADATA
AS
Select SurveyYear
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
, InspectionResult
, Num
, NumThisYear

FROM warehouse.InspectionTable
WHERE InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GO

