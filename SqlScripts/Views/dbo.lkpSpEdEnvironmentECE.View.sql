SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW lkpSpEdEnvironmentECE
AS
SELECT codeCode
      ,codeDescription
      ,codeSeq
      ,codeDescriptionL1
      ,codeDescriptionL2
	  , codeText
      ,pCreateUser
      ,pCreateDateTime
      ,pEditUser
      ,pEditDateTime
	  , pRowversion
  FROM dbo.lkpSpEdEnvironment
WHERE codeGroup = 'ECE'
GO

