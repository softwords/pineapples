SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSurveyRead].[SurveyControl]
AS
SELECT
	SchoolSurvey.ssID

	, Schools.schName
	, Schools.iCode
	, Schools.schType, Schools.schAuth

	, Survey.svyYear
	, Survey.svyCensusDate
	, Survey.svyCollectionDate
	, SYH.syID
	, SYH.syYear
	, SYH.schNo

	, SYH.saSent
	, SYH.saCollected
	, SYH.saReceived
	, SYH.saReceivedTarget
	, SYH.saCompletedby
	, SYH.saEntered
	, SYH.saAudit
	, SYH.saAuditDate
	, SYH.saAuditBy
	, SYH.saAuditResult
	, SYH.saAuditNote
	, SYH.saComment
	, SYH.saFileLocn
	, SYH.syDormant
	, SYH.saReceivedD
	, SYH.saReceivedDTarget

	, SYH.systCode
	, SYH.syAuth
	, SYH.syParent


	, Islands.iGroup
	, D.dName

	, SchoolSurvey.ssReview

	, SchoolSurvey.ssMissingData, SchoolSurvey.ssMissingDataNotSupplied

	, QUAL.NumIssues, QUAL.NumAlerts, QUAL.NumBlocks, QUAL.MaxLevel

	, Schools.schClosed, Schools.schEst
FROM
	Islands
		INNER JOIN Schools ON Islands.iCode = Schools.iCode
		INNER JOIN SchoolYearHistory SYH on Schools.schNo = SYH.schNo
		INNER JOIN Survey  ON Survey.svyYear = SYH.syYear
		LEFT JOIN SchoolSurvey ON (SYH.schNo = SchoolSurvey.schNo) AND (SYH.syYear = SchoolSurvey.svyYear)
		LEFT JOIN pSurveyRead.ssIDQuality QUAL ON SchoolSurvey.ssID = QUAL.ssID
		LEFT JOIN Districts D
			ON Islands.iGRoup = D.dID
GO

