SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Ghislain Hachey
-- Create date: 7 April 2020
-- Description:	A view providing a data in classic format
-- ==================================================
-- This view is designed to provide a familiar data format for FSM/RMI. It is similar
-- to the table ware.Accreditations but already links with bestSurvey and dimensionSchoolSurvey
-- to provide users everything in a single "table". With this view users can pull
-- all data for school accreditation like it was in the classic school accreditation
-- analysis spreadsheet, but with the added benefit of more consistent data from the
-- EMIS, historical analysis (not before possible), and more powerful aggregations
-- ==================================================
CREATE VIEW [warehouse].[AccreditationClassic]
AS
SELECT [School Name]
      ,[District]
      ,[Island]
      ,[Region]
      ,[Local Electorate]
      ,[National Electorate]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[AuthorityGovt]
	  ,[SchoolType]
      ,[Year Established]
      ,[Year Closed]
	  ,BI.[inspID]
      ,BI.[schNo]
      ,A.[StartDate]
      ,[EndDate]
      ,A.[InspectionYear]
      ,[SurveyYear]
      ,A.[InspectionResult]
      ,[SE.1.1]
      ,[SE.1.2]
      ,[SE.1.3]
      ,[SE.1.4]
      ,[SE.1]
      ,[SE.2.1]
      ,[SE.2.2]
      ,[SE.2.3]
      ,[SE.2.4]
      ,[SE.2]
      ,[SE.3.1]
      ,[SE.3.2]
      ,[SE.3.3]
      ,[SE.3.4]
      ,[SE.3]
      ,[SE.4.1]
      ,[SE.4.2]
      ,[SE.4.3]
      ,[SE.4.4]
      ,[SE.4]
      ,[SE.5.1]
      ,[SE.5.2]
      ,[SE.5.3]
      ,[SE.5.4]
      ,[SE.5]
      ,[SE.6.1]
      ,[SE.6.2]
      ,[SE.6.3]
      ,[SE.6.4]
      ,[SE.6]
      ,[CO.1]
      ,[CO.2]
      ,[Level1]
      ,[Level2]
      ,[Level3]
      ,[Level4]
  FROM [warehouse].[Accreditations] AS A
  INNER JOIN [warehouse].[BestInspection] AS BI ON A.inspID = BI.inspID
  INNER JOIN [warehouse].[dimensionSchoolSurvey] AS DSS ON DSS.[Survey ID] = BI.SurveyDimensionID
  WHERE BI.InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GO

