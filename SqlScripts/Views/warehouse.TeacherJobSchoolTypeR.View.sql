SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs school type totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobSchoolTypeR]
WITH VIEW_METADATA
AS
Select SurveyYear
, JobTitle
, StaffType
, TeacherType

, T.SchoolTypeCode
, stDescription SchoolType

, sum(NumStaff) NumStaff
, sum(case when GenderCode = 'M' then NumStaff else null end) NumStaffM
, sum(case when GenderCode = 'F' then NumStaff else null end) NumStaffF


FROM warehouse.TeacherJobTable T
	INNER JOIN  SchoolTypes S
		ON T.SchoolTypeCode = S.stCode
GROUP BY
SurveyYear
, JobTitle
, StaffType
, TeacherType

, T.SchoolTypeCode
, stDescription
GO

