SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs nation totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobNationR]
WITH VIEW_METADATA
AS
Select SurveyYear
, JobTitle
, StaffType
, TeacherType

, sum(NumStaff) NumStaff
, sum(case when GenderCode = 'M' then NumStaff else null end) NumStaffM
, sum(case when GenderCode = 'F' then NumStaff else null end) NumStaffF

FROM warehouse.TeacherJobTable
GROUP BY
SurveyYear
, JobTitle
, StaffType
, TeacherType
GO

