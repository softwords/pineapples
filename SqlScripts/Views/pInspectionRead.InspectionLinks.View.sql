SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInspectionRead].[InspectionLinks]
WITH VIEW_METADATA
AS
SELECT lnkID
      ,IL.inspID
      ,IL.docID
      ,lnkFunction
      ,lnkHidden
      ,IL.pCreateTag
      ,docTitle
      ,docDescription
      ,docSource
      ,docDate
      ,docRotate
      ,docTags
      ,docType
	  ,docCloudID
	  ,docCloudRemoved
      ,D.pCreateUser
      ,D.pCreateDateTime
      ,D.pEditUser
      ,D.pEditDateTime
      ,D.pRowversion
from DocumentLinks_ IL
	INNER JOIN Documents_ D
		ON IL.docID = D.docID
	WHERE IL.inspID is not null
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 5 2021
-- Description:	trigger of edits of InspectionLink
-- =============================================
CREATE TRIGGER [pInspectionRead].[InspectionLinksUpdate]
   ON   [pInspectionRead].[InspectionLinks]
   INSTEAD OF INSERT, UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- INSERT
	-- if there is no docID on a record, create it

	declare @datestamp datetime 
	Select @datestamp = getdate()

		-- insert any new docIDs
	INSERT INTO [dbo].[Documents_]
			   ([docID]
			   ,[docTitle]
			   ,[docDescription]
			   ,[docSource]
			   ,[docDate]
			   ,[docRotate]
			   ,[docTags]
			   , docCloudID		-- not sure why this would ever happen on an insert??
			   , pCreateUser
			   , pCreateDateTime
			   , pEditUser
			   , pEditDateTime
			   )
	SELECT         
				I.docID
			   ,I.[docTitle]
			   ,I.[docDescription]
			   ,I.[docSource]
			   ,I.[docDate]
			   ,isnull(I.[docRotate],0)
			   ,I.[docTags]
			   , I.docCloudID
			   , isnull(I.pCreateUser, original_login())
			   , isnull(I.pCreateDateTime, @datestamp)
			   , isnull(I.pEditUser, original_login())
			   , isnull(I.pEditDateTime, @datestamp)
		FROM INSERTED I
		LEFT JOIN Documents_ DOC
			ON I.docID = DOC.docID
		WHERE DOC.docID is null
		
		-- update exisitng document records
		UPDATE Documents_
		SET docTitle = I.docTitle
		, docDescription = I.docDescription
		, docSource = I.docSource
		, docRotate = isnull(I.docRotate, Documents_.docRotate)
		, pCreateUser = coalesce(I.pCreateUser, Documents_.pCreateUser, original_login())
		, pCreateDateTime = coalesce(I.pCreateDateTime, Documents_.pCreateDateTime, @datestamp)
		, pEditUser = case when update (pEditUser) then isnull(I.pEditUser, original_login()) else original_login() end
		, pEditDateTime = case when update(pEditDateTime) then isnull(I.pEditDateTime, @datestamp) else @datestamp end

		FROM Documents_
			INNER JOIN INSERTED I
				ON Documents_.docId = I.docID

		if update (pEditDateTime) 
			print 'pEditDateTime TRUE'
		else
			print 'pEditDateTime FALSE'

	-- insert to DocumentLinks
	INSERT INTO DocumentLinks_
	(
		inspID
		, schNo
		, docID
		, lnkFunction
		, pCreateTag
	)
	SELECT INSERTED.inspID
	, SI.schNo		-- silently bring in the schno for the inspection
	, docID
	, lnkFunction
	, INSERTED.pCreateTag
	FROM INSERTED 
	INNER JOIN SchoolInspection SI
		ON INSERTED.inspID = SI.inspID
		WHERE lnkID is null

	-- UPDATE to DocumentLinks_

	UPDATE DocumentLinks_
	SET inspID = I.inspID
	, schNo = SI.schNo
	, docID = I.docID
	, lnkFunction = I.lnkFunction
	FROM INSERTED I
	INNER JOIN SchoolInspection SI
		ON I.inspID = SI.inspID
	WHERE I.lnkID = DocumentLinks_.lnkID

	-- DELETE FROM  DocumentLinks_
	DELETE
	FRom DocumentLinks_
	FROM DocumentLinks_
	INNER JOIN DELETED
		ON DocumentLinks_.lnkId = DELETED.lnkID
	WHERE DocumentLinks_.lnkID not in 
	(Select lnkID from INSERTED)

END

GO

