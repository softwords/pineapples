SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		Brian Lewis
-- Create date: 20 July 2019
-- Description:	backward-compatible view of SchoolAccreditation
-- ==================================================
-- 3 views are available of 'Accreditation' objects:
--
-- 1) the XMl representation of the object - contans the full text of each criteria description etc
--    this may be loaded from the survey tool, or else fabricated from earlier SchoolAccreditation objects
--
-- 2) A normalised view showing 1 subcriteria per row, or, if subcriteria are not avilable
--		1 criteria per row. This forms the basis of aggregations going to the warehouse.
--		This view provides the underlying parse of the Xml object.
--		It is not dependent on any particular values for criteria or subcriteria numbers.
--		It requires only that the Xml is structured in the category/standard/criteria/subcriteria hierarchy.
--		We name assessment instruments in this hierarchy 'StandardAssessment'
--		pInspectionRead.SchoolStandardAssessment
--
-- 3) a denormalised view, with 1 column per criteria. this is equivalent to the original School Accreditation object that was stored in
--		its own table. This makes specific assumptions about the naming of criteria in the Xml file
--		e.g. SE.1.2 School Evaluation Standard `1 Criteria 2
--		pInspectionRead.SchoolAccreditations
--
--		Note that in the pInspectionRead schema, all views into Inspections and specifically accreditations derive directly
--		from parsing the Xml.
--		In the warehouse schema, some of these views are frozen as tables, for the usual reasons
--		- performance; and
--		- a stationary target for reporting ( not influenced by operational activities)
--
-- =============================================
CREATE VIEW [pInspectionRead].[SchoolAccreditations]
AS

Select *
FROM pInspectionRead.SCHOOL_ACCREDITATION
WHERE InspTypeCode = 'SCHOOL_ACCREDITATION'
GO

