SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[AccreditationPerformanceAuthority]
AS
SELECT SurveyYear
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, ResultValue
, ResultDescription
, ResultLevel
, sum([SE.1.1_Count]) 		[SE.1.1_Count]
, sum([SE.1.2_Count]) 		[SE.1.2_Count]
, sum([SE.1.3_Count]) 		[SE.1.3_Count]
, sum([SE.1.4_Count]) 		[SE.1.4_Count]
, sum([SE.1_Count]) 		[SE.1_Count]

, sum([SE.2.1_Count]) 		[SE.2.1_Count]
, sum([SE.2.2_Count]) 		[SE.2.2_Count]
, sum([SE.2.3_Count]) 		[SE.2.3_Count]
, sum([SE.2.4_Count]) 		[SE.2.4_Count]
, sum([SE.2_Count]) 		[SE.2_Count]

, sum([SE.3.1_Count]) 		[SE.3.1_Count]
, sum([SE.3.2_Count]) 		[SE.3.2_Count]
, sum([SE.3.3_Count]) 		[SE.3.3_Count]
, sum([SE.3.4_Count]) 		[SE.3.4_Count]
, sum([SE.3_Count]) 		[SE.3_Count]

, sum([SE.4.1_Count]) 		[SE.4.1_Count]
, sum([SE.4.2_Count]) 		[SE.4.2_Count]
, sum([SE.4.3_Count]) 		[SE.4.3_Count]
, sum([SE.4.4_Count]) 		[SE.4.4_Count]
, sum([SE.4_Count]) 		[SE.4_Count]

, sum([SE.5.1_Count]) 		[SE.5.1_Count]
, sum([SE.5.2_Count]) 		[SE.5.2_Count]
, sum([SE.5.3_Count]) 		[SE.5.3_Count]
, sum([SE.5.4_Count]) 		[SE.5.4_Count]
, sum([SE.5_Count]) 		[SE.5_Count]

, sum([SE.6.1_Count]) 		[SE.6.1_Count]
, sum([SE.6.2_Count]) 		[SE.6.2_Count]
, sum([SE.6.3_Count]) 		[SE.6.3_Count]
, sum([SE.6.4_Count]) 		[SE.6.4_Count]
, sum([SE.6_Count]) 		[SE.6_Count]

, sum([CO.1_Count]) 		[CO.1_Count]
, sum([CO.2_Count]) 		[CO.2_Count]

FROM warehouse.AccreditationPerformanceTable
GROUP BY
SurveyYear
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, ResultValue
, ResultDescription
, ResultLevel
GO

