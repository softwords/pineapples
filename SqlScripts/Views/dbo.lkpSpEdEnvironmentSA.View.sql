SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW lkpSpEdEnvironmentSA
AS
SELECT codeCode
      ,codeDescription
      ,codeSeq
      ,codeDescriptionL1
      ,codeDescriptionL2
	  , codeText
      ,pCreateUser
      ,pCreateDateTime
      ,pEditUser
      ,pEditDateTime
	  , pRowversion
  FROM dbo.lkpSpEdEnvironment
WHERE codeGroup = 'SCHOOLAGE'
GO

