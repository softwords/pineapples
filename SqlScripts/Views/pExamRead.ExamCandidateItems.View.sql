SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 9 2021
-- Detailed information about examination candidate items.
-- Uses pExamRead.ExamCandidates, so if the candidate is linked to a student, then the demographic
-- information from the Student_ - name, gender, dob, ethnicity -
-- is taken from the student, overriding the ExamCandidate record
-- =============================================
CREATE VIEW [pExamRead].[ExamCandidateItems]
AS

Select ECI.exItemCode, exciResponse, exciResult, EC.*
, X.exCode examCode
, X.exYear examYear
, XI.IndicatorKey
, XI.BenchmarkKey
, XI.StandardKey
, XI.exitemSeq
from dbo.ExamCandidateItems ECI
 INNER JOIN pExamRead.ExamCandidates EC
 ON ECI.excID = EC.excID
 INNER JOIN Exams X
	ON EC.exID = X.exID
LEFT JOIN ExamITems XI
ON XI.exID = X.exID
AND XI.exitemCode = ECI.exItemCode
GO

