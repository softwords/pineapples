SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Accreditations by District - 'report' format
-- The warehouse.AccreditionXXX views disaggregate by inspection result
-- The 'R' (=report) formats denormalise on InspectionResult,
-- creating columns for [Level 1] [Level 2], [Level 3], [Level 4]
-- This is consistent with naming and denormalising of Gender in
-- enrolment views in the warehouse.
-- Note that denormalising on InspectionResult in this way
-- makes hardcoded assumptions aboout the possible values for InspectinResult
-- and the 'business rule' that Level 2-4 are accredited, Level 1 is not
-- =============================================
CREATE VIEW [warehouse].[AccreditationNationR]
WITH VIEW_METADATA
AS
Select SurveyYear
, sum(NumSchools) NumSchools

-- cumulative
, sum([Level1]) [Level1]
, sum([Level2]) [Level2]
, sum([Level3]) [Level3]
, sum([Level4]) [Level4]
, sum(Evaluated) Evaluated
, sum(Accredited) Accredited

-- in year
, sum([Level1ThisYear]) [Level1ThisYear]
, sum([Level2ThisYear]) [Level2ThisYear]
, sum([Level3ThisYear]) [Level3ThisYear]
, sum([Level4ThisYear]) [Level4ThisYear]

, sum(EvaluatedThisYear) EvaluatedThisYear
, sum(AccreditedThisYear) AccreditedThisYear

FROM warehouse.AccreditationTableR

group by
SurveyYear
GO

