SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: May 2020
-- Description:	Warehouse - Exams Nation standards
--
-- This is the 'R' version of [warehouse].[ExamNationStandards]
-- =============================================
CREATE VIEW [warehouse].[ExamNationStandardsR]
AS
SELECT [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[standardID]
      ,[standardCode]
      ,[standardDesc]
      ,[achievementLevel]
      ,[achievementDesc]
	  ,sum(case when [Gender] = 'M' then Candidates end) CandidatesM
	  ,sum(case when [Gender] = 'F' then Candidates end) CandidatesF
	  ,sum(Candidates) CandidatesT
  FROM [warehouse].[ExamNationStandards]
  GROUP BY
	   [examID]
      ,[examCode]
      ,[examYear]
      ,[examName]
      ,[standardID]
      ,[standardCode]
      ,[standardDesc]
      ,[achievementLevel]
      ,[achievementDesc]
GO

