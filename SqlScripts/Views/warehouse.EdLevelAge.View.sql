SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Over and Under Age reporting
--
-- Using enrolment data and definitions of age ranges for education levels,
-- Calculate the numbers:
-- OVER Age (ie > ed level age range)
-- UNDER age (< ed level age range)
-- AT Age (at offical age range)
-- AT age is the same as nEnrol on Enrolment Ratio views.
-- Note that OVERS are over the education level range; a pupil may be OVERAGE for their class level,
-- but still within the age range for the education level
--
-- =============================================
CREATE VIEW [warehouse].[EdLevelAge]
AS
-- this view is an alias for EdLevelAgeTable
Select * from  [warehouse].[EdLevelAgeTable]
GO

