SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	TeacherJobs authority totals
--
-- =============================================
CREATE VIEW [warehouse].[TeacherJobAuthority]
WITH VIEW_METADATA
AS
Select SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType

, T. AuthorityCode
, Authority
, AuthorityTypeCode
, AuthorityType
, AuthorityGroupCode AuthorityGovtCode
, AuthorityGroup AuthorityGovt

, sum(NumStaff) NumStaff

FROM warehouse.TeacherJobTable T
	INNER JOIN DimensionAuthority AA
		ON T.AuthorityCode = AA.AuthorityCode
GROUP BY
SurveyYear
, GenderCode
, JobTitle
, StaffType
, TeacherType
,  T.AuthorityCode
, Authority
, AuthorityTypeCode
, AuthorityType
, AuthorityGroupCode
, AuthorityGroup
GO

