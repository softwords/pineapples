SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:  23/8/2021
-- Description:	Teacher FTE and FTPT numbers by Activity and School
--  FTE - 'Full Time Equivalent' - is the ratio of Activities in the ISCEDSUbClass / total teacher's activities
--     => sum of FTE = total number of teachers in warehouse.TeacherLocation (where Source not in ('A','A%') )
--  FTPT  'Full time /part time' - is the ratio of teaching activities in this category / total teacher's activities
--     => sum of FTPT = total number of teacher sin warehouse.TeacherLocation with some teaching duty
--  (where Source not in ('A','A%') and TAMX in ('T','M')
-- Q C and QC variants are for Qualified Certified and Qualified-and-Certified.
-- These totals are based on warehouse.TeacherActivityWeigfhts, as is warehouse.uisA9 used for the UIS Survey.
-- So, this can be used to show teacher numbers that agree with the UIS report and definitions.
-- =============================================
CREATE VIEW [warehouse].[TeacherFtptSchool]
AS
  Select W.schNo, W.SurveyYear, W.GenderCode, W.ISCEDSubClass, LSX.ilCode ISCEDClass
 , TL.AgeGroup, BSS.surveyDimensionID
 , DSS.AuthorityCode, DSS.[District Code] DistrictCode, DSS.SchoolTypeCode
      ,sum(W)	FTE	-- FTE
      ,sum(WQ)	FTEQ
      ,sum(WC)  FTEC
      ,sum(WQC) FTEQC
      ,sum(case when ilsCode in ('A','X') then null else WTeach end) FTPT -- => FTPT
      ,sum(WTeachQ)	FTPTQ
      ,sum(WTeachC) FTPTC
      ,sum(WTeachQC) FTPTQC
	  , sum(case TAMX when 'A' then 1 end) A
	  , sum(case TAMX when 'X' then 1 end) X
  from [warehouse].[TeacherActivityWeights] W
  INNER JOIN warehouse.TeacherLocation TL
	ON W.tID = TL.tID
	AND W.SurveyYear = TL.SurveyYear
  INNER JOIN warehouse.bestSurvey BSS
		ON W.schNo = BSS.schNo
		AND W.SurveyYear = BSS.SurveyYear
  INNER JOIN warehouse.DimensionSchoolSurvey DSS
	ON BSS.surveyDimensionID = DSS.[Survey ID]
  LEFT JOIN
  ( Select ilsCode, ilCode from ISCEDLevelSub LS
	UNION Select 'A', 'Support'
	UNION Select 'X', 'Support'
  ) LSX
  ON W.ISCEDSUbClass = LSX.ilsCode
  group by W.schNo, W.SurveyYear, W.GenderCode, W.ISCEDSubClass, TL.AgeGroup, BSS.surveyDimensionID
 , DSS.AuthorityCode, DSS.[District Code] , DSS.SchoolTypeCode, LSX.ilCode
GO

