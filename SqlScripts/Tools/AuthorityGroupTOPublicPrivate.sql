/*
move nationality codes from census workbooks to existing 
table lkpNationality.
*/
if @@SERVERNAME = 'MIEMIS-PROD' begin
	USE MIEMIS
end
begin transaction

---- IMPORTANT Change @commitMode to 1 to commmit the work
declare @commitMode int = 0
if @commitMode = 0
	select 'ROLLBACK mode'
else
	select 'COMMIT mode'
select 'Database is ' + DB_NAME() + ' on ' + @@SERVERNAME

Select 'BEFORE: AuthorityGovt'
Select * from lkpAuthorityGovt


UPDATE lkpAuthorityGovt
SET codeDescription = 'Public' where codeCode = 'G'
UPDATE lkpAuthorityGovt
SET codeDescription = 'Private' where codeCode = 'N'

Select 'AFTER: AuthorityGovt'
Select * from lkpauthorityGovt



if @commitMode = 0 begin
	rollback
	Select 'These changes have been rolled back - change @CommitMode to commit'
	print 'These changes have been rolled back - change @CommitMode to commit'
end
if @commitMode = 1 begin
	commit
	Select 'Changes committed'
	print 'Changes committed'

end

