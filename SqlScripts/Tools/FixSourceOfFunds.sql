/*
Source of Funds code sset up in census workbook are moved to table SourceOfFunds
Note this is not a new table, it has been used in the past to capture finance data on paper census.
*/
begin transaction

---- IMPORTANT Change @commitMode to 1 to commmit the work
declare @commitMode int = 0
if @commitMode = 0
	select 'ROLLBACK mode'
else
	select 'COMMIT mode'
select 'Database is ' + DB_NAME() + ' on ' + @@SERVERNAME


Select 'BEFORE: SourceOfFunds'
Select * from SourceOfFunds

declare @fsmSeq int = case db_name() when 'fedemis' then 0 else 100 end
declare @rmiSeq int = case db_name() when 'miemis' then 0 else 100 end
DELETE from SourceOfFunds
if db_name() = 'MIEMIS' begin
	INSERT INTO SourceOfFunds
	(sofCode, sofDesc, sofC)
	VALUES 
	('COMPACT', 'Compact Fund', 0)
	, ('FEDERAL', 'Federal Fund', 0)
	, ('GENERAL','General Fund', 0)
	, ('SEG', 'SEG',0)
	, ('XXX', 'Other', 0)
end

if db_name() = 'FEDEMIS' begin
	INSERT INTO SourceOfFunds
	(sofCode, sofDesc, sofC)
	VALUES 
	('SEG', 'SEG',0)
	, ('SECTOR', 'Sector', 0)
	, ('FEDERAL', 'Federal', 0)
	, ('DOMESTIC','Domestic', 0)
	, ('XXX', 'Other', 0)
end

Select 'AFTER: SourceOfFunds'
Select * from SourceOfFunds


if @commitMode = 0 begin
	rollback
	Select 'These changes have been rolled back - change @CommitMode to commit'
	print 'These changes have been rolled back - change @CommitMode to commit'
end
if @commitMode = 1 begin
	commit
	Select 'Changes committed'
	print 'Changes committed'

end

