SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolTypes](
	[stCode] [nvarchar](10) NOT NULL,
	[stDescription] [nvarchar](200) NOT NULL,
	[stAgeMin] [int] NULL,
	[stAgeMax] [int] NULL,
	[stOfficialMin] [int] NULL,
	[stOfficialMax] [int] NULL,
	[stTypeGrp] [nvarchar](10) NULL,
	[stForm] [nvarchar](50) NULL,
	[stDescriptionL1] [nvarchar](50) NULL,
	[stDescriptionL2] [nvarchar](50) NULL,
	[stIDPattern] [nvarchar](50) NULL,
	[stOrgUnitNumber] [int] NULL,
	[stGLSalaries] [nvarchar](50) NULL,
	[stSort] [int] NULL,
 CONSTRAINT [aaaaaSchoolTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[stCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[SchoolTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[SchoolTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[SchoolTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SchoolTypes] ADD  CONSTRAINT [DF__SchoolTyp__stAge__4B7734FF]  DEFAULT ((0)) FOR [stAgeMin]
GO
ALTER TABLE [dbo].[SchoolTypes] ADD  CONSTRAINT [DF__SchoolTyp__stAge__4C6B5938]  DEFAULT ((0)) FOR [stAgeMax]
GO
ALTER TABLE [dbo].[SchoolTypes] ADD  CONSTRAINT [DF__SchoolTyp__stOff__4D5F7D71]  DEFAULT ((0)) FOR [stOfficialMin]
GO
ALTER TABLE [dbo].[SchoolTypes] ADD  CONSTRAINT [DF__SchoolTyp__stOff__4E53A1AA]  DEFAULT ((0)) FOR [stOfficialMax]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account for posting salaries - only used if sector code does not provide the value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolTypes', @level2type=N'COLUMN',@level2name=N'stGLSalaries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'reporting order for School Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolTypes', @level2type=N'COLUMN',@level2name=N'stSort'
GO

