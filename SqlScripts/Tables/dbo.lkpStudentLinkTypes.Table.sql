SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpStudentLinkTypes](
	[codeCode] [nvarchar](20) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSort] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpStudentLinkTypes_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[lkpStudentLinkTypes] ADD  CONSTRAINT [DF_lkpStudentLinkTypes_codeSort]  DEFAULT ((0)) FOR [codeSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Functions of files that may be linked to a Student.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpStudentLinkTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Students' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpStudentLinkTypes'
GO

