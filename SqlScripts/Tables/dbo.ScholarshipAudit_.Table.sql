SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScholarshipAudit_](
	[schauID] [int] IDENTITY(1,1) NOT NULL,
	[schoID] [int] NOT NULL,
	[schoStatus] [nvarchar](100) NULL,
	[schoStatusDate] [datetime] NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](100) NULL,
	[posYear] [int] NULL,
	[posSemester] [int] NULL,
	[posAmount] [decimal](12, 2) NULL,
	[posPaid] [datetime] NULL,
	[posGPA] [decimal](3, 2) NULL,
	[fosCode] [nvarchar](10) NULL,
	[instCode] [nvarchar](10) NULL,
	[schoaNote] [nvarchar](100) NULL,
 CONSTRAINT [PK_ScholarshipAudit_] PRIMARY KEY CLUSTERED 
(
	[schauID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

