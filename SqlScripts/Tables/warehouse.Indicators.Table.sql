SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[Indicators](
	[domain] [nvarchar](20) NULL,
	[indicators] [xml] NOT NULL,
	[warehouseversion] [nvarchar](50) NOT NULL,
	[pCreateDateTime] [datetime] NOT NULL,
	[pRowversion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

