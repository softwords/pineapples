SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamCandidateItems](
	[excID] [uniqueidentifier] NOT NULL,
	[exciID] [int] IDENTITY(1,1) NOT NULL,
	[exciResponse] [nvarchar](10) NOT NULL,
	[exciResult] [int] NOT NULL,
	[exitemCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ExamCandidateItems] PRIMARY KEY CLUSTERED 
(
	[exciID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ExamCandidateItems_excID] ON [dbo].[ExamCandidateItems]
(
	[excID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExamCandidateItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidates_ExamCandidateItems] FOREIGN KEY([excID])
REFERENCES [dbo].[ExamCandidates] ([excID])
GO
ALTER TABLE [dbo].[ExamCandidateItems] CHECK CONSTRAINT [FK_ExamCandidates_ExamCandidateItems]
GO

