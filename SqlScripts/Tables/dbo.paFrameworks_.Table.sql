SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paFrameworks_](
	[pafrmCode] [nvarchar](10) NOT NULL,
	[pafrmDescription] [nvarchar](50) NOT NULL,
	[pafrmDescriptionL1] [nvarchar](50) NULL,
	[pafrmDescriptionL2] [nvarchar](50) NULL,
 CONSTRAINT [PK_paFrameworks] PRIMARY KEY CLUSTERED 
(
	[pafrmCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

