SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GeneralLedger](
	[glCode] [nvarchar](20) NOT NULL,
	[glDesc] [nvarchar](50) NULL,
	[glDbCr] [nvarchar](2) NULL,
	[glGroup1] [nvarchar](50) NULL,
	[glGroup2] [nvarchar](50) NULL,
	[glGroup3] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaGeneralLedger1_PK] PRIMARY KEY NONCLUSTERED 
(
	[glCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[GeneralLedger] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[GeneralLedger] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[GeneralLedger] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[GeneralLedger] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[GeneralLedger] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data maintained by Vanuatu Budget and Actual Reporting. There is a small ledger for each school, by survey. ssID is the forign key.
This data is edited in Excel, and read/written by macros in the Excel workbooks. The workbooks are invoked by a simple button form that implements ISurveyComponent.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GeneralLedger'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GeneralLedger'
GO

