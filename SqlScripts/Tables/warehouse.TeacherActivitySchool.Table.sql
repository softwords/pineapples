SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherActivitySchool](
	[schNo] [nvarchar](50) NULL,
	[SurveyYear] [int] NOT NULL,
	[YoE] [int] NULL,
	[Activity] [nvarchar](10) NULL,
	[Staff] [int] NULL,
	[StaffW] [decimal](12, 6) NULL,
	[NumStaff] [int] NULL,
	[Enrol] [int] NULL,
	[surveyDimensionID] [int] NULL,
	[GradePTR] [decimal](18, 6) NULL,
	[GradePTRW] [decimal](18, 6) NULL,
	[StaffQ] [int] NULL,
	[StaffC] [int] NULL,
	[StaffQC] [int] NULL,
	[StaffQ_W] [decimal](12, 6) NULL,
	[StaffC_W] [decimal](12, 6) NULL,
	[StaffQC_W] [decimal](12, 6) NULL
) ON [PRIMARY]
GO

