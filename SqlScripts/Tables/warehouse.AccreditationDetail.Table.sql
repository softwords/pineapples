SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[AccreditationDetail](
	[schNo] [nvarchar](50) NULL,
	[inspID] [int] NULL,
	[InspectionYear] [int] NULL,
	[SurveyDimensionID] [int] NULL,
	[CategoryType] [nvarchar](200) NULL,
	[Category] [nvarchar](200) NULL,
	[Standard] [nvarchar](50) NULL,
	[Criteria] [nvarchar](50) NULL,
	[SubCriteria] [nvarchar](50) NULL,
	[Score] [decimal](5, 1) NULL,
	[MaxScore] [int] NULL,
	[StandardName] [nvarchar](100) NULL,
	[CriteriaName] [nvarchar](300) NULL,
	[SubCriteriaName] [nvarchar](500) NULL,
	[SubCriteriaQuestion] [nvarchar](500) NULL,
	[Answer] [nvarchar](20) NULL
) ON [PRIMARY]
GO

