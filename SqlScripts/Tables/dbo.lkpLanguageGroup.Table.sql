SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpLanguageGroup](
	[lngGrp] [nvarchar](5) NOT NULL,
	[lngGrpName] [nvarchar](50) NULL,
	[lngGrpNameL1] [nvarchar](50) NULL,
	[lngGrpNameL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpLanguageGroup1_PK] PRIMARY KEY NONCLUSTERED 
(
	[lngGrp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpLanguageGroup] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpLanguageGroup] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpLanguageGroup] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpLanguageGroup] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpLanguageGroup] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classification of languages. Principally used to group indigeneous or regional languages.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpLanguageGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Language' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpLanguageGroup'
GO

