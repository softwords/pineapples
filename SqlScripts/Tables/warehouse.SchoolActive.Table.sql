SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SchoolActive](
	[SurveyYear] [int] NULL,
	[schNo] [nvarchar](50) NULL,
	[Active] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO

