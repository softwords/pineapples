SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[InspectionTable](
	[SurveyYear] [int] NOT NULL,
	[InspectionTypeCode] [nvarchar](20) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[District] [nvarchar](50) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[Authority] [nvarchar](100) NULL,
	[AuthorityGovtCode] [nvarchar](1) NULL,
	[AuthorityGovt] [nvarchar](50) NULL,
	[SchoolTypeCode] [nvarchar](10) NOT NULL,
	[SchoolType] [nvarchar](200) NULL,
	[InspectionResult] [nvarchar](50) NULL,
	[Num] [int] NULL,
	[NumThisYear] [int] NULL
) ON [PRIMARY]
GO

