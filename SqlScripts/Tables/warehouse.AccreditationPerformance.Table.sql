SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[AccreditationPerformance](
	[inspID] [int] NOT NULL,
	[ResultValue] [int] NOT NULL,
	[SE.1.1_Count] [int] NOT NULL,
	[SE.1.2_Count] [int] NOT NULL,
	[SE.1.3_Count] [int] NOT NULL,
	[SE.1.4_Count] [int] NOT NULL,
	[SE.1_Count] [int] NULL,
	[SE.2.1_Count] [int] NOT NULL,
	[SE.2.2_Count] [int] NOT NULL,
	[SE.2.3_Count] [int] NOT NULL,
	[SE.2.4_Count] [int] NOT NULL,
	[SE.2_Count] [int] NULL,
	[SE.3.1_Count] [int] NOT NULL,
	[SE.3.2_Count] [int] NOT NULL,
	[SE.3.3_Count] [int] NOT NULL,
	[SE.3.4_Count] [int] NOT NULL,
	[SE.3_Count] [int] NULL,
	[SE.4.1_Count] [int] NOT NULL,
	[SE.4.2_Count] [int] NOT NULL,
	[SE.4.3_Count] [int] NOT NULL,
	[SE.4.4_Count] [int] NOT NULL,
	[SE.4_Count] [int] NULL,
	[SE.5.1_Count] [int] NOT NULL,
	[SE.5.2_Count] [int] NOT NULL,
	[SE.5.3_Count] [int] NOT NULL,
	[SE.5.4_Count] [int] NOT NULL,
	[SE.5_Count] [int] NULL,
	[SE.6.1_Count] [int] NOT NULL,
	[SE.6.2_Count] [int] NOT NULL,
	[SE.6.3_Count] [int] NOT NULL,
	[SE.6.4_Count] [int] NOT NULL,
	[SE.6_Count] [int] NULL,
	[CO.1_Count] [int] NOT NULL,
	[CO.2_Count] [int] NOT NULL,
	[ResultDescription] [nvarchar](100) NULL,
	[ResultLevel] [nvarchar](20) NULL
) ON [PRIMARY]
GO

