SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_](
	[stuID] [uniqueidentifier] NOT NULL,
	[stuCardID] [nvarchar](40) NULL,
	[stuNamePrefix] [nvarchar](50) NULL,
	[stuGiven] [nvarchar](50) NULL,
	[stuMiddleNames] [nvarchar](50) NULL,
	[stuFamilyName] [nvarchar](50) NULL,
	[stuNameSuffix] [nvarchar](20) NULL,
	[stuDoB] [date] NULL,
	[stuDoBEst] [int] NULL,
	[stuGender] [nvarchar](1) NULL,
	[stuEthnicity] [nvarchar](200) NULL,
	[stuCreateFileRef] [uniqueidentifier] NULL,
	[stuEditFileRef] [uniqueidentifier] NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pRowversion] [timestamp] NOT NULL,
	[stuGivenSoundex]  AS (soundex([stuGiven])) PERSISTED,
	[stuFamilySoundex]  AS (soundex([stuFamilyName])) PERSISTED,
	[stuRedirect] [uniqueidentifier] NULL,
	[stuMaritalStatus] [nvarchar](10) NULL,
 CONSTRAINT [PK_Student_] PRIMARY KEY NONCLUSTERED 
(
	[stuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Student_CardID] ON [dbo].[Student_]
(
	[stuCardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Student_FamilyName] ON [dbo].[Student_]
(
	[stuFamilyName] ASC
)
INCLUDE ( 	[stuGiven],
	[stuID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Student_]  WITH CHECK ADD  CONSTRAINT [FK_Student__MaritalStatus] FOREIGN KEY([stuMaritalStatus])
REFERENCES [dbo].[lkpCivilStatus] ([codeCode])
GO
ALTER TABLE [dbo].[Student_] CHECK CONSTRAINT [FK_Student__MaritalStatus]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'filedb identifier of the workbook that created this record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Student_', @level2type=N'COLUMN',@level2name=N'stuCreateFileRef'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This field is set when the student records are merged. It allows any later references to this record (including workbook matches on data) to be identified with the Redirect record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Student_', @level2type=N'COLUMN',@level2name=N'stuRedirect'
GO

