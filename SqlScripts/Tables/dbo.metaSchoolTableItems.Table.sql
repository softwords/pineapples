SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTableItems](
	[tdiCode] [nvarchar](10) NOT NULL,
	[tdiSrc] [nvarchar](255) NULL,
	[tdiSrcSQL] [nvarchar](255) NULL,
 CONSTRAINT [aaaaametaSchoolTableItems1_PK] PRIMARY KEY NONCLUSTERED 
(
	[tdiCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTableItems] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTableItems] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTableItems] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTableItems] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTableItems] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A school TableItem is a code assigned to a SQL statement that will produce 2 columns: a code and a description. Each PupilTableDef may reference a TableItem to supply the rows of the grid, and another to supply the columns of the grid. Certain tokens may be substituted into this SQL Statement, to supply the SchoolType, and Year. 
The TableItems are rendered as XML in the MetaData section of the survey XML.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTableItems'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTableItems'
GO

