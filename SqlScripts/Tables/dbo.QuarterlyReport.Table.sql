SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuarterlyReport](
	[qrID] [int] NOT NULL,
	[qrNumDaysOpen] [real] NULL,
	[qrAggDaysAtt] [real] NULL,
	[qrAggDaysAbs] [real] NULL,
	[qrAggDaysMem]  AS ([qrAggDaysAtt]+[qrAggDaysAbs]),
	[qrAvgDailyAtt]  AS ([qrAggDaysAtt]/[qrNumDaysOpen]),
	[qrAvgDailyMem] [real] NULL,
	[qrTotEnrolToDate] [int] NULL,
	[qrPupilEnrolLastDay] [int] NULL,
	[qrAggDaysAttM] [real] NULL,
	[qrAggDaysAttF] [real] NULL,
	[qrAggDaysAbsM] [real] NULL,
	[qrAggDaysAbsF] [real] NULL,
	[qrAggDaysMemM]  AS ([qrAggDaysAttM]+[qrAggDaysAbsM]),
	[qrAggDaysMemF]  AS ([qrAggDaysAttF]+[qrAggDaysAbsF]),
	[qrAvgDailyAttM]  AS ([qrAggDaysAttM]/[qrNumDaysOpen]),
	[qrAvgDailyAttF]  AS ([qrAggDaysAttF]/[qrNumDaysOpen]),
	[qrAvgDailyMemM] [real] NULL,
	[qrAvgDailyMemF] [real] NULL,
	[qrTotEnrolToDateM] [int] NULL,
	[qrTotEnrolToDateF] [int] NULL,
	[qrDropoutM] [int] NULL,
	[qrDropoutF] [int] NULL,
	[qrDropout] [int] NULL,
	[qrTrinM] [int] NULL,
	[qrTrinF] [int] NULL,
	[qrTrin] [int] NULL,
	[qrTroutM] [int] NULL,
	[qrTroutF] [int] NULL,
	[qrTrout] [int] NULL,
	[qrGrad8M] [int] NULL,
	[qrGrad8F] [int] NULL,
	[qrGrad8] [int] NULL,
	[qrGrad12M] [int] NULL,
	[qrGrad12F] [int] NULL,
	[qrGrad12] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NOT NULL,
	[pCreateTag] [uniqueidentifier] NULL,
 CONSTRAINT [PK__Quarterl__FF4B98F4B72E5C26] PRIMARY KEY CLUSTERED 
(
	[qrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrAggDaysAttM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrAggDaysAttF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrAggDaysAbsM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrAggDaysAbsF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrAvgDailyMemM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrAvgDailyMemF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTotEnrolToDateM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTotEnrolToDateF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrDropoutM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrDropoutF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrDropout]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTrinM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTrinF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTrin]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTroutM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTroutF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrTrout]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrGrad8M]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrGrad8F]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrGrad8]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrGrad12M]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrGrad12F]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  DEFAULT (NULL) FOR [qrGrad12]
GO
ALTER TABLE [dbo].[QuarterlyReport]  WITH CHECK ADD  CONSTRAINT [FK__QuarterlyR__qrID__71E7C201] FOREIGN KEY([qrID])
REFERENCES [dbo].[SchoolInspection] ([inspID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[QuarterlyReport] CHECK CONSTRAINT [FK__QuarterlyR__qrID__71E7C201]
GO

