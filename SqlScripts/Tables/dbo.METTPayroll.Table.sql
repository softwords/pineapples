SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[METTPayroll](
	[Location] [nvarchar](255) NULL,
	[EmployeeCode] [nvarchar](255) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[UDField1] [nvarchar](255) NULL,
	[EmployeeGroup] [nvarchar](255) NULL,
	[Gender] [nvarchar](255) NULL,
	[MaritalStatus] [nvarchar](255) NULL,
	[NormalRate] [float] NULL,
 CONSTRAINT [aaaaaMETTPayroll1_PK] PRIMARY KEY NONCLUSTERED 
(
	[EmployeeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[METTPayroll] TO [pTeacherReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[METTPayroll] TO [pTeacherWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[METTPayroll] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[METTPayroll] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[METTPayroll] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OBSELETE - replaced by TeacherIdentityExternal for more generalised format.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'METTPayroll'
GO

