SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[WashDetail](
	[schNo] [nvarchar](50) NOT NULL,
	[inspID] [int] NOT NULL,
	[InspectionYear] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL,
	[Question] [nvarchar](200) NULL,
	[Response] [nvarchar](1000) NULL,
	[Item] [nvarchar](100) NULL,
	[Answer] [nvarchar](100) NULL,
	[Num] [int] NULL
) ON [PRIMARY]
GO

