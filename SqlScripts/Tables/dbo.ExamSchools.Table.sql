SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamSchools](
	[sProvince] [nvarchar](2) NOT NULL,
	[sID] [int] NOT NULL,
	[sNAme] [nvarchar](100) NULL,
	[sCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_ExamSchools] PRIMARY KEY CLUSTERED 
(
	[sProvince] ASC,
	[sID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ExamSchools] TO [pExamRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ExamSchools] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ExamSchools] TO [pExamWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ExamSchools] TO [pExamWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ExamSchools] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ExamSchools] ADD  CONSTRAINT [DF__ExamSchools__sID__214BF109]  DEFAULT ((0)) FOR [sID]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamSchools'
GO

