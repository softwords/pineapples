SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SchoolInspection](
	[SurveyYear] [int] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[InspectionType] [nvarchar](20) NULL,
	[inspID] [int] NULL,
	[InspectionYear] [int] NULL,
	[StartDate] [datetime] NULL,
	[InspectionResult] [nvarchar](50) NULL,
	[YearSeq] [int] NULL,
	[Seq] [int] NULL,
	[SurveyDimensionID] [int] NULL
) ON [PRIMARY]
GO

