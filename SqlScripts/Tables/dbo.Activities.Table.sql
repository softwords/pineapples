SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activities](
	[actID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[actName] [nvarchar](50) NULL,
	[actDescription] [nvarchar](255) NULL,
	[actTeachers] [smallint] NULL,
	[actStudents] [int] NULL,
	[actIncomeLY] [money] NULL,
 CONSTRAINT [aaaaaActivities1_PK] PRIMARY KEY NONCLUSTERED 
(
	[actID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Activities] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Activities] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Activities] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Activities] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Activities] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Activities_SSID] ON [dbo].[Activities]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Activities]  WITH CHECK ADD  CONSTRAINT [Activities_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Activities] CHECK CONSTRAINT [Activities_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Activities that generated income for the school, and how many teachers and pupils were involved. Part of SI TVET survey. ssID is the foreign key linking to SchoolSurvey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Activities'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Activities'
GO

