SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FundTypes](
	[fundType] [nvarchar](10) NOT NULL,
	[fundDesc] [nvarchar](50) NULL,
	[sofCode] [nvarchar](10) NULL,
	[fundSort] [int] NULL,
	[fundDescL1] [nvarchar](50) NULL,
	[fundDescL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaaFundTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[fundType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[FundTypes] TO [pFinanceAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[FundTypes] TO [pFinanceAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[FundTypes] TO [pFinanceAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[FundTypes] TO [pFinanceAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[FundTypes] TO [pFinanceRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[FundTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[FundTypes] ADD  CONSTRAINT [DF__FundTypes__fundS__1C873BEC]  DEFAULT ((0)) FOR [fundSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Categorisation of Source of Funds.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FundTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FundTypes'
GO

