SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolTables](
	[schtID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[tdefCode] [nvarchar](10) NULL,
	[schtRowCode] [nvarchar](10) NULL,
	[schtColCode] [nvarchar](10) NULL,
	[schtM] [int] NULL,
	[schtF] [int] NULL,
	[schtTot] [int] NULL,
 CONSTRAINT [aaaaaSchoolTables1_PK] PRIMARY KEY NONCLUSTERED 
(
	[schtID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolTables] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolTables] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolTables] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolTables] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolTables] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SchoolTables] ADD  CONSTRAINT [DF__SchoolTabl__ssID__2610A626]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[SchoolTables] ADD  CONSTRAINT [DF__SchoolTab__schtM__2704CA5F]  DEFAULT ((0)) FOR [schtM]
GO
ALTER TABLE [dbo].[SchoolTables] ADD  CONSTRAINT [DF__SchoolTab__schtF__27F8EE98]  DEFAULT ((0)) FOR [schtF]
GO
ALTER TABLE [dbo].[SchoolTables] ADD  CONSTRAINT [DF__SchoolTab__schtT__28ED12D1]  DEFAULT ((0)) FOR [schtTot]
GO

