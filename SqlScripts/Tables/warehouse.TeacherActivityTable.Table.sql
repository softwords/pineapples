SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherActivityTable](
	[SurveyYear] [int] NOT NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[Activity] [nvarchar](10) NULL,
	[Staff] [int] NULL,
	[StaffW] [decimal](12, 6) NULL,
	[Enrol] [int] NULL,
	[GradePTR] [decimal](12, 2) NULL,
	[GradePTRW] [decimal](12, 2) NULL,
	[StaffQ] [int] NULL,
	[StaffC] [int] NULL,
	[StaffQC] [int] NULL,
	[StaffQ_W] [decimal](12, 6) NULL,
	[StaffC_W] [decimal](12, 6) NULL,
	[StaffQC_W] [decimal](12, 6) NULL
) ON [PRIMARY]
GO

