SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[pupiltablesG](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[Age] [int] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[pt] [int] NULL,
	[ptCode] [nvarchar](20) NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Aggregates of PupilTables by school, class level and age and gender.

This is a big table, but allows access to any site specific pupil table definition inside the warehouse system.


' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'pupiltablesG'
GO

