SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherAttendance](
	[tattID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NULL,
	[schNo] [nvarchar](50) NULL,
	[tattAbsentDays] [int] NULL,
	[tattLateDays] [int] NULL,
	[tattGrade] [nvarchar](1) NULL,
	[tattComment] [ntext] NULL,
	[tmID] [int] NULL,
 CONSTRAINT [aaaaaTeacherAttendance1_PK] PRIMARY KEY NONCLUSTERED 
(
	[tattID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TeacherAttendance] TO [pTeacherRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherAttendance] TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAttendance] TO [pTeacherWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherAttendance] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherAttendance_SchNo] ON [dbo].[TeacherAttendance]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TeacherAttendance_tID] ON [dbo].[TeacherAttendance]
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TeacherAttendance] ADD  CONSTRAINT [DF__TeacherAtte__tID__2CBDA3B5]  DEFAULT ((0)) FOR [tID]
GO
ALTER TABLE [dbo].[TeacherAttendance] ADD  CONSTRAINT [DF__TeacherAt__tattA__2DB1C7EE]  DEFAULT ((0)) FOR [tattAbsentDays]
GO
ALTER TABLE [dbo].[TeacherAttendance] ADD  CONSTRAINT [DF__TeacherAt__tattL__2EA5EC27]  DEFAULT ((0)) FOR [tattLateDays]
GO
ALTER TABLE [dbo].[TeacherAttendance] ADD  CONSTRAINT [DF__TeacherAtt__tmID__2F9A1060]  DEFAULT ((0)) FOR [tmID]
GO
ALTER TABLE [dbo].[TeacherAttendance]  WITH CHECK ADD  CONSTRAINT [TeacherAttendance_FK00] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TeacherAttendance] CHECK CONSTRAINT [TeacherAttendance_FK00]
GO
ALTER TABLE [dbo].[TeacherAttendance]  WITH CHECK ADD  CONSTRAINT [TeacherAttendance_FK01] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[TeacherAttendance] CHECK CONSTRAINT [TeacherAttendance_FK01]
GO
ALTER TABLE [dbo].[TeacherAttendance]  WITH CHECK ADD  CONSTRAINT [TeacherAttendance_FK02] FOREIGN KEY([tmID])
REFERENCES [dbo].[TermStructure] ([tmID])
GO
ALTER TABLE [dbo].[TeacherAttendance] CHECK CONSTRAINT [TeacherAttendance_FK02]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teacher attendance by week. Shows number of daya in attendance. Foreign keys are tId, linking to Teacher Identity, and tmID linking to TermStructure, which define the week.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherAttendance'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers (Attendance)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherAttendance'
GO

