SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpSubjects](
	[subjCode] [nvarchar](10) NOT NULL,
	[subjName] [nvarchar](100) NOT NULL,
	[subjSort] [int] NULL,
	[ifID] [nvarchar](4) NULL,
	[subjNameL1] [nvarchar](50) NULL,
	[subjNameL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpSubjects1_PK] PRIMARY KEY NONCLUSTERED 
(
	[subjCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpSubjects] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSubjects] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSubjects] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpSubjects] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpSubjects] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpSubjects] ADD  CONSTRAINT [DF__lkpSubjec__subjS__7BB05806]  DEFAULT ((0)) FOR [subjSort]
GO
ALTER TABLE [dbo].[lkpSubjects]  WITH CHECK ADD  CONSTRAINT [lkpSubjects_FK00] FOREIGN KEY([ifID])
REFERENCES [dbo].[ISCEDField] ([ifID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lkpSubjects] CHECK CONSTRAINT [lkpSubjects_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Subjects taught on schools. Classes may refer to a subject, as well as recources. Teacher are trained to teach a subject, and Teacher Survey may record up to 3 subject taught by the teacher. Teachers can be searched by subject. Subject withy a non-zero sort code appear in the Enrolments by Subject grid.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSubjects'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Curriculum' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSubjects'
GO

