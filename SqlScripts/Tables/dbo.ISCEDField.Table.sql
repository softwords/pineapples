SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISCEDField](
	[ifID] [nvarchar](4) NOT NULL,
	[ifName] [nvarchar](250) NULL,
	[ifDesc] [ntext] NULL,
	[ifgID] [nvarchar](3) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaaISCEDField1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ifID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[ISCEDField] TO [pAdminWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDField] TO [pAdminWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDField] TO [pAdminWriteX] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISCEDField] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISCEDField] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ISCEDField]  WITH CHECK ADD  CONSTRAINT [ISCEDField_FK00] FOREIGN KEY([ifgID])
REFERENCES [dbo].[ISCEDFieldGroup] ([ifgID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ISCEDField] CHECK CONSTRAINT [ISCEDField_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ISCED codes for field of study. Subject can be mapped to these.
As per ISCED-F 2013 this is equivalent of the "Detailed Field"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDField'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'ISCED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDField'
GO

