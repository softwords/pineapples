SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScholarshipStudy_](
	[posID] [int] IDENTITY(1,1) NOT NULL,
	[schoID] [int] NOT NULL,
	[posYear] [int] NULL,
	[gyIndex] [int] NOT NULL,
	[posSemester] [int] NOT NULL,
	[posAmount] [decimal](12, 2) NULL,
	[posPaid] [datetime] NULL,
	[posGPA] [decimal](3, 2) NULL,
	[fosCode] [nvarchar](10) NULL,
	[instCode] [nvarchar](10) NULL,
	[pRowversion] [timestamp] NOT NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[posSubject] [nvarchar](200) NULL,
 CONSTRAINT [PK_ScholarshipStudy_] PRIMARY KEY CLUSTERED 
(
	[posID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ScholarshipStudy_YearSemester] ON [dbo].[ScholarshipStudy_]
(
	[schoID] ASC,
	[posYear] ASC,
	[posSemester] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScholarshipStudy_]  WITH CHECK ADD  CONSTRAINT [FK_Scholarship__ScholarshipStudy_] FOREIGN KEY([schoID])
REFERENCES [dbo].[Scholarships_] ([schoID])
GO
ALTER TABLE [dbo].[ScholarshipStudy_] CHECK CONSTRAINT [FK_Scholarship__ScholarshipStudy_]
GO
ALTER TABLE [dbo].[ScholarshipStudy_]  WITH CHECK ADD  CONSTRAINT [FK_ScholarshipStudy__Grade] FOREIGN KEY([gyIndex])
REFERENCES [dbo].[lkpTertiaryGrades] ([gyIndex])
GO
ALTER TABLE [dbo].[ScholarshipStudy_] CHECK CONSTRAINT [FK_ScholarshipStudy__Grade]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2020
-- Description:	Write audit records
--
-- History Log:
--   * 09/05/2022, Ghislain Hachey, edit the action string in audit logs and add user that deletes record.
-- =============================================
CREATE TRIGGER [dbo].[ScholarshipStudyUpdate] 
   ON  [dbo].[ScholarshipStudy_]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @user nvarchar(100) = original_login()
	DECLARE @dt datetime = getutcdate()

   INSERT INTO ScholarshipAudit_
   (
    schoID
   , posYear
   , posSemester
   , instCode, fosCode
   , posAmount, posPaid
   , posGPA
   , pCreateDateTime
   , pCreateUser
   , schoaNote
   )
   SELECT
   I.schoID
   , I.posYear
   , I.posSemester
   , I.instCode, I.fosCode
   , I.posAmount, I.posPaid
   , I.posGPA
   , I.pCreateDateTime
   , I.pCreateUser
   , 'Scholarship enrolment list created'
   FROM INSERTED I  
	LEFT JOIN DELETED D
		on D.posID = I.posID
	WHERE D.posID is null

-- updates : write ony fields that change value
 INSERT INTO ScholarshipAudit_
   (
    schoID
   , posYear
   , posSemester
   , instCode
   , fosCode
   , posAmount
   , posPaid
   , posGPA
   , pCreateDateTime
   , pCreateUser
   , schoaNote
   )
   Select
   I.schoID
   , I.posYear
   , I.posSemester
   , case when coalesce(I.instCode, D.instCode) is null then null
			when I.instCode = D.instCode then null
			else I.instcode end newInstCode
   , case when coalesce(I.fosCode, D.fosCode) is null then null
			when I.fosCode = D.fosCode then null
			else I.fosCode end newfosCode
	,case when coalesce(I.posAmount, D.posAmount) is null then null
			when I.posAmount = D.posAmount then null
			else I.posAmount end newposAmount
	,case when coalesce(I.posPaid, D.posPaid) is null then null
			when I.posPaid = D.posPaid then null
			else I.posPaid end newposPaid
	,case when coalesce(I.posGPA, D.posGPA) is null then null
			when I.posGPA = D.posGPA then null
			else I.posGPA end newposGPA
	, I.pEditDateTime
	, I.pEditUser
	, 'Scholarship enrolment list updated'
   FROM INSERTED I 
   INNER JOIN DELETED D
		on D.posID = I.posID

    INSERT INTO ScholarshipAudit_
   (
		schoID
		, posYear
		, posSemester
		, schoaNote
		, pCreateDateTime
		, pCreateUser
	)
   Select
   D.schoID
   , D.posYear
   , D.posSemester
   , 'Scholarship enrolment list deleted'
   , getUtcdate()
   , ISNULL(D.pEditUser, @user)
   FROM DELETED D
	LEFT JOIN INSERTED I 
		on D.posID = I.posID
	WHERE I.posID is null



END
GO
ALTER TABLE [dbo].[ScholarshipStudy_] ENABLE TRIGGER [ScholarshipStudyUpdate]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'only one record per semester per scholarship' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScholarshipStudy_', @level2type=N'INDEX',@level2name=N'IX_ScholarshipStudy_YearSemester'
GO

