SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherActivityWeights](
	[schNo] [nvarchar](50) NULL,
	[SurveyYear] [int] NULL,
	[GenderCode] [nvarchar](1) NULL,
	[AuthorityGovt] [nvarchar](1) NULL,
	[IscedSubClass] [nvarchar](20) NULL,
	[tID] [int] NULL,
	[Qualified] [int] NULL,
	[Certified] [int] NULL,
	[QualCert] [int] NULL,
	[Activities] [decimal](12, 6) NULL,
	[W] [decimal](12, 6) NULL,
	[WQ] [decimal](12, 6) NULL,
	[WC] [decimal](12, 6) NULL,
	[WQC] [decimal](12, 6) NULL,
	[WTeach] [decimal](12, 6) NULL,
	[WTeachQ] [decimal](12, 6) NULL,
	[WTeachC] [decimal](12, 6) NULL,
	[WTeachQC] [decimal](12, 6) NULL,
	[ActivityDetail] [int] NULL,
	[LevelStr] [nchar](20) NULL,
	[NewHire] [int] NOT NULL,
	[Inservice] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [warehouse].[TeacherActivityWeights] ADD  CONSTRAINT [DF_TeacherActivityWeights_NewHire]  DEFAULT ((0)) FOR [NewHire]
GO
ALTER TABLE [warehouse].[TeacherActivityWeights] ADD  CONSTRAINT [DF_TeacherActivityWeights_Inservice]  DEFAULT ((0)) FOR [Inservice]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weight within teaching activities' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'TeacherActivityWeights', @level2type=N'COLUMN',@level2name=N'WTeach'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Grade levels from -1 to 15, then T A X ' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'TeacherActivityWeights', @level2type=N'COLUMN',@level2name=N'LevelStr'
GO

