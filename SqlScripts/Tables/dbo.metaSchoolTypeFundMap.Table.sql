SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeFundMap](
	[mrpID] [int] IDENTITY(1,1) NOT NULL,
	[ftSurveyName] [nvarchar](50) NULL,
	[stCode] [nvarchar](10) NULL,
	[svyYear] [int] NULL,
	[fundtype] [nvarchar](10) NULL,
	[ftSort] [int] NULL,
 CONSTRAINT [aaaaametaSchoolTypeFundMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[mrpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeFundMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeFundMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeFundMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeFundMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeFundMap] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeFundMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeFundMap] ADD  CONSTRAINT [DF__metaSchoo__ftSor__5812160E]  DEFAULT ((0)) FOR [ftSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mapping table to specify the fund types that will appear on a given survey. Sets may be created by survey form, school type , or year. The best match (3,2 or 1 fields) wins - that is the set used.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeFundMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeFundMap'
GO

