SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationPrograms](
	[pgID] [int] IDENTITY(1,1) NOT NULL,
	[pgName] [nvarchar](200) NULL,
	[ilsCode] [nvarchar](10) NULL,
	[pgOrientation] [nvarchar](1) NULL,
	[secCode] [nvarchar](3) NULL,
	[pgStartAge] [smallint] NULL,
	[pgDuration] [smallint] NULL,
	[pgPartTime] [smallint] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaaEducationPrograms1_PK] PRIMARY KEY NONCLUSTERED 
(
	[pgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EducationPrograms] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EducationPrograms] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EducationPrograms] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[EducationPrograms] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EducationPrograms] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[EducationPrograms] ADD  CONSTRAINT [DF__Education__pgPar__75F77EB0]  DEFAULT ((0)) FOR [pgPartTime]
GO
ALTER TABLE [dbo].[EducationPrograms]  WITH CHECK ADD  CONSTRAINT [EducationPrograms_FK00] FOREIGN KEY([secCode])
REFERENCES [dbo].[EducationSectors] ([secCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[EducationPrograms] CHECK CONSTRAINT [EducationPrograms_FK00]
GO
ALTER TABLE [dbo].[EducationPrograms]  WITH CHECK ADD  CONSTRAINT [EducationPrograms_FK01] FOREIGN KEY([ilsCode])
REFERENCES [dbo].[ISCEDLevelSub] ([ilsCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[EducationPrograms] CHECK CONSTRAINT [EducationPrograms_FK01]
GO
ALTER TABLE [dbo].[EducationPrograms]  WITH CHECK ADD  CONSTRAINT [CK EducationPrograms pgPartTime] CHECK  (([pgPartTime]=(0) OR [pgPartTime]=(-1)))
GO
ALTER TABLE [dbo].[EducationPrograms] CHECK CONSTRAINT [CK EducationPrograms pgPartTime]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Education Programs is used only to populate the Quest Survey. However, it is required for that to work properly.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationPrograms'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Education System' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationPrograms'
GO

