SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISCEDFieldGroup](
	[ifgID] [nvarchar](3) NOT NULL,
	[ifgName] [nvarchar](250) NULL,
	[ifgDesc] [ntext] NULL,
	[ifgbID] [nvarchar](2) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaaISCEDFieldGroup1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ifgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[ISCEDFieldGroup] TO [pAdminWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDFieldGroup] TO [pAdminWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDFieldGroup] TO [pAdminWriteX] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISCEDFieldGroup] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISCEDFieldGroup] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ISCEDFieldGroup]  WITH CHECK ADD  CONSTRAINT [ISCEDFieldGroup_FK00] FOREIGN KEY([ifgbID])
REFERENCES [dbo].[ISCEDFieldGroupBroad] ([ifgbID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ISCEDFieldGroup] CHECK CONSTRAINT [ISCEDFieldGroup_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISCED groupings of Fields Of Study. 
Note these codes are defined by UNESCO, are system supplied, cannot be changed.
As per ISCED-F 2013 this is equivalent of the Narrow Field"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDFieldGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'ISCED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDFieldGroup'
GO

