SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GovtExpenditureState](
	[fnmID] [int] IDENTITY(1,1) NOT NULL,
	[fnmYear] [int] NOT NULL,
	[fnmDistrict] [nvarchar](10) NULL,
	[fnmExpTotA] [money] NULL,
	[fnmExpTotB] [money] NULL,
	[fnmGNP] [money] NULL,
	[fnmGNPCapita] [money] NULL,
	[fnmSource] [nvarchar](100) NULL,
	[fnmNote] [ntext] NULL,
	[fnmGNPCurrency] [nvarchar](3) NULL,
	[fnmGNPXchange] [float] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaaGovtExpenditureState_PK] PRIMARY KEY NONCLUSTERED 
(
	[fnmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_GovtExpenditureStateYearDistrict] ON [dbo].[GovtExpenditureState]
(
	[fnmYear] ASC,
	[fnmDistrict] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

