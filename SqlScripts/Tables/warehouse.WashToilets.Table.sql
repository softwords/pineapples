SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[WashToilets](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[inspID] [int] NULL,
	[InspectionYear] [int] NULL,
	[ToiletType] [nvarchar](100) NULL,
	[District] [nvarchar](50) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[SchoolType] [nvarchar](200) NULL,
	[SchoolTypeCode] [nvarchar](20) NULL,
	[Authority] [nvarchar](100) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[AuthorityGovt] [nvarchar](50) NULL,
	[AuthorityGovtCode] [nvarchar](1) NULL,
	[TotalF] [int] NULL,
	[UsableF] [int] NULL,
	[EnrolF] [int] NULL,
	[TotalM] [int] NULL,
	[UsableM] [int] NULL,
	[EnrolM] [int] NULL,
	[TotalC] [int] NULL,
	[UsableC] [int] NULL,
	[Total] [int] NULL,
	[Usable] [int] NULL,
	[Enrol] [int] NULL,
	[Source] [nvarchar](20) NULL
) ON [PRIMARY]
GO

