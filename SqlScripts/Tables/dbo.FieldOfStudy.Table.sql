SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FieldOfStudy](
	[fosCode] [nvarchar](10) NOT NULL,
	[fosName] [nvarchar](100) NOT NULL,
	[fosNameL1] [nvarchar](100) NULL,
	[fosNameL2] [nvarchar](100) NULL,
	[fosgrpCode] [nvarchar](10) NOT NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NOT NULL,
 CONSTRAINT [PK_FieldOfStudy] PRIMARY KEY CLUSTERED 
(
	[fosCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

