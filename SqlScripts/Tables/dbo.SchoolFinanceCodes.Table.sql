SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolFinanceCodes](
	[schNo] [nvarchar](50) NOT NULL,
	[fVendorNo] [nvarchar](50) NULL,
	[fBank] [nvarchar](50) NULL,
	[fBankBranch] [nvarchar](50) NULL,
	[fBankAccountNo] [nvarchar](50) NULL,
 CONSTRAINT [PK_SchoolFinanceCodes] PRIMARY KEY CLUSTERED 
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolFinanceCodes]  WITH CHECK ADD  CONSTRAINT [FK_Finance_School] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolFinanceCodes] CHECK CONSTRAINT [FK_Finance_School]
GO

