SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamIndicators](
	[exbnchID] [int] NOT NULL,
	[exID] [int] NOT NULL,
	[exindCode] [nvarchar](50) NOT NULL,
	[exindDescription] [nvarchar](500) NOT NULL,
	[exindID] [int] IDENTITY(1,1) NOT NULL,
	[exstdID] [int] NOT NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_ExamIndicators] PRIMARY KEY CLUSTERED 
(
	[exindID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

