SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[Expenditure](
	[SurveyYear] [int] NULL,
	[DistrictCode] [nvarchar](10) NULL,
	[GNP] [money] NULL,
	[GNPCapita] [money] NULL,
	[GNPCurrency] [nvarchar](3) NULL,
	[GNPLocal] [float] NULL,
	[GNPCapitaLocal] [float] NULL,
	[GovtExpA] [money] NULL,
	[GovtExpB] [money] NULL,
	[CostCentreCode] [nvarchar](20) NULL,
	[SectorCode] [nvarchar](10) NULL,
	[EdExpA] [float] NULL,
	[EdExpB] [float] NULL,
	[EdRecurrentExpA] [float] NULL,
	[EdRecurrentExpB] [float] NULL,
	[Enrolment] [int] NULL,
	[EnrolmentPerc] [float] NULL,
	[EnrolmentNation] [int] NULL,
	[EnrolmentNationPerc] [float] NULL
) ON [PRIMARY]
GO

