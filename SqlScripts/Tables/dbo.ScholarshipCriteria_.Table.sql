SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScholarshipCriteria_](
	[awID] [int] IDENTITY(1,1) NOT NULL,
	[gyIndex] [int] NOT NULL,
	[awGPA] [decimal](3, 2) NOT NULL,
	[awAmount] [decimal](12, 2) NOT NULL,
	[fosgrpCode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_ScholarshipCriteria_] PRIMARY KEY CLUSTERED 
(
	[awID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScholarshipCriteria_]  WITH CHECK ADD  CONSTRAINT [FK_ScholarshipCriteria_fosGroup] FOREIGN KEY([fosgrpCode])
REFERENCES [dbo].[FieldOfStudyGroups] ([codeCode])
GO
ALTER TABLE [dbo].[ScholarshipCriteria_] CHECK CONSTRAINT [FK_ScholarshipCriteria_fosGroup]
GO

