SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FieldOfStudyGroups](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NOT NULL,
	[codeDescriptionL1] [nvarchar](200) NULL,
	[codeDescriptionL2] [nvarchar](200) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_FieldOfStudyGroups] PRIMARY KEY CLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

