SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherJobTable](
	[SurveyYear] [int] NOT NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[SchoolTypeCode] [nvarchar](10) NOT NULL,
	[GenderCode] [nvarchar](1) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[StaffType] [nvarchar](100) NULL,
	[NumStaff] [int] NULL,
	[TeacherType] [nvarchar](100) NULL
) ON [PRIMARY]
GO

