SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[EdExpenditure](
	[SurveyYear] [int] NULL,
	[DistrictCode] [nvarchar](10) NULL,
	[CostCentreCode] [nvarchar](20) NULL,
	[SectorCode] [nvarchar](10) NULL,
	[Actual Recurrent] [float] NULL,
	[Budget Recurrent] [float] NULL,
	[Actual Total] [float] NULL,
	[Budget Total] [float] NULL
) ON [PRIMARY]
GO

