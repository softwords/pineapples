SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[ExamCandidateResultsTyped](
	[excID] [uniqueidentifier] NOT NULL,
	[studentID] [nvarchar](40) NULL,
	[excGiven] [nvarchar](50) NULL,
	[excMiddleNames] [nvarchar](50) NULL,
	[excFamilyName] [nvarchar](50) NULL,
	[examID] [int] NOT NULL,
	[examCode] [nvarchar](10) NULL,
	[examYear] [int] NULL,
	[examName] [nvarchar](50) NULL,
	[schNo] [nvarchar](50) NULL,
	[Gender] [nvarchar](1) NULL,
	[RecordType] [nvarchar](20) NOT NULL,
	[ID] [int] NULL,
	[Key] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[achievementLevel] [int] NOT NULL,
	[achievementDesc] [nvarchar](200) NOT NULL,
	[indicatorCount] [int] NULL,
	[weight] [decimal](15, 6) NULL,
	[candidateCount] [int] NULL
) ON [PRIMARY]
GO

