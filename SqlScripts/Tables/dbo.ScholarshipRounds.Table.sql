SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScholarshipRounds](
	[schrID] [int] IDENTITY(1,1) NOT NULL,
	[scholCode] [nvarchar](10) NOT NULL,
	[schrYear] [int] NOT NULL,
	[schrBudget] [decimal](12, 2) NULL,
	[schrDescription]  AS (([scholCode]+' ')+CONVERT([nvarchar](4),[schrYear],0)),
	[pRowversion] [timestamp] NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
 CONSTRAINT [PK_ScholarshipRounds] PRIMARY KEY CLUSTERED 
(
	[schrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ScholarshipRounds_Year] ON [dbo].[ScholarshipRounds]
(
	[scholCode] ASC,
	[schrYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScholarshipRounds]  WITH CHECK ADD  CONSTRAINT [FK_ScholarshipRounds_Type] FOREIGN KEY([scholCode])
REFERENCES [dbo].[ScholarshipTypes] ([codeCode])
GO
ALTER TABLE [dbo].[ScholarshipRounds] CHECK CONSTRAINT [FK_ScholarshipRounds_Type]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Scholarship Round Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScholarshipRounds', @level2type=N'CONSTRAINT',@level2name=N'FK_ScholarshipRounds_Type'
GO

