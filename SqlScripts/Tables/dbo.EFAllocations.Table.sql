SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EFAllocations](
	[efaID] [int] IDENTITY(1,1) NOT NULL,
	[efbID] [int] NOT NULL,
	[rgCode] [nvarchar](20) NOT NULL,
	[efaAlloc] [int] NOT NULL,
 CONSTRAINT [EFAllocations_PK] PRIMARY KEY NONCLUSTERED 
(
	[efaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EFAllocations] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[EFAllocations] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFAllocations] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EFAllocations] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFAllocations] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EFAllocations] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [efbID] ON [dbo].[EFAllocations]
(
	[efbID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EFAllocations]  WITH CHECK ADD  CONSTRAINT [EFTableBandsEFAllocations] FOREIGN KEY([efbID])
REFERENCES [dbo].[EFTableBands] ([efbID])
GO
ALTER TABLE [dbo].[EFAllocations] CHECK CONSTRAINT [EFTableBandsEFAllocations]
GO
ALTER TABLE [dbo].[EFAllocations]  WITH CHECK ADD  CONSTRAINT [FK_EFAllocations_RoleGrades] FOREIGN KEY([rgCode])
REFERENCES [dbo].[RoleGrades] ([rgCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[EFAllocations] CHECK CONSTRAINT [FK_EFAllocations_RoleGrades]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enrolment Formula Allocations list the number and role/grade of teachers a school will be allocated according to their enrolment. EFAllocations has a foreign key from EFTableBands. Ie the Band determines a set of allocation records. Each allocation is a RoleGrade, and number of positions at that RoleGrade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFAllocations'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'linked table/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFAllocations'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'frmEF defines the Enrolment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFAllocations'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrolment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFAllocations'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment Planning' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFAllocations'
GO

