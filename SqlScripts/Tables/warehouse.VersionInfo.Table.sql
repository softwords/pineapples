SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[VersionInfo](
	[versionID] [nvarchar](50) NULL,
	[versionDateTime] [datetime] NULL,
	[versionUser] [nvarchar](50) NULL
) ON [PRIMARY]
GO

