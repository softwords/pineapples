SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpFurnitureTypes](
	[codeCode] [nvarchar](50) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSort] [smallint] NULL,
	[fmChairCount] [int] NULL,
	[fmDeskCount] [int] NULL,
	[fmStorage] [int] NULL,
	[fmTeacherChairCount] [int] NULL,
	[fmTeacherDeskCount] [int] NULL,
	[fmBlackboard] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaalkpFurnitureTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpFurnitureTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpFurnitureTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpFurnitureTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpFurnitureTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpFurnitureTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpFurnitureTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpFurnitureTypes] ADD  CONSTRAINT [DF__lkpFurnit__codeS__5CD6CB2B]  DEFAULT ((0)) FOR [codeSort]
GO
ALTER TABLE [dbo].[lkpFurnitureTypes] ADD  CONSTRAINT [DF__lkpFurnit__fmCha__5DCAEF64]  DEFAULT ((0)) FOR [fmChairCount]
GO
ALTER TABLE [dbo].[lkpFurnitureTypes] ADD  CONSTRAINT [DF__lkpFurnit__fmDes__5EBF139D]  DEFAULT ((0)) FOR [fmDeskCount]
GO
ALTER TABLE [dbo].[lkpFurnitureTypes] ADD  CONSTRAINT [DF__lkpFurnit__fmSto__5FB337D6]  DEFAULT ((0)) FOR [fmStorage]
GO
ALTER TABLE [dbo].[lkpFurnitureTypes] ADD  CONSTRAINT [DF__lkpFurnit__fmTea__60A75C0F]  DEFAULT ((0)) FOR [fmTeacherChairCount]
GO
ALTER TABLE [dbo].[lkpFurnitureTypes] ADD  CONSTRAINT [DF__lkpFurnit__fmTea__619B8048]  DEFAULT ((0)) FOR [fmTeacherDeskCount]
GO
ALTER TABLE [dbo].[lkpFurnitureTypes] ADD  CONSTRAINT [DF__lkpFurnit__fmBla__628FA481]  DEFAULT ((0)) FOR [fmBlackboard]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'master list of furniture items. The specific items that appear on any survey are determined by the Furniture/Survey mapping mechanism.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpFurnitureTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Resources' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpFurnitureTypes'
GO

