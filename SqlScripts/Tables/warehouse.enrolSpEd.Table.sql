SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[enrolSpEd](
	[schNo] [nvarchar](50) NULL,
	[SurveyYear] [int] NULL,
	[surveyDimensionID] [int] NULL,
	[ClassLevelCode] [nvarchar](10) NULL,
	[ClassLevel] [nvarchar](50) NULL,
	[EdLevelCode] [nvarchar](50) NULL,
	[EdLevel] [nvarchar](50) NULL,
	[EnvironmentCode] [nvarchar](20) NULL,
	[Environment] [nvarchar](100) NULL,
	[DisabilityCode] [nvarchar](10) NULL,
	[Disability] [nvarchar](50) NULL,
	[EnglishLearnerCode] [nvarchar](10) NULL,
	[EnglishLearner] [nvarchar](50) NULL,
	[EthnicityCode] [nvarchar](200) NULL,
	[Ethnicity] [nvarchar](50) NULL,
	[GenderCode] [nvarchar](1) NULL,
	[Gender] [nvarchar](50) NULL,
	[Age] [int] NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[Authority] [nvarchar](100) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[District] [nvarchar](50) NULL,
	[AuthorityGovtCode] [nvarchar](1) NULL,
	[AuthorityGovt] [nvarchar](50) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[SchoolType] [nvarchar](200) NULL,
	[RegionCode] [nvarchar](50) NULL,
	[Region] [nvarchar](100) NULL,
	[Num] [int] NULL
) ON [PRIMARY]
GO

