SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EdExpenditureState](
	[xedID] [int] IDENTITY(1,1) NOT NULL,
	[fnmID] [int] NOT NULL,
	[xedCostCentre] [nvarchar](20) NULL,
	[xedItem] [nvarchar](50) NULL,
	[xedXtrn] [int] NULL,
	[sofCode] [nvarchar](20) NULL,
	[xedTotA] [money] NULL,
	[xedCurrentA] [money] NULL,
	[xedTotB] [money] NULL,
	[xedCurrentB] [money] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
	[xedSector] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaEXS_PK] PRIMARY KEY NONCLUSTERED 
(
	[xedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EdExpenditureState]  WITH CHECK ADD  CONSTRAINT [FK_EdExpenditureState_CostCentres] FOREIGN KEY([xedCostCentre])
REFERENCES [dbo].[CostCentres] ([ccCode])
GO
ALTER TABLE [dbo].[EdExpenditureState] CHECK CONSTRAINT [FK_EdExpenditureState_CostCentres]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sector code may be prompted for if ProRate = 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EdExpenditureState', @level2type=N'COLUMN',@level2name=N'xedSector'
GO

