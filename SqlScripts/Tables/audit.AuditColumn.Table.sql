SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [audit].[AuditColumn](
	[acolID] [int] IDENTITY(1,1) NOT NULL,
	[arowID] [int] NOT NULL,
	[acolName] [nvarchar](50) NOT NULL,
	[acolBefore] [nvarchar](50) NULL,
	[acolAfter] [nvarchar](50) NULL,
 CONSTRAINT [PK_AuditColumn] PRIMARY KEY CLUSTERED 
(
	[acolID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [audit].[AuditColumn]  WITH CHECK ADD  CONSTRAINT [FK_AuditColumn_AuditRow] FOREIGN KEY([arowID])
REFERENCES [audit].[AuditRow] ([arowID])
GO
ALTER TABLE [audit].[AuditColumn] CHECK CONSTRAINT [FK_AuditColumn_AuditRow]
GO

