SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[tableEnrol](
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[Age] [int] NULL,
	[GenderCode] [nvarchar](1) NOT NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[SchoolTypeCode] [nvarchar](10) NOT NULL,
	[Enrol] [int] NULL,
	[Rep] [int] NULL,
	[TrIn] [int] NULL,
	[TrOut] [int] NULL,
	[Boarders] [int] NULL,
	[Disab] [int] NULL,
	[Dropout] [int] NULL,
	[PSA] [int] NULL,
	[Expelled] [int] NULL,
	[Completed] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Table in warehouse holding enrolment related data.

This covers the "enrolment" data collection in survey forms:
-- enrol
-- repeaters
-- transfers in
-- transfers out
-- boarders
-- disability (aggregated)
-- dropouts
-- preschool attenders (aggregated)

Normalised by Gender, separates Estimates and Actuals.

warehouse.tableEnrol aggregates this data up to District, Authority, SchoolTypeCode and is used as the basis for higher level aggregations of any of these enrolment data items

For school level data, use warehouse.enrol' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'tableEnrol'
GO

