SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProvinceCensus](
	[pcId] [int] IDENTITY(1,1) NOT NULL,
	[pcProvince] [nvarchar](3) NULL,
	[pcYear] [int] NULL,
	[pcAge] [int] NULL,
	[pcM] [int] NULL,
	[pcF] [int] NULL,
 CONSTRAINT [aaaaaProvinceCensus1_PK] PRIMARY KEY NONCLUSTERED 
(
	[pcId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[ProvinceCensus] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ProvinceCensus] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ProvinceCensus] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[ProvinceCensus] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ProvinceCensus] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ProvinceCensus] ADD  CONSTRAINT [DF__ProvinceC__pcYea__50FB042B]  DEFAULT ((0)) FOR [pcYear]
GO
ALTER TABLE [dbo].[ProvinceCensus] ADD  CONSTRAINT [DF__ProvinceC__pcAge__51EF2864]  DEFAULT ((0)) FOR [pcAge]
GO
ALTER TABLE [dbo].[ProvinceCensus] ADD  CONSTRAINT [DF__ProvinceCen__pcM__52E34C9D]  DEFAULT ((0)) FOR [pcM]
GO
ALTER TABLE [dbo].[ProvinceCensus] ADD  CONSTRAINT [DF__ProvinceCen__pcF__53D770D6]  DEFAULT ((0)) FOR [pcF]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NOT USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProvinceCensus'
GO

