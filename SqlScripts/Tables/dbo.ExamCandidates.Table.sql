SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamCandidates](
	[excID] [uniqueidentifier] NOT NULL,
	[studentID] [nvarchar](40) NULL,
	[exID] [int] NOT NULL,
	[schNo] [nvarchar](50) NULL,
	[excGiven] [nvarchar](50) NULL,
	[excMiddleNames] [nvarchar](50) NULL,
	[excFamilyName] [nvarchar](50) NULL,
	[excGender] [nvarchar](1) NULL,
	[dID] [nvarchar](5) NULL,
	[excData] [xml] NULL,
	[excSeq] [int] NULL,
	[stuID] [uniqueidentifier] NULL,
	[tID] [int] NULL,
 CONSTRAINT [PK_ExamCandidates] PRIMARY KEY NONCLUSTERED 
(
	[excID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ExamCandidates_Exams] ON [dbo].[ExamCandidates]
(
	[exID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_ExamCandidates_Schools] ON [dbo].[ExamCandidates]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExamCandidates]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidates_Districts] FOREIGN KEY([dID])
REFERENCES [dbo].[Districts] ([dID])
GO
ALTER TABLE [dbo].[ExamCandidates] CHECK CONSTRAINT [FK_ExamCandidates_Districts]
GO
ALTER TABLE [dbo].[ExamCandidates]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidates_Exams] FOREIGN KEY([exID])
REFERENCES [dbo].[Exams] ([exID])
GO
ALTER TABLE [dbo].[ExamCandidates] CHECK CONSTRAINT [FK_ExamCandidates_Exams]
GO
ALTER TABLE [dbo].[ExamCandidates]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidates_Gender] FOREIGN KEY([excGender])
REFERENCES [dbo].[lkpGender] ([codeCode])
GO
ALTER TABLE [dbo].[ExamCandidates] CHECK CONSTRAINT [FK_ExamCandidates_Gender]
GO
ALTER TABLE [dbo].[ExamCandidates]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidates_Schools] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[ExamCandidates] CHECK CONSTRAINT [FK_ExamCandidates_Schools]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'teacher ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamCandidates', @level2type=N'COLUMN',@level2name=N'tID'
GO

