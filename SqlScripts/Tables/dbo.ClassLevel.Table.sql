SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassLevel](
	[pclID] [int] IDENTITY(1,1) NOT NULL,
	[pcID] [int] NULL,
	[pclLevel] [nvarchar](10) NULL,
	[pclNum] [int] NULL,
	[pclF] [int] NULL,
	[pclM] [int] NULL,
 CONSTRAINT [aaaaaPrimaryClassLevel1_PK] PRIMARY KEY NONCLUSTERED 
(
	[pclID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ClassLevel] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ClassLevel] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ClassLevel] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ClassLevel] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ClassLevel] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_PrimaryClassLevel_PCID] ON [dbo].[ClassLevel]
(
	[pcID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClassLevel] ADD  CONSTRAINT [DF__PrimaryCla__pcID__14B10FFA]  DEFAULT ((0)) FOR [pcID]
GO
ALTER TABLE [dbo].[ClassLevel] ADD  CONSTRAINT [DF__PrimaryCl__pclNu__15A53433]  DEFAULT ((0)) FOR [pclNum]
GO
ALTER TABLE [dbo].[ClassLevel]  WITH CHECK ADD  CONSTRAINT [PrimaryClassLevel_FK00] FOREIGN KEY([pclLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[ClassLevel] CHECK CONSTRAINT [PrimaryClassLevel_FK00]
GO
ALTER TABLE [dbo].[ClassLevel]  WITH CHECK ADD  CONSTRAINT [PrimaryClassLevel_FK01] FOREIGN KEY([pcID])
REFERENCES [dbo].[Classes] ([pcID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClassLevel] CHECK CONSTRAINT [PrimaryClassLevel_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains enrolment in a class at a single level. (M and F) Class PK clsID is the foreign key. Note early versions (Primary class format 2002-09) did not split by gender. Table supports Undefined enrolments (M,F, U).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'xml in: pEnrolmentWrite.updateClasses' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Primary class form,' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassLevel'
GO

