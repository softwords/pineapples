SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CensusWorkbookColumnMaps](
	[mapID] [int] IDENTITY(1,1) NOT NULL,
	[sheetName] [nvarchar](50) NULL,
	[columnName] [nvarchar](100) NULL,
	[mapping] [nvarchar](100) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_CensusWorkbookColumnMaps] PRIMARY KEY CLUSTERED 
(
	[mapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maps between column names set up on the sheets of the census workbook, and the names expected by the Workbook uploader. Note that ''vocabulary'' items for District, School Type and School No are always translated, so are not required in this table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CensusWorkbookColumnMaps', @level2type=N'COLUMN',@level2name=N'mapID'
GO

