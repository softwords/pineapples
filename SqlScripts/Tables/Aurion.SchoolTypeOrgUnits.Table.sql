SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Aurion].[SchoolTypeOrgUnits](
	[authCode] [nvarchar](10) NOT NULL,
	[stCode] [nvarchar](50) NOT NULL,
	[OrgUnitNumber] [int] NOT NULL,
 CONSTRAINT [PK_SchoolTypeOrgUnits] PRIMARY KEY CLUSTERED 
(
	[authCode] ASC,
	[stCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines the Organision Units in Aurtion linked to an Authority and school type. Part of the Aurion interface setup.' , @level0type=N'SCHEMA',@level0name=N'Aurion', @level1type=N'TABLE',@level1name=N'SchoolTypeOrgUnits'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Aurion' , @level0type=N'SCHEMA',@level0name=N'Aurion', @level1type=N'TABLE',@level1name=N'SchoolTypeOrgUnits'
GO

