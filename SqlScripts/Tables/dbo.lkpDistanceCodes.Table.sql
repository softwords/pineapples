SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpDistanceCodes](
	[codeNum] [int] NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpDistanceCodes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[lkpDistanceCodes] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[lkpDistanceCodes] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpDistanceCodes] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpDistanceCodes] TO [pSchoolWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpDistanceCodes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpDistanceCodes] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Represents distance from School In Distance/Transport component' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpDistanceCodes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpDistanceCodes'
GO

