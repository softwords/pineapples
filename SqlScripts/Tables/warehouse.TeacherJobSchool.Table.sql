SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherJobSchool](
	[schNo] [nvarchar](50) NOT NULL,
	[GenderCode] [nvarchar](1) NULL,
	[SurveyYear] [smallint] NOT NULL,
	[JobTitle] [nvarchar](100) NULL,
	[StaffType] [nvarchar](100) NULL,
	[surveyDimensionID] [int] NULL,
	[NumStaff] [int] NULL,
	[TeacherType] [nvarchar](100) NULL
) ON [PRIMARY]
GO

