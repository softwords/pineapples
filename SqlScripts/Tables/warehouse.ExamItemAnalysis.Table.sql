SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[ExamItemAnalysis](
	[exID] [int] NULL,
	[exItemCode] [nvarchar](100) NULL,
	[A] [int] NULL,
	[B] [int] NULL,
	[C] [int] NULL,
	[D] [int] NULL,
	[E] [int] NULL,
	[F] [int] NULL,
	[Other] [int] NULL,
	[iaAnswer] [nvarchar](1) NULL,
	[iaResponses] [int] NULL,
	[iaCorrect] [int] NULL,
	[iaCorrectPerc] [decimal](6, 3) NULL,
	[iaVarianceP] [float] NULL,
	[iaTopScore] [int] NULL,
	[iaTopCount] [int] NULL,
	[iaBottomScore] [int] NULL,
	[iaBottomCount] [int] NULL,
	[iaDI] [float] NULL
) ON [PRIMARY]
GO

