SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentEnrolment_](
	[stueID] [int] IDENTITY(1,1) NOT NULL,
	[stuID] [uniqueidentifier] NULL,
	[schNo] [nvarchar](50) NULL,
	[stueYear] [int] NULL,
	[stueClass] [nvarchar](10) NULL,
	[stueFrom] [nvarchar](20) NULL,
	[stueFromSchool] [nvarchar](50) NULL,
	[stueFromDate] [date] NULL,
	[stueSpEd] [nvarchar](1) NULL,
	[stueDaysAbsent] [decimal](10, 2) NULL,
	[stueCompleted] [nvarchar](1) NULL,
	[stueOutcome] [nvarchar](20) NULL,
	[stueOutcomeReason] [nvarchar](50) NULL,
	[stueCreateFileRef] [uniqueidentifier] NULL,
	[stueCreateFileRow] [int] NULL,
	[stueSpEdEnv] [nvarchar](20) NULL,
	[stueSpEdDisability] [nvarchar](10) NULL,
	[stueSpEdEnglish] [nvarchar](10) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pRowversion] [timestamp] NOT NULL,
	[stueData] [xml] NULL,
	[stueSpEdIEP] [int] NULL,
	[stueSpEdHasAccommodation] [int] NULL,
	[stueSpEdAccommodation] [nvarchar](10) NULL,
	[stueSpEdAssessment] [nvarchar](10) NULL,
	[stueSpEdExit] [nvarchar](10) NULL,
 CONSTRAINT [PK_StudentEnrolment_] PRIMARY KEY CLUSTERED 
(
	[stueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_StudentEnrolment_FileRef] ON [dbo].[StudentEnrolment_]
(
	[stueCreateFileRef] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_StudentEnrolment_School] ON [dbo].[StudentEnrolment_]
(
	[schNo] ASC,
	[stueYear] ASC
)
INCLUDE ( 	[stuID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_StudentEnrolment_StudentID] ON [dbo].[StudentEnrolment_]
(
	[stuID] ASC
)
INCLUDE ( 	[schNo],
	[stueYear]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_StudentEnrolment_Year] ON [dbo].[StudentEnrolment_]
(
	[stueYear] ASC,
	[schNo] ASC
)
INCLUDE ( 	[stuID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__Accomm_] FOREIGN KEY([stueSpEdAccommodation])
REFERENCES [dbo].[lkpSpEdAccommodations] ([codeCode])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__Accomm_]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__Assess_] FOREIGN KEY([stueSpEdAssessment])
REFERENCES [dbo].[lkpSpEdAssessmentTypes] ([codeCode])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__Assess_]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__Disab_] FOREIGN KEY([stueSpEdDisability])
REFERENCES [dbo].[lkpDisabilities] ([codeCode])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__Disab_]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__Exit_] FOREIGN KEY([stueSpEdExit])
REFERENCES [dbo].[lkpSpEdExits] ([codeCode])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__Exit_]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__lkpLevels] FOREIGN KEY([stueClass])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__lkpLevels]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__Schools] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__Schools]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__SpEdEnv_] FOREIGN KEY([stueSpEdEnv])
REFERENCES [dbo].[lkpSpEdEnvironment] ([codeCode])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__SpEdEnv_]
GO
ALTER TABLE [dbo].[StudentEnrolment_]  WITH CHECK ADD  CONSTRAINT [FK_StudentEnrolment__Student_] FOREIGN KEY([stuID])
REFERENCES [dbo].[Student_] ([stuID])
GO
ALTER TABLE [dbo].[StudentEnrolment_] CHECK CONSTRAINT [FK_StudentEnrolment__Student_]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How the student arrived in this class Promoted Repeater - from Ece - Transfer In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudentEnrolment_', @level2type=N'COLUMN',@level2name=N'stueFrom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the filedb identifier of the workbook that created this record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudentEnrolment_', @level2type=N'COLUMN',@level2name=N'stueCreateFileRef'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Index into the table of students in the source workbook' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudentEnrolment_', @level2type=N'COLUMN',@level2name=N'stueCreateFileRow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'for manual editing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudentEnrolment_', @level2type=N'COLUMN',@level2name=N'pEditDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'for manual editing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudentEnrolment_', @level2type=N'COLUMN',@level2name=N'pEditUser'
GO

