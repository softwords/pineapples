SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SanitationSupplementaryDeprecated](
	[SW_S_1] [nvarchar](50) NULL,
	[SW_S_2] [nvarchar](50) NULL,
	[SW_S_3] [nvarchar](50) NULL,
	[SW_S_4] [nvarchar](50) NULL,
	[SW_S_5] [nvarchar](50) NULL,
	[SW_S_6] [nvarchar](50) NULL,
	[SW_S_7] [nvarchar](50) NULL,
	[SW_S_8] [nvarchar](50) NULL,
	[SW_S_9] [nvarchar](50) NULL,
	[SW_S_10] [nvarchar](50) NULL,
	[SW_S_11] [nvarchar](50) NULL,
	[SW_S_12] [nvarchar](50) NULL,
	[SW_S_13] [nvarchar](50) NULL,
	[SW_S_14] [nvarchar](50) NULL,
	[SW_S_15] [nvarchar](50) NULL,
	[SW_S_16] [nvarchar](50) NULL,
	[SW_S_18] [nvarchar](50) NULL,
	[SW_S_19] [nvarchar](50) NULL,
	[SW_S_20] [nvarchar](50) NULL,
	[SW_S_22] [nvarchar](50) NULL,
	[SW_S_23] [nvarchar](50) NULL,
	[SW_S_24] [nvarchar](50) NULL,
	[SW_S_25] [nvarchar](50) NULL,
	[SW_S_26] [nvarchar](50) NULL,
	[SW_S_27] [nvarchar](50) NULL,
	[SW_S_28] [nvarchar](50) NULL,
	[SW_S_29] [nvarchar](50) NULL,
	[SW_S_30] [nvarchar](50) NULL,
	[SW_S_31] [nvarchar](50) NULL,
	[SW_S_32] [nvarchar](50) NULL,
	[SW_S_33] [nvarchar](50) NULL,
	[SW_S_34] [nvarchar](50) NULL,
	[SW_S_35] [nvarchar](50) NULL,
	[SW_S_36] [nvarchar](50) NULL,
	[SW_S_37] [nvarchar](50) NULL,
	[SW_S_38] [nvarchar](50) NULL,
	[SW_S_39] [nvarchar](50) NULL
) ON [PRIMARY]
GO

