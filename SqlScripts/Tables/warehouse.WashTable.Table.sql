SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[WashTable](
	[SurveyYear] [int] NOT NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[District] [nvarchar](50) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[Authority] [nvarchar](100) NULL,
	[AuthorityGovtCode] [nvarchar](1) NULL,
	[AuthorityGovt] [nvarchar](50) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[SchoolType] [nvarchar](200) NULL,
	[Question] [nvarchar](200) NULL,
	[Response] [nvarchar](1000) NULL,
	[Item] [nvarchar](100) NULL,
	[Answer] [nvarchar](100) NULL,
	[Num] [int] NULL,
	[NumThisYear] [int] NULL
) ON [PRIMARY]
GO

