SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Scholarships_](
	[schoID] [int] IDENTITY(1,1) NOT NULL,
	[stuID] [uniqueidentifier] NOT NULL,
	[schrID] [int] NOT NULL,
	[schoExpectedCompletion] [datetime] NULL,
	[schoStatus] [nvarchar](10) NULL,
	[schoStatusDate] [datetime] NULL,
	[schoInst] [nvarchar](10) NULL,
	[schoFOS] [nvarchar](10) NULL,
	[schoSubject] [nvarchar](200) NULL,
	[schoGPA] [decimal](3, 2) NULL,
	[pRowversion] [timestamp] NOT NULL,
	[schoNote] [nvarchar](500) NULL,
	[schoAppliedDate] [datetime] NULL,
	[schoCompliantDate] [datetime] NULL,
	[schoAwardedDate] [datetime] NULL,
	[schoCompletedDate] [datetime] NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pCreateTag] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Scholarship_] PRIMARY KEY CLUSTERED 
(
	[schoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Scholarships_StudentRound] ON [dbo].[Scholarships_]
(
	[schrID] ASC,
	[stuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Scholarships_]  WITH CHECK ADD  CONSTRAINT [FK_Scholarship_Round] FOREIGN KEY([schrID])
REFERENCES [dbo].[ScholarshipRounds] ([schrID])
GO
ALTER TABLE [dbo].[Scholarships_] CHECK CONSTRAINT [FK_Scholarship_Round]
GO
ALTER TABLE [dbo].[Scholarships_]  WITH CHECK ADD  CONSTRAINT [FK_Scholarship_Student] FOREIGN KEY([stuID])
REFERENCES [dbo].[Student_] ([stuID])
GO
ALTER TABLE [dbo].[Scholarships_] CHECK CONSTRAINT [FK_Scholarship_Student]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'student may apply once per round' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Scholarships_', @level2type=N'INDEX',@level2name=N'IX_Scholarships_StudentRound'
GO

