SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialResponsibility](
	[frID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[frResp] [nvarchar](20) NULL,
	[frPosition] [nvarchar](50) NULL,
	[frGiven] [nvarchar](50) NULL,
	[frFamilyName] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaFinancialResponsibility1_PK] PRIMARY KEY NONCLUSTERED 
(
	[frID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[FinancialResponsibility] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[FinancialResponsibility] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[FinancialResponsibility] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[FinancialResponsibility] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[FinancialResponsibility] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_FinancialResponsibility_SSID] ON [dbo].[FinancialResponsibility]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FinancialResponsibility]  WITH CHECK ADD  CONSTRAINT [FinancialResponsibility_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FinancialResponsibility] CHECK CONSTRAINT [FinancialResponsibility_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'From the Vanuatu FinancialInternal Audit. List of people with various Financial Responsibilities at the school.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FinancialResponsibility'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FinancialResponsibility'
GO

