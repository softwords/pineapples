SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScholarshipTypes](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](100) NOT NULL,
	[codeDescriptionL1] [nvarchar](100) NULL,
	[codeDescriptionL2] [nvarchar](100) NULL,
	[pRowversion] [timestamp] NOT NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[ConsecutiveSemesters] [int] NULL,
	[ReqGPA] [decimal](3, 2) NULL,
 CONSTRAINT [PK_ScholarshipTypes] PRIMARY KEY CLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

