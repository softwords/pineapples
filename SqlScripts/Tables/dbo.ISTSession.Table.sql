SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISTSession](
	[istsID] [int] IDENTITY(1,1) NOT NULL,
	[istcCode] [nvarchar](20) NOT NULL,
	[istsStatus] [nvarchar](1) NOT NULL,
	[istsInstitution] [nvarchar](50) NULL,
	[istsLocation] [nvarchar](50) NULL,
	[istsRoom] [nvarchar](20) NULL,
	[istsStartDate] [datetime] NOT NULL,
	[istsDuration] [int] NOT NULL,
	[istsDurationUnit] [nvarchar](10) NOT NULL,
	[istsMaxParticipants] [int] NOT NULL,
 CONSTRAINT [ISTSession_PK] PRIMARY KEY NONCLUSTERED 
(
	[istsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ISTSession] TO [pISTRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISTSession] TO [pISTWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISTSession] TO [pISTWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISTSession] TO [pISTWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISTSession] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [istcCode] ON [dbo].[ISTSession]
(
	[istcCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ISTSession]  WITH CHECK ADD  CONSTRAINT [ISTCourseISTSession] FOREIGN KEY([istcCode])
REFERENCES [dbo].[ISTCourse] ([istcCode])
GO
ALTER TABLE [dbo].[ISTSession] CHECK CONSTRAINT [ISTCourseISTSession]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ISTSession is a particuar instance of an ISTCourse, scheduled for a specifc time period. ISTCourse is a foreign key on ISTSession, and ISTCourseGRoup is a foreign key on ISTCourse, enabling some consolidated reporting.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTSession'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'Linked/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTSession'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Session are displayed in frmISTSessionList, and created/edited in
fdlgISTSession.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTSession'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'IST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTSession'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inservice Training' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTSession'
GO

