SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeFurnitureMap](
	[fmID] [int] IDENTITY(1,1) NOT NULL,
	[fmSurveyName] [nvarchar](50) NULL,
	[stCode] [nvarchar](10) NULL,
	[furnCode] [nvarchar](50) NULL,
	[fmSort] [int] NULL,
 CONSTRAINT [aaaaametaSchoolTypeFurnitureMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[fmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeFurnitureMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeFurnitureMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeFurnitureMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeFurnitureMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeFurnitureMap] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeFurnitureMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeFurnitureMap] ADD  CONSTRAINT [DF__metaSchoo__fmSor__731B1205]  DEFAULT ((0)) FOR [fmSort]
GO
ALTER TABLE [dbo].[metaSchoolTypeFurnitureMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeFurnitureMa_FK00] FOREIGN KEY([furnCode])
REFERENCES [dbo].[lkpFurnitureTypes] ([codeCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[metaSchoolTypeFurnitureMap] CHECK CONSTRAINT [metaSchoolTypeFurnitureMa_FK00]
GO
ALTER TABLE [dbo].[metaSchoolTypeFurnitureMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeFurnitureMa_FK01] FOREIGN KEY([stCode])
REFERENCES [dbo].[SchoolTypes] ([stCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[metaSchoolTypeFurnitureMap] CHECK CONSTRAINT [metaSchoolTypeFurnitureMa_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mapping table to specify the furniture type that will appear on the rooms page of a given survey. Sets may be created by survey form, school type , or year. The best match (3,2 or 1 fields) wins - that is the set used.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeFurnitureMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeFurnitureMap'
GO

