SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SchoolCountTable](
	[NumSchools] [int] NULL,
	[SurveyYear] [int] NOT NULL,
	[DistrictCode] [nvarchar](10) NULL,
	[District] [nvarchar](50) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[Authority] [nvarchar](50) NULL,
	[AuthorityGovtCode] [nvarchar](10) NULL,
	[AuthorityGovt] [nvarchar](50) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[SchoolType] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL
) ON [PRIMARY]
GO

