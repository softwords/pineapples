SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[WashWater](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[inspID] [int] NULL,
	[InspectionYear] [int] NULL,
	[District] [nvarchar](50) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[SchoolType] [nvarchar](200) NULL,
	[SchoolTypeCode] [nvarchar](20) NULL,
	[Authority] [nvarchar](100) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[AuthorityGovt] [nvarchar](50) NULL,
	[AuthorityGovtCode] [nvarchar](1) NULL,
	[PipedWaterSupplyCurrentlyAvailable] [nvarchar](3) NULL,
	[PipedWaterSupplyUsedForDrinking] [nvarchar](3) NULL,
	[ProtectedWellCurrentlyAvailable] [nvarchar](3) NULL,
	[ProtectedWellUsedForDrinking] [nvarchar](3) NULL,
	[UnprotectedWellSpringCurrentlyAvailable] [nvarchar](3) NULL,
	[UnprotectedWellSpringUsedForDrinking] [nvarchar](3) NULL,
	[RainwaterCurrentlyAvailable] [nvarchar](3) NULL,
	[RainwaterUsedForDrinking] [nvarchar](3) NULL,
	[BottledWaterCurrentlyAvailable] [nvarchar](3) NULL,
	[BottledWaterUsedForDrinking] [nvarchar](3) NULL,
	[TankerTruckCartCurrentlyAvailable] [nvarchar](3) NULL,
	[TankerTruckCartUsedForDrinking] [nvarchar](3) NULL,
	[SurfacedWaterCurrentlyAvailable] [nvarchar](3) NULL,
	[SurfacedWaterUsedForDrinking] [nvarchar](3) NULL
) ON [PRIMARY]
GO

