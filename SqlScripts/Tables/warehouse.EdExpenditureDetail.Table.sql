SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[EdExpenditureDetail](
	[SurveyYear] [int] NULL,
	[DistrictCode] [nvarchar](10) NULL,
	[CostCentreCode] [nvarchar](20) NULL,
	[Cost Centre] [nvarchar](100) NULL,
	[item] [nvarchar](50) NULL,
	[sectorCode] [nvarchar](10) NULL,
	[ProRated] [int] NULL,
	[Actual Recurrent] [float] NULL,
	[Budget Recurrent] [float] NULL,
	[Actual Total] [float] NULL,
	[Budget Total] [float] NULL,
	[SectorEnrolment] [int] NULL,
	[sectorEnrolmentPerc] [decimal](7, 4) NULL
) ON [PRIMARY]
GO

