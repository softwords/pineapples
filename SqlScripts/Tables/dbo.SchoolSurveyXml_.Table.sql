SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolSurveyXml_](
	[ssID] [int] NOT NULL,
	[ssXml] [xml] NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pRowversion] [timestamp] NOT NULL,
 CONSTRAINT [PK_SchoolSurveyXml_] PRIMARY KEY CLUSTERED 
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolSurveyXml_]  WITH CHECK ADD  CONSTRAINT [FK_SchoolSurveyXml__SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
GO
ALTER TABLE [dbo].[SchoolSurveyXml_] CHECK CONSTRAINT [FK_SchoolSurveyXml__SchoolSurvey]
GO

