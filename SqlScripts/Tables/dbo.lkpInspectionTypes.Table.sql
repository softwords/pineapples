SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpInspectionTypes](
	[intyCode] [nvarchar](20) NOT NULL,
	[intyDesc] [nvarchar](50) NULL,
	[intyForm] [nvarchar](50) NULL,
	[intyDescL1] [nvarchar](50) NULL,
	[intyDescL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
	[intyClass] [nvarchar](50) NULL,
	[intyPartition] [nvarchar](50) NULL,
	[intyResultFunction] [nvarchar](50) NULL,
	[intyTemplate] [xml] NULL,
 CONSTRAINT [lkpInspectionTypes_PK] PRIMARY KEY NONCLUSTERED 
(
	[intyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpInspectionTypes] TO [pInspectionAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpInspectionTypes] TO [pInspectionAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpInspectionTypes] TO [pInspectionAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpInspectionTypes] TO [pInspectionAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpInspectionTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpInspectionTypes] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If SSA, this is a SchoolStandardAssessment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInspectionTypes', @level2type=N'COLUMN',@level2name=N'intyClass'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For an SSA, Partitition Set to to use for assigning levels to scores' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInspectionTypes', @level2type=N'COLUMN',@level2name=N'intyPartition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of a sql function that calculates the ''Result'' of an inspection. this function returns nvarchar, and takes inspID int as its only parameter.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInspectionTypes', @level2type=N'COLUMN',@level2name=N'intyResultFunction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Types of school inspections. An inspection type may have a customised display form. BLDG - BuildingReview - is system defined. Foreign key on InspectionSet, which in turn is foreign key on SchoolInspections.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInspectionTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inspections' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInspectionTypes'
GO

