SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpSchoolRegistration](
	[rxCode] [nvarchar](10) NOT NULL,
	[rxDescription] [nvarchar](50) NOT NULL,
	[rxDescriptionL1] [nvarchar](50) NULL,
	[rxDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_lkpSchoolRegistration] PRIMARY KEY CLUSTERED 
(
	[rxCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpSchoolRegistration] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSchoolRegistration] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSchoolRegistration] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpSchoolRegistration] TO [public] AS [dbo]
GO

