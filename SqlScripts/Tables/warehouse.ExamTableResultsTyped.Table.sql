SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[ExamTableResultsTyped](
	[examID] [int] NOT NULL,
	[examCode] [nvarchar](10) NULL,
	[examYear] [int] NULL,
	[examName] [nvarchar](50) NULL,
	[Gender] [nvarchar](1) NULL,
	[DistrictCode] [nvarchar](10) NULL,
	[District] [nvarchar](100) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[SchoolType] [nvarchar](200) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[Authority] [nvarchar](100) NULL,
	[AuthorityGovtCode] [nvarchar](10) NULL,
	[AuthorityGovt] [nvarchar](100) NULL,
	[RecordType] [nvarchar](20) NULL,
	[ID] [int] NULL,
	[Key] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[achievementLevel] [int] NOT NULL,
	[achievementDesc] [nvarchar](200) NULL,
	[indicatorCount] [int] NULL,
	[weight] [decimal](15, 6) NULL,
	[candidateCount] [int] NULL
) ON [PRIMARY]
GO

