SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schools](
	[schNo] [nvarchar](50) NOT NULL,
	[schName] [nvarchar](200) NOT NULL,
	[schVillage] [nvarchar](50) NULL,
	[iCode] [nvarchar](2) NULL,
	[schType] [nvarchar](10) NOT NULL,
	[schAddr1] [nvarchar](50) NULL,
	[schAddr2] [nvarchar](50) NULL,
	[schAddr3] [nvarchar](50) NULL,
	[schAddr4] [nvarchar](50) NULL,
	[schPh1] [nvarchar](50) NULL,
	[schPh2] [nvarchar](50) NULL,
	[schFax] [nvarchar](50) NULL,
	[schEmail] [nvarchar](50) NULL,
	[schWWW] [nvarchar](150) NULL,
	[schElectN] [nvarchar](10) NULL,
	[schElectL] [nvarchar](10) NULL,
	[schAuth] [nvarchar](10) NULL,
	[schLang] [nvarchar](10) NULL,
	[schLandOwner] [nvarchar](10) NULL,
	[schLandUse] [nvarchar](10) NULL,
	[schLandUseExpiry] [datetime] NULL,
	[schEst] [smallint] NULL,
	[schEstBy] [nvarchar](50) NULL,
	[schClosed] [smallint] NULL,
	[schCloseReason] [nvarchar](50) NULL,
	[schRegStatus] [nvarchar](10) NULL,
	[schReg] [nvarchar](20) NULL,
	[schRegYear] [int] NULL,
	[schRegDate] [datetime] NULL,
	[schRegStatusDate] [datetime] NULL,
	[schXtrnID] [int] NULL,
	[schClass] [nvarchar](10) NULL,
	[schXNo] [nvarchar](8) NULL,
	[schpayptCode] [nvarchar](10) NULL,
	[schClosedLimit]  AS (case [schclosed] when (0) then (9999) else [schclosed] end) PERSISTED,
	[schOrgUnitNumber] [int] NULL,
	[schGLSalaries] [nvarchar](50) NULL,
	[schParent] [nvarchar](50) NULL,
	[schEstablishmentPoint]  AS (isnull([schParent],[schNo])) PERSISTED NOT NULL,
	[schIsExtension]  AS (case when [schParent] IS NULL then (0) else (1) end),
	[pRowversion] [timestamp] NOT NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditContext] [nvarchar](50) NULL,
	[schDormant] [bit] NOT NULL,
	[schElev] [float] NULL,
	[schLat] [float] NULL,
	[schLong] [float] NULL,
	[schPhoto] [uniqueidentifier] NULL,
	[schNextSchool] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaSchools1_PK] PRIMARY KEY NONCLUSTERED 
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT ALTER ON [dbo].[Schools] TO [pineapples] AS [dbo]
GO
GRANT SELECT ON [dbo].[Schools] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Schools] TO [pSchoolWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[Schools] TO [pSchoolWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Schools] TO [public] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schName]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schVillage]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([iCode]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schType]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schAddr1]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schAddr2]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schAddr3]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schAddr4]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schPh1]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schPh2]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schFax]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schEmail]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schWWW]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schElectN]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schElectL]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schAuth]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schLang]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schLandOwner]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schLandUse]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schLandUseExpiry]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schEst]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schEstBy]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schClosed]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schCloseReason]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schRegStatus]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schReg]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schRegYear]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schRegDate]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schRegStatusDate]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schXtrnID]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schClass]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schXNo]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schpayptCode]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schClosedLimit]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schOrgUnitNumber]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schGLSalaries]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schParent]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schEstablishmentPoint]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schIsExtension]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([pCreateDateTime]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([pCreateUser]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([pEditDateTime]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([pEditUser]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([pEditContext]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schDormant]) TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schElev]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schLat]) TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Schools] ([schLong]) TO [pSchoolWrite] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolsName] ON [dbo].[Schools]
(
	[schName] ASC
)
INCLUDE ( 	[schNo]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolsParent] ON [dbo].[Schools]
(
	[schParent] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Schools] ADD  DEFAULT ((0)) FOR [schClosed]
GO
ALTER TABLE [dbo].[Schools] ADD  DEFAULT ((0)) FOR [schXtrnID]
GO
ALTER TABLE [dbo].[Schools] ADD  DEFAULT ((0)) FOR [schDormant]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [FK__Schools__schNext__65E1DDCF] FOREIGN KEY([schNextSchool])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [FK__Schools__schNext__65E1DDCF]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [FK_SchoolParent] FOREIGN KEY([schParent])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [FK_SchoolParent]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK00] FOREIGN KEY([schElectN])
REFERENCES [dbo].[lkpElectorateN] ([codeCode])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK00]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK01] FOREIGN KEY([schClass])
REFERENCES [dbo].[lkpSchoolClass] ([codeCode])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK01]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK02] FOREIGN KEY([schElectL])
REFERENCES [dbo].[lkpElectorateL] ([codeCode])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK02]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK03] FOREIGN KEY([iCode])
REFERENCES [dbo].[Islands] ([iCode])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK03]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK04] FOREIGN KEY([schAuth])
REFERENCES [dbo].[Authorities] ([authCode])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK04]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK05] FOREIGN KEY([schLandOwner])
REFERENCES [dbo].[lkpLandOwnership] ([codeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK05]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK06] FOREIGN KEY([schLandUse])
REFERENCES [dbo].[lkpLandUse] ([codeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK06]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [Schools_FK07] FOREIGN KEY([schType])
REFERENCES [dbo].[SchoolTypes] ([stCode])
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [Schools_FK07]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [CK_Schools] CHECK  (([schParent] IS NULL OR [schParent]<>[SchNo]))
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [CK_Schools]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [CK_Schools_Elev] CHECK  (([schElev] IS NULL OR [schElev]>=(-10) AND [schElev]<=(8000)))
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [CK_Schools_Elev]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [CK_Schools_Lat] CHECK  (([schLat] IS NULL OR [schLat]>=(-90) AND [schLat]<=(90)))
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [CK_Schools_Lat]
GO
ALTER TABLE [dbo].[Schools]  WITH CHECK ADD  CONSTRAINT [CK_Schools_Long] CHECK  (([schLong] IS NULL OR [schLong]>=(-180) AND [schLong]<=(180)))
GO
ALTER TABLE [dbo].[Schools] CHECK CONSTRAINT [CK_Schools_Long]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[Schools_AuditDelete] 
   ON  [dbo].[Schools] 
   
   WITH EXECUTE as 'pineapples'
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from DELETED
	
	
	exec @AuditID =  audit.LogAudit 'Schools', null, 'D', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKeyS, arowName)
	SELECT DISTINCT @AuditID
	, schNo, schName
	FROM DELETED	



END
GO
ALTER TABLE [dbo].[Schools] ENABLE TRIGGER [Schools_AuditDelete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[Schools_AuditInsert] 
   ON  [dbo].[Schools] 
    
   WITH EXECUTE as 'pineapples'

   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	declare @user nvarchar(50)

	select 
		@NumChanges = count(*)
		, @user = case 
					when UPDATE(pEditUser) then min(pEditUser)
					else ORIGINAL_LOGIN() 
				  end
		 from INSERTED	
	
	exec @AuditID =  audit.LogAudit 'Schools', null, 'I', @Numchanges, @user
	
	INSERT INTO audit.AuditRow
	(auditID, arowKeyS)
	SELECT DISTINCT @AuditID
	, schNo
	FROM INSERTED	

-- update the created and edited fields
	UPDATE Schools
		SET pCreateDateTime = aLog.auditDateTime
			, pCreateUser = alog.auditUser

			, pEditDateTime = aLog.auditDateTime
			, pEditUser = alog.auditUser
	FROM INSERTED I
		CROSS JOIN Audit.auditLog alog
	WHERE Schools.schNo = I.schNo
	AND aLog.auditID = @auditID


END
GO
ALTER TABLE [dbo].[Schools] ENABLE TRIGGER [Schools_AuditInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-0912
-- Description:	Logging trigger
-- =============================================
CREATE TRIGGER [dbo].[Schools_AuditUpdate] 
   ON  [dbo].[Schools] 
  WITH EXECUTE AS 'pineapples'
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @AuditID int



		declare @NumChanges int
		declare @user nvarchar(50)
		

		Select 
		@NumChanges = count(*)
		, @user = case 
					when UPDATE(pEditUser) then min(pEditUser)
					else ORIGINAL_LOGIN() 
				  end
		 from INSERTED
		
		
		exec @AuditID =  audit.LogAudit 'Schools', null, 'U', @Numchanges, @user
		
		INSERT INTO audit.AuditRow
		(auditID, arowKeyS)
		SELECT DISTINCT @AuditID
		, schNo
		FROM INSERTED	



		
		INSERT INTO audit.AuditColumn
		(arowID, acolName, acolBefore, acolAfter)	
		
		SELECT arowID
		, ColumnName
		, DValue
		, IValue
		FROM audit.AuditRow Row
		INNER JOIN 
		(
				SELECT 
			I.schNo
			, case num
				when 1 then 'schNo'
				when 2 then 'schName'
				when 3 then 'schVillage'
				when 4 then 'iCode'
				when 5 then 'schType'
				when 6 then 'schAddr1'
				when 7 then 'schAddr2'
				when 8 then 'schAddr3'
				when 9 then 'schAddr4'
				when 10 then 'schPh1'
				when 11 then 'schPh2'
				when 12 then 'schFax'
				when 13 then 'schEmail'
				when 14 then 'schWWW'
				when 15 then 'schElectN'
				when 16 then 'schElectL'
				when 17 then 'schAuth'
				when 18 then 'schLang'
				when 19 then 'schLandOwner'
				when 20 then 'schLandUse'
				when 21 then 'schLandUseExpiry'
				when 22 then 'schEst'
				when 23 then 'schEstBy'
				when 24 then 'schClosed'
				when 25 then 'schCloseReason'
				when 26 then 'schRegStatus'
				when 27 then 'schReg'
				when 28 then 'schRegYear'
				when 29 then 'schRegDate'
				when 30 then 'schRegStatusDate'
				when 31 then 'schXtrnID'
				when 32 then 'schClass'
				when 33 then 'schXNo'
				when 34 then 'schpayptCode'
				when 35 then 'schClosedLimit'
				when 36 then 'schOrgUnitNumber'
				when 37 then 'schGLSalaries'
				when 38 then 'schParent'
				when 39 then 'schDormant'
			end ColumnName
			
			, case num 
				when 1 then cast(D.schNo as nvarchar(50))
				when 2 then cast(D.schName as nvarchar(50))
				when 3 then cast(D.schVillage as nvarchar(50))
				when 4 then cast(D.iCode as nvarchar(50))
				when 5 then cast(D.schType as nvarchar(50))
				when 6 then cast(D.schAddr1 as nvarchar(50))
				when 7 then cast(D.schAddr2 as nvarchar(50))
				when 8 then cast(D.schAddr3 as nvarchar(50))
				when 9 then cast(D.schAddr4 as nvarchar(50))
				when 10 then cast(D.schPh1 as nvarchar(50))
				when 11 then cast(D.schPh2 as nvarchar(50))
				when 12 then cast(D.schFax as nvarchar(50))
				when 13 then cast(D.schEmail as nvarchar(50))
				when 14 then cast(D.schWWW as nvarchar(50))
				when 15 then cast(D.schElectN as nvarchar(50))
				when 16 then cast(D.schElectL as nvarchar(50))
				when 17 then cast(D.schAuth as nvarchar(50))
				when 18 then cast(D.schLang as nvarchar(50))
				when 19 then cast(D.schLandOwner as nvarchar(50))
				when 20 then cast(D.schLandUse as nvarchar(50))
				when 21 then cast(D.schLandUseExpiry as nvarchar(50))
				when 22 then cast(D.schEst as nvarchar(50))
				when 23 then cast(D.schEstBy as nvarchar(50))
				when 24 then cast(D.schClosed as nvarchar(50))
				when 25 then cast(D.schCloseReason as nvarchar(50))
				when 26 then cast(D.schRegStatus as nvarchar(50))
				when 27 then cast(D.schReg as nvarchar(50))
				when 28 then cast(D.schRegYear as nvarchar(50))
				when 29 then cast(D.schRegDate as nvarchar(50))
				when 30 then cast(D.schRegStatusDate as nvarchar(50))
				when 31 then cast(D.schXtrnID as nvarchar(50))
				when 32 then cast(D.schClass as nvarchar(50))
				when 33 then cast(D.schXNo as nvarchar(50))
				when 34 then cast(D.schpayptCode as nvarchar(50))
				when 35 then cast(D.schClosedLimit as nvarchar(50))
				when 36 then cast(D.schOrgUnitNumber as nvarchar(50))
				when 37 then cast(D.schGLSalaries as nvarchar(50))
				when 38 then cast(D.schParent as nvarchar(50))
				when 39 then cast(D.schDormant as nvarchar(50))


			 end DValue
			 
			, case num 
				when 1 then cast(I.schNo as nvarchar(50))
				when 2 then cast(I.schName as nvarchar(50))
				when 3 then cast(I.schVillage as nvarchar(50))
				when 4 then cast(I.iCode as nvarchar(50))
				when 5 then cast(I.schType as nvarchar(50))
				when 6 then cast(I.schAddr1 as nvarchar(50))
				when 7 then cast(I.schAddr2 as nvarchar(50))
				when 8 then cast(I.schAddr3 as nvarchar(50))
				when 9 then cast(I.schAddr4 as nvarchar(50))
				when 10 then cast(I.schPh1 as nvarchar(50))
				when 11 then cast(I.schPh2 as nvarchar(50))
				when 12 then cast(I.schFax as nvarchar(50))
				when 13 then cast(I.schEmail as nvarchar(50))
				when 14 then cast(I.schWWW as nvarchar(50))
				when 15 then cast(I.schElectN as nvarchar(50))
				when 16 then cast(I.schElectL as nvarchar(50))
				when 17 then cast(I.schAuth as nvarchar(50))
				when 18 then cast(I.schLang as nvarchar(50))
				when 19 then cast(I.schLandOwner as nvarchar(50))
				when 20 then cast(I.schLandUse as nvarchar(50))
				when 21 then cast(I.schLandUseExpiry as nvarchar(50))
				when 22 then cast(I.schEst as nvarchar(50))
				when 23 then cast(I.schEstBy as nvarchar(50))
				when 24 then cast(I.schClosed as nvarchar(50))
				when 25 then cast(I.schCloseReason as nvarchar(50))
				when 26 then cast(I.schRegStatus as nvarchar(50))
				when 27 then cast(I.schReg as nvarchar(50))
				when 28 then cast(I.schRegYear as nvarchar(50))
				when 29 then cast(I.schRegDate as nvarchar(50))
				when 30 then cast(I.schRegStatusDate as nvarchar(50))
				when 31 then cast(I.schXtrnID as nvarchar(50))
				when 32 then cast(I.schClass as nvarchar(50))
				when 33 then cast(I.schXNo as nvarchar(50))
				when 34 then cast(I.schpayptCode as nvarchar(50))
				when 35 then cast(I.schClosedLimit as nvarchar(50))
				when 36 then cast(I.schOrgUnitNumber as nvarchar(50))
				when 37 then cast(I.schGLSalaries as nvarchar(50))
				when 38 then cast(I.schParent as nvarchar(50))
				when 39 then cast(I.schDormant as nvarchar(50))

			 end [IValue]
		FROM INSERTED I 
		INNER JOIN DELETED D
			ON I.schNo = D.schNo
		CROSS JOIN metaNumbers
		WHERE num between 1 and 39
		) Edits
		ON Edits.schNo = Row.arowKeyS
		WHERE Row.auditID = @AuditID
		AND isnull(DVAlue,'') <> isnull(IValue,'')

		
		-- update the last edited field	
		UPDATE Schools
			SET pEditDateTime = aLog.auditDateTime
				, pEditUser = alog.auditUser
		FROM INSERTED I
			CROSS JOIN Audit.auditLog alog
		WHERE Schools.schNo = I.schNo
		AND aLog.auditID = @auditID
	
END
GO
ALTER TABLE [dbo].[Schools] ENABLE TRIGGER [Schools_AuditUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 5 2010
-- Description:	Maintain SchoolYearHistory when Schools changes
-- =============================================
CREATE TRIGGER [dbo].[Schools_RelatedData] 
   ON  [dbo].[Schools] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ActiveYear int
	declare @MaxYear int
	declare @StartHistory int
	declare @LastSurvey int	
	
	select @ActiveYear = common.ActiveSurveyYear()
	-- the reason for this is:
	-- when the new survey year is created, any provisional settings in SchoolYearHistory for the new year
	-- (e.g. created by Establishment0 are applied to the Schools table making them current.
	-- These could then be restrosecptively applied to the current year records
	select @StartHistory = min(svyYear)
	, @LastSurvey = max(svyYear)
	from Survey
	
	select @MaxYear = max(syYear)		-- there may be years in history thatare not in survey
	from SchoolYearHistory
	
	-- if its an insert
	-- the range of years to add should be from 
	-- min(Established Year, current Year)
	-- to the maximum year represented in SurveyYear History
	-- This will add them to the establishment plan and survey control
	
	-- note this query will add any needed records if an established date is changed
	INSERT INTO SchoolYearHistory
	( syYear
		, schNo
		, systCode
		, syAuth
		, syParent
		, syDormant

	)
	SELECT num
	, INSERTED.schNo
	, INSERTED.schType
	, INSERTED.schAuth
	, INSERTED.schParent
	, INSERTED.schDormant
	from INSERTED
		
	INNER JOIN metaNumbers
	ON num between
		case when INSERTED.schEst < @StartHistory then @StartHistory else INSERTED.schEst end
		and 
		case when INSERTED.schClosed = 0 then @MaxYear  
			 when INSERTED.schClosed > @MaxYear then @MaxYear
				else INSERTED.schClosed - 1
		end 
	LEFT JOIN SchoolYearHistory
		ON SchoolYearHistory.schNo = INSERTED.schNo
		AND metaNumbers.num = SchoolYearHistory.syYear
	WHERE schoolYearHistory.syID is null
	
	-- similarly for updates, we should clear history records outside the est/closed range
	DELETE FROM SchoolYearHistory
	FROM SchoolYearHistory 
	INNER JOIN INSERTED
		ON SchoolYearHistory.schNo = INSERTED.schNo
	INNER JOIN DELETED
		ON INSERTED.schNo = DELETED.schNo
	WHERE SchoolYearHistory.syYear < INSERTED.schEst
		OR
		( INSERTED. schClosed <> 0 AND SchoolYearHistory.syYear > INSERTED.schClosed)
		 
	

-- update the current year school year history
-- it's always the same as the school record

	UPDATE SchoolYearHistory
		SET systCode = INSERTED.schtype 
			
			, syAuth =INSERTED.schAuth
			, syParent = INSERTED.schParent 
			 
			, syDormant = INSERTED.schDormant 
	from SchoolYearHistory
	INNER JOIN INSERTED
		ON SchoolYearHistory.schNo = INSERTED.schNo
	INNER JOIN DELETED
		ON INSERTED.schNo = DELETED.schNo
	WHERE SchoolYearHistory.syYear = @ActiveYear

-- update all future school history from the current year
-- only change values that were the same as on the schools
-- ie if there was an override in the future, preserve it

	UPDATE SchoolYearHistory
		SET systCode =
			case when systCode = deleted.schType then INSERTED.schtype else systCode end
			
			, syAuth = 
			 case when syAuth = deleted.schAuth then INSERTED.schAuth else syAuth end
			, syParent =
			 case when isnull(syParent,'') = isnull(DELETED.schParent,'') then INSERTED.schParent else syParent end
			 
			, syDormant = 
				case when syDormant = DELETED.schDormant then INSERTED.schDormant else syDormant end
	from SchoolYearHistory
	INNER JOIN INSERTED
		ON SchoolYearHistory.schNo = INSERTED.schNo
	INNER JOIN DELETED
		ON INSERTED.schNo = DELETED.schNo
	WHERE SchoolYearHistory.syYear between @ActiveYear + 1 and @MaxYear

END
GO
ALTER TABLE [dbo].[Schools] ENABLE TRIGGER [Schools_RelatedData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[Schools_Validations] 
   ON  [dbo].[Schools] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin try
	declare @count int
	
	-- cannot have a exension registration status without a parent school
	
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus in ('PX','X')
	AND schParent is null
	
	if @count > 0 begin
		RAISERROR('<ValidationError field="schParent">
					A school with registration status (PX,X) must specify a parent school.
					</ValidationError>', 16,1)
	end 
	
	
	--	-- cannot have a  registration status with a parent school
	
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus in ('PR','R')
	AND schParent is not null
	
	if @count > 0 begin
		RAISERROR('<ValidationError field="schRegStatus">
					An extension school can only have registration status PX or X.
					</ValidationError>', 16,1)
	end 
	
	-- and while we're at it, the parent school cannot be an extension
	-- 

	Select @count = count(*)
	FROM INSERTED I
	WHERE schParent is not null
	and schParent in (select schNo from Schools WHERE schParent is not null)
	
  	if @count > 0 begin
		RAISERROR('<ValidationError field="schParent">
					The parent school cannot be an Extension.
					</ValidationError>', 16,1)
	end 

	---.... the current school cannot be an extension if it is a parent
	
	Select @count = count(*)
	FROM INSERTED I 
	WHERE schParent is not null
	and schNo in 
	(select schParent from Schools)

	
  	if @count > 0 begin
		RAISERROR('<ValidationError field="schParent">
					A school with Extensions cannot have a Parent.
					</ValidationError>', 16,1)
	end 	
	

	-- if there is a parent, it must be the same authority as this school
		Select @count = count(*)
	FROM INSERTED I 
	INNER JOIN Schools P
		on I.schParent = P.schNo
	WHERE I.schAuth <> p.schAuth

  	if @count > 0 begin
		RAISERROR('<ValidationError field="schParent">
					A school and its parent must belong to the same Authority.
					</ValidationError>', 16,1)
	end 	
	
	-- cannot have a registation status without a registration date
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus is not null and schRegStatusDate is null
	
	if @count > 0 begin
		RAISERROR('<ValidationError field="schRegStatusDate">
					A school with registration status must specify the registration status date.
					</ValidationError>', 16,1)
	end 	

	-- cannot have a registation date without a registration status
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus is null and schRegStatusDate is not null
	
	if @count > 0 begin
		RAISERROR('<ValidationError field="schRegStatusDate">
					A school with no registration status cannot have a registration status date.
					</ValidationError>', 16,1)
	end 
	-- registered status R needs registration number
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus = 'R' and schReg is null
	
	if @count > 0 begin
		RAISERROR('<ValidationError field="schReg">
					A school that is Registered must specify the Registration No.
					</ValidationError>', 16,1)
	end 	
	
	
	
	end try	
	/****************************************************************
	Generic catch block
	****************************************************************/
	begin catch
		
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000), 
			@ErrorSeverity INT, 
			@ErrorState INT; 
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		rollback transaction


		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 

	end catch

END
GO
ALTER TABLE [dbo].[Schools] ENABLE TRIGGER [Schools_Validations]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Master table of schools. Defines the SchNo, and stores the key classifiers such as school type, authority, island.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Schools'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Schools'
GO

