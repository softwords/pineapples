SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[AccreditationStandard](
	[inspID] [int] NOT NULL,
	[CategoryType] [nvarchar](200) NOT NULL,
	[standard] [nvarchar](50) NULL,
	[Score] [int] NULL,
	[MaxScore] [int] NULL,
	[ScorePerc] [decimal](3, 0) NULL,
	[Result] [nvarchar](50) NULL
) ON [PRIMARY]
GO

