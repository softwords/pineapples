SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISTCourse](
	[istcCode] [nvarchar](20) NOT NULL,
	[istcDescription] [nvarchar](50) NULL,
	[istcDescriptionL1] [nvarchar](50) NULL,
	[istcDescriptionL2] [nvarchar](50) NULL,
	[istgCode] [nvarchar](10) NULL,
 CONSTRAINT [ISTCourse_PK] PRIMARY KEY NONCLUSTERED 
(
	[istcCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ISTCourse] TO [pISTRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISTCourse] TO [pISTWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISTCourse] TO [pISTWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISTCourse] TO [pISTWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISTCourse] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [ISTCourseGroupISTCourse] ON [dbo].[ISTCourse]
(
	[istgCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ISTCourse]  WITH CHECK ADD  CONSTRAINT [ISTCourseGroupISTCourse] FOREIGN KEY([istgCode])
REFERENCES [dbo].[ISTCourseGroup] ([istgCode])
GO
ALTER TABLE [dbo].[ISTCourse] CHECK CONSTRAINT [ISTCourseGroupISTCourse]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ISTCourse defines Inservice Training Courses.  ISTSession is an instance of an ISTCourse, starting on a specfic date, specfic venue. 
ISTCourse is therefore a foreign key in ISTSession.

Pupil are Waitlisted for a course, then enrolled in a specific session of that course.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourse'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'Linked table/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourse'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'IST Courses are defined through Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourse'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'IST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourse'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inservice Training' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourse'
GO

