SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamCandidateResults](
	[excrID] [int] IDENTITY(1,1) NOT NULL,
	[excID] [uniqueidentifier] NOT NULL,
	[excrLevel] [int] NULL,
	[exbnchID] [int] NULL,
	[exstdID] [int] NULL,
	[exindID] [int] NULL,
	[exCode] [nvarchar](10) NULL,
	[excrLevelCount] [int] NULL,
	[excrWeight] [decimal](7, 6) NULL,
	[excrCandidateCount] [int] NULL,
 CONSTRAINT [PK_ExamCandidateResults] PRIMARY KEY CLUSTERED 
(
	[excrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ExamCandidateResults_excID] ON [dbo].[ExamCandidateResults]
(
	[excID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExamCandidateResults]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidateResults_ExamBenchmarks] FOREIGN KEY([exbnchID])
REFERENCES [dbo].[ExamBenchmarks] ([exbnchID])
GO
ALTER TABLE [dbo].[ExamCandidateResults] CHECK CONSTRAINT [FK_ExamCandidateResults_ExamBenchmarks]
GO
ALTER TABLE [dbo].[ExamCandidateResults]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidateResults_ExamCandidate] FOREIGN KEY([excID])
REFERENCES [dbo].[ExamCandidates] ([excID])
GO
ALTER TABLE [dbo].[ExamCandidateResults] CHECK CONSTRAINT [FK_ExamCandidateResults_ExamCandidate]
GO
ALTER TABLE [dbo].[ExamCandidateResults]  WITH CHECK ADD  CONSTRAINT [FK_ExamCandidateResults_ExamIndicators] FOREIGN KEY([exindID])
REFERENCES [dbo].[ExamIndicators] ([exindID])
GO
ALTER TABLE [dbo].[ExamCandidateResults] CHECK CONSTRAINT [FK_ExamCandidateResults_ExamIndicators]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When NOT NULL, flags that this record is the overall result for the candidate for the whole exam' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamCandidateResults', @level2type=N'COLUMN',@level2name=N'exCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Index on the candidate ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamCandidateResults', @level2type=N'INDEX',@level2name=N'IX_ExamCandidateResults_excID'
GO

