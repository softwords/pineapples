SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolAccreditation](
	[saID] [int] NOT NULL,
	[saL1] [int] NULL,
	[saL2] [int] NULL,
	[saL3] [int] NULL,
	[saL4] [int] NULL,
	[saT1] [int] NULL,
	[saT2] [int] NULL,
	[saT3] [int] NULL,
	[saT4] [int] NULL,
	[saD1] [int] NULL,
	[saD2] [int] NULL,
	[saD3] [int] NULL,
	[saD4] [int] NULL,
	[saN1] [int] NULL,
	[saN2] [int] NULL,
	[saN3] [int] NULL,
	[saN4] [int] NULL,
	[saF1] [int] NULL,
	[saF2] [int] NULL,
	[saF3] [int] NULL,
	[saF4] [int] NULL,
	[saS1] [int] NULL,
	[saS2] [int] NULL,
	[saS3] [int] NULL,
	[saS4] [int] NULL,
	[saCO1] [int] NULL,
	[saCO2] [int] NULL,
	[saLT1] [int] NULL,
	[saLT2] [int] NULL,
	[saLT3] [int] NULL,
	[saLT4] [int] NULL,
	[saT] [int] NULL,
	[saSchLevel] [varchar](10) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NOT NULL,
	[pCreateTag] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK__SchoolAc__6EB49CCEBCD52C2D] PRIMARY KEY CLUSTERED 
(
	[saID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolAccreditation] ADD  CONSTRAINT [DF_SchoolAccreditation_pCreateTag]  DEFAULT (newid()) FOR [pCreateTag]
GO
ALTER TABLE [dbo].[SchoolAccreditation]  WITH CHECK ADD  CONSTRAINT [FK__SchoolAccr__saID__27EECCF7] FOREIGN KEY([saID])
REFERENCES [dbo].[SchoolInspection] ([inspID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolAccreditation] CHECK CONSTRAINT [FK__SchoolAccr__saID__27EECCF7]
GO

