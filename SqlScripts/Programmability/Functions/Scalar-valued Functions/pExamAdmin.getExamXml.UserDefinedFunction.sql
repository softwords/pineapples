SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 06/09/2022
-- Description:	Reconstruct the ListObject Xml used to create an exam (ie passed to LoadSoeExam or LoadExam)
--              This illustrates a general technique for combin an Xml column into a single Xml object
-- =============================================
CREATE FUNCTION [pExamAdmin].[getExamXml]
(
	@examID int
)
RETURNS xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	declare @xml xml
	--Select @xml =
	return
	(
	Select excData.query('.')
	FROM ExamCandidates
	WHERE exID = @examID
	ORDER BY excData.value('(//row/@Index)[1]', 'int')
	FOR XML PATH(''), ROOT('ListObject')
	)
END
GO

