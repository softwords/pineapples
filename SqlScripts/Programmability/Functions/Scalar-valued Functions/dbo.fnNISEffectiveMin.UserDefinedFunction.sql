SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 12 2007
-- Description:	Calculate the effective minimum for a standard using prorate calcs
-- =============================================
CREATE FUNCTION [dbo].[fnNISEffectiveMin]
(
	-- Add the parameters for the function here
	@Min float,
	@ProRateP float,
	@ProRateT float,
	@ProRateCeiling float,
	@NumPupils int,
	@NumTeachers int

)
RETURNS int
AS
BEGIN
declare @ProRate float -- raw prorata per pupils and teachers
select @ProRate =
	isnull(@ProRateP,0) * isnull(@NumPupils,0) + isnull(@ProRateT,0)*isnull(@NumTeachers,0)

if @ProRateCeiling < @ProRate
	-- if not null and less than the calculated prorate, use the ceiling
	select @ProRate = @ProRateCeiling

if @Min > @ProRate
	return ceiling(@Min)

return ceiling(@ProRate)

END
GO
GRANT EXECUTE ON [dbo].[fnNISEffectiveMin] TO [public] AS [dbo]
GO

