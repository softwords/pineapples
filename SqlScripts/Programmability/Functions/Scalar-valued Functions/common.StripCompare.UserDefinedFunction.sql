SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [common].[StripCompare]
(
    @S nvarchar(max),
	@t nvarchar(max),
    @MatchExpression nvarchar(255) = ' "?._-'
)
RETURNS int
AS
BEGIN
    Select @s = common.stripCharacters(@s, @MatchExpression)
	, @t = common.stripCharacters(@t, @MatchExpression)

	return case when @s = @t then 1 else 0 end


END
GO

