SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 9 2011
-- Description:	Helper function for displaying in a PDF
-- =============================================
CREATE FUNCTION [common].[fromPDFChkSet]
(
	-- Add the parameters for the function here
	@Value nvarchar(20)


)
RETURNS nvarchar(20)
AS
BEGIN

	-- Return the result of the function
	RETURN Nullif(@value,'Off')
END
GO

