SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2021
-- Description:	Get the school year string for a date
-- =============================================
CREATE FUNCTION [common].[SchoolYearStr]
(
	@dt datetime, @startDate date = null
)
RETURNS nvarchar(50)
AS
BEGIN
declare @fy int = common.schoolYearInt(@dt, @startDate)

return common.schoolYearFormat(@fy)


END
GO

