SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 9 6 2021
-- Description:	This function returns a dropdown list of teacher roles
-- in CENODPF items text format.
-- The selected value is the code
--
-- =============================================
CREATE FUNCTION [cenopdf].[TeacherRoleItems]
(
	@schooltype nvarchar(10)
)

RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result nvarchar(max)


declare @dt nvarchar(max)  = ' |00|*' + char(13) + char(10)


Select @dt = @dt + codeDescription + '|' + L.codeCode + '|'  + char(13) + char(10)
FRom lkpTeacherRole	L
INNER JOIN metaSchoolTypeTeacherRoleMap M
	ON L.codeCode = M.codeCode
	AND M.stCode = @schoolType
	AND M.typroleSort <= 30			-- kludge : data should be cleaned somehow
ORDER BY typRoleSort

RETURN @dt
END
GO

