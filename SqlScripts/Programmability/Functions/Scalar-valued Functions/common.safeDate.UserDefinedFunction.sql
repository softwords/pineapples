SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 5 2022
-- Description:	return a date value if possible from a string
-- supports: 5 digit 'Excel' format (ie days since 0/1/1900 as stored in Excel)
--				6 or 8 digit yyyymmdd yymmdd
--			other date formats as supported by convert
--			will test for dd and mm reversed from default format
-- if conversion not possible, returns Null
-- =============================================
CREATE FUNCTION [common].[safeDate]
(
	-- Add the parameters for the function here
	@str nvarchar(20)
)
RETURNS date
AS
BEGIN
declare @d date

Select @str = nullif(ltrim(rtrim(@str)),'')
if @str is null return null

if isnumeric(@str) = 1 begin
	if len(@str) < 6 begin
		-- deal with an Excel-style numeric - number of days since 0/1/1900
		declare @i int = convert(int, @str)
		return cast(cast(@i- 2 as datetime) as date)
	end
	else begin
		-- 6 and 8 character numerics
		Select @d =
			coalesce(try_convert(date, @str, 112), try_convert(date, @str, 12))			--yyyymmdd  yymmdd
		if @d is not null return @d
	end
end

select @str = replace(replace(replace(@str, '/','-'),'.','-'),'\','-')
select @d = try_convert(date, @str)

if @d is not null return @d

-- is it unambiguous m and d?
declare @dmy int =isdate('31/01/2020')

if @dmy = 1 begin
	-- sql recognises AU date format (ddmmyyyy) so tyhese would already have been converted
	-- try US formats
	Select @d =
		coalesce(try_convert(date, @str, 110), try_convert(date, @str, 10))  -- mm-dd-yyyy, mm-dd-yy
	if @d is not null return @d
end

if @dmy = 0 begin
	-- sql does not recognise AU date format (ddmmyyyy)
	-- try AU formats explicitly
	Select @d =
		coalesce(try_convert(date, @str, 105), try_convert(date, @str, 5))  -- dd-mm-yyyy, dd-mm-yy
	if @d is not null return @d
end
return null
END
GO

