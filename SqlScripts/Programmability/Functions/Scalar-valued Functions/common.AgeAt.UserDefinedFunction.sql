SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 10 2017
-- Description:	Calculate Age in whole years, frm a dat of birth
-- and a census ( comparison date)
-- =============================================
CREATE FUNCTION [common].[AgeAt]
(
	-- Add the parameters for the function here
	@dob date
	, @referencedate date
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @age int;

	If @Dob is null or @referencedate is null or @referencedate < @dob
		return null

	SELECT @age = case
				when month(@referencedate) > month(@dob) then year(@referencedate) - year(@dob)
				when month(@referencedate) < month(@dob) then year(@referencedate) - year(@dob) - 1
				when day(@referencedate) >= day(@dob) then year(@referencedate) - year(@dob)
				else year(@referencedate) - year(@dob) - 1
			end


	RETURN @age
END
GO

