SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 07/09/2022
-- Description:	Reconstruct the ListObject Xml used to create staff (ie constructed from the census workbook
--				and apssed to pSurveyOps.censusLoadStaff)
--              This illustrates a general technique for combin an Xml column into a single Xml object
-- =============================================
CREATE FUNCTION [pSurveyAdmin].[getStaffXml]
(
	@source uniqueidentifier
	, @year int
	, @state nvarchar(100)
)
RETURNS xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	declare @yrstr nvarchar(20)
	declare @xml xml

	select @yrstr = 'SY' + convert(nvarchar(4), @year - 1) + '-' + convert(nvarchar(4), @year)
	Select @xml =
	(

	Select tcheData.query('.')
		FROM pTeacherRead.TeacherSurveyV TS

		WHERE tcheSource = @source
		ORDER BY tcheData.value('(//row/@Index)[1]', 'int')
		FOR XML PATH(''), ROOT('ListObject')
	);

	SET @xml.modify(
	' insert (
           attribute schoolYear {sql:variable("@yrstr") },
           attribute state {sql:variable("@state")}
        )
into (/ListObject)[1] ');

return @xml
END
GO

