SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: June 2019
-- Description:	Return a value from a partition set in the Partititions table
-- @ptSet - the name of the set
-- @numerator / @denominator * @scale the value to look for
-- to search on a simple float value , pass it as numerator and omit
-- denominator and scale
-- =============================================
CREATE FUNCTION [dbo].[PartitionValue]
(
	@ptSet nvarchar(20)
	, @numerator float
	, @denominator float = 1
	, @scale float = 1
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	declare @result float
	if @denominator is null
		return null
	if @scale is null
		select @scale = 1
	Select @result = ptValue
	from Partitions P
	WHERE ptSet = @ptSet
	AND (@numerator * @scale) / @denominator between ptMin and ptMax

	RETURN @Result

END
GO

