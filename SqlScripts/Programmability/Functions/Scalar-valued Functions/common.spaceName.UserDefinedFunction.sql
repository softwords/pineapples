SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 08 2022
-- Description:	space out a name that has been incorrectly concatenated to leave no spaces between given/middle/family
-- =============================================
CREATE FUNCTION [common].[spaceName]
(
	-- Add the parameters for the function here
	@n nvarchar(128)
)
RETURNS nvarchar(128)
AS
BEGIN
	-- Declare the return variable here
declare @out nvarchar(100) = ''
declare @l int = len(@n)

declare @i int = 1

declare @ch nvarchar(1)
while @i <= @l begin

	Select @ch = substring(@n,@i,1)
	if (ascii(@ch) between ascii('A') and ascii('Z')) and @i > 1 select @out = @out + ' '
	Select @out = @out + @ch
	select @i = @i + 1
end
return @out

END
GO

