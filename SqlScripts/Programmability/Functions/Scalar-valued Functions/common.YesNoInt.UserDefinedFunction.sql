SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [common].[YesNoInt]
(
	 @value nvarchar(20)
)
RETURNS int
AS
BEGIN

	return
	case @value
		when 'Yes' then 1
		when 'No' then 0
		when '1' then 1
		when '-1' then 1
		when '0' then 0
	end

END
GO

