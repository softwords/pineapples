SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 6 2021
-- Description:	Create an xfdf output, by wrapping a fields node
-- This applies the xfdf namespace to the root xfdf node
-- and does this while avoiding the generation of xmlns='' upstream in the xml
-- =============================================
CREATE FUNCTION [common].[xfdf]
(
	@fieldsNode xml			-- <fields>....</for the xfdf
)
RETURNS xml
AS
BEGIN

return
(
Select * from
(
Select 1 Tag, null Parent
, 'http://ns.adobe.com/xfdf/' [xfdf!1!xmlns]
, null [fields!2!!XMLTEXT]
UNION ALL
SELECT 2, 1, null, @fieldsNode
) U
foR XML Explicit
)

END
GO

