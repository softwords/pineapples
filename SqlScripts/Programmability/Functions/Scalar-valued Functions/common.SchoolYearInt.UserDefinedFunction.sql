SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2021
-- Description:	Get the school year string for a date
-- @dt input date
-- @StartDate date of start of school year. If Null, uses sysParamValue SCHOOLYEAR_STARTDATE
--	year is ignored
-- If SCHOOLYEAR_STARTDATE is not defined in SysParams, uses '2000-01-01' ie school year = calendar year
-- =============================================
CREATE FUNCTION [common].[SchoolYearInt]
(
	@dt datetime, @startDate date = null
)
RETURNS int
AS
BEGIN
declare @yr int = year(@dt)
if @startDate is null
	select @startDate = common.SchoolYearStartDate()

if month(@startDate) = 1 and day(@startDate) = 1 begin
	return @yr
end

declare @actStart date = datefromparts(@yr, month(@startDate), day(@startDate))

declare @fy int = case when @dt >= @actStart then year(dateadd(d,- 1, @actStart)) + 1 else @yr end

return @fy


END
GO

