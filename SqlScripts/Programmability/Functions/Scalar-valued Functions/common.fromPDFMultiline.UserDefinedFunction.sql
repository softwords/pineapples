SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 28 12 2011
-- Description:	Turn isolated line feeds into CRLF
-- =============================================
CREATE FUNCTION [common].[fromPDFMultiline]
(
	@in nvarchar(max)

)
RETURNS nvarchar(max)
AS
BEGIN
if (@in is null)
	RETURN NULL

if (CHARINDEX(CHAR(10), @in) = 0)
	RETURN @in

declare @crlf nvarchar(2) = nchar(13) + NCHAR(10)
select @in =  REPLACE(
		REPLACE(
			REPLACE(@in, @crlf, nchar(1))
			, NCHAR(10), NCHAR(1))

		, NCHAR(1), @crlf)

return common.removeAllTrailingAndLeading(@in, @crlf)

END
GO

