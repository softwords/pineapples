SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 9 2022
-- Description:	From a string of years e.g. '2019 2020 2022 2024 2025 '
-- produce a formatted list for display e.g. '2019-2020, 2020, 2024-2025'
-- =============================================
CREATE FUNCTION [common].[YearsDisplay]
(
	-- Add the parameters for the function here
	@yrs nvarchar(100)
)
RETURNS nvarchar(100)
AS
BEGIN
	-- Declare the return variable here

	if (@yrs is null)
		return null

	declare @y TABLE
	(
		id int identity
		, yr int
	)

	declare @max int
	declare @idx int = 1
	select @max = len(@yrs)
	while @idx <=@max begin
		INSERT INTO @y
		(yr)
		Select convert(int,substring(@yrs, @idx, 4))
		select @idx = @idx + 5
	end
	declare @current nvarchar(4) = ''
	declare @prev nvarchar(4)

	declare @result nvarchar(100) = ''
	declare @insequence int = 0;

	Select @idx = 1
	select @max = max(id) from @y
	if (@max=1)
		return ltrim(rtrim(@yrs))
	while @idx <= @max begin

		select @prev = @current
		select @current = yr from @y where id = @idx
		--if @idx = 1 begin
		--	--Select @result = @result + @current
		--end
		if  @idx > 1 begin

			if (@current = @prev + 1) begin -- sequential now
				if @inSequence = 0 begin
					select @result = @result + @prev + '-'
					, @inSequence = 1
				end
			end
			else begin -- no longer in a sequence
				select @result = @result + @prev + ', '
				select @insequence = 0


			end
			if @idx = @max
				select @result = @result  + @current
		end

		Select @idx = @idx + 1

	end
	return @result

END
GO

