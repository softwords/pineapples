SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [common].[prepSearchString]
(
	@searchstring nvarchar(200)
	, @append int = 1			-- put % on the end
	, @prepend int = 0			-- put % at the start
								-- only f user has NOt specified their own wild cards
)
RETURNS nvarchar(200)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result nvarchar(200)


	Select @result = replace(replace(@searchstring,'*','%'),'?','_')

	if charindex('%', @result) = 0 and charindex('_',@result) = 0 begin
		Select @result =
		case when @append = 0 then
			case when @prepend = 0 then @result else '%' + @result end
			else
				case when @prepend = 0 then @result + '%' else '%' + @result + '%' end
		end
	end

	-- Return the result of the function
	RETURN @result

END
GO

