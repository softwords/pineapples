SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 28 12 2011
-- Description:	Turn isolated line feeds into CRLF
-- =============================================
CREATE FUNCTION [common].[fromPDFMultilineNum]
(
	@in nvarchar(max)

)
RETURNS decimal(12,4)
AS
BEGIN
if (@in is null)
	RETURN NULL

declare @num decimal(12,4)

Select @in = common.fromPDFMultiline(@in)

if (isnumeric(@in) = 1)
	Select @num = convert(decimal(12,4),@in)


return @num

END
GO

