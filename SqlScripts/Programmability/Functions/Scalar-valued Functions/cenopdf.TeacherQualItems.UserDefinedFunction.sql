SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 10 2012
-- Description:	This function returns a dropdown list of teacher qualifications for a pdfform
-- in CENODPF items text format.
-- The selected value is the code. Used by NewTeachers and ExistingStaff
-- =============================================
CREATE FUNCTION [cenopdf].[TeacherQualItems]
(
	@EdOnly int = 0
)

RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result nvarchar(max)

declare @dt nvarchar(max)  = ' |00|*' + char(13) + char(10)

Select @dt = @dt + codeGroup + '|' + codeCode + '|'  + char(13) + char(10)
FRom lkpTeacherqual
-- not sure about codeGroup 'Other'
--where (@edOnly = 0 or codeTeach = 1 ) ANd codeGroup is not null and codeGroup <> 'OTHER'
where (@edOnly = codeTeach or @edOnly is null) and codeGroup = codeCode
ORDER BY codeSeq


RETURN @dt
END
GO

