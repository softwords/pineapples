SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 8 2019
-- Description:	Count the number of inputs n1 - n24 that are equal to @value
-- this is specifically for getting tallies on school accreditations
-- used in pinspectionRead.SCHOOL_ACCREDITATION
-- In the interest of multi-lingual punning, this should have been a View. (groan)
-- =============================================
CREATE FUNCTION [common].[tally](
	-- Add the parameters for the function here
	@value int
, @n1 int
, @n2 int
, @n3 int
, @n4 int
, @n5 int
, @n6 int
, @n7 int
, @n8 int
, @n9 int
, @n10 int
, @n11 int
, @n12 int
, @n13 int
, @n14 int
, @n15 int
, @n16 int
, @n17 int
, @n18 int
, @n19 int
, @n20 int
, @n21 int
, @n22 int
, @n23 int
, @n24 int
, @n25 int
, @n26 int


)
RETURNS int
AS
BEGIN
	declare @result int = 0

	if @n1 = @value set @result = @result + 1
	if @n2 = @value set @result = @result + 1
	if @n3 = @value set @result = @result + 1
	if @n4 = @value set @result = @result + 1
	if @n5 = @value set @result = @result + 1
	if @n6 = @value set @result = @result + 1
	if @n7 = @value set @result = @result + 1
	if @n8 = @value set @result = @result + 1
	if @n9 = @value set @result = @result + 1
	if @n10 = @value set @result = @result + 1

	if @n11 = @value set @result = @result + 1
	if @n12 = @value set @result = @result + 1
	if @n13 = @value set @result = @result + 1
	if @n14 = @value set @result = @result + 1
	if @n15 = @value set @result = @result + 1
	if @n16 = @value set @result = @result + 1
	if @n17 = @value set @result = @result + 1
	if @n18 = @value set @result = @result + 1
	if @n19 = @value set @result = @result + 1
	if @n20 = @value set @result = @result + 1

	if @n21 = @value set @result = @result + 1
	if @n22 = @value set @result = @result + 1
	if @n23 = @value set @result = @result + 1
	if @n24 = @value set @result = @result + 1
	if @n25 = @value set @result = @result + 1
	if @n26 = @value set @result = @result + 1
	return @result
END
GO

