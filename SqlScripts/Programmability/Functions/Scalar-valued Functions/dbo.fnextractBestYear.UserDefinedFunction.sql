SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[fnextractBestYear]
(
	-- Add the parameters for the function here
	@BestData nchar(50)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here

	-- Return the result of the function
	RETURN cast( substring(@BestData,7,4) as int)

END
GO
GRANT EXECUTE ON [dbo].[fnextractBestYear] TO [public] AS [dbo]
GO

