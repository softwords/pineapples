SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 8 2021
-- Description:	Convert a string to a numeric, returning null if the
-- input is not numeric.
-- This is intended as a substitute for try_convert built in function,
-- which requires database compatibility level 110.
-- In time we should move to 110 (Sql Server 2012), then this User function
-- can be replaced with the more general try_convert
-- Change History: 26 9 2022
-- This function is now expanded to provided a cleanup of non -numeric chars,
-- and to convert a decimal to int by rounding if required
-- =============================================
CREATE FUNCTION [common].[try_convert_int]
(
		@input nvarchar(200)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	declare @result int = null
	declare @tmp nvarchar(200) = ''

	if @input in ('nil','no','none')
		RETURN 0

	SELECT @tmp = @tmp + CASE WHEN substring(@input,Num,1) LIKE '[0-9|.]'
						THEN substring(@input,Num,1)
			ELSE ''
		END
	FROM metaNumbers WHERE Num <= len(@input)


if (IsNumeric(@tmp) = 1)
	SELECT @result = round(try_convert(decimal(17,5), @tmp),0)

	RETURN @Result

END
GO

