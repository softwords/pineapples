SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 1 2017
-- Description:	Return the svyYear formatted according to sysParam
-- Uses the sysParam value SURVEY_YEAR_FORMAT as a template
-- =============================================
CREATE FUNCTION [common].[surveyYearFormat]
(
	-- Add the parameters for the function here
	@value int
)
returns nvarchar(20)
AS
BEGIN
	return common.schoolYearFormat(@value)
END
GO

