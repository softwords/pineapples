SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 6 2021
-- Description:	Get an xml represenation of a code table
--
-- =============================================
CREATE FUNCTION [cenopdf].[kvxml]
(
	-- Add the parameters for the function here
	@codeTableName nvarchar(100)
)
RETURNS xml
AS
BEGIN

	declare @xml xml

	if @codeTableName = 'lkpDisabilities' begin
		Select @xml =
			(
			Select Row_number() OVER (ORDER BY codeSeq) - 1 [@idx]
			, codeCode k
			, codeDescription v

			From lkpDisabilities
			FOR XML PATH('row'), root('kv')
			)
	end

	RETURN @xml

END
GO

