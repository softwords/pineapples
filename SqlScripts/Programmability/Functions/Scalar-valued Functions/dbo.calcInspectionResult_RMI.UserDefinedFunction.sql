SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: Aug 2019
-- Description:	Implementation of RMI accreditation scoring formula
-- This formula is based on 'tallies' of the number of criteria
-- =============================================
CREATE FUNCTION [dbo].[calcInspectionResult_RMI]
(
	@inspID int
)
RETURNS nvarchar(50)
AS
BEGIN

declare @l1 int, @l2 int, @l3 int, @l4 int
declare @result nvarchar(20)


-- get the level tallies - ie the number of criteria that have sored at each level
Select
@l1 = Level1
, @l2 = level2
, @l3 = level3
, @l4 = level4
FROM pInspectionREad.SCHOOL_ACCREDITATION
WHERE inspID = @inspID

-- if 4 or more level 1, automatically level 1
if @l1 >= 4 begin
	select @result = 'Level 1'
end
else begin
-- otherwise, its the biggest of level tallies for levels 2, 3, and 4
-- ties are decided in favour of the higher option
-- use a sql sort to work this out....
	declare @t TABLE
	(
		lvl nvarchar(10)
		, tally int
	)
	INSERT INTO @t
	VALUES ('Level 2', @l2)
	, ('Level 3', @l3)
	, ('Level 4', @l4)

	Select top 1 @result = lvl
	FROM @t
	ORDER BY Tally DESC, lvl DESC
end

return @result

END
GO

