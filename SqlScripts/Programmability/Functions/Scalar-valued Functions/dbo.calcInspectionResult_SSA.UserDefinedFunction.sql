SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[calcInspectionResult_SSA]
(
	@inspID int
)
RETURNS nvarchar(50)
AS
BEGIN

declare @result nvarchar(50)

declare @coScore int
declare @coMax decimal(5,2)
declare @seScore int
declare @seMax decimal(5,2)
declare @finalScore decimal(5,2)

-- school evaluation is 90%, classroom observation is 10%
-- schoolevalution is out of 96 , each classroom observation is out of 60 (96 + 60 * 2 = 216)
-- so the relevant wieghts are computed from this
declare @coWeight float = convert(float, 10)/60
declare @seWeight float = convert(float, 90)/96


declare @ptSet nvarchar(10)			-- the partititon set used by this


declare @category TABLE
(
	CategoryType nvarchar(100),
	Category		nvarchar(100),
	Partition		nvarchar(10),
	Score			float,
	MaxScore		int

)

INSERT INTO @category
Select categoryType
, category
, min(isnull(SI.Partition, 'SSA')) Partition
, sum(SCore) score
, sum(maxScore) maxScore
from pInspectionRead.SchoolStandardAssessment SSA
	INNER JOIN pInspectionRead.SchoolInspections SI
		ON SSa.inspID = SI.inspID
WHERE SI.inspID = @inspID
group by CategoryType, Category


-- get the partition to apply to the score - same on all records to take min()
select @ptSet = min(Partition)
from @category

Select @coScore = min(Score) -- the lower score of the 2 classroom observations
, @coMax = min(MaxScore)	-- maxScore is the same for all cagegory of CLASSROOM_OBSERVATION, so this is OK to get min
from @category
WHERE CategoryType = 'CLASSROOM_OBSERVATION'

-- sum accross all school evalutations
Select @seScore = sum(Score)
, @seMax = sum(maxScore)
from @category
WHERE CategoryType = 'SCHOOL_EVALUATION'

-- Old requirement to floor this number, not round it based on a meeting with Vishnu
-- and NDOE team was a misunderstanding. They are requesting the common
-- rounding up
--Select @FinalScore = floor(@coScore * @coWeight +  @seScore * @seWeight)
Select @FinalScore = round(@coScore * @coWeight +  @seScore * @seWeight, 0)

-- Not used
--Select (@coScore / @coMax) * 100,  (@seScore / @seMax) * 100, @FinalScore


Select @result = ptName from Partitions
WHERe ptSet = 'SSA'
AND @finalScore between ptMin and ptMax


return @result

END
GO

