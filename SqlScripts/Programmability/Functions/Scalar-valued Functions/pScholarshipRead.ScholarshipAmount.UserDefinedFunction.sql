SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [pScholarshipRead].[ScholarshipAmount]
(
	@gyIndex int,
	@fosCode nvarchar(10),
	@gpa decimal(3,2)
)

RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @amount int = 0

	-- Add the T-SQL statements to compute the return value here
	Select @amount = max(awAmount)
	from ScholarshipCriteria_ C
		INNER JOIN FieldOfstudy FOS
			ON FOS.fosCode = @fosCode
			AND FOS.fosgrpCode = C.fosgrpCode
			AND gyIndex = @gyIndex
			and awGPA <= @gpa
	-- Return the result of the function
	RETURN @amount

END
GO

