SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 Jan 2012
-- Description:	remove all trailing occr=urences of the search string from the target string
-- primarily to remove blank lines from multiline fields, is called by fromPDFMultiline
-- not this function is recursive
-- =============================================
CREATE FUNCTION [common].[removeAllTrailingAndLeading]
(
	-- Add the parameters for the function here
	@In nvarchar(max)
	, @textToRemove nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here

	if RIGHT(@in,len(@TextToRemove)) = @textToRemove
		return common.removeAllTrailingAndLeading( rtrim(left(@In, len(@in) - len(@TextToRemove))), @TextToRemove)

	if LEFT(@in,len(@TextToRemove)) = @textToRemove
		return common.removeAllTrailingAndLeading( ltrim(substring(@In, len(@TextToRemove) + 1, len(@in) - len(@TextToRemove))), @TextToRemove)

	return @in


	-- Return the result of the function


END
GO

