SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2021
-- Description:	Gets the school year start date template from sysParams
-- this is a date in yyyy-mm-dd format. The year is ignored.
-- =============================================
CREATE FUNCTION [common].[SchoolYearStartDate]
(
)
RETURNS date
AS
BEGIN
return convert(date, isnull(common.sysParam('SCHOOLYEAR_STARTDATE'),'2000-01-01'))

END
GO

