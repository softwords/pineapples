SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[UserSystemLanguage]
(

)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int
	select @Result = CASE dbo.UserLanguage()
			WHEN 0 then 0
			WHEN 1 then dbo.sysparamint('AltLanguage1')
			WHEN 2 then dbo.sysparamint('AltLanguage2')
			end

	-- Add the T-SQL statements to compute the return value here

	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON [dbo].[UserSystemLanguage] TO [public] AS [dbo]
GO

