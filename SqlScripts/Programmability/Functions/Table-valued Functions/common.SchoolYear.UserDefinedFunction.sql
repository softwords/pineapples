SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2021
-- Description:	Get the school year as int and string for a date
-- =============================================
CREATE FUNCTION [common].[SchoolYear]
(
	@dt date, @startDate date = null
)
RETURNS @T TABLE
( yr int
, yrStr nvarchar(20)
)
AS
BEGIN
declare @yr int = year(@dt)
if @startDate is null
	select @startDate = common.SchoolYearStartDate()

if month(@startDate) = 1 and day(@startDate) = 1 begin
	INSERT INTO @t
	Select @yr, convert(nvarchar(4), @yr)
	return
end

declare @actStart date = datefromparts(@yr, month(@startDate), day(@startDate))

declare @fy int = case when @dt >= @actStart then year(dateadd(d,- 1, @actStart)) + 1 else @yr end

insert into @t
Select @fy, convert(nvarchar(4),@fy -1) + '-' + convert(nvarchar(4),@fy)

return

END
GO

