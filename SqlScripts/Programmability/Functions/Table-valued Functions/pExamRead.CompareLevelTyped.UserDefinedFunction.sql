SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [pExamRead].[CompareLevelTyped]
(

@schNo nvarchar(50)
, @examCode nvarchar(10)
, @examYear int

)
RETURNS TABLE
AS
RETURN
(

Select examID, examCode, examYear, examName
, Gender
, 'School' Aggregation
, schNo AggregationKey
, SchoolName AggregationDesc
, RecordType
, ID
, [Key]
, Description
, achievementLevel
, achievementDesc
, indicatorCount
, weight
, candidateCount
FRom pExamRead.SchoolLevelTyped
WHERE schNo = @schNo
AND (examCode = @examCode OR @examCode is null)
AND (examYear = @examYear OR @examYear is null)

UNION ALL

Select examID, examCode, examYear, examName
, Gender
, 'District' Aggregation
, DistrictCode AggregationKey
, District AggregationDesc
, RecordType
, ID
, [Key]
, Description
, achievementLevel
, achievementDesc
, indicatorCount
, weight
, candidateCount
FRom pExamRead.DistrictLevelTyped DLT

WHERE DLT.DistrictCode in (
Select iGroup from Schools S
INNER JOIN Islands I
	ON S.iCode = I.iCode
WHERE (schNo = @schNo or @schNo is null)
)
AND (examCode = @examCode OR @examCode is null)
AND (examYear = @examYear OR @examYear is null)

UNION ALL

Select examID, examCode, examYear, examName
, Gender
, 'Nation' Aggregation
, 'Nation' AggregationKey
, 'Nation' AggregationDesc
, RecordType
, ID
, [Key]
, Description
, achievementLevel
, achievementDesc
, indicatorCount
, weight
, candidateCount
FROM pExamRead.NationLevelTyped

WHERE (examCode = @examCode OR @examCode is null)
AND  (examYear = @examYear OR @examYear is null)


)
GO

