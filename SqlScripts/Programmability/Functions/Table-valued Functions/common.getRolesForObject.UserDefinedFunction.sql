SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 03 2010
-- Description:	retiurn the roles that have permission on an object
-- =============================================
CREATE FUNCTION [common].[getRolesForObject]
(
	-- Add the parameters for the function here
	@ObjectName sysname,
	@Schema sysname = 'dbo',
	@Permission nvarchar(50) = null

)
RETURNS TABLE
AS
RETURN
(
	-- Add the SELECT statement with parameter references here
	Select u.Name roleName
      , P.permission_name permissionName
		, SS.object_ID objectID
		, SS.name objectName
		, SCH.name schemaName
      , P.[type] PermissionType

	  FROM sys.objects SS
	  INNER JOIN sys.schemas SCH
		ON SS.schema_id = SCH.schema_id
	  LEFT JOIN [sys].[database_permissions] P
		ON Major_id = SS.object_id
	  LEFT JOIN sys.database_principals U
		on U.principal_id = P.grantee_principal_id
		WHERE ss.name = @ObjectName
			AND SCH.name = @Schema
			AND (P.permission_name = @Permission or @Permission is null)
)
GO

