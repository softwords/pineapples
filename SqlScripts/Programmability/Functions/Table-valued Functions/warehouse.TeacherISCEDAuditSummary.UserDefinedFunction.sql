SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 7 2021
-- Description:	Shows the FTPT and FTE values for each teacher
-- with columns for each ISCED subcategory.
-- Also shows weighted value for Admin and Other
-- Used as audit in Uis Survey
-- =============================================
CREATE FUNCTION [warehouse].[TeacherISCEDAuditSummary]
(
	@surveyYear int

)
RETURNS TABLE
AS
RETURN
(
Select
SUB.tID
, TI.tFullName [Name]
, TI.tSex GenderCode
, Qualified
, Certified
, SUB.schNo
, SUB.schName
, SUB.AuthorityGovt
, nullif(sum([ISCED 01 FTPT]),0) [ISCED 01 FTPT]
, nullif(sum([ISCED 02 FTPT]),0) [ISCED 02 FTPT]
, nullif(sum([ISCED 10 FTPT]),0) [ISCED 10 FTPT]
, nullif(sum([ISCED 24 FTPT]),0) [ISCED 24 FTPT]
, nullif(sum([ISCED 25 FTPT]),0) [ISCED 25 FTPT]
, nullif(sum([ISCED 34 FTPT]),0) [ISCED 34 FTPT]
, nullif(sum([ISCED 35 FTPT]),0) [ISCED 35 FTPT]

, sum([ISCED 01 FTPT] + [ISCED 02 FTPT]
+ [ISCED 10 FTPT]
+ [ISCED 24 FTPT] + [ISCED 25 FTPT]
+ [ISCED 34 FTPT] + [ISCED 35 FTPT]
+ [ISCED 44 FTPT] + [ISCED 45 FTPT]
) FTPT

, nullif(sum([ISCED 01 FTE]),0) [ISCED 01 FTE]
, nullif(sum([ISCED 02 FTE]),0) [ISCED 02 FTE]
, nullif(sum([ISCED 10 FTE]),0) [ISCED 10 FTE]
, nullif(sum([ISCED 24 FTE]),0) [ISCED 24 FTE]
, nullif(sum([ISCED 25 FTE]),0) [ISCED 25 FTE]
, nullif(sum([ISCED 34 FTE]),0) [ISCED 34 FTE]
, nullif(sum([ISCED 35 FTE]),0) [ISCED 35 FTE]

, sum([ISCED 01 FTE] + [ISCED 02 FTE]
+ [ISCED 10 FTE]
+ [ISCED 24 FTE] + [ISCED 25 FTE]
+ [ISCED 34 FTE] + [ISCED 35 FTE]
+ [ISCED 44 FTE] + [ISCED 45 FTE]
) FTE

, nullif(sum([Admin]),0) [Admin]
, nullif(sum([Other]),0) [Other]
, LevelStr

FROM
(
Select L.tID
, L.LevelStr
, L.Qualified
, L.Certified
, case when W.IscedSubClass = 'ISCED 01' then W.WTeach else 0 end [ISCED 01 FTPT]
, case when W.IscedSubClass = 'ISCED 02' then W.WTeach else 0 end [ISCED 02 FTPT]
, case when W.IscedSubClass = 'ISCED 10' then W.WTeach else 0 end [ISCED 10 FTPT]
, case when W.IscedSubClass = 'ISCED 24' then W.WTeach else 0 end [ISCED 24 FTPT]
, case when W.IscedSubClass = 'ISCED 25' then W.WTeach else 0 end [ISCED 25 FTPT]
, case when W.IscedSubClass = 'ISCED 34' then W.WTeach else 0 end [ISCED 34 FTPT]
, case when W.IscedSubClass = 'ISCED 35' then W.WTeach else 0 end [ISCED 35 FTPT]
, case when W.IscedSubClass = 'ISCED 44' then W.WTeach else 0 end [ISCED 44 FTPT]
, case when W.IscedSubClass = 'ISCED 45' then W.WTeach else 0 end [ISCED 45 FTPT]

, case when W.IscedSubClass = 'ISCED 01' then W.W else 0 end [ISCED 01 FTE]
, case when W.IscedSubClass = 'ISCED 02' then W.W else 0 end [ISCED 02 FTE]
, case when W.IscedSubClass = 'ISCED 10' then W.W else 0 end [ISCED 10 FTE]
, case when W.IscedSubClass = 'ISCED 24' then W.W else 0 end [ISCED 24 FTE]
, case when W.IscedSubClass = 'ISCED 25' then W.W else 0 end [ISCED 25 FTE]
, case when W.IscedSubClass = 'ISCED 34' then W.W else 0 end [ISCED 34 FTE]
, case when W.IscedSubClass = 'ISCED 35' then W.W else 0 end [ISCED 35 FTE]
, case when W.IscedSubClass = 'ISCED 44' then W.W else 0 end [ISCED 44 FTE]
, case when W.IscedSubClass = 'ISCED 45' then W.W else 0 end [ISCED 45 FTE]
-- also display A and X
, case when W.IscedSubClass = 'A' then W.W else 0 end [Admin]
, case when W.IscedSubClass = 'X' then W.W else 0 end [Other]
, W.IscedSubClass
, W.schNo
, S.schName
, W.AuthorityGovt
from warehouse.TeacherLevelStr L
	LEFT JOIN warehouse.TeacherActivityWeights W
		ON L.tID = W.tID
		and L.SurveyYear = W.SurveyYear
	LEFT JOIN Schools S
		ON W.schNo = S.schNo
WHERE L.SurveyYEar = @SurveyYear
And Source not in ('A','A?')
) SUB
INNER JOIN TeacherIdentity TI
	ON SUB.tID = TI.tID

GROUP BY SUB.schNo, SUB.schName, SUB.AuthorityGovt
, SUB.tID, TI.tFullName, TI.tSex
, Qualified, Certified
, LevelStr


)
GO

