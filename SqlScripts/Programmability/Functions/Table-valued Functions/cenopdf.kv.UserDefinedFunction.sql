SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 June 2021
--
-- Description:	Denormalise a code table into a single row
-- =============================================
CREATE FUNCTION [cenopdf].[kv]
(
	-- Add the parameters for the function here
	@xml xml

)
RETURNS TABLE
AS
RETURN
(

Select r.value('(./row[@idx=0]/k)[1]','nvarchar(200)') k0
, r.value('(./row[@idx=0]/v)[1]','nvarchar(200)') v0

, r.value('(./row[@idx=1]/k)[1]','nvarchar(200)') k1
, r.value('(./row[@idx=1]/v)[1]','nvarchar(200)') v1

, r.value('(./row[@idx=2]/k)[1]','nvarchar(200)') k2
, r.value('(./row[@idx=2]/v)[1]','nvarchar(200)') v2

, r.value('(./row[@idx=3]/k)[1]','nvarchar(200)') k3
, r.value('(./row[@idx=3]/v)[1]','nvarchar(200)') v3

, r.value('(./row[@idx=4]/k)[1]','nvarchar(200)') k4
, r.value('(./row[@idx=4]/v)[1]','nvarchar(200)') v4

, r.value('(./row[@idx=5]/k)[1]','nvarchar(200)') k5
, r.value('(./row[@idx=5]/v)[1]','nvarchar(200)') v5

, r.value('(./row[@idx=6]/k)[1]','nvarchar(200)') k6
, r.value('(./row[@idx=6]/v)[1]','nvarchar(200)') v6

, r.value('(./row[@idx=7]/k)[1]','nvarchar(200)') k7
, r.value('(./row[@idx=7]/v)[1]','nvarchar(200)') v7

, r.value('(./row[@idx=8]/k)[1]','nvarchar(200)') k8
, r.value('(./row[@idx=8]/v)[1]','nvarchar(200)') v8

, r.value('(./row[@idx=9]/k)[1]','nvarchar(200)') k9
, r.value('(./row[@idx=9]/v)[1]','nvarchar(200)') v9

, r.value('(./row[@idx=10]/k)[1]','nvarchar(200)') k10
, r.value('(./row[@idx=10]/v)[1]','nvarchar(200)') v10

, r.value('(./row[@idx=11]/k)[1]','nvarchar(200)') k11
, r.value('(./row[@idx=11]/v)[1]','nvarchar(200)') v11

, r.value('(./row[@idx=12]/k)[1]','nvarchar(200)') k12
, r.value('(./row[@idx=12]/v)[1]','nvarchar(200)') v12

, r.value('(./row[@idx=13]/k)[1]','nvarchar(200)') k13
, r.value('(./row[@idx=13]/v)[1]','nvarchar(200)') v13

, r.value('(./row[@idx=14]/k)[1]','nvarchar(200)') k14
, r.value('(./row[@idx=14]/v)[1]','nvarchar(200)') v14

, r.value('(./row[@idx=15]/k)[1]','nvarchar(200)') k15
, r.value('(./row[@idx=15]/v)[1]','nvarchar(200)') v15

, r.value('(./row[@idx=16]/k)[1]','nvarchar(200)') k16
, r.value('(./row[@idx=16]/v)[1]','nvarchar(200)') v16

, r.value('(./row[@idx=17]/k)[1]','nvarchar(200)') k17
, r.value('(./row[@idx=17]/v)[1]','nvarchar(200)') v17

, r.value('(./row[@idx=18]/k)[1]','nvarchar(200)') k18
, r.value('(./row[@idx=18]/v)[1]','nvarchar(200)') v18

, r.value('(./row[@idx=19]/k)[1]','nvarchar(200)') k19
, r.value('(./row[@idx=19]/v)[1]','nvarchar(200)') v19

, r.value('(./row[@idx=20]/k)[1]','nvarchar(200)') k20
, r.value('(./row[@idx=20]/v)[1]','nvarchar(200)') v20

, r.value('(./row[@idx=21]/k)[1]','nvarchar(200)') k21
, r.value('(./row[@idx=21]/v)[1]','nvarchar(200)') v21

, r.value('(./row[@idx=22]/k)[1]','nvarchar(200)') k22
, r.value('(./row[@idx=22]/v)[1]','nvarchar(200)') v22

, r.value('(./row[@idx=23]/k)[1]','nvarchar(200)') k23
, r.value('(./row[@idx=23]/v)[1]','nvarchar(200)') v23

, r.value('(./row[@idx=24]/k)[1]','nvarchar(200)') k24
, r.value('(./row[@idx=24]/v)[1]','nvarchar(200)') v24

, r.value('(./row[@idx=25]/k)[1]','nvarchar(200)') k25
, r.value('(./row[@idx=25]/v)[1]','nvarchar(200)') v25

, r.value('(./row[@idx=26]/k)[1]','nvarchar(200)') k26
, r.value('(./row[@idx=26]/v)[1]','nvarchar(200)') v26

, r.value('(./row[@idx=27]/k)[1]','nvarchar(200)') k27
, r.value('(./row[@idx=27]/v)[1]','nvarchar(200)') v27

, r.value('(./row[@idx=28]/k)[1]','nvarchar(200)') k28
, r.value('(./row[@idx=28]/v)[1]','nvarchar(200)') v28

, r.value('(./row[@idx=29]/k)[1]','nvarchar(200)') k29
, r.value('(./row[@idx=29]/v)[1]','nvarchar(200)') v29


from @xml.nodes('kv') KV(r)

)
GO

