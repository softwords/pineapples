SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Function


-- =============================================
-- Author:	Helical
-- Create date: 09 07 2018
-- Description:	Compare school, state and nation for each standard in all exams (total Male/Female as on Phill's charts)
--
-- Note: You may prefer to look at a more general approach offered in pExamRead.SchoolLevelTyped
--
-- Call with following parametrs to get for specific school and year ('AIL100', 2021)
-- Call with following parametrs to get for specific school and all years ('AIL100', DEFAULT)
--
-- History Log:
--   * 01/04/2022, Ghislain Hachey, adjust for recent work on exams module and add note above.
--   * 21/04/2022, Ghislain Hachey, support for getting data for all years.
-- =============================================
CREATE FUNCTION [warehouse].[examsSchoolDistrictNationByStandardResults]
(
	@SchoolNo nvarchar(50)
	, @examYear int = NULL
)
RETURNS
@result TABLE
(
	Cohort nvarchar(50)
	,ExamCode nvarchar(50)
	,ExamName nvarchar(120)
	,ExamYear int
	, ExamStandard nvarchar(120)
	, Candidates int
	, [Tot2] int -- total candidates
	, [Tot1] int
	, [Tot3] int
	, [Tot4] int
	, [2P] float -- in percentages
	, [1P] float
	, [3P] float
	, [4P] float
)
AS
BEGIN
	-- insert school rows
	INSERT INTO @result

	Select Cohort
		,ExamCode
		,ExamName
		,ExamYear
      ,ExamStandard
	  ,Candidates
	  ,[Tot2]
	  ,[Tot1]
	  ,[Tot3]
	  ,[Tot4]
      ,ROUND(ISNULL(convert(float, [Tot2] * -1)/ Candidates, 0), 2) as [2P]
	  ,ROUND(ISNULL(convert(float, [Tot1] * -1)/ Candidates, 0), 2) as [1P]
	  ,ROUND(ISNULL(convert(float, [Tot3] ) / Candidates, 0), 2) as [3P]
	  ,ROUND(ISNULL(convert(float, [Tot4] ) / Candidates, 0), 2) as [4P]
	FROM
	(
		SELECT schName AS Cohort
			,examCode AS ExamCode
			,examName AS ExamName
			,examYear AS ExamYear
			,CONCAT([standardCode],': ',[standardDesc]) AS ExamStandard
			,SUM([Candidates]) AS Candidates
			,SUM(ISNULL([2], 0)) AS [Tot2]
			,SUM(ISNULL([1], 0)) AS [Tot1]
			,SUM(ISNULL([3], 0)) AS [Tot3]
			,SUM(ISNULL([4], 0)) AS [Tot4]
		FROM warehouse.ExamResultsStandardsSchoolX ERSSX
		INNER JOIN Schools S ON ERSSX.schNo = S.schNo
		WHERE (examYear = @examYear OR @examYear IS NULL) AND ERSSX.schNo = @SchoolNo
		GROUP BY schName, ExamCode, ExamName, examYear, [standardCode], [standardDesc]
	) AS T

	-- insert the state rows
		INSERT INTO @result
	Select Cohort
	,ExamCode
	,ExamName
	,ExamYear
	,ExamStandard
	  ,Candidates
	  ,[Tot2]
	  ,[Tot1]
	  ,[Tot3]
	  ,[Tot4]
      ,ROUND(ISNULL(convert(float, [Tot2] * -1)/ Candidates, 0), 2) as [2P]
	  ,ROUND(ISNULL(convert(float, [Tot1] * -1)/ Candidates, 0), 2) as [1P]
	  ,ROUND(ISNULL(convert(float, [Tot3] ) / Candidates, 0), 2) as [3P]
	  ,ROUND(ISNULL(convert(float, [Tot4] ) / Candidates, 0), 2) as [4P]
	FROM
	(
		SELECT [District] AS Cohort
			,examCode AS ExamCode
			,examName AS ExamName
			,examYear AS ExamYear
			,CONCAT([standardCode],': ',[standardDesc]) AS ExamStandard
			,SUM([Candidates]) AS Candidates
			,SUM(ISNULL([2], 0)) AS [Tot2]
			,SUM(ISNULL([1], 0)) AS [Tot1]
			,SUM(ISNULL([3], 0)) AS [Tot3]
			,SUM(ISNULL([4], 0)) AS [Tot4]
		FROM warehouse.ExamResultsStandardsDistrictX AS ERSDX
				INNER JOIN Islands I ON ERSDX.DistrictCode = I.iGroup
				INNER JOIN Schools S ON S.iCode = I.iCode
		WHERE (examYear = @examYear OR @examYear IS NULL) AND S.schNo = @SchoolNo
		GROUP BY [District],ExamCode, ExamName, ExamYear, [standardCode], [standardDesc]
	) AS T

	-- finally national rows
	INSERT INTO @result
	Select Cohort
		,ExamCode
		,ExamName
		,ExamYear
	  ,ExamStandard
	  ,Candidates
	  ,[Tot2]
	  ,[Tot1]
	  ,[Tot3]
	  ,[Tot4]
      ,ROUND(ISNULL(convert(float, [Tot2] * -1)/ Candidates, 0), 2) as [2P]
	  ,ROUND(ISNULL(convert(float, [Tot1] * -1)/ Candidates, 0), 2) as [1P]
	  ,ROUND(ISNULL(convert(float, [Tot3] ) / Candidates, 0), 2) as [3P]
	  ,ROUND(ISNULL(convert(float, [Tot4] ) / Candidates, 0), 2) as [4P]
	From
	(
		SELECT 'Nation' AS Cohort
			,examCode AS ExamCode
			,examName AS ExamName
			,examYear AS ExamYear
			,CONCAT([standardCode],': ',[standardDesc]) AS ExamStandard
			,SUM([Candidates]) AS Candidates
			,SUM(ISNULL([2], 0)) AS [Tot2]
			,SUM(ISNULL([1], 0)) AS [Tot1]
			,SUM(ISNULL([3], 0)) AS [Tot3]
			,SUM(ISNULL([4], 0)) AS [Tot4]
		FROM warehouse.ExamResultsStandardsNationX AS ERSNX
		WHERE (examYear = @examYear OR @examYear IS NULL)
		GROUP BY [standardCode], ExamCode, ExamName, ExamYear, [standardDesc]
	) AS T
	RETURN
END
GO

