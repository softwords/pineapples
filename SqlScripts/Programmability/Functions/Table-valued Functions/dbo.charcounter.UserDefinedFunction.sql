SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[charcounter]
(
	@str nvarchar(1000)
	, @char nvarchar(1)
)
RETURNS
@t TABLE
(
	-- Add the column definitions for the TABLE variable here
	counter int
	, length int
	, scounter nvarchar(20)
	, perc decimal(4,1)
)
AS
BEGIN

declare @a int = len(@str)
if @a = 0 begin
	INSERT INTO @t
		VALUES (0, 0, '', null )
	RETURN
end

declare @b int = @a - len(replace(@str, @char, ''))

INSERT INTO @t
VALUES (@b, @a, convert(nvarchar(20), @b), convert(decimal(4,1),  @b) * 100 / @a )

RETURN


END
GO

