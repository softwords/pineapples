SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Function version of this
-- 10 3 2021 changed to use warehouse.enrol
-- =============================================
CREATE FUNCTION [pSchoolRead].[tfnSchoolDataSetEnrolment]
(
	-- Add the parameters for the function here
	@Year int
	, @SchoolNo nvarchar(50)
	, @ClassLevel nvarchar(10)
	, @Gender nvarchar(1)
)
RETURNS
@schoolData TABLE
(
	-- Add the column definitions for the TABLE variable here
	schNo nvarchar(50)
	, dataValue float
	, Estimate int
	, Quality int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set

INSERT INTO @schoolData
Select schNo,
	sum(Enrol)
	, Estimate, null ActualssqLevel   -- quality level not captured in warehouse
FROM warehouse.enrol
WHERE surveyYear = @Year
AND (schNo = @SchoolNo or @SchoolNo is null)
AND (ClassLevel = @ClassLevel or @ClassLevel is null)
AND (GenderCode = @Gender or @Gender is null)
GROUP BY schNo, Estimate

RETURN
END
GO

