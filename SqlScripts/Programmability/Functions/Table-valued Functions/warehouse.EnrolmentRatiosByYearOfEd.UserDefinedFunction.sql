SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2018
-- Description:	Warehouse - enrolment ratios
--
-- warehouse.EnrolmentRatiosByYearOfEd
-- Replicates dbo.EnrolmentRatiosByYearOfEd
-- but drawing from warehouse rather than live data
--
-- this view allows enrolment ratios to be calculated for a single year of education.
-- This is in contrast to the general calculation which is based on
-- Education Levels, and hence ranges of ages and years of education.
-- Here, a pupil is of official age if they are exactly the age for their current year of ed.
-- the Official age population is just the population of the single year of age for that year of education.
-- =============================================
CREATE FUNCTION [warehouse].[EnrolmentRatiosByYearOfEd]
(
@districtCode nvarchar(10) = null
)
RETURNS TABLE
AS
RETURN
(
SELECT S.*
, case when isnull(popM,0) = 0 then null else convert(float, EnrolM ) / PopM end gerM
, case when isnull(popF,0) = 0 then null else convert(float, EnrolF ) / PopF end gerF
, case when isnull(pop,0) = 0 then null else convert(float, Enrol )/ Pop end ger

, case when isnull(popM,0) = 0 then null else convert(float, nEnrolM ) / PopM end nerM
, case when isnull(popF,0) = 0 then null else convert(float, nEnrolF ) / PopF end nerF
, case when isnull(pop,0) = 0 then null else convert(float, nEnrol )/ Pop end ner

, enrolM - isnull(repM,0) intakeM
, enrolF - isnull(repF,0) intakeF
, enrol - isnull(rep,0) intake

, nEnrolM - isnull(nRepM,0) nIntakeM
, nEnrolF - isnull(nRepF,0) nIntakeF
, nEnrol - isnull(nRep,0)  nIntake

, case when isnull(popM,0) = 0 then null else convert(float, EnrolM - isnull(repM,0) ) / PopM end girM
, case when isnull(popF,0) = 0 then null else convert(float, EnrolF - isnull(repF,0) ) / PopF end girF
, case when isnull(pop,0) = 0 then null else convert(float, Enrol - isnull(rep,0)  )/ Pop end gir

, case when isnull(popM,0) = 0 then null else convert(float, nEnrolM  - isnull(nRepM,0) ) / PopM end nirM
, case when isnull(popF,0) = 0 then null else convert(float, nEnrolF  - isnull(nRepF,0) ) / PopF end nirF
, case when isnull(pop,0) = 0 then null else convert(float, nEnrol  - isnull(nRep,0) )/ Pop end nir


FROM
(
Select SurveyYear [Survey Year]
, min(case when ClassLevelOfficialAge = '=' then Age end) [Official Age]
, sum(case GenderCode when 'M' then Pop end) popM
, sum(case GenderCode when 'F' then Pop end) popF
, sum(Pop) pop

, L.lvlYear YearOfEd

, DF.level levelCode
, sum(case GenderCode when 'M' then Enrol end) enrolM
, sum(case GenderCode when 'F' then Enrol end) enrolF
, sum(Enrol) enrol
, sum(case when GenderCode = 'M' and ClassLevelOfficialAge = '=' then enrol end) nEnrolM
, sum(case when GenderCode = 'F' and ClassLevelOfficialAge = '=' then enrol end) nEnrolF
, sum(case when ClassLevelOfficialAge = '=' then enrol end) nEnrol

-- repeaters, gross and net

, sum(case GenderCode when 'M' then Rep end) repM
, sum(case GenderCode when 'F' then Rep end) repF
, sum(rep) rep
, sum(case when GenderCode = 'M' and ClassLevelOfficialAge = '=' then rep end) nRepM
, sum(case when GenderCode = 'F' and ClassLevelOfficialAge = '=' then rep end) nRepF
, sum(case when ClassLevelOfficialAge = '=' then rep end) nRep

-- psa
, sum(case GenderCode when 'M' then PSA end) psaM
, sum(case GenderCode when 'F' then PSA end) psaF
, sum(PSA) psa


from warehouse.EnrolmentRatios ER
	INNER JOIN lkpLEvels L
		ON ER.ClassLevel = L.codeCode
	LEFT JOIN ListDefaultPathLevels DF
		ON l.lvlYear = DF.YearOfEd
WHERE (ER.DistrictCode = @districtCode OR @DistrictCode is null)
GROUP BY
 SurveyYear
, L.lvlYear
, DF.level
) S

)
GO

