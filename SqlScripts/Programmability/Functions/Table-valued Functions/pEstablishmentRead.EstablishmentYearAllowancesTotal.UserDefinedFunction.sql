SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 31 5 2010
-- Description:	Total of allowances payable to teachers at a school, in a year
-- =============================================
CREATE FUNCTION [pEstablishmentRead].[EstablishmentYearAllowancesTotal]
(
	-- Add the parameters for the function here
	@EstYear int
)
RETURNS TABLE
AS
RETURN
(
	 Select
		count(DISTINCT schNo) NumSchools
		, sum(TotalLineItemAllowances) TotalLineItemAllowances
		, sum(TotalAllowancePayable) TotalAllowancePayable
	 from pEstablishmentRead.SchoolAllowancesTotal(@EstYEar)

)
GO

