SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2 7 2021
-- Description:	Return the age range for a school type and year
-- This has usually been done by looking at SurveyYearSchoolTypes
-- but since for modern installations there is nothing else on this table of interest
-- (it holds metadata about the legacy form to use for data entry, or the PDF form to use)
-- then eliminate the dependency on the existence of this record
-- To use, reference this function on metaSchoolTableItems
-- as the tdiSrc associated to tdiCode 'AGE'
/*
  UPDATE metaSchoolTableItems
  SET tdiSrc = 'Select * from common.SchoolTypeAgeRange([paramSchoolType],[paramYear])'
  WHERE tdiCode = 'AGE'
*/
-- =============================================
CREATE FUNCTION [common].[SchoolTypeAgeRange]
(
	-- Add the parameters for the function here
	@schoolType nvarchar(10)
	, @surveyYear int
)
RETURNS
@r TABLE
(
	codeCode int
	, codeDescription nvarchar(10)
)
AS
BEGIN
declare @min int
declare @max int

Select @min = ytAgeMin
, @max = ytAgeMax
from SurveyYearSchoolTypes

WHERE svyYear = @surveyYear
AND stCode = @schoolType

if @@ROWCOUNT = 0 begin
-- the long way around is we get the official age range of the levels of the school type
-- and 'pad' that range abouve and below

Select @min = min(lvlYear) - 3 + svyPSAge
, @max = max(lvlYear) + 6 + svyPSAge
from metaSchoolTypeLevelMap STLM
INNER JOIN lkpLevels L
	ON STLM.tlmLevel = L.codeCode
	AND STLM.stCode = @schoolType
INNER JOIN Survey S
	ON S.svyYear = @surveyYear
GROUP BY svyPSAge


end

INSERT INTO @r
(codeCode, codeDescription)
Select num, str(num)
From metaNumbers
WHERE num between @min and @max


	RETURN
END
GO

