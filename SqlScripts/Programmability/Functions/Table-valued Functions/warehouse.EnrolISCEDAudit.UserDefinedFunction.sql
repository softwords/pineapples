SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 7 2021
-- Description:    View
-- @iscedclass can be a top level class (e.g. ISCED 1, ISCED 2....
-- or a sub class (IsCED 01, ISCED 24)
-- as well the specical values SECONDARY4, SECONDARY5 can be used:
-- SECONDARY4 = ISCED 24 or ISCED 34
-- SECONDARY5 = ISCED 25 or ISCED 35
-- =============================================
CREATE FUNCTION [warehouse].[EnrolISCEDAudit]
(
	@surveyYear int
)
RETURNS TABLE
AS
RETURN
(
	Select SurveyYear
	, SchNo
	, [School Name]
	, PublicPrivate
	, GenderCode
	, Age
	, A3AgeGroup
	, A5AgeGroup
	, A6AgeGroup
	, ClassLevel
	, [ISCED SubClass]
	, [ISCED Level]
	, Enrol
	, Rep
	, PSA
	, Estimate
	from warehouse.EnrolUis
	WHERE SurveyYear = @surveyYear
)
GO

