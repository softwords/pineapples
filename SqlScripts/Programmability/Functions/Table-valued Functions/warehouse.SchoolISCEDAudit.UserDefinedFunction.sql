SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 7 2021
-- Description:	Show the ISCED levels 0-3 that a school teaches in.
-- As well, show the school and Wash items that are used to calculate sheet A13
-- =============================================
CREATE FUNCTION [warehouse].[SchoolISCEDAudit]
(
	@surveyYear int
)
RETURNS TABLE
AS
RETURN
(
Select S2.schNo
, S2.[School Name]
, S2.PublicPrivate
, S2.[ISCED 0]
, S2.[ISCED 1]
, S2.[ISCED 2]
, S2.[ISCED 3]
, common.YesNoInt(r.value('@Electricity','nvarchar(20)')) Electricity
, r.value('@Electricity_Source','nvarchar(20)') Electricity_Source

, common.YesNoInt(r.value('@Internet_in_teaching','nvarchar(20)')) Internet_in_teaching
, common.YesNoInt(r.value('@Internet','nvarchar(20)')) Internet

, common.YesNoInt(r.value('@HIV_Education','nvarchar(20)')) HIV_Education
, common.YesNoInt(r.value('@Sexuality_Education','nvarchar(20)')) [Sexuality_Education]
, common.YesNoInt(r.value('@SpEd_Infrastructure','nvarchar(20)')) SpEd_Infrastructure

, common.YesNoInt(wash.value('@Available','nvarchar(20)')) Handwashing
, wash.value('@Soap_And_Water','nvarchar(20)') SoapAndWater


, wash.value('@Toilet_Type','nvarchar(20)') Toilet_Type

, wash.value('@Girls_Toilets_Total','int') Girls_Toilets_Total
, wash.value('@Girls_Toilets_Usable','int') Girls_Toilets_Usable

, wash.value('@Boys_Toilets_Total','int') Boys_Toilets_Total
, wash.value('@Boys_Toilets_Usable','int') Boys_Toilets_Usable

, wash.value('@Common_Toilets_Total','int') Common_Toilets_Total
, wash.value('@Common_Toilets_Usable','int') Common_Toilets_Usable

, wash.value('@Main_Source_Drinking_Water','nvarchar(20)') Main_Source_Drinking_Water
, common.YesNoInt(wash.value('@Currently_Available','nvarchar(20)')) Water_Available

from
(
Select schNo
, [School Name]
, SurveyDimensionID
, case AuthorityGovtCode when 'G' then 'Public' when 'N' then 'Private' end PublicPrivate
, sum(case Isced when 'ISCED 0' then 1 end) [Isced 0]
, sum(case Isced when 'ISCED 1' then 1 end) [Isced 1]
, sum(case Isced when 'ISCED 2' then 1 end) [Isced 2]
, sum(case Isced when 'ISCED 3' then 1 end) [Isced 3]
from
(
Select DISTINCT schNo, DSS.[School Name], AuthorityGovtCode, SurveyDimensionID, [ISCED Level] Isced
from warehouse.Enrol E
INNER JOIN DimensionLevel DL
	ON E.ClassLevel = DL.LevelCode
INNER JOIN warehouse.DimensionSchoolSurvey DSS
	ON E.surveyDimensionID = DSs.[Survey ID]
Where SurveyYear = @SurveyYear
and Enrol > 0
) SUB
group by schNo,AuthorityGovtCode, SurveyDimensionID, [School Name]
) S2
INNER JOIN SchoolSurvey SS
	ON s2.SurveyDimensionID = SS.ssID
OUTER APPLY SS.ssData.nodes('row') D(r)
OUTER APPLY SS.ssWashData.nodes('row') W(wash)

)
GO

