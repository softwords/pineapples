SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 7 2021
-- Description:	List all teachers working in the given ISCED category
-- @iscedclass can be a top level class (e.g. ISCED 1, ISCED 2....
-- or a sub class (IsCED 01, ISCED 24)
-- as well the specical values SECONDARY4, SECONDARY5 can be used:
-- SECONDARY4 = ISCED 24 or ISCED 34
-- SECONDARY5 = ISCED 25 or ISCED 35
-- =============================================
CREATE FUNCTION [warehouse].[TeacherISCEDAudit]
(
	@iscedclass nvarchar(10),
	@surveyYear int

)
RETURNS TABLE
AS
RETURN
(
	-- Add the SELECT statement with parameter references here

	Select L.SurveyYear
	, L.schNo
	, S.schName [School]
	, DSS.AuthorityGovt
	, L.tID
	, TI.tFullName [Name]
	, GenderCode
	, Qualified
	, Certified
	, TPK
	, T00, T01, T02, T03, T04, T05, T06, T07, T08, T09, T10, T11, T12, T13, T14, T15, T, A, X
	, Activities
	, case when t=1 then 1 else
		len(replace(substring(LevelStr, II.MinYr + 2, II.NumYrs),'0',''))
	end SelectionActivities
	, case when t=1 then 1 else
		len(replace(substring(LevelStr, II.MinYr + 2, II.NumYrs),'0','')) / Activities
	end SelectionFTE
	, MinYr
	, MaxYr

	from warehouse.TeacherLevelStr L
	INNER JOIN TeacherIdentity TI
		ON L.tID = TI.tID
	INNER JOIN Schools S
		ON L.schNo = S.schNo
	INNER JOIN warehouse.dimensionSchoolSurvey DSS
		ON DSS.[Survey ID] = L.surveyDimensionID

	--
	INNER JOIN (
		 Select schNo, SurveyYear
		 --, DL.[ISCED SubClass]
		 , min(M.lvlYear) minYr
		 , max(M.lvlYear) MaxYr
		,  max(M.lvlYear) - min(M.lvlYear) + 1 NumYrs
		 from warehouse.schoolYearLevelMap M
		 INNER JOIN DimensionLevel DL
		 on M.ClassLevel = DL.levelCode
		 WHERE SurveyYear = @surveyYear
		 AND
		 (
			@iscedclass in ([ISCED SubClass], [ISCED Level]) -- this will handle
			OR (@iscedclass = 'SECONDARY4' and [ISCED SubClass] in ('ISCED 24','ISCED 34'))
			OR (@iscedclass = 'SECONDARY5' and [ISCED SubClass] in ('ISCED 25','ISCED 35'))
		 )
		 GROUP BY schNo, SurveyYEar		--, [ISCED SubClass]
) II
ON
	II.schNo = L.schNo and II.SurveyYear = L.SurveyYear
	AND
	( substring(LevelStr, II.MinYr + 2, II.NumYrs) like '%1%' -- this case gets those that have supplied grtade level data
		OR (T=1 and @iscedclass in (L.ISCEDSubClass, L.ISCEDLevel)) -- this case is for 'Teacher Unspecified.
		OR (T=1 and @iscedclass = 'SECONDARY4' and L.ISCEDSubClass in ('ISCED 24','ISCED 34'))
		OR (T=1 and @iscedclass = 'SECONDARY5' and L.ISCEDSubClass in ('ISCED 25','ISCED 35'))
	)
	WHERE Source not in ('A','A?')

)
GO

