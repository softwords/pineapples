SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 4 2010
-- Description:	Calcuate the best sector for a teacher, from the available options on a survey
-- =============================================
CREATE FUNCTION [pTeacherRead].[TEacherSurveySectorAudit]
(
	-- Add the parameters for the function here
	@TeacherSurveyID int = null
)
RETURNS TABLE
AS
RETURN
(
	-- Add the SELECT statement with parameter references here

Select tchsID
, svyYear
, schNo
, ssSchType
, tchRole
, tchClass
, tchClassMax
, tchSector
, RoleSector
, SchoolTypeSector
, MinClassSector
, MaxClassSector
, ClassLevelSector
, coalesce(SchoolTypeSector,
		case when RoleSector in (ClassLevelSector, MinClassSector,MaxClassSector) then RoleSector
		else Coalesce(RoleSector,MinClassSector,MaxClassSector) end
		) CalcSector
from
(
	Select tchsID
	, SS.svyYear
	, SS.schNo
	, SS.ssSchType
	, tchRole
	, tchClass
	, tchClassMax
	, tchSector
	, TR.secCode RoleSector
	, ST.SchoolTypeSector
	, MinClass.ClassSector MinClassSector
	, MaxClass.ClassSector MaxClassSector
	, case when coalesce( MinClass.ClassSector,MaxClass.ClassSector) = coalesce( MaxClass.ClassSector,MinClass.ClassSector)
		then coalesce( MinClass.ClassSector,MaxClass.ClassSector) else null end ClassLevelSector
	, case
		when TQE.ytqCertified = 1 then 'Y'
		when TQNE.ytqCertified = 1 then 'Y'
		else 'N'
	end Certified
	,  case
		when TQE.ytqQualified = 1 then 'Y'
		when TQNE.ytqQualified = 1 then 'Y'
		else 'N'
	end Qualified
	from TeacherSurvey TS
	LEFT JOIN SchoolSurvey SS
		ON TS.ssID = SS.ssID
	LEFT JOIN lkpTeacherRole TR
		ON TS.tchRole = TR.codeCode
	LEFT JOIN SurveyYearTeacherQual TQE
		ON TQE.svyYEar = SS.svyYear
		AND TQE.ytqSector = TS.tchSector
		AND TQE.ytqQual = TS.tchEdQual
	LEFT JOIN SurveyYearTeacherQual TQNE
		ON TQNE.svyYEar = SS.svyYear
		AND TQNE.ytqSector = TS.tchSector
		AND TQNE.ytqQual = TS.tchQual
	LEFT JOIN
		(
			Select stCode, min(secCode) SchoolTypeSector
			from lkpLevels
			INNER JOIN metaSchoolTypeLevelMap M
			ON lkpLevels.codeCode = M.tlmLevel
			group by stCode
			HAVING count(DISTINCT secCode) = 1
		) ST
		ON ST.stCode = SS.ssSchType
	LEFT JOIN
		(
			Select codeCode, secCode ClassSEctor
			FROM lkpLevels
		) MinClass
			ON MinClass.codeCode = TS.tchClass
	LEFT JOIN
		(
			Select codeCode, secCode ClassSEctor
			FROM lkpLevels
		) MaxClass
			ON MaxClass.codeCode = TS.tchClassMax
	WHERE (tchsID = @TeacherSurveyID
			OR @TeacherSurveyID is null)

) sub

)
GO

