SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 10 2009
-- Description:	Show missing data related to establishment generation
-- =============================================
-- 28 04 2010	-- extended to show the current parent and future parnet of the school
CREATE FUNCTION [pEstablishmentRead].[ExceptionsAndIssues]
(
	-- Add the parameters for the function here
	@EstYear int
)
RETURNS TABLE
AS
RETURN
(
	-- Add the SELECT statement with parameter references here

Select Schools.*
	, EP.Projection
	, case when EP.schNo is not null then 1 else 0 end HasProjection
	, POS.Positions
	, case when POS.schNo is not null then 1 else 0 end HasPositions
	, EPLAN.PlanPositions
	, case when EPLAN.schNo is not null then 1 else 0 end HasEstablishmentPlan
	, eststCode
	, case when isnull(eststCode, schType) = schType then 0 else 1 end ChangeType
	, estAuth
	, case when isnull(estAuth, schAuth) = schAuth then 0 else 1 end ChangeAuthority
	, SE.estParent
	, case when schEstablishmentPoint = estEstablishmentPoint  then 0
			when estEstablishmentPoint is null then 0		-- don;t oinclude those with missing establishment
			else 1 end ChangeParent
	, case when PlanPArents.estParent is null then 0 else 1 end PlanIsParent
	, case when Parents.schParent is null then 0 else 1 end IsParent
	, Parents.NumExtensions
	, PlanPArents.NumExtensions PlanExtensions
	, case when isnull(Parents.NumExtensions,0)
			= isnull(PlanParents.NumExtensions,0) then 0 else 1 end ChangeExtensions

	, case when
		(case when PlanPArents.estParent is null then 0 else 1 end)
		<>
		(case when Parents.schParent is null then 0 else 1 end )
	then 1 else 0 end ParentStatusChanged

from Schools
	LEFT JOIN
		(Select schNo , sum(epdSum) Projection
			from EstablishmentControl
			INNER JOIN EnrolmentProjection
				ON EstablishmentControl.escnID = EnrolmentProjection.escnID
			INNER JOIN EnrolmentProjectionData
				ON EnrolmentProjection.epID = EnrolmentProjectionData.epID
			WHERE estcYear = @EstYEar
			GROUP BY SchNo) EP
		ON Schools.schNo = EP.schNo
	LEFT JOIN
		(Select  schNo , count(schNo) Positions
			from Establishment CROSS JOIN
				EstablishmentControl

			WHERE estcYear = @EstYear
				AND estpActiveDate <= estcSnapshotDate
				and ( estpClosedDate >= estcSnapshotDate or estpClosedDate is null)
			GROUP BY SchNo) POS
		ON Schools.schNo = POS.schNo
	LEFT JOIN
		(Select schNo , sum(estrCount) PlanPositions
			from SchoolEstablishment INNER JOIN SchoolEstablishmentRoles
				ON SchoolEstablishment.estID = SchoolEstablishmentRoles.estID
			WHERE estYEar = @estYear
			GROUP BY SchNo) EPLAN
		ON Schools.schNo = EPLAN.schNo
	LEFT JOIN
		(Select DISTINCT
			schNo
			, eststCode
			, estAuth
			, estParent
			, estEstablishmentPoint
			from SchoolEstablishment
			WHERE estYEar = @estYear) SE
		ON Schools.Schno = SE.schNo
	LEFT JOIN
		(Select schParent, count(schNo) NumExtensions
			FROM Schools
			GROUP BY schParent
		) Parents
		On Parents.schParent = Schools.schNo
	LEFT JOIN
		(Select estParent, count(schNo) NumExtensions
			from SchoolEstablishment
			WHERE estYEar = @estYear
			GROUP BY estParent) PlanParents
		On PlanParents.estParent = Schools.schNo

WHERE
	 schClosed = 0 or schClosed >= @EstYear

)
GO

