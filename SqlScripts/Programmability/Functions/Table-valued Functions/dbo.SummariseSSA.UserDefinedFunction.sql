SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SummariseSSA]
(
	@xml xml
)
RETURNS @t TABLE
(
	Summary xml
)
AS
BEGIN

declare @seScores nvarchar(96) = ''			-- stirn holding the subcriteria scores for school evaluation
declare @co1Scores nvarchar(60) = ''		-- string holding classroom observation 1 scores
declare @co2Scores nvarchar(60) = ''		-- string holding classroom observation 2 scores

declare @t4 TABLE
(
	num int,
	resultValue int,
	result nvarchar(10)
)

declare @t5 TABLE
(
	num int,
	resultValue int,
	result nvarchar(10)
)

--- ONLY collect this if we have a SCHOOL_EVALUATION

declare @xmlType nvarchar(100)

Select @xmlType = @xml.value('(/survey/type)[1]', 'nvarchar(100)')

if (isnull(@xmlType,'') not in ('SCHOOL_ACCREDITATION')) begin
	INSERT INTO @t
	Select convert(xml,'')
	return
end
-- 1) Collect the SE subcriteria scores

Select @seScores = @seScores + answer
FROM
(
Select TOP 100 PERCENT
id
, case answer when 'POSITIVE' then '1' else '0' end answer
FROM
	(
	select
	v.value('id[1]','nvarchar(20)') id
	, v.value('(answer/state)[1]','nvarchar(20)') answer
	from @xml.nodes('/survey/category[type="SCHOOL_EVALUATION"]/standard/criteria/subcriteria') as V(v)
	) SUB
ORDER BY id
) SUB2

-- 2) collect the co1 subcriteria scores
Select @co1Scores = @co1Scores + answer
FROM
(
Select TOP 100 PERCENT
id
, case answer when 'POSITIVE' then '1' else '0' end answer
FROM
	(
	select
	v.value('id[1]','nvarchar(20)') id
	, v.value('(answer/state)[1]','nvarchar(20)') answer
	FROM @xml.nodes('/survey/category[name="Classroom Observation 1"]/standard/criteria/subcriteria') as V(v)
	) SUB
ORDER BY id
) SUB2

-- 3) collect the co2 subcriteria scores

Select @co2Scores = @co2Scores + answer
FROM
(
Select TOP 100 PERCENT
id
, case answer when 'POSITIVE' then '1' else '0' end answer
FROM
	(
	select
	v.value('id[1]','nvarchar(20)') id
	, v.value('(answer/state)[1]','nvarchar(20)') answer
	from @xml.nodes('/survey/category[name="Classroom Observation 2"]/standard/criteria/subcriteria') as V(v)
	) SUB
ORDER BY id
) SUB2


declare @secritScores nvarchar(100) = ''
declare @secritResults nvarchar(100) = ''

declare @co1critScores nvarchar(100) = ''
declare @co1critResults nvarchar(100) = ''

declare @co2critScores nvarchar(100) = ''
declare @co2critResults nvarchar(100) = ''


if (len(@seScores)> 0 ) begin
	-- if we have subcriteria scores, we can get scores and results at the criteria level
	-- this is the NEW format

	Select @secritScores = @secritScores + SE.scounter
	, @secritResults = @secritResults +
		case SE.counter
			when 0 then '1'
			when 1 then '1'
			when 2 then '1'
			when 3 then '2'
			when 4 then '3'
		end
	, @co1critScores = @co1critScores + CO1.scounter
	, @co1critResults = @co1critResults +
		case CO1.scounter
			when '' then ''
			when 0 then '1'
			when 1 then '1'
			when 2 then '1'
			when 3 then '2'
			when 4 then '3'
			when 5 then '4'
			else ''
		end
	, @co2critScores = @co2critScores + CO2.scounter
	, @co2critResults = @co2critResults +
		case CO2.scounter
			when '' then ''
			when 0 then '1'
			when 1 then '1'
			when 2 then '1'
			when 3 then '2'
			when 4 then '3'
			when 5 then '4'
			else ''
		end

	FROM metaNumbers
	CROSS APPLY dbo.charCounter(substring(@seScores, num * 4 + 1, 4), '1') SE
	CROSS APPLY dbo.charCounter(substring(@co1Scores, num * 5 + 1, 5), '1') CO1
	CROSS APPLY dbo.charCounter(substring(@co2Scores, num * 5 + 1, 5), '1') CO2
	WHERE num between 0 and 15
end			-- processing of NEW format

-- PROCESSING of Legacy format
if len(@seScores) = 0 begin
	-- the conversion process has written the result and result value into the criteria

	-- schoolevaluation criteria
	Select @seCritResults = @seCritResults + answer
	FROM
	(
	Select TOP 100 PERCENT
	id
	, answer
	FROM
		(
		select
		v.value('id[1]','nvarchar(20)') id
		, v.value('(resultvalue)[1]','nvarchar(1)') answer
		from @xml.nodes('/survey/category[type="SCHOOL_EVALUATION"]/standard/criteria') as V(v)
		) SUB
	ORDER BY id
	) SUB2

	-- schoolevaluation criteria
	Select @co1CritResults = @co1CritResults + answer
	FROM
	(
	Select TOP 100 PERCENT
	id
	, answer
	FROM
		(
		select
		v.value('id[1]','nvarchar(20)') id
		, v.value('(resultvalue)[1]','nvarchar(1)') answer
		from @xml.nodes('/survey/category[name="Classroom Observation 1"]/standard/criteria') as V(v)
		) SUB
	ORDER BY id
	) SUB2

	-- schoolevaluation criteria
	Select @co2CritResults = @co2CritResults + answer
	FROM
	(
	Select TOP 100 PERCENT
	id
	, answer
	FROM
		(
		select
		v.value('id[1]','nvarchar(20)') id
		, v.value('(resultvalue)[1]','nvarchar(1)') answer
		from @xml.nodes('/survey/category[name="Classroom Observation 2"]/standard/criteria') as V(v)
		) SUB
	ORDER BY id
	) SUB2
end -- end legacy process


--Select SE.* , num, substring(@seScores, num * 4 + 1, 4), P.*
--FROM metaNumbers
--CROSS APPLY dbo.charCounter(substring(@seScores, num * 4 + 1, 4), '1') SE
--INNER JOIN Partitions P
--	ON P.ptSet = 'SSA'
--	AND SE.Perc between ptMin and ptMax
--WHERE num between 0 and 15
declare @summary xml
Select @summary =
(
Select @seScores seScores
, @secritScores seCritScores
, @secritResults seCritResults
, @co1Scores co1Scores
, @co1critScores co1CritScores
, @co1critResults co1CritResults
, @co2Scores co2Scores
, @co2critScores co2CritScores
, @co2critResults co2CritResults
FOR XML PATH('summary')
)
INSERT INTO @t
Select @summary

RETURN

END
GO

