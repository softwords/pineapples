SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description: Get lookup of names for standards and criteria
-- from accreditations
-- =============================================
CREATE FUNCTION [dbo].[lkpInspectionTypeTerms]
(
	-- Add the parameters for the function here
	@type nvarchar(20)
)
RETURNS TABLE
AS
RETURN
(

	Select v.value('id[1]', 'nvarchar(20)') C
	, v.value('name[1]', 'nvarchar(200)') N
	, coalesce(v.value('shortname[1]', 'nvarchar(400)'),v.value('name[1]', 'nvarchar(400)')) S
	, 'S' T

	FROM lkpInspectionTypes
	CROSS APPLY intyTemplate.nodes('/survey/category/standard') V(v)
	WHERE intyCode = @type

	UNION

	Select v.value('id[1]', 'nvarchar(20)') C
	, v.value('name[1]', 'nvarchar(400)') N
	, coalesce(v.value('shortname[1]', 'nvarchar(400)'),v.value('name[1]', 'nvarchar(400)')) S
	, 'C' T
	FROM lkpInspectionTypes
	CROSS APPLY intyTemplate.nodes('/survey/category/standard/criteria') V(v)
	WHERE intyCode = @type
)
GO

