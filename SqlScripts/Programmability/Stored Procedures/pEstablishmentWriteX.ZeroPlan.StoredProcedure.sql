SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2009
-- Description:	set the plan to zero
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[ZeroPlan]
	-- Add the parameters for the stored procedure here
	@schoolNo nvarchar(50)
	, @Year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @err int,
	@ErrorMessage NVARCHAR(4000),
	@ErrorSeverity INT,
	@ErrorState INT;

declare @estID int

		SELECT @estID = estID
		FROM
			SchoolEstablishment
		WHERE
			schNo = @schoolNo
			AND estYear = @Year


	begin try

			DELETE FROM SchoolEstablishmentRoles
			WHERE
				estID = @estID
				AND estrQuota is null

			UPDATE SchoolEstablishmentRoles
				SET estrCountUser = 0
			FROM
				SchoolEstablishmentRoles
				WHERE estID = @estID


		UPDATE SchoolEstablishment
			SET estLocked = 1
			WHERE estID = @estID


	end try
--- catch block
	begin catch

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


	-- commit open transaction if any
	if @@trancount > 0
		commit transaction
END
GO

