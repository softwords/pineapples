SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 4 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[xmlTableDefs]
	-- Add the parameters for the stored procedure here
(
	@SchoolNo nvarchar(20),
	@paramYear int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @thisCode nvarchar(30)
declare @sqlSrc nvarchar(2000)
declare @lastCode nvarchar(30)
declare @xml xml
declare @sql nvarchar(2000)

declare @paramSchoolType nvarchar(5)
-- figure out the school type
--
Select @paramSchoolType = ssSchType from SchoolSurvey WHERE schNo = @SchoolNo AND svyYear = @paramYear
	if @paramSchoolType is null
		Select @paramSchoolType = schType from Schools WHERE schNo = @SchoolNo

create table #xmlTmpTD
(
	theCode nvarchar(20),
	theXML xml
)
select @lastCode = ''
select TOP 1 @thisCode = tdiCode, @sqlSrc = isnull(tdiSrcSQL,tdiSrc) from metaSchoolTableITems WHERE tdiCode > @lastCode ORDER By tdiCode

while @lastCode <> @thisCode
begin
	select @lastCode = @thisCode
	select @sqlSrc = replace(@sqlSrc,'[paramSchoolNo]',quotename(@SchoolNo,''''))

	select @sqlSrc = replace(@sqlSrc,'[paramYear]',@paramYear)
	select @sqlSrc = replace(@sqlSrc,'[paramSchoolType]',quotename(@paramSchoolType,''''))
--	print @thisCode +  @sqlSrc


    -- Insert statements for procedure here
	begin try
		create table #codeTmp
		(
			codeCode nvarchar(20),
			codeDescription nvarchar(1000)
		)
		insert into #codeTmp
		exec (@sqlSrc)
		set @xml = (SELECT * from #codeTmp for XML RAW('code'))
	end try
	begin catch
		set @xml = (select null as code, null as codeDescription FOR XML RAW('code'))

	end catch
	drop table #codeTmp
	insert into #xmlTmpTD
		Select @thisCode, @xml
	select TOP 1 @thisCode = tdiCode, @sqlSrc = isnull(tdiSrcSQL,tdiSrc) from metaSchoolTableITems WHERE tdiCode > @lastCode ORDER By tdiCode
end

SELECT @XML = (sELECT theCode [@itemType], theXML itemValues from #xmlTmpTD FOR XML PATH('TableItem'))
drop table #xmlTmpTD


--
declare @xmlDefs xml
set @xmlDefs =
(Select tdefCode [@code]
, tdefName [@name]
, tdefDataCode [@dataCode]
, tdefRows [@rowItem]
, tdefCols [@colItem]
, tdefHideCol1 [@hideCol1]
, tdefRowTotals [@showRowTotals]
, tdefColTotals [@showColTotals]
, tdefPermissionsReqR [@permissionsReqR]
, tdefPermissionsReqW [@permissionsReqW]
, tdefSrc [@source]
from TRmetaPupilTableDefs
FOR XML PATH('TableDef')
)
select @xmlDefs = (Select @xmlDefs for XML PAth('TableDefs'))
----------------------------------------------------------------
-- get the furniture map
----------------------------------------------------------------
-- maps are created as TableItems in the metaData
-- furniture map defines the furniture list to appear n the rooms page, in differnet forms, school types
create table #xmlFM
		(
			codeCode nvarchar(20),
			codeDescription nvarchar(1000),
			codeSeq int
		)

declare @xmlFM xml

insert into #xmlFM
exec pSchoolRead.mapFurniture @SchoolNo, @paramYear

set @xmlFM =
(Select codeCode
, codeDescription
, codeSeq
from #xmlFM
for XML RAW('code')
)
drop table #xmlFM


select @xmlFM = (Select 'FurnitureMap' [@itemType] , @xmlFM itemValues for XML PATH('TableItem'))


----------------------------------------------------------------
-- get the room map
----------------------------------------------------------------
-- maps are created as TableItems in the metaData
-- room map defines the various rooms to appear on custom room type lists, in differnet forms, school types
create table #xmlRM
		(
			codeCode nvarchar(20),
			codeDescription nvarchar(1000),
			roomGroup nvarchar(20)
		)

declare @xmlRM xml

insert into #xmlRM
exec pSchoolRead.mapRooms @SchoolNo, @paramYear

set @xmlRM =
(Select Tag
, Parent
, roomGroup as [itemValues!1!group]
, codeCode as [code!2!codeCode]
, codeDescription as [code!2!codeDescription]

from
(
Select DISTINCT
1 as Tag
, null as Parent
, roomGroup
, null as codeCode
, null as codeDescription

from
#xmlRM

UNION ALL
Select 2 as Tag
, 1 as Parent
, roomGroup
, codeCode
, codeDescription

from
#xmlRM
) u
ORDER BY
[itemValues!1!group]
, [code!2!codeCode]
, [code!2!codeDescription]

for XML EXPLICIT

)
drop table #xmlRM


select @xmlRM = (Select 'RoomMap' [@itemType] , @xmlRM  for XML PATH('TableItem'))


select @xml = (Select @xml, @xmlFM, @xmlRM for XML PAth('TableItems'))


select @xml = (Select @xmlDefs, @xml for XML PATH('metaData'))

insert into #xmlTmp select @xml


END
GO
GRANT EXECUTE ON [dbo].[xmlTableDefs] TO [pSchoolRead] AS [dbo]
GO

