SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 04 2018
-- Description:	Extended read of student for web site
-- =============================================
CREATE PROCEDURE [pSchoolRead].[StudentReadEx]
	@studentID uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

    Select *
 	from Student_ S
	WHERE stuID = @studentID

	Select *
	from StudentEnrolment_
	WHERE stuID = @studentID

	Select *
	from
	(
	Select S.*
	, SS.posYear
	, SS.posSemester
	, SS.gyIndex
	, SS.instCode
	, SS.fosCode
	, SS.posAmount
	, SS.posPaid
	, SS.posGPA
	, row_number() OVER (PARTITION By S.schoID ORDER BY posYear DESC, posSemester DESC) RN
	from Scholarships S
		LEFT JOIN ScholarshipStudy_ SS
			ON S.schoID = SS.schoID
	WHERE stuID = @studentID
	) SUB
	WHERE RN = 1

	Select * from pStudentRead.StudentLinks
	WHERE stuID = @studentID
END
GO

