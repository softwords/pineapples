SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 9 2021
-- Description:	DELETE a Census Year
-- =============================================
CREATE PROCEDURE [pSurveyAdmin].[CensusDelete]
	@surveyYear int
AS
BEGIN

Declare @numSurveys int

Select @numSurveys = count(*)
from SchoolSurvey WHERE svyYear = @surveyYear

if (@numSurveys = 0) begin

	DELETE from SurveyYearSchoolTypes
	WHERE svyYear = @SurveyYear

	DELETE from SurveyYearTeacherQual
	WHERE svyYear = @SurveyYear

	DELETE from Survey
	WHERE svyYear = @SurveyYear

	return
end


RAISERROR('School Surveys exists in the selected year',16,1);


END
GO

