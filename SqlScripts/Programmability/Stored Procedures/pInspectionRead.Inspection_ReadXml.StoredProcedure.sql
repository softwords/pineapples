SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2 12 2017
-- Description:	Polymorphic read of school inspection - returning Xml
-- When used by the WebAPI, this Xml translate3s to a structred Json object
-- The inspXml (InspectionContent) Xml object becomes a node in the resulting Xml,
-- and hence a structred property when converted to Json.
-- This delivers surveys to the client to be efficiently rendered.
--
-- History Logs:
-- * 12/12/2022, Ghislain Hachey, added Note field to XML output
-- =============================================
CREATE PROCEDURE [pInspectionRead].[Inspection_ReadXml]
	-- Add the parameters for the stored procedure here
	@inspID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @inspType nvarchar(20)

-- find the inspection type
Select @inspType = inspTypeCode
FROM pInspectionRead.SchoolInspections
WHERE inspID = @inspID

-- Is there a custom query for this type?
declare @found int
Select @found = count(*) from INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_SCHEMA = 'pInspectionRead'
AND ROUTINE_NAME = @inspType + '_ReadXml'

if (@found > 0) begin
	-- yes there is a custom query for this
	declare @sql nvarchar(400) = 'exec pInspectionRead.' + @inspType + '_ReadXml @ID'
	declare @ParmDefinition nvarchar(50) = N'@ID int';
	exec sp_executesql @sql, @ParmDefinition, @ID=@inspID
end
else begin

 	Select inspID
	, schNo
	, schName
	, StartDate
	, EndDate
	, InspectionYear
	, InspectedBy
	, inspTypeCode
	, InspectionType
	, InspectionResult
	, SourceId
	, Note
	, InspectionClass
	, Partition
	, pCreateTag
	, pCreateUser
	, pCreateDateTime
	, pEditUser
	, pEditDateTime
	, InspectionContent
	, X.Summary summary
	FROM pInspectionRead.SchoolInspections
	CROSS APPLY dbo.SummariseSSA(InspectionContent) X
	WHERE inspID = @inspID
	FOR XML PATH('inspection')

end


END
GO

