SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of wash data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- 29 06 2021 save the entire Xml to ssWashData on SchoolSurvey

-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusLoadWash]
@xml xml
, @filereference uniqueidentifier
, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- totals by education level

-- start tyhe try catch


begin try

--- call validations
--- Validations must be passed before anything else happens
DECLARE @validationResult int
declare @validations TABLE
(
	field nvarchar(50),
	errorValue nvarchar(100),
	valMsg nvarchar(200),
	severity nvarchar(20)
	, NumRows int
	, FirstRow int
	, LastRow int
)


INSERT INTO @Validations
exec pSurveyOps.CensusValidateWash @xml

if @@Rowcount > 0  begin
	Select * from @Validations
	RAISERROR ('Validations failed - cannot proceed', 16, 255)
end


declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8
-- startpos = 8

Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
From @xml.nodes('ListObject') as V(v)


Select @districtID = dID
from Districts WHERE dName = @districtName

Select @districtID, @districtName

declare @counter int	-- for storing @@ROWCOUNT etc

DECLARE @ndoeWash TABLE
(
RowIndex int
--, SchoolYear                 nvarchar(100) NULL
--, State                      nvarchar(100) NULL
, School_Name                nvarchar(100) NULL
, School_No                  nvarchar(100) NULL
--, School_Type                nvarchar(100) NULL
, Main_Source_Drinking_Water nvarchar(100) NULL
, Currently_Available        nvarchar(100) NULL
, Toilet_Type                nvarchar(100) NULL
, Girls_Toilets_Total        nvarchar(100) NULL
, Girls_Toilets_Usable       nvarchar(100) NULL
, Boys_Toilets_Total         nvarchar(100) NULL
, Boys_Toilets_Usable        nvarchar(100) NULL
, Common_Toilets_Total       nvarchar(100) NULL
, Common_Toilets_Usable      nvarchar(100) NULL
, Available                  nvarchar(100) NULL
, Soap_and_Water             nvarchar(100) NULL
-- end genereted code
, fileReference				uniqueidentifier
, xmldata					xml

, ssID						int
)


INSERT INTO @ndoeWash
(
RowIndex
--, SchoolYear
--, State
, School_Name
, School_No
--, School_Type
, Main_Source_Drinking_Water
, Currently_Available
, Toilet_Type
, Girls_Toilets_Total
, Girls_Toilets_Usable
, Boys_Toilets_Total
, Boys_Toilets_Usable
, Common_Toilets_Total
, Common_Toilets_Usable
, Available
, Soap_and_Water
, fileReference
, xmldata
)
Select
v.value('@Index', 'int') [ RowIndex]
--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')                              [State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                                   [School_Type]
, nullif(ltrim(v.value('@Main_Source_Drinking_Water', 'nvarchar(100)')),'')                       [Main_Source_Drinking_Water]
, nullif(ltrim(v.value('@Currently_Available', 'nvarchar(100)')),'')                              [Currently_Available]
, nullif(ltrim(v.value('@Toilet_Type', 'nvarchar(100)')),'')                                      [Toilet_Type]
, nullif(ltrim(v.value('@Girls_Toilets_Total', 'nvarchar(100)')),'')                             [Girls'_Toilets_Total]
, nullif(ltrim(v.value('@Girls_Toilets_Usable', 'nvarchar(100)')),'')                                          [Girls'_Toilets_Usable]
, nullif(ltrim(v.value('@Boys_Toilets_Total', 'nvarchar(100)')),'')                              [Boys'_Toilets_Total]
, nullif(ltrim(v.value('@Boys_Toilets_Usable', 'nvarchar(100)')),'')                             [Boys'_Toilets_Usable]
, nullif(ltrim(v.value('@Common_Toilets_Total', 'nvarchar(100)')),'')                             [Common_Toilets_Total]
, nullif(ltrim(v.value('@Common_Toilets_Usable', 'nvarchar(100)')),'')                                          [Common_Toilets_Usable]
, nullif(ltrim(v.value('@Available', 'nvarchar(100)')),'')                                        [Available]
, nullif(ltrim(v.value('@Soap_and_Water', 'nvarchar(100)')),'')                                   [Soap_and_Water]
-- End Generated Code
, @fileReference
, v.query('.')			-- returns the xml object v from its root node 'row'
from @xml.nodes('ListObject/row') as V(v)


----- PROCESSING --------

-- force the removal of any duplicate
DELETE from @ndoeWash
FROM @ndoeWash
INNER JOIN
( Select RowIndex
, row_number() OVER(Partition by school_No ORDER BY RowIndex) RN
FROM @ndoeWash
) U
ON [@ndoeWash].RowIndex = U.RowIndex
WHERE RN > 1

-- create any records required in SchoolSurvey
-- but they should not be needed if we have already processed Schools


UPDATE @ndoeWash
SET ssID = SS.ssID
FROM @ndoeWash
	INNER JOIN SchoolSurvey SS
		ON SS.schNo = [@ndoeWash].School_No
		AND SS.svyYear = @SurveyYear

UPDATE SchoolSurvey
SET
ssSource = @filereference
, ssWashData = xmldata
FROm SchoolSurvey
INNER JOIN @ndoeWash N
	ON SchoolSurvey.ssID = N.ssID
INNER JOIN Schools S
	ON SchoolSurvey.schNo = S.schNo


-- create any records required in SchoolSurvey
-- but they should not be needed if we have already processed Schools
INSERT INTO SchoolSurvey
(
svyYear
, schNo
, ssSchType
, ssAuth
, ssElectN
, ssElectL
, ssSource
, ssWashData
)
Select @SurveyYear
, NDOE.School_No
, schType
, schAuth
, schElectN
, schElectL
, @filereference
, xmldata

FROM @ndoeWash NDOE
INNER JOIN Schools S
	ON NDOE.School_No = S.schNo
LEFT JOIN SchoolSurvey SS
	ON NDOE.School_No = SS.schNo
	AND @SurveyYear = SS.svyYear
WHERE SS.ssID is null

-- for convenience, put the ssID back on the @ndoeWash table

UPDATE @ndoeWash
SET ssID = SS.ssID
FROM @ndoeWash
	INNER JOIN SchoolSurvey SS
		ON SS.schNo = [@ndoeWash].School_No
		AND SS.svyYear = @SurveyYear

Select * from @ndoeWash
-- Write toilet data
DELETE
FROM Toilets
WHERE ssID in (Select ssID from @ndoeWash)

INSERT INTO Toilets
(ssID
, toiType
, toiUse
, toiNum
, toiNumUsable
)
SELECT ssID
, Toilet_Type
, 'Girls'
, Girls_Toilets_Total
, Girls_Toilets_Usable
FROM @ndoeWash Ndoe
WHERE Toilet_Type is not null
AND (Girls_Toilets_Total is not null or Girls_Toilets_Usable is not null)

INSERT INTO Toilets
(ssID
, toiType
, toiUse
, toiNum
, toiNumUsable
)
SELECT ssID
, Toilet_Type
, 'Boys'
, Boys_Toilets_Total
, Boys_Toilets_Usable
FROM @ndoeWash Ndoe
WHERE Toilet_Type is not null
AND (Boys_Toilets_Total is not null or Boys_Toilets_Usable is not null)


INSERT INTO Toilets
(ssID
, toiType
, toiUse
, toiNum
, toiNumUsable
)
SELECT ssID
, Toilet_Type
, 'Common'
, Common_Toilets_Total
, Common_Toilets_Usable
FROM @ndoeWash Ndoe
WHERE Toilet_Type is not null
AND (Common_Toilets_Total is not null or Common_Toilets_Usable is not null)

-- Handwashing - goes in Resources
-- category: Handwashing
---s plit : one of Spoa and Water, Soap only, water only , 'Neither water nor soap'
--- Handwashing Y/N value is stored in Available

DELETE
from Resources
WHERE ssID in (Select ssID from @ndoeWash)
AND resName = 'Handwashing'

INSERT INTO Resources
(
ssID
, resName
, resSplit
, resAvail
)
SELECT
ssID
, 'Handwashing'
, soap_and_water
, case available when 'Yes' then -1 when 'No' then 0 else null end
from @ndoeWash
WHERE (available in ('Yes', 'No') OR soap_and_water is not null)

-- water supply - also goes in resources
-- category: Water Supply
---split : the value in ater Supply column
--- Available value is stored in 'Functioning'
--- So we interpet that the main water supply is 'available'
-- but not 'currently available' => Not functioning
DELETE
from Resources
WHERE ssID in (Select ssID from @ndoeWash)
AND resName = 'Water Supply'


INSERT INTO Resources
(
ssID
, resName
, resSplit
, resAvail
, resFunctioning
)
SELECT
ssID
, 'Water Supply'
, Main_Source_Drinking_Water
, -1		-- available
, case currently_available when 'Yes' then -1 when 'No' then 0 else null end
from @ndoeWash
WHERE (currently_available in ('Yes', 'No') OR Main_Source_Drinking_Water is not null)


end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

