SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 October 2021
-- Description:	Utility to construct Standard and Exam level results
-- in 'Soe' format, from examinations data containing only benchmarks
-- We effectively do an ItemCount - by assuming that each benchmark is derived from a single indicator
-- and the indicator is derived from 4 items.
-- From this we derive the implied ItemCount within the benchmark as being eqqual to the benchmark score -
-- with one subtlety: a benchamrks score of 1 could comr from ItemCount of 0 or ItemCount of 1
-- so to account for this, when aggregating to standard or exam level
-- we allocate .5 as the ItemCount for Benchmark = 1. This gives better looking results: otherwise
-- taking 'ceiling' at all times forces all results upwards.
-- If the exams are subsequently reloaded from orginal Soe format data, this tweak should give better
-- continuity by making the results less divergent.
-- =============================================
CREATE PROCEDURE [pExamAdmin].[ConstructSoeTotals]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- we identify exams not loaded from Soe data as ones with no ExamCandidateItems

-- based on the assumption that every benchmark has one indicator
-- we can update the benchmark records

UPDATE ExamCandidateResults
SET excrCandidateCount = 1
, excrLevelCount = 1
, excrWeight = 1
WHERe exbnchId is not null
ANd excId not in (select excID from ExamCandidateItems)

--- create results for Standards

DELETE FROM ExamCandidateResults
WHERe exstdID is not null
ANd excId not in (select excID from ExamCandidateItems)

-- see similar logic in pExamWrite.LoadSoeExam
INSERT INTO ExamCandidateResults
( excID, B.exstdID, excrLevel
, excrLevelCount
, excrWeight
, excrCandidateCount
)
SELECT * FROM
(
Select excID, B.exstdID, num
, sum(case when num = excrLevel then excrLevelCount end) LevelCount
, sum(convert(decimal(8,6), case when num = excrLevel then excrLevelCount end))/NumBenchmarkPerStd Weight
-- here is where we adjust for Level 1 may be ItemCount 0 or 1
-- this places candidatecount of 1 on the relevant achievement record
, case when num = ceiling(sum(case when excrLevel = 1 then .5 else 1 end * excrCandidateCount * excrLevel)/NumBenchmarkPerStd)
		then 1 else null end CandidateCount
FROM metaNumbers N
CROSS JOIN ExamCandidateResults CR
INNER JOIN ExamBenchmarks B
ON CR.exbnchID = B.exbnchID
	INNER JOIN
	( Select exstdID, convert(decimal(10,6), count(*)) NumBenchmarkPerStd
	from ExamBenchmarks B
	GROUP BY exStdID
	) SubNumBenchmarks
ON B.exStdID = SubNumBenchmarks.exstdID
WHERE N.Num between 1 and 4
	and CR.exbnchID is not null
	and excID not in (Select excID from ExamCandidateItems)

GROUP BY excID, B.exstdID, Num, NumBenchmarkPerStd
) CALC
WHERE COALESCE(LevelCount, Weight, Candidatecount) is not null

ORDER BY excID, exstdID, Num

-- finally exam totals

DELETE FROM ExamCandidateResults
WHERe exCode is not null
ANd excId not in (select excID from ExamCandidateItems)


INSERT INTO ExamCandidateResults
( excID, exCode, excrLevel
, excrLevelCount
, excrWeight
, excrCandidateCount
)
SELECT * FROM
(
Select CR.excID, X.exCode, num
, sum(case when num = excrLevel then excrLevelCount end) LevelCount
, sum(convert(decimal(8,6), case when num = excrLevel then excrLevelCount end))/NumBenchmarks Weight
, case when num = ceiling(sum(case when excrLevel = 1 then .5 else 1 end * excrCandidateCount * excrLevel)/NumBenchmarks)
		then 1 else null end CandidateCount
FROM metaNumbers N
CROSS JOIN ExamCandidateResults CR
INNER JOIN ExamCandidates C
	ON CR.excID = C.excID
INNER JOIN Exams X
	ON C.exID = X.exID
INNER JOIN
	( Select exID, convert(decimal(10,6), count(*)) NumBenchmarks
	from ExamBenchmarks B
	GROUP BY exID
	) SubNumBenchmarks
ON C.exID = SubNumBenchmarks.exID
WHERE N.Num between 1 and 4
	and CR.exbnchID is not null
	and C.excID not in (Select excID from ExamCandidateItems)

GROUP BY CR.excID, X.exCode, Num, NumBenchmarks
) CALC
WHERE COALESCE(LevelCount, Weight, Candidatecount) is not null

ORDER BY excID, exCode, Num


END
GO

