SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Number of teachers at the school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolDataSetRepeaterCount]
	-- Add the parameters for the stored procedure here
	@Year int
	, @SchoolNo nvarchar(50) = null
	, @ClassLevel nvarchar(10) = null
	WITH EXECUTE AS 'pineapples'
AS
BEGIN

	Select schNo
	, isnull(sum(ptSum),0) Rep
	, Estimate
	, ActualssqLevel
	FROM dbo.tfnESTIMATE_BestSurveyEnrolments() EE
		LEFT JOIN PupilTables PT
			ON EE.bestssID = PT.ssID
	WHERE lifeYear = @Year
		AND (SchNo = @SchoolNo or @SchoolNo is null)
		AND (ptLevel = @ClassLevel or @ClassLevel is null)
		AND ptCode = 'REP'

	GROUP BY SchNo, Estimate, ActualssqLevel

END
GO

