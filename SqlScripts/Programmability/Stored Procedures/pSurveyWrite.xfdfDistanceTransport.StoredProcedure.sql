SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	update "mini preschool survey" found in KI primary form
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfDistanceTransport]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Distance / TRansport block
:
  <field name="DT">
      <field name="D">
        <field name="00">
          <field name="00">
            <field name="All">
              <value>111</value>
            </field>
          </field>
          <field name="01">
            <field name="All">
              <value>3</value>
            </field>
          </field>
        </field>
        <field name="01">
          <field name="00">
            <field name="All">
              <value>100</value>
            </field>
          </field>
          <field name="01">
            <field name="All">
              <value>10</value>
            </field>
          </field>
        </field>
        <field name="02">
          <field name="00">
            <field name="All">
              <value>98</value>
            </field>
          </field>
          <field name="01">
            <field name="All">
              <value>6</value>
            </field>
          </field>
        </field>
        <field name="03">
          <field name="00">
            <field name="All" />
          </field>
          <field name="01">
            <field name="All">
              <value>1</value>
            </field>
          </field>
        </field>
      </field>
      <field name="C">
        <field name="00">
          <field name="V">
            <value>On Foot</value>
          </field>
        </field>
        <field name="01">
          <field name="V">
            <value>Transport</value>
          </field>
        </field>
      </field>
      <field name="R">
        <field name="00">
          <field name="V">
            <value>Less than 1 km</value>
          </field>
        </field>
        <field name="01">
          <field name="V">
            <value>1 to 2 km</value>
          </field>
        </field>
        <field name="02">
          <field name="V">
            <value>2 to 3 km</value>
          </field>
        </field>
        <field name="03">
          <field name="V">
            <value>More than 3 km</value>
          </field>
        </field>
      </field>
			...

*/

-- this is simple enough to update with using table variables

begin transaction


begin try

	-- clear any exisitng entries
	DELETE from DistanceTransport
	WHERe ssId = @SurveyID


	INSERT INTO DistanceTransport
	(
		ssID
		, dtCode
		, dtFoot
		, dtTransport
	)
	SELECT
		@SurveyID
		, convert(int,r)
		, foot
		, transport
	FROM OPENXML(@idoc, '/x:field/x:field[@name="D"]/x:field',2)		-- base is the row counter Node name="00","01 etc
	WITH
	(
		r					nvarchar(2)		'@name'
		, foot				int				'x:field[@name="00"]/x:field[@name="All"]/x:value'
		, transport			int				'x:field[@name="01"]/x:field[@name="All"]/x:value'
	) X


exec audit.xfdfInsert @SurveyID, 'Distance-Transport records inserted','DistanceTransport',@@rowcount
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,'DistanceTransport'

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

-- and commit
if @@trancount > 0
	commit transaction


END
GO

