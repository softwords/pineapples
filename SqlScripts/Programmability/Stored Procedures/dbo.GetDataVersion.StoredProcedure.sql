SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDataVersion]
	-- Add the parameters for the stored procedure here
	@TableGroup int = 0,
	@p2 int = 0
AS
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	return the timestamp for a group of tables
-- =============================================

BEGIN
	SET NOCOUNT ON;
	DECLARE @Result binary(8)
	Select @Result = max(TableVersion)  from TableDataVersion
		WHERE TableGroup = @TableGroup
	Select @Result = isnull(@Result,0) + dbo.UserLanguage()
	Select @Result  DataVersion
END
GO
GRANT EXECUTE ON [dbo].[GetDataVersion] TO [public] AS [dbo]
GO

