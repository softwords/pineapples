SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Validate wash sheet
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusValidateWash]
@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- totals by education level

declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8
-- startpos = 8

Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
From @xml.nodes('ListObject') as V(v)


Select @districtID = dID
from Districts WHERE dName = @districtName

declare @counter int	-- for storing @@ROWCOUNT etc

DECLARE @ndoeWash TABLE
(
RowIndex int
--, SchoolYear                 nvarchar(100) NULL
--, State                      nvarchar(100) NULL
, School_Name                nvarchar(100) NULL
, School_No                  nvarchar(100) NULL
--, School_Type                nvarchar(100) NULL
, Main_Source_Drinking_Water nvarchar(100) NULL
, Currently_Available        nvarchar(100) NULL
, Toilet_Type                nvarchar(100) NULL
, Girls_Toilets_Total        nvarchar(100) NULL
, Girls_Toilets_Usable       nvarchar(100) NULL
, Boys_Toilets_Total         nvarchar(100) NULL
, Boys_Toilets_Usable        nvarchar(100) NULL
, Common_Toilets_Total       nvarchar(100) NULL
, Common_Toilets_Usable      nvarchar(100) NULL
, Available                  nvarchar(100) NULL
, Soap_and_Water             nvarchar(100) NULL
-- end genereted code
, fileReference				uniqueidentifier

, ssID						int
)


--- validations table
declare @validations TABLE
(
	field nvarchar(50),
	errorValue nvarchar(100),
	valMsg nvarchar(200),
	severity nvarchar(20)
	, NumRows int
	, FirstRow int
	, LastRow int
)

INSERT INTO @ndoeWash
(
RowIndex
--, SchoolYear
--, State
, School_Name
, School_No
--, School_Type
, Main_Source_Drinking_Water
, Currently_Available
, Toilet_Type
, Girls_Toilets_Total
, Girls_Toilets_Usable
, Boys_Toilets_Total
, Boys_Toilets_Usable
, Common_Toilets_Total
, Common_Toilets_Usable
, Available
, Soap_and_Water
, fileReference
)
Select
v.value('@Index', 'int') [ RowIndex]
--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')                              [State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                                   [School_Type]
, nullif(ltrim(v.value('@Main_Source_Drinking_Water', 'nvarchar(100)')),'')                       [Main_Source_Drinking_Water]
, nullif(ltrim(v.value('@Currently_Available', 'nvarchar(100)')),'')                              [Currently_Available]
, nullif(ltrim(v.value('@Toilet_Type', 'nvarchar(100)')),'')                                      [Toilet_Type]
, nullif(ltrim(v.value('@Girls_Toilets_Total', 'nvarchar(100)')),'')                             [Girls'_Toilets_Total]
, nullif(ltrim(v.value('@Girls_Toilets_Usable', 'nvarchar(100)')),'')                                          [Girls'_Toilets_Usable]
, nullif(ltrim(v.value('@Boys_Toilets_Total', 'nvarchar(100)')),'')                              [Boys'_Toilets_Total]
, nullif(ltrim(v.value('@Boys_Toilets_Usable', 'nvarchar(100)')),'')                             [Boys'_Toilets_Usable]
, nullif(ltrim(v.value('@Common_Toilets_Total', 'nvarchar(100)')),'')                             [Common_Toilets_Total]
, nullif(ltrim(v.value('@Common_Toilets_Usable', 'nvarchar(100)')),'')                                          [Common_Toilets_Usable]
, nullif(ltrim(v.value('@Available', 'nvarchar(100)')),'')                                        [Available]
, nullif(ltrim(v.value('@Soap_and_Water', 'nvarchar(100)')),'')                                   [Soap_and_Water]
-- End Generated Code
, null
-- placeholders for the nomarlsied fields
from @xml.nodes('ListObject/row') as V(v)


--- Validations?

-- water source null
INSERT INTO @Validations
(	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select 'Main Source Drinking Water'
, null
, 'Missing water source'
,'Critical'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeWash
WHERE Main_Source_Drinking_Water is null
GROUP BY Main_Source_Drinking_Water

-- water source unknown
INSERT INTO @Validations
(	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select 'Main Source Drinking Water'
, Main_Source_Drinking_Water
, 'Unknown water source'
,'Critical'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeWash
WHERE Main_Source_Drinking_Water is not null and
Main_Source_Drinking_Water not in (Select mresName
from metaResourceDefs
WHERE mresCat = 'Water Supply')
GROUP BY Main_Source_Drinking_Water

-- toilet type null
INSERT INTO @Validations
(	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select 'Toilet Type'
, null
, 'Missing toilet type'
,'Critical'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeWash
WHERE Toilet_Type is null
GROUP BY Toilet_Type


-- toilet type missing
-- toilet type null
INSERT INTO @Validations
(	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select 'Toilet Type'
, Toilet_Type
, 'unknown toilet type'
,'Critical'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeWash
WHERE Toilet_Type not in (Select ttypName from lkpToiletTypes)
GROUP BY Toilet_Type

-- return the validations
Select *
FROM @Validations

END
GO

