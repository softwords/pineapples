SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 9 10 2021
-- Description:	FTPT and FTE calculations
-- this populates the Teachers node of the VERMPAF Xml file
-- =============================================
CREATE PROCEDURE [warehouse].[_vermTeachers]
	-- Add the parameters for the stored procedure here
	@DistrictCode nvarchar(10) = null
	, @SendASXML int = 0
	, @xmlOut xml  = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @xmlIsced xml
declare @xmlEdLevel xml
declare @xmlSector xml


declare @results TABLE
(
recordType nvarchar(10)
, surveyYear int
, code nvarchar(10)
, genderCode nvarchar(1)
, teachers int
, qual int
, cert int
, qualCert int

, fte decimal(12,6)
, fteQ decimal(12,6)
, fteC decimal(12,6)
, fteQC decimal(12,6)

, ftpt decimal(12,6)
, ftptQ decimal(12,6)
, ftptC decimal(12,6)
, ftptQC decimal(12,6)

, enrol int
)


declare @output TABLE
(
recordType nvarchar(10)
, surveyYear int
, code nvarchar(10)

, teachers int
, qual int
, cert int
, qualCert int

, fte decimal(12,6)
, fteQ decimal(12,6)
, fteC decimal(12,6)
, fteQC decimal(12,6)

, ftpt decimal(12,6)
, ftptQ decimal(12,6)
, ftptC decimal(12,6)
, ftptQC decimal(12,6)

, teachersM int
, qualM int
, certM int
, qualCertM int

, fteM decimal(12,6)
, fteQM decimal(12,6)
, fteCM decimal(12,6)
, fteQCM decimal(12,6)

, ftptM decimal(12,6)
, ftptQM decimal(12,6)
, ftptCM decimal(12,6)
, ftptQCM decimal(12,6)

, teachersF int
, qualF int
, certF int
, qualCertF int

, fteF decimal(12,6)
, fteQF decimal(12,6)
, fteCF decimal(12,6)
, fteQCF decimal(12,6)

, ftptF decimal(12,6)
, ftptQF decimal(12,6)
, ftptCF decimal(12,6)
, ftptQCF decimal(12,6)

, enrolM int
, enrolF int
, enrol int

, teachersPTR decimal(12,6)
, qualPTR decimal(12,6)
, certPTR decimal(12,6)
, qualCertPTR decimal(12,6)

, ftePTR decimal(12,6)
, fteQPTR decimal(12,6)
, fteCPTR decimal(12,6)
, fteQCPTR decimal(12,6)

, ftptPTR decimal(12,6)
, ftptQPTR decimal(12,6)
, ftptCPTR decimal(12,6)
, ftptQCPTR decimal(12,6)
)
-- isced top level

-- isced sub class

-- ed level

INSERT INTO @results
(
recordType
, surveyYear
, code
, genderCode
, teachers
, qual
, cert
, qualCert
, fte
, fteQ
, fteC
, fteQC

, ftpt
, ftptQ
, ftptC
, ftptQC
)
Select
'edLevel'
, SurveyYear
, edLevelCode
, GenderCode
, count(DISTINCT tID) NumTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when Certified = 1 and Qualified = 1 then tID end)  CertQual

, sum(W) FTE
, sum(WQ) FTEQ
, sum(WC) FTEC
, sum(WQC) FTEQC
, sum(WTeach) FTPT
, sum(WTeachQ) FTPTQ
, sum(WTeachC) FTPTC
, sum(WTeachC) FTPTQC


from warehouse.TeacherActivityWeightsEdLevel TAW
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON TAW.SurveyDimensionID = DSS.[Survey ID]
WHERE (DSS.DistrictCode = @DistrictCode or @DistrictCode is null)
group by SurveyYear, edLevelCode, GenderCode

-- ed level enrolment
INSERT INTO @results
(
recordType
, surveyYear
, code
, genderCode
, enrol
)
Select 'edLevel'
, SurveyYear
, edLEvelCode
, Gendercode
, sum(Enrol)
from warehouse.tableEnrol E
INNER JOIN DimensionLevel DL
ON E.classLevel = DL.LevelCode
WHERE ( @DistrictCode = DistrictCode or @DistrictCode is null)
GROUP By SurveyYEar, edLevelCode, Gendercode


-- sector

INSERT INTO @results
(
recordType
, surveyYear
, code
, genderCode
, teachers
, qual
, cert
, qualCert
, fte
, fteQ
, fteC
, fteQC

, ftpt
, ftptQ
, ftptC
, ftptQC
)
Select
'sector'
, SurveyYear
, sectorCode
, GenderCode
, count(DISTINCT tID) NumTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when Certified = 1 and Qualified = 1 then tID end)  CertQual

, sum(W) FTE
, sum(WQ) FTEQ
, sum(WC) FTEC
, sum(WQC) FTEQC
, sum(WTeach) FTPT
, sum(WTeachQ) FTPTQ
, sum(WTeachC) FTPTC
, sum(WTeachC) FTPTQC


from warehouse.TeacherActivityWeightsSector TAW
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON TAW.SurveyDimensionID = DSS.[Survey ID]
WHERE (DSS.DistrictCode = @DistrictCode or @DistrictCode is null)
group by SurveyYear, sectorCode, GenderCode

-- sector enrolment
INSERT INTO @results
(
recordType
, surveyYear
, code
, genderCode
, enrol
)
Select 'sector'
, SurveyYear
, sectorCode
, Gendercode
, sum(Enrol)
from warehouse.tableEnrol E
INNER JOIN DimensionLevel DL
ON E.classLevel = DL.LevelCode
WHERE ( @DistrictCode = DistrictCode or @DistrictCode is null)
GROUP By SurveyYEar, sectorCode, Gendercode

-- isced subclass


-- sector

INSERT INTO @results
(
recordType
, surveyYear
, code
, genderCode
, teachers
, qual
, cert
, qualCert
, fte
, fteQ
, fteC
, fteQC

, ftpt
, ftptQ
, ftptC
, ftptQC
)
Select
'isced'
, SurveyYear
, IscedSubClass
, GenderCode
, count(DISTINCT tID) NumTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when Certified = 1 and Qualified = 1 then tID end)  CertQual

, sum(W) FTE
, sum(WQ) FTEQ
, sum(WC) FTEC
, sum(WQC) FTEQC
, sum(WTeach) FTPT
, sum(WTeachQ) FTPTQ
, sum(WTeachC) FTPTC
, sum(WTeachC) FTPTQC


from warehouse.TeacherActivityWeights TAW
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON TAW.SurveyDimensionID = DSS.[Survey ID]
WHERE (DSS.DistrictCode = @DistrictCode or @DistrictCode is null)
group by SurveyYear, TAW.IscedSubClass, GenderCode

-- isced subclass enrolment
INSERT INTO @results
(
recordType
, surveyYear
, code
, genderCode
, enrol
)
Select 'isced'
, SurveyYear
, [ISCED SubClass]
, Gendercode
, sum(Enrol)
from warehouse.tableEnrol E
INNER JOIN DimensionLevel DL
ON E.classLevel = DL.LevelCode
WHERE ( @DistrictCode = DistrictCode or @DistrictCode is null)
GROUP By SurveyYEar, DL.[ISCED SubClass], Gendercode

INSERT INTO @output
(
recordType
, SurveyYear
, code
, teachersM, teachersF, teachers
, qualM, qualF, qual
, certM, certF, cert
, qualcertM, qualcertF, qualcert

, fteM, fteF, fte
, fteqM, fteqF, fteq
, ftecM, ftecF, ftec
, fteqcM, fteqcF, fteqc

, ftptM, ftptF, ftpt
, ftptqM, ftptqF, ftptq
, ftptcM, ftptcF, ftptc
, ftptqcM, ftptqcF, ftptqc

, enrolM, enrolF, enrol

, ftePTR, fteqPTR, ftecPTR, fteqcPTR
, ftptPTR, ftptqPTR, ftptcPTR, ftptqcPTR
)
Select RecordType
, SurveyYear [@year]
, code [@isced]
, sum(case when GenderCode = 'M' then teachers end) teachersM
, sum(case when GenderCode = 'F' then teachers end) teachersF
, sum(teachers) teachers

, sum(case when GenderCode = 'M' then qual end) qualM
, sum(case when GenderCode = 'F' then qual end) qualF
, sum(qual) qual

, sum(case when GenderCode = 'M' then cert end) certM
, sum(case when GenderCode = 'F' then cert end) certF
, sum(cert) cert

, sum(case when GenderCode = 'M' then qualcert end) qualcertM
, sum(case when GenderCode = 'F' then qualcert end) qualcertF
, sum(qualcert) qualcert

, sum(case when GenderCode = 'M' then FTE end) fteM
, sum(case when GenderCode = 'F' then FTE end) fteF
, sum(FTE) fte

, sum(case when GenderCode = 'M' then FTEQ end) fteqM
, sum(case when GenderCode = 'F' then FTEQ end) fteqF
, sum(FTEQ) fteq

, sum(case when GenderCode = 'M' then FTEC end) ftecM
, sum(case when GenderCode = 'F' then FTEC end) ftecF
, sum(FTEC) ftec

, sum(case when GenderCode = 'M' then FTEQC end) fteqcM
, sum(case when GenderCode = 'F' then FTEQC end) fteqcF
, sum(FTEC) fteqc

, sum(case when GenderCode = 'M' then FTPT end) ftptM
, sum(case when GenderCode = 'F' then FTPT end) ftptF
, sum(FTPT) ftpt

, sum(case when GenderCode = 'M' then FTPTQ end) ftptqM
, sum(case when GenderCode = 'F' then FTPTQ end) ftptqF
, sum(FTPTQ) ftptq

, sum(case when GenderCode = 'M' then FTPTC end) ftptcM
, sum(case when GenderCode = 'F' then FTPTC end) ftptcF
, sum(FTPTC) ftptc


, sum(case when GenderCode = 'M' then FTPTQC end) ftptQCM
, sum(case when GenderCode = 'F' then FTPTQC end) ftptQCF
, sum(FTPTQC) ftptQC

, sum(case when GenderCode = 'M' then Enrol end) enrolM
, sum(case when GenderCode = 'F' then Enrol end) enrolF
, sum(Enrol) enrol

, case when sum(fte) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(fte) end ftePTR
, case when sum(fteq) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(fteq) end fteqPTR
, case when sum(ftec) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(ftec) end fteCPTR
, case when sum(fteqc) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(fteqc) end fteQCPTR

, case when sum(ftpt) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(ftpt) end ftptPTR
, case when sum(ftptq) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(ftptq) end ftptqPTR
, case when sum(ftptc) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(ftptc) end ftptCPTR
, case when sum(ftptqc) = 0 then null else convert(decimal(10,3),sum(Enrol))/sum(ftptqc) end ftptQCPTR

from @results

GROUP BY SurveyYear, recordType, code

if @sendAsXML = 0 begin
	Select * from @output
end
IF @SendASXML <> 0 begin

	Select @xmlISCED =
	( Select SurveyYear [@year]
		, code [@isced]
		, teachersM, teachersF, teachers
		, qualM, qualF, qual
		, certM, certF, cert
		, qualcertM, qualcertF, qualcert

		, fteM, fteF, fte
		, fteqM, fteqF, fteq
		, ftecM, ftecF, ftec
		, fteqcM, fteqcF, fteqc

		, ftptM, ftptF, ftpt
		, ftptqM, ftptqF, ftptq
		, ftptcM, ftptcF, ftptc
		, ftptqcM, ftptqcF, ftptqc

		, enrolM, enrolF, enrol

		, ftePTR, fteqPTR, ftecPTR, fteqcPTR
		, ftptPTR, ftptqPTR, ftptcPTR, ftptqcPTR

--- % qualified certified ftpt
, case when ftpt = 0 then null else (ftptQ)/ftpt end ftptQPerc
, case when ftpt = 0 then null else (ftptC)/ftpt end ftptCPerc
, case when ftpt = 0 then null else (ftptQC)/ftpt end ftptQCPerc

, case when ftptM = 0 then null else (ftptQM)/ftptM end ftptQPercM
, case when ftptM = 0 then null else (ftptCM)/ftptM end ftptCPercM
, case when ftptM = 0 then null else (ftptQCM)/ftptM end ftptQCPercM

, case when ftptF = 0 then null else (ftptQF)/ftptF end ftptQPercF
, case when ftptF = 0 then null else (ftptCF)/ftptF end ftptCPercF
, case when ftptF = 0 then null else (ftptQCF)/ftptF end ftptQCPercF

--- % qualified certified
, case when fte = 0 then null else (fteQ)/fte end fteQPerc
, case when fte = 0 then null else (fteC)/fte end fteCPerc
, case when fte = 0 then null else (fteQC)/fte end fteQCPerc

, case when fteM = 0 then null else (fteQM)/fteM end fteQPercM
, case when fteM = 0 then null else (fteCM)/fteM end fteCPercM
, case when fteM = 0 then null else (fteQCM)/fteM end fteQCPercM

, case when fteF = 0 then null else (fteQF)/fteF end fteQPercF
, case when fteF = 0 then null else (fteCF)/fteF end fteCPercF
, case when fteF = 0 then null else (fteQCF)/fteF end fteQCPercF

--- % qualified certified
, case when teachers = 0 then null else (qual )/teachers end qualPerc
, case when teachers = 0 then null else (cert )/teachers end certPerc
, case when teachers = 0 then null else (qualcert )/teachers end qualcertPerc

, case when teachersM = 0 then null else (qualM )/teachersM end qualPercM
, case when teachersM = 0 then null else (certM )/teachersM end certPercM
, case when teachersM = 0 then null else (qualcertM )/teachersM end qualcertPercM

, case when teachersF = 0 then null else (qualF )/teachersF end qualPercF
, case when teachersF = 0 then null else (certF )/teachersF end teachersCPercF
, case when teachersF = 0 then null else (qualcertF )/teachersF end qualcertPercF
	from @output
	WHERe recordType = 'Isced'
	 FOR XML PATH('Isced')
	 )
	 -- ed level
	 Select @xmlEdLevel =
	( Select SurveyYear [@year]
		, code [@edLevelCode]
		, teachersM, teachersF, teachers
		, qualM, qualF, qual
		, certM, certF, cert
		, qualcertM, qualcertF, qualcert

		, fteM, fteF, fte
		, fteqM, fteqF, fteq
		, ftecM, ftecF, ftec
		, fteqcM, fteqcF, fteqc

		, ftptM, ftptF, ftpt
		, ftptqM, ftptqF, ftptq
		, ftptcM, ftptcF, ftptc
		, ftptqcM, ftptqcF, ftptqc

		, enrolM, enrolF, enrol

		, ftePTR, fteqPTR, ftecPTR, fteqcPTR
		, ftptPTR, ftptqPTR, ftptcPTR, ftptqcPTR

--- % qualified certified ftpt
, case when ftpt = 0 then null else (ftptQ )/ftpt end ftptQPerc
, case when ftpt = 0 then null else (ftptC )/ftpt end ftptCPerc
, case when ftpt = 0 then null else (ftptQC )/ftpt end ftptQCPerc

, case when ftptM = 0 then null else (ftptQM )/ftptM end ftptQPercM
, case when ftptM = 0 then null else (ftptCM )/ftptM end ftptCPercM
, case when ftptM = 0 then null else (ftptQCM )/ftptM end ftptQCPercM

, case when ftptF = 0 then null else (ftptQF )/ftptF end ftptQPercF
, case when ftptF = 0 then null else (ftptCF )/ftptF end ftptCPercF
, case when ftptF = 0 then null else (ftptQCF )/ftptF end ftptQCPercF

--- % qualified certified
, case when fte = 0 then null else (fteQ )/fte end fteQPerc
, case when fte = 0 then null else (fteC )/fte end fteCPerc
, case when fte = 0 then null else (fteQC )/fte end fteQCPerc

, case when fteM = 0 then null else (fteQM )/fteM end fteQPercM
, case when fteM = 0 then null else (fteCM )/fteM end fteCPercM
, case when fteM = 0 then null else (fteQCM )/fteM end fteQCPercM

, case when fteF = 0 then null else (fteQF )/fteF end fteQPercF
, case when fteF = 0 then null else (fteCF )/fteF end fteCPercF
, case when fteF = 0 then null else (fteQCF )/fteF end fteQCPercF

--- % qualified certified
, case when teachers = 0 then null else (qual)/teachers end qualPerc
, case when teachers = 0 then null else (cert *100)/teachers end certPerc
, case when teachers = 0 then null else (qualcert *100)/teachers end qualcertPerc

, case when teachersM = 0 then null else (qualM )/teachersM end qualPercM
, case when teachersM = 0 then null else (certM )/teachersM end certPercM
, case when teachersM = 0 then null else (qualcertM )/teachersM end qualcertPercM

, case when teachersF = 0 then null else (qualF )/teachersF end qualPercF
, case when teachersF = 0 then null else (certF )/teachersF end teachersCPercF
, case when teachersF = 0 then null else (qualcertF )/teachersF end qualcertPercF

	from @output
	WHERe recordType = 'EdLevel'
	 FOR XML PATH('EdLevel')
	 )

	 	 -- sector
	 Select @xmlSector =
	( Select SurveyYear [@year]
		, code [@sectorCode]
		, teachersM, teachersF, teachers
		, qualM, qualF, qual
		, certM, certF, cert
		, qualcertM, qualcertF, qualcert

		, fteM, fteF, fte
		, fteqM, fteqF, fteq
		, ftecM, ftecF, ftec
		, fteqcM, fteqcF, fteqc

		, ftptM, ftptF, ftpt
		, ftptqM, ftptqF, ftptq
		, ftptcM, ftptcF, ftptc
		, ftptqcM, ftptqcF, ftptqc

		, enrolM, enrolF, enrol

		, ftePTR, fteqPTR, ftecPTR, fteqcPTR
		, ftptPTR, ftptqPTR, ftptcPTR, ftptqcPTR

--- % qualified certified ftpt
, case when ftpt = 0 then null else (ftptQ )/ftpt end ftptQPerc
, case when ftpt = 0 then null else (ftptC )/ftpt end ftptCPerc
, case when ftpt = 0 then null else (ftptQC )/ftpt end ftptQCPerc

, case when ftptM = 0 then null else (ftptQM )/ftptM end ftptQPercM
, case when ftptM = 0 then null else (ftptCM )/ftptM end ftptCPercM
, case when ftptM = 0 then null else (ftptQCM )/ftptM end ftptQCPercM

, case when ftptF = 0 then null else (ftptQF )/ftptF end ftptQPercF
, case when ftptF = 0 then null else (ftptCF )/ftptF end ftptCPercF
, case when ftptF = 0 then null else (ftptQCF )/ftptF end ftptQCPercF


--- % qualified certified
, case when fte = 0 then null else (fteQ )/fte end fteQPerc
, case when fte = 0 then null else (fteC )/fte end fteCPerc
, case when fte = 0 then null else (fteQC )/fte end fteQCPerc

, case when fteM = 0 then null else (fteQM )/fteM end fteQPercM
, case when fteM = 0 then null else (fteCM )/fteM end fteCPercM
, case when fteM = 0 then null else (fteQCM )/fteM end fteQCPercM

, case when fteF = 0 then null else (fteQF )/fteF end fteQPercF
, case when fteF = 0 then null else (fteCF )/fteF end fteCPercF
, case when fteF = 0 then null else (fteQCF )/fteF end fteQCPercF

--- % qualified certified
, case when teachers = 0 then null else (qual )/teachers end qualPerc
, case when teachers = 0 then null else (cert )/teachers end certPerc
, case when teachers = 0 then null else (qualcert )/teachers end qualcertPerc

, case when teachersM = 0 then null else (qualM )/teachersM end qualPercM
, case when teachersM = 0 then null else (certM )/teachersM end certPercM
, case when teachersM = 0 then null else (qualcertM )/teachersM end qualcertPercM

, case when teachersF = 0 then null else (qualF )/teachersF end qualPercF
, case when teachersF = 0 then null else (certF )/teachersF end teachersCPercF
, case when teachersF = 0 then null else (qualcertF )/teachersF end qualcertPercF
	from @output
	WHERe recordType = 'Sector'
	 FOR XML PATH('Sector')
	 )

	SELECT @XMLOut =
	(
		Select @xmlIsced, @xmlEdLevel, @xmlSector for xml path('Teachers')
	)

	IF @SendASXML = 1
		SELECT @XMLOut
	end

end
GO

