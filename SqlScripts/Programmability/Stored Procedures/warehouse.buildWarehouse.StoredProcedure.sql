SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2016
-- Description:
-- =============================================
CREATE PROCEDURE [warehouse].[buildWarehouse]
	@StartFromYear int = null			-- FOR FUTURE use support partial rebuild to preserve earlier data
	with EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- get rid of 'Warning: Null value is eliminated by an aggregate or other SET operation.'
	set ansi_warnings off
    -- Insert statements for procedure here


	---- diemensions
	begin transaction
	IF OBJECT_ID('warehouse.dimensionSchoolSurvey', 'U') IS NOT NULL
		DROP TABLE warehouse.dimensionSchoolSurvey;

	SELECT *
	, [District Code] DistrictCode				-- eliminate the need for this annoying rrename downstream
	, AuthorityGroupCode AuthorityGovtCode
	, AuthorityGroup AuthorityGovt
	INTO warehouse.dimensionSchoolSurvey
	FROM DimensionSchoolSurveyNoYear

	ALTER TABLE warehouse.dimensionSchoolSurvey
	-- this schNo comes from listxtab - we can't know in advance the columns we really want from here
	-- so this kludge prevents it being null
	DROP COLUMN schNo
	commit

	begin transaction
	-- dont drop and create this object, becuase we lose its extended properties
	DELETE
	FROM warehouse.bestSurvey
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

	INSERT INTO warehouse.bestSurvey
	(
		SurveyYear
		, schNo
		, surveyDimensionID
	)
	SELECT LifeYear SurveyYear
	, schNo
	, surveyDimensionssID surveyDimensionID
	FROM tfnESTIMATE_BestSurveyEnrolments()
	commit

	--- enrolments
	exec warehouse.buildEnrolments @StartFromYear

	--- population
	exec warehouse.buildPopulation  @StartFromYear

	--- School counts
	exec warehouse.buildSchoolCounts @StartFromYear

	--- disability
	exec warehouse.buildDisability @StartFromYear
	--- special education
	exec warehouse.buildSpecialEducation @StartFromYear

	--- Cohort -- aggregated
	exec warehouse.buildCohort @StartFromYear

	---- school level flow model
	exec warehouse.buildSchoolCohort @StartFromYear

	--- other data
	exec warehouse.buildTeacherLocation
	exec warehouse.buildRoomCounts
	exec warehouse.buildTextbooks

	--- finance and expenditure
	exec warehouse.buildBudgets


--- inspections and accreditations

	exec warehouse.buildInspections

	--- wash
	exec warehouse.buildWash
	exec warehouse.buildWashToilets

---- vermpaf support
	exec warehouse.buildClassLevelER
	exec warehouse.buildEdLevelER
	exec warehouse.buildEdLevelAltER -- Using as convenience for UIS levels of education
	exec warehouse.BuildRank

---- record that the warehouse and changed so clients using ETag will get updated
	exec warehouse.logVersion

--- some sanity checks

exec warehouse.reconcile


END
GO

