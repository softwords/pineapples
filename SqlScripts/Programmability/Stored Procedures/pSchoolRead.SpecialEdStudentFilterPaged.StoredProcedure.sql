SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 03 2018
-- Description:	Filter of student
--              TODO - Add ability to filter by currently enrolled school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SpecialEdStudentFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@StudentID uniqueidentifier = null,
	@StudentCardID nvarchar(20) = null,
    @StudentGiven nvarchar(50) = null,
    @StudentFamilyName  nvarchar(50) = null,
    @StudentDoB  date = null,
	@StudentGender nvarchar(1) = null,
	@StudentEthnicity nvarchar(200) = null,

	@EnrolledAt nvarchar(50) = null,
	@EnrolYear int = null,
	@EnrolLevel nvarchar(10) = null,
	@EnrolDistrict nvarchar(10) = null,
	@EnrolAuthority nvarchar(10) = null,

	-- special ed specific
	 @Disability nvarchar(20) = null,
	 @EnglishLearner nvarchar(10)= null,
	 @Environment nvarchar(20)= null,
	 @CaseManager nvarchar(100) = null

AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id uniqueidentifier PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pSchoolRead.SpecialEdStudentFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

		, @StudentID
	    , @StudentCardID
        , @StudentGiven
        , @StudentFamilyName
        , @StudentDoB
		, @StudentGender
		, @StudentEthnicity

		, @EnrolledAt
		, @EnrolYear
		, @EnrolLevel
		, @EnrolDistrict
		, @EnrolAuthority
			-- special ed specific
		, @Disability
		, @EnglishLearner
		, @Environment
		, @CaseManager


--- column sets ----
	SELECT S.stuID
	, S.stuCardID
	, S.stuGiven
	, S.stuFamilyName
	, S.stuDoB
	, S.stuGender
	, S.stuEthnicity
	, S.stueSpEdDisability
	, S.stueSpEdEnglish
	, S.stueSpEdEnv
	FROM SpecialEdStudent_ S
		INNER JOIN @keys K
			ON S.stuID = K.ID
	ORDER BY recNo


--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

