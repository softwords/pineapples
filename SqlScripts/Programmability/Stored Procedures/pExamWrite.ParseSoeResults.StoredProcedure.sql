SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brian Lewis
-- Create date: 2021
-- Description:	Returns a resultset of 3 columns:
-- row - the index number of the row
-- item -- the column name 'Item_xxxx'
-- response : the response on that row
-- =============================================
CREATE PROCEDURE [pExamWrite].[ParseSoeResults]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- list of the items
declare @Items TABLE
(
	itemName nvarchar(100)
)

INSERT INTO @items
Select * from
(
Select distinct r.value('local-name(.)', 'nvarchar(100)') item
from @xml.nodes('ListObject/row/@*') V(r)
) SUB
WHERE item like 'item%'
ORDER BY item

declare @itemstr nvarchar(max) = ''
declare @cols nvarchar(max) =''

-- comma delimited string of the item names
Select @itemstr = @itemstr +
	case when @itemstr = '' then '' else ', ' end + itemName
from @Items

declare @sql nvarchar(max) = ''
declare @sql2 nvarchar(max) = ''

-- build the dynamic sql
-- the dynamic table definition
Select @sql = 'row int '
Select @sql = @sql + ', ' + itemName + ' nvarchar(max)'
from @items

-- dynamic xml parsing based on the item names found

Select @sql2 = 'r.value(''@Index'', ''int'') Row'

Select @sql2 = @sql2 + ', '  + 'r.value(''@' + itemName + ''', ''nvarchar(max)'') ' + itemName
from @items

--  declare the table
Select @sql =
'DECLARE @t TABLE
(' +  @sql
+ '
) '

-- insert into that table from the xml
Select @sql = @sql +
'
INSERT INTO @t
SELECT '
+ @sql2
+ ' FROM @x.nodes(''//row'') R(r) '

-- construct the UNPIVOT to normalise the item names into item, response columns
Select @sql = @sql +
'
SELECT row, item, result FROM (Select * from @t ) T UNPIVOT
( result for item in
('+ @itemstr + ')
) as unpvt
'
-- for debugging
--select @sql

/*
Item_001_AS0302010101e_aaa
Info	Start Length
Item Serial Number	6	3
Subject	10	1
S or C	11	1
Grade	12	2
STD	14	2
BM	16	2
IND	18	2
ItemInBM	20	2
e/m/d	22	1
Answer Key (or ZZZ for C item)	24	1	<= 1 is OK even if it is 3
*/

declare @n TABLE
(
	[row] int
	, item nvarchar(100)
	, answer nvarchar(100)
)

-- execute that which will return the result
--INSERT INTO @n
exec sp_executesql @sql, N'@x xml', @x=@xml

END
GO

