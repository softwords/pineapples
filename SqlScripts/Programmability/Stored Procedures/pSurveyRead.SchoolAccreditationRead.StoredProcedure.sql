SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis / Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	return a single school accreditation
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SchoolAccreditationRead]
	-- Add the parameters for the stored procedure here
	@SAID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON    -- Insert statements for procedure here

	Select inspID
	, schNo
	, schName
	, StartDate
	, EndDate
	, InspectionYear
	, InspectedBy
	, inspTypeCode
	, InspectionType
	, InspectionResult
	, SourceId
	, InspectionClass
	, Partition
	, pCreateTag
	, pCreateUser
	, pCreateDateTime
	, pEditUser
	, pEditDateTime
	, InspectionContent
	, X.Summary summary
FROM pInspectionRead.SchoolInspections
CROSS APPLY dbo.SummariseSSA(InspectionContent) X
WHERE inspID = @SAID
FOR XML PATH('accreditation')
--WHERE inspID = @SAID
--FOR XML PATH('accreditation')
	 --SELECT * from pInspectionRead.SchoolAccreditations
	 --WHERE inspID = @SAID

END
GO

