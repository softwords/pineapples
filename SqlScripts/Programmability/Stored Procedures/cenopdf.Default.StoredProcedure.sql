SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 6 2021
-- Description:	Proc to customize generation of pdf using
--				cenopdf - this default does not set field attributes
--				but passes the form type and target file
-- Refer to http://www.lystech.com/webhelp/default.htm 'Exporting to PDF from a Database'
-- =============================================
CREATE PROCEDURE [cenopdf].[Default]
	@formtype nvarchar(10),	-- either PRI or SEC
	@targetFile nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @schoolType nvarchar(10)
	declare @ClassLevelItems nvarchar(max)
	declare @TeacherRoleItems nvarchar(max)

	Select @schoolType = case @formType when 'PRI' then 'P' when 'SEC' then 'CS' end
	select @ClassLevelItems = cenopdf.ClassLevelItems(@SchoolType)
	select @TeacherRoleItems = cenopdf.TeacherRoleItems(@SchoolType)

	Select
	@targetFile targetFile


END
GO

