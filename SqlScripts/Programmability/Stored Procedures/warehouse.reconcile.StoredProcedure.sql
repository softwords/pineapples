SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 9 2018
-- Description:	PRovide a reconciliation of key warehousenumbers, taken from different sources
-- =============================================
CREATE PROCEDURE [warehouse].[reconcile]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- suppress all these
	-- Warning: Null value is eliminated by an aggregate or other SET operation.
	SET ANSI_WARNINGS OFF;


	DECLARE @enrol TABLE
	(
	svyYear int
	, tableEnrol int
	, enrol int
	, EdLevelEr int
	, EdLevelErDistrict int
	, ClassLevelEr int
	, ClassLevelErDistrict int
	, enrolmentRatios int
	, enrolmentRatiosAlt int
	, enrolmentRatiosAlt2 int
	, EdLevelAge int
	, DistrictEdLevelAge int
	, schoolCohort int
	, nationCohort int
	, districtCohort int
	, schoolFlow int
	, nationFlow int
	, districtFlow int

	)

    -- Insert statements for procedure here
	INSERT INTO @enrol
	(svyYEar, tableEnrol)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.tableEnrol
	GROUP BY surveyYear

	    -- Insert statements for procedure here
	INSERT INTO @enrol
	(svyYEar, enrol)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.Enrol
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, edLevelEr)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.EdLevelEr
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, edLevelErDistrict)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.EdLevelErDistrict
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, ClassLevelEr)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.ClassLevelEr
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, ClassLevelErDistrict)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.ClassLevelErDistrict
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, enrolmentRatios)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.enrolmentRatios
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, enrolmentRatiosAlt)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.enrolmentRatiosAlt
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, enrolmentRatiosAlt2)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.enrolmentRatiosAlt2
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, edLevelAge)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.EdLevelAge
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, DistrictEdLevelAge)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.EdLevelAgeDistrict
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, SchoolCohort)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.SchoolCohort
	GROUP BY surveyYear


	INSERT INTO @enrol
	(svyYEar, DistrictCohort)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.CohortDistrict
	WHERE GenderCode is not null
	GROUP BY surveyYear


	INSERT INTO @enrol
	(svyYEar, NationCohort)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.CohortNation
	WHERE GenderCode is not null
	GROUP BY surveyYear

	INSERT INTO @enrol
	(svyYEar, SchoolFlow)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.FlowSchool
	GROUP BY surveyYear


	INSERT INTO @enrol
	(svyYEar, DistrictFlow)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.FlowDistrict
	WHERE GenderCode is not null
	GROUP BY surveyYear


	INSERT INTO @enrol
	(svyYEar, NationFlow)
	SElect SurveyYear, sum(enrol)
	FROM warehouse.FlowNation
	WHERE GenderCode is not null
	GROUP BY surveyYear

	Select svyYear
	, sum(tableEnrol) tableEnrol
	, sum(enrol) Enrol
	, sum(EdLevelEr) EdLevelEr
	, sum(EdLevelErDistrict) EdLevelErDistrict
	, sum(ClassLevelEr) ClassLevelEr
	, sum(ClassLevelErDistrict) ClassLevelErDistrict
	, sum(enrolmentRatios) enrolmentRatios
	, sum(enrolmentRatiosAlt) enrolmentRatiosAlt
	, sum(enrolmentRatiosAlt2) enrolmentRatiosAlt2
	, sum(EdLevelAge) EdLevelAge
	, sum(DistrictEdLevelAge) DistrictEdLevelAge
	, sum(schoolCohort) SchoolCohort
	, sum(DistrictCohort) DistrictCohort
	, sum(NationCohort) NationCohort
	, sum(schoolFlow) SchoolFlow
	, sum(DistrictFlow) DistrictFlow
	, sum(NationFlow) NationFlow
	from @enrol
	GROUP BY svyYear
	ORDER BY svyYear


	-- now do teachers!
	DECLARE @teachers TABLE
	(
	svyYear int
	, NationTeacherCount int
	, NationPupilTeacherRatio int
	, DistrictTeacherCount int
	, DistrictPupilTeacherRatio int

	, TeacherCount int
	, TeacherCountAudit int
	, SchoolTeachers int
	, SchoolTeacherCount int
	, TeacherLocation int
	, TeacherLocationBySurvey int
	, TeacherSurvey	int				-- number of records in the operational TeacherSurvey table
	)

	INSERT INTO @teachers
	(svyYEar, NationTeacherCount)
	SElect SurveyYear, sum(NumTeachers)
	FROM warehouse.TeacherCountNation
	GROUP BY surveyYear

	INSERT INTO @teachers
	(svyYEar, DistrictTeacherCount)
	SElect SurveyYear, sum(NumTeachers)
	FROM warehouse.TeacherCountDistrict
	GROUP BY surveyYear

	INSERT INTO @teachers
	(svyYEar, SchoolTeacherCount)
	SElect SurveyYear, sum(NumTeachers)
	FROM warehouse.TeacherCountSchool
	GROUP BY surveyYear

	INSERT INTO @teachers
	(svyYear, SchoolTeachers)
	SElect SurveyYear, sum(NumTeachers)
	FROM warehouse.SchoolTeachers
	GROUP BY SurveyYear

	INSERT INTO @teachers
	(svyYear, TeacherCount)
	SElect SurveyYear, sum(NumTeachers)
	FROM warehouse.TeacherCountTable
	GROUP BY SurveyYear

	INSERT INTO @teachers
	(svyYear, TeacherCountAudit)
	SElect SurveyYear, sum(NumTeachers)
	FROM warehouse.TeacherCountAudit
	GROUP BY SurveyYear

	INSERT INTO @teachers
	(svyYear, TeacherLocation)
	SElect SurveyYear, count( tID)		-- one record per teacher per year in here
	FROM warehouse.TeacherLocation
	GROUP BY surveyYear

	INSERT INTO @teachers
	(svyYear, TeacherLocationBySurvey)
	SElect SurveyYear, count( tID)
	FROM warehouse.TeacherLocation
	WHERE Source not in ('A','A?')
	GROUP BY surveyYear

	INSERT INTO @teachers
	(svyYear, TeacherSurvey)
	Select svyYear, count( tID)
	FROM TeacherSurvey TS
		INNER JOIN SchoolSurvey SS
			ON TS.ssID = SS.ssID
	GROUP BY svyYear

	Select svyYear
	, sum(NationTeacherCount) NationTeacherCount
	, sum(DistrictTeacherCount) DistrictTeacherCount
	, sum(SchoolTeacherCount) SchoolTeacherCount
	, sum(TeacherCount) TeacherCount
	, sum(TeacherCountAudit) TeacherCountAudit
	, sum(SchoolTeachers) SchoolTeachers
	, sum(TeacherLocation) TeacherLocation
	, sum(TeacherLocationBySurvey) TeacherLocationBySurvey
	, sum(TeacherSurvey) as TeacherSurvey
	from @teachers
	GROUP BY svyYear
	ORDER BY svyYear
END
GO

