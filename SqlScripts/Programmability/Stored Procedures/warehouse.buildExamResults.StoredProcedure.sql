SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 10 2017
-- Description:	Rebuild the examination warehouse tables
--              ExamSchoolREsults and ExamStateResults
--              The tables are rebuilt from ExamCandidates and ExamCandidatereSults
-- ie this relies on the NEW (FSM) Xml loaded exam structure for Standard Tests of Achievement
-- ARGS: @examID - delete and reinstate this single examID
-- in this mode, it is called from loadExam - we may as well recreate the warehouse table as soon as batch loading the exam?
-- @year - recalculate every exam from this year forward - this will become the standard for warehouse regeneration
--
-- History Logs:
--   * 2021, Brian  Lewis, updated lots of this to be "SOE" compatible
--   * 08/04/2022, Ghislain Hachey, added support to build warehouse.ExamCandidateResultsTyped
-- =============================================
CREATE PROCEDURE [warehouse].[buildExamResults]
@examID int = null
, @baseYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- first clear the record we need to clear from ExamSchoolResults
DELETE
From warehouse.ExamSchoolResults
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)
print 'warehouse.ExamSchoolResults deletes - rows:' + convert(nvarchar(10), @@rowcount)


DELETE
From warehouse.ExamStateResults
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)
print 'warehouse.ExamStateResults deletes - rows:' + convert(nvarchar(10), @@rowcount)


DELETE
From warehouse.ExamSchoolResultsTyped
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)
print 'warehouse.ExamSchoolResultsTyped deletes - rows:' + convert(nvarchar(10), @@rowcount)


DELETE
From warehouse.ExamTableResultsTyped
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)
print 'warehouse.ExamTableResultsTyped deletes - rows:' + convert(nvarchar(10), @@rowcount)

DELETE
From warehouse.ExamCandidateResultsTyped
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)
print 'warehouse.ExamCandidateResultsTyped deletes - rows:' + convert(nvarchar(10), @@rowcount)

-- rebuild school results from base tables
INSERT INTO warehouse.examSchoolResults
([examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]
,[schNo]
,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[benchmarkDesc]
,[achievementLevel]
,[achievementDesc]
,[Candidates])
Select
X.exID					examID
, X.exCode				examCode
, X.exYear				examYear
, XT.exName				examName
, C.dID					StateID
, D.dName				[State]
, C.schNo				schNo
, C.excGender			Gender
, B.exstdID				standardID
, STD.exstdCode			standardCode
, STD.exstdDescription	standardDesc
, R.exbnchID			benchmarkID
, B.exbnchCode			benchmarkCode
, B.exbnchDescription	benchmarkDesc
, R.excrLevel			achievementLevel
, A.exalDescription		achievementDesc
, count(R.excrID)		Candidates
from ExamCandidateResults R
INNER JOIN ExamCandidates C
	ON R.excID = C.excID
INNER JOIN Exams X
	ON C.exID = X.exID
INNER JOIN lkpExamTypes XT
	ON X.exCode = XT.exCode
INNER JOIN Districts D
	ON D.dID = C.dID
INNER JOIN examBenchmarks B
	ON R.exbnchID = B.exbnchID
INNER JOIN examStandards STD
	ON B.exstdID = STD.exstdID
INNER JOIN examAchievementLevels A
	ON A.exID = C.exID
	AND A.exalVal = R.excrLevel
WHERE
	(X.exYear >= @baseYear or @baseYear is null)
	AND (X.exID = @examID or @examID is null)

GROUP BY
X.exID
, X.exCode
, X.exYear
, XT.exName
, C.dID
, D.dName
, C.schNo
, C.excGender
, B.exstdID
, STD.exstdCode
, STD.exstdDescription
, R.exbnchID
, B.exbnchCode
, B.exbnchDescription
, R.excrLevel
, A.exalDescription

print 'warehouse.ExamSchoolResults inserts - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.ExamStateResults
(
[examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]

,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[benchmarkDesc]
,[achievementLevel]
,[achievementDesc]
,[Candidates]
)
SELECT
[examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]

,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[benchmarkDesc]
,[achievementLevel]
,[achievementDesc]
,sum(Candidates) Candidates
FROM warehouse.ExamSchoolResults
WHERE
	(examYear >= @baseYear or @baseYear is null)
	AND (examID = @examID or @examID is null)
GROUP BY
[examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]

,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[benchmarkDesc]
,[achievementLevel]
,[achievementDesc]

print 'warehouse.ExamStateResults inserts - rows:' + convert(nvarchar(10), @@rowcount)

-- 2021 enhancements
INSERT INTO warehouse.ExamSchoolResultsTyped
(
examID
      ,examCode
      ,examYear
      ,examName
	  , schNo
	  , schoolName
      ,Gender
      ,DistrictCode
      ,District
      ,SchoolTypeCode
      ,SchoolType
	  , AuthorityCode
	  , Authority
	  , AuthorityGovtCode
	  , AuthorityGovt
      ,RecordType
      ,ID
      ,[Key]
      ,[Description]
      ,achievementLevel
      ,achievementDesc
      , IndicatorCount
      , weight
      , CandidateCount
	)
SELECT examID
      ,examCode
      ,examYear
      ,examName
	  , schNo
	  , schoolName
      ,Gender
      ,DistrictCode
      ,District
      ,SchoolTypeCode
      ,SchoolType
	  , AuthorityCode
	  , Authority
	  , AuthorityGovtCode
	  , AuthorityGovt
      ,RecordType
      ,ID
      ,[Key]
      ,[Description]
      ,achievementLevel
      ,achievementDesc
      , IndicatorCount
      , weight
      , CandidateCount
FROM pExamRead.SchoolLevelTyped
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)
AND (isnull(IndicatorCount,0) > 0 or isnull(CandidateCount,0) > 0)

print 'warehouse.ExamSchoolResultsTyped inserts - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.ExamTableResultsTyped
(
examID
      ,examCode
      ,examYear
      ,examName
      ,Gender
      ,DistrictCode
      ,District
      ,SchoolTypeCode
      ,SchoolType
      ,AuthorityCode
      ,Authority
      ,AuthorityGovtCode
      ,AuthorityGovt
      ,RecordType
      ,ID
      ,[Key]
      ,Description
      ,achievementLevel
      ,achievementDesc
      , indicatorCount
      , weight
      , candidateCount
)
SELECT
examID
      ,examCode
      ,examYear
      ,examName
      ,Gender
      ,DistrictCode
      ,District
      ,SchoolTypeCode
      ,SchoolType
      ,AuthorityCode
      ,Authority
      ,AuthorityGovtCode
      ,AuthorityGovt
      ,RecordType
      ,ID
      ,[Key]
      ,Description
      ,achievementLevel
      ,achievementDesc
      ,sum(IndicatorCount) indicatorCount
      ,sum(weight) weight
      ,sum(candidateCount) candidateCount
  FROM warehouse.ExamSchoolResultsTyped
  WHERE (examID = @examID or @examID is null)

GROUP BY
examID
      ,examCode
      ,examYear
      ,examName
      ,Gender
      ,DistrictCode
      ,District
      ,SchoolTypeCode
      ,SchoolType
      ,AuthorityCode
      ,Authority
      ,AuthorityGovtCode
      ,AuthorityGovt
      ,RecordType
      ,ID
      ,[Key]
      ,Description
      ,achievementLevel
      ,achievementDesc

print 'warehouse.ExamTableResultsTyped inserts - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO [warehouse].[ExamCandidateResultsTyped]
(
excID
      ,studentID
      ,excGiven
      ,excMiddleNames
      ,excFamilyName
      ,examID
      ,examCode
      ,examYear
      ,examName
      ,schNo
      ,Gender
      ,RecordType
      ,ID
      ,[Key]
      ,[Description]
      ,achievementLevel
      ,achievementDesc
      ,IndicatorCount
      ,weight
      ,CandidateCount
	)
SELECT CandidateID
      ,stuID
      ,GivenName
      ,NULL
      ,FamilyName
      ,examID
      ,examCode
      ,examYear
      ,examName
      ,schNo
      ,Gender
      ,RecordType
      ,ID
      ,[Key]
      ,[Description]
      ,achievementLevel
      ,achievementDesc
      ,IndicatorCount
      ,weight
      ,CandidateCount
FROM pExamRead.CandidateLevelTyped
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)
AND (isnull(IndicatorCount,0) > 0 or isnull(CandidateCount,0) > 0)

print 'warehouse.ExamCandidateResultsTyped inserts - rows:' + convert(nvarchar(10), @@rowcount)

---- record that the warehouse and changed so clients using ETag will get updated
	exec warehouse.logVersion
END
GO

