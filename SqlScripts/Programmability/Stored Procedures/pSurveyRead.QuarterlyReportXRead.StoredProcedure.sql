SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	return a single quarterly report extended version
-- =============================================
CREATE PROCEDURE [pSurveyRead].[QuarterlyReportXRead]
	-- Add the parameters for the stored procedure here
	@QRID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON    -- Insert statements for procedure here

	 SELECT * from pInspectionRead.QuarterlyReportsX
	 WHERE qrID = @QRID

END
GO

