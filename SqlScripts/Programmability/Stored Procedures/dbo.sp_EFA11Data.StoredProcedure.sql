SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2007
-- Description:	Core data for calculation of EFA 11. Used by EFA11 query.
-- =============================================
-- -1 = full details including dimensions
--  0 = usual return of data
--  1 = national summary
CREATE PROCEDURE [dbo].[sp_EFA11Data]
	@Consolidation int = 0	,
	@SendAsXML int = 0,			-- only used when consolidation = 1
	@xmlout xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()

Select *
into #ebst
from dbo.tfnESTIMATE_BestSurveyTeachers(DEFAULT,DEFAULT)

SELECT
sub.[Survey Year],
case @Consolidation
	when -1 then schNo
	when 0 then schNo
	when 1 then null
end schNo,

case @Consolidation
	when -1 then isnull(surveyDimensionssID,TsurveyDimensionssID)
	when 0 then isnull(surveyDimensionssID,TsurveyDimensionssID)
	when 1 then null
end  surveyDimensionssID,
sub.SectorCode,

Sum(sub.EnrolM) AS EnrolM,
Sum(sub.enrolF) AS enrolF,
Sum(sub.Enrol) AS Enrol,
Sum(sub.Total) AS Total,
Sum(sub.NumQualifiedM) AS NumQualifiedM,
Sum(sub.NumQualifiedF) AS NumQualifiedF,
Sum(sub.NumQualified) AS NumQualified,

Sum(sub.NumCertifiedM) AS NumCertifiedM,
Sum(sub.NumCertifiedF) AS NumCertifiedF,
Sum(sub.NumCertified) AS NumCertified,

Sum(sub.QualifiedSalary) AS QualifiedSalary,
Sum(sub.CertifiedSalary) AS CertifiedSalary,
Sum(sub.MaleTeachers) AS MaleTeachers,
Sum(sub.FemaleTeachers) AS FemaleTeachers,
Sum(sub.TotalAgeMale) AS TotalAgeMale,
Sum(sub.TotalAgeFemale) AS TotalAgeFemale,
Sum(sub.Fulltime) AS Fulltime,
Sum(sub.FullTimeM) AS FullTimeM,
Sum(sub.FullTimeF) AS FullTimeF,
Sum(sub.FTE) AS FTE,
Sum(sub.FTEM) AS FTEM,
Sum(sub.FTEF) AS FTEF,
-- added TAM 23 11 2008
Sum(sub.TAM) AS TeachingDuties,
Sum(sub.TAMM) AS TeachingDutiesM,
Sum(sub.TAMF) AS TeachingDutiesF,
-- adn the product
-- added TAM 23 11 2008
Sum(sub.TAMFTE) AS DutiesFTE,
Sum(sub.TAMFTEM) AS DutiesFTEM,
Sum(sub.TAMFTEF) AS DutiesFTEF,

Sum(sub.GovTeacher) AS GovTeacher,
Sum(sub.GovTeacherM) AS GovTeacherM,
Sum(sub.GovTeacherF) AS GovTeacherF,

Max(case [sub].[Enrolment Estimate]
		when 9999 then Null
		else [sub].[Enrolment Estimate]
	end) AS [Enrolment Estimate],
Max(case [sub].[Year of Enrolment Data]
		when 9999 then Null
		else [sub].[Year of Enrolment Data]
	end) AS [Year of Enrolment Data],
Max(case [sub].[Age of Enrolment Data]
		when 9999 then Null
		else [sub].[Age of Enrolment Data]
	end) AS [Age of Enrolment Data],

Max(case [sub].[Teacher Estimate]
		when 9999 then Null
		else [sub].[Teacher Estimate]
	end) AS [Teacher Estimate],
Max(case [sub].[Year of Teacher Data]
		when 9999 then Null
		else [sub].[Year of Teacher Data]
	end) AS [Year of Teacher Data],
Max(case [sub].[Age of Teacher Data]
		when 9999 then Null
		else [sub].[Age of Teacher Data]
	end) AS [Age of Teacher Data]

into #tmpT

FROM
(
	SELECT
	E.LifeYear as [Survey Year],
	E.schNo,
	E.surveyDimensionssID,
	SS.SectorCode,
	SS.EnrolM, SS.enrolF, SS.Enrol,
	0 AS Total,
	0 AS NumQualifiedM,
	0 AS NumQualifiedF,
	0 AS NumQualified,

	0 AS NumCertifiedM,
	0 AS NumCertifiedF,
	0 AS NumCertified,

	0 AS QualifiedSalary,
	0 AS CertifiedSalary,
	0 AS MaleTeachers, 0 AS FemaleTeachers,
	0 AS TotalAgeMale, 0 AS TotalAgeFemale,
	0 AS Fulltime, 0 AS FullTimeM, 0 AS FullTimeF,
	0 AS FTE, 0 AS FTEM, 0 AS FTEF,
	0 AS TAM, 0 AS TAMM, 0 AS TAMF,
	0 AS TAMFTE, 0 AS TAMFTEM, 0 AS TAMFTEF,

	0 AS GovTeacher, 0 AS GovTeacherM, 0 AS GovTeacherF,
	E.Estimate as [Enrolment Estimate],
	E.bestYear as [Year of Enrolment Data],
	E.offset as [Age of Enrolment Data],
	0 as TsurveyDimensionssID,
	9999 as [Teacher Estimate],
	9999 as [Year of Teacher Data],
	9999 as [Age of Teacher Data]

	FROM #ebse AS E
	INNER JOIN pEnrolmentRead.ssidEnrolmentSector AS SS
	  ON E.bestssID = SS.ssID


	UNION SELECT
	E.LifeYear as [Survey Year],
	E.schNo,
	null,
	TSEC.SectorCode,
	0 AS EnrolM, 0 AS EnrolF, 0 AS Enrol,
	TSEC.Total,
	TSEC.NumQualifiedM,
	TSEC.NumQualifiedF,
	TSEC.NumQualified,
	TSEC.NumCertifiedM,
	TSEC.NumCertifiedF,
	TSEC.NumCertified,
	TSEC.QualifiedSalary,
	TSEC.CertifiedSalary,
	TSEC.MaleTeachers, TSEC.FemaleTeachers,
	TSEC.TotalAgeMale, TSEC.TotalAgeFemale,
	TSEC.FullTime, TSEC.FullTimeM, TSEC.FullTimeF,
	TSEC.FTE, TSEC.FTEM, TSEC.FTEF,
	TSEC.TAM, TSEC.TAMM, TSEC.TAMF,
	TSEC.TAMFTE, TSEC.TAMFTEM, TSEC.TAMFTEF,

	TSEC.GovTeacher, TSEC.GovTeacherM, TSEC.GovTeacherF,
	9999 as [Enrolment Estimate],
	9999 as [Year of Enrolment Data],
	9999 as [Age of Enrolment Data],
	E.surveydimensionssID,
	E.Estimate as [Teacher Estimate],
	E.bestYear as [Year of Teacher Data],
	E.offset as [Age of Teacher Data]
	FROM #ebst AS E
	INNER JOIN ssIDTeacherSummarySector AS TSEC
	  ON E.bestssID = TSEC.ssID
) sub

GROUP BY [Survey Year]
, case @Consolidation
	when -1 then schNo
	when 0 then schNo
	when 1 then null
end

, case @Consolidation
	when -1 then isnull(surveyDimensionssID,TsurveyDimensionssID)
	when 0 then isnull(surveyDimensionssID,TsurveyDimensionssID)
	when 1 then null
end
, SectorCode


If (@Consolidation = -1) begin
	Select T.*
		, R.[District Rank]
		, R.[District Decile]
		, R.[District Quartile]
		, R.[Rank]
		, R.[Decile]
		, R.[Quartile]
	from #tmpT T
		left join DimensionRank R
		on T.[Survey Year] = R.svyYear
			and T.SchNo = R.SchNo
end

-- default support sdetailed client side assm,ebly
If (@Consolidation = 0) begin
	Select T.*
		, R.[District Rank]
		, R.[District Decile]
		, R.[District Quartile]
		, R.[Rank]
		, R.[Decile]
		, R.[Quartile]
	from #tmpT T
		left join DimensionRank R
		on T.[Survey Year] = R.svyYear
			and T.SchNo = R.SchNo
end

If (@Consolidation = 1) begin


	if @SendAsXML = 0
		Select T.*
			, case when isnull(total,0) = 0 then null
				else	cast(NumCertified as float) / Total
			  end CertifiedPerc
			, case when isnull(total,0) = 0 then null
				else cast(NumQualified as float) / Total
			  end  QualifiedPerc
			, case when isnull(total,0) = 0 then null
				else cast(Enrol as float) / Total
			  end  PTR
			, case when isnull(NumCertified,0) = 0 then null
					else cast(Enrol as float) / NumCertified
			  end CertifiedPTR
			, case when isnull(NumQualified,0) = 0 then null
					else cast(Enrol as float) / NumQualified
			  end QualifiedPTR

		from #tmpT T


	if @SendAsXML <> 0 begin
		declare @xml xml
		SELECT @xml =
		(
		Select [Survey Year] [@year]
			, sectorCode		[@sectorCode]
			, Enrol
			, MaleTeachers		TeachersM
			, FemaleTeachers	TeachersF
			, total				Teachers
			, NumQualifiedM		QualM
			, NumQualifiedF		QualF
			, NumQualified		Qual
			, NumCertifiedM		CertM
			, NumCertifiedF		CertF
			, NumCertified		[Cert]

			, case when isnull(MaleTeachers,0) = 0 then null
				else cast(NumCertifiedM as float) / MaleTeachers
				end CertPercM
			, case when isnull(FemaleTeachers,0) = 0 then null
				else cast(NumCertifiedF as float) / FemaleTeachers
				end CertPercF
			, case when isnull(total,0) = 0 then null
				else	cast(NumCertified as float) / Total
			  end CertPerc


			, case when isnull(MaleTeachers,0) = 0 then null
				else cast(NumQualifiedM as float) / MaleTeachers
				end QualPercM
			, case when isnull(FemaleTeachers,0) = 0 then null
				else cast(NumQualifiedF as float) / FemaleTeachers
				end QualPercF

			, case when isnull(total,0) = 0 then null
				else cast(NumQualified as float) / Total
			  end  QualPerc

			, case when isnull(total,0) = 0 then null
				else cast(Enrol as float) / Total
			  end  PTR
			, case when isnull(NumCertified,0) = 0 then null
					else cast(Enrol as float) / NumCertified
			  end CertPTR
			, case when isnull(NumQualified,0) = 0 then null
					else cast(Enrol as float) / NumQualified
			  end QualPTR
		from #tmpT T
		FOR XML PATH('TeacherQC')
		)
		SELECT @xmlOut =
		(
			SELECT @xml
			FOR XML PATH('TeacherQCs')
		)

		if @SendAsXML = 1
			SELECT @xmlOut
	end

end


END
GO
GRANT EXECUTE ON [dbo].[sp_EFA11Data] TO [pSchoolRead] AS [dbo]
GO

