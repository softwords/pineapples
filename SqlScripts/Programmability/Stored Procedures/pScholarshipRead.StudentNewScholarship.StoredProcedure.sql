SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2020
-- Description: Create a new scholarship record populating Student fields
-- =============================================
CREATE PROCEDURE [pScholarshipRead].[StudentNewScholarship]
	@studentID uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;


-- from student_
SELECT ST.stuID
	  ,stuCardID
      ,stuNamePrefix
      ,stuGiven
      ,stuMiddleNames
      ,stuFamilyName
      ,stuNameSuffix
      ,stuDoB
      ,stuDoBEst
      ,stuGender
      ,stuEthnicity
	  , newid() pCreateTag

	  , schoID
	  , null scholCode
	  , null schrYear
      ,schoExpectedCompletion
      ,schoStatus
      ,schoStatusDate
      , S.pRowversion
      , S.pCreateDateTime
      , S.pCreateUser
      , S.pEditDateTime
      , S.pEditUser
FROM Student ST
	LEFT JOIN Scholarships_ S
		ON S.schoID is null
WHERE ST.stuID = @studentID

	-- scholarship study records
	Select * from ScholarshipStudy_
	WHERE schoID is null
END
GO

