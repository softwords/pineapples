SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 9 2017
-- Description:	This script will load Xml files
-- as produced in Fsm from the Examination processing database.
-- 26 10 2019 Modifications to support Kiribati exam exports
-- The main format change is the change of root node
-- and the test definition node is now Test (was NMCT_Test)
-- Both formats are now supported
-- Change History: 22 3 2022 : new format of Exam
--					15 8 2022 common.fnParseName, not (dbo.)fnParseName
-- =============================================
CREATE PROCEDURE [pExamWrite].[loadExam]
	-- Add the parameters for the stored procedure here
	@xml xml
	, @fileReference uniqueidentifier
	, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- what is the exam? do we need to define it?
	--
	declare @examCode nvarchar(10)			-- the code of the exam = test_group_ID
	declare @examName nvarchar(50)
	declare @examYearStr nvarchar(10)
	declare @examYear int
	declare @examTypeCreated int = 0

	declare @examID int						-- the ID of the exam in the exams table
	declare @examScoresUpdated int = 0

	-- identify the exam - try for 'version 2' which expects the exam node to be 'Test'
	Select
		@examCode = x.value('@test_group_ID','varchar(10)')
		, @examName = x.value('Test_name[1]','varchar(50)')
		, @ExamYearStr = x.value('Year[1]','varchar(10)')
		FROM @xml.nodes('//Test') as EXAM(x)

	if (@examCode is null) begin
		-- check for 'Version 1' which call the exam node NMCT_test
		Select
			@examCode = x.value('@test_group_ID','varchar(10)')
			, @examName = x.value('Test_name[1]','varchar(50)')
			, @ExamYearStr = x.value('Year[1]','varchar(10)')
			FROM @xml.nodes('//NMCT_test') as EXAM(x)
	end

	-- IF a Range is given ( ie FSM RMI) cast the exam year as the ENDING year of the range:
	-- If it is a simple value, use that value:
		SELECT @examYear =
			case when (len(@ExamYearStr) > 4)
				then convert(int, left(@examYearStr, 4)) + 1
				else convert(int, @examYearStr)
			end

	-- the examCode needs to be defined in the lkpExamType table
	if not exists (Select exCode from lkpExamTypes WHERE exCode = @examCode) begin
	-- insert it
		INSERT INTO lkpExamTypes
		(exCode, exName)
		VALUES (@examCode, @examName)

		SELECT @examTypeCreated = 1
	end

--
	DECLARE @tblStandards TABLE
	(
		standardID nvarchar(20)
		, standardDescription nvarchar(200)
	)

	DECLARE @tblBenchmarks TABLE
	(
		standardID nvarchar(20)
		, benchmarkID nvarchar(20)
		, benchmarkDescription nvarchar(200)
	)
	DECLARE @tblAchievement TABLE
	(
		achievementID int
		, achievementDescription nvarchar(200)
	)

	DECLARE @tblStudents TABLE
	(
		StudentName nvarchar(500)
		, Gender nvarchar(10)
		, StudentID nvarchar(20)
		, schNo nvarchar(50)
		, district nvarchar(100)
		, candidateID	uniqueidentifier
		, results xml
	)

	DECLARE @tblResults TABLE
	(
		candidateID	uniqueidentifier
		, AchievementLevel int
		, BenchmarkID nvarchar(50)
	)

	DECLARE @errors TABLE
	(
		rowID int
		, errorValue nvarchar(100)
		, errorMessage nvarchar(500)
	)

	DECLARE @warnings TABLE
	(
		rowID int
		, errorValue nvarchar(100)
		, errorMessage nvarchar(500)
	)
	declare @numErrors int
	declare @numWarnings int

	--- Step 1 pick up the standards, benchmark and achievement definition
	INSERT INTO @tblStandards
	SELECT
		s.value('@Standard_ID', 'nvarchar(20)')
		, s.value('Standard_description[1]', 'nvarchar(200)')
	FROM @xml.nodes('//Curriculum/Standards/Standard') as S(s)


	INSERT INTO @tblBenchmarks
	SELECT
		s.value('@Standard_ID', 'nvarchar(20)')
		, s.value('@Benchmark_ID', 'nvarchar(20)')
		, s.value('Benchmark_description[1]', 'nvarchar(200)')
	FROM @xml.nodes('//Curriculum/Benchmarks/Benchmark') as S(s)

	INSERT INTO @tblAchievement
	SELECT
		s.value('@AchievementLevel_ID', 'int')
		, s.value('AchievementLevel_name[1]', 'nvarchar(200)')
	FROM @xml.nodes('//Curriculum/AchievementLevels/AchievementLevel') as S(s)


	-- extract the students and result into table variables
	-- 'seq' is the uniqueconnector

	declare @numCandidates int
	declare @numResults int
	-- improved hanlding of student-Result one-to-many relationship:
	-- results are stored as an xml object on each student record, rather than read en masse
	-- Then, cross apply gets the results for each student
	-- This eliminates the need to add a Seq to each student ( which was done in the web controller)
	INSERT INTO @tblStudents
	Select
		replace(s.value('Student_name[1]','varchar(500)'),char(34),'') AS StudentName
		, nullif(s.value('Gender[1]', 'nvarchar(10)'),'') as Gender
		, s.value('@Student_ID', 'nvarchar(20)') as StudentID
		, nullif(s.value('School[1]/@School_ID', 'nvarchar(50)'),'') as schNo
		-- version 2 files do not contain state, but derive it from the school
		-- however, if the state name is provided, we can at least verify it
		-- against the EMIS version and report errors
		, nullif(s.value('State[1]', 'nvarchar(50)') , '')

		, newid()
		, s.query('./Results')
	FROM @xml.nodes('//Students/Student') as ST(s)
	SELECT @numCandidates = @@ROWCOUNT


	INSERT INTO @tblResults
	Select
		STU.CandidateID
		, s.value('@AchievementLevel_ID','int') AS Achievement
		, s.value('@Benchmark_ID','varchar(50)') AS Benchmark
	FROM @tblStudents STU
	CROSS APPLY STU.results.nodes('//Result') as ST(s)
	SELECT @numResults = @@ROWCOUNT

	-- some preliminary verifications


	-- are the schools OK?
	INSERT INTO @errors
	Select DISTINCT null
	, '<missing school>'
	, 'Missing school number on student' + T.StudentName
	FROM @tblStudents T
	WHERE schNo is null

	INSERT INTO @errors
	Select DISTINCT null
	, SchNo
	, 'missing school code'
	FROM @tblStudents
	WHERE schNo not in (Select schNo from Schools)
	AND schNo is not null


	-- any schools not in their correct district
	INSERT INTO @warnings
	Select DISTINCT null
	, R.schNo + ' (' + District + ')'
	, 'Conflict in District recorded for school'
	FROM @tblStudents R
	INNER JOIN Schools S
		ON R.schNo = S.schNo
	INNER JOIN Islands I
		ON S.iCode = I.iCode
	INNER JOIN Districts D
		ON I.iGroup = D.dID
	-- only check non-nulls - for version 2 files that don;t provide district
	WHERE isnull(District, dName) <> dName

	INSERT INTO @warnings
	Select null
	, T.StudentName + ' (' + T.schNo + ')'
	, 'No student matching this name could be found in the Students list.'
	FROM
	@tblStudents T
	CROSS APPLY common.fnParseName(T.StudentName) N
	LEFT JOIN Student_ S
	ON ( N.FirstName = S.stuGiven
	AND N.LastName = S.stuFamilyName
	) OR ( N.FirstName = S.stuFamilyName
	AND N.LastName = S.stuGiven
	)
	WHERE T.StudentName is not null
	AND stuID is null

	-- student name not matched with *any* enrolment at the nominated school

	INSERT INTO @warnings
	Select DISTINCT null					-- DISTINCT becuase there may be matching Student_
	, T.StudentName + ' (' + T.schNo + ')'
	, 'No enrolment for any student named ' + T.StudentName + ' at school ' + T.schNo
	FROM
	@tblStudents T
	CROSS APPLY common.fnParseName(T.StudentName) N
	INNER JOIN Student_ S
	ON ( N.FirstName = S.stuGiven
	AND N.LastName = S.stuFamilyName
	) OR ( N.FirstName = S.stuFamilyName
	AND N.LastName = S.stuGiven
	)
	LEFT JOIN StudentEnrolment_ SE
		ON T.schNo = SE.schNo
		AND S.stuID = SE.stuID
	WHERE T.StudentName is not null
	AND stueID is null

	-- abort here is validations failed

 -- report the validations.
	Select rowID
	, errorValue
	, errorMessage
	from @errors
	ORDER BY rowID
	Select @numErrors = @@ROWCOUNT

	Select rowID
	, errorValue
	, errorMessage
	from @warnings
	ORDER BY rowID
	Select @numWarnings = @@ROWCOUNT

	-- abort here if validations failed
	if (@numErrors > 0 ) begin
		-- caller is expecting 3 recordsets to come back, so create a dummy 'data'
		Select null NoData
		WHERE 1=0
		return 1
	end
	-- moving beyond the verifications


	-- does the exam instance exists in Exams table ? If so, we are reloading everything
	-- either way we get @examID - the exId into Exams table

	SELECT @examID = exID
	FROM Exams
	WHERE exCode = @examCode
	AND exYear = @examYear
	if @@ROWCOUNT = 0 begin
		INSERT INTO Exams
		(exCode, exYear, exUser, exDate, exCandidates, exMarks, docID)
		VALUES
		( @examCode
		, @examYear
		, original_login()
		, getdate()
		, @numCandidates
		, @NumResults
		, @fileReference
		)

		SELECT @examID = SCOPE_IDENTITY()
	end
	else begin
	UPDATE Exams
		SET exUser = original_login()
		, exDate = getdate()
		, exCandidates = @numCandidates
		, exMarks = @numResults
		, docID = @fileReference
		WHERE exID = @examID
	end

	-- if proceding, deete any existing records from ttarget tables

	DELETE FROM ExamCandidateResults
	WHERE excID in (Select excID from ExamCandidates WHERE exID =  @examID)

	DELETE FROM ExamCandidates
	WHERE exID = @examID

	-- Standards and Benchmarks
	DELETE FROM ExamBenchmarks
	WHERE exID = @examID

	DELETE from ExamStandards
	WHERE exID = @examID

	DELETE From ExamAchievementLevels
	WHERE exID = @examID

	INSERT INTO ExamStandards
	( exstdCode
	, exID
	, exstdDescription
	)
	Select StandardID
	, @examID
	, StandardDescription
	FROM @tblStandards

	INSERT INTO ExamBenchmarks
	( exbnchCode
	, exID
	, exbnchDescription
	, exstdID
	)
	Select BenchmarkID
	, @examID
	, BenchmarkDescription
	, exStdID
	FROM @tblBenchmarks B
	INNER JOIN ExamStandards STD
		ON B.StandardID = STD.exstdCode
		AND STD.exID = @examID

	INSERT INTO ExamAchievementLevels
	( exID
	, exalVal
	, exalDescription
	)
	Select @examID
	, achievementID
	, achievementDescription
	FROM @tblAchievement

	-- Students - for now, we will not write into students

	-- Candidates - the set of candidates for the exam
	-- this is one-to-one with Student nodes in the Xml

	INSERT INTO ExamCandidates
	( excID
	, studentID
	, exID
	, excGiven
	, excMiddleNames
	, excFamilyName
	, excGender
	, dID
	, schNo
	)
	SELECT candidateID
	, nullif(StudentID , '')
	, @examID
	, N.FirstName
	, N.MiddleName
	, N.LastName
	, left(Gender,1)
	, D.dID
	, T.schNo
	FROM @tblStudents T
	LEFT JOIN Schools S
		ON T.schNo = S.schNo
		LEFT JOIN Islands I
			ON S.iCode = I.iCode
		LEFT JOIN Districts D
			ON I.iGroup = D.dID
	CROSS APPLY common.fnParseName(T.StudentName) N

	INSERT INTO ExamCandidateResults
	( excID
	, exbnchID
	, excrLevel
	, excrCandidateCount
	)
	SELECT R.candidateID
	, B.exbnchID
	, R.AchievementLevel
	, 1
	FROM @tblResults R

	INNER JOIN ExamBenchmarks B
		ON B.exbnchCode = R.BenchmarkID
		AND B.exID = @examID


	-- build the warehouse table entries for this exam
	-- Notethat a full warehouse build may entirely recreate these tables

	exec warehouse.buildExamResults @examID, null

	-- build the legacy table ExamStandardTest

	DELETE FROM ExamStandardTest
	WHERE exId = @examID

	-- easiest to do this using the warehouse crosstab, which is closest to
	-- the shape of ExamStandardTest

	INSERT INTO ExamStandardTest
	( exID
	, schNo
	, stschName
	, stGender
	, stSubject		-- IMPORTANT we cant sum components ( ie benchmarks) to get subjects (ie standards)
	, stComponent
	, st0
	, st1
	, st2
	, st3
	, st4
	, st5
	, st6
	, st7
	, st8
	, st9
	)
	Select examID
	, schNo
	, schoolName
	, Gender
	, S.exstdCode   --  here for historical compatibility, but this value should not be used to get 'totals by standard'
	, [Key] -- benchmark
	, sum(case when achievementLevel = 0 then candidateCount end) [0]
	, sum(case when achievementLevel = 1 then candidateCount end) [1]
	, sum(case when achievementLevel = 2 then candidateCount end) [2]
	, sum(case when achievementLevel = 3 then candidateCount end) [3]
	, sum(case when achievementLevel = 4 then candidateCount end) [4]
	, sum(case when achievementLevel = 5 then candidateCount end) [5]
	, sum(case when achievementLevel = 6 then candidateCount end) [6]
	, sum(case when achievementLevel = 7 then candidateCount end) [7]
	, sum(case when achievementLevel = 8 then candidateCount end) [8]
	, sum(case when achievementLevel = 9 then candidateCount end) [9]

	From warehouse.examSchoolResultsTyped T
	INNER JOIN examBenchmarks B
		ON B.exbnchID = T.ID
		AND B.exID = T.examID
	INNER JOIN ExamStandards S
		ON B.exstdID = S.exstdID
		AND S.exID = B.exID
	WHERE RecordType = 'Benchmark'
	AND examID = @examID
	GROUP BY examID
	, schNo
	, schoolName
	, Gender
	, S.exstdCode
	, [Key] -- benchmark


	-- finally add to the Documents_ table
	declare @logDate datetime = getUTCDate()
	UPDATE Documents_
	SET docTitle = 'Exam data ' + @examCode + ' ' + @examYearStr
	, pEditDateTime = @logDate
	, pEditUser = @user

	WHERE docID = @fileReference

	if @@ROWCOUNT = 0 begin
		INSERT INTO Documents_
		(
			docID
			, docTitle
			, docDescription
			, docDate
			, docTags
			, pCreateUser
			, pCreateDateTime
			, pEditUser
			, pEditDateTime
		)
		VALUES
		(
			@fileReference
			, 'Exam data ' + @examCode + ' ' + @examYearStr
			, null
			, @logDate
			, 'EXAM ' + @examCode + convert(nchar(4), @examYear)
			, @user
			, @logDate
			, @user
			, @logDate
		)
	end

	--- Return the Result
	--Select *
	--from warehouse.ExamSchoolResultsX

	Select *
	from warehouse.ExamSchoolResults

	WHERE examID = @examID
END
GO

