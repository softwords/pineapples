SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 7 2021
-- Description:	Sheet A12 of UIS Survey
-- In this calculation we count every school teaching at each Isced Level
-- schools that teach across muliple levels will contribute to more than one row,
-- hence the total number of schools shown may well exceed the actual number of schools
-- this is consistent with the FullTimePartTime count of teachers in sheets A9 and A10.
-- Conceptually having a single school offering both ISCED 1 and 2 is functionally the same as having
-- a primary school and junior secondary school side-by-side.
-- =============================================
CREATE PROCEDURE [warehouse].[uisA12]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE  @Gov Table
(
Gov nvarchar(1),
GovGroup nvarchar(10)
)

INSERT INTO @Gov
VALUES ('G','Public'),
('N','Private')

Select Isced, GovGroup, count(schNo) NumSchools
FROM
(
Select DISTINCT surveyYear, schNo , [ISCED Level] Isced
, G.GovGroup
from warehouse.Enrol E
INNER JOIN DimensionLevel DL
	ON E.ClassLevel = DL.LevelCode
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON E.SurveyDimensionID = DSS.[Survey ID]
INNER JOIN @Gov G
	ON G.Gov = Dss.AuthorityGovtCode
where Enrol > 0
AND SurveyYear = @year
) SUB
GROUP BY ISCED, GovGroup

END
GO

