SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 12 2017
-- Description:	first attempt at working with kobo downloaded data
-- =============================================
CREATE PROCEDURE [pInspectionWrite].[koboProc]
	-- Add the parameters for the stored procedure here

	@xml xml
	, @filereference uniqueidentifier
	, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- this is the type of Kobo form implemented by this stored proc
-- it must match a description in the table lkpInspectionTypes

declare @rows TABLE
(
 	rowIndex int
	, [start] datetime
	, [end] datetime
	, inspectionType nvarchar(20)
	, inspBy nvarchar(100)
	, schNo nvarchar(50)
	, note nvarchar(500)
	, rowXml xml
	, filereference uniqueidentifier null
	, koboId int
)

declare @idMap TABLE
(
	koboID int
	, inspID int
)

-- represent the xml file - column names are 'sanitized'
INSERT INTO @rows
SELECT
v.value('@Index', 'int') [ RowIndex]
, nullif(ltrim(v.value('@start', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@end', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@inspectionType', 'nvarchar(50)')),'')
, nullif(ltrim(v.value('@inspBy', 'nvarchar(100)')),'')
, nullif(ltrim(v.value('@schNo', 'nvarchar(50)')),'')
, nullif(ltrim(v.value('@note', 'nvarchar(max)')),'')
, v.query('.')

-- file reference is the id of the input file
, @fileReference
, nullif(ltrim(v.value('@_id', 'int')),'')
from @xml.nodes('ListObject/row') as V(v)

INSERT INTO @idMap
Select koboID
, inspID
FROM @rows R
	LEFT JOIN SchoolInspection SI
	ON R.koboID = SI.inspXml.value('*[1]/@_id','int')

-- the inspection sets we need are for each combination of inspection type and year in the file
-- if there is one, that's great, otherwise create them

INSERT INTO InspectionSet
(
	inspsetName
	, inspsetType
	, inspSetYear
)
Select DISTINCT
	N'eSURVEY: ' + intyDesc
	, intyCode
	, year([start])
FROM @rows R
INNER JOIN lkpInspectionTypes I
	ON R.inspectionType = I.intyCode
LEFT JOIN InspectionSet INSP
	ON year(start) = inspsetYear
	AND inspSetName = N'eSURVEY: ' + intyDesc
	AND I.intyCode = INSP.inspsetType
WHERE INSP.inspsetID is null

declare @dt datetime = getUtcDate()
-- create the inspections
INSERT INTO SchoolInspection
( schNo,
inspStart,
inspEnd,
inspsetID,
inspBy,
inspNote,
inspXml,
inspSource,
pCreateUser,
pCreateDateTime,
pEditUser,
pEditDateTime
)
SELECT schNo
, [start]
, [end]
, INSP.inspSetId
, R.inspBy
, R.note
, rowXml
, @filereference
, @user
, @dt
, @user
, @dt
FROM @rows R
INNER JOIN @idMap IDs
ON R.koboID = IDs.koboID
AND IDs.inspId is null
INNER JOIN lkpInspectionTypes I
	ON R.inspectionType = I.intyCode
INNER JOIN
-- just be a bit careful we don't get a duplicate here
(
Select inspSetYear, inspSetType, inspSetName, max(inspSetID) inspSetID
FROM
InspectionSet
GROUP BY inspSetYear, inspSetType, inspSetName
)
INSP
	ON year(start) = inspsetYear
	AND inspSetName = N'eSURVEY: ' + intyDesc
	AND I.intyCode = INSP.inspsetType


-- updates
UPDATE SchoolInspection
SET schNo = R.schNo,
inspStart = R.[start],
inspEnd = R.[end],
inspsetID = INSP.inspSetId,
inspBy = R.inspBy,
inspNote = R.note,
inspXml = R.rowXml,
inspSource = @filereference,
pEditUser = @user,
pEditDateTime = @dt

FROM SchoolInspection
INNER JOIN @idMap IDs
ON SchoolInspection.inspID = IDs.inspID
INNER JOIN @rows R
	ON IDs.koboID = R.koboID
INNER JOIN lkpInspectionTypes I
	ON R.inspectionType = I.intyCode
INNER JOIN
-- just be a bit careful we don't get a duplicate here
(
Select inspSetYear, inspSetType, inspSetName, max(inspSetID) inspSetID
FROM
InspectionSet
GROUP BY inspSetYear, inspSetType, inspSetName
)
INSP
	ON year(start) = inspsetYear
	AND inspSetName = N'eSURVEY: ' + intyDesc
	AND I.intyCode = INSP.inspsetType


-- return the inspections
Select SI.*
, case when Ids.inspID is not null then ' (updated)' end uploadStatus from SchoolInspection SI
INNER JOIN @rows R
ON SI.inspXml.value('*[1]/@_id','int') = R.koboId
LEFT JOIN @idMap IDs
ON R.koboID = Ids.koboID


END
GO

