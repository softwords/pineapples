SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 8 2009
-- Description:	EFA7 calculation
-- =============================================
CREATE PROCEDURE [warehouse].[BuildBudgets]
	@StartFromYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- turn ansi warnings off to get a cleaner log; by removing all these:
	--Warning: Null value is eliminated by an aggregate or other SET operation.
	SET ANSI_WARNINGS OFF;

-- this is a temp table to hold the sector expenditure
-- after the prorata calculation are taken into account
-- it is built from a union query over 3 cases:
-- direct sector expenditure, prorated sector expenditure, ....

DECLARE @sectorInfo TABLE
(
	SurveyYear int
	, DistrictCode nvarchar(10)
	, CostCentreCode nvarchar(20)
	, [Cost Centre] nvarchar(100)
	, item nvarchar(50)
	, sectorCode nvarchar(10)
	, ProRated int
	, [Actual Recurrent] float
	, [Budget Recurrent] float
	, [Actual Total] float
	, [Budget Total] float
	, SectorEnrolment int
	, sectorEnrolmentPerc decimal(7,4)
)


-- Totals by expenditure point
DECLARE @ExpDistrictYear TABLE

(
	ExpYear int
	, ExpenditurePoint nvarchar(10)
	, currentA money
	, currentB money
	, TotA money
	, TotB money
	, GovExpA money
	, GovExpB money
	, GNP money
	, GNPLocal money
	, GNPCapita money
	, GNPCapitaLocal money
	, GNPCurrency nvarchar(3)
)

DECLARE @ExpYear TABLE

(
	ExpYear int
	, currentA money
	, currentB money
	, TotA money
	, TotB money
	, GovExpA money
	, GovExpB money
	, GNP money
	, GNPLocal money
	, GNPCapita money
	, GNPCapitaLocal money
	, GNPCurrency nvarchar(3)
)


-- this will hold the rolled up totals from sectorInfo
DECLARE @sectorTotals TABLE
(
	SurveyYear int
	, sectorCode nvarchar(10)
	, costCentreCode nvarchar(20)
	, [Actual Recurrent] float
	, [Budget Recurrent] float
	, [Actual Total] float
	, [Budget Total] float
	, SectorEnrolment int
	, sectorEnrolmentPerc float
)

-- temp table for enrolments by district by sector
DECLARE @sectorEnrolments TABLE
(
	SurveyYear int
	, DistrictCode nvarchar(10)
	, SectorCode nvarchar(10)
	, EnrolM int
	, EnrolF int
	, Enrol int
	, EnrolPerc decimal(7,4)
)

-- enrolments by sector - nation
DECLARE @sectorEnrolmentsTotal TABLE
(
	SurveyYear int
	, SectorCode nvarchar(10)
	, EnrolM int
	, EnrolF int
	, Enrol int
	, EnrolPerc decimal(7,4)
)


----- STEP 1 ----- Secotr Enrolment totals, and percentages ------
----- The percentage of each sector's enrolment over the total enrolments is calcuated and stored at district level
----- and overall
-- build the sector enrolments from the warehouse enrolments

-- get the sector total , by district
INSERT INTO @sectorEnrolments
(
	SurveyYear
	, DistrictCode
	, SectorCode
	, EnrolM
	, EnrolF
	, Enrol
)
select TE.SurveyYear
, TE.DistrictCode
, DL.SectorCode
, sum(case when GenderCode = 'M' then Enrol end) EnrolM
, sum(case when GenderCode = 'F' then Enrol end) EnrolF
, sum(TE.Enrol) Enrol
from warehouse.tableEnrol TE
INNER JOIN DimensionLevel DL
	ON TE.ClassLevel = DL.LevelCode
GROUP BY
TE.SurveyYear
, TE.DistrictCode
, DL.SectorCode
, DL.Sector

-- get the sector total , overall
INSERT INTO @SectorEnrolmentsTotal
(
	SurveyYear
	, SectorCode
	, EnrolM
	, EnrolF
	, Enrol
)
SELECT SurveyYear
, SectorCode
, sum(EnrolM) EnrolM
, sum(EnrolF) EnrolF
, sum(Enrol) Enrol
FROM @SectorEnrolments
GROUP BY
SurveyYear
, SectorCode


-- put the enrolment percentage on the sector record for district
UPDATE @sectorEnrolments
SET EnrolPerc = case when isnull(TotalEnrol,0) = 0 then null when Enrol is null then null else Enrol / TotalEnrol end
FROM @sectorEnrolments
INNER JOIN
(
Select
SurveyYear
, DistrictCode
, convert(float, sum(Enrol) ) TotalEnrol
From @sectorEnrolments
GROUP BY
SurveyYear
, DistrictCode
) TOT
ON [@sectorEnrolments].SurveyYear = TOT.SurveyYear
AND [@sectorEnrolments].DistrictCode = TOT.DistrictCode

print 'SectorEnrolments perc updated: ' + convert(nvarchar(4), @@ROWCOUNT)

-- put the enrolment percetnage on the overall sector total record
UPDATE @sectorEnrolmentsTotal
SET EnrolPerc = case when isnull(TotalEnrol,0) = 0 then null when Enrol is null then null else Enrol / TotalEnrol end
FROM @sectorEnrolmentsTotal
INNER JOIN
(
Select
SurveyYear
, convert(float, sum(Enrol) ) TotalEnrol
From @sectorEnrolmentsTotal
GROUP BY
SurveyYear
) TOT
ON [@sectorEnrolmentsTotal].SurveyYear = TOT.SurveyYear

print 'SectorEnrolmentsTotal perc updated: ' + convert(nvarchar(4), @@ROWCOUNT)


--- STEP 2) Cost Centre totals

/*
-- begin by collecting the cost centre amounts
-- 3 cases:
-- 1) specific postings to a sector
------ sector is specified on cost centre record (prorate = 1)
------ or else is on the EdExpenditure record (prorate = 3)
------ Note that if either of these values is present, it is used
------ regardless of the value of prorate. Pro-rate is therefor primarily controlling the Ui
-- 2) pro-rated across all sectors (prorate = 1)
-- 3) not posted to any sector (prorate = 2)

*/

-- case 1 - all goes to one sector
INSERT INTO @sectorInfo
	SELECT
		FN.fnmYear as SurveyYear
		, FN.fnmDistrict
		, ccCode as [CostCentreCode]
		, ccDescription as [Cost Centre]
		, EX.xedItem
		, coalesce(xedSector,ccSector) as SectorCode
		, ccProRate as ProRated
		, sum(EX.xedCurrentA) as [Actual Recurrent]
		, sum(EX.xedCurrentB) as [Budget Recurrent]
		, sum(EX.xedTotA) as [Actual Total]
		, sum(EX.xedTotB) as [Budget Total]
		, SE.Enrol as SectorEnrolment
		, SE.EnrolPerc SectorEnrolmentPerc
	FROM EdExpenditureState EX
		INNER JOIN CostCentres
			ON EX.xedCostCentre = CostCentres.ccCode
		INNER JOIN GovtExpenditureState FN
			ON FN.fnmID = EX.fnmID
		INNER JOIN @sectorEnrolments SE
			ON FN.fnmYear = SE.SurveyYear
			AND FN.fnmDistrict = SE.DistrictCode
			AND coalesce(xedSector,ccSector) = SE.SectorCode
		--- only update those records where the Expenditure Point is an actual district,
		--- this related to a specific subset of enrolments
		INNER JOIN Districts D
			ON FN.fnmDistrict = D.dID
	WHERE coalesce(EX.xedSector, ccSector) is not null

GROUP BY
		FN.fnmYear
		, FN.fnmDistrict
		, ccCode
		, ccDescription
		, ccSector
		, ccProRate
		, xedItem
		, xedSector
		, SE.Enrol
		, SE.EnrolPerc

print 'SectorInfo case xedSector / ccSEctor not null inserted (district): ' + convert(nvarchar(4), @@ROWCOUNT)


-- case 1 - all goes to one sector
INSERT INTO @sectorInfo
	SELECT
		FN.fnmYear as SurveyYear
		, FN.fnmDistrict
		, ccCode as [CostCentreCode]
		, ccDescription as [Cost Centre]
		, EX.xedItem
		, coalesce(xedSector,ccSector) as SectorCode
		, ccProRate as ProRated
		, sum(EX.xedCurrentA) as [Actual Recurrent]
		, sum(EX.xedCurrentB) as [Budget Recurrent]
		, sum(EX.xedTotA) as [Actual Total]
		, sum(EX.xedTotB) as [Budget Total]
		, SE.Enrol as SectorEnrolment
		, SE.EnrolPerc SectorEnrolmentPerc
	FROM EdExpenditureState EX
		INNER JOIN CostCentres
			ON EX.xedCostCentre = CostCentres.ccCode
		INNER JOIN GovtExpenditureState FN
			ON FN.fnmID = EX.fnmID
		INNER JOIN @sectorEnrolmentsTotal SE
			ON FN.fnmYear = SE.SurveyYear
			AND coalesce(xedSector,ccSector) = SE.SectorCode
		--- only update those records where the Expenditure Point is an actual district,
		--- this related to a specific subset of enrolments
		LEFT JOIN Districts D
			ON FN.fnmDistrict = D.dID
	WHERE coalesce(EX.xedSector, ccSector) is not null
		AND D.dID is null

GROUP BY
		FN.fnmYear
		, FN.fnmDistrict
		, ccCode
		, ccDescription
		, ccSector
		, ccProRate
		, xedItem
		, xedSector
		, SE.Enrol
		, SE.EnrolPerc

print 'SectorInfo case xedSector / ccSEctor not null inserted (national): ' + convert(nvarchar(4), @@ROWCOUNT)

-- case 2 - prorated (prorated = 1) - for district
INSERT INTO @sectorInfo
	SELECT FN.fnmYear AS [Year]
		, FN.fnmDistrict
		, ccCode
		, ccDescription
		, EX.xedItem
		, SE.SectorCode AS Sector
		, ccProRate
		, xedCurrentA * SE.EnrolPerc CurrentA
		, xedCurrentB * SE.EnrolPerc CurrentB
		, xedTotA * SE.EnrolPerc as [Actual Total]
		, xedTotB * SE.EnrolPerc as [Budget Total]
		, SE.Enrol as SectorEnrolment
		, SE.EnrolPerc SectorEnrolmentPerc
	FROM EdExpenditureState EX
		INNER JOIN CostCentres
			ON EX.xedCostCentre = CostCentres.ccCode
		INNER JOIN GovtExpenditureState FN
			ON FN.fnmID = EX.fnmID
		--- we get one record for every sector where the district has enrolments
		INNER JOIN @sectorEnrolments SE
			ON FN.fnmYear = SE.SurveyYear
			AND FN.fnmDistrict = SE.DistrictCode
		--- only update those records where the Expenditure Point is an actual district,
		--- this related to a specific subset of enrolments
		INNER JOIN Districts D
			ON FN.fnmDistrict = D.dID
	WHERE
		coalesce(EX.xedSector,ccSector) Is Null AND ccProRate=1

print 'SectorInfo prorate=1 inserted ( districts) : ' + convert(nvarchar(4), @@ROWCOUNT)


-- case 2 - now handle the prorate for any national expenditure -
-- this is prorated by total enrolments
INSERT INTO @sectorInfo
	SELECT FN.fnmYear AS [Year]
		, FN.fnmDistrict
		, ccCode
		, ccDescription
		, EX.xedItem
		, SE.SectorCode AS Sector
		, ccProRate
		, xedCurrentA * SE.EnrolPerc CurrentA
		, xedCurrentB * SE.EnrolPerc CurrentB
		, xedTotA * SE.EnrolPerc as [Actual Total]
		, xedTotB * SE.EnrolPerc as [Budget Total]
		, SE.Enrol as SectorEnrolment
		, SE.EnrolPerc SectorEnrolmentPerc
	FROM EdExpenditureState EX
		INNER JOIN CostCentres
			ON EX.xedCostCentre = CostCentres.ccCode
		INNER JOIN GovtExpenditureState FN
			ON FN.fnmID = EX.fnmID
		INNER JOIN @sectorEnrolmentsTotal SE
			ON FN.fnmYear = SE.SurveyYear
		--- only update those records where the Expenditure Point is NOT actual district,
		--- this related to a specific subset of enrolments
		LEFT JOIN Districts D
			ON FN.fnmDistrict = D.dID
	WHERE
		coalesce(EX.xedSector,ccSector) Is Null AND ccProRate=1
		AND d.dID is null

print 'SectorInfo prorate=1 inserted ( national) : ' + convert(nvarchar(4), @@ROWCOUNT)

-- case 3 ignored becuase ccPRoRAte is 2 and the sector code is null, so sector is left blank in the output
INSERT INTO @sectorInfo
	Select FN.fnmYear AS [Year]
		, FN.fnmDistrict
		, ccCode as [CostCentreCode]
		, ccDescription as [Cost Centre]
		, EX.xedItem
		, null as Sector
		, ccProRate as ProRated
		, EX.xedCurrentA as [Actual Recurrent]
		, EX.xedCurrentB as [Budget Recurrent]
		, EX.xedTotA as [Actual Total]
		, EX.xedTotB as [Budget Total]
		, null as SectorEnrolment
		, null as SectorEnrolmentPerc
	FROM EdExpenditureState EX
		INNER JOIN CostCentres
			ON EX.xedCostCentre = CostCentres.ccCode
		INNER JOIN GovtExpenditureState FN
			ON FN.fnmID = EX.fnmID
	WHERE
	coalesce(xedSector,ccSector) Is Null AND (ccProRate=2 or ccProRate is null)	-- catch any bad data set up

print 'SectorInfo prorate=2 inserted: ' + convert(nvarchar(4), @@ROWCOUNT)

----------- now insert records with no financial values, to hold the enrolments
--------INSERT INTO @sectorInfo
--------	Select DISTINCT
--------		FN.fnmYear AS [Year]
--------		, FN.fnmDistrict
--------		, null [CostCentreCode]
--------		, null as [Cost Centre]
--------		, '(enrolment)'
--------		, SE.SectorCode AS Sector
--------		, null -- ccProRate
--------		, null CurrentA
--------		, null CurrentB
--------		, null [Actual Total]
--------		, null [Budget Total]
--------		, SE.Enrol as SectorEnrolment
--------		, SE.EnrolPerc SectorEnrolmentPerc
--------	FROM EdExpenditureState EX
--------		INNER JOIN CostCentres
--------			ON EX.xedCostCentre = CostCentres.ccCode
--------		INNER JOIN GovtExpenditureState FN
--------			ON FN.fnmID = EX.fnmID
--------		--- we get one record for every sector where the district has enrolments
--------		INNER JOIN @sectorEnrolments SE
--------			ON FN.fnmYear = SE.SurveyYear
--------			AND FN.fnmDistrict = SE.DistrictCode
--------		--- only update those records where the Expenditure Point is an actual district,
--------		--- this related to a specific subset of enrolments
--------		INNER JOIN Districts D
--------			ON FN.fnmDistrict = D.dID


INSERT INTO @sectorTotals
Select U.SurveyYear
	, U.sectorCode
	, U.CostCentreCode
	, sum(U.[Actual Recurrent]) [Actual Recurrent]
	, sum(U.[Budget Recurrent]) [Budget Recurrent]
	, sum(U.[Actual Total]) [Actual Total]
	, sum(U.[Budget Total]) [Budget Total]
	, SEDT.Enrol				-- its repeated on each row
	, SEDT.enrolPerc			[Sector Alloc]
FROM @sectorInfo U
	INNER JOIN @sectorEnrolmentsTotal SEDT
		ON U.SurveyYear = SEDT.SurveyYear
		AND U.sectorCode = SEDT.SectorCode
Group BY
	U.SurveyYear
	,U.sectorCode
	, U.costCentreCode
	, SEDT.Enrol
	, SEDT.enrolPerc

-- now the annual totals
INSERT INTO @ExpYear
SELECT fnmYear

	, sum(U.[Actual Recurrent]) [Actual Recurrent]
	, sum(U.[Budget Recurrent]) [Budget Recurrent]
	, sum(U.[Actual Total]) [Actual Total]
	, sum(U.[Budget Total]) [Budget Total]
	, fnmExpTotA
	, fnmExpTotB
	, fnmGNP
	, fnmGNP * (case when isnull(fnmGNPXchange,0) = 0 then 1 else fnmGNPXChange end)
	, fnmGNPCapita
	, fnmGNPCapita * (case when isnull(fnmGNPXchange,0) = 0 then 1 else fnmGNPXChange end)
	, fnmGNPCurrency

FROM GovtExpenditure GV
	LEFT JOIN @sectorInfo U
		ON GV.fnmYear = U.SurveyYear
GROUP BY fnmYear
, fnmExpTotA
, fnmExpTotB
, fnmGNP
, fnmGNPCapita
, fnmGNPXchange
, fnmGNPCurrency


------------------------------------------------------------------------------------
-- Write out the final warehouse tables
------------------------------------------
DELETE
	FROM warehouse.EdExpenditureDetail
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

DELETE
	FROM warehouse.EdExpenditure
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

DELETE
	FROM warehouse.Expenditure
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

INSERT INTO warehouse.EdExpenditureDetail
(
	surveyYear
	, DistrictCode
	, CostCentreCode
	, item
	, sectorCode
	, ProRated
	, [Actual Recurrent]
	, [Budget Recurrent]
	, [Actual Total]
	, [Budget Total]
	, SectorEnrolment
	, sectorEnrolmentPerc
)
Select
	SurveyYear
	, DistrictCode
	, CostCentreCode
	, item
	, sectorCode
	, ProRated
	, [Actual Recurrent]
	, [Budget Recurrent]
	, [Actual Total]
	, [Budget Total]
	, SectorEnrolment
	, sectorEnrolmentPerc
FROM @sectorInfo
WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

INSERT INTO warehouse.EdExpenditure
(
	SurveyYear
	, DistrictCode
	, CostCentreCode
	, sectorCode
	, [Actual Recurrent]
	, [Budget Recurrent]
	, [Actual Total]
	, [Budget Total]
)
SELECT SurveyYear
, DistrictCode
, CostCentreCode
, SectorCode
	, sum([Actual Recurrent]) [Actual Recurrent]
	, sum([Budget Recurrent]) [Budget Recurrent]
	, sum([Actual Total]) [Actual Total]
	, sum([Budget Total]) [Budget Total]
FROM @sectorInfo
WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
GROUP BY
 SurveyYear
, DistrictCode
, CostCentreCode
, SectorCode

INSERT INTO warehouse.Expenditure
(
	SurveyYear
	, DistrictCode
	, GNP
	, GNPCapita
	, GNPCurrency
	, GNPLocal
	, GNPCapitaLocal
	, GovtExpA
	, GovtExpB
	, CostCentreCode
	, sectorCode
	, EdExpA
	, EdExpB
	, EdRecurrentExpA
	, EdRecurrentExpB
	, Enrolment
	, EnrolmentPerc
	, EnrolmentNation
	, EnrolmentNationPerc

)
SELECT *
from
(
Select fnmYear SurveyYear
, null DistrictCode
, GEX.fnmGNP
, GEX.fnmGNPCapita
, GEX.fnmGNPCurrency
, GEX.fnmGNP * isnull(GEX.fnmGNPXchange,1)  GNPLocal
, GEX.fnmGNPCapita * isnull(GEX.fnmGNPXchange, 1) GNPCapitaLocal
, null GovtExpA
, null GovtExpB
, Null CostCentreCode
, null SectorCode
, null EdExpA
, null EdExpB
, null EdExpRecurrentA
, null EdExpRecurrentB
, null Enrolment
, null EnrolmentPerc
, null EnrolmentNation
, null EnrolmentNationPerc

From GovtExpenditureState GEX
WHERE fnmDistrict = 'NATIONAL'
UNION
Select fnmYear SurveyYear
, fnmDistrict DistrictCode
, null fnmGNP
, null fnmGNPCapita
, null fnmGNPCurrency
, null fnmGNPLocal
, null fnmGNPCapitaLocal
, GEX.fnmExpTotA GovtExpA
, GEX.fnmExpTotB GovtExpB
, Null CostCentreCode
, null SectorCode
, null EdExpTotalA
, null EdExpTotalB
, null EdExpRecurrentA
, null EdExpRecurrentB
, null Enrolment
, null EnrolmentPerc
, null EnrolmentNation
, null EnrolmentNationPerc
from GovtExpenditureState GEX
UNION
-- adds a row for year, distrct, sector - holding only the enrolment and enrolment perc
-- this is for district <> NATIONAL ie we can get specific enrolments in the district
Select
SurveyYear
, DistrictCode
, null fnmGNP
, null fnmGNPCapita
, null fnmGNPCurrency
, null fnmGNPLocal
, null fnmGNPCapitaLocal
, null GovtExpA
, null GovtExpB
, Null CostCentreCode
, SectorCode
, null EdExpTotalA
, null EdExpTotalB
, null EdExpRecurrentA
, null EdExpRecurrentB
, E.Enrol Enrolment
, E.EnrolPerc EnrolmentPerc
, null EnrolmentNation
, null EnrolmentNationPerc
from @sectorEnrolments E

UNION
-- adds a row for year, distrct, sector - holding only the enrolment and enrolment perc
-- this is for district <> NATIONAL ie we can get specific enrolments in the district
Select
SurveyYear
, 'NATIONAL' DistrictCode
, null fnmGNP
, null fnmGNPCapita
, null fnmGNPCurrency
, null fnmGNPLocal
, null fnmGNPCapitaLocal
, null GovtExpA
, null GovtExpB
, Null CostCentreCode
, SectorCode
, null EdExpTotalA
, null EdExpTotalB
, null EdExpRecurrentA
, null EdExpRecurrentB
, null Enrolment
, null EnrolmentPerc
, E.Enrol EnrolmentNation
, E.EnrolPerc EnrolmentNationPerc
from @sectorEnrolmentsTotal E
UNION
Select
SurveyYear
, DistrictCode
, null fnmGNP
, null fnmGNPCapita
, null fnmGNPCurrency
, null fnmGNPLocal
, null fnmGNPCapitaLocal
, null GovtExpA
, null GovtExpB
, CostCentreCode
, SectorCode
, sum([Actual Total]) [Actual Total]
, sum([Budget Total]) [Budget Total]
, sum([Actual Recurrent]) [Actual Total]
, sum([Budget Recurrent]) [Budget Total]
, null Enrolment
, null EnrolmentPerc
, null EnrolmentNation
, null EnrolmentNationPerc

from @sectorInfo I
GROUP BY
SurveyYear
, DistrictCode
, SectorCode
, CostCentreCode
) SUB
WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
ORDER BY SurveyYear ASC
, DistrictCode ASC
, SectorCode
, CostCentreCode


--- output for debugging - do not expose in production; breaks reconcile by adding unexpected recordsets
--Select * from CostCentres
--Select * from GovtExpenditureState
--Select * from EdExpenditureState
--Select * from @sectorEnrolments
--Select * from @sectorEnrolmentsTotal

--Select * from @sectorInfo
--Select * from @sectorTotals

--Select SurveyYear
--, sum([Actual Total]) SectorTotals
--from @sectorTotals
--GROUP BY
--SurveyYear

--Select SurveyYear
--, sum([Actual Total]) SectorInfo
--from @sectorInfo
--GROUP BY
--SurveyYear


--Select fnmYear
--, sum([xedTotA])
--from GovtExpenditureState GEX
--	INNER JOIN EdExpenditureState ED
--		ON GEX.fnmID = ED.fnmID
--GROUP BY
--	fnmYear

--Select fnmYear
--, fnmDistrict
--, sum([xedTotA])
--from GovtExpenditureState GEX
--	INNER JOIN EdExpenditureState ED
--		ON GEX.fnmID = ED.fnmID
--GROUP BY
--	fnmYear
--	, fnmDistrict
--Select SurveyYear
--, DistrictCode
--, sum([Actual Total])
--from @sectorInfo
--GROUP BY
--SurveyYear
--, DistrictCode

exec warehouse.logVersion

declare @versionID nvarchar(50)
declare @versionDateTime dateTime

Select @VersionId = versionID
, @VersionDateTime = versionDateTime
from warehouse.versionInfo


print ''
print 'warehouse.BuildBudgets completed'
print '------------------------------------'
print ''
print 'Warehouse version: ' + @versionId + ' ' + convert(nvarchar(40), @versionDateTime) + ' (UTC)'
END
GO

