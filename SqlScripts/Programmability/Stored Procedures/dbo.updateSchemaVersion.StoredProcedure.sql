SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 12 2007
-- Description:	update the schemaVersion table
-- =============================================
CREATE PROCEDURE [dbo].[updateSchemaVersion]
	@version nvarchar(10) = null,
	@VersionDate datetime = null
AS
BEGIN
set nocount on;
delete from schemaVersion

if @Version is null
	begin
		Select @version = '20071218A'
		select @VersionDate = '2007-12-18'
	end

insert into schemaVersion(schemaID, schemaDate, schemaApplied)
values
(
-- this here is what must be modified whenever a schema is released
@version,
@versionDate,

getdate()
)
END
GO

