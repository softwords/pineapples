SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 9 11 2022
-- Description:	Retireve staff numbers for XY charts using warehouse
-- =============================================
CREATE PROCEDURE [warehouse].[schoolDataSetStaff]
	-- Add the parameters for the stored procedure here
	@year int
	, @dataItem nvarchar(100)
	, @filter nvarchar(10) = null
	, @schoolNo nvarchar(50) = null
	, @parameter nvarchar(100) = null
AS
BEGIN
	SET NOCOUNT ON;
; with T
AS
(
select schNo, surveyYear
, tID
, Qualified
, Certified
, CertQual
, max(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null) then 1 else 0 end) TeachingFTPT
, sum(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null) then W else 0 end) TeachingFTE
, max(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null) then Qualified else 0 end) TeachingFTPTQ
, sum(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null) then W * Qualified else 0 end) TeachingFTEQ
, max(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null) then Certified else 0 end) TeachingFTPTC
, sum(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null) then W * Certified else 0 end) TeachingFTEC
, max(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null) then CertQual else 0 end) TeachingFTPTCQ
, sum(case when ISCEDSubClass like 'ISCED%' AND (ClassLevel = @Filter OR @Filter is null)then W * CertQual else 0 end) TeachingFTECQ
, max(case when ISCEDSubClass not like 'ISCED%' then 1 else 0 end) NonTeachingFTPT
, sum(case when ISCEDSubClass not like 'ISCED%' then W else 0 end) NonTeachingFTE
from warehouse.buildTeacherActivityWeightsClassLevel
Where surveyYear = @year
AND (schNo = @schoolNo or @schoolNo is null)
GROUP BY schNo, surveyYear, tID
,  Qualified
, Certified
, CertQual
)

Select SCHOOLS.schNo,
isnull(
case @dataItem
	when 'NumStaff' then NumStaff
	when 'NumStaffC' then NumStaffC
	when 'NumStaffQ' then NumStaffQ
	when 'NumStaffCQ' then NumStaffCQ
	-- percentages (by FTE is most useful)
	when 'NumStaffC%' then NumStaffC / nullif(NumStaff,0)
	when 'NumStaffQ%' then NumStaffQ / nullif(NumStaff,0)
	when 'NumStaffCQ%' then NumStaffCQ / nullif(NumStaff,0)


	when 'TeachingFTPT' then TeachingFTPT
	when 'TeachingFTPTC' then TeachingFTPTC
	when 'TeachingFTPTQ' then TeachingFTPTQ
	when 'TeachingFTPTCQ' then TeachingFTPTCQ

	when 'TeachingFTPTC%' then TeachingFTPTC / nullif(TeachingFTPT,0)
	when 'TeachingFTPTQ%' then TeachingFTPTQ / nullif(TeachingFTPT,0)
	when 'TeachingFTPTCQ%' then TeachingFTPTCQ / nullif(TeachingFTPT,0)

	when 'TeachingFTE' then TeachingFTE
	when 'TeachingFTEC' then TeachingFTEC
	when 'TeachingFTEQ' then TeachingFTEQ
	when 'TeachingFTECQ' then TeachingFTECQ

	when 'TeachingFTEC%' then TeachingFTEC / nullif(TeachingFTE,0)
	when 'TeachingFTEQ%' then TeachingFTEQ / nullif(TeachingFTE,0)
	when 'TeachingFTECQ%' then TeachingFTECQ / nullif(TeachingFTE,0)

	when 'NonTeachingFTPT' then NonTeachingFTPT
	when 'NonTeachingFTE' then NonTeachingFTE
	when 'NonTeachingFTE%' then NonTeachingFTE /  nullif(NumStaff,0)

end, 0) DataValue
, Estimate Estimate
, null Quality
FROM
(
	Select schNo, surveyYear, max(Estimate) Estimate from warehouse.TeacherLocation WHERE surveyYear = @year GROUP BY schNo, surveyYear
) SCHOOLS
INNER JOIN
(
Select schNo, surveyYear
, count(DISTINCT tID) NumStaff
, sum(Certified) NumStaffC
, sum(Qualified) NumStaffQ
, sum(CertQual) NumStaffCQ
, sum(TeachingFTPT) TeachingFTPT
, sum(TeachingFTE) TeachingFTE

, sum(NonTeachingFTPT) NonTeachingFTPT
, sum(NonTeachingFTE) NonTeachingFTE

, sum(TeachingFTPTQ) TeachingFTPTQ
, sum(TeachingFTPTC) TeachingFTPTC
, sum(TeachingFTPTCQ) TeachingFTPTCQ

, sum(TeachingFTEQ) TeachingFTEQ
, sum(TeachingFTEC) TeachingFTEC
, sum(TeachingFTECQ) TeachingFTECQ
from T
GROUP BY schNo, surveyYear
) SchoolTotals
ON SCHOOLS.SchNo = SchoolTotals.schNo
AND Schools.SurveyYEar = SchoolTotals.surveyYear
ORDER BY Schools.schNo

END
GO

