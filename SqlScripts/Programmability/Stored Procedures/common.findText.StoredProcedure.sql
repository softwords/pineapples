SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 Sep 2020
-- Description:	Return the object IDs of views, procs etc
-- that match a given input string
-- =============================================
CREATE PROCEDURE [common].[findText]
	-- Add the parameters for the stored procedure here
	@text nvarchar(100),
	@schema sysname = null,
	@objectType nvarchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @count int
	if @objectType is not null begin
		Select @count = count(*)
		from sys.objects
		WHERE type = @objectType
		if @count = 0 begin
			declare @msg nvarchar(400)
			Select @msg = 'Object type ' + @objectType + ' is not known'
			Raiserror(@msg, 16,1);
			return
		end
	end

	select S.name [schema], O.name [object], o.type, o.type_desc,  text
	from sys.syscomments C
	inner join sys.objects O
		on O.object_id = C.id
	inner join sys.schemas S
		ON S.schema_id = o.schema_id
	where
	text like '%' + @text + '%'
	and (s.name = @schema or @schema is null)
	and (o.type = @objectType or @objectType is null)
END
GO

