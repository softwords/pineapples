SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 June 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[PIVEnrolmentProjection]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @appname nvarchar(50)

Select @appname = paramText from sysparams WHERE paramName = 'APP_NAME'
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

Select *
into #tmpD
from
(
	select
		ES.escnID [Scenario ID]
		, escnName	[Scenario Name]

		, epYear  [Projection Year]
		, EP.dID	[District Code]
		, schNo		[School No]
		, null	as [School Name]
		, null as SchoolTypeCode
		, null as AuthCode
		, epdAge	Age
		, epdLevel
		, epdM		enrolM
		, epdF		enrolF
		, epdU		enrolU
		, null as Estimate
		, 'P' as Projection

	from EnrolmentScenario ES
		inner join EnrolmentProjection EP
			on ES.escnID = EP.escnID
		inner join EnrolmentProjectionData EPD
			on EP.epID = EPD.epID
	where  schNo is null

UNION

	select
		ES.escnID
		, escnName	[Scenario Name]
		, epYear
		, I.iGroup
		, S.schNo
		, S.schName
		, S.schType as SchoolTypeCode
		, S.schAuth as AuthCode
		, epdAge
		, epdLevel
		, epdM
		, epdF
		, epdU
		, null as Estimate
		, 'P' as Projection

	from EnrolmentScenario ES
		inner join EnrolmentProjection EP
			on ES.escnID = EP.escnID
		inner join EnrolmentProjectionData EPD
			on EP.epID = EPD.epID
		inner join Schools S
			ON S.schNo = EP.SchNo
		inner join Islands I
			ON I.iCode = S.iCode

	UNION
		SELECT null
		, @appname
		, EE.LifeYear
		, I.iGroup
		, null		--	S.schNo
		, null		-- S.schName
		, S.schType as SchoolTypeCode
		, S.schAuth as AuthCode

		, Enrollments.enAge
		, dbo.Enrollments.enLevel
		, sum(enM)
		, sum(enF)
		, null
		, EE.Estimate
		, 'A'


		FROM #ebse EE
			INNER JOIN	Enrollments
				ON EE.bestssID = dbo.Enrollments.ssID
			INNER JOIN Schools S
				ON EE.schNo = S.schNo
			INNER JOIN Islands I
				on S.iCode = I.iCode
		GROUP BY
			EE.LifeYEar
			, I.iGRoup

			, S.schType
			, s.schAuth
			, enAge
			, enLevel
			, EE.Estimate

) U

Select [Scenario ID]
	, [Scenario Name]
	, [Projection Year]
	, [District Code]
	, dName District
	, [School No]
	, [School Name]
	, SchoolTypeCode
	, ST.stDescription
	, Age
	, G.*
	, DA.*
	, DL.*
	, (case when G.GenderCode = 'M' then enrolM
			when G.GenderCode = 'F' then enrolF
			when G.GenderCode = '<>' then enrolU
 		end) Enrol
	,  (case when G.GenderCode = 'M' then enrolM
			else null
 		end) EnrolM
	,  (case when G.GenderCode = 'F' then enrolF
			else null
 		end) EnrolF
	,  (case when G.GenderCode = '<>' then enrolU
			else null
 		end) EnrolU
	, Estimate
	, Projection
from #tmpD P
	left JOIN Districts
		on P.[District Code] = Districts.dID
	LEFT JOIN TRSchoolTypes ST
		on P.SchoolTypeCode = ST.stCode
	LEFT JOIN DimensionAuthority DA
		on DA.[AuthorityCode] = P.authCode
	LEFT JOIN DimensionLEvel DL
		on DL.levelCode = P.epdLevel
	CROSS JOIN DimensionGenderU G
WHERE (case when G.GenderCode = 'M' then enrolM
			when G.GenderCode = 'F' then enrolF
			when G.GenderCode = '<>' then enrolU
 		end)  is not null
END
GO

