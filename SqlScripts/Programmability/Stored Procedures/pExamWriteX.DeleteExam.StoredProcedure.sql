SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 Jun 2024
-- Description:	Delete an exam, by ID
-- =============================================
CREATE PROCEDURE [pExamWriteX].[DeleteExam]
	-- Add the parameters for the stored procedure here
	@examID int
AS
BEGIN

SET NOCOUNT ON;

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
Select * from Exams WHERE exID = @examID
if @@ROWCOUNT = 1 begin
	begin try
		declare @C TABLE
		(
		cID uniqueidentifier PRIMARY KEY
		)
		INSERT INTO @C
		Select excID from ExamCandidates WHERE exID = @examID

		begin transaction

		declare @examDeleted int
		declare @CandidateItemsDeleted int
		declare @CandidateResultsDeleted int
		declare @CandidatesDeleted int

		print Formatmessage('Start Time: %s', format(getutcdate(),'HH:mm:ss.fff'))


		print Formatmessage('Collect Candidates %d Time: %s', @@ROWCOUNT, format(getutcdate(),'HH:mm:ss.fff'))
		DELETE from ExamItems WHERE exID = @examID
		print Formatmessage('Items Deleted %d Time: %s', @@ROWCOUNT, format(getutcdate(),'HH:mm:ss.fff'))

		Delete from EXamCAndidateItems
			WHERE excID in (Select CID from @C)
			-- WHERE excID in (Select excID from ExamCandidates WHERE exID = @exID)
			Select @CandidateItemsDeleted = @@ROWCOUNT
			print Formatmessage('Items Deleted %d Time: %s', @CandidateItemsDeleted, format(getutcdate(),'HH:mm:ss.fff'))

		Delete from EXamCAndidateResults
			WHERE excID in (Select CID from @C)
		--FROM ExamCandidateResults
		--	INNER JOIN @C C
		--	ON ExamCandidateREsults.excID = C.CID

			-- WHERE excID in (Select excID from ExamCandidates WHERE exID = @exID)
			Select @CandidateResultsDeleted = @@ROWCOUNT
			print Formatmessage('Results Deleted %d Time: %s', @CandidateResultsDeleted, format(getutcdate(),'HH:mm:ss.fff'))

		Delete from ExamCandidates
			WHERE excID in (Select CID from @C)
			-- WHERE exID = @exID
			Select @CandidatesDeleted = @@ROWCOUNT
			print Formatmessage('CandidatesDeleted %d Time: %s', @CandidatesDeleted, format(getutcdate(),'HH:mm:ss.fff'))

		DELETE from ExamScores WHERE exID = @examID

		DELETE FROM ExamStandardTest  WHERE exID = @examID
		DELETE from ExamSubjectGrades WHERE exID = @examID

		Delete from ExamAchievementLevels WHERE exID = @examID
		Delete from ExamBenchmarks WHERE exID = @examID
		Delete from ExamStandards WHERE exID = @examID  -- RI enforced
		Delete from ExamItems WHERE exID = @examID
		Delete from Exams WHERE exID = @examID
			Select @ExamDeleted = @@ROWCOUNT
		commit
		print Formatmessage('Transaction Committed Time: %s', format(getutcdate(),'HH:mm:ss.fff'))
		Select @ExamDeleted ExamDeleted, @CandidatesDeleted CandidatesDeleted,
		@CandidateResultsDeleted CandidateResultsDeleted, @CandidateItemsDeleted CandidateItemsDeleted
	end try

	--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch
end
print 'end'
END
GO

