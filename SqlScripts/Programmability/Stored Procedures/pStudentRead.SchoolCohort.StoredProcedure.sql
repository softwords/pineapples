SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2022
-- Description:	 Show teachers and pupils at a school in a year, at a class level
-- =============================================
CREATE PROCEDURE [pStudentRead].[SchoolCohort]
	-- Add the parameters for the stored procedure here
	@teacherID int = null
	, @schNo nvarchar(50) = null
	, @svyYear int = null
	, @classLevel nvarchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @yearOfEd int
	-- we must have either a teacher or a school, throw an error if not

	if (@teacherId is null and @schNo is null) begin
		raiserror('Must supply a teacher or school',16,1)
		return
	end

	if (@schNo is not null) begin
		select @schNo = schNo
		from Schools
		WHERE schNo = @schNo
		if (@@rowcount = 0) begin
			raiserror('School %s not defined',16,1,@schNo)
			return
		end
	end
	-- if we dont have  a school, get it from the teacher and/or year
	-- remember the settings for intial parameters
	declare @paramCase int = case when @schNo is null then 1
		when @teacherId is null then 2
		else 3 end

	if (@paramCase = 1) begin
		-- teacherID / no schNo
		Select TOP 1 @schNo = TL.schNo
		, @svyYear = TL.SurveyYear
		from warehouse.TeacherLevelStr TL
			INNER JOIN
			(Select DISTINCT schNo, stueYear from StudentEnrolment) SE
				ON TL.SchNo = SE.schNo
				AND TL.surveyYear = SE.stueYear
		WHERE (TL.surveyYear = @svyYear or @svyYEar is null)
		AND tID = @teacherID
		ORDER BY TL.SurveyYear DESC

---		Select @svyYEar, @schNo

		-- if not teaching in that year may not have the record
		if (@@ROWCOUNT = 0) begin
			if (@svyYear is null) begin
				raiserror('No teaching records for nominated teacher %i', 16,1,@teacherID)
				return
			end
			raiserror('No teaching record for nominated teacher %i in %i', 16,1,@teacherID, @svyYear)
			return

		end
	end
	if @paramCase = 2 begin
		-- schNo, no teacher iD
		if (@svyYEar is null) begin
			Select @svyYear = max(stueYear)
			from StudentEnrolment
			WHERE schNo = @schNo
		end

	end


	if @paramCase = 3 begin
		-- schNo and teacher iD supplied
		-- are they consistent
		Select TOP 1
			@svyYear = surveyYear
		from warehouse.teacherLocation TL
		WHERE (TL.surveyYear = @svyYear or @svyYEar is null)
		AND tID = @teacherID
		AND schNo = @schNo
		ORDER BY surveyYear DESC
		if @@rowcount = 0 begin
			if (@svyYear is not null) begin
				raiserror('Teacher %i not at school %s in year %i',16,1,@teacherID, @schNo, @svyYear)
				return
			end
			raiserror('Teacher %i not ever at school %s ',16,1,@teacherID, @schNo)
			return
		end
	end
	-- we now have schNo, year
	-- if classlevel was supplied as a parameter, ensure that it is valid
	-- otherwise clear it and find the default
	if @classLevel is not null begin
		if not exists(select classLevel from warehouse.Enrol
		WHERE classLevel = @classLevel
		AND SchNo = @schNo
		AND surveyYEar = @svyYear) begin
			Select @ClassLevel = null
		end
	end
	-- if we don't have class level:
	if (@classLevel is null) begin
		-- if teacher is supplied, get the lowest level taught by the teacher
		if (@teacherID is not null) begin
			-- find the lowest year of ed the teacher teaches at, in the given year
			declare @cIdx int
			Select @cidx = CHARINDEX('1', LevelStr) - 2
			from warehouse.TeacherLevelStr
			WHERE tID = @teacherID
			AND surveyYEar =  @svyYear

			Select @yearOfEd = lvlYear
			, @classLevel = L.codeCode
			FROM lkpLevels L
			INNER JOIN warehouse.Enrol E
				ON L.codeCode = E.classLevel
				AND E.schNo = @schNo
				AND L.lvlYear = @cidx
				-- dont really have to join on year here because we only want the correct classlevel, for the school type, matching the year of ed
			if (@classLevel is null) begin
				raiserror('Could not find class level taught by teacher %i',16,1,@teacherID)
				return
			end
		end
		if (@teacherID is null) begin
			-- get it from enrolments
			Select @classLevel = classLevel
			, @yearOfEd = lvlYear
			FROM
			(
			select E.classLevel , L.lvlYEar
			, row_number() over (order by L.lvlYear) R
			from warehouse.enrol E
				INNER JOIN lkpLevels L
					ON E.ClassLevel = L.codeCode
				WHERE schNo = @schNo
				AND surveyYEar = @svyYear
			) EE
			WHERE R = 1
		end
	end
	else begin
	-- classlevel was suppplied as a parameter
	-- get the YoE
		select @YearOfed = lvlYear
		FROM lkpLevels
		WHERE codeCode = @classLevel
		if (@yearOfEd is null) begin
			raiserror('Invalid class level supplied: %s',16,1,@classLevel)
			return
		end
	-- check the supplied classlevel is ok for the teacher
		if (@teacherID is not null) begin
			declare @ls nvarchar(1)
			Select  @ls = substring(LevelStr, @yearOfEd + 2,1)
			from warehouse.TeacherLevelStr
			WHERE tID = @teacherID
			AND surveyYEar =  @svyYear
			if @@rowcount = 0 or @ls <> '1' begin
				raiserror('Teacher %i does not teach class level %s in year %i',16,1, @teacherID, @classLevel, @svyYear)
				return
			end
		end
	end

-- we now have schNo, year classlevel and yearofed - may teacherID as well
declare @TeacherMatchFTPT int
declare @TeacherMatchFTE float
declare @StaffCount int
declare @TeachingFTPT int
declare @TeachingFTE float
declare @StudentMatch int
declare @StudentMatchM int
declare @StudentMatchF int

Select @TeacherMatchFTPT = sum(selectedLevel)
, @TeacherMatchFTE = sum(selectedLevel/Activities)
, @StaffCount =count(*)
, @TeachingFTPT = sum(case TAMX when 'T' then 1 when 'M' then 1 else 0 end)
, @TeachingFTE = sum(TeachingActivities/Activities)
FROM
(
Select choose(@yearOfEd+2 ,TPK, T00,T01,T02,T03,T04,T05,T06,T07,T08,T09,T10, T11,T12,T13,T14, T15) selectedLevel
, TPK+T00+T01+T02+T03+T04+T05+T06+T07+T08+T09+T10+ T11+T12+T13+T14+ T15 + T TeachingActivities
, convert(float,Activities) Activities
, TAMX
from warehouse.TeacherLocation
WHERe surveyYear = @svyYear
AND schNo = @schNo
) SUB


Select @StudentMatch = count(*)
, @StudentMatchM = sum(case when stuGender = 'M' then 1 end)
, @StudentMatchF = sum(case when stuGender = 'F' then 1 end)
from StudentEnrolment SE
WHERE (Se.stueClass = @classLevel)
AND SE.schNo = @schNo
AND stueYear = @svyYear


-- summary
Select @teacherID tID, @schNo schNo, @svyYear svyYear, @ClassLevel ClassLevel, @yearOfEd YoE
, @TeacherMatchFTPT TeacherMatchesFTPT
, @TeacherMatchFTE TeacherMatchesFTE
, @StaffCount StaffCount
, @TeachingFTPT TeachingFTPT
, @TeachingFTE TeachingFTE
, @StudentMatch StudentMatches
, @StudentMatchM StudentMatchesM
, @StudentMatchF StudentMatchesF

-- teachers -- all teachers at school
Select TL.tID
, schNo
, surveyYear
, LevelStr
, tSurname
, tGiven
, tDoB
, tSex
, TPK, T00,T01,T02,T03,T04,T05,T06,T07,T08,T09,T10, T11,T12,T13,T14, T15, X, A
, Activities
, Role
, Qualified
, Certified
, convert(int,choose(@yearOfEd+2 ,TPK, T00,T01,T02,T03,T04,T05,T06,T07,T08,T09,T10, T11,T12,T13,T14, T15)) InSelectedGrade
from warehouse.TeacherLevelStr TL
	INNER JOIN TeacherIdentity TI
	ON TL.tID = TI.tID
WHERe surveyYear = @svyYear
AND schNo = @schNo
--AND choose(@yearOfEd+2 ,TPK, T00,T01,T02,T03,T04,T05,T06,T07,T08,T09,T10, T11,T12,T13,T14, T15) = 1
ORDER BY tSurname, tGiven, tDoB

-- students in the selected grade
Select SE.stuID
, SE.stueClass
, SE.stueFrom
, SE.schNo
, SE.stueYear
, SE.stuFamilyName
, SE.stuGiven
, SE.stuCardID
, SE.stuGender
, SE.stuDoB
from StudentEnrolment SE
WHERE (Se.stueClass = @classLevel)
AND SE.schNo = @schNo
AND stueYear = @svyYear
ORDER BY stuFamilyName, stuGiven, stuDoB


-- get the levels to navigate to
-- derived from the school if no teacher, derived from the teacher otherwise
Select levelCode
from
(
Select DISTINCT classLevel levelCode
from warehouse.enrol
WHERE schNo = @schNo
AND surveyYear = @svyYear
) EE
INNER JOIN lkpLevels L
	ON codecode = levelCode
ORDER BY lvlYear

-- finally get the years to navigate to
-- derived from the school if no teacher, derived from the teacher otherwise
Select surveyYear, Enrol, EnrolM, EnrolF
from warehouse.EnrolschoolR
WHERE schNo = @schNo
ORDER BY surveyYear DESC


END
GO

