SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 11 2021 revised
-- Description:	Calculates key statistics of the Flow model, at the national level
-- this populates the Survial node of the VERMPAF Xml file
-- =============================================
CREATE PROCEDURE [warehouse].[_vermSurvival]
	-- Add the parameters for the stored procedure here
	@districtCode nvarchar(10) = null
	, @SendASXML int = 0
	, @xmlOut xml  = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @results TABLE
(
SurveyYear int
, YearOfEd int
, levelCode nvarchar(10)
, enrolM int
, enrolF int
, enrol int
, repM int
, repF int
, rep int
, PRM decimal(8,5)   -- promote rate
, PRF decimal(8,5)
, PR decimal(8,5)
, RRM decimal(8,5)		-- repeat rate
, RRF decimal(8,5)
, RR decimal(8,5)
, DRM decimal(8,5)		-- dropout rate
, DRF decimal(8,5)
, DR decimal(8,5)
, TRM decimal(8,5)		-- transition rate
, TRF decimal(8,5)
, TR decimal(8,5)
, SRM decimal(8,5)		-- survival rate
, SRF decimal(8,5)
, SR decimal(8,5)
)

if (@districtCode is null) begin
INSERT INTO @results
(SurveyYear
, YearOfEd
, levelCode
, enrolM , enrolF, enrol
, repM, repF, rep
, PRM, PRF, PR
, RRM, RRF, RR
, DRM, DRF, DR
, TRM, TRF, TR
)
SELECT
SurveyYear
, U.YearOfEd
, levelCode
, enrolM
, enrolF
, enrol

, repM
, repF
, rep

, PromoteRateM
, PromoteRateF
, PromoteRate

, RepeatRateM
, RepeatRateF
, RepeatRate

, DropoutRateM
, DropoutRateF
, DropoutRate

, SurvivalRateM	-- single year survival; ie "transition rate"
, SurvivalRateF
, SurvivalRate


FROM warehouse.FlowNationR U
	INNER JOIN ListDefaultPathLevels DL
			ON DL.YearOfEd = U.yearOfEd
end

if (@districtCode is not null) begin
INSERT INTO @results
(SurveyYear
, YearOfEd
, levelCode
, enrolM , enrolF, enrol
, repM, repF, rep
, PRM, PRF, PR
, RRM, RRF, RR
, DRM, DRF, DR
, TRM, TRF, TR
)
SELECT
SurveyYear
, U.YearOfEd
, levelCode
, enrolM
, enrolF
, enrol

, repM
, repF
, rep

, PromoteRateM
, PromoteRateF
, PromoteRate

, RepeatRateM
, RepeatRateF
, RepeatRate

, DropoutRateM
, DropoutRateF
, DropoutRate

, SurvivalRateM	-- single year survival; ie "transition rate"
, SurvivalRateF
, SurvivalRate

FROM warehouse.FlowDistrictR U
	INNER JOIN ListDefaultPathLevels DL
			ON DL.YearOfEd = U.yearOfEd
	WHERE U.districtCode = @districtCode
end

declare @i int
declare @max int
SELECT @max = max(lvlYear)
from lkpLevels

-- this part multiplies the transitions rates together
UPDATE @results
	SET SRM = 1
	, SRF = 1
	, SR = 1
WHERE yearofEd in (0,1)

Select @i = 1
while (@i < @max) begin
	UPDATE @Results
	SET SRM = T.SRM * T.TRM
	, SRF = T.SRF * T.TRF
	, SR = T.SR * T.TR
	FROM @REsults
		INNER JOIN @results T
	ON [@results].SurveyYear = T.SurveyYear
		AND [@results].yearOfEd = T.yearOfEd + 1
		AND T.yearofEd = @i
	WHERE T.SR * T.TR < 1000	-- kill these bad survivial rates here

	SELECT @i = @i + 1

end

IF @SendASXML = 0 begin
	SELECT * from @results
	ORDER BY surveyYear
	, yearOfEd

end

IF @SendASXML <> 0 begin

	declare @XML xml
	SELECT @XML =
	(
		SElect surveyYear [@year]
		, levelCode [@levelCode]
		,yearOfEd	[@yearOfEd]
		, EnrolM
		, EnrolF
		, Enrol
		, RepM
		, RepF
		, Rep
		, cast(PRM as decimal(8,5)) PRM
		, cast(PRF as decimal(8,5)) PRF
		, cast(PR as decimal(8,5)) PR
		, cast(RRM as decimal(8,5)) RRM
		, cast(RRF as decimal(8,5)) RRF
		, cast(RR as decimal(8,5)) RR
		, cast(DRM as decimal(8,5)) DRM
		, cast(DRF as decimal(8,5)) DRF
		, cast(DR as decimal(8,5)) DR
		-- change these names to T1R (single-year transition) becuae jquery is not happy
		-- with the node name TR (thinks we mean table row???)
		, cast(TRM as decimal(8,5)) T1RM
		, cast(TRF as decimal(8,5)) T1RF
		, cast(TR as decimal(8,5)) T1R
		, cast(SRM as decimal(8,5)) SRM
		, cast(SRF as decimal(8,5)) SRF
		, cast(SR as decimal(8,5)) SR
	FROM @results

	FOR XML PATH('Survival')
	)

	SELECT @XMLOut =
	(
		SELECT @xml
		FOR XML PATH('Survivals')
	)

	IF @SendASXML = 1
		SELECT @XMLOut
end
END
GO

