SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 03 2018
-- Description:	Student filter
-- =============================================
CREATE PROCEDURE [pSchoolRead].[StudentFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@StudentID uniqueidentifier = null,
	@StudentCardID nvarchar(20) = null,
    @StudentGiven nvarchar(50) = null,
    @StudentFamilyName  nvarchar(50) = null,
    @StudentDoB  date = null,
	@StudentGender nvarchar(1) = null,
	@StudentEthnicity nvarchar(200) = null,

	-- selection based on enrolment
	@EnrolledAt nvarchar(50) = null,
	@EnrolYear int = null,
	@EnrolLevel nvarchar(10) = null,
	@EnrolDistrict nvarchar(10) = null,
	@EnrolAuthority nvarchar(10) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
	selectedStuID nvarchar(50)
	, recNo int IDENTITY PRIMARY KEY
	)
	DECLARE @enrolMatch TABLE
	(
		ID uniqueidentifier
	)
	DECLARE @UseEnrol int = 0;
	-- clean up some empty strings that may be passed from the client
	Select @StudentGiven = nullif(ltrim(@StudentGiven),'')
	, @StudentFamilyName = nullif(ltrim(@StudentFamilyName),'')
	, @StudentCardID = nullif(ltrim(@StudentCardID),'')

	-- interpret enrolyear - if it is 0:
	-- if school supplied, the most recent enrolment for the school
	-- if no school, the most recent year for any school
	--
	if (@EnrolYear = 0) begin
		select @enrolYear = max(stueYear)
		from StudentEnrolment_
		WHERE (schNo = @EnrolledAt or @EnrolledAt is null)
	end
	print @EnrolYear
	if ( @EnrolledAt is not null) or (@EnrolYear is not null) or (@EnrolLevel is not null)
		or (@EnrolDistrict is not null) or (@EnrolAuthority is not null)  begin
		select @UseEnrol = 1
		--INSERT INTO @enrolMatch
		--(ID)
		--Select stuID
		--FROM
		--StudentEnrolment_ SE
		--	LEFT JOIN Schools S
		--		ON SE.schNo = S.schNo
		--			LEFT JOIN Islands I
		--				ON S.iCode = I.iCode
		--WHERE (SE.schNo = @EnrolledAt or @EnrolledAt is null)
		--AND (I.iGroup = @EnrolDistrict or @EnrolDistrict is null)
		--AND (S.schAuth = @EnrolAuthority or @EnrolAuthority is null)
		--AND (stueClass = @EnrolLevel or @EnrolLevel is null)
		--AND (stueYEar = @EnrolYear or @EnrolYear is null)
	end

	Select @StudentGiven = replace(@StudentGiven, '*','%')
	, @StudentFamilyName = replace(@StudentFamilyName, '*','%')
	-- if the user has entered a * or % in the search string, do a fuzzy search
	Declare @FuzzySearch int = 0
	if (charindex('%', @StudentGiven) > 0 or charindex('%', @StudentFamilyName) > 0) begin
		Select @FuzzySearch = 1
	end
	else begin
		select @StudentGiven = @StudentGiven + '%'
		, @StudentFamilyName = @StudentFamilyName + '%'
	end
	INSERT INTO @keys
	(selectedStuID)
	SELECT stuID
	FROM Student_ S
	WHERE
	(stuID = @StudentID or @StudentID is null)
	AND (S.stuCardID = @StudentCardID or @StudentCardID is null)
	AND (
		(@StudentGiven is null and @StudentFamilyName is null)
		OR
		( @FuzzySearch = 0
			AND (S.stuGiven like @StudentGiven or @StudentGiven is null)
			AND (S.stuFamilyName like @StudentFamilyName or @StudentFamilyName is null)
		) OR
		( @FuzzySearch = 1
			AND (	S.stuGiven like @StudentFamilyName
				OR S.stuFamilyName like @StudentFamilyName
				OR soundex(@StudentFamilyName) in (S.stuGivenSoundex,S.stuFamilySoundex)
				OR @StudentFamilyName is null)
			AND (  S.stuGiven like @StudentGiven
				OR S.stuFamilyName like @StudentGiven
				OR soundex(@StudentGiven) in (S.stuGivenSoundex,S.stuFamilySoundex)
				OR @StudentGiven is null)
		)
	)
	AND (S.stuDoB = @StudentDoB or @StudentDoB is null)
	AND (S.stuGender = @StudentGender or @StudentGender is null)
	AND (S.stuEthnicity = @StudentEthnicity or @StudentEthnicity is null)
	AND (@useEnrol = 0 or S.stuID in
		(
		Select stuID
		FROM
		StudentEnrolment_ SE
			LEFT JOIN Schools S
				ON SE.schNo = S.schNo
					LEFT JOIN Islands I
						ON S.iCode = I.iCode
		WHERE (SE.schNo = @EnrolledAt or @EnrolledAt is null)
		AND (I.iGroup = @EnrolDistrict or @EnrolDistrict is null)
		AND (S.schAuth = @EnrolAuthority or @EnrolAuthority is null)
		AND (stueClass = @EnrolLevel or @EnrolLevel is null)
		AND (stueYEar = @EnrolYear or @EnrolYear is null)
		)
	)

	ORDER BY
	Case
		when @FuzzySearch = 1 AND (coalesce(@StudentGiven, @StudentFamilyName) is not null) then
			case when @studentGiven is null then
				case when S.stuFamilyName = replace(@StudentFamilyName,'%','') then 0
					when S.stuGiven = replace(@StudentFamilyName,'%','') then 10			--swap
					when S.stuFamilyName like @StudentFamilyName then 20					--like
					when S.stuGiven like @StudentFamilyName then 20					--like swap
					else 999
				end -- family name search
			when @StudentFamilyName is null then
				-- search only on given
				case when S.stuGiven = replace(@StudentGiven,'%','') then 0
					when S.stuFamilyName = replace(@StudentGiven,'%','') then 10			--swap
					when S.stuGiven like @StudentGiven then 20					--like
					when S.stuFamilyName like @StudentGiven then 20					--like swap
					else 999
				end -- given name search

			else
				case when replace(@StudentGiven,'%','') = S.stuGiven
					and replace(@StudentFamilyName,'%','') = S.stuFamilyName then 0
			-- swapped
				when replace(@StudentGiven,'%','') = S.stuFamilyName
					and replace(@StudentGiven,'%','') = S.stuGiven then 10
			-- like
				when S.stuGiven like @StudentGiven and S.stuFamilyName like @StudentFamilyName then 12
			-- like swap
				when S.stuGiven like @StudentFamilyName and S.stuFamilyName like @StudentGiven then 15

			-- one exact 1 soundex
				when S.stuGiven like @StudentGiven and soundex(@StudentFamilyName) = S.stuFamilySoundex then 20
				when soundex(@StudentGiven) = S.stuGivenSoundex and S.stuFamilyName like @StudentFamilyName then 20
			-- one exact 1 soundex swapped
				when @StudentGiven = S.stuFamilyName and soundex(@StudentFamilyName) = S.stuGivenSoundex then 30
				when soundex(@StudentGiven) = S.stuFamilySoundex and @StudentFamilyName = S.stuGiven then 30
			-- 1 exact
				when @StudentGiven = S.stuGiven then 40
				when @StudentFamilyName = S.stuFamilyName then 40
			-- 1 exact swapped
				when @StudentGiven = S.stuFamilyName then 50
				when @StudentFamilyName = S.stuGiven then 50
				else 999
			end
			end -- given and family name search
		else 0
		end,

		CASE @sortColumn			-- strings


			WHEN 'stuCardID' then stuCardID
			WHEN 'stuGiven' then stuGiven
			WHEN 'stuFamilyName' then stuFamilyName
			WHEN 'stuEthnicity' then stuEthnicity
			WHEN 'stuGender' then stuGender

			WHEN 'StudentCardID' then stuCardID
			WHEN 'StudentGiven' then stuGiven
			WHEN 'StudentFamilyName' then stuFamilyName
			WHEN 'StudentEthnicity' then stuEthnicity
			WHEN 'StudentGender' then stuGender
			ELSE null
			END,
		CASE @sortColumn -- dates
			WHEN 'StudentDoB' then stuDoB
			WHEN 'stuDoB' then stuDoB
			ELSE null
		END,

		stuID


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedStuID
		, RecNo
		FROM
		(
			Select selectedStuID
			, @NumMatches - RecNo + 1 RecNo
			FROM @Keys
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)
				)
		ORDER BY RecNo
	end
	else begin

		SELECT selectedStuID
		, RecNo
		FROM @Keys
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
END
GO

