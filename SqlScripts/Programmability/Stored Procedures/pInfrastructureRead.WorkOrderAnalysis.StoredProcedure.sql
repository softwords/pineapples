SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 1 2010
-- Description:
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[WorkOrderAnalysis]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT schNo
	, sum(case witGroup when 'N' then witmEstCost end) totalN
	, sum(case witGroup when 'M' then witmEstCost end) totalM
	, sum(case witGroup when 'R' then witmEstCost end) totalR
	, sum(case witGroup when 'F' then witmEstCost end) totalF
	, sum(case witGroup when 'X' then witmEstCost end) totalX


INTO #wiTotals
FROM
	WorkItems WI
	INNER JOIN lkpWorkItemType
	ON WI.witmType = lkpWorkItemType.codeCode
GROUP BY SchNo

CREATE TABLE #woAnalysis
(
	Tag				int
	, Parent		int
	, districtCode	nvarchar(10) NULL
	, district		nvarchar(50) NULL
	, iSeq			int NULL
	, islandCode	nvarchar(10) NULL
	, island		nvarchar(50) NULL
	, schoolNo		nvarchar(50) NULL
	, schoolName	nvarchar(100) NULL
	, ISClass		nvarchar(10) NULL
	, ISSize		nvarchar(10) NULL
	, witmType		nvarchar(15) NULL
	, witmDesc		nvarchar(50) NULL
	, witmEstCost	money NULL
	, witmQty		decimal(18,2) NULL
	, totalM		decimal(18,2) NULL
	, totalN		decimal(18,2) NULL
	, totalF		decimal(18,2) NULL
	, totalR		decimal(18,2) NULL
	, totalX		decimal(18,2) NULL
)

INSERT INTO #woAnalysis
(Tag, Parent)
SELECT 1, null
-- first level is district

INSERT INTO #woAnalysis
(Tag, Parent, DistrictCode, district)
SELECT 2
	,  1
	, dID
	, dName
from Districts
ORDER BY dID


INSERT INTO #woAnalysis
(Tag, Parent, DistrictCode, iSeq, islandCode, island)
SELECT 3
	,  2
	, iGroup
	, iSeq
	, iCode
	, iName
from Islands
ORDER BY iGRoup, iCode

-- the principal row is based on school
INSERT INTO #woAnalysis
(Tag, Parent, DistrictCode, iSeq, islandCode
, schoolNo, schoolName, ISClass, ISSize
	, totalN
	, totalM
	, totalR
	, totalF
	, totalX)
SELECT 4
	,  3
	, iGroup
	, iSeq
	, Islands.iCode
	, Schools.schNo
	, schName
	, ssISClass
	, ssISSize
	, totalN
	, totalM
	, totalR
	, totalF
	, totalX
from Islands
	INNER JOIN Schools
		ON Islands.iCode = Schools.iCode
	INNER JOIN SchoolSurvey
		ON Schools.schNo = SchoolSurvey.schno
		AND SchoolSurvey.svyYear in (Select max(svyYear) from SchoolSurvey SS WHERE SS.SchNo = Schools.schNo)
	LEFT JOIN #wiTotals on Schools.schNo = #wiTotals.schNo
INSERT INTO #woAnalysis
(Tag, Parent, DistrictCode, iSeq, islandCode
, schoolNo, witmType, witmEstCost, witmQty)
SELECT 5
	,  4
	, iGroup
	, iSeq
	, Islands.iCode
	, Schools.schNo
	, witmType
	, witmEstCost
	, isnull(witmQty,1)
from Islands
	INNER JOIN Schools
		ON Islands.iCode = Schools.iCode
	INNER JOIN WorkItems WI
		ON Schools.SchNo = WI.SchNo


Select Tag
	, Parent
	, 'WorkOrders' as	[WorkOrders!1!ID!Hide]
	, districtCode as	[District!2!code]
	, district as		[District!2!name]
	, islandCode as		[Island!3!code]
	, island as			[Island!3!name]
	, schoolNo as		[School!4!schoolNo]
	, schoolName as		[School!4!schoolName]
	, ISCLass as		[School!4!isClass]
	, ISSize as			[School!4!isSize]
	, totalN as			[School!4!totalN]
	, totalM as			[School!4!totalM]
	, totalR as			[School!4!totalR]
	, totalF as			[School!4!totalF]
	, totalX as			[School!4!totalX]
	, witmType as		[WorkItem!5!type]
	, witmEstCost as	[WorkItem!5!cost]
	, witmQty as		[WorkItem!5!qty]

from #woAnalysis
ORDER by districtCode, iSeq, islandCode, schoolNo, witmType, tag
FOR XML EXPLICIT

DROP TABLE #woAnalysis
END
GO

