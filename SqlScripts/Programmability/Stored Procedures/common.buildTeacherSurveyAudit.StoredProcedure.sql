SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 12 2020
-- Description:	Parse the xml data field on teacher survey
-- to build a select query enumerating all the attributes that appear in that field
-- Limitation is getting them in the same order as they appear on the workbook,
-- in the output from this proc they are not quite right. So, use  [pSurveyOps].[TeacherSurveyAudit]
-- for a view that is easier to compare to the workbook.
--  [pSurveyOps].[TeacherSurveyAudit] can be rebuiilt periodically from this proc (see messages window)
-- and tidied up as needed to get fields in the right order.
-- Also, the view is more usful for joining to other objects.
-- =============================================
CREATE PROCEDURE [common].[buildTeacherSurveyAudit]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @r TABLE
	(
		f nvarchar(500)
		, RN int
	)

INSERT INTO @r
Select Attribute
, max(RN)
FROM
(
SELECT
CAST(Attribute.Name.query('local-name(.)') AS VARCHAR(100)) Attribute
, row_number() OVER (Partition by tchsID ORDER BY tchsID) RN
FROM TeacherSurvey
CROSS APPLY tcheData.nodes('//@*') Attribute(Name)
) SUB
GROUP BY Attribute
ORDER BY max(RN)


  Select *
  from @r
  ORDER BY RN

	declare @t nvarchar(max) = 'Select tchsID, tId, ssID, schNo, svyYear ' + char(13) + char(10)

	Select @t = @t +', tcheData.value(''(//row/@' + f + ')[1]'', ''nvarchar(100)'') [' + f + ']' + char(13) + char(10)
	FROM @r
	ORDER BY RN

	Select @t = @t + ' FROM pTeacherRead.TeacherSurveyV'
	print @t

	print right(@t,2000)

	exec sp_sqlexec @t
END
GO

