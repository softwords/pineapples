SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Brian Lewis
-- Create date: 13 08 2015
-- Description: Read multiple reordsets about school
-- 28 3 2023 Sort by name
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SchoolReadEx]
 -- Add the parameters for the stored procedure here
 @SchoolNo nvarchar(50)
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

 SELECT * from Schools WHERE schNo = @schoolNo
 if @@ROWCOUNT = 0 begin
	raiserror('Record not found: %s',16,1,@schoolNo)
	return
 end
 -- survey summary data
 exec pEnrolmentRead.schoolAnnualSummary  @schoolNo
 Select * from pExamRead.SchoolExams WHERE schNo = @schoolNo
 Select * from pInspectionRead.SchoolInspections WHERE schNo = @schoolNo

 	SELECT *
	from pSchoolRead.SchoolLinks
	WHERE schNo = @SchoolNo

 -- quarterly reports is specific to RMI. However, this recordset will always be
 -- exported to simplfy the construction of the School object
 Select * from pInspectionRead.QuarterlyReports WHERE schNo = @schoolNo
 Select * from pInspectionRead.SchoolAccreditations WHERE schNo = @schoolNo

 -- the teachers at the school
 select  T.tGiven
 , T.tSurname
 , T.tchGender
 , T.tchDoB
 , T.tPayroll
 , T.tProvident
 , T.tchRole
 , WT.Certified
 , WT.Qualified
 , WT.TPK
 , T00, T01, T02, T03, T04, T05, T06, T07, T08, T09, T10, T11, T12, T13, T14, T15
 , T, A, X, Activities
	from pTeacherRead.TeacherSurveyV T
	--from warehouse.TeacherLocation T
	INNER JOIN
	(Select schNo, max(svyYear) MaxSurvey from SchoolSurvey GROUP BY schNo) SS
	ON T.schNo = SS.schNo
	AND T.svyYear = SS.MaxSurvey
	LEFT JOIN warehouse.TeacherLocation WT
		ON WT.tID = T.tID
		AND WT.SurveyYear = T.svyYear
	WHERE T.schNo = @schoolNo
ORDER BY T.tSurname, T.tGiven
END
GO

