SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 12 2007
-- Description:	data for transfers pivot
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVTransfersData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- enrolment estimaates
Select *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT E.LifeYear AS [Survey Year],
E.schNo,
E.Estimate,
E.bestYear AS [Year of Data],
E.Offset AS [Age of Data],
E.bestssqLevel AS [Data Quality Level],
E.ActualssqLevel AS [Survey Year Quality Level],
E.surveyDimensionssID,
ptCode,
LevelCode,
trM, trF,
ssIDTransfersLevel.TrfrCode,
ssIDTransfersLevel.TrfrName
FROM #ebse E
INNER JOIN ssIDTransfersLevel
	ON E.bestssID = ssIDTransfersLevel.ssID


    -- Insert statements for procedure here
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVTransfersData] TO [pEnrolmentRead] AS [dbo]
GO

