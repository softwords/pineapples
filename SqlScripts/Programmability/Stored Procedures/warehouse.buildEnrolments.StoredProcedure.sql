SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2016
-- Description:
-- =============================================
CREATE PROCEDURE [warehouse].[buildEnrolments]
	@StartFromYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- get rid of 'Warning: Null value is eliminated by an aggregate or other SET operation.'
	set ansi_warnings off
    -- Insert statements for procedure here

	-- support @StartFromYear
	-- dont assume we may ever need to create this object
		begin transaction
		DELETE
		FROM warehouse.measureEnrolSchoolG
		WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)
		print 'warehouse.measureEnrolSchoolG deletes - rows:' + convert(nvarchar(10), @@rowcount)

		-- WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
		INSERT INTO warehouse.measureEnrolSchoolG
		(
			schNo
			, SurveyYear
			, Estimate
			, SurveyDimensionID
			, GenderCode
			, Enrol
		)
		SELECT
			schNo
			, SurveyYear
			, Estimate
			, SurveyDimensionID
			, GenderCode
			, Enrol
		FROM dbo.measureEnrolSchoolG		-- note if we don;t put dbo. here, it defaults to warehouse schema...
		WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

		print 'warehouse.measureEnrolSchoolG inserts - rows:' + convert(nvarchar(10), @@rowcount)
	commit


	-- enrolment-related pupil tables
	begin transaction
		DELETE
		FROM Warehouse.PupilTablesG
		WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)
		print 'warehouse.pupilTablesG deletes - rows:' + convert(nvarchar(10), @@rowcount)

		INSERT INTO warehouse.PupilTablesG
		Select
		schNo
		, LifeYear SurveyYear
		, Estimate
		, SurveyDimensionssID SurveyDimensionID

		, ptLevel ClassLevel
		, ptAge Age
		, G.genderCode
		, case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end pt
		, ptCode
		--- here we use Enrolments Actual ie ignore any explicit estimates ('smoothing')
		--- this means that smoothed enrolments may be entered, without the assumption'
		--- that explicit pupil tables values are supplied
		--- in fact, explicit pupil table values will be ignored if Enrolments are an Explicit Estimate
		from dbo.tfnESTIMATE_BestSurveyEnrolmentsActual() B
			INNER JOIN PupilTables PT
				ON B.bestssID = PT.ssID
			CROSS JOIN DimensionGender G
		WHERE ptCode in ('BRD', 'REP', 'TRIN','TROUT', 'DIS', 'DROP','PSA','EXPL','COMP')
		AND isnull(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end ,0) <> 0
 		AND (LifeYear >=@StartFromYear or @StartFromYear is null)
		print 'warehouse.pupilTablesG inserts - rows:' + convert(nvarchar(10), @@rowcount)
--- pre school attenders ( in the same transaction)
--- this is assuming that preschool attenders are in PupilTables with code KINDER
--- This is therefore for histroical purposes?? Probably not need, but won;t do any harm
--- since it won;t find anything to write in FSM RMI systems

	declare @yr1 nvarchar(10)
	select @yr1 = levelCode
		FROM ListDefaultPathLevels
		WHERE yearOfEd = 1

	If @yr1 is null
		Select top 1 @yr1 =  codeCode
		from lkpLevels
		-- note that warehouse.PupilTablesG must exist by now
		INSERT INTO warehouse.PupilTablesG

		Select
		schNo
		, LifeYear SurveyYear
		, Estimate
		, SurveyDimensionssID SurveyDimensionID

		, @yr1 ClassLevel
		, ptAge Age
		, G.genderCode
		, sum(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end) pt
		, 'PSA'
		from dbo.tfnESTIMATE_BestSurveyEnrolmentsActual() B
		-- in vermpaf, psa is only shown if there are Enrolments in yearofEd = 1
		-- (the psa are a subset of these)
		-- so this join will exclude any psa that do not have an enrolment record,
		-- even if that psa is in PupilTables
		-- see [dbo].[EnrolmentRatiosByYearOfEd]
--			INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelN N
--				ON B.bestssID = N.ssID
--				AND N.YearOfEd = 1
			INNER JOIN PupilTables PT
				ON B.bestssID = PT.ssID
			CROSS JOIN DimensionGender G
		WHERE ptCode in ('KINDER')
 		AND (LifeYear >=@StartFromYear or @StartFromYear is null)

		GROUP BY
		schNo
		, LifeYear
		, Estimate
		, SurveyDimensionssID
		, ptAge
		, G.genderCode
		HAVING isnull(sum(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end) ,0) <> 0
		print 'warehouse.pupilTablesG inserts (KINDER) - rows:' + convert(nvarchar(10), @@rowcount)

		commit

		-- combine with enrolments and cross tab the measures
		-- OUTPUT: warehouse.enrol

		begin transaction

			DELETE FROM warehouse.enrol
			WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)

			print 'warehouse.enrol deletes - rows:' + convert(nvarchar(10), @@rowcount)

			INSERT INTO warehouse.enrol
			(
				schNo
				, SurveyYear
				, Estimate
				, SurveyDimensionID
				, ClassLevel
				, Age
				, GenderCode
				, Enrol
				, Rep
				, Trin
				, Trout
				, Boarders
				, Disab
				, Dropout
				, Expelled
				, PSA
				, Completed
			)
			Select schNo
				, surveyYear
				, max(Estimate)
				, SurveyDimensionID
				, ClassLevel
				, nullif(Age,0) Age
				, GenderCode
				, sum(case when ptCode = 'E' then Enrol end) Enrol
				, sum(case when ptCode = 'REP' then Enrol end) Rep
				, sum(case when ptCode = 'TRIN' then Enrol end) Trin
				, sum(case when ptCode = 'TROUT' then Enrol end) Trout
				, sum(case when ptCode = 'BRD' then Enrol end) Boarders
				, sum(case when ptCode = 'DIS' then Enrol end) Disab
				, sum(case when ptCode = 'DROP' then Enrol end) Dropout
				, sum(case when ptCode = 'EXPL' then Enrol end) Expelled
				, sum(case when ptCode = 'PSA' then Enrol end) PSA
				, sum(case when ptCode = 'COMP' then Enrol end) Completed
				FROM
				-- union query to combine enrolment with pupil table columns, summing to flatten to one record
				(
				SELECT schNo
				  ,SurveyYear
				  ,Estimate
				  ,SurveyDimensionID
				  ,ClassLevel
				  ,Age
				  ,genderCode
				  ,Enrol
				, 'E' ptCode from measureEnrolG
				WHERE Enrol is not null		-- issue #1109 https://bitbucket.org/softwords/pineapples/issues/1109

				UNION ALL
				SELECT schNo
				  ,SurveyYear
				  , 0 -- Estimate is not used here so the Estimate on warehouse.enrol relates explcitly to the enrolment
				  ,SurveyDimensionID
				  ,ClassLevel
				  ,Age
				  ,genderCode
				  ,pt Enrol
				  ,ptCode
				from warehouse.pupilTablesG

				) U
				WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)
				GROUP BY
				schNo
				, surveyYear
				--- , Estimate   - if EXPLICIT enrolments, Estimate of enrolment will be 1, but estimate of PupilTables will be 0 - ie can;t group on this
				, SurveyDimensionID
				, ClassLevel
				, Age
				, GenderCode
		print 'warehouse.enrol inserts: '+ convert(nvarchar(10), @@rowcount)


		commit
		-- group by key dimension keys
		-- OUTPUT:  warehouse.tableEnrol
		begin transaction

		-- reconstruct TableEnrol from Enrol
		DELETE FROM warehouse.TableEnrol
		WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)

		print 'warehouse.tableenrol deletes: '+ convert(nvarchar(10), @@rowcount)

		INSERT INTO warehouse.tableEnrol
		(
			SurveyYear
			, Estimate
			, ClassLevel
			, Age
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, SchoolTypeCode
			, Enrol
			, Rep
			, TrIn
			, TrOut
			, Boarders
			, Disab
			, Dropout
			, Expelled
			, PSA
			, Completed
		)
		Select E.SurveyYear
			, E.Estimate
			, ClassLevel
			, nullif(Age, 0) Age
			, GenderCode

			, DSS.[District Code] DistrictCode

			, [AuthorityCode]
			, [SchoolTypeCode]
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(Trin) TrIn
			, sum(TROUT) TrOut
			, sum(Boarders) Boarders
			, sum(Disab) Disab
			, sum(Dropout) Dropout
			, sum(Expelled) Expelled
			, sum(PSA) PSA
			, sum(Completed) Completed

			from warehouse.enrol E
			INNER JOIN warehouse.dimensionSchoolSurvey DSS
			ON E.surveyDimensionID = DSS.[Survey ID]
			WHERE (E.SurveyYear >=@StartFromYear or @StartFromYear is null)
			GROUP BY
			E.SurveyYear
			, E.Estimate
			, ClassLevel
			, Age
			, GenderCode

			, DSS.[District Code]
			, [AuthorityCode]
			, [SchoolTypeCode]
		print 'warehouse.tableenrol inserts: '+ convert(nvarchar(10), @@rowcount)
		commit transaction


END
GO

