SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 10 2020
-- Description:	Filter of scholarships
-- =============================================
CREATE PROCEDURE [pScholarshipRead].[ScholarshipFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@ScholarshipID int = null,
	@ScholarshipType nvarchar(10) = null,
	@ScholarshipStatus nvarchar(20) = null,
	@ScholarshipStatusChange datetime = null,
	@ScholarshipYear int = null,

	@StudentID uniqueidentifier = null,
	@StudentCardID nvarchar(20) = null,
    @StudentGiven nvarchar(50) = null,
    @StudentFamilyName  nvarchar(50) = null,
    @StudentDoB  date = null,
	@StudentGender nvarchar(1) = null,
	@StudentEthnicity nvarchar(200) = null,

	-- selection based on program of study
	@EnrolledAt nvarchar(50) = null,			--- the institution
	@EnrolYear int = null,						--  year
	@EnrolLevel int	= null,						-- from enrolment level 1 2 3 4 = Freshman, sophomore etc
	@EnrolField nvarchar(10) = null,
	@EnrolFieldGroup nvarchar(10) = null,
	@GPAMin decimal(3,2) = null,
	@GPAMax decimal(3,2) = null

AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int


	INSERT INTO @keys
	EXEC pScholarshipRead.ScholarshipFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	@ScholarshipID,
	@ScholarshipType,
	@ScholarshipStatus,
	@ScholarshipStatusChange,
	@ScholarshipYear,

	@StudentID ,
	@StudentCardID,
    @StudentGiven ,
    @StudentFamilyName,
    @StudentDoB,
	@StudentGender,
	@StudentEthnicity,

	-- selection based on program of study
	@EnrolledAt,		--- the institution
	@EnrolYear,					--  year
	@EnrolLevel,					-- from enrolment level 1 2 3 4 = Freshman, sophomore etc
	@EnrolField ,
	@EnrolFieldGroup,
	@GPAMin,
	@GPAMax


--- column sets ----

if @Columnset = 0 begin
	Select * from
	(
		SELECT
		S.schoID
		, S.scholCode
		, S.schrYear
		, S.schoStatus
		, S.stuID
		, S.stuCardID
		, S.stuGiven
		, S.stuFamilyName
		, S.stuDoB
		, S.stuGender
		, S.stuEthnicity

		, ROW_NUMBER() over (PARTITION BY S.schoID ORDER BY posYear DESC, posSemester DESC) RN
		, SS.posYear
		, SS.posSemester
		, SS.gyIndex
		, isnull(SS.instCode, S.schoinst) instCode
		, isnull(SS.fosCode, S.schoFos) fosCode
		FROM Scholarships S
			LEFT JOIN ScholarshipStudy_ SS
			ON S.schoID = SS.schoID
	) SUB
	INNER JOIN @keys K
		ON SUB.schoID = K.ID
	WHERE RN = 1
	ORDER BY recNo
end

if @Columnset = 1 begin
	Select * from
	(
		SELECT
		S.schoID
		, S.scholCode
		, S.schrYear
		, S.schoStatus
		, S.stuID
		, S.stuCardID
		, S.stuGiven
		, S.stuFamilyName
		, S.stuDoB
		, S.stuGender
		, S.stuEthnicity
		, S.schoStatusDate
		, S.schoExpectedCompletion
		, S.schoAppliedDate
		, S.schoCompliantDate
		, S.schoAwardedDate
		, S.schoCompletedDate
		, ROW_NUMBER() over (PARTITION BY S.schoID ORDER BY posYear DESC, posSemester DESC) RN
		, SS.posYear
		, SS.posSemester
		, SS.gyIndex
		, isnull(SS.instCode, S.schoinst) instCode
		, isnull(SS.fosCode, S.schoFos) fosCode
		FROM Scholarships S
			LEFT JOIN ScholarshipStudy_ SS
			ON S.schoID = SS.schoID
	) SUB
	INNER JOIN @keys K
		ON SUB.schoID = K.ID
	WHERE RN = 1
	ORDER BY recNo
end


if @columnset = 2 begin -- status review
	Select RS.*
	from pScholarshipRead.StatusReview RS
	INNER JOIN @keys K
		ON RS.schoID = K.ID
	ORDER BY recNo

end

--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

