SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 05 2010
-- Description:	Build the SchoolYearHistory records
-- =============================================
CREATE PROCEDURE [common].[BuildSchoolYearHistory]
	-- Add the parameters for the stored procedure here
	WITH EXECUTE AS 'pineapples'
AS
BEGIN
	SET NOCOUNT ON;
-- begin by inserting all the records for the school life years - that are after the first survey year.

	declare @ActiveYear int
	declare @MaxYear int
	declare @StartHistory int
	declare @LastSurvey int

	select @ActiveYear = common.ActiveSurveyYear()
	-- the reason for this is:
	-- when the new survey year is created, any provisional settings in SchoolYearHistory for the new year
	-- (e.g. created by Establishment0 are applied to the Schools table making them current.
	-- These could then be restrosecptively applied to the current year records
	select @StartHistory = min(svyYear)
	, @LastSurvey = max(svyYear)
	from Survey

	select @MaxYear = max(syYear)		-- there may be years in history thatare not in survey
	from SchoolYearHistory

	If isnull(@MaxYear,0) < @LastSurvey
		SELECT @MaxYEar = @LastSurvey
	-- if its an insert
	-- the range of years to add should be from
	-- min(Established Year, current Year)
	-- to the maximum year represented in SurveyYear History
	-- This will add them to the establishment plan and survey control


	UPDATE Schools
		SET schEst = 0
		WHERE schEst is null

	-- note this query will add any needed records if an established date is changed
	INSERT INTO SchoolYearHistory
	( syYear
		, schNo

	)
	SELECT num
	,Schools.schNo
	from Schools

	INNER JOIN metaNumbers
	ON num between
		case when Schools.schEst < @StartHistory then @StartHistory else Schools.schEst end
		and
		case when Schools.schClosed = 0 then @MaxYear
			 when Schools.schClosed > @MaxYear then @MaxYear
				else Schools.schClosed - 1
		end
	LEFT JOIN SchoolYearHistory
		ON SchoolYearHistory.schNo = Schools.schNo
		AND metaNumbers.num = SchoolYearHistory.syYear
	WHERE schoolYearHistory.syID is null

	-- we should clear history records outside the est/closed range
	DELETE FROM SchoolYearHistory
	FROM SchoolYearHistory
	INNER JOIN Schools
		ON SchoolYearHistory.schNo = Schools.schNo
	WHERE SchoolYearHistory.syYear < Schools.schEst
		OR
		( Schools.schClosed <> 0 AND SchoolYearHistory.syYear >= Schools.schClosed)


	-- now update anything from SchoolSurvey
	UPDATE SchoolYearHistory
		SET syAuth = isnull(syAuth,ssAuth)
		, systCode = isnull(systCode,ssSchType)
		FROM SchoolSurvey SS
		WHERE ss.schNo = SchoolYearHistory.schNo
		AND ss.svyYEar = SchoolYearHistory.syYear

	-- now update the active year forward from Schools
	UPDATE SchoolYearHistory
		SET syAuth = schAuth
		, systCode =schType
		, syParent = schParent
		, syDormant = schDormant
		FROM Schools
		WHERE Schools.schNo = SchoolYearHistory.schNo
		AND SchoolYearHistory.syYear = common.ActiveSurveyYear()

	-- now  the active year forward from Schools - for projections
	UPDATE SchoolYearHistory
		SET syAuth = isnull(syAuth,schAuth)
		, systCode =isnull(systCode,schType)
		FROM Schools
		WHERE Schools.schNo = SchoolYearHistory.schNo
		AND SchoolYearHistory.syYear > common.ActiveSurveyYear()

	-- if its a school closed before the active year, write its school info on to the last record before its closure

	UPDATE SchoolYearHistory
		SET syAuth = schAuth
		, systCode =schType
		, syParent = schParent
		, syDormant = schDormant
		FROM Schools
		WHERE Schools.schNo = SchoolYearHistory.schNo
		AND SchoolYearHistory.syYear = Schools.schClosed - 1


	-- now try to fill the blanks -
Declare @MoreToDo int

SELECT @MoreToDo = 1

while ( @MoreToDo = 1) begin
	UPDATE SchoolYearHistory
	SET syAuth = isnull(SchoolYearHistory.syAuth,SYH2.syAuth)
	, systCode = isnull(SchoolYearHistory.systCode,SYH2.systCode)

	FROM SchoolYearHistory
			INNER JOIN 	SchoolYearHistory SYH2
			ON SchoolYEarHistory.schNo = SYH2.schNo
			AND SchoolYearHistory.syYear = SYH2.syYear + 1
		WHERE
		 isnull(SchoolYearHistory.syAuth,'') <>  isnull(SYH2.syAuth,'')
		AND isnull(SchoolYearHistory.systCode,'') <>  isnull(SYH2.systCode,'')
		AND (SYH2.syAuth is not null or SYH2.systCode is not null)

	print @@Rowcount
	IF @@ROWCOUNT = 0
		SELECT @MoreToDo = 0
end

-- going backwards
;
WITH X AS
(
	Select schNo,
	min(syYear) syYear
	FROM SchoolYearHistory
	WHERE syAuth is not null
	GROUP BY schNo
)
UPDATE SchoolYearHistory
SET syAuth = SYH2.syAuth
FROM SchoolYearHistory
INNER JOIN SchoolYearHistory SYH2
	ON SchoolYearHistory.schNo = SYH2.schno
	AND SchoolYearHistory.syYear < SYH2.syYear
INNER JOIN X
	ON 	SYH2.schNo = X.schNo
	AND SYH2.syYEar = X.syYear

;
WITH X AS
(
	Select schNo,
	min(syYear) syYear
	FROM SchoolYearHistory
	WHERE systCode is not null
	GROUP BY schNo
)
UPDATE SchoolYearHistory
SET systCode = SYH2.systCode
FROM SchoolYearHistory
INNER JOIN SchoolYearHistory SYH2
	ON SchoolYearHistory.schNo = SYH2.schno
	AND SchoolYearHistory.syYear < SYH2.syYear
INNER JOIN X
	ON 	SYH2.schNo = X.schNo
	AND SYH2.syYEar = X.syYear


SELECT * from SchoolYearHistory
WHERE syAuth is null
OR systCode is null


SELECT DISTINCT SchoolYearHistory.SchNo
, schName
, schAuth
, schType
, schEst
, schClosed
from SchoolYearHistory
INNER JOIN Schools
	ON SchoolYearHistory.schNo = Schools.schNo
WHERE syAuth is null
OR systCode is null

END
GO

