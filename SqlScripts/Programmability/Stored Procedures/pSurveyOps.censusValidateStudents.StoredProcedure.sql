SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Procedure
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of student data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusValidateStudents]
@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @ndoe TABLE
(
RowIndex int
-- Start Generated Code
--	          , SchoolYear                   nvarchar(100) NULL
--              , [State]                      nvarchar(100) NULL
              , School_Name                nvarchar(100) NULL
              , School_No                  nvarchar(100) NULL
--              , School_Type                nvarchar(100) NULL
              , National_Student_ID        nvarchar(100) NULL
              , First_Name                 nvarchar(100) NULL
              , Middle_Name                nvarchar(100) NULL
              , Last_Name                  nvarchar(100) NULL
              , Full_Name                  nvarchar(100) NULL
              , Gender                     nvarchar(100) NULL
              , inDate_of_Birth            nvarchar(100) NULL
              , inDoB_Estimate             nvarchar(50) NULL
              , Age                        int NULL
              , Citizenship                nvarchar(100) NULL
              , Ethnicity                  nvarchar(100) NULL
              , Grade_Level                nvarchar(100) NULL
              , [From]                       nvarchar(100) NULL
              , Transferred_From_which_school   nvarchar(100) NULL
              , inTransfer_In_Date         nvarchar(100) NULL
              , SpEd_Student               nvarchar(100) NULL
			  , [IDEA_SA_ECE]				nvarchar(100) NULL
			  , [IDEA_SA_School_Age]		nvarchar(100) NULL
			  , Disability					nvarchar(100) NULL
			  , English_Learner				nvarchar(100) NULL
			  , Has_IEP						nvarchar(100) NULL
			  , Has_SBA_Accommodation		nvarchar(100) NULL
			  , Type_of_Accommodation		nvarchar(100) NULL
			  , Assessment_Type				nvarchar(100) NULL
			  , Exiting						nvarchar(100) NULL
              , Full_Name_2                nvarchar(100) NULL
              , Days_Absent               nvarchar(20)	null
              , Completed                 nvarchar(100) NULL
              , Outcome                    nvarchar(100) NULL
              , Dropout_Reason             nvarchar(100) NULL
              , Expulsion_Reason           nvarchar(100) NULL
              , Post_secondary_study       nvarchar(100) NULL
-- END generated Code
, fileReference		uniqueidentifier
, studentID uniqueidentifier		-- search in the Student_ table

-- put some normalised values in here
, clsLevel nvarchar(10)			-- Grade Level is the grade name, translate back to code
, yearofEd	int					-- the realted year of ed to the class - to get the right value for SpEdEnv
, tfrSchNo nvarchar(50)			-- school number transferred from Transferred_From_which_school is the school name

, spDisability	 nvarchar(20)	-- special ed - disability code
, spEnv			 nvarchar(20)	-- special ed environment
, spEnglish		nvarchar(10)	-- special ed english learner
, spAccommodation	nvarchar(10) -- special ed accommodation
, spAssessmentType  nvarchar(10)
, spExit			nvarchar(10)

, ssID		int


-- cleanups
, Date_of_Birth          date NULL
, DoB_Estimate			 int			-- convert this from No Yes to 1 or null
, Transfer_In_Date		 date NULL
)

--- validations table
declare @validations TABLE
(
	field nvarchar(50),
	errorValue nvarchar(100),
	valMsg nvarchar(200),
	severity nvarchar(20)
	, NumRows int
	, FirstRow int
	, LastRow int
)

--- warnings thable - these are mismatches on NationalID numbers
---- between current and exisitng data
--- they may be of two kinds :
--- the IDs match, but the data does not
--- the data matches (first name,last name,  DoB gender) but the IDs are different
declare @warnings TABLE
(
	rowID int,
	Id	nvarchar(50),
	givenName nvarchar(100),
	lastName  nvarchar(100),
	dob		  date,
	dobEstimate int,
	registerId	nvarchar(50),
	registerGivenName nvarchar(100),
	registerLastName  nvarchar(100),
	registerDob		  date,
	registerDobEstimate int
)


declare @SurveyYear int
declare @districtName nvarchar(50)
declare @firstRow int

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8
-- startpos = 8

Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
, @firstRow = v.value('@FirstRow', 'int')
From @xml.nodes('ListObject') as V(v)

if @DistrictName is null or @DistrictName = 'ALL'
	Select @DistrictID = 'ALL'
else
	Select @districtID = dID
	from Districts WHERE dName = @districtName

print @SurveyYear
print @DistrictID

--INSERT INTO NdoeStudents_
INSERT INTO @ndoe
Select
v.value('@Index', 'int') [ RowIndex]
-- Start Gendereted Code -- a macro in the workbook can generate this from column names
--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')											[State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                                   [School_Type]
, nullif(ltrim(v.value('@National_Student_ID', 'nvarchar(100)')),'')                              [National_Student_ID]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                                       [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                                      [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                                        [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                                        [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')											[Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')								[Date_of_Birth]
, nullif(ltrim(v.value('@DoB_Estimate', 'nvarchar(50)')),'')												[DoB_Estimate]
, nullif(ltrim(v.value('@Age', 'int')),'')														  [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                                      [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                                        [Ethnicity]
, nullif(ltrim(v.value('@Grade_Level', 'nvarchar(100)')),'')                                      [Grade_Level]
, nullif(ltrim(v.value('@From', 'nvarchar(100)')),'')                               [From]
, nullif(ltrim(v.value('@Transferred_From_which_school', 'nvarchar(100)')),'')                                  [Transferred_From_which_school]
, nullif(ltrim(v.value('@Transfer_In_Date', 'nvarchar(100)')),'')                                 [Transfer_In_Date]
, nullif(ltrim(v.value('@SpEd_Student', 'nvarchar(100)')),'')                                     [SpEd_Student]

, nullif(ltrim(v.value('@IDEA_SA_ECE', 'nvarchar(100)')),'')                                    [IDEA_SA_ECE]
, nullif(ltrim(v.value('@IDEA_SA_School_Age', 'nvarchar(100)')),'')                             [IDEA_SA_School_Age]
, nullif(ltrim(v.value('@Disability', 'nvarchar(100)')),'')										 [Disability]
, nullif(ltrim(v.value('@English_Learner', 'nvarchar(100)')),'')								  [English_Learner]

, nullif(ltrim(v.value('@Has_IEP', 'nvarchar(100)')),'')											[Has_IEP]
, nullif(ltrim(v.value('@Has_SBA_Accommodation', 'nvarchar(100)')),'')								[Has_SBA_Accommodation]
, nullif(ltrim(v.value('@Type_of_Accommodation', 'nvarchar(100)')),'')								[Type_Of_Accommodation]
, nullif(ltrim(v.value('@Assessment_Type', 'nvarchar(100)')),'')									[Assessment_Type]
, nullif(ltrim(v.value('@Exiting', 'nvarchar(100)')),'')											[Exiting]

, nullif(ltrim(v.value('@Full_Name_2', 'nvarchar(100)')),'')                                      [Full_Name_2]
, nullif(ltrim(v.value('@Days_Absent', 'nvarchar(200)')),'')                                      [Days_Absent]

, nullif(ltrim(v.value('@Completed', 'nvarchar(100)')),'')                                        [Completed]
, nullif(ltrim(v.value('@Outcome', 'nvarchar(100)')),'')                                          [Outcome]
, nullif(ltrim(v.value('@Dropout_Reason', 'nvarchar(100)')),'')                                   [Dropout_Reason]
, nullif(ltrim(v.value('@Expulsion_Reason', 'nvarchar(100)')),'')                                 [Expulsion_Reason]
, nullif(ltrim(v.value('@Post_secondary_study', 'nvarchar(100)')),'')                             [Post_secondary_study]
-- End Generated Code
, null	-- file reference not required?
, null	-- the student Id - set to null first, it will get filled by matches where possible, then any remaining unmatched get a NEWID()
-- placeholders for the normalised fields
, null			-- class level; from lkp levels based on class name
, null			-- year of ed
, null			-- transfer school; from schools based on name

, null			-- sp disability
, null			-- sp environment
, null			-- sp english learner
, null			-- sp accommodation
, null			-- sp assessment
, null			-- sp exit

, null			-- ssID from SchoolSurvey based on school no and year
-- cleanups
, null				-- Date_Of_Birth
, null				---Dob_Estimate
, null				-- Transfer_In_Date
from @xml.nodes('ListObject/row') as V(v)


-- poulate the normalised values
-- normalise school names for transfer schools
UPDATE @ndoe
SET tfrSchNo = S.schNo
FROM @ndoe
LEFT JOIN Schools S
	ON [@ndoe].Transferred_From_which_school in (S.schNo, S.schName)


-- date values need to be cleaned up depending  on whether they are presented as a string or number
UPDATE @ndoe
SET Date_Of_Birth =
	case ISNUMERIC(inDate_Of_Birth)
		when 0 then	case isdate(inDate_Of_Birth) when 1 then convert(date, inDate_Of_Birth) else null end
		when 1 then cast( convert(float, inDate_Of_Birth) - 2 as datetime)
	end
, DoB_Estimate =
	case inDoB_Estimate
		when 'Y' then 1
		when 'Yes' then 1
		when '1' then 1
		when 'unknown' then 1			-- allow for some confirmation we really don;t know the DoB
		else null end
, Transfer_In_Date =
	case ISNUMERIC(inTransfer_In_Date)
		when 0 then	case  isdate(inTransfer_In_Date) when 1 then convert(date, inTransfer_In_Date) else null end
		when 1 then cast( convert(float, inTransfer_In_Date) - 2 as datetime)
	end

-- normalise grade levels
UPDATE @ndoe
SET clsLevel = codeCode
, yearofEd = lvlYear
FROM @ndoe
LEFT JOIN lkpLEvels L
	ON [@ndoe].Grade_Level = L.codeDescription

print 'before gender'

-- normalise Gender
UPDATE @ndoe
SET Gender = case when codeCode is null then  Gender else codeCode end		-- avoid isnull becuase it will case as nvarchar(1)
FROM @ndoe
LEFT JOIN lkpGender G
	ON [@ndoe].Gender = G.codeDescription


-- normalise ethnicity
UPDATE @ndoe
SET ethnicity = codeCode
FROM @ndoe
LEFT JOIN lkpEthnicity E
	ON [@ndoe].Ethnicity = E.codeDescription

--- nrmalisations for special ed codes
update @ndoe
SET spDisability = codeCode
FROM @ndoe
	INNER JOIN lkpDisabilities D
		ON [@ndoe].Disability = D.codeDescription

-- special ed environment -- chose between ECE and school age fields, based on the pupil's grade
update @ndoe
SET spEnv = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdEnvironment ENV
		ON
	case when yearofed <= 0 then coalesce(IDEA_SA_ECE, IDEA_SA_School_Age)  else coalesce(IDEA_SA_School_Age, IDEA_SA_ECE) end
	 = ENV.codeDescription

-- special ed english learner
update @ndoe
SET spEnglish = codeCode
FROM @ndoe
	INNER JOIN lkpEnglishLearner E
		ON
		[@ndoe].English_Learner	 = E.codeDescription
-- special ed accommodation
update @ndoe
SET spAccommodation = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdAccommodations E
		ON
		[@ndoe].Type_of_Accommodation = E.codeDescription

		-- special ed assessment type
update @ndoe
SET spAssessmentType = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdAssessmentTypes E
		ON
		[@ndoe].Assessment_Type= E.codeDescription

-- special ed exit
update @ndoe
SET spExit = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdExits E
		ON
		[@ndoe].Assessment_Type= E.codeDescription

-------------------------------------------------------------------------------------------
-- Validations on the input data - the user can abort and correct it in the source document

declare @missingSchool int
declare @missingGrade int
declare @missingDoB int
declare @missingNatID int

declare @UnknownSchool int
declare @badStateSchool int

declare @unknownGrade int
declare @missingName int
declare @missingTransferSchool int


-- Validation 1) school must be supplied on every record
print 'start of validations'

SELECT @missingSchool = count(*)
FROM @ndoe
WHERE School_No is null

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'School'
, null
, 'No school'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE School_Name is null
HAVING count(*) > 0

-- it must be a valid school code
SELECT @unknownSchool = count(DISTINCT School_No)
from @ndoe
WHERE School_No not in (Select schNo from Schools)


INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'School'
, School_No
, 'unknown school no'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE School_No not in (Select schNo from Schools)
and School_No is not null
Group BY School_No
HAVING count(*) > 0


-- it must be in the nominated state
SELECT @badStateSchool = count(DISTINCT School_No)
from @ndoe
WHERE School_No not in
(Select schNo from Schools
 INNER JOIN Islands I
 ON Schools.iCode = I.iCode
 	WHERE ( I.iGroup <> @districtID and @districtID <> 'ALL')
	)

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'School'
, School_No
, 'School not in ' + @DistrictName
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE
School_No not in
	(Select schNo from Schools
	 INNER JOIN Islands I
	 ON Schools.iCode = I.iCode
 		WHERE I.iGroup = @districtID
		)
-- allow for a full national upload by setting the workbook district to 'ALL'
and @districtID <> 'ALL'
and School_No in (Select schNo from Schools)
GROUP BY School_No
HAVING count(*) > 0

-------

-- Validation 2) grade must be supplied on every record

SELECT @missingGrade = count(*)
FROM @ndoe
WHERE Grade_Level is null


INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Grade'
, null
, 'no grade level'
, count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoe
WHERE Grade_Level is null
HAVING count(*) > 0


-- it must be a defined  grade level

Select @unknownGrade = COUNT(DISTINCT Grade_Level)
FROM @ndoe
WHERE Grade_Level not in (Select codeDescription from lkpLevels)

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Grade'
, Grade_Level
, 'unknown grade level'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE  Grade_Level not in (Select codeDescription from lkpLevels)
GROUP BY Grade_Level
HAVING count(*) > 0

-- Validation 3 date of birth must be on every record (even if there is a estimate)
print 'check DoB'

SELECT @missingDoB = count(*)
FROM @ndoe
WHERE Date_of_Birth is null and
( DoB_Estimate is null)							-- in other words, we can allow a null DoB if it is marked as an estimate

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Date of birth'
, null
, 'no date of birth'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Date_of_Birth is null and
( DoB_Estimate is null)
HAVING count(*) > 0

-- now any specific date of birth values that are not null, but not valid dates
-- check for malformed dates
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Date of birth'
, inDate_Of_Birth
, 'invalid date of birth'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inDate_Of_Birth is not null
AND isnumeric(inDate_Of_Birth) = 0
AND isdate(inDate_Of_Birth) = 0

-- check for time part
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Date of birth'
, cast(convert(float, inDate_Of_Birth) - 2 as datetime)
, 'date of birth must not include time'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inDate_Of_Birth is not null
AND isnumeric(inDate_Of_Birth) = 1
AND (inDate_Of_Birth - floor(inDate_Of_Birth)) <> 0


INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Transfer-In Date'
, inTransfer_In_Date
, 'invalid transfer date'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inTransfer_In_Date is not null
AND isnumeric(inTransfer_In_Date) = 0
AND isdate(inTransfer_In_Date) = 0

-- check for time part
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Transfer-In Date'
 , cast(convert(float, inTransfer_In_Date) - 2 as datetime)
, 'transfer date must not include time'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE inTransfer_In_Date is not null
AND isnumeric(inTransfer_In_Date) = 1
AND (inTransfer_In_Date - floor(inTransfer_In_Date)) <> 0


-- Validation 4 missing national student ID
print 'check national id'

SELECT @missingNatID = count(*)
FROM @ndoe
WHERE National_Student_ID is null

-- Validation 5 some part of name
print 'check name'
SELECT @missingName = count(*)
FROM @ndoe
WHERE First_Name is null or Last_Name is null

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Name'
, NULL
, 'incomplete name'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE First_Name is null or Last_Name is null
HAVING count(*) > 0

print 'check gender'
-- Validation 6 -- gender must be supplied and valid
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Gender'
, null
, 'missing gender'
, count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoe
WHERE Gender is null
HAVING count(*) > 0

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Gender'
, Gender
, 'unknown gender value'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Gender is not null and Gender not in (select codeCode from lkpGender)
GROUP BY Gender
HAVING count(*) > 0

print 'special ed fields and special ed indicator'
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed'
, null
, 'special ed fields set, but not flagged as a special ed student'
, 1
, (rowIndex)
, (rowIndex)
FROM @ndoe
WHERE coalesce(Disability,
	IDEA_SA_ECE,
	IDEA_SA_School_Age,
	English_Learner,
	Has_IEP,
	Type_of_Accommodation,
	Assessment_Type,
	Exiting
	) is not null
	and SpED_Student is null

print 'lookup validation'
-- disability
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed Disability'
, Disability
, 'unknown Disability value'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Disability is not null
	and Disability not in (select codeDescription from lkpDisabilities)
GROUP BY Disability
HAVING count(*) > 0

-- environment
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed Environment'
, IDEA_SA_School_Age
, 'select an ECE environment for special ed'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE yearofed <= 0
	and IDEA_SA_ECE is null
	and IDEA_SA_School_Age is not null
GROUP BY IDEA_SA_School_Age
HAVING count(*) > 0

-- environment
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed Environment'
, IDEA_SA_ECE
, 'select a SCHOOL AGE environment for special ed'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE yearofed > 0
	and IDEA_SA_ECE is not null
	and IDEA_SA_School_Age is null
GROUP BY IDEA_SA_ECE
HAVING count(*) > 0

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed Environment'
, coalesce(IDEA_SA_ECE, IDEA_SA_School_Age)
, 'unknown special ed environment'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE coalesce(IDEA_SA_ECE, IDEA_SA_School_Age) is not null
	and coalesce(IDEA_SA_ECE, IDEA_SA_School_Age) not in (Select codeDescription from lkpSpEdEnvironment)
GROUP BY coalesce(IDEA_SA_ECE, IDEA_SA_School_Age)
HAVING count(*) > 0


-- english learner
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed English Learner'
, English_Learner
, 'unknown English Learner value'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE English_Learner is not null
	and English_Learner not in (select codeDescription from lkpEnglishLearner)
GROUP BY English_Learner
HAVING count(*) > 0


-- accommodations
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed Accommodation'
, Type_of_Accommodation
, 'unknown Sp Ed Accommodation'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Type_of_Accommodation is not null
	and Type_of_Accommodation not in (select codeDescription from lkpSpEdAccommodations)
GROUP BY Type_of_Accommodation
HAVING count(*) > 0

-- assessment type
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed Assessment Type'
, Assessment_Type
, 'unknown Sp Ed Assessment Type'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Assessment_Type is not null
	and Assessment_Type not in (select codeDescription from lkpSpEdAssessmentTypes)
GROUP BY Assessment_Type
HAVING count(*) > 0

-- exits
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Sp Ed Exit'
, Exiting
, 'unknown Sp Ed Exit'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE Exiting is not null
	and Exiting not in (select codeDescription from lkpSpEdExits)
GROUP BY Exiting
HAVING count(*) > 0

print 'check missing ID'

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Student ID'
, null
, 'no Student ID number'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE National_Student_ID is null
HAVING count(*) > 0

-- Validation 6 Student Id must be unique in the input file
print 'check duplicate ID'
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Student ID'
, National_Student_ID
, 'duplicate Student ID'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE National_Student_ID is not null
GROUP BY National_Student_ID
having count(*) > 1

-- Validation 7 transfer school must be valid if it is supplied, but if its not there, we can;t do much
print 'check transfer school'

SELECT @missingTransferSchool = count(DISTINCT Transferred_From_which_school)
FROM @ndoe
WHERE [From] = 'Transfer In' and Transferred_From_which_school is not null
and tfrSchNo is null

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
' Transfer-From School'
, Transferred_From_which_school
, 'Transfer origin school not known'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE [From] = 'Transfer In' and Transferred_From_which_school is not null
and tfrSchNo is null
GROUP BY Transferred_From_which_school


-- check lengths of a few fields, note this is temporary, these should not be freely editable
INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Dropout Reason'
, Dropout_reason
, 'dropout reason > 50 chars: ' + convert(nvarchar(2), len(dropout_reason))
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE len(Dropout_reason) > 50
GROUP BY Dropout_reason

INSERT INTO @Validations
(
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Explusion Reason'
, Expulsion_reason
, 'expulsion reason > 50 chars: ' + convert(nvarchar(2), len(expulsion_reason))
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoe
WHERE len(expulsion_reason) > 50
GROUP BY expulsion_reason

--- Return the collection of errors
declare @NumValidationErrors int
print 'Report Validation errors'
select
	field
	, errorValue
	, valMsg
	, NumRows
	, FirstRow + @firstRow FirstRow
	, LastRow + @FirstRow LastRow
from @Validations V
ORDER BY V.FirstRow, V.valMsg


-- Return any warnings about ID numbers

-- Days Absent
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Days Absent'
, Days_Absent
, 'Days Absent not recorded'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)
From @ndoe ndoe
WHERE Days_Absent is null
GROUP BY Days_Absent


INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Days Absent'
, Days_Absent
, 'Days Absent must be numeric, 0, 1 or 2 decimal places'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE Days_Absent is not null and isnumeric(Days_Absent) = 1
AND convert(decimal(13,5) , convert(decimal(8,2), Days_Absent)) <>   convert(decimal(13,5), Days_Absent)

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Days Absent'
, Days_Absent
, 'Days Absent must be numeric, 0, 1 or 2 decimal places'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoe ndoe
WHERE Days_Absent is not null and isnumeric(Days_Absent) = 0

-- look

Select * from @Validations
Select @NumValidationErrors = @@ROWCOUNT
print @NumValidationErrors
print 'collect warnings'
-------------------------------------------------------
INSERT INTO @warnings
(
	rowID
	, Id
	, givenName
	, lastName
	, dob
	, dobEstimate
	, registerId
	, registerGivenName
	, registerLastName
	, registerDob
	, registerDobEstimate
)
SELECT NDOE.rowIndex
, stuCardID
, stuGiven
, stuFamilyName
, stuDoB
, stuDoBEst
, NDOE.National_Student_ID
, NDOE.First_Name
, NDOE.Last_Name
, NDOE.Date_of_Birth
, NDOE.DoB_Estimate

FROM Student_ STU
INNER JOIN @ndoe NDOE
ON NDOE.National_Student_ID = STU.stuCardID
WHERE (
	NDOE.Last_Name <> STU.stuFamilyName
	OR NDOE.Gender <> STU.stuGender
	OR
		( NDOE.Date_of_Birth <> STU.stuDoB
			AND isnull(NDOE.DoB_Estimate,0) = 0
			AND STU.stuDoBEst = 0
		)
)

print 'report warnings'

Select *
from @warnings

----- IF THERE ARE VALIDATION ERRORS LOGGED RETURN HERE
----- the data will not be processed

END
GO

