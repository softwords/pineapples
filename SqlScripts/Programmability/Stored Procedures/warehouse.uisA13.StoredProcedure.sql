SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 7 2021
-- Description:	Sheet A13 of UIS Survey
-- In this calculation we count every school teaching at each Isced Level
-- schools that teach across muliple levels will contribute to more than one column,
-- hence the total number of schools shown may well exceed the actual number of schools.
-- Data originates on the survey School and Wash sheets.
-- Most is read directly from there - ie frpm the ssData and ssWashData images of the census table rows.
-- Some data points may be taken from Resources - they are put on that table by the census load. This has
-- the advantage of backward compatibility.
-- =============================================
CREATE PROCEDURE [warehouse].[uisA13]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- columns
declare @IPED nvarchar(200) = 'Internet_in_teaching';
declare @INTERNET nvarchar(200) = 'Internet'

declare @HIVED nvarchar(200) = 'HIV_Education';
declare @SEXED nvarchar(200) = 'Sexuality_Education';

declare @HANDWASHING nvarchar(200) = 'Available';
declare @SOAPWATER nvarchar(200) = 'Soap_And_Water';

declare @SPED nvarchar(200)	= 'SpEd_Infrastructure'

declare @GIRLSTOILETS nvarchar(200) = 'Girls_Toilets_Total';
declare @BOYSTOILETS nvarchar(200) = 'Boys_Toilets_Total';
declare @GIRLSTOILETSUSABLE nvarchar(200) = 'Girls_Toilets_Usable';
declare @BOYSTOILETSUSABLE nvarchar(200) = 'Boys_Toilets_Usable';
declare @TOILETTYPE nvarchar(200) = 'Toilet_Type';

declare @WATERSOURCE nvarchar(200) = 'Main_Source_Drinking_Water';
declare @WATERAVAILABLE nvarchar(200) = 'Currently_Available';

declare @t TABLE (
item nvarchar(50)
, schNo nvarchar(50)
, num integer
, flag nvarchar(1)
)
DECLARE @items TABLE
(
	outputitem nvarchar(40)
)

DECLARE @c TABLE
(
	ssID int
	, schNo nvarchar(50)
	, columnName nvarchar(200)
	, value nvarchar(200)
	, yesNo int
)
-- table of available column names in the year - we can use this to handle variations in the input data year to year

DECLARE @cn TABLE
(
	columnName nvarchar(200)
)


DECLARE  @Gov Table
(
Gov nvarchar(1),
GovGroup nvarchar(10)
)

INSERT INTO @Gov
VALUES ('G','Public'),
('N','Private')

-- school categorisation
-- a school will appear multiple times if it teaches in multiple isced levels
DECLARE @s TABLE
(
	schNo nvarchar(50)
	, Isced nvarchar(20)
	, AuthorityGovtCode nvarchar(1)
)

INSERT INTO @s
(schNo, Isced, AuthorityGovtCode)
Select DISTINCT schNo
, [Isced Level] Isced
, DSS.AuthorityGovtCode
from warehouse.Enrol E
	INNER JOIN DimensionLevel DL
	on E.ClassLevel = DL.levelCode
	INNER JOiN warehouse.DimensionSchoolSurvey DSS
	ON E.surveyDimensionID = DSS.[Survey ID]
WHERE E.Enrol > 0
AND E.surveyYear = @year

-- populate all the key value pairs for the year
INSERT INTO @c
( ssID, schNo, columnName, value, yesNo)
Select ssID, BS.schNo, columnName, value
, case value when 'Yes' then 1 when 'No' then 0 end yesNo
From warehouse.BestSurvey BS	-- will make provision to get an earlier survey if current not around
	INNER JOIN pSurveyRead.SurveySheetData D
		ON BS.surveyDimensionID = D.ssID
WHERE SurveyYear = @year

-- get the list of names that are supported
INSERT INTO @cn
Select DISTINCT columnName from @c

-- these are the supported outputs
-- these correspond to range names on the A13 sheet
INSERT INTO @items
VALUES ('Electricity')
, ('Computers')
, ('Internet')
, ('Toilets')
, ('SingleSexToilets')
, ('UsableToilets')
, ('WaterSource')
, ('WaterAvailable')
, ('Handwashing')
, ('SpecialEd')
, ('Lifeskills')

--Select * from @c
--Select * from @cn


-- Electricity
-- this comes from Resources


INSERT INTO @t
( item, schNo, num, flag)
Select
'Electricity'
, schNo
, 1
, null
from warehouse.BestSurvey BS
INNER JOIN Resources R
	ON BS.surveyYear = @year
	AND BS.SurveyDimensionID = R.ssID
	AND resName = 'Power Supply'
	AND resAvail = -1 and resSplit <> 'None'

-- Computers for pedagocial purposes
-- from census workbook Schools page column 'Computers In Teaching'
-- => Resources <ICT> Split = Computer Lab (deprecated)
-- OR Resources <ICT> Split = Computers in Teaching


INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'Computers'
, schNo
, 1
, null
from warehouse.BestSurvey BS
INNER JOIN Resources R
	ON BS.surveyYear = @year
	AND BS.SurveyDimensionID = R.ssID
	AND resName = 'ICT'
	AND resAvail = -1 and resSplit in ('Computer Lab','Computers in Teaching')


-- internet in teaching
-- captured in column 'Internet in Teaching'

INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'Internet'
, schNo
, 1
, null
FROM @c
WHERE
	(
	columnName = @IPED
	OR
	(
		columnName = @INTERNET and @IPED not in (select columnName from @cn)
	)
	)
	AND yesNo = 1

print @@rowcount

--- LifeSkills
--- requires yes to both HIV_Education and Sexuality edccuation

INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'Lifeskills'
, C1.schNo
, 1
, null
FROM @c C1
	INNER JOIN @c C2
		ON C1.schNo = C2.schNo
		AND C1.columnName = @HIVED
		AND C2.columnName = @SEXED
		AND C1.yesNo = 1 and C2.yesNo = 1


-- Handwashing facilities
-- Column 'Available' comes from Wash sheet and represents Handwashing. Soap And Water should not be None.

INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'Handwashing'
, C1.schNo
, 1
, null
FROM @c C1
	INNER JOIN @c C2
		ON C1.schNo = C2.schNo
		AND C1.columnName = @HANDWASHING
		AND C2.columnName = @SOAPWATER
		AND C1.yesNo = 1 and
		C2.value not in ('Neither soap nor water')

-- Special Ed Infrastructure
-- SpEd Insfrasture (Yes/no) from School sheet
INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'SpecialEd'
, C1.schNo
, 1
, null
FROM @c C1
WHERE C1.columnName = 'SpED_Infrastructure' and C1.yesNo = 1


-- improved toilets


INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'Toilets'
, C1.schNo
, 1
, null
FROM @c C1
	WHERE columnName = @TOILETTYPE
	AND value <> 'No toilets or latrines'

-- Single Sex Toilets
-- Must have Girls' Toilets > 0 Boys' Toilets > 0, AND Toilet Type <> 'No toilets or latrines'

INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'SingleSexToilets'
, C1.schNo
, 1
, null
FROM @c C1
	INNER JOIN @c C2
		ON C1.schNo = C2.schNo
		AND C1.columnName = @GIRLSTOILETS
		AND C2.columnName = @BOYSTOILETS
		AND C1.value > 0 and C2.value > 0
	INNER JOIN @C C3
		ON C1.schNo = C3.schNo
		AND C3.columnName = @TOILETTYPE
		AND C3.value <> 'No toilets or latrines'

-- Single Sex Toilets
-- Must have Girls' Toilets > 0 Boys' Toilets > 0, AND Toilet Type <> 'No toilets or latrines'

INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'UsableToilets'
, C1.schNo
, 1
, null
FROM @c C1
	INNER JOIN @c C2
		ON C1.schNo = C2.schNo
		AND C1.columnName = @GIRLSTOILETSUSABLE
		AND C2.columnName = @BOYSTOILETSUSABLE
		AND C1.value > 0 and C2.value > 0
	INNER JOIN @C C3
		ON C1.schNo = C3.schNo
		AND C3.columnName = @TOILETTYPE
		AND C3.value <> 'No toilets or latrines'


-- water - improved drinking water source

INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'WaterSource'
, C1.schNo
, 1
, null
FROM @c C1
	WHERE C1.columnName = @WATERSOURCE
	AND C1.value not in ('No water source')


INSERT INTO @t
( item, schNo, num, flag)
Select DISTINCT
'WaterAvailable'
, C1.schNo
, 1
, null
FROM @c C1
	INNER JOIN @c C2
		ON C1.schNo = C2.schNo
		AND C1.columnName = @WATERSOURCE
		AND C1.value not in ('No water source')

		AND C2.columnName = @WATERAVAILABLE
		AND C2.yesNo = 1

--Select * from @t

-- put it all together and aggregate

Declare @output TABLE
(
	Isced nvarchar(10)
	, GovGroup nvarchar(10)
	, item nvarchar(50)
	, num int
	, flag nvarchar(1)
)
-- populate 0s
INSERT INTO @output
Select DISTINCT ilCode
, GovGroup
, outputitem
, 0
, null
from ISCEDLevel
CROSS JOIN @Gov G
CROSS JOIN @items

INSERT INTO @output
Select Isced
, GovGroup
, item
, sum(num) Num
, min(flag) Flag
from @T T
INNER JOIN @s S
	ON T.schNo = S.schNo
INNER JOIN @Gov G
	ON S.AuthorityGovtCode = G.Gov
GROUP BY GovGroup, Isced, item
ORDER BY GovGroup, Isced, item

Select @year SurveyYear
, Isced
, GovGroup
, item
, sum(num) Num
, min(flag) Flag
from @output
GROUP BY Isced, GovGroup, item
ORDER BY Isced, GovGroup, item
END
GO
GRANT EXECUTE ON [warehouse].[uisA13] TO [dbreadonly] AS [dbo]
GO

