SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 11 2008
-- Description:	SQL server side implemenation of teacher reconcile
-- =============================================
CREATE PROCEDURE [pTeacherOps].[TeacherReconcile]
	-- Add the parameters for the stored procedure here

	@strSource nvarchar(20) = ''

AS
BEGIN

	create table #TeacherMatch
(
	tID [int] NOT NULL,
	MatchType nchar(1),
	Matches [int]
)

	SET NOCOUNT ON
-- prepare the external data first
-- pTeacherOps has premissions to TeacherIdentityExternal
update TeacherIdentityExternal
set
	xtNamePrefix = N.Honorific
	, xtNameFirst = N.FirstName
	, xtNameMiddle = N.MiddleName
	, xtNameLast = N.LastName
	, xtNameSuffix = N.Suffix
	, xtMatchId = null
	, xtMatchType = null
from TeacherIdentityExternal
	CROSS APPLY fnParseName(xtGiven + '.'+ xtSurname) N

-- prepare the pineapples data - we don;t want to do the cross apply every time...?

Select tID
	, FirstName
	, MiddleName
	, LastName
	, tPayroll
	, tSex
	, tDOB
	, tGiven
	, tSurname
INTO #ti
from
	TeacherIdentity T CROSS APPLY fnParseName(tGiven + '.' + tSurname) N;

declare @matchType nchar(1)

	-- A class match
	select @matchType = 'A';

	with X (xtID, xtNameLast, xtNameFirst, xtPayroll, xtDOB, xtSex, xtMatchID, xtMatchType)
	as
	(Select xtID, xtNameLast, xtNameFirst, xtPayroll , xtDOB, xtSex,  xtMatchID, xtMatchType
		from TeacherIdentityExternal
		WHERE xtSrc = @strSource

	)
	UPDATE X
		set xtMatchID = tID
			, xtMatchType = @matchType
	from

      #ti T, X

		WHERE tID not in (Select xtMatchID from X WHERE xtMatchID is not null) AND
			xtMatchID is null AND
			LastName = xtNameLast AND
			FirstName = xtNameFirst AND
			--tSurname = xtSurname AND
			--tGiven = xtGiven AND
			tDOB = xtDOB AND
			tSex = xtSex AND
			tPayroll = xtPayroll

		-- B match, not payroll
	select @matchType = 'B';

	with X (xtID, xtNameLast, xtNameFirst, xtPayroll, xtDOB, xtSex, xtMatchID, xtMatchType)
	as
	(Select xtID, xtNameLast, xtNameFirst, xtPayroll , xtDOB, xtSex,  xtMatchID, xtMatchType
		from TeacherIdentityExternal
		WHERE xtSrc = @strSource

	)
	UPDATE X
		set xtMatchID = tID
			, xtMatchType = @matchType
	from

      #ti T, X

		WHERE tID not in (Select xtMatchID from X WHERE xtMatchID is not null) AND
			xtMatchID is null AND

			LastName = xtNameLast AND
			FirstName = xtNameFirst AND
			tDOB = xtDOB AND
			tSex = xtSex


		-- C match, payroll, name, gender
		select @matchType = 'C';

	with X (xtID, xtNameLast, xtNameFirst, xtPayroll, xtDOB, xtSex, xtMatchID, xtMatchType)
	as
	(Select xtID, xtNameLast, xtNameFirst, xtPayroll , xtDOB, xtSex,  xtMatchID, xtMatchType
		from TeacherIdentityExternal
		WHERE xtSrc = @strSource

	)
	UPDATE X
		set xtMatchID = tID
			, xtMatchType = @matchType
	from

      #ti T, X

		WHERE tID not in (Select xtMatchID from X WHERE xtMatchID is not null) AND
			xtMatchID is null AND

			LastName = xtNameLast AND
			FirstName = xtNameFirst AND
			tSex = xtSex AND
			tPayroll = xtPayroll


		-- D match,  name, gender
		select @matchType = 'D';

	with X (xtID, xtNameLast, xtNameFirst, xtPayroll, xtDOB, xtSex, xtMatchID, xtMatchType)
	as
	(Select xtID, xtNameLast, xtNameFirst, xtPayroll , xtDOB, xtSex,  xtMatchID, xtMatchType
		from TeacherIdentityExternal
		WHERE xtSrc = @strSource

	)
	UPDATE X
		set xtMatchID = tID
			, xtMatchType = @matchType
	from

      #ti T, X

		WHERE tID not in (Select xtMatchID from X WHERE xtMatchID is not null) AND
			xtMatchID is null AND

			LastName = xtNameLast AND
			FirstName = xtNameFirst AND
			tSex = xtSex

		-- E match, payroll, name,
		select @matchType = 'E';


	with X (xtID, xtNameLast, xtNameFirst, xtPayroll, xtDOB, xtSex, xtMatchID, xtMatchType)
	as
	(Select xtID, xtNameLast, xtNameFirst, xtPayroll , xtDOB, xtSex,  xtMatchID, xtMatchType
		from TeacherIdentityExternal
		WHERE xtSrc = @strSource

	)
	UPDATE X
		set xtMatchID = tID
			, xtMatchType = @matchType
	from

      #ti T, X

		WHERE tID not in (Select xtMatchID from X WHERE xtMatchID is not null) AND
			xtMatchID is null AND

			LastName = xtNameLast AND
			FirstName = xtNameFirst AND
			tPayroll = xtPayroll


		-- F match - name only
		select @matchType = 'F';


	with X (xtID, xtNameLast, xtNameFirst, xtPayroll, xtDOB, xtSex, xtMatchID, xtMatchType)
	as
	(Select xtID, xtNameLast, xtNameFirst, xtPayroll , xtDOB, xtSex,  xtMatchID, xtMatchType
		from TeacherIdentityExternal
		WHERE xtSrc = @strSource

	)
	UPDATE X
		set xtMatchID = tID
			, xtMatchType = @matchType
	from

      #ti T, X

		WHERE tID not in (Select xtMatchID from X WHERE xtMatchID is not null) AND
			xtMatchID is null AND


			LastName = xtNameLast AND
			FirstName = xtNameFirst


		-- G match, payroll only
		select @matchType = 'G';


	with X (xtID, xtNameLast, xtNameFirst, xtPayroll, xtDOB, xtSex, xtMatchID, xtMatchType)
	as
	(Select xtID, xtNameLast, xtNameFirst, xtPayroll , xtDOB, xtSex,  xtMatchID, xtMatchType
		from TeacherIdentityExternal
		WHERE xtSrc = @strSource

	)
	UPDATE X
		set xtMatchID = tID
			, xtMatchType = @matchType
	from

      #ti T, X

		WHERE tID not in (Select xtMatchID from X WHERE xtMatchID is not null) AND
			xtMatchID is null AND

			tPayroll = xtPayroll


Select xtMatchID tID
		,xtMatchType MatchType
		,count(xtID) Matches
from
	TeacherIdentityExternal
WHERE xtSrc = @strSource

group by xtMatchID, xtMatchType

drop table #ti

END
GO

