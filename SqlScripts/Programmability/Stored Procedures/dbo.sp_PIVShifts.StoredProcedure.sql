SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13/11/2007
-- Description:	Data for schools pivot table
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVShifts]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- collect the estimate temp table
Select * into
#ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments() E
WHERE (E.schNo = @SchNo or @SchNo is null)
	and (E.LifeYear = @SurveyYear or @SurveyYear is null)


-- now the output
SELECT
	EE.LifeYear [Survey Year],
	EE.Estimate,
    EE.Offset AS [Age of Data],
	EE.bestYear AS [Year of Data],
    EE.bestssqLevel AS [Data Quality Level],
	EE.ActualssqLevel AS [Survey Year Quality Level],
	EE.SurveyDimensionssID,
	SS.ssEnrol as Enrol,

	SS.ssAcaYearStartM AcaYearStartM,
	SS.ssAcaYearStartY AcaYearStartY,
	SS.ssAcaYearEndM AcaYearEndM,
	SS.ssAcaYearEndY AcaYearEndY,


	isnull(SS.ssShifts,1) NumShifts,
	-- duplicated becuase we may want to group on this
	isnull(SS.ssShifts,1) ShiftsAtSchool,
	ssClassesShift1 ClassesShift1,
	ssClassesShift2 ClassesShift2,
	ssClassesShiftAll ClassesAllShifts,

	SS.ssEdSystem	[EducationSystem],
	SS.ssMediumOfInstruction [MediumOfInstruction],


-- from pEnrolmentRead.ssIDEnrolmentLevelsummary
	ELS.NumLevels,
	ELS.RangeYearOfEd,
	ELS.HighestLevel,
	ELS.LowestLevel,
	ELS.HighestYearOfEd,
	ELS.LowestYearOfEd,
	ELS.MaxLevelEnrol,
	ELS.LevelOfMaxEnrol,
	ELS.FullRangeYN,
	ELS.FullRange

INTO #tmp
FROM
	#ebse EE INNER JOIN SchoolSurvey SS
		on EE.bestssID = SS.ssID
	INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelSummary ELS
		on EE.bestssID = ELS.ssID

if (@DimensionColumns is null) begin
	SELECT T.*
	FROM #tmp T
end


if (@DimensionColumns= 'ALL') begin
	SELECT T.*
	, DSS.*
	FROM #tmp T
	INNER JOIN DimensionSchoolSurvey DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end

if (@DimensionColumns= 'CORE') begin

	SELECT T.*
	, DSS.*
	FROM #tmp T
	INNER JOIN DimensionSchoolSurveyCore DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'CORESUMM') begin
	SELECT
	sum(NumShifts) NumShifts,
	sum(ClassesShift1) ClassesShift1,
	sum(ClassesShift2) ClassesShift2,
	sum(ClassesAllShifts) ClassesAllShifts,
	sum(NumSchools) NumSchools,
	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool
	FROM #tmp T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	GROUP BY
	[Survey Year]
	, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool

end

-- variant on core including electorates, region and popgis ids, lat/long/elev
--
if (@DimensionColumns= 'GEO') begin

	SELECT T.*
	, DSS.*
	FROM #tmp T
	INNER JOIN DimensionSchoolSurveyGeo DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'RANK') begin
-- rebuild the rank table
	exec buildRank


	SELECT T.*
	, DSS.*

      -- from DimensionRank
      ,	DRANK.[School Enrol],
		DRANK.[District Rank],
		DRANK.[District Decile],
		DRANK.[District Quartile],
		DRANK.[Rank],
		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmp T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear


end


if (@DimensionColumns= 'RANKSUMM') begin
-- rebuild the rank table
	exec buildRank


	SELECT
	sum(NumShifts) NumShifts,
	sum(ClassesShift1) ClassesShift1,
	sum(ClassesShift2) ClassesShift2,
	sum(ClassesAllShifts) ClassesAllShifts,
	sum(NumSchools) NumSchools,

	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool
      -- from DimensionRank
		, sum(DRANK.[School Enrol]) [School Enrol]
		, DRANK.[District Decile],
		DRANK.[District Quartile],

		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmp T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear

	GROUP BY
	[Survey Year]
		, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool
-- from DimensionRank

	, DRANK.[District Decile],
	DRANK.[District Quartile],
	--DRANK.[Rank],			don;t use rank when grouping
	DRANK.[Decile],
	DRANK.[Quartile]

end

drop table #tmp

END
GO

