SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 26 04 2021
-- Description:	Filter of student links
-- =============================================
CREATE PROCEDURE [pStudentRead].[StudentLinkFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@LinkID				int = null,		-- the unique idenitifer of the
	@StudentID			uniqueidentifier = null,
	@StudentName		nvarchar(100) = null,		-- matched with like if
	@DocumentID			uniqueidentifier = null,
	@Keyword			nvarchar(50) = null,
	@Function			nvarchar(50) = null,
	@DateStart			datetime = null,
	@DateEnd			datetime = null,
	@DocumentSource		nvarchar(100) = null,
	@DocType			nvarchar(10) = null,
	@IsImage			int = null
AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pStudentRead.StudentLinkFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

		, @LinkID
		, @StudentID
		, @StudentName
		, @DocumentID
		, @Keyword
		, @Function
		, @DateStart
		, @DateEnd
		, @DocumentSource
		, @DocType
		, @IsImage


--- column sets ----
	Select lnkID
	, SL.stuID
	, SL.docID
	, lnkFunction
	, lnkHidden
	, docTitle
	, docDescription
	, docSource
	, docDate
	, docRotate
	, docTags
	, docType
	, SL.pCreateUser
	, SL.pCreateDateTime
	, SL.pEditUser
	, SL.pEditDateTime
	, SL.pRowversion
	, S.stuGiven
	, S.stuFamilyName
	, stuDoB
	, stuGender

	FROM pStudentRead.StudentLinks SL
		INNER JOIN @keys K
			ON SL.lnkID = K.ID
		LEFT JOIN Student_ S
			ON SL.stuID = S.stuID
	ORDER BY recNo


--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

