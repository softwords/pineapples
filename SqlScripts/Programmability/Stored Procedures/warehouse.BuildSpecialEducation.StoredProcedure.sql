SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: November 2019
-- Description:	Generate all warehouse objects for Special Education
-- =============================================
CREATE PROCEDURE [warehouse].[BuildSpecialEducation]

	@StartFromYear int = null
AS

BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS ON;

	print ''
	print 'warehouse.BuildSpecialEducation'
	print '--------------------------'
	print ''


---------------------------------------------------------------------
-- warehouse.enrolSpEd
	-- school lebel totals of special education students
	-- this is the foundation for more aggregated views
	--
--------------------------------------------------------------------
	DELETE
	FROM warehouse.enrolSpEd
	WHERE surveyYear >= @StartFromYear or @StartFromYear is null


	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.enrolSpEd'


	INSERT INTO warehouse.enrolSpEd
	(
		schNo
	, SurveyYear
	, surveyDimensionID
	, ClassLevelCode
	, ClassLevel
	, EdLevelCode
	, EdLevel
	, EnvironmentCode
	, Environment
	, DisabilityCode
	, Disability
	, EnglishLearnerCode
	, EnglishLearner
	, EthnicityCode
	, Ethnicity
	, GenderCode
	, Gender
	, Age
	, AuthorityCode
	, Authority
	, DistrictCode
	, District
	, AuthorityGovtCode
	, AuthorityGovt
	, SchoolTypeCode
	, SchoolType
	, RegionCode
	, Region
	, Num
	)
Select SE.schNo
, stueYear SurveyYear
, surveyDimensionID
, stueClass ClassLevelCode
, LVL.Level	ClassLevel
, LVL.edLevelCode EdLevelCode
, LVL.[Education Level] EdLevel
, stueSpEdEnv EnvironmentCode
, ENV.codeDescription Environment
, stueSpEdDisability DisabilityCode
, DIS.codeDescription Disability
, stueSpEdEnglish EnglishLearnerCode
, EL.codeDescription EnglishLearner
, stuEthnicity EthnicityCode
, ETH.codeDescription Ethnicity
, stuGender GenderCode
, G.codeDescription Gender
, Age
, DSS.AuthorityCode
, DSS.Authority
, DSS.[District Code] DistrictCode
, DSs.District
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, DSS.[Region Code] RegionCode
, DSS.Region
, count(*) Num
FROM StudentEnrolment SE
LEFT JOIN warehouse.BestSurvey BSS
	ON Se.schNo = BSS.schNo
	AND SE.stueYear =  BSS.surveyYear
LEFT JOIN warehouse.dimensionSchoolSurvey DSS
	ON BSS.surveyDimensionID = DSS.[Survey ID]
LEFT JOIN lkpEnglishLearner EL
	ON stueSpEdEnglish = EL.codeCode
LEFT JOIN lkpSpEdEnvironment ENV
	ON stueSpEdEnv = ENV.codeCode
LEFT JOIN lkpEthnicity ETH
	ON stueSpEdEnv = ETH.codeCode
LEFT JOIN lkpDisabilities DIS
	ON stueSpEdDisability = DIS.codeCode
LEFT JOIN lkpGender G
	ON stuGender = G.codeCode
LEFT JOIN DimensionLevel LVL
	ON stueClass = LVL.LevelCode
WHERE stueSpEd = 1
AND (stueYear >= @StartFromYear or @StartFromYear is null)
GROUP BY SE.schNo
, stueYear
, surveyDimensionID
, stueClass
, stueSpEdEnv
, stueSpEdDisability
, stueSpEdEnglish
, stuEthnicity
, stuGender
, Age
, DSS.AuthorityCode
, DSS.Authority
, DSS.[District Code]
, DSs.District
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, DSS.[Region Code]
, DSS.Region
, EL.codeDEscription
, ENV.codeDescription
, ETH.codeDescription
, DIS.codeDescription
, G.codeDescription
, LVL.Level
, LVL.edLevelCode
, LVL.[Education Level]


	print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.enrolSpEd'


---- record that the warehouse and changed so clients using ETag will get updated
	exec warehouse.logVersion

	print ''
	print 'warehouse.BuildSpecialEducation completed'
	print ''

END
GO

