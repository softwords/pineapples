SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 14 04 2016
-- Description:	quick Pivot for teachers
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherTable]
	-- Add the parameters for the stored procedure here
	@RName nvarchar(50) = 'School Type'		-- identifier of row
	, @CName nvarchar(50) = 'Authority'	-- identifier of column
	, @xmlFilter xml = null				-- xml filter of selected data
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--- All teacher IDs
DECLARE @ids TABLE
(
	tID nvarchar(50)
	, recNo int
)

declare @NumM int = 0

if @xmlFilter is not null begin

	INSERT INTO @ids
	EXEC pTeacherRead.TeacherFilterIDs @NumMatches = @NumM OUT, @xmlFilter = @xmlFilter
	---EXEC pSchoolRead.SchoolFilterIDs @NumMatches = @NumM OUT, @xmlFilter = @xmlFilter

end
    -- Insert statements for procedure here
declare @district nvarchar(100)

declare @electN nvarchar(100)

declare @electL nvarchar(100)

declare @salaryLevel nvarchar(100)

Select @electN = vocabTerm
from sysVocab
WHERE vocabName = 'National Electorate'

Select @electL = vocabTerm
from sysVocab
WHERE vocabName = 'Local Electorate'

Select @district = vocabTerm
from sysVocab
WHERE vocabName = 'District'

Select @salaryLevel = vocabTerm
from sysVocab
WHERE vocabName = 'SalaryLevel'

declare @output TABLE
(
	R nvarchar(200) NULL,
	C nvarchar(200) NULL,
	RC nvarchar(200) NULL,
	Num int NULL, -- Total teachers
--	C int NULL, -- Certified total
--	T int NULL, -- Trained total
--	CF int NULL, -- Certified female
--	TF int NULL, -- Trained female
	RName nvarchar(400) NULL,
	CName nvarchar(400) NULL
)

-- get only the most recent teacherlocation record
-- for each teacher

-- is there a specifi year in the filter? if os, get the records from there, otherwise latest year
declare @InYear int
Select @InYear = n.value('@InYear','int')
from @xmlFilter.nodes('/Filter') T(n)

INSERT INTO @output

Select

	isnull(R,'<>') R
	, isnull(C,'<>') C
	, isnull(RC,'<>') RC
	, count(*) Num
    -- Include other data here

	, @RName Rname
	, @CName Cname
FROM
(

Select
	convert(nvarchar(100),
	case @RName
				when 'School Type' then ST.stDescription
				when 'SchoolType' then ST.stDescription
				when 'Authority' then A.authName
				when 'District' then D.dName
				when @district then D.dName
				when @electN then S.schElectL
			end) R
    ,
	convert(nvarchar(100),
	case @RName
				when 'School Type' then schType
				when 'SchoolType' then schType
				when 'Authority' then schAuth
				when 'District' then D.dID
			end) RC

	,
	case @CName
				when 'School Type' then ST.stDescription
				when 'SchoolType' then ST.stDescription
				when 'Authority' then A.authName
				when 'District' then D.dName

			end  C
From TeacherIdentity T
 LEFT JOIN (Select tID, coalesce(@InYear, max(SurveyYear)) dataYear from warehouse.TeacherLocation GROUP BY TID) MX
	ON T.tID = MX.tID
 LEFT JOIN warehouse.TeacherLocation TA
	ON MX.tID= TA.tID
	AND MX.dataYear = TA.SurveyYear
 LEFT JOIN Schools S
	ON S.schNo = TA.schNo
 LEFT JOIN SchoolTypes ST
	ON S.schType = ST.stCode
 LEFT JOIN Authorities A
	ON S.schAuth = A.authCode
 LEFT JOIN Islands I
	ON S.iCode = I.iCode
 LEFT JOIN Districts D
	ON I.iGroup = D.dID
 LEFT JOIN lkpElectorateN N
	ON S.schElectN = N.codeCode
 LEFT JOIN lkpElectorateL L
	ON S.schElectL = L.codeCode


WHERE (@xmlFilter is null or T.tID in (Select tID from @Ids))
) SUB
GROUP BY R,C,RC

declare @summaryRows int


Select R,C, RC, RName, CName, Num
from @Output
ORDER BY R,C


Select @summaryRows = @@rowcount -- = count(*) from @output

-- finally the summary
                SELECT isnull(sum(Num),0) NumMatches
                , 1 PageFirst
                , isnull(sum(Num),0) PageLast
                , 0 PageSize
                , 1 PageNo
                , 0 columnSet
                , count(*) SummaryRows
	FROM @output
END
GO

