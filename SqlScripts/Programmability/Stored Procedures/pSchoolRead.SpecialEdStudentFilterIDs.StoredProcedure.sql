SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 03 2018
-- Description:	Special Ed Student filter
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SpecialEdStudentFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@StudentID uniqueidentifier = null,
	@StudentCardID nvarchar(20) = null,
    @StudentGiven nvarchar(50) = null,
    @StudentFamilyName  nvarchar(50) = null,
    @StudentDoB  date = null,
	@StudentGender nvarchar(1) = null,
	@StudentEthnicity nvarchar(200) = null,

	-- selection based on enrolment
	@EnrolledAt nvarchar(50) = null,
	@EnrolYear int = null,
	@EnrolLevel nvarchar(10) = null,
	@EnrolDistrict nvarchar(10) = null,
	@EnrolAuthority nvarchar(10) = null,

	-- special ed specific
	@Disability nvarchar(20) = null,
	@EnglishLearner nvarchar(10)= null,
	@Environment nvarchar(20)= null,
	@CaseManager nvarchar(100) = null
AS
BEGIN
	SET NOCOUNT ON;
	-- preprocess some arguments so as to do pattern searches
	-- family name is always a like search. If no wild card
	if not (@StudentFamilyName is null) begin

			/* if surname or given name do not contain pattern characters %_
				add % to the end */
			Select @StudentFamilyName = replace(replace(@StudentFamilyName,'*','%'),'?','_')
			if patindex('%[%_]%',@StudentFamilyName) = 0
				set @StudentFamilyName = @StudentFamilyName + '%'
	end
	if not (@StudentGiven is null) begin
		Select @StudentGiven = replace(replace(@StudentGiven,'*','%'),'?','_')
	end

	DECLARE @keys TABLE
	(
	selectedStuID nvarchar(50)
	, recNo int IDENTITY PRIMARY KEY
	)
	-- interpret enrolyear - if it is 0:
	-- if school supplied, the most recent enrolment for the school
	-- if no school, the most recent year for any school
	--
	if (@EnrolYear = 0) begin
		select @enrolYear = max(stueYear)
		from StudentEnrolment_
		WHERE (schNo = @EnrolledAt or @EnrolledAt is null)
	end

	DECLARE @enrolMatch TABLE
	(
		ID uniqueidentifier
	)
	DECLARE @UseEnrol int = 0;

	if ( @EnrolledAt is not null) or (@EnrolYear is not null) or (@EnrolLevel is not null)
		or (@EnrolDistrict is not null) or (@EnrolAuthority is not null)  begin
		select @UseEnrol = 1
	end

	INSERT INTO @keys
	(selectedStuID)
	SELECT stuID
	FROM SpecialEdStudent_ S
	WHERE
	(stuID = @StudentID or @StudentID is null)
	AND (S.stuCardID = @StudentCardID or @StudentCardID is null)
	AND (S.stuGiven like @StudentGiven or @StudentGiven is null)
	AND (S.stuFamilyName like @StudentFamilyName or @StudentFamilyName is null)
	AND (S.stuDoB = @StudentDoB or @StudentDoB is null)
	AND (S.stuGender = @StudentGender or @StudentGender is null)
	AND (S.stuEthnicity = @StudentEthnicity or @StudentEthnicity is null)
	AND (S.stueSpEdDisability = @Disability or @Disability is null)
	AND (S.stueSpEdEnglish = @EnglishLearner or @EnglishLearner is null)
	AND (S.stueSpEdEnv = @Environment or @Environment is null)
	AND (@useEnrol = 0 or S.stuID in
		(
		Select stuID
		FROM
		StudentEnrolment_ SE
			LEFT JOIN Schools S
				ON SE.schNo = S.schNo
					LEFT JOIN Islands I
						ON S.iCode = I.iCode
		WHERE (SE.schNo = @EnrolledAt or @EnrolledAt is null)
		AND (I.iGroup = @EnrolDistrict or @EnrolDistrict is null)
		AND (S.schAuth = @EnrolAuthority or @EnrolAuthority is null)
		AND (stueClass = @EnrolLevel or @EnrolLevel is null)
		AND (stueYEar = @EnrolYear or @EnrolYear is null)
		)
	)


	ORDER BY

		CASE @sortColumn			-- strings
			WHEN 'StudentCardID' then stuCardID
			WHEN 'StudentGiven' then stuGiven
			WHEN 'StudentFamilyName' then stuFamilyName
			WHEN 'StudentEthnicity' then stuEthnicity
			WHEN 'StudentGender' then stuGender
			WHEN 'Disability' then stueSpEdDisability
			WHEN 'English' then stueSpEdEnglish
			WHEN 'Environment' then stueSpEdEnv
			-- check the actual database column names as well
			WHEN 'stuCardID' then stuCardID
			WHEN 'stuGiven' then stuGiven
			WHEN 'stuFamilyName' then stuFamilyName
			WHEN 'stuEthnicity' then stuEthnicity
			WHEN 'stuGender' then stuGender
			WHEN 'stueSpEdDisability' then stueSpEdDisability
			WHEN 'stueSpEdEnglish' then stueSpEdEnglish
			WHEN 'stueSpEdEnv' then stueSpEdEnv

			ELSE null
			END,
		CASE @sortColumn -- dates
			WHEN 'StudentDoB' then stuDoB
			WHEN 'stuDoB' then stuDoB
			ELSE null
		END,

		stuID


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedStuID
		, RecNo
		FROM
		(
			Select selectedStuID
			, @NumMatches - RecNo + 1 RecNo
			FROM @Keys
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)
				)
		ORDER BY RecNo
	end
	else begin

		SELECT selectedStuID
		, RecNo
		FROM @Keys
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
END
GO

