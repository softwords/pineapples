SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Reenable all triggers on tables and views
-- This may be necessary if some operation (sqlDelta v4?) has disabled triggers in a database operation
-- that did not complete. Invoked by common.afterschemaupgrade as an integrity measure
-- Oct 2017 : Changed to execute as 'pineapples' formerly 'dbo' => user pineapples must be a db_owner
-- =============================================
CREATE PROCEDURE [common].[EnableAllTriggers]
	-- Add the parameters for the stored procedure here

	with EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


 declare @output table
 (
	msg nvarchar(400)
 )
    declare @strSQL nvarchar(400)
	declare @LastSQL nvarchar(400)
	Select @strSQL = ''
	while (@strSQL is not null) begin

		Select @strSQL = null
		select TOP 1 @strSQL = 'ENABLE TRIGGER ' + t.name + ' ON ' + S.name + '.' + O.name from sys.triggers T
		INNER JOIN sys.objects O
		ON T.parent_id = o.object_id
		INNER JOIN sys.schemas S
		ON O.schema_id = S.schema_id
		WHERE T.is_disabled = 1
		if (@strSQL = @LastSQL)
			select @strSQL = null		-- force an exit if we look like getting loopy

		if ( @strSQL is not null) begin
			print @strSQL
			exec sp_executesql @strSQL
			Select @LastSQL = @strSQL

			INSERT INTO @output
			VALUES (@strSQL)
		end

end
Select * from @output
END
GO

