SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 8 2020
-- Description:	Report various table data created from a census workbook.
--
-- History Log:
--   * 1/08/2023, Ghislain Hachey, Disambiguate audit columns caused by support for audit data in SchoolSurvey table
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusStudentOutput]
	-- Add the parameters for the stored procedure here
	@fileReference uniqueidentifier			-- guid from the filedb of the census book
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @scope TABLE
(
	ssID int
	, schNo nvarchar(50)
	, surveyYear int
	, pCreateUser nvarchar(100)
	, pCreateDateTime datetime
)

INSERT INTO @scope
(ssID
, schNo
, surveyYear
, pCreateUser
, pCreateDateTime
)
Select ssID
, E.schNo
, stueYear surveyYear
, E.pCreateUser
, E.pCreateDateTime
FROM StudentEnrolment_ E
	LEFT JOIN SchoolSurvey SS
		on E.schNo = SS.schNo
		AND E.stueYear = SS.svyYear
WHERE stueCreateFileRef = @fileReference

-- RETURN ENROLMENTS
-- recordset 1: identifying data -forms the base of the return package
Select TOP 1 @filereference fileID
, surveyYear
, pCreateUser
, pCreateDateTime

FROM @scope
-- next the lookup lists:
-- schools
-- levels
-- ages
--

-- 2 the row code set -- in this case the school nos
-- schoolList
SElect DISTINCT isnull(C.schNo,'<>') codeCode
, isnull(C.schNo,'<>') + ': ' + schName codeDescription
FROM @scope C
	LEFT JOIN Schools S
		ON C.schNo = S.schNo
ORDER BY isnull(C.schNo,'<>')

---- 3 is the column definitions

Select codeCode
, codeDescription
FROM lkpLevels
ORDER BY lvlYear


--- ages
-- 3 - age range
SELECT num codeCode
, num codeDescription
FRom metaNumbers
WHERE num between 4 and 12
ORDER BY Num

-- enrolmentTableData

Select enLevel col
, isnull(schNo,'<>') row
, sum(enM) M
, sum(enF) F
FROM Enrollments E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @scope)
GROUP BY enLevel
, SchNo
ORDER BY SchNo
, enLevel


-- repeaterTableData

Select ptLevel col
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @scope)
AND ptCode = 'REP'
GROUP BY ptLevel
, SchNo
ORDER BY SchNo
, ptLevel


-- psaTableData

Select ptAge col
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @scope)
AND ptCode = 'PSA'
GROUP BY ptAge
, SchNo
ORDER BY SchNo
, ptAge


--- RETURN DIS
-- disabCodes
--
SELECT codeCode
, codeDescription
From lkpDisabilities
ORDER BY codeSeq

-- disabTableData

Select ptRow col		-- disability code is in ptRow, even though we wnt a column orientation in this case
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @scope)
AND ptCode = 'DIS'
GROUP BY ptRow
, SchNo
ORDER BY SchNo
, ptRow

--- RETURN DROPOUTS

-- 3 - columns are dropout reasons
--dropoutCodes
SELECT DISTINCT ptPage codeCode
, ptPage codeDescription
From PupilTables
WHERE ptCode = 'DROP'


-- 4 dropout data
-- dropoutTableData

Select ptPage col		-- dropout code is in ptPage
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @scope)
AND ptCode = 'DROP'
GROUP BY ptPage
, SchNo
ORDER BY SchNo
, ptPage

--- RETURN TRIN
-- trinTableData
Select ptLevel col		-- dropout code is in ptPage
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @scope)
AND ptCode = 'TRIN'
GROUP BY ptLevel
, SchNo

--- RETURN TROUT
-- troutTableData
Select ptLevel col		-- dropout code is in ptPage
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @scope)
AND ptCode = 'TROUT'
GROUP BY ptLevel
, SchNo
END
GO

