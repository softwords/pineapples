SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 6 2021
-- Description:	Proc to customize generation of pdf using
--				cenopdf
-- =============================================
CREATE PROCEDURE [cenopdf].[ExistingStaff]
	@formtype nvarchar(10),	-- either PRI or SEC
	@targetFile nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @t TABLE
	(
		formtype nvarchar(10),
		schooltype nvarchar(10),
		targetfile nvarchar(500),
		classlevels nvarchar(max),
		teacherroles nvarchar(max),
		teacherQualE nvarchar(max),
		teacherQualNonE nvarchar(max)
	)

	INSERT INTO @t (formtype, schooltype)
	Select tt, st from
	(
		-- this implicitly maps the formtype to a school type
		-- to use for getting levels and teacher roles etc
		Select 'SEC' tt, 'CS' st UNION ALL Select 'PRI' tt, 'P' st
	) U
	WHERE @formtype = U.tt or nullif(@formtype, '') is null

	UPDATE @t
	SET targetfile = replace(@targetfile,'%formtype%', formtype),
	classlevels = cenopdf.ClassLevelItems(schoolType),		-- schooltype, not formtype
	teacherroles = cenopdf.TeacherRoleItems(schoolType),
	teacherQualE = cenopdf.TeacherQualItems(1),			-- ed qualifications
	teacherQualNonE = cenopdf.TeacherQualItems(0)		-- non ed quals


	declare @fontSize int = 6

	Select
	teacherroles [TL.00.Role_ItemsText]
	, teacherroles [TL.01.Role_ItemsText]
	, teacherroles [TL.02.Role_ItemsText]
	, teacherroles [TL.03.Role_ItemsText]
	, teacherroles [TL.04.Role_ItemsText]
	, teacherroles [TL.05.Role_ItemsText]
	, teacherroles [TL.06.Role_ItemsText]
	, teacherroles [TL.07.Role_ItemsText]
	, teacherroles [TL.08.Role_ItemsText]
	, teacherroles [TL.09.Role_ItemsText]
	, teacherroles [TL.10.Role_ItemsText]
	, teacherroles [TL.11.Role_ItemsText]
	, teacherroles [TL.12.Role_ItemsText]
	, teacherroles [TL.13.Role_ItemsText]
	, teacherroles [TL.14.Role_ItemsText]

	, teacherQualE [TL.00.QualEd_ItemsText]
	, teacherQualE [TL.01.QualEd_ItemsText]
	, teacherQualE [TL.02.QualEd_ItemsText]
	, teacherQualE [TL.03.QualEd_ItemsText]
	, teacherQualE [TL.04.QualEd_ItemsText]
	, teacherQualE [TL.05.QualEd_ItemsText]
	, teacherQualE [TL.06.QualEd_ItemsText]
	, teacherQualE [TL.07.QualEd_ItemsText]
	, teacherQualE [TL.08.QualEd_ItemsText]
	, teacherQualE [TL.09.QualEd_ItemsText]
	, teacherQualE [TL.10.QualEd_ItemsText]
	, teacherQualE [TL.11.QualEd_ItemsText]
	, teacherQualE [TL.12.QualEd_ItemsText]
	, teacherQualE [TL.13.QualEd_ItemsText]
	, teacherQualE [TL.14.QualEd_ItemsText]


	, teacherQualNonE [TL.00.Qual_ItemsText]
	, teacherQualNonE [TL.01.Qual_ItemsText]
	, teacherQualNonE [TL.02.Qual_ItemsText]
	, teacherQualNonE [TL.03.Qual_ItemsText]
	, teacherQualNonE [TL.04.Qual_ItemsText]
	, teacherQualNonE [TL.05.Qual_ItemsText]
	, teacherQualNonE [TL.06.Qual_ItemsText]
	, teacherQualNonE [TL.07.Qual_ItemsText]
	, teacherQualNonE [TL.08.Qual_ItemsText]
	, teacherQualNonE [TL.09.Qual_ItemsText]
	, teacherQualNonE [TL.10.Qual_ItemsText]
	, teacherQualNonE [TL.11.Qual_ItemsText]
	, teacherQualNonE [TL.12.Qual_ItemsText]
	, teacherQualNonE [TL.13.Qual_ItemsText]
	, teacherQualNonE [TL.14.Qual_ItemsText]

	, @FontSize [TL.00.Role_FontSize]
	, @FontSize [TL.01.Role_FontSize]
	, @FontSize [TL.02.Role_FontSize]
	, @FontSize [TL.03.Role_FontSize]
	, @FontSize [TL.04.Role_FontSize]
	, @FontSize [TL.05.Role_FontSize]
	, @FontSize [TL.06.Role_FontSize]
	, @FontSize [TL.07.Role_FontSize]
	, @FontSize [TL.08.Role_FontSize]
	, @FontSize [TL.09.Role_FontSize]
	, @FontSize [TL.10.Role_FontSize]
	, @FontSize [TL.11.Role_FontSize]
	, @FontSize [TL.12.Role_FontSize]
	, @FontSize [TL.13.Role_FontSize]
	, @FontSize [TL.14.Role_FontSize]

	, @FontSize [TL.00.QualEd_FontSize]
	, @FontSize [TL.01.QualEd_FontSize]
	, @FontSize [TL.02.QualEd_FontSize]
	, @FontSize [TL.03.QualEd_FontSize]
	, @FontSize [TL.04.QualEd_FontSize]
	, @FontSize [TL.05.QualEd_FontSize]
	, @FontSize [TL.06.QualEd_FontSize]
	, @FontSize [TL.07.QualEd_FontSize]
	, @FontSize [TL.08.QualEd_FontSize]
	, @FontSize [TL.09.QualEd_FontSize]
	, @FontSize [TL.10.QualEd_FontSize]
	, @FontSize [TL.11.QualEd_FontSize]
	, @FontSize [TL.12.QualEd_FontSize]
	, @FontSize [TL.13.QualEd_FontSize]
	, @FontSize [TL.14.QualEd_FontSize]

	, @FontSize [TL.00.Qual_FontSize]
	, @FontSize [TL.01.Qual_FontSize]
	, @FontSize [TL.02.Qual_FontSize]
	, @FontSize [TL.03.Qual_FontSize]
	, @FontSize [TL.04.Qual_FontSize]
	, @FontSize [TL.05.Qual_FontSize]
	, @FontSize [TL.06.Qual_FontSize]
	, @FontSize [TL.07.Qual_FontSize]
	, @FontSize [TL.08.Qual_FontSize]
	, @FontSize [TL.09.Qual_FontSize]
	, @FontSize [TL.10.Qual_FontSize]
	, @FontSize [TL.11.Qual_FontSize]
	, @FontSize [TL.12.Qual_FontSize]
	, @FontSize [TL.13.Qual_FontSize]
	, @FontSize [TL.14.Qual_FontSize]

	, targetFile targetFile
	FROM @t


END
GO

