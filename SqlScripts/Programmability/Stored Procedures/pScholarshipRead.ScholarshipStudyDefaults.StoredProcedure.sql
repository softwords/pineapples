SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 11 2020
-- Description:	Roll over the scholarship study: ie generate a 'virtual record'
-- that is used as the defaults for a new scholarship study record at the client
--
-- =============================================
CREATE PROCEDURE [pScholarshipRead].[ScholarshipStudyDefaults]
	-- Add the parameters for the stored procedure here
	@scholarshipID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select schoID
, posYear
, posSemester
, gyIndex
, instCode
, fosCode
, posSubject
, null posGPA
, null posPaid
, pScholarshipRead.scholarshipAmount(gyIndex, fosCode, prevGPA) posAmount
FROM
(
Select TOP 1 S.schoID
, case
	when posSemester is null then S.schrYear
	when posSemester = instTermsPerYear then posYear + 1
	when posSemester = 3 then posYear + 1
	else posYear end posYear
, case
	when posSemester is null then 1
	when posSemester = instTermsPerYear then 1
	when posSemester = 3 then 1
	else posSemester + 1 end posSemester
, case
	when posSemester is null then 1
	when posSemester = instTermsPerYear then gyIndex + 1
	when posSemester = 3 then gyIndex + 1
	else gyIndex end gyIndex
, isnull(Ss.instCode, s.schoInst) instCode
, isnull(SS.fosCode, S.schoFos) fosCode
, isnull(SS.posSubject, S.schoSubject) posSubject
, case when posID is null then S.schoGPA else posGPA end prevGPA

from Scholarships S
LEFT JOIN ScholarshipStudy_ SS
	ON S.schoID = SS.schoID
LEFT JOIN ScholarshipInstitutions I
	ON SS.instCode = I.instCode
WHERE S.schoID = @scholarshipID
ORDER BY posYear DESC, posSemester DESC
) SUB
END
GO

