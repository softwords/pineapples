SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 6 2022
-- Description:	REconstruct the table warehouse.washToilets
-- This version comabines data from the Toiulets table (as collected by the FSM/RMI census)
-- with data collected from the Android wash survey. The Android survey takes precedence
-- Census data for a given year is not included if the school has a wash survey in that year
--   Now supports disaggregation by ToiuletType, in order to support KEMIS Wash collection
-- =============================================
CREATE PROCEDURE [warehouse].[buildWashToilets]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS ON;

	declare @debug int = 0
	declare @counter int

	print ''
	print 'warehouse.WashToilets'
	print '--------------------------'
	print ''


-- for each year, holds the survey year that is the best avilable data in the year
-- it may be an earlier year, if the survey does not contain wash data
 DECLARE @bestToilets TABLE
 (
	surveyYear int
	, schoolNo nvarchar(50)
	, ToiletSurveyID int
	, ToiletDataYear int
 )

 INSERT INTO @bestToilets

 Select surveyYear
 , schNo
 , ToiletSurveyID
 , ToiletDataYear
 FROM
 (
 Select BS.SurveyYEar, BS.schNo, BS.SurveyDimensionID
 , SS.ssID ToiletSurveyID, SS.svyYEar ToiletDataYear
 , ROW_NUMBER() over (Partition by BS.SurveyYEar, BS.schNo order by SS.svyYear DESC) RN
 from warehouse.BestSurvey BS
 LEFT JOIN SchoolSurvey SS
 ON BS.schNo = SS.schNo
 AND BS.SurveyYear >= SS.svyYear
 AND ss.ssID in ( Select DISTINCT ssID from Toilets WHERe toiNum is not null)
 )SUB
 WHERE rn = 1
 ORDER BY surveyYEar, schNo, ToiletDataYear

-- temporary table to assemble the toilet data from both Inspections (Android survey) and census data
 DECLARE @tmpToilets TABLE
 (
 surveyYear int
 , schNo nvarchar(50)
 , inspID int NULL
, InspectionYear int NULL
, TotalF int NULL
, UsableF int NULL
, EnrolF int NULL
, TotalM int NULL
, UsableM int NULL
, EnrolM int NULL
, TotalC int NULL
, UsableC int NULL
, Total int NULL
, Usable int NULL
, Enrol int NULL
, rowType nvarchar(10) null
, ToiletType nvarchar(100)
)

-- step 1 : collect toilet data from android surveys. This is already in warehouse.washDetail

--- here are the hardcoded strings from the WASH survey Xml that we rely on
declare @toiletTypeQuestionTag nvarchar(30) = 'CW.S.1'
declare @toiletQuestionTag nvarchar(30) = 'CW.S.2'
--- These are items in question CW.S.2
declare @usableTag nvarchar(100) = 'Number that are usable (accessible, functional, private)'
declare @totalTag nvarchar(100) = 'Total Number'

declare @girlsTag nvarchar(100) = 'Girls’ only toilets'
declare @boysTag nvarchar(100) = 'Boys’ only toilets'
declare @commonTag nvarchar(100) = 'Common use only toilets'


Select @counter = count(*) from
(
Select DISTINCT schNo, inspectionYEar from warehouse.WashDetail
WHERE Question = @toiletQuestionTag
) S
print convert(nvarchar(5), @counter) + ' WashDetail distinct school/year records found with Toilet info (Android Wash surveys)'


INSERT INTO @tmpToilets
( schNo
, surveyYear
, RowType
, inspID
, InspectionYear
, ToiletType
, TotalF
, UsableF
, TotalM
, UsableM
, TotalC
, UsableC
, Total
, Usable
)
Select BI.schNo
, SurveyYear
, 'WASH'
, BI.inspID
, BI.InspectionYear
-- toilet type
, max(case Question when @ToiletTypeQuestionTag then
		Response end) ToiletType

, sum(case Response
		when @totalTag then
			case Item
				when @girlsTag then Num end
		end) TotalF
, sum(case Response
		when @usableTag then
			case Item
				when @girlsTag then Num end
		end) UsableF
, sum(case Response
		when @totalTag then
			case Item
				when @boysTag then Num end
		end ) TotalM

, sum(case Response
		when @usableTag then
			case Item
				when @boysTag then Num end
		end ) UsableM
, sum(case Response
		when @totalTag then
			case Item
				when @commonTag then Num end
		end ) TotalC
, sum(case Response
		when @usableTag then
			case Item
				when @commonTag then Num end
		end ) UsableC
, sum(case Response
		when @totalTag then Num end) Total

, sum(case Response
		when  @usableTag then Num end ) Usable

from warehouse.BestInspection BI
INNER JOIN warehouse.WashDetail W
	ON BI.inspID = W.inspID
where Question in (@toiletQuestionTag, @ToiletTypeQuestionTag)
GROUP BY
SurveyYear
, BI.schNo
, BI.inspID
, BI.InspectionYear

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts into temp Toilets from WashDetail (Android Wash surveys)'

-- Step 2:
-- data from Toilets table records. This table is written by both the FEDEMIS/RMI Excel census (pSurveyOps.censusLoadWash)
-- and the Kiribati pdf survey ( pSurveyWrite.xfdfToilets)

INSERT INTO @tmpToilets
( schNo
, surveyYear
, RowType
 , InspectionYear
, ToiletType
, TotalF
, UsableF
, TotalM
, UsableM
, TotalC
, UsableC
, Total
, Usable
)
Select SUB.schoolNo
, SUB.surveyYear
, 'CENSUS'
, ToiletDataYear
, toiType
, sum(case when toiUse = 'Girls' then Num end) totalF
, sum(case when toiUse = 'Girls' then NumUsable end) UsableF
, sum(case when toiUse = 'Boys' then Num end) totalM
, sum(case when toiUse = 'Boys' then NumUsable end) UsableM
, sum(case when toiUse in ('Shared','Common') then Num end) totalC

, sum(case when toiUse in ('Shared','Common') then NumUsable end) UsableC
, sum(Num) total

, sum(NumUsable) Usable

from
(
SELECT
	  schoolNo
	  , surveyYear
	  , ToiletDataYear
      ,[toiUse]
	  , toiType
      ,sum([toiNum]) Num
      ,sum(case when [toiNumUsable] is not null then toiNumUsable
			when toiCondition is null then toiNum
			when [toiCondition] not in ('3','C') then toiNum
			else null end) NumUsable

  FROM @BestToilets BT
  LEFT JOIN [Toilets] T
	 ON T.ssID = BT.ToiletSurveyID
 GROUP BY
	schoolNo
	, surveyYear
	, ToiletDataYear
    , [toiUse]
	, toiType
) SUB
LEFT JOIN @Tmptoilets TT
ON SUB.schoolNo = TT.schNo
AND SUB.SurveyYEar = TT.surveyYear
AND ( ToiletDataYear <= TT.InspectionYear or ToiletDataYear is null)
WHERE TT.surveyYear is null  -- dont include census data if there is an android survey in that year or later than the year of census data
 GROUP BY
	schoolNo
	, SUB.surveyYear
	, ToiletDataYear
	, toiType
print convert(nvarchar(5), @@ROWCOUNT) + ' inserts into temp Toilets from Toilets table (census)'

-- we still may need to clean up some dups where an older WASH is superceded by a survey
DELETE FROM @tmpToilets

FROM @TmpToilets
INNER JOIN @TmpToilets C
ON [@TmpToilets].schNo = C.schNo
and [@TmpToilets].surveyYear = C.surveyYEar
and C.rowType = 'CENSUS'
and [@tmpToilets].rowType = 'WASH'
and C.inspectionYEar > [@tmpToilets].InspectionYear

print convert(nvarchar(5), @@ROWCOUNT) + ' superceded WASH removed from temp Toilets'

-- debugging toverify we have only one entry per school in any year, either from CENSUS or WASH
-- NO LOGER VALID BECUASE WE DISAGGRAGATE BY TOILET TYPE
if @debug = 1 begin
	Select rowType, count(*)
	from @tmpToilets
	group by rowType

	Select schNo, surveyYear, count(*)
	from @tmpToilets
	 group by schNo, surveyYear
	HAVING count(*) > 1
	-- now insert the enrolments

	Select T.*
	from @tmpToilets T
	inner join
	(
	Select schNo, surveyYear, count(*) NUM
	from @tmpToilets
	group by schNo, surveyYear
	HAVING count(*) > 1
	) S
	ON T.schNo = S.schNo
	AND T.surveyYEar = S.surveyyear

end -- debug

-- insert the enrolments - create a new record if there is more than 1 toilet type
-- otherwise update the one and only record

UPDATE @tmpToilets
SET Enrol = R.Enrol
, EnrolF = R.EnrolF
, EnrolM = R.EnrolM
FROM @tmpToilets
INNER JOIN warehouse.enrolSchoolR R
ON [@tmpToilets].schNo = R.schNo
AND [@tmpToilets].surveyYear = R.surveyYear

WHERE Exists (
Select schNo, surveyYEar, count(DISTINCT toiletType)
from @tmpToilets WT
WHERE WT.schNo = R.schNo AND WT.surveyYear = R.surveyYear
group by WT.schNo, WT.surveyYEar
HAVING count(DISTINCT ToiletType) = 1
)
-- include the enrolments at the school even if there are multiple toilet types
raiserror('%i updates of temp Toilets of enrolments',0,1, @@rowcount);

INSERT INTO @tmpToilets
(
schNo
, surveyYear
, rowType
, EnrolF
, EnrolM
, Enrol
)
SElect
schNo
, surveyYear
, null			-- omit the row type so as not to complicate the output of source (max(rowType))
, EnrolF
, EnrolM
, Enrol
from warehouse.enrolSchoolR R

WHERE not Exists (
Select schNo, surveyYEar, count(DISTINCT toiletType)
from @tmpToilets WT
WHERE WT.schNo = R.schNo AND WT.surveyYear = R.surveyYear
group by WT.schNo, WT.surveyYEar
HAVING count(DISTINCT ToiletType) = 1
)

raiserror('%i inserts into temp Toilets of enrolments', 0,1,@@rowcount)

-- now update the warehouse.WashToilets

DELETE from warehouse.WashToilets
WHERE SurveyYear >= @StartFromYear or @StartFromYear is null

print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.WashToilets'

INSERT INTO warehouse.WashToilets
           (schNo
	        ,SurveyYear
			,inspID
			,InspectionYear
			,ToiletType
			,TotalF
			,UsableF
			,EnrolF
			,TotalM
			,UsableM
			,EnrolM
			,TotalC
			,UsableC
			,Total
			,Usable
			,Enrol
			,Source
			,DistrictCode
			,District
			, SchoolTypeCode
			, SchoolType
			,AuthorityCode
			,Authority
			,AuthorityGovtCode
			,AuthorityGovt
          )

Select
 T.schNo
 , T.surveyYear
 , max(inspID) inspID
, max(InspectionYear) InspectionYear
, ToiletType
, sum(TotalF) TotalF
, sum(UsableF) UsableF
, sum(EnrolF) EnrolF
, sum(TotalM) TotalM
, sum(UsableM) UsableM
, sum(EnrolM) EnrolM
, sum(TotalC) TotalC
, sum(UsableC) UsableC
, sum(Total) Total
, sum(Usable) Usable
, sum(Enrol) Enrol
, max(rowType) rowType -- we only have EITHER a CENSUS or a WASH type record, and an Enrolment record (which has null rowType)
-- dimensions
,DistrictCode
,District
,SchoolTypeCode
,SchoolType
,AuthorityCode
,Authority
,AuthorityGovtCode
,AuthorityGovt

from @tmpToilets T
LEFT JOIN warehouse.BestSurvey BS
	ON T.surveyYear = BS.SurveyYear
	AND T.schNo = BS.schNo
LEFT JOIN warehouse.dimensionSchoolSurvey DSS
	ON DSS.[Survey ID] = BS.surveyDimensionID
WHERE T.SurveyYear >= @StartFromYear or @StartFromYear is null
GROUP BY T.SurveyYEar
, T.schNo
,DistrictCode
,District
,SchoolTypeCode
,SchoolType
,AuthorityCode
,Authority
,AuthorityGovtCode
,AuthorityGovt
, ToiletType

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts into warehouse.WashToilets from temp Toilets'

print '--- end warehouse.BuildWashToilets'

exec warehouse.logVersion

END
GO

