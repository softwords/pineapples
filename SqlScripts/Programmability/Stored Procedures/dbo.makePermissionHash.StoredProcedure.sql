SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 2 2017
-- Description:	transpose security defined by AD groups / role memberships
--
-- =============================================
CREATE PROCEDURE [dbo].[makePermissionHash]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @hash TABLE
(
	name nvarchar(200)
	, permissionHash nvarchar(20)
)
Select RM.*, R.name, M.name, M.type_desc
  from sys.database_role_members RM
	INNER JOIN sys.database_principals R
		ON RM.role_principal_id = R.principal_id
	INNER JOIN sys.database_principals M
		ON RM.member_principal_id = M.principal_id
WHERE M.type = 'G'
ORDER BY M.name, R.Name


 select M.name,
   case
	when R.name like 'pAdmin%' then 'Admin'
	when R.name like 'pEnrolment%' then 'Enrolment'
	when R.name like 'pEstablishment%' then 'Establishment'
	when R.name like 'pFinance%' then 'Finance'
	when R.name like 'pInfrastructure%' then 'Infrastructure'
	when R.name like 'pInspection%' then 'Inspection'
	when R.name like 'pIST%' then 'IST'
	when R.name like 'pSchool%' then 'School'
	when R.name like 'pSecurity%' then 'Security'
	when R.name like 'pSurvey%' then 'Survey'
	when R.name like 'pTeacher%' then 'Teacher'
  end Topic,
  case
	when R.name like '%Read' then 1
	when R.name like '%ReadX' then 2
	when R.name like '%Write' then 4
	when R.name like '%WriteX' then 8
	when R.name like '%Ops' then 16
	when R.name like '%Admin' then 32
  end Access
  from sys.database_role_members RM
	INNER JOIN sys.database_principals R
		ON RM.role_principal_id = R.principal_id
	INNER JOIN sys.database_principals M
		ON RM.member_principal_id = M.principal_id
WHERE M.type = 'G'
ORDER BY M.name, R.Name


Select name
, Topic
, case when Topic = 'Enrolment' then char(sum(Access) + 48) end Enrolment
, case when Topic = 'Establishment' then char(sum(Access) + 48)  end Establishment
, case when Topic = 'Exam' then char(sum(Access) + 48)  end Exam
, case when Topic = 'Finance' then char(sum(Access) + 48)  end Finance
, case when Topic = 'Infrastructure' then sum(Access) end Infrastructure
, case when Topic = 'Inspection' then sum(Access) end Inspection
, case when Topic = 'IST' then sum(Access) end IST
, case when Topic = 'PA' then sum(Access) end PA
, case when Topic = 'School' then sum(Access) end School
, case when Topic = 'Survey' then sum(Access) end Survey
, case when Topic = 'Teacher' then sum(Access) end Teacher

, sum(Access) Access
, char(sum(Access) + 48) C
FROM
(
  select M.name,
   case
	when R.name like 'pAdmin%' then 'Admin'
	when R.name like 'pEnrolment%' then 'Enrolment'
	when R.name like 'pEstablishment%' then 'Establishment'
	when R.name like 'pFinance%' then 'Finance'
	when R.name like 'pInfrastructure%' then 'Infrastructure'
	when R.name like 'pInspection%' then 'Inspection'
	when R.name like 'pIST%' then 'IST'
	when R.name like 'pSchool%' then 'School'
	when R.name like 'pSecurity%' then 'Security'
	when R.name like 'pSurvey%' then 'Survey'
	when R.name like 'pTeacher%' then 'Teacher'
  end Topic,
  case
	when R.name like '%Read' then 1
	when R.name like '%ReadX' then 2
	when R.name like '%Write' then 4
	when R.name like '%WriteX' then 8
	when R.name like '%Ops' then 16
	when R.name like '%Admin' then 32
  end Access
  from sys.database_role_members RM
	INNER JOIN sys.database_principals R
		ON RM.role_principal_id = R.principal_id
	INNER JOIN sys.database_principals M
		ON RM.member_principal_id = M.principal_id
WHERE M.type = 'G'
) U
WHERE Topic is not null
GROUP BY name, Topic
ORDER BY Name, Topic


--Select * from [SIEMIS-Identities].dbo.AspNetUserClaims

--Select ascii('O')

--select char(79)

INSERT INTO @hash
( name, permissionHash)

Select substring(name, charindex('\', name) + 1, len(name) - charindex('\', name))
, (
	isnull(min(Enrolment),'0')
	+ isnull(min(Establishment),'0')
	+ isnull(min(Exam),'0')
	+ isnull(min(Finance),'0')
	+ isnull(min(Infrastructure),'0')
	+ isnull(min(Inspection),'0')
	+ isnull(min(IST),'0')
	+ isnull(min(PA),'0')
	+ isnull(min(School),'0')
	+ isnull(min(Survey),'0')
	+ isnull(min(Teacher),'0')
	) PermissionHash
FROM
(
Select name
, Topic
, case when Topic = 'Enrolment' then char(sum(Access) + 48) end Enrolment
, case when Topic = 'Establishment' then char(sum(Access) + 48)  end Establishment
, case when Topic = 'Exam' then char(sum(Access) + 48)  end Exam
, case when Topic = 'Finance' then char(sum(Access) + 48)  end Finance
, case when Topic = 'Infrastructure' then char(sum(Access) + 48)  end Infrastructure
, case when Topic = 'Inspection' then char(sum(Access) + 48)  end Inspection
, case when Topic = 'IST' then char(sum(Access) + 48)  end IST
, case when Topic = 'PA' then char(sum(Access) + 48) end PA
, case when Topic = 'School' then char(sum(Access) + 48)  end School
, case when Topic = 'Survey' then char(sum(Access) + 48)  end Survey
, case when Topic = 'Teacher' then char(sum(Access) + 48)  end Teacher

, sum(Access) Access
, char(sum(Access) + 48) C
FROM
(
  select M.name,
   case
	when R.name like 'pAdmin%' then 'Admin'
	when R.name like 'pEnrolment%' then 'Enrolment'
	when R.name like 'pEstablishment%' then 'Establishment'
	when R.name like 'pFinance%' then 'Finance'
	when R.name like 'pInfrastructure%' then 'Infrastructure'
	when R.name like 'pInspection%' then 'Inspection'
	when R.name like 'pIST%' then 'IST'
	when R.name like 'pSchool%' then 'School'
	when R.name like 'pSecurity%' then 'Security'
	when R.name like 'pSurvey%' then 'Survey'
	when R.name like 'pTeacher%' then 'Teacher'
  end Topic,
  case
	when R.name like '%Read' then 1
	when R.name like '%ReadX' then 2
	when R.name like '%Write' then 4
	when R.name like '%WriteX' then 8
	when R.name like '%Ops' then 16
	when R.name like '%Admin' then 32
  end Access
  from sys.database_role_members RM
	INNER JOIN sys.database_principals R
		ON RM.role_principal_id = R.principal_id
	INNER JOIN sys.database_principals M
		ON RM.member_principal_id = M.principal_id
WHERE M.type = 'G'
) U
WHERE Topic is not null
GROUP BY name, Topic
) U2
GROUP BY name


Select * from @hash

Select C.* , U.UserName
from [SIEMIS-Identities].dbo.AspNetUserClaims C
	INNER JOIN [SIEMIS-Identities].dbo.AspNetUsers U
		on C.UserID = U.Id

Select H.* , U.id
from @Hash H
	LEFT JOIN [SIEMIS-Identities].dbo.AspNetUsers U
		ON H.name = U.userName

Select * from [SIEMIS-Identities].dbo.AspNetUsers

END
GO

