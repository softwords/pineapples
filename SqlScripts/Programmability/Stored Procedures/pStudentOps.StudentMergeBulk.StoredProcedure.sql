SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 9 2022
-- Description:	Deduplicate student records in bulk
-- a pair will be Deduplicated iff:
-- -- 1a) the match between records is complete: given name,family name, student card, gender and dob
	-- OR 1b) the match differs only by gender, dob or card
		--		AND - the enrolments share a common school, or the aggregate enrolments form a contiguous
		--		series of years with a coherent chain of promotions
-- AND 2) there are no conflicting enrolment records (ie enrolments in same year)
-- =============================================
CREATE PROCEDURE [pStudentOps].[StudentMergeBulk]
	-- Add the parameters for the stored procedure here
	@PageSize int			-- maximum number to dedup - cannot be great than 1000
	, @PageNo int = 1
	, @Given nvarchar(100) = null	-- filter by given name
	, @Surname nvarchar(100) = null
	, @Ignore nvarchar(10) = null
	, @commit int = 1			-- for testing set to 0 to rollback
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- validate the arguments

if not (@Ignore is null or @Ignore in ('card','gender', 'dob') ) begin
	raiserror('@Ignore argument must be "card" "gender" "dob" or null',10,1)
	return
end

Select NE NumEnrolmentRecords, count(stuID) NumStudents
from
(
select stuID, count(*) NE
from StudentEnrolment_
GROUP BY stUID
) SUB
GROUP BY NE
ORDER BY NE
declare @K TABLE
(
stuID uniqueidentifier
, dupID uniqueidentifier
, RecNo int
)


declare @matchCard int = 1
declare @MatchGender int = 1
declare @MatchDoB int = 1
declare @sourceID uniqueidentifier
declare @targetID uniqueidentifier
declare @sourceS nvarchar(100)
declare @targetS nvarchar(100)

declare @lastYear int			-- for a source/target pair, the year of the most recent studentEnrolment_
declare @rec int = 1
declare @maxrec int = 1
declare @Merged int = 0;
declare @Ignored int = 0
declare @error int = 0
declare @found int

declare @Matches int

declare @rc int					-- to capture @@rowcount when necessary


Select @MatchCard = case when @Ignore = 'card' then 0 else 1 end
, @MatchGender = case when @Ignore = 'gender' then 0 else 1 end
, @MatchDoB = case when @Ignore = 'dob' then 0 else 1 end
INSERT INTO @K
exec pstudentOps.DuplicateStudentFilterIds  @Matches OUT,@PageSize,@PageNo,'Surname',0, 1,1,@MatchCard,@MatchDoB,@MatchGender,  @given, @Surname

select @found = count(*) from @K
raiserror('Student Merge Bulk',0,1)
raiserror('%i duplicates found',0,1,@found)

if @found = 0 begin
	Select @Matches DuplicatesFound, @found SelectedToProcess, @Merged Merged, @ignored Ignored, @error Errors
	return
end

Select @rec = min(recNo)
, @maxrec = max(recNo)
from @k K

begin transaction
-- set Target to be the most recently used record, assume this one has the better set of values
Select TOP 1 @targetID = SE.stuID			-- TOP 1 not actually necessary, assigning variables from a multirow set will only ever use the first row
, @sourceID = case SE.stuID when K.dupID then K.stuID when K.stuID then K.dupID end
, @given = stuGiven, @surname = stuFamilyName
, @lastYear = stueYear
from StudentEnrolment SE
INNER JOIN @K K
	ON SE.stuID in (K.stuID, K.dupID)
	AND recNo = @rec
ORDER BY SE.stueYear DESC, SE.stuID

-- string versions

while @sourceID is not null begin
	select @sourceS = convert(nvarchar(100), @sourceID), @targetS = convert(nvarchar(100),@targetID)
	raiserror('%i: %s=>%s Last Enrolment=%i', 0,1, @rec, @sourceS, @targetS, @lastYear)

	if exists(select stuID from Student_ WHERE stuID = @sourceID)
		and exists(select stuID from Student_ WHERE stuID = @targetID) begin -- there is a matching student of source
		-- are there any conflicts in enrolment years? ie Source and Target both have an enrolment in a given year...
		declare @yearDups int  -- number of such duplicate years

		-- what if we have two records that are the same year, school and  class level? we can delete one...
		DELETE
		FROM StudentEnrolment_
		FROM StudentEnrolment_
			INNER JOIN StudentEnrolment_ StudentEnrolmentT
			ON StudentEnrolment_.stuID = @sourceID  -- ie the one referencing @sourceID is the one that gets removed
			AND StudentEnrolmentT.stuID = @targetID
			AND StudentEnrolment_.stueYear = StudentEnrolmentT.stueYear
			AND StudentEnrolment_.schNo = StudentEnrolmentT.schNo
			AND StudentEnrolment_.stueClass = StudentEnrolmentT.stueClass
		select @rc = @@rowcount
		if @rc > 0 begin
			raiserror('%i: %i duplicate enrolments removed from %s', 0,1, @rec, @rc, @sourceS)
		end
		Select @yearDups = count(*)
			from
			(
			Select stueYear from StudentEnrolment_ SE
			WHERE SE.stuID in (@sourceID, @targetID)
			GROUP BY stueYear
			HAVING count(*) > 1
			) SUB

		-- if we are not matching on card, require that at least we have a matching school
		-- in each list
		declare @schoolMatches int
		declare @enrolSequence int
		if @Ignore is not null begin
			Select @schoolMatches = count(*)
			FROM
			(
				select isnull(schNextSchool, SE.schNo) schNo
				from StudentEnrolment_ SE
				INNER JOIN Schools
				ON SE.schNo = Schools.schNo
				WHERE stuID in (@sourceID, @targetID)
				Group by isnull(schNextSchool, SE.schNo)
				having count(distinct stuID) > 1
			) SUB
			if (@schoolMatches = 0) begin
				raiserror('%i: %s %s %i matching schools for source and target', 0,1, @rec, @given, @surname, @SchoolMatches)
			end

			-- a sequence of grades in consecutive years
			Select @enrolSequence =
			case when
				-- regular promotion or repeitions identified to account for the extra year
				(
					(MaxYear - MinYear = MaxYoE - minYoE)
					OR (MaxYear - MinYear = MaxYoE - minYoE + NumRepeats)
				)
				-- no gaps in enrolment series
				and NumYears = (MaxYear - MinYear + 1)
				and
				-- more than 2 years, or 2 years at the same school OR change of level of education
				(
					NumYears > 2 OR (NumYears = 2 and NumSchools = 1) OR
					(numYears = 2 AND maxYoE - minYoE = 1 and minEdLevelCode <> maxEdLevelCode)
				)

				then 1 else 0 end
			FROM
			(
			select max(stueYear) MaxYear, min(stueYear) MinYear, max(lvlYear) maxYoE, min(lvlYear) minYoE
			, count(DISTINCT stueYear) NumYears
			, sum(case when stueFrom = 'REP' then 1 else 0 end) NumRepeats
			, count(DISTINCT isnull(schNextSchool,SE.schNo)) NumSchools
			, min(edLevelCode) minEdLevelCode, max(edLevelCode) maxEdLevelCode -- not necessailry the 'minimum' and 'maximum' levels, just want to test for a cross over

			FROM
				StudentEnrolment SE
				INNER JOIN Schools S
					ON SE.schNo = S.schNo
				where stuID in (@sourceID, @targetID)
			) SUB
		end

		-- proceed if there are no year conflicts, and we have confirmation by matching schools
		if @YEarDups = 0 and (@Ignore is null or @SchoolMatches > 0 or @enrolSequence = 1) begin
			begin try
			exec pStudentOps.StudentMerge @targetID, @sourceID
			raiserror('%i: %s %s %s=>%s Merged', 0,1, @rec, @given, @surname, @sourceS, @targetS)
			Select @Merged = @merged + 1
			end try
			begin catch
			declare @e nvarchar(500) = ERROR_MESSAGE()
				raiserror('%i: %s %s %s=>%s ERROR: %s', 10,1, @rec, @given, @surname, @sourceS, @e)
				Select @error = @error+1
			end catch
		end
		else begin
			raiserror('%i: %s %s %s=>%s Enrolment Dups: %i SchoolMatches: %i Enrolment Seq: %i', 0,1, @rec, @given, @surname, @sourceS, @targetS, @YearDups, @SchoolMatches, @enrolSequence)
			Select @ignored = @ignored + 1
		end
	end
	else begin
		raiserror('%i: %s=>%s Student not found - removed in previous merge?', 0,1, @rec, @sourceS, @targetS)
		Select @ignored = @ignored + 1
	end
	Select @rec = @rec + 1, @sourceID = null, @targetID = null
	Select TOP 1 @targetID = SE.stuID			-- TOP 1 not actually necessary, assigning variables from a multirow set will only ever use the first row
	, @sourceID = case SE.stuID when K.dupID then K.stuID when K.stuID then K.dupID end
	, @given = stuGiven, @surname = stuFamilyName
	, @lastYear = stueYear
	from StudentEnrolment SE
	INNER JOIN @K K
		ON SE.stuID in (K.stuID, K.dupID)
		AND recNo = @rec
	ORDER BY SE.stueYear DESC, SE.stuID
end


Select NE NumEnrolmentRecords, count(stuID) NumStudents
from
(
select stuID, count(*) NE
from StudentEnrolment_
GROUP BY stUID
) SUB
GROUP BY NE
ORDER BY NE

if @commit = 1 commit
else begin
	rollback
	raiserror('Rollback - no changes committed',0,1)
end

Select @Matches DuplicatesFound, @found SelectedToProcess, @Merged Merged, @ignored Ignored, @error Errors

END
GO

