SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis / Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	return a single school accreditation
--
-- History Logs:
-- * 12/12/2022, Ghislain Hachey, added Note field to XML output
-- =============================================
CREATE PROCEDURE [pInspectionRead].[SCHOOL_ACCREDITATION_ReadXml]
	-- Add the parameters for the stored procedure here
	@SAID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON    -- Insert statements for procedure here

	Select inspID
	, schNo
	, schName
	, StartDate
	, EndDate
	, InspectionYear
	, InspectedBy
	, inspTypeCode
	, InspectionType
	, InspectionResult
	, SourceId
	, Note
	, InspectionClass
	, Partition
	, pCreateTag
	, pCreateUser
	, pCreateDateTime
	, pEditUser
	, pEditDateTime
	, InspectionContent
	, X.Summary summary
FROM pInspectionRead.SchoolInspections
CROSS APPLY dbo.SummariseSSA(InspectionContent) X
WHERE inspID = @SAID
FOR XML PATH('inspection')
--WHERE inspID = @SAID
--FOR XML PATH('accreditation')
	 --SELECT * from pInspectionRead.SchoolAccreditations
	 --WHERE inspID = @SAID

END
GO

