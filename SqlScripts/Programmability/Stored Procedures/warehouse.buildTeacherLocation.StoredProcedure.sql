SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 11 2016
-- Description:	Collect the school and role of teachers, using appointments if available,
-- confirmed (or contradicted) by TeacherSurvey
-- This procedure is responsible for populating these tables:
-- warehouse.TeacherQual
-- * the qualifications earned by each teacher, and the year in which acquired (or reported)
-- warehouse.TeacherLocation
-- * where each teacher is in a year. This can be determined from the TeacherSurvey or the appointment
-- warehouse.schoolStaffCount
-- * number of teachers at each school, also number of support staff at the school is disaggregated
--	built from warehouse.TeacherLocation, but OMITS records based only on appointment
-- warehouse.TeacherActivitySchool
--   using the 'activity data' on warehouse.TeacherLocation, counts the number of teachers at each school
--   by 'activity'. In particular this is the number of teachers teaching a
--   particular grade. Includes both gross number, and the weighted value (ie a 'Full time Equivalent') which takes
--   into account that a teacher may teach mre than one grade, or perform Admin/Other duties as well as teaching.
--   As above, OMITS records in warehouse.TeacherLocation based only on appointment
-- warehouse.TeacherJobSchool
--   this is a simple tally from the workbook, by school and gender, of the recorded Staff-type and Job title
--   this is derived directly from the census data row image held on TeacherSurvey so by definition only includes those
--   where there is a survey record

-- Two views are built from warehouse.SchoolStaffCount
-- * warehouse.SchoolTeacherCount
-- * warehouse.DistrictTeacherCount
-- These show numbers of teachers, certified and qualified, and number of support staff

-- History Log:
--   * Ghislain Hachey, 24/09/2023: support to include renewed certification records otherwise lost through the min() aggregation when populating [warehouse].[TeacherQual]

-- =============================================
CREATE PROCEDURE [warehouse].[buildTeacherLocation]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS ON;
		DELETE
		FROM warehouse.teacherQual
		WHERE (yr >= @StartFromYear or @StartFromYEar is null)

		print 'warehouse.teacherQual deletes - rows:' + convert(nvarchar(10), @@rowcount)

		INSERT INTO Warehouse.TeacherQual
		( tID, yr, tchQual)
		Select tID
		, min(yr) yr
		, qual
		FROM (
			Select tID
				, svyYear yr
				, tchQual Qual
				FROM TEacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				WHERE tchQual is not null

			UNION
				Select tID
				, svyYear yr
				, tchEdQual
				FROM TEacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				WHERE tchEdQual is not null
			UNION
				Select tID
				, coalesce(trYear, @StartFromYear, 0) yr
				, trQual
				FROM TeacherTraining
				) U
		WHERE (yr >= @StartFromYear or @StartFromYEar is null)
		GROUP BY tID, qual

		-- A quick fix for adding more recent certifications (or anything with a non permanent duration)
		-- that are otherwise lost by the above MIN() aggregation.
		-- Also note that the trYear is not always a good measure of the first year the teacher should be considered as certified *in that school year*
		-- A simple solution would be to introduce some cutoff date after which teachers would not have been considered certified in that
		-- school year but only the next. For example, take a teacher that was certified with an trEffectiveDate of 8 August 2022 and a
		-- trExpiryDate of 8 August 2026. The trYear will be set by the user to 2022 but the teacher was not really considered certified
		-- in school year 2022 (i.e. 2021-22). It would be more correct to say the teacher was certified in school years 2023, 2024, 2025, 2026.
		-- If the certification record came from the survey (above) then there is no need for this distinction but with carefully entered certification
		-- record I think it is fairly easy and important to come up with something a bit more refined as below.

		UNION

		Select tID
			, coalesce(common.SchoolYearInt(trEffectiveDate, common.sysParam('TEACHERCERT_CUTOFFDATE')), trYear, null, 0) yr
			, trQual
		FROM TeacherTraining TT
		INNER JOIN lkpTeacherQual LTQ ON TT.trQual = LTQ.codeCode
		WHERE (codeDurationYears IS NOT NULL AND codeDurationYears != 0) -- Only consider qualifications with a duration

		print 'warehouse.teacherQual inserts - rows:' + convert(nvarchar(10), @@rowcount)

---------------------------------------------------------------------
-- TEACHER LOCATION ---
---------------------------------------------------------------------

-- update of exisitng table

-- there is a significant performane problem here with filtering on svyYear is done directly
-- so lets write a simple table out for it
declare @yrs TABLE
(
	svyYear int
	, svyCensusDate datetime
)

INSERT INTO @yrs
Select svyYear
, svyCensusDate
from Survey
WHERE (svyYear >= @StartFromYear or @StartFromYEar is null)


	DELETE
	from warehouse.TeacherLocation
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
	print 'warehouse.teacherLocation deletes - rows:' + convert(nvarchar(10), @@rowcount)

	INSERT INTO warehouse.TeacherLocation
           (TID
           ,SurveyYear
           ,GenderCode
           ,BirthYear
           ,AgeGroup
           ,ApptSchNo
           ,ApptRole
           ,ApptYear
           ,ApptSchEstimate
           ,SurveySchNo
           ,SurveyRole
           ,SurveyDataYear
-- estimate may be 2 when the estimate is used becuase actual values are suppressed by SchoolSurveyQuality
-- 'Teachers' = 2
           ,Estimate
           ,SurveySector
           ,ISCEDSubClass
           ,SurveySupport
           ,SchNo
           ,Source
           ,Role
           ,Sector
           ,Certified
           ,Qualified
           ,XtraSurvey
		   , TpK
		   , T00
		   , T01
		   , T02
		   , T03
		   , T04
		   , T05
		   , T06
		   , T07
		   , T08
		   , T09
		   , T10
		   , T11
		   , T12
		   , T13
		   , T14
		   , T15
		   , A
		   , X
		   , T

 )
	Select S.TID
	, svyYear SurveyYear
	, tSex GenderCode
	, year(tDoB) BirthYear
	, case
		when svyYear - year(tDoB) >= 60 then '60+'
		when svyYear - year(tDoB) >= 50 then '50-59'
		when svyYear - year(tDoB) >= 40 then '40-49'
		when svyYear - year(tDoB) >= 30 then '30-39'
		when svyYear - year(tDoB) >= 20 then '20-29'
		when svyYear - year(tDoB) >= 0 then '<20'
		else null end AgeGroup
	, ApptSchNo
	, ApptRole
	, ApptYear
	, ApptSchEstimate
	, SurveySchNo
	, SurveyRole
	, SurveyDataYear
	, Estimate
	, SurveySector
	, ISCEDSubClass
	, SurveySupport
	, case
			-- no disagreement between survey and appt ( survey may be estimate)
			when SurveySchNo = ApptSchNo then SurveySchNo
			-- conflict but survey is not estimate - use survet
			when SurveySchNo <> ApptSchNo and Estimate = 0 then SurveySchNo
			-- conflict but survey is estimate and appt is newer than last survey - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear >= SurveyDataYear then ApptSchNo
			-- conflict and survey is estimate but is more recent than the appt - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear < SurveyDataYear then SurveySchNo
			else coalesce(ApptSchNo, SurveySchNo) end SchNo
	, case when SurveySchNo = ApptSchNo and Estimate = 0 then 'C' -- confirmed
			when SurveySchNo = ApptSchNo then 'CE'				-- matching, but survey is estimate
			-- survey contradicts appt - using school ( see rules for SchNo)
			when surveySchNo <> ApptSchNo and Estimate = 0 then 'XS'
			-- conflict but survey is estimate and appt is newer than last survey - so use it
			-- but flag if the appt school returned a survey and the teacher was not on it
			when SurveySchNo <> ApptSchNo and Estimate <> 0 and ApptYear >= SurveyDataYear and ApptSchEstimate = 0 then 'XA?'

			when SurveySchNo <> ApptSchNo and Estimate <> 0 and ApptYear >= SurveyDataYear then 'XA'
			-- conflict but survey is estimate and rolled back to this year from a future year
			-- if an appt is available, we'll use it
			-- again flag if the school did not report the teacher
			when SurveySchNo <> ApptSchNo and Estimate <> 0 and SurveyDataYear > svyYear and apptSchEstimate = 0 then 'XA?'
			when SurveySchNo <> ApptSchNo and Estimate <> 0 and SurveyDataYear > svyYear then 'XA'
			-- conflict and survey is estimate but is more recent than the appt - so use it
			when SurveySchNo <> ApptSchNo and Estimate <> 0 then 'XE'
			-- left with the cases that survey or appt null
			-- ApptSchEstimate flags whether we got a survey from the appt school
			-- if we have not, we can assume the appointment is correct, but unvalidated
			when ApptSchNo is not null and ApptSchEstimate = 0 then 'A?'						--
			-- if we did get the school survey, but this teacher is not on there, and there is not other estimate for this teacher
			-- they are probably not there
			when ApptSchNo is not null then 'A'						--
			when SurveySchNo is not null and Estimate = 0 then 'S'
			when SurveySchNo is not null and Estimate <> 0 then 'E'
			end Source
	, case
			-- no disagreement between survey and appt ( survey may be estimate)
			when SurveyRole = ApptRole then SurveyRole
			-- conflict but survey is not estimate - use survet
			when SurveySchNo <> ApptSchNo and Estimate = 0 then SurveyRole
			-- conflict but survey is estimate and appt is newer than last survey - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear >= SurveyDataYear then ApptRole
			-- conflict but survey is estimate and survey is rolled back - use appt
			when SurveySchNo <> ApptSchNo and Estimate = 1 and SurveyDataYear > svyYear then ApptRole
			-- conflict and survey is estimate but is more recent than the appt - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 then SurveyRole
			else coalesce(ApptRole, SurveyRole) end Role

	, SurveySector				Sector
	, convert(int, 0)			Certified
	, convert(int, 0)			Qualified
	-- record any extra surveys this teacher may have reported in the year
	-- for example, they may be on an actual at their new school, and still on Estimate from last school
	, convert(int, 0)			XtraSurvey
	-- fields for class taught, activity
	-- this is lifted directly from the census
		   , TpK
		   , T00
		   , T01
		   , T02
		   , T03
		   , T04
		   , T05
		   , T06
		   , T07
		   , T08
		   , T09
		   , T10
		   , T11
		   , T12
		   , T13
		   , T14
		   , T15
		   , A
		   , X
		   , T
	FROM
	(
		Select TID
		, svyYear
		, min(ApptSchNo) ApptSchNo
		, min(ApptRole) ApptRole
		, min(ApptYear) ApptYear
		, min(ApptSchEstimate) ApptSchEstimate
		, min(SurveySchNo) SurveySchNo
		, min(SurveyDataYear) SurveyDataYear
		, min(Estimate) Estimate
		, min(SurveyRole) SurveyRole
		, min(SurveySector) SurveySector
		, min(ISCEDSubClass) ISCEDSubClass
		, min(SurveySupport) SurveySupport
		, max(YPK) TPK
		, max(Y00) T00
		, max(Y01) T01
		, max(Y02) T02
		, max(Y03) T03
		, max(Y04) T04
		, max(Y05) T05
		, max(Y06) T06
		, max(Y07) T07
		, max(Y08) T08
		, max(Y09) T09
		, max(Y10) T10
		, max(Y11) T11
		, max(Y12) T12
		, max(Y13) T13
		, max(Y14) T14
		, max(Y15) T15
		, max(A) A
		, max(X) X
		, max(T) T
		FROM
		(
		Select TA.tID
		, SVY.svyYear
		, E.schNo ApptSchNo
		, RG.roleCode ApptRole
		, year(taDate) ApptYear
		, EST.Estimate ApptSchEstimate
		, null SurveySchNo
		, null SurveyDataYear
		, null Estimate
		, null SurveyRole
		, null SurveySector
		, null ISCEDSubClass			-- not an obvious way to get this frin appointment
		, null SurveySupport
		-- grade activity
		, 0 YPK
		, 0 Y00
		, 0 Y01
		, 0 Y02
		, 0 Y03
		, 0 Y04
		, 0 Y05
		, 0 Y06
		, 0 Y07
		, 0 Y08
		, 0 Y09
		, 0 Y10
		, 0 Y11
		, 0 Y12
		, 0 Y13
		, 0 Y14
		, 0 Y15
		, 0 A
		, 0 X
		, 0 T
		from TeacherAppointment TA
			INNER JOIN TeacherIDentity TI
				ON TA.tID = TI.tID
			INNER JOIN @yrs SVY
				ON TA.taDate <= SVY.svyCensusDate
				AND (TA.taEndDate is null or TA.taEndDate >= svyCensusDate)
			INNER JOIN Establishment E
				ON TA.estpNo = E.estpNo
			LEFT JOIN RoleGrades RG
				ON E.estpRoleGrade = RG.rgCode
			LEFT JOIN ESTIMATE_BestSurveyTeachers EST
				ON SVY.svyYEar = LifeYear
				AND E.schNo = EST.schNo
		-- performance is markedly better when this criteria is applied before the UNION

		UNION ALL
		-- we need to be careful oly to get one record for a teacher
		-- if they have changed schools, they may appear on multiple estimates for a given year
		Select tID
			, svyYear
			, null ApptSchNo
			, null ApptRole
			, null ApptYear
			, null ApptSchEstimate
			, schNo
			, SurveyDataYear
			, Estimate
			, SurveyRole
			, SurveySector
			, ISCEDSubClass
			, SurveySupport
			-- grade activity
			, YPK
			, Y00
			, Y01
			, Y02
			, Y03
			, Y04
			, Y05
			, Y06
			, Y07
			, Y08
			, Y09
			, Y10
			, Y11
			, Y12
			, Y13
			, Y14
			, Y15
			-- these 3 rules for calulating T ( unspecified teching) A and X account for various missing data
			-- if no activities are provided ( ie no grade levels, no A or X) then these 3 rules are mutually exclusive
			-- We consider Stff Type and Job Title to infer the A X or T if necessary

			-- admin activity

			, case
				when YPK + Y00 + Y01 + Y02 + Y03 + Y04 + Y05 + Y06 + Y07 + Y08 + Y09 + Y10 + Y11+ Y12 + Y13 + Y14 + Y15  >  0 then A
				when A = 1 then 1
				when X = 1 then 0
				-- noactiities, inferred
				when Staff_Type = 'Teaching Staff' then 0				-- teacher
				when Staff_Type = 'Non Teaching Staff'
						and (Job_Title is null) then 1
				when Staff_Type = 'Non Teaching Staff'
						and (Job_Title like '%Principal%' or Job_Title like '%Teacher%') then 1
				when (Job_Title like '%Principal%' or Job_Title like '%Teacher%') then 0
				when (Job_Title is not null) then 0
				when Staff_Type = 'Non Teaching Staff' then 1

				else 0 -- no acitivities, no Staff_Type, no job type
			end	A
			-- other activity
			, case
				when YPK + Y00 + Y01 + Y02 + Y03 + Y04 + Y05 + Y06 + Y07 + Y08 + Y09 + Y10 + Y11+ Y12 + Y13 + Y14 + Y15  >  0 then X

				when A = 1 then 0
				when X = 1 then 1
				-- inferred
				when Staff_Type = 'Teaching Staff' then 0				-- teacher
				when Staff_Type = 'Non Teaching Staff'
						and (Job_Title is null) then 0
				when Staff_Type = 'Non Teaching Staff'
						and (Job_Title like '%Principal%' or Job_Title like '%Teacher%') then 0
				when (Job_Title like '%Principal%' or Job_Title like '%Teacher%') then 0
				when (Job_Title is not null) then 1
				when Staff_Type = 'Non Teaching Staff' then 0

				else 0 -- no acitivities, no Staff_Type, no job type
			end	X
			, case
				when YPK + Y00 + Y01 + Y02 + Y03 + Y04 + Y05 + Y06 + Y07 + Y08 + Y09 + Y10 + Y11+ Y12 + Y13 + Y14 + Y15  >  0 then 0 -- no unspecified teaching
				when A = 1 then 0
				when X = 1 then 0
				when Staff_Type = 'Teaching Staff' then 1				-- teacher
				when Staff_Type = 'Non Teaching Staff'
						and (Job_Title is null) then 0
				when Staff_Type = 'Non Teaching Staff'
						and (Job_Title like '%Principal%' or Job_Title like '%Teacher%') then 0
				when Staff_Type = 'Non Teaching Staff' then 0
				when (Job_Title like '%Principal%' or Job_Title like '%Teacher%') then 1
				when (Job_Title is not null) then 0
				else 1 -- no acitivities, no Staff_Type, no job type
			end	T

			FROM (
				SELECT TS.tID
				, EST.LifeYEar svyYear
				, EST.schNo
				, EST.bestYear SurveyDataYear
				, EST.Estimate
				, TS.tchRole SurveyRole
				, TS.tchTAM
			-- grade activity converted to 1 or 0
			, case when ltrim(isnull(tcheData.value('(/row/@Pre_K)[1]', 'nvarchar(10)'),'')) > '' then 1 else 0 end YPK
			, case when ltrim(isnull(tcheData.value('(/row/@ECE)[1]', 'nvarchar(10)'),'')) > '' then 1
				-- 6 5 2022
				-- this logic is to deal with kiribati situation where tchMin and tchMax are captured
				-- but there is no tcheData
				when tcheData is null and DL.[Year of Education] = 0 then 1
				when tcheData is null and DLMin.[Year of Education] = 0 then 1

			else 0 end Y00
 			, case when ltrim(isnull(tcheData.value('(/row/@Grade_1)[1]', 'nvarchar(10)'),'')) > '' then 1
				-- 6 5 2022
				-- this logic is to deal with kiribati situation where tchMin and tchMax are captured
				-- but there is no tcheData
				when tcheData is null and DL.[Year of Education] = 1 then 1
				when tcheData is null and DLMin.[Year of Education] = 1 then 1
				else 0 end Y01
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_2)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 2 then 1
				when tcheData is null and DLMin.[Year of Education] = 2 then 1
				else 0 end Y02
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_3)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 3 then 1
				when tcheData is null and DLMin.[Year of Education] = 3 then 1
				else 0 end Y03
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_4)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 4 then 1
				when tcheData is null and DLMin.[Year of Education] = 4 then 1
				else 0 end Y04
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_5)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 5 then 1
				when tcheData is null and DLMin.[Year of Education] = 5 then 1
				else 0 end Y05
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_6)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 6 then 1
				when tcheData is null and DLMin.[Year of Education] = 6 then 1
				else 0 end Y06
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_7)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 7 then 1
				when tcheData is null and DLMin.[Year of Education] = 7 then 1
				else 0 end Y07
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_8)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 8 then 1
				when tcheData is null and DLMin.[Year of Education] = 8 then 1
				else 0 end Y08
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_9)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 9 then 1
				when tcheData is null and DLMin.[Year of Education] = 9 then 1
				else 0 end Y09
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_10)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 10 then 1
				when tcheData is null and DLMin.[Year of Education] = 10 then 1
				else 0 end Y10
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_11)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 11 then 1
				when tcheData is null and DLMin.[Year of Education] = 11 then 1
				else 0 end Y11
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_12)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 12 then 1
				when tcheData is null and DLMin.[Year of Education] = 12 then 1
				else 0 end Y12
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_13)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 13 then 1
				when tcheData is null and DLMin.[Year of Education] = 13 then 1
				else 0 end Y13
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_14)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 14 then 1
				when tcheData is null and DLMin.[Year of Education] = 14 then 1
				else 0 end Y14
			, case when ltrim(isnull(tcheData.value('(/row/@Grade_15)[1]', 'nvarchar(10)'),'')) > '' then 1
				when tcheData is null and DL.[Year of Education] = 15 then 1
				when tcheData is null and DLMin.[Year of Education] = 15 then 1
				else 0 end Y15
			, case when ltrim(isnull(tcheData.value('(/row/@Admin)[1]', 'nvarchar(10)'),'')) > '' then 1
				-- 6 5 2022 for kiribati
				-- if we have tchTam = A or X record admin duty
				when tcheData is null and tchTAM in ('A','M') then 1
				else 0 end A
			, case when ltrim(isnull(tcheData.value('(/row/@Other)[1]', 'nvarchar(10)'),'')) > '' then 1 else 0 end X

			, nullif(ltrim(tcheData.value('(/row/@Job_Title)[1]', 'nvarchar(100)')),'')                          [Job_Title]
			, nullif(ltrim(tcheData.value('(/row/@Staff_Type)[1]', 'nvarchar(100)')),'')                         [Staff_Type]

				, case when TS.tchSupport is null
					then coalesce(TS.tchSector, DL.sectorCode) end SurveySector
				, DL.[ISCED SubClass] ISCEDSubClass
				, TS.tchSupport SurveySupport
				-- the teacher may appear on more than one survey in a year ( one actual + multiple Estimate?)
				-- find the best survey to represent them
				, Row_number() OVER ( Partition by TS.tID, LifeYear ORDER BY Estimate ASC,
					case when bestYear < LifeYear then 0 else 1 end ASC,
					case when bestYear < LifeYear then bestYear * -1 else bestYear end ASC) RowNo
				FROM ESTIMATE_BestSurveyTeachers EST
					INNER JOIN TeacherSurvey TS
						ON EST.bestssID = TS.ssID
					-- added ISCED to here
					LEFT JOIN DimensionLevel DL
						ON coalesce(TS.tchClassMax, TS.tchClass) = DL.LevelCode
					LEFT JOIN DimensionLevel DLMin
						ON TS.tchClass = DLMin.LevelCode
				) TSurveys
			WHERE RowNo = 1
			AND (svyYear >= @StartFromYear or @StartFromYEar is null)
		) U


		GROUP BY tID, svyYEar
	) S
	INNER JOIN TeacherIdentity TI
		ON S.tID = TI.tID
	WHERE coalesce(ApptSchNo, SurveySchNo) is not null


	print 'warehouse.teacherLocation inserts - rows:' + convert(nvarchar(10), @@rowcount)

---------------------------------------------------------------------------------------
-- Sector and ISCED codes, activity
---------------------------------------------------------------------------------------
-- sector and ISCED are already populated using the techer survey class/ class max if these are available

-- try to patch any missing sector values before calculating cert / qual

-- first pass - use the grade levels taught to definiteively get the sector/isced

UPDATE warehouse.TeacherLocation
SET Sector = calcSector.secCode
, ISCEDSubClass = coalesce(ISCEdSubClass,CalcSector.ILSCode)
from warehouse.TeacherLocation

INNER JOIN warehouse.bestSurvey BSS
	ON warehouse.TeacherLocation.SchNo = BSS.schNo
	AND warehouse.TeacherLocation.SurveyYear = BSS.SurveyYear
 INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON DSS.[Survey ID] = BSS.SurveyDimensionID
INNER JOIN
(
-- subquery finds the class level, taught in this school type, that matches the highest
-- year of education taught by the teacher
Select stCode
, lvlYear
, L.ilsCode
, secCode
FROM metaSchoolTypeLevelMap
INNER JOIN lkpLevels L
	ON tlmLevel = L.codecode
) CalcSector
ON CalcSector.stCode = DSS.SchoolTypeCode
AND lvlYear =
case when t15 = 1 then 15
when T14 = 1 then 14
when T13 = 1 then 13
when T12 = 1 then 12
when T11 = 1 then 11
when T10 = 1 then 10
when T09 = 1 then 9
when T08 = 1 then 8
when T07 = 1 then 7
when T06 = 1 then 6
when T05 = 1 then 5
when T04 = 1 then 4
when T03 = 1 then 3
when T02 = 1 then 2
when T01 = 1 then 1
when T00 = 1 then 0
when TPK = 1 then -1
end

print 'warehouse.teacherLocation update sector from grade level - rows:' + convert(nvarchar(10), @@rowcount)
-- second attempt - get the School type of the school in the year
-- If all class levels in that school type have the samee sector, use that sector

UPDATE warehouse.TeacherLocation
SET Sector = coalesce(Sector,CalcSector.SectorCode)
, ISCEDSubClass = CalcSector.ISCEDSubClass
from warehouse.TeacherLocation

INNER JOIN warehouse.bestSurvey BSS
	ON warehouse.TeacherLocation.SchNo = BSS.schNo
	AND warehouse.TeacherLocation.SurveyYear = BSS.SurveyYear
 INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON DSS.[Survey ID] = BSS.SurveyDimensionID
INNER JOIN
(
-- subquery will find if there is a unique sector code for this school type
-- ie all grade levels for the school type have the same sector code,
-- so this can be selected.
-- Note that this is less common with K-12 type schools prevalent
-- If a
Select stCode
, case when min(L.secCode) = max(L.secCode) then max(L.secCode) end SectorCode
, max(L.ilsCode) ISCEDSubClass
FROM metaSchoolTypeLevelMap
INNER JOIN lkpLevels L
	ON tlmLevel = L.codecode
	GROUP BY stCode
) CalcSector
ON CalcSector.stCode = DSS.SchoolTypeCode
WHERE (Sector is null or  warehouse.TeacherLocation.ISCEDSubClass is null)

print 'warehouse.teacherLocation update sector - rows:' + convert(nvarchar(10), @@rowcount)

----------------------------------------------
-- Activities

-- this just picks up any records that have come from an appointment
UPDATE warehouse.TeacherLocation
SET T = 1
WHERE TPK + T00 + T01 + T02 + T03 + T04 + T05 + T06 + T07 + T08 + T09 + T10 + T11+ T12 + T13 + T14 + T15
				+ A + X + T = 0

-- recalculate the total activities for the tescher - this is used for weighting of teacher/pupil ratios
-- also calculate TAMX as a summary - so you can get teaching staff as TAMX in ('T','M')
UPDATE warehouse.TeacherLocation
SET Activities = TPK + T00 + T01 + T02 + T03 + T04 + T05 + T06 + T07 + T08 + T09 + T10 + T11+ T12 + T13 + T14 + T15
				+ A + X + T
, TAMX = case when TPK + T00 + T01 + T02 + T03 + T04 + T05+ T06 + T07 + T08 + T09 + T10 + T11 + T12 + T13 + T14 + T15 + T > 0 then
	case when A + X = 0 then 'T'
	else 'M'
	end
when A = 1 then 'A'
when X = 1 then 'X'
end

----------------------------------------------
-- Qualification
----------------------------------------------
-- now find all the qualifications earned that are prior to a reporting year
-- we'll take these as anything that has appeared on the school survey.
--
-- to get Certified and qual, look at all the qualifications the teacher has in years <= sumarveyyear
-- then check whether any of those make the teacher certified or qualified
-- 28 9 2022 this logic now has to take into account potential expiry of a certificate

-- first figure out if we are differentiating qual cert based on sector - ie are the qual cert records all the same?
declare @defaultSector nvarchar(10)

Select @defaultSector = max(ytqSector)
from SurveyYearTeacherQual

-- 26 6 2020
-- an ongoing porblem has been that SurveyYearTeacherQual has needed to be populated
-- in each year to provide a result. Instead, consider, for each year `Yr`, and for each Sector/Qualification pair
-- the most recent record for that Sector and Qualification prior to Yr
-- In this way, we only need to put records into SurveyYearTeacherQual if the Qualified/Certified Status changes

DECLARE @ytq TABLE
(
yr int,
ytqSector nvarchar(10),
ytqQual nvarchar(50),
ytqQualified int,
ytqCertified int
)
INSERT INTO @ytq
Select Yr
, ytqSector
, ytqQual
, ytqQualified
, ytqCertified
from
(
Select
S.svyYEar Yr
, ytq.*
, row_number() OVER (PARTITION BY S.svyYear, ytqSector, ytqQual ORDER BY ytq.svyYEar DESC) RowNo
from SurveyYearTeacherQual ytq
INNER JOIN Survey s
on ytq.svyYear <= S.svyYEar
) SUB
WHERE RowNo = 1

--Select * from @ytq

-- now update
UPDATE warehouse.TeacherLocation
SET Qualified = BestQual
, Certified = BestCert
FROM warehouse.TeacherLocation
INNER JOIN
(
Select LOC.tID
, LOC.SurveyYear
, LOC.Sector
, max(convert(int,ytqQualified)) BestQual
, max(convert(int,ytqCertified)) BestCert
from @ytq Q
INNER JOIN lkpTeacherQual LKP
	ON LKP.codeCode = Q.ytqQual
INNER JOIN warehouse.TeacherLocation LOC
	ON Q.Yr = LOC.SurveyYear
	AND (
		isnull(Q.ytqSector,'') = isnull(LOC.Sector,'')		-- allow for sector NULL ie not used
		OR
			@defaultSector is null		-- ie there is no sector breakdown; this is really just a simple shortcut
		)
INNER JOIN warehouse.TeacherQual TQ
	ON  LOC.tID = TQ.tID
	AND Q.ytqQual = TQ.tchQual
	AND (
		TQ.yr is null -- those teachers with a qualification without a year are assumed to have been qualified way back
		OR
			TQ.yr <= LOC.SurveyYear
		)
		AND (isnull(LKP.codeDurationYears,0) = 0  -- Duration 0 means a permanent qualification - ie does not expire
		-- the year is within the duration of the certification period
		-- note if there is no year - TQ.yr is null -- then this expression is null so the certification
		-- will be considered expired
		-- also not < not <= here; in other words if DurationYears = 1, the certification will only apply
		-- for the year in which the qualification was obtained
		OR LOC.surveyYear < TQ.yr + LKP.codeDurationYears
		)
GROUP BY LOC.tID, LOC.SurveyYear, LOC.Sector
	) YTQ
ON warehouse.TeacherLocation.tID = YTQ.tID
AND warehouse.TeacherLocation.SurveyYear = YTQ.SurveyYear

print 'warehouse.teacherLocation update certifed / qualifed - rows:' + convert(nvarchar(10), @@rowcount)

-- now pick up those dups
UPDATE warehouse.TeacherLocation
SET XtraSurvey = NumSurveys - 1
FROM warehouse.TeacherLocation
INNER JOIN
(
Select LifeYear, tID
, count(*) NumSurveys
		FROM ESTIMATE_BestSurveyTeachers EST
			INNER JOIN TeacherSurvey TS
				ON EST.bestssID = TS.ssID
GROUP BY LifeYear, tID
HAVING count(*) > 1
) U
ON warehouse.TeacherLocation.tID = U.tID
AND warehouse.TeacherLocation.SurveyYear = U.LifeYear

print 'warehouse.teacherLocation flag dups - rows:' + convert(nvarchar(10), @@rowcount)

---------------------------------------------------------------
-- School Staff Summary
---------------------------------------------------------------

		DELETE
		FROM warehouse.schoolStaffCount
		WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
		print 'warehouse.schoolStaffCount deletes - rows:' + convert(nvarchar(10), @@rowcount)

		INSERT INTO warehouse.SchoolStaffCount
           (SchNo
           ,SurveyYear
           ,GenderCode
           ,AgeGroup
           ,DistrictCode
           ,AuthorityCode
           ,SchoolTypeCode
           ,Sector
           ,ISCEDSubClass
           ,Support
           ,NumStaff
           ,Certified
           ,Qualified
           ,CertQual
           ,Estimate)
		Select TL.SchNo
		, TL.SurveyYear
		, GenderCode
		, AgeGroup
		, [District Code] DistrictCode
		, AuthorityCode
		, SchoolTypeCode
		-- don't record a Sector for support staff
		-- updated to use TAMX - for more detailed breakdown
		, case when TL.TAMX in ('T','M') then Sector end
		, ISCEDSubClass
		-- updated to use TAMX - for more detailed breakdown
		-- but for backward compatiibility, let this field be null for a teacher
		, case when TL.TAMX in ('T','M') then null else TL.TAMX end

		, count(*) NumTeachers
		, sum(Certified) Certified
		, sum(Qualified) Qualified
		, sum(case when Certified = 1 and Qualified = 1 then 1 else 0 end) CertQual
		, sum (case when Source in ('E','XE', 'CE', 'XA', 'XA?') then 1 else 0 end ) Estimate
		From warehouse.TeacherLocation TL
		LEFT JOIN warehouse.bestSurvey BSS
			ON TL.SurveyYear = BSS.SurveyYear
			AND TL.schNo = BSS.schNo
		LEFT JOIN warehouse.dimensionSchoolSurvey DSS
			ON DSS.[Survey Id] = BSS.SurveyDimensionID
		WHERE Source not in ('A','A?')			-- records derived from appointments are omitted
		AND (TL.SurveyYear >= @StartFromYear or @StartFromYEar is null)

		Group BY TL.schNo, TL.SurveyYear, TL.AgeGroup, GenderCode, [District Code], [AuthorityCode], SchooltypeCode, TL.TAMX
		, Sector, ISCEDSubClass, SurveySupport
		print 'warehouse.schoolStaffCount inserts - rows:' + convert(nvarchar(10), @@rowcount)

---------------------------------------------------------------
-- School Teacher Activity
---------------------------------------------------------------
-- this is derived from the teacher grade and 'Admin/Other' data
-- it can be used to produce PTR at single grade levels
-- both gross (ie every teacher teaching the grade at least some of the time)
-- and 'weighted' - ie divded by the number of different classes/activities the teacher has

DELETE from warehouse.TeacherActivitySchool
WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)

print 'warehouse.TeacherActivitySchool deletes - rows:' + convert(nvarchar(10), @@rowcount)

-- use temo table to hold teachers, then enrolments, then collapse these pairs to one record in TeacherActivitySchool


DECLARE @tmpActivity TABLE
(
schNo				nvarchar(50)
, SurveyYear		int NULL
, YoE				int NULL
, GenderCode		nvarchar(1)
, Staff				int NULL
, StaffQ			int NULL
, StaffC			int NULL
, StaffQC			int NULL
, StaffW			decimal(12,6) null
, StaffQ_W			decimal(12,6) null
, StaffC_W			decimal(12,6) null
, StaffQC_W			decimal(12,6) null
, NumStaff			INT null
, Enrol				INT null
)

-- add the teachers)
INSERT INTO @tmpActivity
(
schNo
, SurveyYear
, YoE
, GenderCode
, Staff
, StaffQ
, StaffC
, StaffQC

, StaffW
, StaffQ_W
, StaffC_W
, StaffQC_W

, NumStaff
)

Select TL.schNo
, TL.SurveyYear
, num
, GenderCode
, sum(
	case num
		when -1 then TPK
		when 0 then T00
		when 1 then T01
		when 2 then T02
		when 3 then T03
		when 4 then T04
		when 5 then T05
		when 6 then T06
		when 7 then T07
		when 8 then T08
		when 9 then T09
		when 10 then T10
		when 11 then T11
		when 12 then T12
		when 13 then T13
		when 14 then T14
		when 15 then T15

		when -2 then A
		when -3 then X
		when -4 then T
	end
 ) Staff
 , sum(
	case when Qualified = 1 then
		case num
			when -1 then TPK
			when 0 then T00
			when 1 then T01
			when 2 then T02
			when 3 then T03
			when 4 then T04
			when 5 then T05
			when 6 then T06
			when 7 then T07
			when 8 then T08
			when 9 then T09
			when 10 then T10
			when 11 then T11
			when 12 then T12
			when 13 then T13
			when 14 then T14
			when 15 then T15

			when -2 then A
			when -3 then X
			when -4 then T
		end
	end
 ) StaffQ

  , sum(
	case when Certified = 1 then
		case num
			when -1 then TPK
			when 0 then T00
			when 1 then T01
			when 2 then T02
			when 3 then T03
			when 4 then T04
			when 5 then T05
			when 6 then T06
			when 7 then T07
			when 8 then T08
			when 9 then T09
			when 10 then T10
			when 11 then T11
			when 12 then T12
			when 13 then T13
			when 14 then T14
			when 15 then T15

			when -2 then A
			when -3 then X
			when -4 then T
		end
	end
 ) StaffC
  , sum(
	case when Qualified = 1 and Certified = 1 then
		case num
			when -1 then TPK
			when 0 then T00
			when 1 then T01
			when 2 then T02
			when 3 then T03
			when 4 then T04
			when 5 then T05
			when 6 then T06
			when 7 then T07
			when 8 then T08
			when 9 then T09
			when 10 then T10
			when 11 then T11
			when 12 then T12
			when 13 then T13
			when 14 then T14
			when 15 then T15

			when -2 then A
			when -3 then X
			when -4 then T
		end
	end
 ) StaffQC
 , sum(
	case num
		when -1 then TPK/Activities
		when 0 then T00/Activities
		when 1 then T01/Activities
		when 2 then T02/Activities
		when 3 then T03/Activities
		when 4 then T04/Activities
		when 5 then T05/Activities
		when 6 then T06/Activities
		when 7 then T07/Activities
		when 8 then T08/Activities
		when 9 then T09/Activities
		when 10 then T10/Activities
		when 11 then T11/Activities
		when 12 then T12/Activities
		when 13 then T13/Activities
		when 14 then T14/Activities
		when 15 then T15/Activities

		when -2 then A/Activities
		when -3 then X/Activities
		when -4 then T/Activities

	end
 ) StaffW
 , sum(
	case when Qualified = 1 then
		case num
			when -1 then TPK/Activities
			when 0 then T00/Activities
			when 1 then T01/Activities
			when 2 then T02/Activities
			when 3 then T03/Activities
			when 4 then T04/Activities
			when 5 then T05/Activities
			when 6 then T06/Activities
			when 7 then T07/Activities
			when 8 then T08/Activities
			when 9 then T09/Activities
			when 10 then T10/Activities
			when 11 then T11/Activities
			when 12 then T12/Activities
			when 13 then T13/Activities
			when 14 then T14/Activities
			when 15 then T15/Activities

			when -2 then A/Activities
			when -3 then X/Activities
			when -4 then T/Activities

		end
	end
 ) StaffQ_W
  , sum(
	case when Certified = 1 then
		case num
			when -1 then TPK/Activities
			when 0 then T00/Activities
			when 1 then T01/Activities
			when 2 then T02/Activities
			when 3 then T03/Activities
			when 4 then T04/Activities
			when 5 then T05/Activities
			when 6 then T06/Activities
			when 7 then T07/Activities
			when 8 then T08/Activities
			when 9 then T09/Activities
			when 10 then T10/Activities
			when 11 then T11/Activities
			when 12 then T12/Activities
			when 13 then T13/Activities
			when 14 then T14/Activities
			when 15 then T15/Activities

			when -2 then A/Activities
			when -3 then X/Activities
			when -4 then T/Activities

		end
	end
 ) StaffC_W
  , sum(
	case when Qualified = 1 and Certified = 1 then
		case num
			when -1 then TPK/Activities
			when 0 then T00/Activities
			when 1 then T01/Activities
			when 2 then T02/Activities
			when 3 then T03/Activities
			when 4 then T04/Activities
			when 5 then T05/Activities
			when 6 then T06/Activities
			when 7 then T07/Activities
			when 8 then T08/Activities
			when 9 then T09/Activities
			when 10 then T10/Activities
			when 11 then T11/Activities
			when 12 then T12/Activities
			when 13 then T13/Activities
			when 14 then T14/Activities
			when 15 then T15/Activities

			when -2 then A/Activities
			when -3 then X/Activities
			when -4 then T/Activities

		end
	end
 ) StaffQC_W
 , count(*) NumStaff
 from warehouse.TeacherLocation TL
INNER JOIN metaNumbers N

ON num between -4 and 15


WHERE (TL.SurveyYear >= @StartFromYear or @StartFromYear is null)
	AND Source not in ('A','A?')			-- records derived from appointments are omitted

GROUP BY TL.schNo, TL.surveyYear, num, GenderCode

print 'warehouse.TeacherActivitySchool Tmp inserts (teachers) - rows:' + convert(nvarchar(10), @@rowcount)

-- insert enrolments
INSERT INTO @tmpActivity
(
schNo
, SurveyYear
, YoE
, Enrol
)
SELECT schNo
, SurveyYear
, lvlYear
, sum(Enrol)
FROM warehouse.Enrol E
INNER JOIN lkpLevels L
	ON E.ClassLevel = L.codeCode
GROUP BY schNo
, SurveyYear
, lvlYear

print 'warehouse.TeacherActivitySchool Tmp inserts (enrolment) - rows:' + convert(nvarchar(10), @@rowcount)


INSERT INTO warehouse.TeacherActivitySchool
(
schNo
, SurveyYear
, YoE
, Activity
, Staff
, StaffQ
, StaffC
, StaffQC
, StaffW
, StaffQ_W
, StaffC_W
, StaffQC_W
, NumStaff
, SurveyDimensionID
, Enrol
, GradePTR
, GradePTRW
)
SElect
A.schNo
, A.SurveyYEar
, case YoE
	when -2 then null
	when -3 then null
	when -4 then null
	else YoE
	end YoE

, case YoE
	when -2 then 'Admin'
	when -3 then 'Other'
	when -4 then 'Teacher'
	else levelCode
	end Activity
, sum(Staff)
, sum(StaffQ)
, sum(StaffC)
, sum(StaffQC)
, sum(StaffW)
, sum(StaffQ_W)
, sum(StaffC_W)
, sum(StaffQC_W)
, sum(NumStaff)
, min(SurveyDimensionID)
, sum(Enrol)
, case when isnull(sum(Staff),0) = 0 then null else sum(Enrol)/sum(Staff) end
, case when isnull(sum(StaffW),0) = 0 then null else sum(Enrol)/sum(StaffW) end

FROM @tmpActivity A
 INNER JOIN warehouse.bestSurvey BSS
 ON BSS.schNo = A.schNo
 AND BSS.SurveyYear = A.SurveyYear

	-- we have to deal with the issue of translating a 'Year of Ed' back to a classLevel
	-- to do this reliably , we need to know the school type
	LEFT JOIN ListDefaultPathLevels L
		ON L.YearOfEd = A.YoE

GROUP BY A.schNo, A.SurveyYear, YoE, LevelCode
HAVING sum(staff) > 0 or sum(enrol) > 0

print 'warehouse.TeacherActivitySchool inserts - rows:' + convert(nvarchar(10), @@rowcount)


-- warehouse.TeacherActivity is aggregated up to District, Authority, SchoolTypeCode
DELETE from warehouse.TeacherActivityTable

print 'warehouse.TeacherActivityTable deletes - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.TeacherActivityTable
(
SurveyYear
, AuthorityCode
, DistrictCode
, SchoolTypeCode
, Activity
, Staff
, StaffQ
, StaffC
, StaffQC
, staffW
, StaffQ_W
, StaffC_W
, StaffQC_W
, Enrol
, GradePTR
, GradePTRW
)
Select SurveyYear

, DSS.AuthorityCode
, DSS.[District Code] DistrictCode
, DSS.SchoolTypeCode
, Activity
, sum(Staff) Staff
, sum(StaffQ) StaffQ
, sum(StaffC) StaffC
, sum(StaffQC) StaffQC
, sum(StaffW) StaffW
, sum(StaffQ_W) StaffQ_W
, sum(StaffC_W) StaffC_W
, sum(StaffQC_W) StaffQC_W
, sum(Enrol) Enrol
, case when isnull(sum(Staff),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(Staff))) end GradePTR
, case when isnull(sum(StaffW),0) = 0 then null else  convert(decimal(12,2), sum(Enrol) / convert(float,Sum(StaffW))) end GradePTRW
FROM warehouse.TeacherActivitySchool STA
	INNER JOIN warehouse.dimensionSchoolSurvey DSS
		ON Dss.[Survey ID] = STA.surveyDimensionID
GROUP BY SurveyYear
, DSS.AuthorityCode
, DSS.[District Code]
, DSS.SchoolTypeCode
, Activity
HAVING sum(Staff) > 0 or sum(Enrol) > 0

print 'warehouse.TeacherActivityTable inserts - rows:' + convert(nvarchar(10), @@rowcount)

----------------------------------------------------------------------------------
-- TeacherActiviyWeights
----------------------------------------------------------------------------------

-- we clear and recreate the whole table, since it is derived from another warehouse table (TeacherLocation)
DELETE from warehouse.TeacherActivityWeights

print 'warehouse.TeacherWeight deletes - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.TeacherActivityWeights
(
schNo
,SurveyYear
,GenderCode
,AuthorityGovt
,IscedSubClass
,tID
,Qualified
,Certified
,QualCert
,Activities
,W
,WQ
,WC
,WQC
-- WTeach - 'weight teaching' - is used in Uis reporting
-- it is the weight of this isced over teaching activities ie omitting Admin (A) and Other (X) activities
-- the sum for a single teacher across all ISCEDs is 1; therefore the sum across all teachers is equal to the total
-- number of teachers 'teaching'. These are also the same records in TeacherLocation where TAMX in ('T','M')
-- Note if TAMX = 'T' then WT = W
,WTeach
,WTeachQ
,WTeachC
,WTeachQC
,ActivityDetail
,LevelStr
,SurveyDimensionID
)
SELECT
	schNo
	,SurveyYear
	,GenderCode
	,AuthorityGovtCode
	, IscedSubClass
	,tID
	,Qualified
	,Certified
	,Qualified * Certified
	,Activities
	,W
	,Qualified * W WQ
	,Certified * W WC
	,Qualified * Certified * W WQC
	,WTeach
	,Qualified * WTeach WTeachQ
	,Certified * WTeach WTeachC
	,Qualified * Certified * WTeach  WTeachQC
	,ActivityDetail
	,LevelStr
	,SurveyDimensionID
FROM warehouse.buildTeacherActivityWeightsISCED

print 'warehouse.TeacherActivityWeights inserts - rows:' + convert(nvarchar(10), @@rowcount)

-- A New hire is from october of the year before...
UPDATE warehouse.TeacherActivityWeights
SET NewHire = case when TI.tId is null then 0 else 1 end
FROM warehouse.TeacherActivityWeights
LEFT JOIN TeacherIdentity TI
	ON warehouse.TeacherActivityWeights.tID = TI.tID
	AND year(dateadd(m,3,tDatePSAppointed)) = warehouse.TeacherActivityWeights.SurveyYear

print 'warehouse.TeacherActivityWeights: update newHire - rows:' + convert(nvarchar(10), @@rowcount)


-- InService has a teacher training record in the year
UPDATE warehouse.TeacherActivityWeights
SET Inservice = case when TQ.codeInService is null then 0 else TQ.codeInService end
FROM warehouse.TeacherActivityWeights
LEFT JOIN TeacherTraining TT
	ON warehouse.TeacherActivityWeights.tID = TT.tID
	AND TT.trYear = warehouse.TeacherActivityWeights.SurveyYear
	AND TT.trComplete = 1
-- 8 9 2022 only consider training flagged as InService
LEFT JOIN lkpTeacherQual TQ
	ON TT.trQual = TQ.codeCode


print 'warehouse.TeacherActivityWeights: update Inservice - rows:' + convert(nvarchar(10), @@rowcount)

----------------------------------------------------------------------------------
--- Teacher activity weights by Education Level
----------------------------------------------------------------------------------
-- the logic of assembling the data has been largey pushed into helper view
-- warehouse.buildTeacherActivityWeightsEdLevel
DELETE from warehouse.TeacherActivityWeightsEdLevel

print 'warehouse.TeacherActivityWeightsEdLevel deletes - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.TeacherActivityWeightsEdLevel
(
schNo
,SurveyYear
,GenderCode
,AuthorityGovt
,edLevelCode
,tID
,Qualified
,Certified
,QualCert
,Activities
,W
,WQ
,WC
,WQC
,WTeach
,WTeachQ
,WTeachC
,WTeachQC
,ActivityDetail
,LevelStr
,SurveyDimensionID
)
SELECT
	schNo
	,SurveyYear
	,GenderCode
	,AuthorityGovtCode
	,edLevelCode
	,tID
	,Qualified
	,Certified
	,Qualified * Certified
	,Activities
	,W
	,Qualified * W WQ
	,Certified * W WC
	,Qualified * Certified * W WQC
	,WTeach
	,Qualified * WTeach WTeachQ
	,Certified * WTeach WTeachC
	,Qualified * Certified * WTeach  WTeachQC
	,ActivityDetail
	,LevelStr
	,SurveyDimensionID
FROM warehouse.buildTeacherActivityWeightsEdLevel

print 'warehouse.TeacherActivityWeightsEdLevel inserts - rows:' + convert(nvarchar(10), @@rowcount)

----------------------------------------------------------------------------------
--- Teacher activity weights by Sector
----------------------------------------------------------------------------------
-- the logic of assembling the data has been largey pushed into helper view
-- warehouse.buildTeacherActivityWeightsSector
DELETE from warehouse.TeacherActivityWeightsSector

print 'warehouse.TeacherActivityWeightsSector deletes - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.TeacherActivityWeightsSector
(
schNo
,SurveyYear
,GenderCode
,AuthorityGovt
,SectorCode
,tID
,Qualified
,Certified
,QualCert
,Activities
,W
,WQ
,WC
,WQC
,WTeach
,WTeachQ
,WTeachC
,WTeachQC
,ActivityDetail
,LevelStr
,SurveyDimensionID
)
SELECT
	schNo
	,SurveyYear
	,GenderCode
	,AuthorityGovtCode
	,SectorCode
	,tID
	,Qualified
	,Certified
	,Qualified * Certified
	,Activities
	,W
	,Qualified * W WQ
	,Certified * W WC
	,Qualified * Certified * W WQC
	,WTeach
	,Qualified * WTeach WTeachQ
	,Certified * WTeach WTeachC
	,Qualified * Certified * WTeach  WTeachQC
	,ActivityDetail
	,LevelStr
	,SurveyDimensionID
FROM warehouse.buildTeacherActivityWeightsSector

print 'warehouse.TeacherActivityWeightsSector inserts - rows:' + convert(nvarchar(10), @@rowcount)
---------------------------------------------------------------
-- Teacher Jobs
---------------------------------------------------------------
-- this provides a simple tally
DELETE FROM warehouse.TeacherJobSchool
WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)

print 'warehouse.TeacherJobSchool deletes - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.TeacherJobSchool
(
schNo
, GenderCode
, SurveyYear
, JobTitle
, StaffType
, TeacherType
, surveyDimensionID
,  NumStaff
)
Select SUB.schNo
, tSex GenderCode
, svyYear SurveyYear
, JobTitle
, StaffType
, TeacherType
, BSS.surveyDimensionID
, count(*) NumStaff

from
(
Select schNo
, tSex
, svyYear
, nullif(ltrim(tcheData.value('(/row/@Job_Title)[1]', 'nvarchar(100)')),'')                          [JobTitle]
, nullif(ltrim(tcheData.value('(/row/@Staff_Type)[1]', 'nvarchar(100)')),'')                         [StaffType]
, nullif(ltrim(tcheData.value('(/row/@Teacher-Type)[1]', 'nvarchar(100)')),'')                         [TeacherType]
from pTeacherRead.TeacherSurveyV
WHERE (svyYear >= @StartFromYear or @StartFromYEar is null)
) SUB
 INNER JOIN warehouse.bestSurvey BSS
 ON BSS.schNo = SUB.schNo
 AND BSS.SurveyYear = SUB.svyYear

GROUP BY
SUB.schNo
, svyYear
, tSex
, JobTitle
, StaffType
, TeacherType
, surveyDimensionID

print 'warehouse.TeacherJobSchool inserts - rows:' + convert(nvarchar(10), @@rowcount)


DELETE FROM warehouse.TeacherJobTable
WHERE ( SurveyYear > =@StartFromYear or @StartFromYear is null)

print 'warehouse.TeacherJobTable deletes - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.TeacherJobTable
(
SurveyYear
, AuthorityCode
, DistrictCode
, SchoolTypeCode
, GenderCode
, JobTitle
, StaffType
, TeacherType
,  NumStaff
)
Select SurveyYear

, DSS.AuthorityCode
, DSS.[District Code] DistrictCode
, DSS.SchoolTypeCode
, GenderCode
, JobTitle
, StaffType
, TeacherType
, sum(NumStaff)

FROM warehouse.TeacherJobSchool TJS
	INNER JOIN warehouse.dimensionSchoolSurvey DSS
		ON Dss.[Survey ID] = TJS.surveyDimensionID
WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
GROUP BY SurveyYear
, DSS.AuthorityCode
, DSS.[District Code]
, DSS.SchoolTypeCode
, GenderCode
, JobTitle
, StaffType
, TeacherType
HAVING sum(NumStaff) > 0

print 'warehouse.TeacherJobTable inserts - rows:' + convert(nvarchar(10), @@rowcount)
END
GO

