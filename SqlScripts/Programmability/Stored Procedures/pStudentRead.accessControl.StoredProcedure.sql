SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 6 2020
-- Description:	check access control for Student and special ed
-- the business rule is: the user may view the teacher
-- if student has EVER been enrolled at any school to which the user has access
-- to which the user has access
-- =============================================
CREATE PROCEDURE [pStudentRead].[accessControl]
	-- Add the parameters for the stored procedure here
	@studentID uniqueidentifier
	, @district nvarchar(10) = null
	, @authority nvarchar(20) = null
	, @userSchool nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @count int

	Select @count = count(*)
	FROM
	(
	SELECT DISTINCT stuID
		FROM StudentEnrolment_ SE
			INNER JOIN Schools S
				ON SE.schNo = S.schNo
			INNER JOIN lkpIslands I
				ON S.iCode = I.iCode
		WHERE (schAuth = @authority or @authority is null)
			AND (I.iGroup = @district or @district is null)
			AND (S.schNo = @userSchool  or @userSchool is null)
			AND (SE.stuID = @studentID)

	) S

	if @count = 0 begin
		Raiserror('Forbidden', 16,1);
	end

END
GO

