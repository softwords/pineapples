SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 31 5 2010
-- Description:	The GRand Totals and totals by authority for a planned establishment. Incudes Teaching Positions, allowances, and Supernumeraries
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[PlannedEstablishmentGrandTotals]
	-- Add the parameters for the stored procedure here
	@EstYear int = 0,
	@UseAuthorityDate bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @AsAtDate datetime		-- date for reporting the current position
	declare @NumPointIncrements int	-- number of points to increment current positions when estimating future salary
	declare @AdjustNominalSalaryPoint int
	declare @RetainUnderPays int		-- if this flag is on, positions currently underpaid will be assumed to stay underpaid

	-- get the parameter values set up on the establishment control


	-- temp tables bring together the 2 main outputs - school positions and supers
	-- for school positions
	-- corresponds to the output from PlannedEstablishmentSummary


declare @POS TABLE
(
	estAuth nvarchar(10)
	, authName nvarchar(100)
	, CurrentSchools int
	, NumSchools int
	, CurrentPositions int
	, CurrentFilled int
	, Quota int
	, FinalPositions int
	, FinalCost money
	, TotalAllowancePayable money

)

declare @ScopePOS TABLE
(
	scope nvarchar(1)
	, estAuth nvarchar(10)
	, authName nvarchar(100)
	, CurrentPositions int NULL
	, CurrentFilled int NULL
	, Quota int NULL
	, FinalPositions int NULL
	, FinalCost money NULL

)
		declare @Summary int
		Select @Summary = 1

		SELECT
			@AsAtDate = case @UseAuthorityDate when 0 then estcSnapshotDate else estcAuthorityDate end
			, @NumPointIncrements = estcIncrementPoints
			, @AdjustNominalSalaryPoint = estcNominalPointAdjust
			, @RetainUnderPays = estcRetainUnderpays
		FROM
			EstablishmentControl
		WHERE
			estcYear = @EstYear


		INSERT INTO @POS
		EXEC [pEstablishmentRead].[PlannedEstablishmentSummary] 1, @estYear
					, @AsAtDate, @NumPointIncrements, @AdjustNominalSalaryPoint
					, @RetainUnderPays


		INSERT INTO @ScopePOS
		EXEC [pEstablishmentRead].[PlannedEstablishmentSupersYear]  @estYear
					, @UseAuthorityDate


--------------------------------------------------------------------------
-- now join these together
-- since its only little data (max rows = number of authorities
-- do this by two outer joins from the Authorities

-- This query is the pinnacle of the EstablishmentPlanning subsystem

/***********************************************************************/
Select authCode
	, Auth.authName
	, POS.CurrentSchools
	, POS.NumSchools
	, POS.CurrentPositions
	, POS.CurrentFilled
	, POS.Quota
	, POS.FinalPositions
	, POS.FinalCost
	, POS.TotalAllowancePayable

	, ScopePOS.CurrentPositions ScopeCurrentPositions
	, ScopePOS.CurrentFilled  ScopeCurrentFilled
	, ScopePOS.Quota ScopeQuota
	, ScopePOS.FinalPositions ScopeFinalPositions
	, ScopePOS.FinalCost ScopeFinalCost
	, isnull(POS.Quota,0)
		+ isnull(ScopePOS.Quota,0) AuthorityQuota
	, isnull(POS.FinalPositions,0)
		+ isnull(ScopePOS.FinalPositions,0) AuthorityPositions
	, isnull(POS.FinalCost,0)
		+ isnull(POS.TotalAllowancePayable,0)
		+ isnull(ScopePOS.FinalCost,0) AuthorityTotalCost
INTO #tmpGrandTotals
FROM Authorities AUTH
	LEFT JOIN @POS POS
		ON AUTH.authCode = POS.estAuth
	LEFT JOIN @ScopePOS ScopePOS	-- note this works becuase we cannot have scopepos without an authority
		ON Auth.authCode = ScopePOS.estAuth

SELECT * from #tmpGrandTotals
WHERE CurrentSchools > 0 or NumSchools > 0

SELECT
	sum(CurrentSchools) CurrentSchools
	, sum(NumSchools) NumSchools
	, sum(CurrentPositions)  CurrentPositions
	, sum(CurrentFilled) CurrentFilled
	, sum(Quota) Quota
	, sum(FinalPositions)  FinalPositions
	, sum(FinalCost) FinalCost
	, sum(TotalAllowancePayable) TotalAllowancePayable

	, sum(ScopeCurrentPositions) ScopeCurrentPositions
	, sum(ScopeCurrentFilled) ScopeCurrentFilled
	, sum(ScopeQuota) ScopeQuota
	, sum(ScopeFinalPositions) ScopeFinalPositions
	, sum(ScopeFinalCost) ScopeFinalCost
	, sum(AuthorityQuota) TotalQuota
	, sum(AuthorityPositions) TotalPositions

	, sum(AuthorityTotalCost) TotalCost    -- this is the final cost of the whole establishment

FROM #tmpGrandTotals
HAVING sum(CurrentSchools) > 0 or sum(NumSchools) > 0


DROP TABLE #tmpGrandTotals

/***********************************************************************/

END
GO

