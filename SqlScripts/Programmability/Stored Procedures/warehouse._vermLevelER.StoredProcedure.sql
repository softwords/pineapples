SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 12 2010
-- Description:	Nation-level summary of enrolment ratios at the class level
-- , rather than by level of education
-- Output is Xml, creates the levelERs node in the VERMPAF
-- 30/10/2021 -- support diistrict
-- =============================================
CREATE PROCEDURE [warehouse].[_vermLevelER]
	-- Add the parameters for the stored procedure here
	@DistrictCode nvarchar(10) = null
	, @SendASXML int = 0
	, @xmlOut xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @schoolNo nvarchar(50)
declare @SurveyYear int

declare @checkPop int		-- number of population models
declare @popModCode nvarchar(10)			-- the pop model we will use
declare @popModName nvarchar(50)
declare @popModDefault int

Select @checkPop = count(*)
, @popModCode = min(popModCode)
From PopulationModel


if (@checkPop=0) begin
	raiserror('There is no Population Model to use. ',
				16,10)
end

if (@checkPop > 1) begin
	declare @checkEFA int
	Select @checkEFA = count(*)

	-- SRSI0045 20 9 2010 get the one that is the default
	, @popModCode = min(popModCode)
	From PopulationModel

	WHERE popModEFA = 1

	if (@checkEFA <> 1) begin
		raiserror('You must specify exactly one Population Model to use for EFA reporting. ',
					16,10)
	end
end


IF @SendAsXML = 0

	Select *
	from warehouse.enrolmentRatiosByYearOfEd(@DistrictCode)

if @SendASXML <> 0  begin


   declare @XML xml
   declare @XML2 xml

   if (@DistrictCode is null) begin
	   SELECT @XML =
	   (
	   Select
	   SurveyYear		[@year]
	   , OfficialAge		[@officialAge]
	   , YearOfEd		[@yearOfEd]
	   , ClassLevel	[@levelCode]
	   , popM				--[pop/@M]
	   , popF				--[pop/@F]
	   , pop				--[pop/@T]
	   , enrolM				--[enrol/@M]
	   , enrolF				--[enrol/@F]
	   , enrol				--[enrol/@T]
	   , nEnrolM			--[nEnrol/@M]
	   , nEnrolF			--[nEnrol/@F]
	   , nEnrol				--[nEnrol/@T]
	   , repM				--[rep/@M]
	   , repF				--[rep/@F]
	   , rep				--[rep/@T]
	   , nRepM				--[nRep/@M]
	   , nRepF				--[nRep/@F]
	   , nRep				--[nRep/@T]

	   , psaM
	   , psaF
	   , psa

	   , intakeM			--[intake/@M]
	   , intakeF			--[intake/@F]
	   , intake				--[intake/@T]
	   , nIntakeM			--[nIntake/@M]
	   , nIntakeF			--[nIntake/@F]
	   , nIntake			--[nIntake/@T]

	   , cast(psaM as int)								psaM	--[psa/@M]
	   , cast(psaF as int)								psaF	--[psa/@F]
	   , cast(psa as int)								psa		--[psa/@T]

	   , convert(decimal(8,5),isnull(enrolM,0)/cast(popM as decimal(13,5)))		gerM	--[ger/@M]
	   , convert(decimal(8,5),isnull(enrolF,0)/cast(popF as decimal(13,5)))		gerF	--[ger/@F]
	   , convert(decimal(8,5),isnull(enrol,0)/cast(pop as decimal(13,5)))		ger		--[ger/@T]
	   , convert(decimal(8,5),isnull(nEnrolM,0)/cast(popM as decimal(13,5)))	nerM	--[ner/@M]
	   , convert(decimal(8,5),isnull(nEnrolF,0)/cast(popF as decimal(13,5)))	nerF	--[ner/@F]
	   , convert(decimal(8,5),isnull(nEnrol,0)/cast(pop as decimal(13,5)))		ner		--[ner/@T]

	   , convert(decimal(8,5),isnull(intakeM,0)/cast(popM as decimal(13,5)))	girM	--[gir/@M]
	   , convert(decimal(8,5),isnull(intakeF,0)/cast(popF as decimal(13,5)))	girF	--[gir/@F]
	   , convert(decimal(8,5),isnull(intake,0)/cast(pop as decimal(13,5)))		gir		--[gir/@T]
	   , convert(decimal(8,5),isnull(nIntakeM,0)/cast(popM as decimal(13,5)))	nirM	--[nir/@M]
	   , convert(decimal(8,5),isnull(nIntakeF,0)/cast(popF as decimal(13,5)))	nirF	--[nir/@F]
	   , convert(decimal(8,5),isnull(nIntake,0)/cast(pop as decimal(13,5)))		nir		--[nir/@T]

	   , case when YearOfEd = 1 then
				case when isnull(nIntakeM,0) = 0 then 0 else cast(psaM/ IntakeM  as decimal(18,5)) end
			else null end								psaPercM	--[psaPerc/@M]

	   , case when YearOfEd = 1 then
				case when isnull(nIntakeF,0) = 0 then 0 else cast(psaF / IntakeF as decimal(8,5)) end
			else null end								psaPercF	--[psaPerc/@F]

		, case when YearOfEd = 1 then
				case when isnull(nIntake,0) = 0 then 0 else cast(psa / Intake as decimal(8,5)) end
			else null end 								psaPerc		--[psaPerc/@T]
	   FROM
			warehouse.classLevelER
 		ORDER BY SurveyYear, YearOfEd
		FOR XML PATH('LevelER')
		)
	end -- national version @DisitrictCode is null

	if (@DistrictCode is not null) begin

		   SELECT @XML =
	   (
	   Select
	   SurveyYear		[@year]
	   , OfficialAge		[@officialAge]
	   , YearOfEd		[@yearOfEd]
	   , ClassLevel	[@levelCode]
	   , popM				--[pop/@M]
	   , popF				--[pop/@F]
	   , pop				--[pop/@T]
	   , enrolM				--[enrol/@M]
	   , enrolF				--[enrol/@F]
	   , enrol				--[enrol/@T]
	   , nEnrolM			--[nEnrol/@M]
	   , nEnrolF			--[nEnrol/@F]
	   , nEnrol				--[nEnrol/@T]
	   , repM				--[rep/@M]
	   , repF				--[rep/@F]
	   , rep				--[rep/@T]
	   , nRepM				--[nRep/@M]
	   , nRepF				--[nRep/@F]
	   , nRep				--[nRep/@T]

	   , psaM
	   , psaF
	   , psa

	   , intakeM			--[intake/@M]
	   , intakeF			--[intake/@F]
	   , intake				--[intake/@T]
	   , nIntakeM			--[nIntake/@M]
	   , nIntakeF			--[nIntake/@F]
	   , nIntake			--[nIntake/@T]

	   , cast(psaM as int)								psaM	--[psa/@M]
	   , cast(psaF as int)								psaF	--[psa/@F]
	   , cast(psa as int)								psa		--[psa/@T]

	   , convert(decimal(8,5),isnull(enrolM,0)/cast(popM as decimal(13,5)))		gerM	--[ger/@M]
	   , convert(decimal(8,5),isnull(enrolF,0)/cast(popF as decimal(13,5)))		gerF	--[ger/@F]
	   , convert(decimal(8,5),isnull(enrol,0)/cast(pop as decimal(13,5)))		ger		--[ger/@T]
	   , convert(decimal(8,5),isnull(nEnrolM,0)/cast(popM as decimal(13,5)))	nerM	--[ner/@M]
	   , convert(decimal(8,5),isnull(nEnrolF,0)/cast(popF as decimal(13,5)))	nerF	--[ner/@F]
	   , convert(decimal(8,5),isnull(nEnrol,0)/cast(pop as decimal(13,5)))		ner		--[ner/@T]

	   , convert(decimal(8,5),isnull(intakeM,0)/cast(popM as decimal(13,5)))	girM	--[gir/@M]
	   , convert(decimal(8,5),isnull(intakeF,0)/cast(popF as decimal(13,5)))	girF	--[gir/@F]
	   , convert(decimal(8,5),isnull(intake,0)/cast(pop as decimal(13,5)))		gir		--[gir/@T]
	   , convert(decimal(8,5),isnull(nIntakeM,0)/cast(popM as decimal(13,5)))	nirM	--[nir/@M]
	   , convert(decimal(8,5),isnull(nIntakeF,0)/cast(popF as decimal(13,5)))	nirF	--[nir/@F]
	   , convert(decimal(8,5),isnull(nIntake,0)/cast(pop as decimal(13,5)))		nir		--[nir/@T]

	   , case when YearOfEd = 1 then
				case when isnull(nIntakeM,0) = 0 then 0 else cast(psaM/ IntakeM  as decimal(18,5)) end
			else null end								psaPercM	--[psaPerc/@M]

	   , case when YearOfEd = 1 then
				case when isnull(nIntakeF,0) = 0 then 0 else cast(psaF / IntakeF as decimal(8,5)) end
			else null end								psaPercF	--[psaPerc/@F]

		, case when YearOfEd = 1 then
				case when isnull(nIntake,0) = 0 then 0 else cast(psa / Intake as decimal(8,5)) end
			else null end 								psaPerc		--[psaPerc/@T]
	   FROM
			warehouse.classLevelERDistrict
		WHERE @DistrictCode = districtCode
 		ORDER BY SurveyYear, YearOfEd
		FOR XML PATH('LevelER')
		)

	end -- end district version


	SElect @xmlOut =
		(Select @xml
			FOR XML PATH('LevelERs')
		)
	-- send the xml as the result set
	if @SendAsXML = 1
		SELECT @xmlOut LevelNers
end

END
GO

