SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [audit].[xfdfError]
	-- Add the parameters for the stored procedure here
	@ssID int
	, @error nvarchar(1000)
	, @section nvarchar(100)
	, @data nvarchar(50)= null
	, @row nvarchar(20)= null
	with execute as 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO audit.xfdfAudit
	(ssID, svyYear, schNo, sxaDate, sxaUser, sxaSection,
		sxaErrorText, sxaErrorFlag,
		sxaData, sxaRow
	)
	 Select ssID, svyYear, schNo,
		getdate(), original_login(),
		@section,
		@error, 2,
		 @data, @row
	FROM SchoolSurvey
	WHERE ssID = @ssID

END
GO
GRANT EXECUTE ON [audit].[xfdfError] TO [pSurveyWrite] AS [pineapples]
GO

