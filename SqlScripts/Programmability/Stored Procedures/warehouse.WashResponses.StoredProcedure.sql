SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 1 2020
-- Description:
--
--
--
--
-- =============================================
CREATE PROCEDURE [warehouse].[WashResponses]
	-- Add the parameters for the stored procedure here

	@question nvarchar(20),		-- the question to analyse
	@BestUpTo int = null,	-- best inspection up to and including this year

	-- adding district and authority in order to enforce security on these
	@InspYear int = null,
	@School nvarchar(50) = null,
	@SchoolNo nvarchar(50) = null,
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@AuthorityGroup nvarchar(10) = null,
	@SchoolType nvarchar(10) = null,

	@xmlFilter xml = null

AS
BEGIN


-- first return the results, best up the given year

Select D.*
FROM warehouse.washdetail D
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON D.SurveyDimensionID = DSS.[Survey ID]
WHERE question = @question
AND (InspectionYear = @InspYear OR @InspYear IS NULL)

AND (DSS.[School Name] like '%' +  @School + '%' OR @School IS NULL)
AND (D.schNo = @SchoolNo OR @SchoolNo IS NULL)
AND (DSS.AuthorityCode = @Authority OR @authority is null)
AND (DSS.AuthorityGroupCode = @AuthorityGroup OR @authorityGroup is null)
AND (DSS.[District Code] = @District OR @District is null)
AND (DSS.SchoolTypeCode = @SchoolType or @SchoolType is null)
AND (@BestUpTo is null
	OR (D.inspID in (Select inspID from warehouse.BestInspection WHERE SurveyYear = @BestUpTo))
)

-- also, return the set of all items for the question
	Select item from
	(
	Select v.value('../id[1]', 'nvarchar(100)') id
	,  v.value('.', 'nvarchar(100)') item
	from lkpInspectionTypes
	CROSS APPLY intyTemplate.nodes('//question/item') as V(v)
	WHERE intyCode = 'WASH'
	) SUB
	where id = @question
END
GO

