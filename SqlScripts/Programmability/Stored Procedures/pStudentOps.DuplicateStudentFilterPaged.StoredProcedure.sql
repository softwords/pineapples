SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Edit date: 11 9 2020
-- Description:
-- change History:
--			21 9 2022 added support for Enrollments View Mode
-- =============================================
CREATE PROCEDURE [pStudentOps].[DuplicateStudentFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters
	 @MatchGiven int = NULL
	, @MatchSurname int = NULL
	, @MatchCard int = NULL
	, @MatchDoB int = null
	, @MatchGender int = null
	, @Given nvarchar(100) = null
	, @Surname nvarchar(100) = null
	, @District nvarchar(5) = null
	, @XmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		stuID uniqueidentifier
		, DupID uniqueidentifier
		, recNo int PRIMARY KEY
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pStudentOps.DuplicateStudentFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	 @MatchGiven
	, @MatchSurname
	, @MatchCard
	, @MatchDoB
	, @MatchGender
	, @Given
	, @Surname
	, @District
	, @xmlFilter
-------------------------------------
-- return results

if @ColumnSet = 0 begin
	Select S.stuID
	, S.stuNamePrefix
	, S.stuGiven
	, S.stuMiddleNames
	, S.stuFamilyName
	, S.stuNameSuffix
	, S.stuCardID
	, S.stuDoB
	, S.stuGender
	, S.stuEthnicity
	-- second copy
	, DUP.stuID				dupID
	, DUP.stuNamePrefix		dupNamePrefix
	, DUP.stuGiven			dupGiven
	, DUP.stuMiddleNames	dupMiddleNames
	, DUP.stuFamilyName		dupFamilyName
	, DUP.stuNameSuffix		dupNameSuffix
	, DUP.stuCardID			dupCardID
	, DUP.stuDoB			dupDoB
	, DUP.stuGender			dupGender
	, DUP.stuEthnicity		dupEthnicity
	FROM Student_ S
		INNER JOIN @Keys AS K
			ON S.stuID = K.stuID
		INNER JOIN Student_ DUP
			ON DUP.stuID = K.DupID
		ORDER BY K.RecNo
end


if (@ColumnSet = 1) begin
	declare @T TABLE
	(
	stuID uniqueidentifier
	, dupID uniqueidentifier
	, RecNo int
	, stuMaxYear int
	, stuMinYear int
	, dupMaxYear int
	, dupMinYear int
	, stuMaxSchNo nvarchar(50)  -- school on last enrolment record
	, stuMinSchNo nvarchar(50)  -- school on first enrolment record
	, dupMaxSchNo nvarchar(50)
	, dupMinSchNo nvarchar(50)
	, stuMaxClassLevel nvarchar(50)  -- class level on last enrolment record
	, stuMinClassLevel nvarchar(50)  -- class level on first enrolment record
	, dupMaxClassLevel nvarchar(50)
	, dupMinClassLevel nvarchar(50)

	, countConflictYears int
	)

	INSERT INTO @t
	(stuID, dupID, recNo)
	Select K.stuID, K.dupID, K.recNo
	FROM @Keys K

	UPDATE @t
		SET stuMaxYear = MaxYr
		, stuMinYear = MinYr
		FROM @t
		INNER JOIN
		(
		Select stUID , max(stueYear) maxyr, min(stueYear) minYr
		From StudentEnrolment_ SE
		GROUP BY stuID
		) SS
		ON [@t].stuID = SS.stuID

	UPDATE @t
		SET dupMaxYear = MaxYr
		, dupMinYear = MinYr
		FROM @t
		INNER JOIN
		(
		Select stUID , max(stueYear) maxyr, min(stueYear) minYr
		From StudentEnrolment_ SE
		GROUP BY stuID
		) SS
		ON [@t].dupID = SS.stuID

	-- get the first and last school for stu

	UPDATE @t
		SET stuMinSchNo = schNo
		, stuMinClassLevel = stueClass

		FROM @t
		INNER JOIN
		StudentEnrolment_ SE
		ON [@t].stuID = SE.stuID
		AND SE.stueYear = stuMinYear
	UPDATE @t
		SET stuMaxSchNo = schNo
		, stuMaxClassLevel = stueClass
		FROM @t
		INNER JOIN
		StudentEnrolment_ SE
		ON [@t].stuID = SE.stuID
		AND SE.stueYear = stuMaxYear

	UPDATE @t
		SET dupMinSchNo = schNo
		,  dupMinClassLevel = stueClass

		FROM @t
		INNER JOIN
		StudentEnrolment_ SE
		ON [@t].dupID = SE.stuID
		AND SE.stueYear = dupMinYear

	UPDATE @t
		SET dupMaxSchNo = schNo
		, dupMaxClassLevel = stueClass
		FROM @t
		INNER JOIN
		StudentEnrolment_ SE
		ON [@t].dupID = SE.stuID
		AND SE.stueYear = dupMaxYear

		-- look for conflict years in enrolment histories
	UPDATE @t
		set CountConflictYears = NumDups
		FROM
		@t
		INNER JOIN
		(
		Select recNo, count(stueYear) NumDups
		FROM
		(
		Select recNo, stueYear, count(*) NumStudents
		FROM StudentEnrolment_  SE
		INNER JOIN @t T
		ON SE.stuID in (T.stuID, T.dupID)
		group by recNo, stueYear
		) SUB
		WHERE numStudents > 1
		group by recNo
		) SUB2
		ON [@t].recNo = SUB2.recNo


		Select S.stuID
		, S.stuNamePrefix
		, S.stuGiven
		, S.stuMiddleNames
		, S.stuFamilyName
		, S.stuNameSuffix
		, S.stuCardID
		, S.stuDoB
		, S.stuGender
		-- second copy
		, DUP.stuID				dupID
		, DUP.stuNamePrefix		dupNamePrefix
		, DUP.stuGiven			dupGiven
		, DUP.stuMiddleNames	dupMiddleNames
		, DUP.stuFamilyName		dupFamilyName
		, DUP.stuNameSuffix		dupNameSuffix
		, DUP.stuCardID			dupCardID
		, DUP.stuDoB			dupDoB
		, DUP.stuGender			dupGender
		, T.stuMinYear
		, T.stuMaxYear
		, T.dupMinYear
		, T.dupMaxYear
		, T.stuMinSchNo
		, T.stuMaxSchNo
		, T.dupMinSchNo
		, T.dupMaxSchNo
		, T.stuMinClassLevel
		, T.stuMaxClassLevel
		, T.dupMinClassLevel
		, T.dupMaxClassLevel

		, T.countConflictYears
		FROM Student_ S
			INNER JOIN @T AS T
				ON S.stuID = T.stuID
			INNER JOIN Student_ DUP
				ON DUP.stuID = T.DupID
		ORDER BY T.RecNo

end
-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

