SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Procedure

-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 3 2021
-- Description:	Filter of scholarships
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SurveyControlFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0


--filter parameters
	, @SurveyYear int = null
	, @District nvarchar(10) = null
	, @Island nvarchar(10)  = null
	, @SchoolType nvarchar(10) = null
	, @Authority nvarchar(10)= null
	, @FileLocation nvarchar(30)= null
	, @Status int = -1


AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int


	INSERT INTO @keys
	EXEC pSurveyRead.SurveyControlFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

	, @SurveyYear
	, @District
	, @Island
	, @SchoolType
	, @Authority
	, @FileLocation
	, @Status


--- column sets ----

if @Columnset = 1 begin
	Select * from
	pSurveyRead.SurveyControl SC
	INNER JOIN @keys K
		ON SC.syID = K.ID
	ORDER BY recNo
end

if @Columnset = 2 begin

	Select count(saReceived) as Received
	, count(SchNo) as Total
	, count(saSent) as Sent
	, count(saEntered) as Entered
	, sum(CASE WHEN (saAudit <> 0) then 1 else 0 end) as AuditReq
	, count(saAuditDate) as Auditcomplete
	, sum(case when (saCollected <= svyCollectionDate) then 1 else 0 end) as CollectedOnTime
	, sum(case when (saReceived <= saReceivedTarget) then 1 else 0 end) as ReceivedOnTime
	, sum(case when (saAuditResult = 'F') then 1 else 0 end) as AuditFail
	--, sum(Enrol) Enrol
	--, sum(EnrolM) EnrolM
	--, sum(EnrolF) EnrolF
	--, sum(TeacherSurveyCount) TeacherSurveyCount
	--, sum(ClassroomCount) ClassRoomCount
	FROM pSurveyRead.SurveyControl SC
	INNER JOIN @keys K
		ON SC.syID = K.ID
end

--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

