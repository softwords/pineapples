SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017 / 21 8 2019
-- Description: Filter accreditions in the warehouse
-- This is designed for future uses in drilling down into dashboard
-- number to see the accreditations contributing to the total
-- Derived from [pSurveyRead].[SchoolAccreditationFilterIDs]
--  but using warehouse version od dimensional data
-- =============================================
CREATE PROCEDURE [warehouse].[AccreditationFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@SAID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50) = null, --'e.g. GHS100'
	@InspYear nvarchar(50) = null, --- e.g. '2016'
	@InspResult nvarchar(50) = null,
	@FilterColumn nvarchar(10) = null,
	@FilterValue int = null,

	@BestUpTo int = null,	-- best inspection up to and including this year

	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@AuthorityGroup nvarchar(10) = null,
	@SchoolType nvarchar(10) = null,

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

-- if filter params come from XML
if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	    @SAID = isnull(@SAID, saID),
		@School = isnull(@School, School),
		@SchoolNo = isnull(@SchoolNo, SchoolNo),
		@InspYear = isnull(@InspYear, InspYear),
		@InspResult = isnull(@InspResult, InspResult),
		@District = isnull(@District, District),
		@Authority = isnull(@Authority, Authority),
		@AuthorityGroup = isnull(@AuthorityGroup, AuthorityGroup),
		@SchoolType = isnull(@SchoolType, SchoolType),
		@FilterColumn = isnull(@FilterColumn, FilterColumn),
		@FilterValue = isnull(@FilterValue, FilterValue),
		@BestUpTo = isnull(@BestUpTo, BestUpTo)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	saID int '@SAID',
	School nvarchar(50) '@School',
	SchoolNo nvarchar(50) '@SchoolNo',
	InspYear nvarchar(50) '@InspYear',
	InspResult nvarchar(50) '@InspResult',
	District nvarchar(10) '@District',
	Authority nvarchar(10) '@Authority',
	AuthorityGroup nvarchar(10) '@AuthorityGroup',
	SchoolType nvarchar(10)	'@SchoolType',
	FilterColumn nvarchar(10) '@FilterColumn',
	FilterValue int '@FilterValue',
	BestUpTo int '@BestUpTo'
	)
end

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)

INSERT INTO @KeysAll (ID)
SELECT inspID
from warehouse.Accreditations SA -- view
	LEFT JOIN warehouse.BestSurvey BS
		ON SA.schNo = BS.schNo
			AND SA.InspectionYear = BS.SurveyYear
	LEFT JOIN warehouse.DimensionSchoolSurvey DSS
		ON BS.surveyDimensionID = DSS.[Survey ID]
WHERE
(inspID = @SAID OR @SAID IS NULL)
AND (InspectionYear = @InspYear OR @InspYear IS NULL)
AND (InspectionResult = @InspResult or @InspResult is null)
AND (SA.schNo = @SchoolNo OR @SchoolNo IS NULL)
AND (DSS.[School Name] like '%' +  @School + '%' OR @School IS NULL)
AND (DSS.AuthorityCode = @Authority OR @authority is null)
AND (DSS.AuthorityGroupCode = @AuthorityGroup OR @authorityGroup is null)
AND (DSS.[District Code] = @District OR @District is null)
AND (DSS.SchoolTypeCode = @SchoolType or @SchoolType is null)
AND (@FilterColumn is null or @FilterValue is null
	OR case @filterColumn
		when 'se.1' then [SE.1]
		when 'se.2' then [SE.2]
		when 'se.3' then [SE.3]
		when 'se.4' then [SE.4]
		when 'se.5' then [SE.5]
		when 'se.6' then [SE.6]
	end = @FilterValue)

	AND (@BestUpTo is null
	OR (inspID in (Select inspID from warehouse.BestInspection WHERE SurveyYear = @BestUpTo))
	)
OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

