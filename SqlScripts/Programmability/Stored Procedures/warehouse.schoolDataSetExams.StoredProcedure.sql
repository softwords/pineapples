SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 11 2022
-- Description:	Data items from exams

-- =============================================
CREATE PROCEDURE [warehouse].[schoolDataSetExams]
	-- Add the parameters for the stored procedure here
	@year int
	, @dataItem nvarchar(100)
	, @filter nvarchar(10) = null			-- examType
	, @schoolNo nvarchar(50) = null
	, @parameter nvarchar(100) = null
AS
BEGIN
	SET NOCOUNT ON;

if (@filter is null) begin
	raiserror('Exam item requires an exam type and year', 16,1)
	return
end

If not exists(select exCode from lkpExamtypes WHERE exCode = @filter) begin
	raiserror('Invalid exam code: %s', 16,1, @filter)
	return
end

If not exists(select exID from exams WHERE exCode = @filter and exYear = @year) begin
	raiserror('No exam data for exam code: %s year: %i', 16,1, @filter, @year)
	return
end


; with XTOT
AS
(
select examID, schNo
, convert(float,sum(CandidateCount)) CCount
, sum( case when AchievementLevel <= 1 then CandidateCount else 0 end) Level1
, sum( case when AchievementLevel = 2 then CandidateCount else 0 end) Level2
, sum( case when AchievementLevel <= 2 then CandidateCount else 0 end) Level1_2
, sum( case when AchievementLevel in (3) then CandidateCount else 0 end) Level3
, sum( case when AchievementLevel in (3,4) then CandidateCount else 0 end) Level3_4
, sum( case when AchievementLevel in (4) then CandidateCount else 0 end) Level4

, convert(float,sum(IndicatorCount)) ICount
, sum( case when AchievementLevel <= 1 then IndicatorCount else 0 end) ILevel1
, sum( case when AchievementLevel = 2 then IndicatorCount else 0 end) ILevel2
, sum( case when AchievementLevel <= 2 then IndicatorCount else 0 end) ILevel1_2
, sum( case when AchievementLevel in (3) then IndicatorCount else 0 end) ILevel3
, sum( case when AchievementLevel in (3,4) then IndicatorCount else 0 end) ILevel3_4
, sum( case when AchievementLevel in (4) then IndicatorCount else 0 end) ILevel4

from warehouse.ExamSchoolResultsTyped
WHERE REcordType = 'Exam'
AND examYear = @year
AND examCode = @filter
GROUP BY SchNo, examID
)
Select schNo
, isnull(
	case @dataItem
		when 'Candidates' then CCount
		when 'Level1' then Level1
		when 'Level1_2'then Level1_2
		when 'Level3_4'then Level3_4
		when 'Level4'then Level4
		when 'Level1%' then convert(decimal(6,2), 100 * Level1/CCount)
		when 'Level1_2%'then convert(decimal(6,2), 100 * Level1_2/CCount)
		when 'Level3_4%'then convert(decimal(6,2), 100 * Level3_4/CCount)
		when 'Level4%'then convert(decimal(6,2), 100 * Level4/CCount)
		-- do it all with indicator count as well
		when 'Indicators' then ICount
		when 'ILevel1' then ILevel1
		when 'ILevel1_2'then ILevel1_2
		when 'ILevel3_4'then ILevel3_4
		when 'ILevel4'then ILevel4
		when 'ILevel1%' then convert(decimal(6,2), 100 * ILevel1/ICount)
		when 'ILevel1_2%'then convert(decimal(6,2), 100 * ILevel1_2/ICount)
		when 'ILevel3_4%'then convert(decimal(6,2), 100 * ILevel3_4/ICount)
		when 'ILevel4%'then convert(decimal(6,2), 100 * ILevel4/ICount)
	end, 0) DataValue
, 0 Estimate
, null Quality
FROM  XTOT
ORDER BY schNo
END
GO

