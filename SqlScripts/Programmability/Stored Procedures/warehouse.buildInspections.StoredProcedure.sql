SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 July 2019
-- Description:	Generate all warehouse objects that are derived from school inspections
--
-- =============================================
-------------------------------------------------------------
-- 5 key tables make up the warehouse representation of accreditations
--- 1) warehouse.Accreditations
---		this is the denormalised view by criteria, equivaent to and generated from pInspectionRead.SchoolAccreditations
--- 2) warehouse.AccreditationDetail
---		normalised view of accreditations, showing one row per subcriteria where available
---		1 row per criteria where subcriteria is not available
---		This table can contain over 200 records ( ie number of subcriteria) for each inspection.
---
--- 3)	warehouse.AccreditationPerformance
---		by inspID and rank, shows those criteria scoring in that rank.
---		e.g. the data looks like
---    InspID Rank	[SE.1.1] [SE.1.1_Count]
---		9876	2		2			1
---		ie:
---			the count column is 1 when the coresponding criteria score equals the Rank of that record.
---			the score column is the criteria score when that score equals the Rank of that record
---
---		Views will combine this with BestInspection to get one inspection per survey oer year
---		and use the supplied SurveyDimensionId to aggregate this data.
---
---	 4) warehouse.AccreditationStandard
---		by inspId and Standard, the claculated Result for that standard

---   5) warehouse.BestInspection

---			shows, for each survey year,  school and inspection type the most recent matching inspection
---			up to that year. This allows aggregations across inspections without duplicating a school
---			Also makes possible cumulative time-lines on inspections

--		Aggregated tables, then views are built on these foundations. Using SurveyDimensionId from warehouse.BestSurvey
--		allows us to retireve aggregation dimensions (District, Authority etc) for each school.
--		Use warehouse.BestInspection to ensure that a school represented nomore than once in any aggregation

--	Consolidation Tables
--------------------------

-- warehouse.InspectionTable: Consolidates the overall result of inspections. Filter on InspectionType
--	to restict to accreditations

-- warehouse.StandardTable: Consolidates each standard by result level

-- warehouse.PerformanceTable : consolidates performance data

-- The above tables are useful for pivot tables, and crossfilters, allowing multiple dimensions to be manipulated
-- interactively

-- Consolidation Views
----------------------

-- These views are built on the above two tables, and provide smaller datasets, by aggregating only on one dimenstion ( District, Authority, School Type)
-- warehouse.AccreditationDistrict
-- warehouse.InspectionDistrict
-- warehouse.AccreditationAuthority
-- warehouse.InspectionAuthority

-- History Log:
--   * Ghislain Hachey, 23 July 2021: modify warehouse.AccreditationDetail and warehouse.BestInspection to only include schools that were opened in the given year

-------------------------------------------------------------
CREATE PROCEDURE [warehouse].[buildInspections]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS ON;

	print 'warehouse.BuildInspections'
	print '--------------------------'
	print ''

--------------------------------------------------------------------
-- warehouse.Accreditations - 1 row pre accreditation
--------------------------------------------------------------------
	DELETE FROM warehouse.Accreditations
	WHERE inspectionYear >= @StartFromYear or @StartFromYear is null

	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.Accreditations'

	INSERT INTO warehouse.Accreditations
	(
	inspID
	, schNo
	, StartDate
	, EndDate
	, InspectionYear
	, InspectionResult
	,[SE.1.1]
	,[SE.1.2]
	,[SE.1.3]
	,[SE.1.4]
	,[SE.1]

	,[SE.2.1]
	,[SE.2.2]
	,[SE.2.3]
	,[SE.2.4]
	,[SE.2]

	,[SE.3.1]
	,[SE.3.2]
	,[SE.3.3]
	,[SE.3.4]
	,[SE.3]

	,[SE.4.1]
	,[SE.4.2]
	,[SE.4.3]
	,[SE.4.4]
	,[SE.4]

	,[SE.5.1]
	,[SE.5.2]
	,[SE.5.3]
	,[SE.5.4]
	,[SE.5]

	,[SE.6.1]
	,[SE.6.2]
	,[SE.6.3]
	,[SE.6.4]
	,[SE.6]

	,[CO.1]
	,[CO.2]
	,[Level1]
	,[Level2]
	,[Level3]
	,[Level4]
	)
	SELECT inspID
	, schNo
	, StartDate
	, EndDate
	, InspectionYear
	, InspectionResult
	,[SE.1.1]
	,[SE.1.2]
	,[SE.1.3]
	,[SE.1.4]
	,[SE.1]

	,[SE.2.1]
	,[SE.2.2]
	,[SE.2.3]
	,[SE.2.4]
	,[SE.2]

	,[SE.3.1]
	,[SE.3.2]
	,[SE.3.3]
	,[SE.3.4]
	,[SE.3]

	,[SE.4.1]
	,[SE.4.2]
	,[SE.4.3]
	,[SE.4.4]
	,[SE.4]

	,[SE.5.1]
	,[SE.5.2]
	,[SE.5.3]
	,[SE.5.4]
	,[SE.5]

	,[SE.6.1]
	,[SE.6.2]
	,[SE.6.3]
	,[SE.6.4]
	,[SE.6]

	,[CO.1]
	,[CO.2]
	,[Level1]
	,[Level2]
	,[Level3]
	,[Level4]

	FROM [pInspectionRead].[SCHOOL_ACCREDITATION]
	WHERE inspectionYear >= @StartFromYear or @StartFromYear is null

	print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.Accreditations'
-------------------------------------------------------------------------
-- warehouse.AccreditationDetail
-------------------------------------------------------------------------
	DELETE FROM warehouse.AccreditationDetail
	WHERE inspectionYear >= @StartFromYear or @StartFromYear is null
	OR inspID not in (Select inspId from warehouse.Accreditations)

	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.AccreditationDetail'
	INSERT INTO warehouse.AccreditationDetail
	(
		schNo
		, inspID
		, InspectionYear
		, SurveyDimensionID
		, CategoryType
		, Category
		, Standard
		, StandardName
		, Criteria
		, CriteriaName
		, SubCriteria
		, SubCriteriaName
		, SubCriteriaQuestion
		, Answer
		, Score
		, MaxScore
	)
	SELECT
		SI.schNo
		, SI.inspID
		, SSA.InspectionYear
		, BSS.surveyDimensionID
		, CategoryType
		, Category
		, Standard
		, StandardName
		, Criteria
		, CriteriaName
		, SubCriteria
		, SubCriteriaName
		, SubCriteriaQuestion
		, Answer
		, Score
		, MaxScore
	from pInspectionRead.SchoolStandardAssessment SSA
	INNER JOIN pInspectionRead.SchoolInspections SI
		ON SSA.inspID = SI.inspID
	LEFT JOIN warehouse.bestSurvey BSS
		ON BSS.schNo = SI.schNo
		AND BSS.SurveyYear = SI.InspectionYear
	INNER JOIN Schools SS ON BSS.schNo = SS.schNo AND (SS.schClosed >= BSS.SurveyYear OR SS.schClosed = 0) -- only include schools that were opened in the given year
	WHERE SSA.inspectionYear >= @StartFromYear or @StartFromYear is null

	print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.AccreditationDetail'
---------------------------------------------------------------------
-- warehouse.AccreditationPerformance
---------------------------------------------------------------------

	DELETE From warehouse.AccreditationPerformance
	WHERE inspID IN
	(	Select inspId
		from warehouse.Accreditations
		WHERE InspectionYear >= @StartFromYear or @StartFromYear is null
	)
	OR inspID not in (Select inspId from warehouse.Accreditations)

	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.AccreditationPerformance'

	INSERT INTO warehouse.AccreditationPerformance
	  ([inspID]
           ,[ResultValue]
           ,[SE.1.1_Count]
           ,[SE.1.2_Count]
           ,[SE.1.3_Count]
           ,[SE.1.4_Count]
           ,[SE.1_Count]
           ,[SE.2.1_Count]
           ,[SE.2.2_Count]
           ,[SE.2.3_Count]
           ,[SE.2.4_Count]
           ,[SE.2_Count]
           ,[SE.3.1_Count]
           ,[SE.3.2_Count]
           ,[SE.3.3_Count]
           ,[SE.3.4_Count]
           ,[SE.3_Count]
           ,[SE.4.1_Count]
           ,[SE.4.2_Count]
           ,[SE.4.3_Count]
           ,[SE.4.4_Count]
           ,[SE.4_Count]
           ,[SE.5.1_Count]
           ,[SE.5.2_Count]
           ,[SE.5.3_Count]
           ,[SE.5.4_Count]
           ,[SE.5_Count]
           ,[SE.6.1_Count]
           ,[SE.6.2_Count]
           ,[SE.6.3_Count]
           ,[SE.6.4_Count]
           ,[SE.6_Count]
           ,[CO.1_Count]
           ,[CO.2_Count]
           ,[ResultDescription]
           ,[ResultLevel])
	Select [inspID]
           ,[ResultValue]
           ,[SE.1.1_Count]
           ,[SE.1.2_Count]
           ,[SE.1.3_Count]
           ,[SE.1.4_Count]
           ,[SE.1_Count]
           ,[SE.2.1_Count]
           ,[SE.2.2_Count]
           ,[SE.2.3_Count]
           ,[SE.2.4_Count]
           ,[SE.2_Count]
           ,[SE.3.1_Count]
           ,[SE.3.2_Count]
           ,[SE.3.3_Count]
           ,[SE.3.4_Count]
           ,[SE.3_Count]
           ,[SE.4.1_Count]
           ,[SE.4.2_Count]
           ,[SE.4.3_Count]
           ,[SE.4.4_Count]
           ,[SE.4_Count]
           ,[SE.5.1_Count]
           ,[SE.5.2_Count]
           ,[SE.5.3_Count]
           ,[SE.5.4_Count]
           ,[SE.5_Count]
           ,[SE.6.1_Count]
           ,[SE.6.2_Count]
           ,[SE.6.3_Count]
           ,[SE.6.4_Count]
           ,[SE.6_Count]
           ,[CO.1_Count]
           ,[CO.2_Count]
           ,[ResultDescription]
           ,[ResultLevel]
	FROM pInspectionRead.SchoolAccreditationPerformance
	WHERE inspID IN
	(	Select inspId
		from warehouse.Accreditations
		WHERE InspectionYear >= @StartFromYear or @StartFromYear is null
	)
	OR inspID not in (Select inspId from warehouse.Accreditations)

	print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.AccreditationPerformance'
----------------------------------------------------------------
-- warehouse.AccreditationStandard
--
--		uses warehouse.AccreditationDetail
----------------------------------------------------------------

	DELETE from warehouse.AccreditationStandard
	WHERE inspID IN
	(	Select inspId
		from warehouse.Accreditations
		WHERE InspectionYear >= @StartFromYear or @StartFromYear is null
	)
	OR inspID not in (Select inspId from warehouse.Accreditations)

	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.AccreditationStandard'

	INSERT INTO warehouse.AccreditationStandard
	Select SUB.*
	, P.ptName
	FROM
	(
	Select inspID, CategoryType, Standard, sum(score) Score
	, sum(maxscore) MaxScore
	, convert(decimal(3,0), sum(Score) * 100 / convert(float,sum(MaxScore))) ScorePerc
	from warehouse.AccreditationDetail
	group by  inspID, CategoryType, Standard
	) SUB
		LEFT JOIN Partitions P
			on p.ptSet = 'SSA'
		AND SUB.ScorePerc between ptMin and ptMax

	print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.AccreditationStandard'
---------------------------------------------------------------------
-- warehouse.BestInspection
	-- get the best inspection by type, up to and including
	-- each survey year
--------------------------------------------------------------------
	DELETE
	FROM warehouse.BestInspection
	WHERE surveyYear >= @StartFromYear or @StartFromYear is null

	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.BestInspection'

	INSERT INTO warehouse.BestInspection
	(
		SurveyYear
		, schNo
		, InspectionTypeCode
		, inspID
		, InspectionYear
		, StartDate
		, InspectionResult
		, SurveyDimensionID
	)
	Select
	SurveyYear svyYear
	, SchNo
	, InspTypeCode
	, inspID
	, InspectionYear
	, StartDate
	, InspectionResult
	, SurveyDimensionID
	FROM
	(
		Select SurveyYear
		,  SI.schNo
		, InspTypeCode
		, inspID
		, InspectionYear
		, StartDate
		,  InspectionResult
		, SurveyDimensionID
		, row_number() OVER (PARTITION BY SurveyYear, SI.SchNo, InspectionType ORDER BY StartDate DESC, SurveyYear DESC,  inspID DESC) RNY
		from pInspectionRead.SchoolInspections SI
		INNER JOIN Schools SS ON SI.schNo = SS.schNo
		INNER JOIN warehouse.BestSurvey BS
			ON SI.schNo = BS.schNo
			AND (SS.schClosed >= BS.SurveyYear OR SS.schClosed = 0) -- only include schools that were opened in the given year
			AND SI.InspectionYEar <= BS.surveyYear
	) R
	WHERE RNY = 1

	print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.BestInspection'
---------------------------------------------------------------
-- ** Aggregation tables ** --
---------------------------------------------------------------

-- warehouse.InspectionTable

DELETE from warehouse.InspectionTable			-- clear the whole table and rebuild it

print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.InspectionTable'

INSERT INTO warehouse.InspectionTable
select SurveyYear
, InspectionTypeCode
, DSS.[District Code] DistrictCode
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGroupCode	AuthorityGovtCode
, DSS.AuthorityGroup		AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, InspectionResult
, count(*) Num
, sum(case when InspectionYear = SurveyYear then 1 else null end) NumThisYear
from warehouse.BestInspection BI
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON BI.SurveyDimensionID = DSS.[Survey ID]
GROUP BY
SurveyYear
, InspectionTypeCode
, DSS.[District Code]
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGroupCode
, DSS.AuthorityGroup
, DSS.SchoolTypeCode
, DSS.SchoolType
, InspectionResult

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.InspectionTable'

--------------------------------------------------------------
-- warehouse.PerformanceTable
--------------------------------------------------------------
DELETE from warehouse.AccreditationPerformanceTable			-- clear the whole table and rebuild it

print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.AccreditationPerformanceTable'

INSERT INTO warehouse.AccreditationPerformanceTable
 ([SurveyYear]
           ,[DistrictCode]
           ,[District]
           ,[AuthorityCode]
           ,[Authority]
           ,[AuthorityGovtCode]
           ,[AuthorityGovt]
           ,[SchoolTypeCode]
           ,[SchoolType]
           ,[ResultValue]
           ,[ResultDescription]
           ,[ResultLevel]
           ,[SE.1.1_Count]
           ,[SE.1.2_Count]
           ,[SE.1.3_Count]
           ,[SE.1.4_Count]
           ,[SE.1_Count]
           ,[SE.2.1_Count]
           ,[SE.2.2_Count]
           ,[SE.2.3_Count]
           ,[SE.2.4_Count]
           ,[SE.2_Count]
           ,[SE.3.1_Count]
           ,[SE.3.2_Count]
           ,[SE.3.3_Count]
           ,[SE.3.4_Count]
           ,[SE.3_Count]
           ,[SE.4.1_Count]
           ,[SE.4.2_Count]
           ,[SE.4.3_Count]
           ,[SE.4.4_Count]
           ,[SE.4_Count]
           ,[SE.5.1_Count]
           ,[SE.5.2_Count]
           ,[SE.5.3_Count]
           ,[SE.5.4_Count]
           ,[SE.5_Count]
           ,[SE.6.1_Count]
           ,[SE.6.2_Count]
           ,[SE.6.3_Count]
           ,[SE.6.4_Count]
           ,[SE.6_Count]
           ,[CO.1_Count]
           ,[CO.2_Count])
SELECT SurveyYear
, DSS.[District Code] DistrictCode
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGroupCode	AuthorityGovtCode
, DSS.AuthorityGroup		AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, ResultValue
, ResultDescription
, ResultLevel
, sum([SE.1.1_Count]) 		[SE.1.1_Count]
, sum([SE.1.2_Count]) 		[SE.1.2_Count]
, sum([SE.1.3_Count]) 		[SE.1.3_Count]
, sum([SE.1.4_Count]) 		[SE.1.4_Count]
, sum([SE.1_Count]) 		[SE.1_Count]
, sum([SE.2.1_Count]) 		[SE.2.1_Count]
, sum([SE.2.2_Count]) 		[SE.2.2_Count]
, sum([SE.2.3_Count]) 		[SE.2.3_Count]
, sum([SE.2.4_Count]) 		[SE.2.4_Count]
, sum([SE.2_Count]) 		[SE.2_Count]
, sum([SE.3.1_Count]) 		[SE.3.1_Count]
, sum([SE.3.2_Count]) 		[SE.3.2_Count]
, sum([SE.3.3_Count]) 		[SE.3.3_Count]
, sum([SE.3.4_Count]) 		[SE.3.4_Count]
, sum([SE.3_Count]) 		[SE.3_Count]
, sum([SE.4.1_Count]) 		[SE.4.1_Count]
, sum([SE.4.2_Count]) 		[SE.4.2_Count]
, sum([SE.4.3_Count]) 		[SE.4.3_Count]
, sum([SE.4.4_Count]) 		[SE.4.4_Count]
, sum([SE.4_Count]) 		[SE.4_Count]
, sum([SE.5.1_Count]) 		[SE.5.1_Count]
, sum([SE.5.2_Count]) 		[SE.5.2_Count]
, sum([SE.5.3_Count]) 		[SE.5.3_Count]
, sum([SE.5.4_Count]) 		[SE.5.4_Count]
, sum([SE.5_Count]) 		[SE.5_Count]
, sum([SE.6.1_Count]) 		[SE.6.1_Count]
, sum([SE.6.2_Count]) 		[SE.6.2_Count]
, sum([SE.6.3_Count]) 		[SE.6.3_Count]
, sum([SE.6.4_Count]) 		[SE.6.4_Count]
, sum([SE.6_Count]) 		[SE.6_Count]
, sum([CO.1_Count]) 		[CO.1_Count]
, sum([CO.2_Count]) 		[CO.2_Count]

 FROM warehouse.AccreditationPerformance A
	INNER JOIN warehouse.BestInspection BI
		ON A.inspID = BI.inspID
	LEFT JOIN warehouse.dimensionSchoolSurvey DSS
	ON BI.SurveyDimensionID = DSS.[Survey ID]
-- this is only meaningful for SCHOOL_ACCREDITATION
WHERE BI.InspectionTypeCode = 'SCHOOL_ACCREDITATION'
GROUP BY
SurveyYear
, DSS.[District Code]
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGroupCode
, DSS.AuthorityGroup
, DSS.SchoolTypeCode
, DSS.SchoolType
, ResultValue
, ResultDescription
, ResultLevel

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.AccreditationPerformanceTable'

--------------------------------------------------------------
-- warehouse.StandardTable
--------------------------------------------------------------
DELETE from warehouse.StandardTable			-- clear the whole table and rebuild it

print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.StandardTable'

INSERT INTO warehouse.StandardTable
select SurveyYear
, InspectionTypeCode
, DSS.[District Code] DistrictCode
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGroupCode	AuthorityGovtCode
, DSS.AuthorityGroup		AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, InspectionResult
, SSAS.Standard
, SSAS.Result StandardResult
, count(*) Num
, sum(case when InspectionYEar = SurveyYear then 1 else null end) NumThisYear
from warehouse.BestInspection BI
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON BI.SurveyDimensionID = DSS.[Survey ID]
INNER JOIN warehouse.AccreditationStandard SSAS
	ON SSAS.inspID = BI.inspID
GROUP BY
SurveyYear
, InspectionTypeCode
, DSS.[District Code]
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGroupCode
, DSS.AuthorityGroup
, DSS.SchoolTypeCode
, DSS.SchoolType
, InspectionResult
, SSAS.Standard
, SSAS.Result

	print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.StandardTable'

	exec warehouse.logVersion

	declare @versionID nvarchar(50)
	declare @versionDateTime dateTime

	Select @VersionId = versionID
	, @VersionDateTime = versionDateTime
	from warehouse.versionInfo


	print ''
	print 'warehouse.BuildInspections completed'
	print '------------------------------------'
	print ''
	print 'Warehouse version: ' + @versionId + ' ' + convert(nvarchar(40), @versionDateTime) + ' (UTC)'

END
GO

