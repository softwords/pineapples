SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 12 2017
-- Description:	first attempt at working with kobo downloaded data
-- =============================================
CREATE PROCEDURE [pInspectionWrite].[koboValidate]
	-- Add the parameters for the stored procedure here

	@xml xml
	, @filereference uniqueidentifier
	, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- this is the type of Kobo form implemented by this stored proc
-- it must match a description in the table lkpInspectionTypes

declare @FirstRow int = 0;  -- the first row of the table data in the source worksheet
Select @FirstRow = @xml.value('*[1]/@FirstRow', 'int')

declare @rows TABLE
(
 	rowIndex int
	, [start] datetime
	, [end] datetime
	, inspectionType nvarchar(20)
	, inspBy nvarchar(100)
	, schNo nvarchar(50)
	, rowXml xml
	, filereference uniqueidentifier null
	, koboId int
)


DECLARE @errors TABLE
(
	rowID int
	, errorValue nvarchar(100)
	, errorMessage nvarchar(500)
)
DECLARE @numErrors int

-- represent the xml file - column names are 'sanitized'
INSERT INTO @rows
SELECT
v.value('@Index', 'int') [ RowIndex]
, nullif(ltrim(v.value('@start', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@end', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@inspectionType', 'nvarchar(50)')),'')
, nullif(ltrim(v.value('@inspBy', 'nvarchar(100)')),'')
, nullif(ltrim(v.value('@schNo', 'nvarchar(50)')),'')
, v.query('.')

-- file reference is the id of the input file
, @fileReference
, nullif(ltrim(v.value('@_id', 'int')),'')


from @xml.nodes('ListObject/row') as V(v)

IF @@ROWCOUNT = 0 begin
INSERT INTO @errors
VALUES (null, null, 'There are no records in this file')
Select *
from @errors
return
end

--- check the mandatory columns
DECLARE @MandatoryColumns TABLE
(
	col nvarchar(100)
)

INSERT INTO @MandatoryColumns
VALUES ('start')
, ('end')
, ('inspectionType')
, ('inspBy')
, ('schNo')
, ('_id')

INSERT INTO @errors
Select null, M.col, 'Required column is not defined in the input survey'
FROM @MandatoryColumns M
WHERE col not in (
Select c.value('local-name(.)', 'nvarchar(100)') col
FROM @xml.nodes('(ListObject/row[1])/@*') C(c)
)


IF @@ROWCOUNT > 0 begin
	Select *
	from @errors

	return
end


-- report any rows where
-- the inspectionType is missing or invalid
-- school is missing or invalid
-- inspBy is missing
INSERT INTO @errors
Select rowIndex
, R.schNo
, case when R.schNo is null then 'Missing school no'
	when S.schNo is null then 'Invalid school no'
end
from @rows R
LEFT JOIN Schools S
	ON R.schNo = S.schNo
WHERE S.schNo is null

INSERT INTO @errors
Select rowIndex
, R.inspectionType
, case when inspectionType is null then 'Missing inspection type code'
	when intyCode is null then 'Invalid inspection type code'
end
from @rows R
LEFT JOIN lkpInspectionTypes I
ON R.inspectionType = I.intyCode
WHERE intyCode is null

INSERT INTO @errors
Select rowIndex
, null
, 'missing Inspected By (inspBy)'
from @rows R
WHERE inspBy is null


INSERT INTO @errors
Select rowIndex
, null
, 'missing start'
from @rows R
WHERE [start] is null


INSERT INTO @errors
Select rowIndex
, null
, 'missing end'
from @rows R
WHERE [end] is null
-- report the validations. Note that the collected row item is the 'logical' (0-based0 row of data in the table
-- to convert this to a Row number in the source Excel, add the value of FirstRow
	Select rowID + @FirstRow rowID
	, errorValue
	, errorMessage
	from @errors
	ORDER BY rowID
	Select @numErrors = @@ROWCOUNT
END
GO

