SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 9 2022
-- Description:	Utility routine to reprocess an exam using the shredded xml
--              held on ExamCandidates
-- =============================================
CREATE PROCEDURE [pExamAdmin].[ReplaySoeExam]
	-- Add the parameters for the stored procedure here
	@exCode nvarchar(10)
	, @exYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @exID int
	declare @docID uniqueidentifier

    Select @exID = exID
	, @docID = docID
	from Exams
	WHERE exCode = @exCode
	AND exYear = @exYear

	if (@exID is null) begin
		raiserror('Exam %s %i not found',16,1,@exCode, @exYear)
		return
	end

	declare @xml xml
	select @xml = pExamAdmin.getExamXml(@exID)
	exec pExamWrite.LoadSoeExam @xml, @docID, original_login, @exYear

END
GO

