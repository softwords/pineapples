SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 3 2021
-- Description:	Sheet 5 of UIS Survey
-- =============================================
CREATE PROCEDURE [warehouse].[uisA5]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @ISCED nvarchar(10) = 'ISCED 1'

DECLARE  @AgeGroups Table
(
Age int,
AgeGRoup nvarchar(10)
)

declare @minYoE int
declare @maxYoE int


Select @minYoE = min(DL.[Year of Education]),
@maxYoE = max(DL.[Year Of Education])
from DimensionLevel DL
WHERE DL.[ISCED Level] = @ISCED

DECLARE  @d Table
(
Isced nvarchar(10),
YoE int,			-- year of education within the isced level
Age int,
Flag nvarchar(1),
EnrolM int,
EnrolF int,
Enrol int,
RepM int,
RepF int,
Rep int,
PSAM int,
PSAF int,
PSA int
)

INSERT INTO @AgeGroups
Select DISTINCT Age,
case when Age =  -1 then 'Unknown'
when Age > 24 then '>24'
else
	case age
		when -1 then 'Unknown'
		when 0 then '<4'
		when 1 then '<4'
		when 2 then '<4'
		when 3 then '<4'
		else convert(nvarchar(2), Age)
	end
end AgeGroup
from
(Select isnull(Age, -1) Age from warehouse.tableEnrol
UNION
Select num from metaNumbers WHERE num between -1 and 30
) U

-- build the data
-- 1) this set ensures that all age cells get populated
-- but only if the class level has some data in the year
INSERT INTO @d
SELECT DISTINCT
DL.[ISCED Level] Isced,
DL.[Year of Education],
AG.Age,
null Flag,
0 EnrolM,
0 EnrolF,
0 Enrol,
0 RepM,
0 RepF,
0 Rep,
0 PSAM,
0 PSAF,
0 PSA

from warehouse.EnrolNation E
INNER join DimensionLevel DL on E.ClassLevel = DL.LevelCode
CROSS JOIN @AgeGroups AG
WHERE surveyYear = @year
AND DL.[ISCED Level] = @ISCED


-- 2) this set supplies the actual records - this is sparse
INSERT INTO @d
SELECT
DL.[ISCED Level] Isced,
DL.[Year of Education],
isnull(Age, -1) Age,
convert(nvarchar(1),null) Flag,
EnrolM,
EnrolF,
Enrol,
RepM,
RepF,
Rep,
PSAM,
PSAF,
PSA

from warehouse.EnrolNationR E
left join DimensionLevel DL on E.ClassLevel = DL.LevelCode

where E.SurveyYear = @year
	AND DL.[ISCED Level] = @ISCED
	AND (E.Enrol IS NOT NULL OR E.Rep is not null)


-- 3) Missing data (M flag)
-- this identifies the class levels that are in DimmensionLevel
-- (ie there is a ClassLevel defined for that Isced level / year of ed )
-- but there is no enrolment data for that class level at all.
-- therefore we go with M - missing
INSERT INTO @d
SELECT
DL.[ISCED Level] Isced,
DL.[Year of Education],
Age,
'M' Flag,
null EnrolM,
null EnrolF,
null Enrol,
null RepM,
null RepF,
null Rep,
null PSAM,
null PSAF,
null PSA


FROM DimensionLevel DL
CROSS JOIN @AgeGroups

WHERE DL.[ISCED Level] = @ISCED
AND DL.LevelCode not in (select E.ClassLevel from warehouse.EnrolNation E where surveyYear = @year)

-- 4) Not applicablt ( Z flag)
-- this identifies years that are not included in the ISCED Level
-- ie there are no Grades defined that for the ISCED LEvel and Year of Ed.
-- therefore we go with Z - not applicable
-- This is to populate the 7th and 8th columns of sheet A5, if there is no 7th /8th year of ISCED1
INSERT INTO @d
SELECT
@ISCED Isced,
YearRange.Num YoE,
AgeRange.num Age,

'Z' Flag,
null EnrolM,
null EnrolF,
null Enrol,
null RepM,
null RepF,
null Rep,
null PSAM,
null PSAF,
null PSA


FROM metaNumbers YearRange
CROSS JOIN metaNumbers AGERange
WHERE YearRange.num between @maxYoE + 1 and @minYoE + 7
AND AgeRange.num between -1 and 30


-- Aggregate the results

Select Isced,
AgeGroup,
YoE - @minYoE + 1 Grade,
max(Flag) Flag,
sum(EnrolM) EnrolM,
sum(EnrolF) EnrolF,
sum(Enrol) Enrol,
sum(RepM) RepM,
sum(RepF) RepF,
sum(Rep) Rep,
sum(EnrolM) - sum(RepM) IntakeM,
sum(EnrolF) - sum(RepF) IntakeF,
sum(Enrol) - sum(Rep) Intake,

sum(PSAM) PSAM,
sum(PSAF) PSAF,
sum(PSA) PSA

FROM
@d D
INNER JOIN @AgeGroups AG
ON D.Age = AG.Age

group by Isced, AgeGroup, YoE


UNION ALL

-- add the totals row to the bottom, to support reporting on total repeaters
Select Isced,
'Aggregated' AgeGroup,
YoE - @minYoE + 1 Grade,
max(Flag) Flag,
sum(EnrolM) EnrolM,
sum(EnrolF) EnrolF,
sum(Enrol) Enrol,
sum(RepM) RepM,
sum(RepF) RepF,
sum(Rep) Rep,

sum(EnrolM) - sum(RepM) IntakeM,
sum(EnrolF) - sum(RepF) IntakeF,
sum(Enrol) - sum(Rep) Intake,

sum(PSAM) PSAM,
sum(PSAF) PSAF,
sum(PSA) PSA
FROM
@d D
group by Isced, YoE

order by isced, Grade

--select DL.[ISCED Level], min([Year of Education]) from DimensionLevel DL
--group by DL.[ISCED Level]


END
GO

