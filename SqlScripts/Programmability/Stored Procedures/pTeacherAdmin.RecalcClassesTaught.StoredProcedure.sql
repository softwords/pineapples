SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pTeacherAdmin].[RecalcClassesTaught]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @ndoeStaff TABLE
(
schNo nvarchar(50)
, surveyYear int

, tchsID int
    , ECE                    nvarchar(1) NULL
    , Grade_1                    nvarchar(1) NULL
    , Grade_2                    nvarchar(1) NULL
    , Grade_3                    nvarchar(1) NULL
    , Grade_4                    nvarchar(1) NULL
    , Grade_5                    nvarchar(1) NULL
    , Grade_6                    nvarchar(1) NULL
    , Grade_7                    nvarchar(1) NULL
    , Grade_8                    nvarchar(1) NULL
    , Grade_9                    nvarchar(1) NULL
    , Grade_10                   nvarchar(1) NULL
    , Grade_11                   nvarchar(1) NULL
    , Grade_12                   nvarchar(1) NULL
	, Admin						 nvarchar(1) NULL
	, Other						 nvarchar(1) null
	, staffType					 nvarchar(100) null
, minYearTaught int
, maxYearTaught int
, ClassMin nvarchar(10)
, ClassMax nvarchar(10)
, sector nvarchar(10)
, TAM nvarchar(10)
, Support nvarchar(10)
)

INSERT INTO @ndoeStaff
(
schNo
, surveyYear
, tchsID
	, ECE
    , Grade_1
    , Grade_2
    , Grade_3
    , Grade_4
    , Grade_5
    , Grade_6
    , Grade_7
    , Grade_8
    , Grade_9
    , Grade_10
    , Grade_11
    , Grade_12
	, Admin
	, Other
	, StaffType

)
Select
SS.schNo
, SS.svyYear
	, tchsID
	, nullif(ltrim(tcheData.value('(/row/@ECE)[1]', 'nvarchar(1)')),'')                                          [ECE]
	, nullif(ltrim(tcheData.value('(/row/@Grade_1)[1]', 'nvarchar(1)')),'')                                          [Grade_1]
	, nullif(ltrim(tcheData.value('(/row/@Grade_2)[1]', 'nvarchar(1)')),'')                                          [Grade_2]
	, nullif(ltrim(tcheData.value('(/row/@Grade_3)[1]', 'nvarchar(1)')),'')                                          [Grade_3]
	, nullif(ltrim(tcheData.value('(/row/@Grade_4)[1]', 'nvarchar(1)')),'')                                          [Grade_4]
	, nullif(ltrim(tcheData.value('(/row/@Grade_5)[1]', 'nvarchar(1)')),'')                                          [Grade_5]
	, nullif(ltrim(tcheData.value('(/row/@Grade_6)[1]', 'nvarchar(1)')),'')                                          [Grade_6]
	, nullif(ltrim(tcheData.value('(/row/@Grade_7)[1]', 'nvarchar(1)')),'')                                          [Grade_7]
	, nullif(ltrim(tcheData.value('(/row/@Grade_8)[1]', 'nvarchar(1)')),'')                                          [Grade_8]
	, nullif(ltrim(tcheData.value('(/row/@Grade_9)[1]', 'nvarchar(1)')),'')                                          [Grade_9]
	, nullif(ltrim(tcheData.value('(/row/@Grade_10)[1]', 'nvarchar(1)')),'')                                         [Grade_10]
	, nullif(ltrim(tcheData.value('(/row/@Grade_11)[1]', 'nvarchar(1)')),'')                                         [Grade_11]
	, nullif(ltrim(tcheData.value('(/row/@Grade_12)[1]', 'nvarchar(1)')),'')                                         [Grade_12]
	, nullif(ltrim(tcheData.value('(/row/@Admin)[1]', 'nvarchar(1)')),'')                                        Admin
	, nullif(ltrim(tcheData.value('(/row/@Other)[1]', 'nvarchar(1)')),'')                                         Other
	, nullif(ltrim(tcheData.value('(/row/@Staff_Type)[1]', 'nvarchar(100)')),'')                                         StaffType
FROM TeacherSurvey TS
	LEFT JOIN SchoolSurvey SS
	ON TS.ssId = SS.ssID
	WHERE tcheData is not null
-- get the minimum and maximum levels taught
UPDATE @ndoeStaff
 set minYearTaught =
	case
	 when ECE = 'X' then 0
	 when Grade_1 = 'X' then 1
	 when Grade_2 = 'X' then 2
	 when Grade_3 = 'X' then 3
	 when Grade_4 = 'X' then 4
	 when Grade_5 = 'X' then 5
	 when Grade_6 = 'X' then 6
	 when Grade_7 = 'X' then 7
	 when Grade_8 = 'X' then 8
	 when Grade_9 = 'X' then 9
	 when Grade_10 = 'X' then 10
	 when Grade_11 = 'X' then 11
	 when Grade_12 = 'X' then 12
	 else null
	end,
	maxYearTaught =
	case
	 when Grade_12 = 'X' then 12
	 when Grade_11 = 'X' then 11
	 when Grade_10 = 'X' then 10
	 when Grade_9 = 'X' then 9
	 when Grade_8 = 'X' then 8
	 when Grade_7 = 'X' then 7
	 when Grade_6 = 'X' then 6
	 when Grade_5 = 'X' then 5
	 when Grade_4 = 'X' then 4
	 when Grade_3 = 'X' then 3
	 when Grade_2 = 'X' then 2
	 when Grade_1 = 'X' then 1
	 when ECE = 'X' then 0
	 else null
	end

-- now get classMin and classMax
UPDATE @ndoeStaff
SET ClassMin = tlmLevel
FROM @ndoeStaff
	INNER JOIN Schools S
		on [@ndoeStaff].schNo = S.schNo
	INNER JOIN metaSchoolTypeLevelMap TLM
		ON S.schType = TLM.stCode
		AND [@ndoeStaff].minYearTaught = TLM.tlmOffset

		-- now get classMin and classMax
UPDATE @ndoeStaff
SET ClassMax = nullif(tlmLevel, ClassMin)
FROM @ndoeStaff
	INNER JOIN Schools S
		on [@ndoeStaff].schNo = S.schNo
	INNER JOIN metaSchoolTypeLevelMap TLM
		ON S.schType = TLM.stCode
		AND [@ndoeStaff].maxYearTaught = TLM.tlmOffset

UPDATE @ndoeStaff
SET ClassMin = levelCode
FROM @ndoeStaff
	INNER JOIN (
		Select [Year of Education]
			, min(levelCode) LevelCode
			, count(levelCode) NumLevels
			from DimensionLevel
			GROUP BY [Year of Education]
			HAVING count(levelCode) = 1
			) EE
		ON minYearTaught = [Year of Education]
		WHERE ClassMin is null

UPDATE @ndoeStaff
SET ClassMax = nullif(levelCode, classMin)
FROM @ndoeStaff
	INNER JOIN (
		Select [Year of Education]
			, min(levelCode) LevelCode
			, count(levelCode) NumLevels
			from DimensionLevel
			GROUP BY [Year of Education]
			HAVING count(levelCode) = 1
			) EE
		ON maxYearTaught = [Year of Education]
		WHERE ClassMax is null


 -- if they are teaching a class, force them to be teaching staff

UPDATE @ndoeStaff
	SET Support = 'Support'
WHERE StaffType = 'Non teaching Staff'

UPDATE @ndoeStaff
set TAM = 'T'
, Support = null

WHERE coalesce(ECE, Grade_1, GRade_2, Grade_3, Grade_4, Grade_5, Grade_6, Grade_7, Grade_8, Grade_9, GRade_10, Grade_11, Grade_12)  is not null
AND Other is null and Admin is null

UPDATE @ndoeStaff
set TAM = 'M'
, Support = null
WHERE coalesce(ECE, Grade_1, GRade_2, Grade_3, Grade_4, Grade_5, Grade_6, Grade_7, Grade_8, Grade_9, GRade_10, Grade_11, Grade_12)  is not null
AND (Other is not null or Admin is not null)

-- doing admin duties, no teaching
UPDATE @ndoeStaff
set TAM = 'A'
WHERE coalesce(ECE, Grade_1, GRade_2, Grade_3, Grade_4, Grade_5, Grade_6, Grade_7, Grade_8, Grade_9, GRade_10, Grade_11, Grade_12)  is null
AND (Admin is not null)


-- only other duties no teaching, or nothing at all and non-teaching staff
UPDATE @ndoeStaff
set TAM = 'X'
WHERE coalesce(ECE, Grade_1, GRade_2, Grade_3, Grade_4, Grade_5, Grade_6, Grade_7, Grade_8, Grade_9, GRade_10, Grade_11, Grade_12)  is null
AND (Other is not null or Support = 'Support') and Admin is null

-- no teaching or admin or other, but flagged as teaching staff - record TAM=T
print 'TAM=t'
UPDATE @ndoeStaff
set TAM = 'T'
WHERE coalesce(ECE, Grade_1, GRade_2, Grade_3, Grade_4, Grade_5, Grade_6, Grade_7, Grade_8, Grade_9, GRade_10, Grade_11, Grade_12)  is null
AND Other is null and Admin is null
AND StaffType = 'Teaching Staff'

print @@rowcount

Select 'teaching staff with no grades'
Select * from @ndoeStaff
WHERE StaffType = 'Teaching Staff'
AND coalesce(ECE, Grade_1, GRade_2, Grade_3, Grade_4, Grade_5, Grade_6, Grade_7, Grade_8, Grade_9, GRade_10, Grade_11, Grade_12)  is null
ORDER BY tchsID

Select * from @ndoeStaff
WHERE StaffType = 'Non Teaching Staff'
AND Support is null

Select * from @ndoeStaff
WHERE Support is not null

-- now get the sector
UPDATE @ndoeStaff
SET sector = SectorCode
FROM @ndoeStaff
 INNER JOIN DimensionLevel DL
 ON isnull(classMax, ClassMin) = levelCode

-- next best is, if we doon't have class levels and have a teaching staff, get from the highlest level taught in the school that year
UPDATE @ndoeStaff
SET sector = DL.SectorCode
FROM @ndoeStaff
	INNER JOIN (
		Select  schNo, svyYear, max([Year of Education]) maxYear
		from DimensionLevel
			INNER JOIN Enrollments E
				ON levelCode = E.enLevel
			INNER JOIN SchoolSurvey SS
				ON E.ssID = SS.ssID
		GROUP BY schNo, svyYEar
		) SS
		ON [@ndoeStaff].schNo = SS.schNo
		AND [@ndoeStaff].surveyYear = SS.svyYear
	INNER JOIN DimensionLevel DL
		ON DL.[Year of Education] = SS.maxYear
WHERE [@ndoeStaff].sector is null and support is null

-- and if we still don't hve a sector, default to the unique sector code in that school if there is
UPDATE @ndoeStaff
SET Sector = ILS.uniqueSecCode
FROM @ndoeStaff
	INNER JOIN Schools S
		on [@ndoeStaff].schNo = S.schNo
	INNER JOIN SchoolTypeILSCode ILS
		ON S.schType = ILS.stCode
WHERE [@ndoeStaff].sector is null and support is null
	AND ILS.uniqueSecCode is not null

UPDATE TeacherSurvey
SET tchClass = ClassMin
, tchClassMax = ClassMax
, tchTAM = TAM
, tchStatus = TS.statusCode
, tchSupport = Support
, tchSector = case when Support is null then Sector else null end
FROM TEacherSurvey
	INNER JOIN @ndoeStaff N
	ON TeacherSurvey.tchsID = N.tchsID
	INNER JOIN lkpTeacherStatus TS
		ON N.staffType = TS.statusDesc


END
GO

