SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 9 11 2022
-- Description:	Retireve staff numbers for XY charts using warehouse
-- =============================================
CREATE PROCEDURE [warehouse].[schoolDataSetEnrolment]
	-- Add the parameters for the stored procedure here
	@year int
	, @dataItem nvarchar(100)
	, @filter nvarchar(10) = null
	, @schoolNo nvarchar(50) = null
	, @parameter nvarchar(100) = null
AS
BEGIN
	SET NOCOUNT ON;
	if (@filter is not null) begin
		if not exists(Select codeCode from lkpLevels WHERE codeCode = @filter) begin
			raiserror('Invalid level code: %s', 16,1,@filter)
			return
		end
	end

	if not exists(select * from warehouse.enrol WHERE surveyYear = @year) begin
		raiserror('No enrolment data for year: %i', 16,1,@year)
		return
	end


	select schNo
	, isnull(sum(
	case @dataItem
		when 'enrol' then Enrol
		when 'rep' then Rep
		when 'trin' then Trin
		when 'trout' then Trout
		when 'expelled' then Expelled
		when 'psa' then PSA
		when 'completed' then Completed

	end),0) DataValue
	, min(EStimate) Estimate
	, null Quality
	from warehouse.enrol
	WHERE (schNo = @schoolNo OR @schoolNo is null)
	AND (@filter = ClassLevel or @filter is null)
	AND (@parameter = GenderCode or @parameter is null)
	AND surveyYear = @Year
	GROUP BY schNo
	ORDER BY schNo

END
GO

