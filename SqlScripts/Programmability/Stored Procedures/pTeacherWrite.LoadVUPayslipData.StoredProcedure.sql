SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 6 2010
-- Description:	Load payslip info fro the Vanuatu format
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[LoadVUPayslipData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


begin try
	begin transaction

-- first remove any exisitng payslips corresponding to entries in the file
-- we'll reload these
	DELETE FROM TeacherPayslips
	FROM TeacherPAyslips
	INNER JOIN mofTransfer.VUPayslipData  PD
		ON TeacherPayslips.tpsPayroll = PD.emp_ID
		AND TeacherPayslips.tpsPeriodEnd = PD.pay_period_to_date

	-- and insert
	INSERT INTO [VemisLive].[dbo].[TeacherPayslips]
			   ([tpsPeriodStart]
			   ,[tpsPeriodEnd]
			   ,[tpsGross]
			   ,[tpsTitle]
			   ,[tpsSalaryPoint]
			   ,[tpsPosition]
			   ,[tpsPayPoint]
			   ,[tpsPayroll]
			   ,[tpsNamePrefix]
			   ,[tpsGiven]
			   ,[tpsMiddleNames]
			   ,[tpsSurname]
			   ,[tpsNameSuffix]
			   ,[tpsPaySlip]
			   ,[tpsXML])


		Select
			pay_period_from_date
			, pay_period_to_date
			, total_gross_pay_amt
			, rtrim(position)
			, rtrim(base_rate_table_code)
			, emp_job_or_position
			, rtrim(prim_disbursal_loc_code)
			, rtrim(emp_ID)
		, N.Honorific
		, N.FirstName
		, N.MiddleName
		, N.LastName
		, N.Suffix
		, null
		, null
		from
		(
		Select DISTINCT
			pay_period_from_date
			, pay_period_to_date
			, total_gross_pay_amt
			, prim_disbursal_loc_code
			, position
			, base_rate_table_code
			, emp_job_or_position
			, emp_ID
			, stuff(emp_name,charindex(' ', emp_name,1),0,',') theName
		from mofTransfer.VUPayslipData
		) D
			CROSS APPLY common.fnParseName(theName) N
		WHERE emp_job_or_position like 'T%'

	-- maintain any new paypoint codes
	INSERT INTO PayPointcodes
	(
	payptCode
	, payptDesc
	)
	SELECT rtrim(code)
	, rtrim(descp)
	FROM
	(
	SELECT DISTINCT prim_disbursal_loc_code code
	, disbursal_location_descp descp
		from mofTransfer.VUPayslipData
	) PD
	WHERE pd.code not in (select payptCode from PayPointCodes)


	-- remove the contents of the upload table


	commit transaction


end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch


END
GO

