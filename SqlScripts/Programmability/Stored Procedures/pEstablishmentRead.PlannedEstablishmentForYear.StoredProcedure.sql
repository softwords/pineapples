SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 10 2009
-- Description:
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[PlannedEstablishmentForYear]
	-- Add the parameters for the stored procedure here
	@EstYear int
	, @UseAuthorityDate bit = 0
-- the next fields are filtering fields

	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @AsAtDate datetime		-- date for reporting the current position
	declare @NumPointIncrements int	-- number of points to increment current positions when estimating future salary
	declare @AdjustNominalSalaryPoint int
	declare @RetainUnderPays int		-- if this flag is on, positions currently underpaid will be assumed to stay underpaid

	-- get the parameter values set up on the establishment control

	begin try

		declare @Summary int
		Select @Summary = 0

		SELECT
			@AsAtDate = case @UseAuthorityDate when 0 then estcSnapshotDate else estcAuthorityDate end
			, @NumPointIncrements = estcIncrementPoints
			, @AdjustNominalSalaryPoint = estcNominalPointAdjust
			, @RetainUnderPays = estcRetainUnderpays
		FROM
			EstablishmentControl
		WHERE
			estcYear = @EstYear

		-- now invoke the main procedure with these parameters

		exec pEstablishmentRead.PlannedEstablishmentSummary
				@Summary
				, @EstYear
				, @AsAtDate
				, @NumPointIncrements
				, @AdjustNominalSalaryPoint
				, @RetainUnderPays
				, @SchoolNo
				, @Authority
				, @SchoolType
	end try
	begin catch

		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch

    -- Insert statements for procedure here
END
GO

