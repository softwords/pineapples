SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 2 2010
-- Description:	Find the island mutliplier for a school
-- =============================================
CREATE PROCEDURE [common].[getIslandMultiplier]
	-- Add the parameters for the stored procedure here
	@Island nvarchar(10) ,
	@Year int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @Island is null begin
	Raiserror('No island supplied', 16,1)
end

if (@Year is null) begin
	Select @Year = 0
end
declare @mult float;
Select TOP 1 @mult = iMultFactor from IslandValuationRates
WHERE iCode = @Island
AND (imultYear <= @Year or @Year = 0)
ORDER BY imultYear DESC

Select isnull(@mult,1) MultFactor

END
GO

