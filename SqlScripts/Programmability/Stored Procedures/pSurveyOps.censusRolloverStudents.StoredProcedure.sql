SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--===========================================
-- Author:		Brian Lewis
-- Create date: 10 August 2021
-- Description:	Create a recordset to populate the census workbook staff page
/*
''
'' the rollover depends on these fields of the Src
'' - Completed
'' - Outcome
'' - Grade

'' Outcome = Repeat - Rollover Grade is current grade, school is current school
''
'' COMPLETED - NO
'' -> Outcome = Repeat - Rollover Grade is current grade, school is current school
'' -> any other - no rollover

'' COMPLETED - YES
'' Grade = Last Grade -> no rollover created
'' Otherwise -
-- rollover grade is next grade
--   if school teaches that next grade, the rollover school is current school
--	 But if the school does not teach the next level, (e.g. a Kinder school promotes to Grade 1)
--   then we find the 'promotedefault' school by examing previous promotions in StudentEnrolment
--

'' TRANSFERS - transfers  out will be placed in the target school if supplied

*/
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusRolloverStudents]
	-- Add the parameters for the stored procedure here
	@SurveyYear int
	, @schoolNo nvarchar(50) = null
	, @districtCode nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @ON nvarchar(1) = 'X';
	Declare @OFF nvarchar(1) = null;

	-- table of promotion grades

DECLARE @DEFAULT_LEVEL_SRC nvarchar(30) ='Default'

-- This table tracks the class levels that correspond to each year of education at a school
-- It also shows how this class level is derived:
-- Enrolment - from enrolments at that level
-- SchoolType - the class level is part of the schooltype
-- Default - from the Default education path - there are no enrolments at the year of education in the school
-- and no class level at the year of ed.
--
DECLARE @promotions TABLE
(
Schno nvarchar(50),
ClassLevelCode nvarchar(10),
ClassLevelYearOfEd int,
PromoteLevelCode nvarchar(10),
PromoteLevel nvarchar(20),
ClassLevelSrc nvarchar(30),
PromoteSrc nvarchar(30)			-- status of this level at the school,
--ie actual Enrolment, or is a Default level code to use for this year of ed
)

--for use in calculating PromoteDefault
DECLARE @movesumm TABLE
(
fromSchool nvarchar(50)
, fromClass nvarchar(10)
, toSchool  nvarchar(50)
, toClass	nvarchar(10)
, Num		int
)

-- most common destination school from out-of-school promotions
-- from <fromSchool>, <fromClass>
-- used in Case 4 - out of school promotions
DECLARE @PromoteDefault TABLE
(
fromSchool nvarchar(50)
, fromClass nvarchar(10)
, toSchool  nvarchar(50)
)


-- included students
-- This is the output table. We write into this table for each of the rollover cases
DECLARE @s TABLE
(
stueID int,
toSch nvarchar(50),
toLevel nvarchar(20),
Source nvarchar(20),
SourceSchool nvarchar(200)
)


/*************************************************************
-- PREPARATION - -assemble the temp tables
*************************************************************/

-- populate @promotions

INSERT INTO @promotions
Select SRC.schNo
,SRC.ClassLevel
, SRC.lvlYear
, DEST.ClassLevel PromoteLevelCode
, L.codeDescription PromoteLevel
, SRC.Src
, DEST.Src
from warehouse.SchoolYEarLevelMap SRC
INNER JOIN warehouse.SchoolYEarLevelMap DEST
ON SRC.schNo = DEST.schNo
AND SRC.surveyYear = DEST.surveyYear
AND DEST.lvlYear = SRC.lvlYEar + 1
AND SRC.SurveyYear = @SurveyYear
AND (DEST.Src <> @DEFAULT_LEVEL_SRC or SRC.Src <> @DEFAULT_LEVEL_SRC)
INNER JOIN lkpLevels L
	ON DEST.ClassLevel = L.codeCode
WHERE (SRC.schNo = @schoolNo or @schoolNo is null)

-----Select * from @promotions

-- populate PromoteDefults

-- for each <fromSchool>, <fromClass>, get all the hisotrical target schools
INSERT INTO @movesumm
(fromSchool, fromClass, toSchool, toClass, num)
Select
fromSchool
, fromClass
, toSchool
, toClass
, count(*) Num
from
(
Select
S1.stuID
, S1.stueYear fromYear
, S1.schNo		fromSchool

, S1.stueClass	fromClass
, S2.stueYear	toYear
, S2.schNo		toSchool
, S2.stueClass	toClass


from StudentEnrolment S1
iNNER JOIN lkpLevels L1
ON S1.stueClass = L1.codeCode
iNNER JOIN lkpLevels L2
ON L2.lvlYear = L1.lvlYear + 1		-- one level up
INNER JOIN StudentEnrolment S2
	on L2.codeCode = S2.stueClass
	AND S1.stuID = S2.stuID				-- same pupil
	AND S2.stueYear = S1.stueYear + 1	-- one year later
WHERE S1.schNo <> S2.schNo				-- only consider change of school
) MOVES
GROUP BY
fromSchool
, fromClass
, toSchool
, toClass
ORDER BY
fromSchool
, fromClass
, toSchool
, toClass

-- from the table just created, extract the most common target school
INSERT INTO @PromoteDefault
(
fromSchool
, fromClass
, toSchool
)
Select fromSchool, fromClass, toSchool
FROM
(
Select *
, row_number() OVER (partition by fromSchool, fromClass ORDER BY num DESC) RN
from @moveSumm
) RANKED
where RN = 1

--------Select * from @movesumm
--------Select * from @PromoteDefault

/********************************************************
-- PROCESS ROLLOVER CASES --------------------------
*********************************************************/
--------------------------------------------------
-- CASE 1: first handle the REPEATERS who are easy
--------------------------------------------------
INSERT INTO @s
SELECT stueID
, SE.schNo
, L.codeDescription
, 'Repeater'
, null
FROM StudentEnrolment SE
INNER JOIN Schools S
	ON S.schNo = SE.schNo
LEFT JOIN Islands I
	ON S.iCode = I.iCode
INNER JOIN lkpLevels L
	ON SE.stueClass = L.codeCode
WHERE stueOutcome = 'To Repeat'
AND SE.stueYear = @SurveyYear
AND (SE.schNo = @schoolNo OR @schoolNo is null)
AND (I.iGroup = @districtCode OR @DistrictCode is null)


--------------------------------------------------
-- CASE 2: Transfers
--------------------------------------------------
-- we look for a transfer out outcome, and 'Transferred to which school'
-- If there is no 'transfer school', leave the school blank
-- If Completed is explicitly 'Y' or blank, they are a promotion, and code stays as 'Transferred In' - unless promoted from ECE
-- If Completed is explcitly 'N' they are by default repeater, code is Repeater

INSERT INTO @s
SELECT stueID
, DESTSCHOOL.schNo

, case
	when stueCompleted = 'N' then L.codeDescription -- not promoted
	else P.PromoteLevel -- promoted
end

, case
	when stueCompleted = 'N' then 'Repeater'
	when P.ClassLevelYearOfEd = 0  then 'ECE'
	else 'Transferred In'
	end
, SOURCESCHOOL.schName		-- source school: name is exported
FROM StudentEnrolment SE
LEFT JOIN @promotions P
	ON SE.schNo = P.schNo
	AND SE.stueClass = P.ClassLevelCode
	AND P.PromoteSrc <> @DEFAULT_LEVEL_SRC
INNER JOIN Schools SOURCESCHOOL
	ON SE.schNo = SOURCESCHOOL.schNo

LEFT JOIN Schools DESTSCHOOL
	ON DESTSCHOOL.schName = SE.stueData.value('(row/@Transferred_To_Which_School)[1]', 'nvarchar(200)')
-- the district we want is that of the dest school, if not defined, of the source school
LEFT JOIN Islands I
	ON COALESCE(DESTSCHOOL.iCode, SOURCESCHOOL.iCode) = I.iCode
INNER JOIN lkpLevels L
	ON SE.stueClass = L.codeCode
WHERE stueOutcome = 'Transferred Out'
AND SE.stueYear = @SurveyYear
--- if producing for a single school, include this record on the transfer school list
-- if transfer school is known, otherwise on the source school list
AND (coalesce(DESTSCHOOL.schNo, SE.schNo) = @schoolNo OR @schoolNo is null)
AND (I.iGroup = @districtCode OR @DistrictCode is null)
AND (
		-- issue 1273 ignore international transfers Out
		SE.stueData.value('(row/@Transferred_To_Which_School)[1]', 'nvarchar(200)') is null
		OR SE.stueData.value('(row/@Transferred_To_Which_School)[1]', 'nvarchar(200)') <> 'International'
	)
--------------------------------------------------
-- CASE 3: regular PROMOTIONS (same school)
--------------------------------------------------
INSERT INTO @s
SELECT stueID
, SE.schNo
, P.PromoteLevel
, case when P.ClassLevelYearOfEd = 0 then 'ECE' end
, null
FROM StudentEnrolment SE
 -- note the inner join here means that the student
 -- will not be included if there is no promotion path
 -- at the school. Excluding PromoteSrc = 'Default' means this school teaches the promotion level
INNER JOIN @promotions P
	ON SE.schNo = P.schNo
	AND SE.stueClass = P.ClassLevelCode
	AND P.PromoteSrc <> @DEFAULT_LEVEL_SRC
INNER JOIN Schools S
	ON S.schNo = SE.schNo
LEFT JOIN Islands I
	ON S.iCode = I.iCode
WHERE stueOutcome is null
AND (stueCompleted = 'Y' or stueCompleted is null)
AND SE.stueYear = @SurveyYear
AND (SE.schNo = @schoolNo OR @schoolNo is null)
AND (I.iGroup = @districtCode OR @DistrictCode is null)

--------------------------------------------------
-- CASE 4: promotions that probably mean a change of school
--------------------------------------------------
--
-- in particular ECE pu0ils at a Kinder, assumed to promote to Yr1, but that class level is not taught at the Kinder
-- To handle this we make a best guess at the target school, based on previous history of StudentEnrolments
-- , and put the current school in 'transfer from'

INSERT INTO @s
SELECT stueID
, PD.toSchool		-- use the default promote school if we have one
, P.PromoteLevel
, case when P.ClassLevelYearOfEd = 0 then 'ECE' end
, S.schName
FROM StudentEnrolment SE
 -- the student
 -- will ONLY be included if there is no promotion path
 -- at the school ie school does not teach this level
 -- (so promotion is a Default class level for this year of ed)
INNER JOIN @promotions P
	ON SE.schNo = P.schNo
	AND SE.stueClass = P.ClassLevelCode
	AND P.PromoteSrc = @DEFAULT_LEVEL_SRC

-- check if we can find a default promotion
LEFT JOIN @PromoteDefault PD
	ON SE.schNo = PD.fromSchool
	AND SE.stueClass = PD.fromClass
INNER JOIN Schools S
	ON S.schNo = SE.schNo
--- now that the school may be balnk ( for unknown transfers or promotions out-of-school-range
--- we should select on district based first on the school on the list, second the source school
LEFT JOIN Islands I
	ON S.iCode = I.iCode
WHERE stueOutcome is null
AND (stueCompleted = 'Y' or stueCompleted is null)
AND SE.stueYear = @SurveyYear
AND (COALESCE(PD.toSchool, SE.schNo) = @schoolNo OR @schoolNo is null)
AND (I.iGroup = @districtCode OR @DistrictCode is null)

-- finally return the Student table. Columns in table (that may be editable)
-- have preference over columns recorded in the Xml, where available
-- columns that are calculated on the target workbook also do not need to be included
SELECT SE.schNo, SE.stueYear,

v.value('@Index', 'int') [ RowIndex]

--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')											[State]
, S.schName			[School_Name]
--, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
--, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                                   [School_Type]
-- use the best known values for Name, DoB, Gender, Student_ID
-- improvements made to data, including through Deduplicating,
-- will be preserved
, SE.stuCardID																			[National_Student_ID]
, SE.stuGiven																			[First_Name]
, SE.stuMiddleNames																		[Middle_Name]
, SE.stuFamilyName																		[Last_Name]
, G.codeDescription																		[Gender]
, SE.stuDoB																				[Date_of_Birth]
, case stuDobEst when 1 then 'Yes' end [DoB_Estimate]
-- age is calculated

, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                            [Citizenship]
-- translated from stuEtnicity
, ETH.codeDescription	[Ethnicity]

, nullif(ltrim(v.value('@Language', 'nvarchar(100)')),'')                            [Language]
-- ECE Attender is an attribute of student
, case when ECEAttenderID is not null then 'Yes' else null end						[Attended_ECE]

-- handling of these fields is the core of the rollover logic
, LIST.toLevel																				[Grade_Level]
, LIST.Source																			[From]
, LIST.sourceSchool         [Transferred_From_which_school]

-- on load, all special ed fields are translated into codes and stored as fields
-- we retrieve the description from the codes
, case stueSpEd when 1 then 'Yes' end [SpEd_Student]
, spedECE.codeDescription [IDEA_SA_ECE]
, spedSA.codeDescription [IDEA_SA_School_Age]
, DIS.codeDescription [Disability]
, nullif(ltrim(v.value('@English_Learner', 'nvarchar(100)')),'')								  [English_Learner]
, case stueSpEdIep when 1 then 'Yes' when 0 then 'No' end [Has_IEP]
, case when stueSpEdHasAccommodation = 1 then 'Yes' end [Has_SBA_Accommodation]
, spEdAcc.codeDescription [Type_Of_Accommodation]
, spEdAT.codeDescription [Assessment_Type]
, spEDX.codeDescription [Exiting]
-- outcome fields are not rolled over
, v.query('.')																			[rowXml]
from @s LIST
INNER JOIN StudentEnrolment SE
	ON List.stueID = SE.stueID
INNER JOIN lkpGender G
	ON SE.stuGender = G.codeCode
OUTER APPLY SE.stueData.nodes('row') as V(v)
LEFT JOIN Schools S
	ON S.schNo = LIST.toSch

LEFT JOIN Islands I
	ON S.iCode = I.iCode
LEFT JOIN lkpEthnicity ETH
	ON ETH.codeCode = SE.stuEthnicity
LEFT JOIN lkpSpEdAccommodations spEdAcc
	ON SE.stueSpEdAccommodation = spEdAcc.codeCode
LEFT JOIN lkpSpEdEnvironment spEdEnv
	ON SE.stueSpEdEnv = spEdEnv.codeCode
LEFT JOIN lkpSpEdAssessmentTypes spEdAT
	ON SE.stueSpEdAssessment = spEdAT.codeCode
LEFT JOIN lkpDisabilities DIS
	ON SE.stueSpEdDisability = DIS.codeCode
LEFT JOIN lkpSpEdExits spEdX
	on SE.stueSpEdExit = spedX.codeCode
-- handle the two environments lists, so we get two separate fields in the output
LEFT JOIN lkpSpEdEnvironmentECE spedECE
	on SE.stueSpEdEnv = spedECE.codeCode
LEFT JOIN lkpSpEdEnvironmentSA spedSA
	on SE.stueSpEdEnv = spedSA.codeCode
LEFT JOIN
	(
	-- this recordset is to detect ECE attenders in three ways:
	-- we have an ECE enrolment - definitive
	-- G1 record has From 'ECE'
	-- Nominated on a previous survey collection as an ECE attender
	Select DISTINCT stuID ECEAttenderID
	FROM StudentEnrolment SE2
	WHERE
	lvlYear < 1 OR
	stueFrom = 'ECE' OR
	stueData.value('(//row/@Attended_ECE)[1]', 'nvarchar(1)') = 'Y'
	) ECEAttenders
	ON ECEAttenderID = SE.stuID
ORDER BY COALESCE(LIST.toSch, SE.schNo), LIST.toLevel, SE.stuFamilyName, SE.stuGiven
END
GO

