SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 11 2013
-- Description:	Report on curriculum ,ed system, medium of instruction
-- from school survey -
-- principally used by somalia ( CS), curriculum CS SL PL
-- =============================================
CREATE PROCEDURE [dbo].[PIVSurvey_Enrol]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,		-- ENROL is required?
	@Group nvarchar(30) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	create table #tmppivcols
	(
		ssID int,
		GenderCode nvarchar(1),
		Gender nvarchar(20),
		LevelCode nvarchar(10),
		Level nvarchar(30),
		edLevelCode nvarchar(10),
		[Education Level] nvarchar(100),
		Enrol int
	)
	SET NOCOUNT ON;
	print 'Enrol'
	if (@Group = 'SCHOOL') begin

		INSERT INTO #tmppivcols(ssID, Enrol)
		Select ssID, ssEnrol Enrol
		FROM SchoolSurvey


	end

	else
	   INSERT
	   INTO #tmppivcols
	   Select ssID, GenderCode, Gender, LevelCode, Level, edLevelCode, [Education Level], Enrol
	   FROM PIVColsEnrol

	exec dbo.PIVSurvey_EXEC

   			@DimensionColumns,
			'ENROL', -- @DataColumns,
			@Group,
			@SchNo,
			@SurveyYear
END
GO

