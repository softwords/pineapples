SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22/10/2019
-- Description:	Identify duplicate teachers
-- =============================================
CREATE PROCEDURE [pTeacherOps].[DuplicateTeacherFilterIDs]
	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	 @MatchGiven int = NULL
	, @MatchSurname int = NULL
	, @MatchPayroll int = NULL
	, @MatchProvident int = NULL
	, @MatchRegister int = null
	, @MatchDoB int = null
	, @MatchGender int = null
	, @Given nvarchar(100) = null
	, @Surname nvarchar(100) = null
	, @District nvarchar(5) = null

	, @xmlfilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @keysAll TABLE
	(
	tID int,
	dupID int,
	recNo int IDENTITY PRIMARY KEY
	)

	-- use a helper table for District
	DECLARE @teacherDistricts TABLE
	(
		tId int
	)

if (@District is not null) begin
	INSERT INTO @teacherDistricts
	( tID )
	Select DISTINCT tID from TeacherSurvey TS
	INNER JOIN SchoolSurvey SS
		ON TS.ssID = SS.ssID
	INNER JOIN Schools S
		ON SS.schNo = S.schNo
	INNER JOIN lkpIslands I
		ON S.iCode = I.iCode
	WHERE I.iGroup = @District
end

	INSERT INTO @KeysAll (TID, DupID)
Select T.tID
, DUP.tID
FROM TeacherIdentity T
INNER JOIN TeacherIdentity DUP
ON (@MatchGiven is null
or (@MatchGiven = 1 AND T.tGiven = DUP.tGiven)
OR (@MatchGiven = 0 AND isnull(T.tGiven,'') <> isnull(DUP.tGiven,''))
)
AND (@MatchSurname is null
or (@MatchSurname = 1 AND T.tSurname = DUP.tSurname)
OR (@MatchSurname = 0 AND isnull(T.tSurname,'') <> isnull(DUP.tSurname,''))
)
AND (@MatchPayroll is null
or (@MatchPayroll = 1 AND T.tPayroll = DUP.tPayroll)
OR (@MatchPayroll = 0 AND isnull(T.tPayroll,'') <> isnull(DUP.tPayroll,''))
)
AND (@MatchProvident is null
or (@MatchProvident = 1 AND T.tProvident = DUP.tProvident)
OR (@MatchProvident = 0 AND isnull(T.tProvident,'') <> isnull(DUP.tProvident,''))
)
AND (@MatchRegister is null
or (@MatchRegister = 1 AND T.tRegister = DUP.tRegister)
OR (@MatchRegister = 0 AND isnull(T.tRegister,'') <> isnull(DUP.tRegister,''))
)
AND (@MatchDoB is null
or (@MatchDoB = 1 AND T.tDoB = DUP.tDoB)
OR (@MatchDoB = 0 AND isnull(T.tDoB,'1901-01-01') <> isnull(DUP.tDoB,'1901-01-01'))
)
AND (@MatchGender is null
or (@MatchGender = 1 AND T.tSex = DUP.tSex)
OR (@MatchGender = 0 AND isnull(T.tSex,'') <> isnull(DUP.tSex,''))
)
AND (@Given is null
OR T.tGiven like @Given + '%'
OR DUP.tGiven like @Given + '%'
)
AND (@Surname is null
OR T.tSurname like @Surname + '%'
OR DUP.tSurname like @Surname + '%'
)
AND (@District is null
	OR ( T.tID in (Select tID from @teacherDistricts)
	AND DUP.tID in (Select tID from @teacherDistricts)
	)
)

AND Coalesce(@MatchGiven, @MatchSurname, @MatchPayroll, @MatchProvident,
	@MatchRegister, @MatchDoB, @MatchGender) is not null

WHERE t.tID > DUP.tID

ORDER BY
-- strings
		case @SortColumn
			when 'Given' then T.tGiven
			when 'Surname' then T.tSurname
		end,
		--numerics
		case @sortColumn
			when 'DoB' then T.tDoB
		end,
		T.tID
OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT tID
		, DupID
		, RecNo
		FROM
		(
			Select tID, DupID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT TID, DupID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

