SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 4 2010
-- Description:	Fix the totals on the SchoolSurvey record.
-- This may be required if something goes worng with the trigger.
-- Non-destructive, so anybody with permission to the table should be able to do this.
-- =============================================
CREATE PROCEDURE [common].[calcSurveyEnrolTotals]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET ANSI_WARNINGS OFF;
	SET NOCOUNT ON;

 UPDATE SchoolSurvey
		set ssenrolM = M
			, ssEnrolF = F
			, ssEnrol = Tot
		FROM SchoolSurvey
		LEFT JOIN
		(Select ssID, sum(enM) M, sum(enF) F, sum(enSum) Tot
		from Enrollments GROUP BY ssID ) E
		ON SchoolSurvey.ssID = E.ssID

	return @@ROWCOUNT

END
GO

