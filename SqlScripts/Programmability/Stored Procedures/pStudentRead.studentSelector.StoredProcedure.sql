SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 June 2024
-- Description:	Selector for finder control based on student
--
-- Revision History: 8 June 2024 Allow 'constraints' to limit the selection
-- attendance at a school, year
-- =============================================
CREATE PROCEDURE [pStudentRead].[studentSelector]
	@s nvarchar(50),
	@year int=null,
	@schoolNo nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @ss nvarchar(100)
declare @sg nvarchar(100)
declare @k int
declare @twopartSearch int = 0

-- first split to see if we read on two parts of the name
-- supported formats are
-- * Family, Given
-- * Given Family
select @k =charindex(',', @s)
print @k
if (@k >= 1) begin
	select @ss = rtrim(left(@s, @k-1))
	, @sg = ltrim(substring(@s,@k + 1, len(@s) - @k + 1))
	, @twoPartSearch = 1
end
else begin
	select @k =charindex(' ', @s)
	if (@k >= 1) begin
		select @sg = ltrim(left(@s, @k-1))
		, @ss = ltrim(substring(@s,@k + 1, len(@s) - @k + 1))
		, @twoPartSearch = 2
	end
end

print @s
print @ss
print @sg

if (isnumeric(@s) = 1) begin
select top 30 stuFamilyName + ', ' + stuGiven N, stuID C, stuCardID P,
	stuFamilyName F, stuGiven G
	From Student_
	 WHERE stuCardID like @s + '%'
	 return
end
if @year is null and @schoolNo is null begin
	if @twoPartSearch > 0 begin
		Select @ss = nullif(@ss,''), @sg = nullif(ltrim(rtrim(@sg)),'')

		Select TOP 30
		case @twopartSearch
			when 1 then stuFamilyName + ', ' + stuGiven
			when 2 then	stuGiven + ' ' + stuFamilyName
		end N, stuID C, stuCardID P,
		stuFamilyName F, stuGiven G, stuDob D
		from Student_
		WHERE (stuFamilyName like @ss +'%' or @ss is null)
		AND (stuGiven like @sg + '%' or @sg is null)

		ORDER BY stuFamilyName, stuGiven
		select @@rowcount num

		return
	end

	-- if not doing a 2 part search, search n Family and given, but prioiritise family
	select top 30 stuFamilyName + ', ' + stuGiven N, stuID C, stuCardID P,
		stuFamilyName F, stuGiven G, stuDob D
		From
		(Select TOP 5000 stuFamilyName, stuGiven, stuID, stuCardID, stuDob,
		case
			when stuFamilyName like @s +'%' then 0
			when stuGiven like @s +'%' then 1
			when stuFamilyName like '%' + @s +'%' then 2
			when stuGiven like '%' + @s +'%' then 3
		end Pty
		from Student_
		WHERE stuFamilyName like '%' + @s +'%'
		or stuGiven like '%' + @s +'%'
		ORDER BY stuFamilyName, stuGiven
		) SUB
		ORDER BY Pty, stuFamilyName, stuGiven

	select @@rowcount num
	return
end

-- support contraints based on enrolment - year / schoolNo
-- note these queries will work even when year and schoolno arae not supplied
-- but the simpler case avoids the join for performance
if @year is not null or @schoolNo is not null begin
	if @twoPartSearch > 0 begin
		Select @ss = nullif(@ss,''), @sg = nullif(ltrim(rtrim(@sg)),'')

		Select TOP 30
		case @twopartSearch
			when 1 then stuFamilyName + ', ' + stuGiven
			when 2 then	stuGiven + ' ' + stuFamilyName
		end N, S.stuID C, stuCardID P,
		stuFamilyName F, stuGiven G, stuDob D
		from Student_ S
			INNER JOIN StudentEnrolment_ SE
				ON S.stuID = SE.stuID
				AND (stueYear = @year or @year is null)
				AND (SE.schNo = @schoolNo or @schoolNo is null)
		WHERE (stuFamilyName like @ss +'%' or @ss is null)
		AND (stuGiven like @sg + '%' or @sg is null)
		ORDER BY stuFamilyName, stuGiven

		select @@rowcount num

		return
	end

	-- if not doing a 2 part search, search n Family and given, but prioiritise family
	-- DISITNCT avoids multiples in StudentEnrolment_
	select DISTINCT top 30  stuFamilyName + ', ' + stuGiven N, stuID C, stuCardID P, Pty,
		stuFamilyName F, stuGiven G, stuDob D
		From
		(
		Select TOP 5000 stuFamilyName, stuGiven, S.stuID, stuCardID, stuDob,
		case
			when stuFamilyName like @s +'%' then 0
			when stuGiven like @s +'%' then 1
			when stuFamilyName like '%' + @s +'%' then 2
			when stuGiven like '%' + @s +'%' then 3
		end Pty
		from Student_ S
			INNER JOIN StudentEnrolment_ SE
				ON S.stuID = SE.stuID
				AND (stueYear = @year or @year is null)
				AND (SE.schNo = @schoolNo or @schoolNo is null)
		WHERE stuFamilyName like '%' + @s +'%'
		or stuGiven like '%' + @s +'%'
		ORDER BY stuFamilyName, stuGiven
		) SUB
		ORDER BY Pty, stuFamilyName, stuGiven
	select @@rowcount num

	return
end

END
GO

