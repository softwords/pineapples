SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 03 2018
-- Description:	Scholarship filter
--              TODO - Add ability to filter by currently enrolled school
-- =============================================
CREATE PROCEDURE [pScholarshipRead].[ScholarshipFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@ScholarshipID int = null,
	@ScholarshipType nvarchar(10) = null,
	@ScholarshipStatus nvarchar(10) = null,
	@ScholarshipStatusChange datetime = null,
	@ScholarshipYear int = null,

	@StudentID uniqueidentifier = null,
	@StudentCardID nvarchar(20) = null,
    @StudentGiven nvarchar(50) = null,
    @StudentFamilyName  nvarchar(50) = null,
    @StudentDoB  date = null,
	@StudentGender nvarchar(1) = null,
	@StudentEthnicity nvarchar(200) = null,

	-- selection based on program of study
	@EnrolledAt nvarchar(50) = null,			--- the institution
	@EnrolYear int = null,						--  year
	@EnrolLevel int	= null,						-- from enrolment level 1 2 3 4 = Freshman, sophomore etc
	@EnrolField nvarchar(10) = null,
	@EnrolFieldGroup nvarchar(10) = null,
	@GPAMin decimal(3,2) = null,
	@GPAMax decimal(3,2) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
	ID int
	, recNo int IDENTITY PRIMARY KEY
	)
	DECLARE @enrolMatch TABLE
	(
		ID uniqueidentifier
	)
	DECLARE @UseEnrol int = 0;

	-- interpret enrolyear - if it is 0:
	-- if school supplied, the most recent enrolment for the school
	-- if no school, the most recent year for any school
	--
	if (@EnrolYear = 0) begin
		select @enrolYear = max(stueYear)
		from StudentEnrolment_
		WHERE (schNo = @EnrolledAt or @EnrolledAt is null)
	end
	print @EnrolYear
	if ( @EnrolledAt is not null) or (@EnrolYear is not null) or (@EnrolLevel is not null)
	  begin
		select @UseEnrol = 1
	end

	-- prepare given and family names
	-- replace dos search chars * ? with Sql alternatives

	Select @StudentGiven = common.prepSearchString(@StudentGiven, DEFAULT, DEFAULT)
	,  @StudentFamilyName = common.prepSearchString(@StudentFamilyName, DEFAULT, DEFAULT)

	print @StudentGiven

	INSERT INTO @keys
	(ID)
	SELECT schoID
	FROM Scholarships S
	WHERE
	(stuID = @StudentID or @StudentID is null)
	AND (S.stuCardID = @StudentCardID or @StudentCardID is null)
	AND (S.stuGiven like @StudentGiven or @StudentGiven is null)
	AND (S.stuFamilyName like @StudentFamilyName or @StudentFamilyName is null)
	AND (S.stuDoB = @StudentDoB or @StudentDoB is null)
	AND (S.stuGender = @StudentGender or @StudentGender is null)
	AND (S.stuEthnicity = @StudentEthnicity or @StudentEthnicity is null)

	AND (S.scholCode = @ScholarshipType or @ScholarshipType is null)
	AND (S.schrYear = @ScholarshipYear or @ScholarshipYear is null)
	AND (S.schoStatus = @ScholarshipStatus or @ScholarshipStatus is null)
	AND (@useEnrol = 0 or S.schoID in
		(
		Select schoID
		FROM
		ScholarshipStudy_ SS
			LEFT JOIN FieldOfStudy FS
				ON SS.fosCode = FS.fosCode
		WHERE -- (SE.schNo = @EnrolledAt or @EnrolledAt is null)
		( gyIndex = @EnrolLevel or @EnrolLevel is null)
		AND (posYear = @EnrolYear or @EnrolYear is null)
		AND (SS.instCode = @EnrolledAt or @EnrolledAt is null)
		AND (SS.fosCode = @EnrolField or @EnrolField is null)
		AND (fosgrpCode = @EnrolFieldGroup or @EnrolFieldGroup is null)
		AND (posGPA >= @GPAMin or @GPAMin is null)
		AND (posGPA <= @GPAMax or @GPAMax is null)
		)
	)

	ORDER BY
		CASE @sortColumn			-- strings


			WHEN 'stuCardID' then stuCardID
			WHEN 'stuGiven' then stuGiven
			WHEN 'stuFamilyName' then stuFamilyName
			WHEN 'stuEthnicity' then stuEthnicity
			WHEN 'stuGender' then stuGender

			WHEN 'StudentCardID' then stuCardID
			WHEN 'StudentGiven' then stuGiven
			WHEN 'StudentFamilyName' then stuFamilyName
			WHEN 'StudentEthnicity' then stuEthnicity
			WHEN 'StudentGender' then stuGender

			WHEN 'schoStatus' then schoStatus

			ELSE null
			END,
		CASE @sortColumn -- dates
			WHEN 'StudentDoB' then stuDoB
			WHEN 'stuDoB' then stuDoB

			WHEN 'schoStatusDate' then schoStatusDate
			WHEN 'schoExpectedCompletion' then schoExpectedCompletion
			WHEN 'schoAppliedDate'   then schoAppliedDate
			WHEN 'schoCompliantDate' then schoCompliantDate
			WHEN 'schoAwardedDate'   then schoAwardedDate
			WHEN 'schoCompletedDate' then schoCompletedDate

			WHEN 'StatusDate' then schoStatusDate
			WHEN 'ExpectedCompletion' then schoExpectedCompletion
			WHEN 'AppliedDate'   then schoAppliedDate
			WHEN 'CompliantDate' then schoCompliantDate
			WHEN 'AwardedDate'   then schoAwardedDate
			WHEN 'CompletedDate' then schoCompletedDate
			ELSE null
		END,
		schoID

	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @Keys
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)
				)
		ORDER BY RecNo
	end
	else begin

		SELECT ID
		, RecNo
		FROM @Keys
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
END
GO

