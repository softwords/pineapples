SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis and Ghislain Hachey
-- Description: Filter accreditions in the warehouse
-- Create date: 27 04 2017 / 21 8 2019
-- This is designed for future uses in drilling down into dashboard
-- number to see the accreditations contributing to the total
-- Derived from [pSurveyRead].[SchoolAccreditationFilterIDs]
--  but using warehouse version od dimensional data
-- =============================================
CREATE PROCEDURE [warehouse].[AccreditationFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@SAID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50) = null, --'e.g. GHS100'
	@InspYear nvarchar(50) = null, --- e.g. '2016'
	@InspResult nvarchar(20) = null,
	@FilterColumn nvarchar(10) = null,
	@FilterValue int = null,

	@BestUpTo int = null,

	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@AuthorityGroup nvarchar(10) = null,
	@SchoolType nvarchar(10) = null,

	@XmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		ID int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC warehouse.AccreditationFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	@SAID,
	@School,
	@SchoolNo,
	@InspYear,
	@InspResult,
	@FilterColumn,
	@FilterValue,
	@BestUpTo,

	@District,
	@Authority,
	@AuthorityGroup,
	@SchoolType,
	@xmlFilter


-------------------------------------
-- return results

Select A.*
FROM warehouse.Accreditations A
INNER JOIN @Keys AS K ON A.inspID = K.ID


-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

