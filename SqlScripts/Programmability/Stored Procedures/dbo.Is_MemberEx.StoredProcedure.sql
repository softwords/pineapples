SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 11 2009
-- Description:	check role membership, but a sysadmin is always a member
-- =============================================
CREATE PROCEDURE [dbo].[Is_MemberEx]
	-- Add the parameters for the stored procedure here
	@RoleOrGroup sysname = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select common.fnIs_MemberEx(@RoleOrGroup)

END
GO
GRANT EXECUTE ON [dbo].[Is_MemberEx] TO [public] AS [dbo]
GO

