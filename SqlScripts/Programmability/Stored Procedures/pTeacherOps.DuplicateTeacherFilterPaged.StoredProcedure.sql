SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:
-- =============================================
CREATE PROCEDURE [pTeacherOps].[DuplicateTeacherFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters
	 @MatchGiven int = NULL
	, @MatchSurname int = NULL
	, @MatchPayroll int = NULL
	, @MatchProvident int = NULL
	, @MatchRegister int = null
	, @MatchDoB int = null
	, @MatchGender int = null
	, @Given nvarchar(100) = null
	, @Surname nvarchar(100) = null
	, @District nvarchar(5) = null
	, @XmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		TID int
		, DupID int
		, recNo int PRIMARY KEY
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pTEacherOps.DuplicateTeacherFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	 @MatchGiven
	, @MatchSurname
	, @MatchPayroll
	, @MatchProvident
	, @MatchRegister
	, @MatchDoB
	, @MatchGender
	, @Given
	, @Surname
	, @District
	, @xmlFilter
-------------------------------------
-- return results


Select T.tID
, T.tNamePrefix
, T.tGiven
, T.tMiddleNames
, T.tSurname
, T.tNameSuffix
, T.tPayroll
, T.tProvident
, T.tRegister
, T.tDoB
, T.tSex
-- second copy
, DUP.tID			dupID
, DUP.tNamePrefix	dupNamePrefix
, DUP.tGiven		dupGiven
, DUP.tMiddleNames	dupMiddleNames
, DUP.tSurname		dupSurname
, DUP.tNameSuffix	dupNameSuffix
, DUP.tPayroll		dupPayroll
, DUP.tProvident	dupProvident
, DUP.tRegister		dupRegister
, DUP.tDoB			dupDoB
, DUP.tSex			dupSex
FROM TeacherIdentity T
	INNER JOIN @Keys AS K
		ON T.tID = K.TID
	INNER JOIN TeacherIdentity DUP
		ON DUP.tID = K.DupID
	ORDER BY K.RecNo

-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

