SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 9 2022
-- Description:	Copy standards, benchmarks indicators between exams in bulk.
--				@examCode - exam code: must be supplied
--				@FraomYear : year to copy from
--				@ToYear: year to copy to
-- NOTE: either FronYear or ToYear must be supplied.
-- If FromYear and ToYear, a single copy is made between these two years
-- If FromYear only, copy is made to every exam AFTER fromYear
-- If ToYear only, copy is from most recent BEFORE ToYear
-- =============================================
CREATE PROCEDURE [pExamWrite].[CloneExamFrameBulk]
	-- Add the parameters for the stored procedure here
	@examCode nvarchar(20)
	, @fromYear int
	, @toYear int = null
	, @returnResults int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @results TABLE
(
	yr int,
	msg nvarchar(500)
)
begin try
if (@examCode is null) begin
	raiserror('Exam code must be specified',16,1)
end

if (@fromYear is null and @toYear is null) begin
	raiserror('Either source year or target year must be specified',16,1)
end


if not exists(Select exCode from lkpExamTypes WHERE exCode = @examCode) begin
	raiserror('Exam code not defined: %s',11,1,@examCode)
end

if @fromYear is null begin
	-- most recent year before the target
	Select @fromYear = max(exYear)
	from exams
	WHERE exCode = @examCode
	and exYear < @toYear
end

if (@fromYear is null) begin
	raiserror('Source exam not found: %s %i',11,1,@examCode, @toYear)
end
raiserror('From year = %i',0,1, @fromYEar)


if @toYear is null begin
	Select @toYear = 9999
end

declare @targetYear int
declare @numCopies int = 0

Select @targetYear = min(exYear)
, @numCopies = count(*)
from Exams
WHERE exCode = @examCode
AND exYear > @fromYear
and exYear <= @toYear

if @numCopies = 0 begin
	raiserror('No target exams found: %s from %i to %i',11,1,@examCode, @fromYear, @toYear)
end

while @targetYEar is not null begin
	print @targetYear

	begin try

		exec pExamWrite.CloneExamFrame @examCode, @fromYear, @TargetYear,0
		INSERT INTO @results
		VALUES (@TargetYear, 'Copied from ' + convert(nvarchar(4), @FromYear))
	end try
	begin catch
		INSERT INTO @results
		VALUES (@TargetYear, ERROR_MESSAGE())
	end catch

	Select @targetYear = min(exYear)
	from Exams
	WHERE exCode = @examCode
	AND exYear > @targetYear
	AND exYear <= @toYear
end

if @returnResults = 1 begin
	Select * from @Results
end
end try
begin catch

throw;
end catch

END
GO

