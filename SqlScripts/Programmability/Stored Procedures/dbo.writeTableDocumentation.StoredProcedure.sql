SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 11 2009
-- Description:	wirte table documentatin
-- =============================================
CREATE PROCEDURE [dbo].[writeTableDocumentation]
	-- Add the parameters for the stored procedure here
	@TableName sysname
	, @SchemaName sysname
	, @description nvarchar(4000)
	, @systemTopic nvarchar(100)
	, @dataAccessUI nvarchar(4000)
	, @dataAccessDB nvarchar(4000)
	, @DiagramName nvarchar(100)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @xprops table
(
propName sysname
)
begin try
	begin transaction
	-- make a little temp table

	INSERT INTO @xprops
	Select
		P.name
	from
		sys.extended_properties P
		INNER JOIN sys.tables T
			on P.major_Id = T.object_id
		INNER JOIN sys.schemas S
		ON S.schema_ID = T.schema_ID
	WHERE T.name = @TableName
		AND S.name = @schemaName
		AND P.Minor_ID = 0

	declare @propName sysname

	select @propName = N'MS_Description'
	if EXISTS(Select propName from @xprops WHERE propName = @propName)
		EXEC sys.sp_dropExtendedProperty
			@name= @propName,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName

	select @propName = N'pSystemTopic'
	if EXISTS(Select propName from @xprops WHERE propName = @propName)
		EXEC sys.sp_dropExtendedProperty
			@name= @propName,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName

	select @propName = N'pDataAccessUI'
	if EXISTS(Select propName from @xprops WHERE propName = @propName)
		EXEC sys.sp_dropExtendedProperty
			@name= @propName,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName


	select @propName = N'pDataAccessDB'
	if EXISTS(Select propName from @xprops WHERE propName = @propName)
		EXEC sys.sp_dropExtendedProperty
			@name= @propName,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName


	select @propName = N'pDiagramName'
	if EXISTS(Select propName from @xprops WHERE propName = @propName)
		EXEC sys.sp_dropExtendedProperty
			@name= @propName,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName


	-- the description goes in the common MS_Description
	If (@description is not null)
		EXEC sys.sp_addextendedproperty
			@name=N'MS_Description',
			@value=@description ,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName

	-- systemTopic is the broad area os the system to which this table relates
	If (@systemTopic is not null)
		EXEC sys.sp_addextendedproperty
			@name=N'pSystemTopic',
			@value=@systemTopic ,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName

	-- dataAccessUI shows where this data appears in the UI
	If (@dataAccessUI is not null)
		EXEC sys.sp_addextendedproperty
			@name=N'pDataAccessUI',
			@value=@dataAccessUI ,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName

	-- dataAccessDB describes the principal interfaces (vies SP) to this data inthe database
	If (@dataAccessDB is not null)
		EXEC sys.sp_addextendedproperty
			@name=N'pDataAccessDB',
			@value=@dataAccessDB ,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName

	-- DiagramName is the schema.path identified of a database diagram in this database
	If (@DiagramName is not null)
		EXEC sys.sp_addextendedproperty
			@name=N'pDiagramName',
			@value=@DiagramName ,
			@level0type=N'SCHEMA',
			@level0name= @schemaName,
			@level1type=N'TABLE',
			@level1name=@TableName

	commit transaction
end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

