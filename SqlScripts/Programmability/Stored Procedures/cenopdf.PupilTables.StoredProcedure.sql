SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 6 2021
-- Description:	Proc to customize generation of pdf using
--				cenopdf - this is applied to classrooms and extra classrooms pages
--				to change the dropdown of class levels
-- Refer to http://www.lystech.com/webhelp/default.htm 'Exporting to PDF from a Database'
-- =============================================
CREATE PROCEDURE [cenopdf].[PupilTables]
	@formtype nvarchar(10),	-- either PRI or SEC
	@targetFile nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @schoolType nvarchar(10)
	declare @ClassLevelItems nvarchar(max)

	-- kiribati specific
	Select @schoolType = case @formType when 'PRI' then 'P' when 'SEC' then 'CS' end
	select @ClassLevelItems = cenopdf.ClassLevelItems(@SchoolType)

	declare @dis xml = cenopdf.kvxml('lkpDisabilities')
	declare @small int = 7
	declare @normal int = 7		-- both the same

	Select @targetFile targetFile
	, Dis.k0 [Dis.R.00.K_Text]
	, Dis.v0 [Dis.R.00.V_Text]
	, Dis.k1 [Dis.R.01.K_Text]
	, Dis.v1 [Dis.R.01.V_Text]
	, Dis.k2 [Dis.R.02.K_Text]
	, Dis.v2 [Dis.R.02.V_Text]
	, Dis.k3 [Dis.R.03.K_Text]
	, Dis.v3 [Dis.R.03.V_Text]
	, Dis.k4 [Dis.R.04.K_Text]
	, Dis.v4 [Dis.R.04.V_Text]
	, Dis.k5 [Dis.R.05.K_Text]
	, Dis.v5 [Dis.R.05.V_Text]
	, Dis.k6 [Dis.R.06.K_Text]
	, Dis.v6 [Dis.R.06.V_Text]
	, Dis.k7 [Dis.R.07.K_Text]
	, Dis.v7 [Dis.R.07.V_Text]
	, Dis.k8 [Dis.R.08.K_Text]
	, Dis.v8 [Dis.R.08.V_Text]
	, Dis.k9 [Dis.R.09.K_Text]
	, Dis.v9 [Dis.R.09.V_Text]
	, Dis.k10 [Dis.R.10.K_Text]
	, Dis.v10 [Dis.R.10.V_Text]

	, case when len(v0) > 30 then @small else @normal end [Dis.R.00.V_FontSize]
	, case when len(v1) > 30 then @small else @normal end [Dis.R.01.V_FontSize]
	, case when len(v2) > 30 then @small else @normal end [Dis.R.02.V_FontSize]
	, case when len(v3) > 30 then @small else @normal end [Dis.R.03.V_FontSize]
	, case when len(v4) > 30 then @small else @normal end [Dis.R.04.V_FontSize]
	, case when len(v5) > 30 then @small else @normal end [Dis.R.05.V_FontSize]
	, case when len(v6) > 30 then @small else @normal end [Dis.R.06.V_FontSize]
	, case when len(v7) > 30 then @small else @normal end [Dis.R.07.V_FontSize]
	, case when len(v8) > 30 then @small else @normal end [Dis.R.08.V_FontSize]
	, case when len(v9) > 30 then @small else @normal end [Dis.R.09.V_FontSize]
	, case when len(v10)> 30 then @small else @normal end [Dis.R.10.V_FontSize]

	From cenopdf.kv(@dis) Dis


END
GO

