SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 11 2009
-- Description:	Transform subject data on teacher survey into normalised form
-- =============================================
CREATE PROCEDURE [dbo].[NormaliseTeacherSubjects]
	-- Add the parameters for the stored procedure here
	@TestOnly int = 1
AS
BEGIN

-- first, collect all the subjects that are referenced for each teacher

begin try
	SET NOCOUNT ON;
	begin transaction

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'MISCA')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('MISCA', 'Arts (Other)')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'MISCH')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('MISCH', 'Humanities (Other)')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'MISCS')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('MISCS', 'Science (Other)')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'MISCC')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('MISCC', 'Commerce (Other)')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'MISCT')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('MISCT', 'Trade (Other)')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'MISCR')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('MISCR', 'Religion (Other)')

		-- some specifics we do need
	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'MUSIC')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('MUSIC', 'Music')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'DEV')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('DEV', 'Development Studies')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'CUL')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('CUL', 'Cultural Studies')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'IT')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('IT', 'Information Technology')


	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'SOC')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('SOC', 'Social Science')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'CAR')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('CAR', 'Careers Guidance')

	IF NOT EXISTS(Select subjCode from lkpSubjects WHERE subjCode = 'DESN')
		INSERT INTO lkpSubjects
		(subjCode, subjName)
		VALUES ('DESN', 'Design Technology')

	UPDATE lkpSubjects
	SET subjName = 'Carpentry'
	WHERE subjName = 'Carpentary'

	Select count(*)
	from TeacherSurvey
	WHERE
		tchSubjectMajor is not null
		or tchSubjectMinor is not null
		or tchSubjectMinor2 is not null


	Select *
	INTO #tchSubjects
	From
	(
	Select TS.tchsID
	,TS.tchSubjectMajor
	, TS.tchSubjectMinor
	, TS.tchSubjectMinor2
	, s.split
	, subj.subjCode
	, row_number() over (PARTITION by TS.tchsID ORDER BY U.seq) SubjNum
	from
	TeacherSurvey TS
	INNER JOIN
	(
	select tchsID, tchSubjectMajor rawSubject
	, 1 as seq
	from TeacherSurvey
	WHERE tchSubjectMajor is not null

	UNION

	select tchsID, tchSubjectMinor
	, 2 as seq
	from TeacherSurvey
	WHERE tchSubjectMinor is not null


	UNION

	select tchsID, tchSubjectMinor2
	, 3 as seq
	from TeacherSurvey
	WHERE tchSubjectMinor2 is not null
	) U
	on TS.tchsID = U.tchsID
	OUTER APPLY common.splitText(rawSubject,'/') S
	LEFT JOIN lkpSubjects subj
	ON ( S.split in (subj.subjName, subj.subjCode))
	) U2


	declare @unknowns TABLE
	(
		split nvarchar(50)
		, subjName nvarchar(50)
	)
	INSERT INTO @unknowns
	Select DISTINCT split, null
	from #tchSubjects
	WHERE subjCode is null


	UPDATE @Unknowns
	SET subjName = 'Business Studies'
	WHERE split in
	(
	'Business'
	,'Business Stdies'
	,'Bussiness'
	,'Busuness Studies'
	, 'Management'
	, 'Administration'
	)

	UPDATE @Unknowns
	SET subjName = 'Agriculture'
	WHERE split in
	(
	'Argriculture'
	,'horticulture'
	, 'Piggery'

	)

	UPDATE @Unknowns
	SET subjName = 'Religion (Other)'
	WHERE split in
	(
	'Bible'
	, 'Biblical literacy'
	, 'C'			-- it was entered as C/Education
	, 'RE'
	)

	UPDATE @Unknowns
	SET subjName = 'Life Skills'
	WHERE split in
	(
	'Life Skils'
	,'Life Sklills'
	,'Life-Skills'
	, 'health'
	, 'Health Studies'
	, 'Leadership'
	, 'First Aid'
	)


	UPDATE @Unknowns
	SET subjName = 'Cultural Studies'
	WHERE split in
	(
	'Culture'

	)

	UPDATE @Unknowns
	SET subjName = 'Carpentry'
	WHERE split in
	(
	'Carpentary'
	)

	UPDATE @Unknowns
	SET subjName = 'Development Studies'
	WHERE split in
	(
	'Development'
	)

	UPDATE @Unknowns
	SET subjName = 'Biology'
	WHERE split in
	(
	'Human Science'
	)


	UPDATE @Unknowns
	SET subjName = 'Secretarial Studies'
	WHERE split in
	(
		'Secretarial'
		, 'Typing'
	)


	UPDATE @Unknowns
	SET subjName = 'Information Technology'
	WHERE split in
	(
	'information tech'
	,'IT'
	)

	UPDATE @Unknowns
	SET subjName = 'Social Science'
	WHERE split in
	(
	'Social'
	,'Sociology'
	, 'Soc Science'
	)

	UPDATE @Unknowns
	SET subjName = 'Social Studies'
	WHERE split in
	(
	'Social Study'
	,'S.Study'
	)

	UPDATE @Unknowns
	SET subjName = 'Kiribati Studies'
	WHERE split in
	(
	'Kiribati  Studies'
	, 'local skills'
	)


	UPDATE @Unknowns
	SET subjName = 'Physical Education'
	WHERE split in
	(
	'PE'
	)


	UPDATE @Unknowns
	SET subjName = 'Home Economics'
	WHERE split in
	(
	'Sewing'
	)

	UPDATE @Unknowns
	SET subjName = 'English'
	WHERE split in
	(
	'Literature'
	)


	UPDATE @Unknowns
	SET subjName = 'Religion'
	WHERE split in
	(
		'Theology'
		,'Catechesis'
	)


	UPDATE @Unknowns
	SET subjName = 'Science'
	WHERE split in
	(
	'General Science'
	,'Sscience'
	)

	UPDATE @Unknowns
	SET subjName = 'New Testament Studies'
	WHERE split in
	(
	'New Testament'
	)

	UPDATE @Unknowns
	SET subjName = 'Mathematics'
	WHERE split in
	(
	'Maths'
	)

	UPDATE @Unknowns
	SET subjName = 'Arts (Other)'
	WHERE split in
	(
	'arts'
	,'Expressive Arts'
	, 'Crafts'
	)

	UPDATE @Unknowns
	SET subjName = 'Industrial Arts'
	WHERE split in
	(
	'Ind.Arts'
	)

	UPDATE @Unknowns
	SET subjName = 'Design Technology'
	WHERE split in
	(
	'Design Tech'
	)


	UPDATE @Unknowns
	SET subjName = 'Trade (Other)'
	WHERE split in
	(
	'Electrical'
	,'Mechanic'
	, 'Joinery'
	, 'Welding'
	, 'Building'
	, 'Plumbing'
	, 'Sawmill and plumbing'
	, 'Forestry'
	)

	UPDATE @Unknowns
	SET subjName = 'Home Economics'
	WHERE split in
	(
	'Home Economic'
	)


	UPDATE @Unknowns
	SET subjName = 'Careers Guidance'
	WHERE split in
	(
	'Careers'
	)


	/*

		NULL
		NULL


	*/

	--  now clean up the splits with their matched up values

	UPDATE #tchSubjects
	SET
		split = Subj.subjName
		, subjCode = subj.subjCode
	FROM
		#tchSubjects
		INNER JOIN @Unknowns U
			ON #tchSubjects.split = U.split
		INNER JOIN lkpSubjects Subj
			ON subj.subjName = U.subjName

	-- now we get rid of dups again, and renumber
	-- becuase two subjects entered originally may now just be one

	Select
		tchsID
		, split
		, subjCode
		, row_number() OVER (Partition BY tchsID ORDER BY SubjNum) subjNum
	INTO #tchSubj2
	FRom #tchSubjects
	WHERE subjCode is not null
	print 'Start udpdates'
	-- now we can update
	-- clear first

	UPDATE TeacherSurvey
	SET tchSubjectMajor = null
		, tchSubjectMinor = null
		, tchSubjectMinor2 = null
	FROM
		TeacherSurvey TS
		INNER JOIN #tchSubjects
			ON TS.tchsId = #tchSubjects.tchsID

	UPDATE TeacherSurvey
	SET tchSubjectMajor = subjCode
	FROM
		TeacherSurvey TS
		INNER JOIN #tchSubj2
			ON TS.tchsId = #tchSubj2.tchsID
	WHERE
		#tchSubj2.SubjNum = 1

	UPDATE TeacherSurvey
	SET tchSubjectMinor = subjCode
	FROM
		TeacherSurvey TS
		INNER JOIN #tchSubj2
			ON TS.tchsId = #tchSubj2.tchsID
	WHERE
		#tchSubj2.SubjNum = 2

	UPDATE TeacherSurvey
	SET tchSubjectMinor2 = subjCode
	FROM
		TeacherSurvey TS
		INNER JOIN #tchSubj2
			ON TS.tchsId = #tchSubj2.tchsID
	WHERE
		#tchSubj2.SubjNum = 3

	print 'end updates'

--------
--------	select tchsID
--------		, tchSubjectMajor
--------		, tchSubjectMinor
--------		, tchSubjectminor2
--------	FROM
--------		TeacherSurvey
--------	WHERE tchSubjectMajor is not null

	iF (@TestOnly = 1 ) begin
			Select tchsID, tchSubjectMajor, tchSubjectMinor, tchSubjectMinor2
			FROM TeacherSurvey
			Select * from #tchSubjects

			Select split, Count(*)
		   from   #tchSubjects
			WHERE subjCode is null
			GROUP BY split

		end

	DROP TABLE #tchSubjects
	DROP TABLE #tchSubj2

	iF (@TestOnly = 1 )
			rollback transaction

	else
			commit transaction


end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

