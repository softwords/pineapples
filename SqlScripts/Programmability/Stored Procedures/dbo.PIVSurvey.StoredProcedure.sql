SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for toilets pivot
-- =============================================
CREATE PROCEDURE [dbo].[PIVSurvey]
	@DimensionColumns nvarchar(20) = 'CORE',
	@DataColumns nvarchar(20) = null,
-- for filtering by school and year
	@Group nvarchar(30) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null,
	-- for interactive enquiries - 'quick pivots'
	@RowField nvarchar(50) = null,
	@ColField nvarchar(50) = null,
	@DataField nvarchar(50) = 'Number'


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select @DataColumns = dbo.pivDefaultDataColumns(@DataColumns)

if (@group = 'ENROL') begin
		EXEC dbo.PIVSurvey_Enrol
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear

		return
	end

-- handle some user firendly aliasing of @group, also some logic
if (@Group in ('REP','REPEATERS'))
	-- figure out whether we need to show age
	if (@DataColumns = 'SOMALIA')
		Select @Group = 'REPN'
	else
		SELECT @Group = 'REPA'

Select @group = case @group
			when 'BOARDERS' then 'BRD'
			when 'DROPOUTS' then 'DROP'
			when 'REPEATERSN' then 'REPN'
			when 'REPEATERSAGED' then 'REPA'


			when 'Feeding Program' then 'FEEDPROG'
			-- these use a row field that is a code, not age
			when 'Child Protection' then 'CHILDPROT'
			when 'Disability' then 'DIS'
			else @group
		end

-- no branching yet on datacolumns

	if (@Group = 'Rubbish') begin

			EXEC dbo.PIVSurvey_Rubbish
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end

	if (@Group = 'RandM') begin

			EXEC dbo.PIVSurvey_RandM
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end

	if (@Group = 'Curriculum') begin

			EXEC dbo.PIVSurvey_Curriculum
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end
	if (@Group = 'ParentCommittee') begin

			if (@DataColumns = 'SOMALIA')
				EXEC dbo.PIVSurvey_ParentCommitteeSO
					@DimensionColumns ,
					@DataColumns,
					@Group,
					@SchNo ,
					@SurveyYear
			else
				EXEC dbo.PIVSurvey_ParentCommittee
					@DimensionColumns ,
					@DataColumns,
					@Group,
					@SchNo ,
					@SurveyYear

		return
	end


--- classes and subjects
	if (@Group = 'SubjectOffered') begin

			EXEC dbo.PIVClasses_SubjectOffered
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end

if (@Group = 'TeacherHours') begin

			if (@DataColumns = 'SOMALIA')
				EXEC dbo.PIVClasses_TeacherHoursSO
					@DimensionColumns ,
					@DataColumns,
					@Group,
					@SchNo ,
					@SurveyYear
			else
				EXEC dbo.PIVClasses_TeacherHours
					@DimensionColumns ,
					@DataColumns,
					@Group,
					@SchNo ,
					@SurveyYear

		return
	end

--- pupil table items
	if (@Group in ('FEEDPROG')) begin
			EXEC dbo.PIVPupilTable_Gender
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end

-- split by level and row code
	if (@Group in ('CHILDPROT', 'DIS')) begin
			EXEC dbo.PIVPupilTable_RowLevelGender
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end


---- repeaters
--- always split by level and gender, but not age
	if (@Group in ('BRD', 'DROP','REPN')) begin
			Select @group = case @group
								when 'REPN' then 'REP'
								else @group
							end
			EXEC dbo.PIVPupilTable_LevelGender
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end


-- repeaters may or may not be splut by age
	if (@Group in (	'REPA') ) begin


			Select @group = case @group
								when 'REPA' then 'REP'
								else @group
							end
			EXEC dbo.PIVPupilTable_AgeLevelGender
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end
-- specific fromatting
---- distance transport

	if (@Group in ('TRANSPORT')) begin
			EXEC dbo.PIVPupilTable_DistanceTransport
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end

---- REsources
	if (@Group = 'Power Supply') begin
		if (@DataColumns is null)
			EXEC dbo.PIVResources_Resources
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVResources_Resources_SO
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear
		return
	end

	if (@Group in ('Text Books','Readers')) begin
		if (@DataColumns is null)
			EXEC dbo.PIVResources_TextBooks
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVResources_TextBooks
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear
		return
	end
	if (@Group in ('Library Resources','Special Needs')) begin
			--somalia uses the default for these formats
			EXEC dbo.PIVResources_Default
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		return
	end

	if (@Group = 'Fees') begin
		if (@DataColumns is null)
			EXEC dbo.PIVSurvey_Fees
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVSurvey_FeesSO
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear
		return
	end
	-- all other categries
		if (@DataColumns is null)
			EXEC dbo.PIVResources_Default
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear

		if (@DataColumns = 'SOMALIA')
			EXEC dbo.PIVResources_default_SO
				@DimensionColumns ,
				@DataColumns,
				@Group,
				@SchNo ,
				@SurveyYear
		return


END
GO

