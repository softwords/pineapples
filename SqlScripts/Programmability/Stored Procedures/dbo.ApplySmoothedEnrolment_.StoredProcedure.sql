SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ApplySmoothedEnrolment_]
(
@schoolNo nvarchar(50) = 'KPS002'
,@year int = 2020
, @commit int = 0
)
AS
BEGIN
begin transaction


Select * from Schools WHERE schNo = @schoolNo

declare @ssID int = null

select @ssID = ssID from SchoolSurvey
WHERE schNo = @schoolNo
AND svyYear = @year

Select @ssID FoundSSID

If (@ssID is null) begin
	INSERT INTO SchoolSurvey
	(svyYEar, schNo, ssSchType)
	Select @year, @schoolNo, S.schType
	FROM Schools S
		WHERE schNo = @schoolNo

			select @ssID = ssID from SchoolSurvey
			WHERE schNo = @schoolNo
			AND svyYear = @year
		Select @ssID IdentitySSID
end

Select * from SchoolSurvey WHERE ssID = @ssID

Select 'Existing Enrollments'
Select * from Enrollments
WHERE ssID = @ssID

select 'Delete Enrolments'


DELETE from Enrollments
WHERE ssID = @ssID


INSERT INTO EnrollmentS
(ssID, enAge, enLevel, enOrigin, enM, enF)
Select SS.ssID, Age, ClassLevel, 'SMOOTHING'
, nullif(sum(case when GenderCode = 'M' then Enrol end),0) M
, nullif(sum(case when GenderCode = 'F' then Enrol end),0) F

FROM
SchoolSurvey SS
	INNER JOIN SmoothedData D
		ON SS.schNo = D.schNo
		AND SS.svyYEar = D.surveyYear
	WHERE SS.ssID = @ssID
GROUP BY SS.ssID, Age,  ClassLevel

Select * from Enrollments
WHERE ssID = @ssID

SElect sum(enF), sum(enM) M, sum(enSum) Tot
FROM ENRollments
WHERE ssID = @ssID

if @commit = 1 begin
	commit transaction
end
else begin
	rollback
	print 'Rolled Back'
end

END
GO

