SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 8 2020
-- Description:	Report various table data created from a census workbook.
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusStaffOutput]
	-- Add the parameters for the stored procedure here
	@fileReference uniqueidentifier			-- guid from the filedb of the census book
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @fr nvarchar(100)
	SELECT @fr = convert(nvarchar(50), @fileReference)

-- recordset 1: identifying data -forms the base of the return package
Select TOP 1 @filereference fileID
, svyYear surveyYear
, pCreateUser
, pCreateDateTime
FROM pTeacherREad.TeacherSurveyV T
WHERE tcheSource = @fr

-- next the lookup list
-- 2 the row code set -- in this case the school nos
-- schoolList
SELECT DISTINCT isnull(T.schNo,'<>') codeCode
, isnull(T.schNo,'<>') + ': ' + schName codeDescription
FROM pTeacherREad.TeacherSurveyV T
WHERE tcheSource = @fr
ORDER BY isnull(T.schNo,'<>')

---- 3 is the column definitions
-- based on TAM

Select
case num
	when 1 then 'T'
	when 2 then 'A'
	when 3 then 'X'
	when 4 then 'M'
end codeCode
,  case num
	when 1 then 'Teaching'
	when 2 then 'Admin'
	when 3 then 'Other'
	when 4 then 'Mixed Duties'
end codeDescription
from metaNumbers
WHERE num between 1 and 4

-- now pick up the teacher surveys

Select schNo row
, tchTAM col
, sum (case tSex when 'M' then 1 end) M
, sum (case tSex when 'F' then 1 end) F
FROM pTeacherRead.TeacherSurveyV
WHERE tcheSource = @fr
GROUP BY schNo
, tchTAM

-- by qualification?
-- finally, relay the breakdown by highest_qualification and school
--Select @stage = 'Writing output 1 - staff by school by qualification'
--Select schNo
--, codeDescription
--, count(*)
--from pTeacherRead.TeacherSurveyV TS
--	LEFT JOIN lkpTeacherQual Q
--	ON  TS.tchQual = Q.codeCode
---- convert @filereference to string, since tcheSource may contain values that have not originated from guids
--WHERE tcheSource = convert(nvarchar(36),@filereference)
--GROUP BY SchNo, codeDescription
--ORDER BY schNo, codeDescription

END
GO

