SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 03 08 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[FlowAudit]
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
create table #efa

(
	schNo nvarchar(50),
	svyYear int,
	surveyDimensionssID int,

	LevelCode nvarchar(20),
	YearOfEd int,
	EnrolM int,
	EnrolF int,
	EnrolNYM int,
	EnrolNYF int,
	EnrolNYNextLevelM int,
	EnrolNYNextLevelF int,
	RepM int,
	RepF int,
	RepNYM int,
	RepNYF int,
	RepNYNextLevelM int,
	RepNYNextLevelF int,
	Estimate smallint,
	[Year Of Data] int,
	[Age of Data] int,
	[Estimate NY] smallint,
	[Year Of Data NY] int,

	[Age of Data NY] int
)


insert into #efa
exec dbo.sp_efacore 1;

-- we we select from this data the odd things
--

WITH Audit
	(SchNo, svyYear, levelCode
	, EnrolM , EnrolF
	, EnrolNYM , EnrolNYF
	, EnrolNYNextLevelM, EnrolNYNextLevelF
	, RepM, RepF
	, RepNYM, REpNYf
	, RepNYNextLevelM, RepNYNextLevelF

	, M1,F1,M2,F2,M3, F3, M4, F4, M5, F5)
 AS
(
Select schNo
	, svyYear
	, levelCode
	, EnrolM , EnrolF
	, EnrolNYM , EnrolNYF
	, EnrolNYNextLevelM, EnrolNYNextLevelF
	, RepM, RepF
	, RepNYM, REpNYf
	, RepNYNextLevelM, RepNYNextLevelF

	, M1
	,  F1
	,  M2
	,  F2
	,	M3
	, F3
	, M4
	, F4
	, M5 , F5

from
(
Select
	schNo
	, svyYear
	, levelCode
	, EnrolM , EnrolF
	, EnrolNYM , EnrolNYF
	, EnrolNYNextLevelM, EnrolNYNextLevelF
	, RepM, RepF
	, RepNYM, REpNYf
	, RepNYNextLevelM, RepNYNextLevelF

	, case when EnrolNYNextLevelM > EnrolM then 1 else 0 end M1
	, case when EnrolNYNextLevelF > EnrolF then 1 else 0 end F1
	, case when RepNYM > EnrolM then 1 else 0 end M2
	, case when RepNYF > EnrolF then 1 else 0 end F2
	, case when RepM >= EnrolM then 1 else 0 end M3
	, case when RepF >= EnrolF then 1 else 0 end F3
	, case when RepNYNextLevelM >= EnrolNYNextLevelM then 1 else 0 end M4
	, case when RepNYNextLevelF >= EnrolNYNextLevelF then 1 else 0 end F4
	, case when EnrolNYNextLevelM - isnull(RepNYNextLevelM,0) + isnull(RepNYM,0) > EnrolM then 1 else 0 end M5
	, case when EnrolNYNextLevelF - isnull(RepNYNextLevelF,0) + isnull(RepNYF,0) > EnrolF then 1 else 0 end F5

from #efa
WHERE
	( svyYEar = @surveyYear or @SurveyYear = 0)

) E
WHERE
	(E.M1) + (E.f1) + (E.M2) + (E.F2) + (E.M3) + (E.F3) + E.M4 + E.F4 + E.M5+ E.F5 > 0
)
Select
	Audit.svyYear [Survey Year]

	, Audit.schNo [School ID]
	,  s.schName [School Name]
	, S.SchNo + ': ' + S.SchName as [SchoolID_Name]

	, Audit.levelCode [Level Code]
	, G.*
	, case when GenderCode = 'M' then EnrolM else EnrolF end Enrol
	, case when GenderCode = 'M' then EnrolNYM else EnrolNYF end EnrolNY
	, case when GenderCode = 'M' then EnrolNYNextLevelM else EnrolNYNextLevelF end EnrolNYNextLevel

	, case when GenderCode = 'M' then RepM else RepF end Rep
	, case when GenderCode = 'M' then RepNYM else RepNYF end RepNY
	, case when GenderCode = 'M' then RepNYNextLevelM else RepNYNextLevelF end RepNYNextLevel

	, case when GenderCode = 'M' then M1 else F1 end EnrolGrowthInconsistent
	, case when GenderCode = 'M' then M2 else F2 end RepeaterGrowthInconsistent
	, case when GenderCode = 'M' then M3 else F3 end RepeaterError
	, case when GenderCode = 'M' then M4 else F4 end RepeaterErrorNY
	, case when GenderCode = 'M' then M5 else F5 end DropIns
	, 1 as RowIssues
	, case when GenderCode = 'M' then M1 + M2 + M3 + M4 + M5
			else F1+F2+F3+F4 +F5
			end TotalIssues


from Audit

INNER JOIN Schools S ON Audit.schNo = S.schNo


CROSS JOIN DimensionGender G


END
GO

