SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 8 2017
-- Description:	Teacher Link filter
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherLinkFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@LinkID				int = null,		-- the unique idenitifer of the
	@TeacherID			int = null,
	@TeacherName		nvarchar(100) = null,		-- matched with like if
	@PayrollNo			nvarchar(10) = null,
	@DocumentID			uniqueidentifier = null,
	@Keyword			nvarchar(50) = null,
	@Function			nvarchar(50) = null,
	@DateStart			datetime = null,
	@DateEnd			datetime = null,
	@DocumentSource		nvarchar(100) = null,
	@DocType			nvarchar(10) = null,
	@IsImage			int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
	selectedID int
	, recNo int IDENTITY PRIMARY KEY
	)
	INSERT INTO @keys
	(selectedID)
	Select lnkID
	From pTeacherRead.TeacherLinks TL
		LEFT JOIN TeacherIdentity TI
			ON TL.tID = TI.tID
	WHERE
	(lnkID = @linkID or @linkID is null)
	AND (TL.tID = @TeacherID or @TeacherID is null)
	AND (Ti.tSurname = @TeacherName or @TeacherName is null)
	AND (TI.tPayroll = @PayrollNo or @PayrollNo is null)
	AND (docID = @DocumentID or @DocumentID is null)
	AND (lnkFunction = @Function or @Function is null)
	AND (docDate >= @DateStart or @DateStart is null)
	AND (common.dropTime(docDate) <= @DateEnd or @DateEnd is null)
	AND (docSource = @documentSource OR @documentSource is null)
	AND (docType = @docType or @docType is null)
	AND (isnull(@IsImage, 0) = 0 or docType in ('jpg','jpeg','bmp','png','gif') )

	AND (docTags like '%' + @Keyword + '%' or
			docDescription like '%' + @Keyword + '%'
			or @Keyword is null)

	ORDER BY
		case @sortColumn			-- strings

			WHEN 'Teacher' then tSurname
			WHEN 'TeacherName' then tSurname
			WHEN 'tSurname' then tSurname

			WHEN 'Payroll' then tPayroll
			WHEN 'PayrollNo' then tPayroll
			WHEN 'tPayroll' then tPayroll

			WHEN 'Document' then docID
			WHEN 'docID' then docID

			WHEN 'Function' then lnkFunction
			WHEN 'lnkFunction' then lnkFunction


			WHEN 'Source' then docSource
			WHEN 'docSource' then docSource

			WHEN 'docType' then docType
			ELSE null
			end,
		case @sortColumn -- numbers

			When 'TeacherID' then TL.tID
			WHEN 'tID' then TL.tID
			ELSE null
		end,
		case @sortColumn -- numbers
			WHEN 'docDate' then docDate
			ELSE null
		end,
		case @sortColumn -- numbers
			WHEN 'Document' then docID
			WHEN 'docID' then docID
			ELSE null
		end,

		lnkID


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedID
		, RecNo
		FROM
		(
			Select selectedID
			, @NumMatches - RecNo + 1 RecNo
			FROM @Keys
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)
				)
		ORDER BY RecNo
	end
	else begin

		SELECT selectedID
		, RecNo
		FROM @Keys
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
END
GO

