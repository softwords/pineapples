SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 9 2019
-- Description:	Register the photos in a survey
--		Creates a document record for the photo, and a link to its school
--		Note that the documents are not actually downloaded and stored against the created docIDs.
--		This happens 'on demand'; ie when the document is requested for display.
-- =============================================
CREATE PROCEDURE [pInspectionWrite].[RegisterInspectionPhotos]
	-- Add the parameters for the stored procedure here
	@inspID int			-- the inspection
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin transaction

declare @ph TABLE
(
	inspID int
	, schNo nvarchar(50)
	, phDate datetime
	, docID uniqueidentifier		-- will hold a new document ID if needed
	, fileId nvarchar(50)
	, description nvarchar(1000)
	, questionID nvarchar(10)
	, InspectionType nvarchar(20)
	, photoDocID uniqueidentifier	-- the located document ID
)

-- collect the information about each photo in the inspection
-- check if it is already registered in the documents table
INSERT INTO @ph
Select inspID
	, schNo
	, StartDate
	, null
	, v.value('.', 'nvarchar(50)') photoId
	, v.value('../../../name[1]', 'nvarchar(1000)') caption
	, v.value('../../../id[1]', 'nvarchar(1000)') qid
	, SI.inspTypeCode
	, docID
		from pInspectionRead.SchoolInspections SI
		CROSS APPLY inspectionContent.nodes('//photo/remotePath') V(v)
		LEFT JOIN Documents_ D
		ON v.value('.', 'nvarchar(50)') = D.docCloudID
		WHERE inspID = @inspID

-- generate a new guid a docID for those documents not yet registered
UPDATE @ph
SET docID = newid()
WHERe photoDocID is null

-- insert the document records if not present
INSERT INTO Documents_
(
	docID
	, docDescription
	, docDate
	, docTitle
	, docCloudID
	, docCloudRemoved
	, docSource
	, docTags

)
Select docID
, description
, phDate
, inspectionType + ' ' + convert(nvarchar(7), inspID) + ': ' + questionID +
	case RN when 1 then '' else ' :' + convert(nvarchar(3), RN) end
	+ '.JPG'
, fileID
, 0
, inspectionType + ' ' + convert(nvarchar(7), inspID)
, questionID
FROM
( Select *
, row_number() OVER (PARTITION BY questionID ORDER BY fileID) RN
FROM @ph
WHERe photoDocID is null
) SUB

print @@rowcount
print 'Documents added:'


-- update if existing
UPDATE Documents_
SET docDescription = description
, docDate = phDate
, docTitle = inspectionType + ' ' + convert(nvarchar(7), inspID) + ': ' + questionID +
	case RN when 1 then '' else ' :' + convert(nvarchar(3), RN) end
	+ '.JPG'
, docCloudID = fileID
, docCloudRemoved = 0
, docSource = inspectionType + ' ' + convert(nvarchar(7), inspID)
, docTags = questionID
FROM Documents_
INNER JOIN ( Select *
, row_number() OVER (PARTITION BY questionID ORDER BY fileID) RN
FROM @ph
) SUB
	ON Documents_.docId = SUB.photoDocID

print @@rowcount
print 'documents updated:'


-- delete any existing relevant documentlinks_
DELETE from DocumentLinks_
from DocumentLinks_
INNER JOIN @ph
	ON DocumentLinks_.docId = photoDocID
	AND DocumentLinks_.schNo = [@ph].schNo

-- insert/reinsert document links
-- link to the school and the inspection on the same record
-- if the inspection is removed, the link from the school will go too
INSERT INTO DocumentLinks_
(
	schNo
	, inspID
	, docID
	, lnkFunction
)
Select schNo
, inspID
, coalesce(photoDocId, docID)
, 'PHOTO'
from @ph

commit transaction


END
GO

