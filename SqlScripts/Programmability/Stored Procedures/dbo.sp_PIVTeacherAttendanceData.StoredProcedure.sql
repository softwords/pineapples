SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 11 2007
-- Description:	for teacher attendance pivot table
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVTeacherAttendanceData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
-- to do need to get a complete spultion to the issue of supplying
-- dimension data for schools that have no survey

Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT
TermStructure.tmYear AS [Year],
TermStructure.tmNo AS Term,
TermStructure.tmWeek AS Week,
TermStructure.tmDate AS WeekStartDate,
tmPay AS PayWeek,
TeacherAttendance.schNo,
TeacherAttendance.tID,
[tSurname] + ', ' + [tGiven] AS Teacher,
TeacherAttendance.tattAbsentDays AS Absent,
TeacherAttendance.tattLateDays AS Late,
TermStructure.tmDays AS TeacherDays,
tattAbsentDays + tattLateDays AS Noncompliant,
surveyDimensionssID
FROM TermStructure
	INNER JOIN TeacherAttendance
		ON TermStructure.tmID = TeacherAttendance.tmID
	INNER JOIN TeacherIdentity
		ON TeacherIdentity.tID = TeacherAttendance.tID
	INNER JOIN #ebse E
		ON E.schNo = TeacherAttendance.schNo
			AND E.LifeYear = TermStructure.tmYear


END
GO
GRANT EXECUTE ON [dbo].[sp_PIVTeacherAttendanceData] TO [pTeacherReadX] AS [dbo]
GO

