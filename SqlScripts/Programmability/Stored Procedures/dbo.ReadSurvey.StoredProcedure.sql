SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-02-07
-- Description:	Read Enrolments as XML
-- =============================================
CREATE PROCEDURE [dbo].[ReadSurvey]
	-- Add the parameters for the stored procedure here

	@SchNo nvarchar(50) ,
	@SurveyYear int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
create table #xmlTmp
(
	theXML xml
)
declare @Survey xml
declare @SchoolXMl xml
declare @RoomsXML xml
declare @EnrolXML xml
declare @PupilTableXML xml
declare @TeachersXML xml
declare @ClassesXML xml
declare @ResourcesXML xml
declare @ToiletsXML xml
declare @FundingXML xml
declare @DisabilityVillageXML xml
declare @ResourceProvisionXML xml
declare @TableDefsXML xml
declare @LayoutXML xml


-- begn with the schoool data
select @SchoolXML =
(
Select
Tag
, Parent
		,	schNo as [School!1!schoolNo]
		,	schName as [School!1!schoolName]
		,	schClosed as [School!1!Closed]
		,	schCloseReason as [School!1!ClosedReason]
		,	schEst as [School!1!Established]
		,	schEstBy as [School!1!EstablishedBy]
		,	ssAuth as [School!1!Authority]
		,	ssschType as [School!1!SchoolType]
		,	schVillage as [Location!2!Village]
		,	schAddr1 as [Location!2!Address1]
		,	schAddr2 as [Location!2!Address2]
		,	schAddr3 as [Location!2!Address3]
		,	schAddr4 as [Location!2!Address4]
		,	schPh1 as [Location!2!Ph1]
		,	schPh2 as [Location!2!Ph2]
		,	schFax as [Location!2!Fax]
		,	schEmail as [Location!2!Email]
		,	schWWW as [Location!2!WebSite]

			, ssCatch as [Catchment!10!!cdata]

			, ssLandowner as [Land!11!owner]
			, ssLandUse as [Land!11!use]
			, ssLandUseExpiry as [Land!11!expiry]

			, ssSvcBank as [Services!9!nearestBank]
			, ssSvcAir  as [Services!9!nearestAir]
			, ssSvcShip  as [Services!9!nearestShip]
			, ssSvcClinic  as [Services!9!nearestClinic]
			, ssSvcPost as  [Services!9!nearestPost]
			, ssSvcHospital  as [Services!9!nearestHospital]
			, ssSvcSupplies  as [Services!9!nearestSupplies]


			, ssSchoolCouncil as [Management!8!hasSchoolCouncil]
			, ssSCMeet as [Management!8!numSCMeetings]
			, ssParentCommittee as [Management!8!hasParentCommittee]
			, ssPCMeet as [Management!8!numPCMeetings]
			, ssPCSupport as [Management!8!ratingPCSupport]

			, ssNote as [Comment!12!!cdata]

			, ssSiteSecure as [Site!3!secureFence]
			, ssRubbishFreq as [Site!3!rubbishCollectionFreq]
			, ssRubbishMethod as [Site!3!rubbishCollectionMethod]


			, 'TotalArea' as [Dimensions!4!region]
			, ssSiteL	as [Dimensions!4!length]


			, ssSiteW	as [Dimensions!4!width]
			, ssSizeSite as [Dimensions!4!area]

			, 'Playground' as [Dimensions!5!region]
			, ssPlaygroundL as [Dimensions!5!length]
			, ssPlaygroundW as [Dimensions!5!width]
			, ssSizePlayground as [Dimensions!5!area]

			, 'Farm' as  [Dimensions!6!region]
			, ssfarmL as [Dimensions!6!length]
			, ssfarmW as [Dimensions!6!width]
			, ssSizefarm as [Dimensions!6!area]

			, 'FarmUsed' as  [Dimensions!7!region]

			, ssfarmUsedL as [Dimensions!7!length]
			, ssfarmUsedW as [Dimensions!7!width]
			, ssSizefarmUsed as [Dimensions!7!area]


			, ssPlan0 as [EnrolEstimate!13!est0]
			, ssPlan1 as [EnrolEstimate!13!est1]
			, ssPlan2 as [EnrolEstimate!13!est2]
			, ssPlan3 as [EnrolEstimate!13!est3]
			, ssPlan4 as [EnrolEstimate!13!est4]

from
(Select
	num as Tag,
	case num when 1 then null
		when 2 then 1
		when 3 then 1
		when 4 then 3
		when 5 then 3
		when 6 then 3
		when 7 then 3
		when 8 then 1
		when 9 then 1
		when 10 then 1
		when 11 then 1
		when 12 then null
		when 13 then null
 end Parent,
	SS.*
		,  schName
		,	schClosed
		,	schCloseReason
		,	schEst
		,	schEstBy
		,	schVillage
		,	schAddr1
		,	schAddr2
		,	schAddr3
		,	schAddr4
		,	schPh1
		,	schPh2
		,	schFax
		,	schEmail
		,	schWWW


from Schools S
inner join SchoolSurvey SS
	on s.SchNo = SS.SchNo,
metaNumbers M
WHERE SS.SchNo = @SchNo and SS.SvyYear = @SurveyYear
and
M.num between 1 and 13
) School
ORDER BY Tag
for xml explicit
)

insert into #xmlTmp
exec dbo.xmlRooms @schNo, @SurveyYear

select @RoomsXML = theXML from #xmlTmp


-- the enrolments
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlEnrolments @schNo, @SurveyYear
select @EnrolXML = theXML from #xmlTmp

-- the pupil tables
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlPupilTables @schNo, @SurveyYear
select @PupilTableXML = theXML from #xmlTmp

-- teachers
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlTeachers @schNo, @SurveyYear
select @TeachersXML = theXML from #xmlTmp

-- classes
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlClasses @schNo, @SurveyYear
select @ClassesXML = theXML from #xmlTmp


-- resources
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlResources @schNo, @SurveyYear
select @ResourcesXML = theXML from #xmlTmp

-- resource provision
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlResourceProvision @schNo, @SurveyYear
select @ResourceProvisionXML = theXML from #xmlTmp


-- toilets
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlToilets @schNo, @SurveyYear
select @ToiletsXML = theXML from #xmlTmp

-- funding
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlFunding @schNo, @SurveyYear
select @FundingXML = theXML from #xmlTmp

-- disabilityVillage
DELETE from #xmltmp
insert into #xmlTmp
exec dbo.xmlDisabilityVillage @schNo, @SurveyYear
select @DisabilityVillageXML = theXML from #xmlTmp


-- school table item definitions
DELETE from #xmltmp
--insert into #xmlTmp
exec dbo.xmlTableDefs @schNo, @SurveyYear
select @TableDefsXML = theXML from #xmlTmp


-- 28 09 2011 add layout xml to this
-- to support dynamic survey

Select @LayoutXML = formLayout
FROM metaFormLayouts FL
	INNER JOIN SurveyYearSchoolTypes YT
		ON FL.formName = YT.ytLayout
	INNER JOIN schoolYearHistory SYH
		ON YT.stCode = SYH.systCode
WHERE SYH.schNo = @SchNo
	AND YT.svyYear = @SurveyYear

Select @Survey =
(
Select
	@SchoolXML
	, @RoomsXML
	, @EnrolXML
	, @PupilTableXML
	, @DisabilityVillageXML
	, @TeachersXML
	, @ClassesXML
	, @ResourcesXML
	, @ResourceProvisionXML
	, @ToiletsXML
	, @FundingXML
	, @TableDefsXML
	, @LayoutXML
for XML PATH('Survey')
)

-- @Survey is an XML object

declare @Received datetime

-- finally may as well put the ID on here too
declare @SurveyID int
Select @SurveyID = ssID
FROM
	SchoolSurvey
WHERE schNo = @schNo
	AND svyYear = @SurveyYear


set @Survey.modify('
insert
	(
	 attribute schoolNo {sql:variable("@schNo") }
	,  attribute surveyYear {sql:variable("@SurveyYear") }
	,  attribute surveyID {sql:variable("@SurveyID") }
	)
into   (/Survey[1]) ')

Select @Survey

--- some logging
declare @ssID int

select @ssID = ssID
from
SchoolSurvey SS
WHERE SS.SchNo = @SchNo and SS.SvyYear = @SurveyYear


exec LogActivity 'ReadSurvey' , @ssID

END
GO
GRANT EXECUTE ON [dbo].[ReadSurvey] TO [pSchoolRead] AS [dbo]
GO

