SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3/1/2010
-- Description:	Set up all room function codes
-- =============================================
CREATE PROCEDURE [dbo].[setup_lkpRoomFunction]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #tmpRfcn
(
	rfcnCode nvarchar(10)
	, rfcnDesc nvarchar(50)
)
INSERT INTO #tmpRfcn
Select 'Class','Classroom'
INSERT INTO #tmpRfcn
Select 'OHT','Head Teacher Office'
INSERT INTO #tmpRfcn
Select 'Staff','Staff Room'
INSERT INTO #tmpRfcn
Select 'Admin','Administration Office'
INSERT INTO #tmpRfcn
Select 'Storage','Storeroom or shed'
INSERT INTO #tmpRfcn
Select 'Dorm','Dormitory room'
INSERT INTO #tmpRfcn
Select 'Dining','Dining Room'
INSERT INTO #tmpRfcn
Select 'Kitchen','Kitchen'
INSERT INTO #tmpRfcn
Select 'Library','Library'
INSERT INTO #tmpRfcn
Select 'Hall','Hall or Maneaba'
INSERT INTO #tmpRfcn
Select 'Spec','Special Tuition Room'
INSERT INTO #tmpRfcn
Select 'Other','Other';

INSERT INTO lkpRoomFunction
(
rfcnCode
, rfcnDesc
)
SELECT rfcnCode
	, rfcnDesc
FROM #tmpRfcn
WHERE #tmpRfcn.rfcnCode not in (Select rfcnCode from lkpRoomFunction)

END
GO

