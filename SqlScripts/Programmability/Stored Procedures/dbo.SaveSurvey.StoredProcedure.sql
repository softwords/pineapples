SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 12 2007
-- Description:	process the xmlfile produced by pineapples @ school
-- =============================================

CREATE PROCEDURE [dbo].[SaveSurvey]
	-- Add the parameters for the stored procedure here
	@Survey xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- we break up the survey into its various grids using the query method
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @Survey

	declare @SurveyID int
	declare @SurveyYear int
	declare @schNo nvarchar(50)
	declare @grid xml

   DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;


-- get the school number and year from the survey root node
	declare @SurveyInfo table(	SurveyYear int,
								schNo nvarchar(50)
							  )

	INSERT @SurveyInfo
		Select
		*
		from OPENXML (@idoc, '/Survey',2)
		WITH (
				SurveyYear int '@surveyYear',
				SchNo nvarchar(50) '@schoolNo'
			)


	select @SurveyYear = SurveyYear,
		@SchNo = SchNo
	from @SurveyInfo
	select @SurveyYear, @SchNo

-- check whether this is a known valid school, and valid survey year
--
	declare @total int
	Select @total=  count(S.schNo )
	from Schools S
	WHERE S.schNo in (Select SchNo from @SurveyInfo)


	if (@Total=0)
		begin
			set @err = 999
			set @ErrorMessage = 'This School No is not known'
			set @ErrorSeverity = 16
			set @ErrorState = 0

			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			return @err

		end


	Select @total = count(SY.svyYear)
	from Survey SY
	WHERE SY.svyYear in (Select SurveyYear from @SurveyInfo)

	if (@total=0)
		begin
				set @err = 999
				set @ErrorMessage = 'Not a valid survey year'
				set @ErrorSeverity = 16
				set @ErrorState = 0

				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
				return @err
			return
		end

	select @SurveyID = ssID
	from SchoolSurvey
	WHERE SchNo = @schNo
		AND svyYEar = @SurveyYear

	if @SurveyID is null
		begin
			INSERT INTO SchoolSurvey(schNo, svyYear)
			VALUES (@SchNo, @SurveyYear)
			Select @SurveyID = @@IDENTITY
		end
	select @SurveyID


-- now we have the survey id, we can split up the
-- xml and process using existing routines

-- first update all the school survey data
	update SchoolSurvey
		SET
			ssSchType = S.SchoolType
			, ssAuth = Authority

--	management
			, ssSchoolCouncil = SchoolCouncil
			, ssSCMeet = SCMeet
			, ssSCApproved = SCApproved
			, ssParentCommittee = ParentCommittee
			, ssPCMeet = PCMeet
			, ssPCSupport = PCSupport

-- land use
			, ssLandOwner = LandOwner
			, ssLandUse = LandUse
			, ssLandUseExpiry = LandUseExpiry

-- catchment
			, ssCatch = Catchment

-- services
			, ssSvcBank = svcBank
			, ssSvcAir  = svcAir
			, ssSvcShip  = SvcShip
			, ssSvcClinic  = SvcClinic
			, ssSvcPost   = svcPost
			, ssSvcHospital  = svcHospital
			, ssSvcSupplies  = svcSupplies
--enrolEstimates
			, ssPlan0 = Plan0
			, ssPlan1 = Plan1
			, ssPlan2 = Plan2
			, ssPlan3 = Plan3
			, ssPlan4 = Plan4

-- note
			, ssNote = Comment

	from OPENXML (@idoc, '/Survey/School',2)
		WITH (
				SchoolType nvarchar(10) '@SchoolType'
				, Authority nvarchar(10) '@Authority'
	-- management node
				, SchoolCouncil bit 'Management/@hasSchoolCouncil'
				, SCMeet smallint 'Management/@numSCMeetings'
				, SCApproved bit 'Management/@approvedSchoolCouncil'
				, ParentCommittee bit 'Management/@hasParentCommittee'
				, PCMeet smallint 'Management/@numPCMeetings'
				, PCSupport smallint 'Management/@ratingPCSupport'
	-- land use
				, LandOwner nvarchar(10) 'Land/@owner'
				, LandUse nvarchar(50) 'Land/@use'
				, LandUseExpiry datetime 'Land/@expiry'
 -- Catchment
				, Catchment ntext 'Catchment'
-- comment
				, Comment ntext 'Comment'
	-- services
				, SvcBank nvarchar(30) 'Services/@nearestBank'
				, SvcAir  nvarchar(30) 'Services/@nearestAir'
				, svcShip  nvarchar(30) 'Services/@nearestShip'
				, SvcClinic  nvarchar(30) 'Services/@nearestClinic'
				, SvcPost nvarchar(30)  'Services/@nearestPost'
				, SvcHospital  nvarchar(30) 'Services/@nearestHospital'
				, SvcSupplies  nvarchar(30) 'Services/@nearestSupplies'

--enrolEstimates
				, Plan0 int 'EnrolEstimates/@est0'
				, Plan1 int 'EnrolEstimates/@est1'
				, Plan2 int 'EnrolEstimates/@est2'
				, Plan3 int 'EnrolEstimates/@est3'
				, Plan4 int 'EnrolEstimates/@est4'

				) S

	WHERE		-- 22 04 2008 add where clause to fix an horrendous bug
		SchoolSurvey.ssID = @SurveyID

-- update the school table now

	UPDATE Schools
		SET
			schVillage = Village,
			schAddr1 = Address1,
			schAddr2 = Address2,
			schAddr3 = Address3,
			schAddr4 = Address4,
-- phones
			schPh1 = Ph1,
			schPh2 = Ph2,
			schFax = Fax,
			schEmail = Email,
			schWWW = WebSite,
-- closure and establishment
			schClosed = Closed,
			schCloseReason = ClosedReason,
			schEst = Established,
			schEstBy = EstablishedBy,
			schAuth = Authority,
			schType = SchoolType


	from OPENXML (@idoc, '/Survey/School',2)
		WITH (
				SchoolType nvarchar(10) '@SchoolType',

				Closed int			'@Closed',
				ClosedReason nvarchar(50) '@ClosedReason',
				Established int		'@Established',
				EstablishedBy nvarchar(50) '@EstablishedBy',
				Authority nvarchar(10) '@Authority'

-- location node
				, Village nvarchar(50) 'Location/@Village'
				, Address1 nvarchar(50) 'Location/@Address1'
				, Address2 nvarchar(50) 'Location/@Address2'
				, Address3 nvarchar(50) 'Location/@Address3'
				, Address4 nvarchar(50) 'Location/@Address4'
				, Ph1 nvarchar(50) 'Location/@Ph1'
				, Ph2 nvarchar(50) 'Location/@Ph2'
				, Fax nvarchar(50) 'Location/@Fax'
				, Email nvarchar(50) 'Location/@EMail'
				, Website nvarchar(150) 'Location/@Website'


				) S
	WHERE
		Schools.schNo = @SchNo


-- there are multiple dimension nodes, process these separately
declare @el xml
declare @strXPath nvarchar(500)
set @strXPAth = '/Survey/School/Site'
if (@Survey.exist('/Survey/School/Site')=1)
	begin
		select @el = @Survey.query('/Survey/School/Site')
		select  @el.value('(/Site/Dimensions[@region="TotalArea"]/@length)[1]','int')
		UPDATE SchoolSurvey
			SET	ssSiteL = @el.value('(/Site/Dimensions[@region="TotalArea"]/@length)[1]','int')
				, ssSiteW =  @el.value('(/Site/Dimensions[@region="TotalArea"]/@width)[1]','int')
				, ssSizeSite = @el.value('(/Site/Dimensions[@region="TotalArea"]/@area)[1]','int')

				, ssFarmL = @el.value('(/Site/Dimensions[@region="Farm"]/@length)[1]','int')
				, ssFarmW =  @el.value('(/Site/Dimensions[@region="Farm"]/@width)[1]','int')
				, ssSizeFarm = @el.value('(/Site/Dimensions[@region="Farm"]/@area)[1]','int')

				, ssFarmUsedL = @el.value('(/Site/Dimensions[@region="FarmUsed"]/@length)[1]','int')
				, ssFarmUsedW =  @el.value('(/Site/Dimensions[@region="FarmUsed"]/@width)[1]','int')
				, ssSizeFarmUsed = @el.value('(/Site/Dimensions[@region="FarmUsed"]/@area)[1]','int')

				, ssPlaygroundL = @el.value('(/Site/Dimensions[@region="Playground"]/@length)[1]','int')
				, ssPlaygroundW =  @el.value('(/Site/Dimensions[@region="Playground"]/@width)[1]','int')
				, ssSizePlayground = @el.value('(/Site/Dimensions[@region="Playground"]/@area)[1]','int')
		WHERE
			SchoolSurvey.ssID = @SurveyID
	end

-- enrolments
	if (@Survey.exist('/Survey/Grid[@Tag="Enrolment"]')=1)
		begin

			Select @grid =@Survey.query('/Survey/Grid[@Tag="Enrolment"]')
			select 'Enrol', @grid


			begin try
				exec pEnrolmentWrite.updateEnrolment @SurveyID, @grid
			end try
			begin catch
				Select @@ERROR
			end catch
		end
-- streams
	if (@Survey.exist('/Survey/Streams')=1)
		begin

			Select @grid =@Survey.query('/Survey/Streams')
			select 'Streams',@grid

			begin try
				exec pSchoolWrite.updateStreams @SurveyID, @grid
			end try
			begin catch
				Select @@ERROR
			end catch
		end
-- now process all the non-enrolment grids
-- we use the nodes collection to find them, then a cursor to step through them

	declare  @grids  table
		(v xml)
	insert into @grids SELECT T.grid.query('.') V from @Survey.nodes('/Survey/Grid[@Tag!="Enrolment" and @Tag!="Streams" and @Tag!="Subjects"]') as T(grid)
	declare gridCursor CURSOR FOR
	select v from @grids
	open gridCursor
	declare @xmlGrid xml
	FETCH NEXT FROM gridCursor INTO @xmlGrid


		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @xmlGrid
			exec updategrid @SurveyID, null, null, @xmlgrid


			FETCH NEXT FROM gridCursor INTO @xmlGRid

		END

	close gridCursor


-- Teachers

	if (@Survey.exist('/Survey/Teachers')=1)
		begin
			declare @BatchID nvarchar(50)
			Select @grid =@Survey.query('Survey/Teachers')
			select 'Teachers',@grid
			exec pTeacherWrite.updateTeachers @SurveyID, @grid, @BatchID OUTPUT

			select @BatchID BatchID
		end

-- classrooms

-- try to avoid enumerating the qualityCodes

	declare  @surveyNodes  table
		(v xml)
	declare nodeCursor CURSOR FOR
	select v from @surveyNodes
	declare @xmlNode xml

	insert into @surveyNodes SELECT T.roomSet.query('.') V from @Survey.nodes('/Survey/Rooms') as T(roomSet)
	open nodeCursor
	FETCH NEXT FROM nodeCursor INTO @xmlNode


		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT 'Roomset',@xmlNode
			exec pSchoolWrite.updateRooms @SurveyID, @xmlNode


			FETCH NEXT FROM nodeCursor INTO  @xmlNode

		END

	close nodeCursor


-- Primary classes
-- we have to do this after teachers and rooms, so that they are flagged with their tsmisID
	if (@Survey.exist('/Survey/Classes')=1)
		begin
			select @grid = @Survey.query('Survey/Classes')
			select @grid
			exec pSchoolWrite.updateClasses @surveyID, @grid
		end


-- Resources comes next
--

	delete from @surveyNodes

	insert into @surveyNodes SELECT T.resSet.query('.') V from @Survey.nodes('/Survey/Resources') as T(resSet)
	open nodeCursor

	FETCH NEXT FROM nodeCursor INTO @xmlNode


		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @xmlNode

			exec pSchoolWrite.updateResourceList @SurveyID, null, @xmlNode


			FETCH NEXT FROM nodeCursor INTO @xmlNode

		END

	close nodeCursor

-- ResourceProvision
	-- we can resue the exisitng objects
	Delete from @SurveyNodes

	insert into @SurveyNodes SELECT T.resourceSet.query('.') V from @Survey.nodes('/Survey/ResourceProvision') as T(resourceSet)
	open NodeCursor

	FETCH NEXT FROM nodeCursor INTO @xmlNode


		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @xmlNode

			exec pSchoolWrite.updateResourceProvision @SurveyID,  @xmlNode


			FETCH NEXT FROM nodeCursor INTO @xmlNode

		END

	close nodeCursor

END
GO

