SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 3 2021
-- Description:	Sheet A1 of UIS Survey
-- =============================================
CREATE PROCEDURE [warehouse].[uisA1]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- this sheet provides some basic identifying data relating to the survey year
-- and country

declare @countryCode nvarchar(2)

-- shouldn't be this hard to get the 2 char iso country code

select @countryCode = paramText
from sysParams
WHERE paramName = 'USER_NATION_ISO'

if (@countryCode is null)
	select @countryCode = codeCode
		from lkpCountries
		inner join sysParams
		on codeDescription = paramText
		and paramName = 'USER_NATION'

if (@countryCode is null)
	Select @countryCode =
	case paramText
		when 'FEDEMIS' then 'FM'
		When 'MIEMIS' then 'MH'
		when 'KEMIS' then 'KI'
	end
	from sysParams
	WHERE paramName = 'APP_NAME'

Select @countryCode CountryCode
, *
from Survey
WHERE svyYear = @year
END
GO

