SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 9 2018
-- Description:	check access control for Teacher
-- the business rule is: the user may view the teacher
-- if the teacher has EVER had an appointment, or a TeacherSurvey, at ANY school
-- to which the user has access
-- =============================================
CREATE PROCEDURE [pTeacherRead].[accessControl]
	-- Add the parameters for the stored procedure here
	@teacherID int
	, @district nvarchar(10) = null
	, @authority nvarchar(20) = null
	, @userSchool nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @count int

	Select @count = count(*)
	FROM
	(
	SELECT DISTINCT TA.tID
		FROM TeacherAppointment TA
			INNER JOIN Schools S
				ON TA.schNo = S.schNo
			INNER JOIN lkpIslands I
				ON S.iCode = I.iCode
		WHERE (schAuth = @authority or @authority is null)
			AND (I.iGroup = @district or @district is null)
			AND (S.schNo = @userSchool  or @userSchool is null)
			AND (TA.tID = @teacherID)
	UNION
	SELECT DISTINCT TS.tID
		FROM TeacherSurvey TS
			INNER JOIN SchoolSurvey SS
				ON TS.ssID = SS.ssID
			INNER JOIN Schools S
				ON SS.schNo = S.schNo
			INNER JOIN lkpIslands I
				ON S.iCode = I.iCode
		WHERE (schAuth = @authority or @authority is null)
			AND (I.iGroup = @district or @district is null)
			AND (SS.schNo = @userSchool  or @userSchool is null)
			AND (TS.tID = @teacherID)

	) S

	if @count = 0 begin
		Raiserror('Forbidden', 16,1);
	end

END
GO

