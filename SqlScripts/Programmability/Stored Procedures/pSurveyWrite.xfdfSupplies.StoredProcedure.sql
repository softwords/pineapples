SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	update 'supplies' questions - KI 2014 ff
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfSupplies]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Supplies block

	<field name="Supplies">
			<field name="Student">
				<value>ALL</value>
			</field>
			<field name="Teacher">
				<value>NONE</value>
			</field>
			<field name="Curriculum" />		-- y/n
			<field name="InUse" />
		</field>


*/
begin try
UPDATE schoolSurvey
	SET ssSuppliesCurriculum = case Curriculum when 'Y' then 1 when 'N' then 0 else null end
	, ssSuppliesInUse = case InUse when 'Y' then 1 when 'N' then 0 else null end
	, ssSuppliesStudent = Student
	, ssSuppliesTeacher = Teacher

FROM OPENXML(@idoc, '/x:field',2)		-- base is the Supplies Node
WITH
(
	Student			nvarchar(10)			'x:field[@name="Student"]/x:value'
	, Teacher		nvarchar(10)			'x:field[@name="Teacher"]/x:value'
	, Curriculum	nvarchar(1)			'x:field[@name="Curriculum"]/x:value'
	, InUse			nvarchar(1)			'x:field[@name="InUse"]/x:value'

) X
WHERE SchoolSurvey.ssID = @SurveyID

exec audit.xfdfInsert @SurveyID, 'Survey updated','Supplies',@@rowcount

end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,'Supplies'

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

