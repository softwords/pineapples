SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis / Ghislain Hachey
-- Create date: 12 10 2016
-- Description:	return a single quarterly report
-- =============================================
CREATE PROCEDURE [pSurveyRead].[QuarterlyReportRead]
	-- Add the parameters for the stored procedure here
	@QRID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON    -- Insert statements for procedure here

	 SELECT * from pInspectionRead.QuarterlyReports
	 WHERE qrID = @QRID

END
GO

