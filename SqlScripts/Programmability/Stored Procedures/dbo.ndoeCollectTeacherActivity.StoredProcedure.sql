SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 6 2020
-- Description:	Collect TAM, support and grades taught info
-- on teacher survey using the census workboook source data
-- This is part of the ndoe upload, but alos may be run separately.
-- =============================================
CREATE PROCEDURE [dbo].[ndoeCollectTeacherActivity]
	-- Add the parameters for the stored procedure here
	@sourceWorkbook nvarchar(50) = null
	, @surveyYear int = null
	, @schoolNo nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @t TABLE
(
tchsID int,
tID int,
tchTAM nvarchar(10),
tchSupport nvarchar(10),
hasGrade int,
hasAdmin int,
Admin nvarchar(10),
hasOther int,
isAcademic int,
staffType nvarchar(100),
jobTitle nvarchar(100)
)

INSERT INTO @t
SELECT tchsID
, tID
, tchTAM
, tchSupport
, case when coalesce(
tcheData.value('(/row/@ECE)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_0)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_1)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_2)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_3)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_4)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_5)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_6)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_7)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_8)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_9)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_10)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_11)[1]', 'nvarchar(10)')
, tcheData.value('(/row/@Grade_12)[1]', 'nvarchar(10)')
) = 'X' then 1 else 0 end
, case when tcheData.value('(/row/@Admin)[1]', 'nvarchar(10)') = 'x' then 1 else 0 end
, tcheData.value('(/row/@Admin)[1]', 'nvarchar(10)')
, case when tcheData.value('(/row/@Other)[1]', 'nvarchar(10)') = 'x' then 1 else 0 end
, case when tcheData.value('(/row/@Staff_Type)[1]', 'nvarchar(100)') = 'Teaching Staff' then 1
	when tcheData.value('(/row/@Job_Title)[1]', 'nvarchar(100)') like  '% Teacher%' then 1
	when tcheData.value('(/row/@Job_Title)[1]', 'nvarchar(100)') like  '%Principal%' then 1
	else 0 end
, tcheData.value('(/row/@Staff_Type)[1]', 'nvarchar(100)')
, tcheData.value('(/row/@Job_Title)[1]', 'nvarchar(100)')

from TeacherSurvey
	INNER JOIN SchoolSurvey SS
		ON TeacherSurvey.ssID = SS.ssID
WHERE
	(@sourceWorkbook = tcheSource or @sourceWorkbook is null)
	AND (ss.svyYear = @surveyYear or @surveyYear is null)
	and (ss.schNo = @schoolNo or @schoolNo is null)
Select *
from
(
Select T.*
, case
	when hasGrade = 1 and hasAdmin = 0 and hasOther = 0 and isAcademic = 1 then 'T'
	when hasGrade = 1 then 'M'
	when hasAdmin = 1 then 'A'
	when hasOther = 1 then 'X'
	when staffType= 'Teaching Staff' then 'T'
	when isAcademic = 1 then 'A'
	else 'X'
end TAM
, case
	when hasGrade = 0 and isAcademic = 0 then 'Support'
	else null
end Support
from @t T
) S
WHERE (tchTam is null or tchTAM <> TAM)
OR isnull(tchSupport,'') <> isnull(Support,'')
ORDER BY tchsID

UPDATE TeacherSurvey
	set tchTAM = TAM
	, tchSupport = Support
FROM TeacherSurvey
	INNER JOIN
(Select T.tchsID
, case
	when hasGrade = 1 and hasAdmin = 0 and hasOther = 0 and isAcademic = 1 then 'T'
	when hasGrade = 1 then 'M'
	when hasAdmin = 1 then 'A'
	when hasOther = 1 then 'X'
	when staffType= 'Teaching Staff' then 'T'
	when isAcademic = 1 then 'A'
	else 'X'
end TAM
, case
	when hasGrade = 0 and isAcademic = 0 then 'Support'
	else null
end Support
from @t T
) T
		ON TeacherSurvey.tchsID = T.tchsID
WHERE (tchTam is null or tchTAM <> TAM)
OR isnull(tchSupport,'') <> isnull(Support,'')

Select TeacherSurvey.tchsID
, TeacherSurvey.tchTAM
, TeacherSurvey.tchSupport
FROM TeacherSurvey
	INNER JOIN @t T
	ON TeacherSurvey.tchsID = T.tchsID
ORDER BY tchsID
END
GO

