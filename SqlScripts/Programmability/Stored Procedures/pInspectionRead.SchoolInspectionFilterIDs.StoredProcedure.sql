SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	Filter school accreditation IDs
-- =============================================
CREATE PROCEDURE [pInspectionRead].[SchoolInspectionFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@inspID int = null,
    @School nvarchar(50)	= null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50)	= null, --'e.g. GHS100'
	@InspYear nvarchar(50)	= null, --- e.g. '2016'
	@InspType nvarchar(20)  = null,  -- inspection type
	@InspBy	  nvarchar(50)  = null,  -- inspected by
	@InspResult nvarchar(20) = null, -- result

	@fileSource nvarchar(100) = null,		-- the source file - may be a google drive file ID

	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@AuthorityGroup nvarchar(10) = null,  -- govt/non-govt
	@SchoolType nvarchar(10) = null,

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

-- if filter params come from XML
if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	    @inspID = isnull(@inspID, inspID),
		@School = isnull(@School, School),
		@SchoolNo = isnull(@SchoolNo, SchoolNo),
		@InspYear = isnull(@InspYear, InspYear),
		@InspType = isnull(@InspType, InspType),
		@InspBy = isnull(@InspBy, InspBy),
		@InspResult = isnull(@InspResult, InspResult),

		@District = isnull(@District, District),
		@Authority = isnull(@Authority, Authority),
		@AuthorityGroup = isnull(@AuthorityGroup, AuthorityGroup),
		@SchoolType = isnull(@SchoolType, SchoolType)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	inspID int '@inspID',
	School nvarchar(50) '@School',
	SchoolNo nvarchar(50) '@SchoolNo',
	InspYear nvarchar(50) '@InspYear',
	InspType nvarchar(50) '@InspType',
	InspBy nvarchar(50) '@InspBy',
	InspResult nvarchar(50) '@InspResult',
	District nvarchar(10) '@District',
	Authority nvarchar(10) '@Authority',
	AuthorityGroup nvarchar(10) '@AuthorityGroup',
	SchoolType nvarchar(10) '@SchoolType'
	)
end

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)


INSERT INTO @KeysAll (ID)
SELECT inspID
from pInspectionRead.SchoolInspections INSP -- view
	LEFT JOIN Schools S
		ON INSP.schNo = S.schNo
	LEFT JOIN DimensionAuthority AUTH
			ON AUTH.AuthorityCode = S.schAuth
	LEFT JOIN lkpIslands I
		ON S.iCode = I.iCode
WHERE
(inspID = @inspID OR @inspID IS NULL)
AND (InspectionYear = @InspYear OR @InspYear IS NULL)
AND (InspectedBy  like '%' +  @InspBy  + '%' OR @InspBy IS NULL)
AND (InspTypeCode = @InspType OR @InspType IS NULL)
AND (InspectionResult = @InspResult OR @InspResult IS NULL)
AND (INSP.schNo = @SchoolNo OR @SchoolNo IS NULL)
AND (S.schName like '%' +  @School + '%' OR @School IS NULL)
AND (S.schAuth = @Authority OR @authority is null)
AND (AUTH.AuthorityGroupCode = @AuthorityGroup OR @AuthorityGroup is null)
AND (I.iGroup = @District OR @District is null)
AND (S.schType = @SchoolType or @SchoolType is null)

ORDER BY
-- strings
		case @SortColumn
			when 'schName' then INSP.schName
			when 'schNo' then INSP.schNo
			when 'InspTypeCode' then InspTypeCode
			when 'InspectedBy' then InspectedBy
			when 'InspectionResult' then InspectionResult
		end,
		--numerics
		case @sortColumn
			when 'InspectionYear' then InspectionYear
		end,
		inspID
OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

