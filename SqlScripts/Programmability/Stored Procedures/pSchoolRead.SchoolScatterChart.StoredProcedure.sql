SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 05 2010
-- Description:	School scatter chart
-- june 2014: support kml export
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SchoolScatterChart]
	-- Add the parameters for the stored procedure here
	@BaseYear int = 0,
	@XSeries nvarchar(50),
	@XSeriesOffset int,
	@YSeries nvarchar(50),
	@YSeriesOffset int,
	@xArg1 nvarchar(50) = null,
	@xFilter nvarchar(50) = null,
	@yArg1 nvarchar(50) = null,
	@yFilter nvarchar(50) = null,
	@SchoolType nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@District nvarchar(10) = null,
	@SchoolNo nvarchar(10) = null,
	@kml nvarchar(max) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @XData TABLE
(
  schNo nvarchar(50)
, XValue float
, Estimate int
, XQuality int
)


DECLARE @YData TABLE
(
  schNo nvarchar(50)
, YValue float
, Estimate int
, YQuality int
)

DECLARE @scatter TABLE
(
	schNo nvarchar(50)
,  schName nvarchar(200)

, XValue float
, YValue float
, XEstimate int
, YEstimate int
, XQuality int
, YQuality int

, authCode nvarchar(10)
, authName nvarchar(100)
, AuthorityGroupCode nvarchar(10)
, AuthorityGroup nvarchar(100)

, schType nvarchar(10)
, stDescription nvarchar(100)
, dID nvarchar(10)
, District nvarchar(50)
, langCode nvarchar(10)
, langName nvarchar(50)
, regionCode nvarchar(10)
, Region nvarchar(100)
-- 15 10 2014 added lat and long
, schLat decimal(12,8)
, schLong decimal(12,8)

-- calculate3d values
, XonY float
, YonX float
, XminusY float
, YminusX float
, XQ float
, YQ float
, YonXQ float
, YonXQuality int
, PtSelected int
, XAccum float		-- accumulated total strictly less than current XValue
, YAccum float
, XAccumInc float		-- accumulated total <= current XValue (= XAccum + XValue)
, YAccumInc float

, XAccumTot float		-- accumulated total strictly less than current XValue
, YAccumTot float

, XAccumIncTot float		-- accumulated total strictly less than current XValue
, YAccumIncTot float


, XIdx int
, YIdx int
)

-- decode the series identifier to get the stored proc
declare @XSeriesSP nvarchar(200)
declare @YSeriesSP nvarchar(200)
declare @XDataItem nvarchar(1000)
declare @YDataItem nvarchar(1000)

declare @XParameter nvarchar(100)
declare @YParameter nvarchar(100)


declare @S TABLE
(
	sID	nvarchar(20)			-- a unique id for each potential data item
	, sname nvarchar(100)
	, sproc nvarchar(500)
	, dataItem nvarchar(100)
	, parameter nvarchar(100)
)


INSERT INTO @s
VALUES -- enrolment / pupil numbers
		('ENROL_ENROL','Enrolment' , 'warehouse.SchoolDataSetEnrolment', 'enrol', null)
		,('ENROL_REP','Repeaters' , 'warehouse.SchoolDataSetEnrolment', 'rep', null )
		,('ENROL_PSA','Preschool Attenders' , 'warehouse.SchoolDataSetEnrolment', 'psa', null)
		,('ENROL_COMPLETED','Pupils Completed' , 'warehouse.SchoolDataSetEnrolment', 'completed', null)

		,('ENROL_ENROL_M','Enrolment (Male)' , 'warehouse.SchoolDataSetEnrolment', 'enrol', 'M')
		,('ENROL_REP_M','Repeaters (Male)' , 'warehouse.SchoolDataSetEnrolment', 'rep', 'M' )

		,('ENROL_ENROL_F','Enrolment (Female)' , 'warehouse.SchoolDataSetEnrolment', 'enrol', 'F')
		,('ENROL_REP_F','Repeaters (Female)' , 'warehouse.SchoolDataSetEnrolment', 'rep', 'F' )

		--,( 'Teachers' , 'pSchoolRead.SchoolDataSetTeacherCount', null)
		--,( 'Teacher Count' , 'pSchoolRead.SchoolDataSetTeacherCount', null)
		--,( 'TeachersQualCert' , 'pSchoolRead.SchoolDataSetQualCertCount', null)
		--,( 'Teacher Qualified Count' , 'pSchoolRead.SchoolDataSetQualCertCount', null)
		--,( 'Teacher Certified Count' , 'pSchoolRead.SchoolDataSetQualCertCount', null)
		--,( 'TeachersQualCertPerc' , 'pSchoolRead.SchoolDataSetQualCertPerc', null)

		-- teacher numbers
		,('STAFF_STAFF', 'All Staff', 'warehouse.schoolDataSetStaff', 'NumStaff', null)
		,('STAFF_STAFF','All Staff Qualified', 'warehouse.schoolDataSetStaff', 'NumStaffQ', null)
		,('STAFF_STAFF','All Staff Certified', 'warehouse.schoolDataSetStaff', 'NumStaffC', null)
		,('STAFF_STAFF','All Staff Cert/Qual', 'warehouse.schoolDataSetStaff', 'NumStaffCQ', null)
		,('STAFF_FTPT','Teaching Staff (FTPT)', 'warehouse.schoolDataSetStaff','TeachingFTPT', null)
		,('STAFF_FTPTQ','Teaching Staff (FTPT) Qualified', 'warehouse.schoolDataSetStaff','TeachingFTPTQ', null)
		,('STAFF_FTPTC','Teaching Staff (FTPT) Certified', 'warehouse.schoolDataSetStaff','TeachingFTPTC', null)
		,('STAFF_FTPTCQ','Teaching Staff (FTPT) Cert/Qual', 'warehouse.schoolDataSetStaff','TeachingFTPTCQ', null)

		,('STAFF_FTPTQ%','Teaching Staff (FTPT) Qualified %', 'warehouse.schoolDataSetStaff','TeachingFTPTQ%', null)
		,('STAFF_FTPTC%','Teaching Staff (FTPT) Certified %', 'warehouse.schoolDataSetStaff','TeachingFTPTC%', null)
		,('STAFF_FTPTCQ%','Teaching Staff (FTPT) Cert/Qual %', 'warehouse.schoolDataSetStaff','TeachingFTPTCQ%', null)

		,('STAFF_FTE','Teaching Staff (FTE)', 'warehouse.schoolDataSetStaff','TeachingFTE', null)
		,('STAFF_FTEQ','Teaching Staff (FTE) Qualified', 'warehouse.schoolDataSetStaff','TeachingFTEQ', null)
		,('STAFF_FTEC','Teaching Staff (FTE) Certified', 'warehouse.schoolDataSetStaff','TeachingFTEC', null)
		,('STAFF_FTECQ','Teaching Staff (FTE) Cert/Qual', 'warehouse.schoolDataSetStaff','TeachingFTECQ', null)

		,('STAFF_FTEQ%','Teaching Staff (FTE) Qualified %', 'warehouse.schoolDataSetStaff','TeachingFTEQ%', null)
		,('STAFF_FTEC%','Teaching Staff (FTE) Certified %', 'warehouse.schoolDataSetStaff','TeachingFTEC%', null)
		,('STAFF_FTECQ%','Teaching Staff (FTE) Cert/Qual %', 'warehouse.schoolDataSetStaff','TeachingFTECQ%', null)

		,('STAFF_FTPT_XA','Non-Teaching Staff (FTPT)', 'warehouse.schoolDataSetStaff','NonTeachingFTPT', null)
		,('STAFF_FTE_XA','Non-Teaching Staff (FTE)', 'warehouse.schoolDataSetStaff','NonTeachingFTE', null)
		,('STAFF_FTE_XA%','Non-Teaching Staff (FTE) %', 'warehouse.schoolDataSetStaff','NonTeachingFTE%', null)


		-- school accreditations
		,('SACC','Accreditation Level', 'warehouse.SchoolDatasetAccreditation','SCHOOL_ACCREDITATION', null)

		-- exams
		,('EXAM_C','Exam Candidates', 'warehouse.SchoolDatasetExams','Candidates', null)
		,('EXAM_CN1','Exam Candidates At Risk', 'warehouse.SchoolDatasetExams','Level1', null)
		,('EXAM_CP1','Exam Candidates At Risk %', 'warehouse.SchoolDatasetExams','Level1%', null)
		,('EXAM_CN2','Exam Candidates Level 2', 'warehouse.SchoolDatasetExams','Level2', null)
		,('EXAM_CP2','Exam Candidates Level 2 %', 'warehouse.SchoolDatasetExams','Level2%', null)
		,('EXAM_CN12','Exam Candidates Not Competent', 'warehouse.SchoolDatasetExams','Level1_2', null)
		,('EXAM_CP12','Exam Candidates Not Competent %', 'warehouse.SchoolDatasetExams','Level1_2%', null)
		,('EXAM_CN3','Exam Candidates Level 3', 'warehouse.SchoolDatasetExams','Level3', null)
		,('EXAM_C3','Exam Candidates Level 3 %', 'warehouse.SchoolDatasetExams','Level3%', null)

		,('EXAM_CN34','Exam Candidates Competent', 'warehouse.SchoolDatasetExams','Level3_4', null)
		,('EXAM_CP34','Exam Candidates Competent %', 'warehouse.SchoolDatasetExams','Level3_4%', null)
		,('EXAM_CN4','Exam Candidates Exceeding Standard', 'warehouse.SchoolDatasetExams','Level4', null)
		,('EXAM_CP4','Exam Candidates Exceeding Standard %', 'warehouse.SchoolDatasetExams','Level4%', null)

		-- exams - by indicator count
		,('EXAM_I','Exam Indicators', 'warehouse.SchoolDatasetExams','Indicators', null)
		,('EXAM_IN1','Exam At Risk (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel1', null)
		,('EXAM_IP1','Exam At Risk % (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel1%', null)
		,('EXAM_IN2','Exam Level 2 (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel2', null)
		,('EXAM_IP2','Exam Level 2 % (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel2%', null)
		,('EXAM_IN12','Exam Not Competent (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel1_2', null)
		,('EXAM_IP12','Exam Not Competent % (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel1_2%', null)
		,('EXAM_IN3','Exam  Level 3 (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel3', null)
		,('EXAM_I3','Exam Level 3 % (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel3%', null)

		,('EXAM_IN34','Exam Competent (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel3_4', null)
		,('EXAM_IP34','Exam Competent % (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel3_4%', null)
		,('EXAM_IN4','Exam Exceeding Standard (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel4', null)
		,('EXAM_IP4','Exam Exceeding Standard % (Indicator Count)', 'warehouse.SchoolDatasetExams','ILevel4%', null)

		-- toilets
		,('WASH_TOILETS_USABLE','Toilets - Usable', 'warehouse.SchoolDatasetToilets','USABLE', null)
		,('WASH_TOILETS_USABLEM','Toilets, Boys - Usable', 'warehouse.SchoolDatasetToilets','USABLEM', null)
		,('WASH_TOILETS_USABLEF','Toilets, Girls - Usable', 'warehouse.SchoolDatasetToilets','USABLEF', null)
		,('WASH_TOILETS_TOTAL','Toilets - Total', 'warehouse.SchoolDatasetToilets','TOTAL', null)
		,('WASH_TOILETS_TOTALM','Toilets, Boys - Total', 'warehouse.SchoolDatasetToilets','TOTALM', null)
		,('WASH_TOILETS_TOTALF','Toilets, Girls - Total', 'warehouse.SchoolDatasetToilets','TOTALF', null)
		-- toilet pupil ratios


Select @XseriesSP = sproc
		, @XDataItem = dataItem
		, @XParameter = parameter
		from @s
		where sname = @XSeries

Select @XSeriesSP = isnulL(@XSeriesSP, @Xseries)
Select @YseriesSP = sproc
		, @YDataItem = dataItem
		, @YParameter = parameter
		from @s
		where sname = @YSeries
Select @YSeriesSP = isnulL(@YSeriesSP, @Yseries)

declare @YearArg int

declare @SQL nvarchar(400)
declare @params nvarchar(max) = N'@year int, @dataItem nvarchar(100), @filter nvarchar(100), @schoolNo nvarchar(50), @parameter nvarchar(100)'

------ set up the XData

Select @YearArg = @BaseYear + @XSeriesOffset
SELECT @SQL = 'exec ' + @XSeriesSP + ' @year, @dataItem, @filter, @schoolNo, @parameter'

begin try
	INSERT INTO @XData
	EXEC sp_executesql  @SQL
			, @params
			, @year=@YearArg, @dataItem=@XDataItem, @filter=@XFilter, @schoolNo=@schoolNo, @parameter=@xParameter
end try
begin catch
	raiserror('Error on XData %s , %s',1,1,@SQL, @params)
end catch
--- the y data

Select @YearArg = @BaseYear + @YSeriesOffset
SELECT @SQL = 'exec ' + @YSeriesSP + ' @year, @dataItem, @filter, @schoolNo, @parameter'

begin try
INSERT INTO @YData
exec sp_executesql @SQL
		, @params
		, @year=@YearArg, @dataItem=@YDataItem, @filter=@YFilter, @schoolNo=@schoolNo, @parameter=@YParameter
end try
begin catch
	declare @msg nvarchar(500) = Error_Message()
	raiserror('Error on Y Data (%s): %s' ,16,1,@YSeries, @msg)
	return
end catch
--combine these into the main staging table

INSERT INTO @scatter
(
schNo
, schName

, XValue
, YValue
, XEstimate
,  YEstimate
, XQuality
, YQuality
,  authCode
, authName
, AuthorityGroupCode
, AuthorityGroup
,  schType
, stDescription
, dID
, District
, langCode
, langName
, regionCode
, Region
, schLat
, schLong

, XonY
, YonX
, XminusY
, YminusX

, PtSelected
, XIdx
, YIdx
)
Select
S.schNo
, s.schName

, X.XValue
, Y.YValue
, X.Estimate XEstimate
, Y.Estimate YEstimate
, X.XQuality
, Y.YQuality
, isnull(SYH.syAuth, S.schAuth) authCode
, AUTH.Authority
, AuthorityGroupCode
, AuthorityGroup
, isnull(SYH.systCode, S.schType) schType
, ST.stDescription
, dID
, dName District
, schLang
, LangName
, I.iOuter
, REG.codeDescription
-- lat and long 15 10 2014
, S.schLat
, S.schLong

, case when isnull(YValue,0) = 0 then null else XValue / YValue end XonY
, case when isnull(XValue,0) = 0 then null else YValue / XValue end YonX
, XValue - YValue XminusY
, YValue - XValue YminusX
, 0
, row_number() OVER (ORDER BY XValue)
, row_number() OVER (ORDER BY YValue)
from Schools S
LEFT JOIN @XData X
	ON S.schNo = X.schNo
LEFT JOIN @YData Y
	ON S.schNo = Y.schNo
LEFT JOIN SchoolYearHistory SYH
	ON SYH.schNo = S.schNo
	AND SYH.syYear = @BaseYear
LEFT JOIN DimensionAuthority AUTH
	ON isnull(SYH.syAuth,S.schAuth) = AUTH.authorityCode

LEFT JOIN TRSchoolTypes ST
	ON isnull(SYH.systCode, S.schType) = ST.stCode
LEFT JOIN Islands I
	ON I.iCode = S.iCode
LEFT JOIN lkpRegion REG
	ON REG.codeCode = I.iOuter
LEFT JOIN Districts D
	ON I.iGRoup = D.dID
LEFT JOIN TRLanguage LNG
	ON S.schLang= LNG.langCode
WHERE (XValue is not null or YValue is not null)
AND (dID = @District or @District is null)
AND (isnull(SYH.syAuth,S.schAuth) = @Authority or @Authority is null)
AND (isnull(SYH.systCode,S.schType) = @SchoolType or @SchoolType is null)

ORDER BY schNo, XValue, YValue

-- now we have captured the WIDx and YIdx, gt the accumulated X and Y values
-- use this to produce a 'GINI Coefficent


UPDATE @scatter
SET XAccum = (select sum(XValue) from @scatter T WHERE T.XIdx < [@scatter].XIdx)
, YAccum = (select sum(YValue) from @scatter Y WHERE Y.YIdx < [@scatter].YIdx)
, XAccumInc = (select sum(XValue) from @scatter T WHERE T.XIdx <= [@scatter].XIdx)
, YAccumInc = (select sum(YValue) from @scatter Y WHERE Y.YIdx <= [@scatter].YIdx)


declare @sumX float
declare @avgX float
declare @stdevX float
declare @minX float
declare @maxX float
declare @medianX float
declare @Q1X float
declare @Q3X float
declare @sumY float
declare @avgY float
declare @stdevY float
declare @minY float
declare @maxY float
declare @medianY float
declare @Q1Y float
declare @Q3Y float

declare @avgYonX float
declare @stdevYonX float
declare @minYonX float
declare @maxYonX float
declare @medianYonX float
declare @Q1YonX float
declare @Q3YonX float


declare @XAccumTot float
declare @YAccumTot float
declare @XAccumIncTot float
declare @YAccumIncTot float
declare @GiniX float
declare @GiniY float

;
with X as
(
	Select *
	, row_number() OVER (ORDER BY XValue, schNo) UPX
	, row_number() OVER (ORDER BY XValue DESC, schNo DESC ) DOWNX
	, row_number() OVER (ORDER BY YValue, schNo) UPY
	, row_number() OVER (ORDER BY YValue DESC, schNo DESC ) DOWNY
	from @Scatter
)
Select @sumX = sum(XValue)
, @avgX = avg(XValue)
, @stdevX = stdev(XValue)
, @minX = min(XValue)
, @maxX = max(XValue)
, @medianX = avg(case when (UPX - DOWNX) between -1 and 1 then XValue else null end)
, @Q1X = avg(case when (3*UPX - DOWNX) between -3 and 3 then XValue else null end)
, @Q3X = avg(case when (3*DOWNX - UPX) between -3 and 3 then XValue else null end)

, @sumY = sum(YValue)
, @avgY = avg(YValue)
, @stdevY = stdev(YValue)
, @minY = min(YValue)
, @maxY= max(YValue)
, @medianY = avg(case when (UPY - DOWNY) between -1 and 1 then YValue else null end)
, @Q1Y = avg(case when (3*UPY - DOWNY) between -3 and 3 then YValue else null end)
, @Q3Y= avg(case when (3*DOWNY - UPY) between -3 and 3 then YValue else null end)

, @XAccumTot = sum(XAccum)
, @YAccumTot = sum(YAccum)
, @XAccumIncTot = sum(XAccumInc)
, @YAccumIncTot = sum(YAccumInc)


, @GiniX = 1 - 2 * sum(XAccumInc)/(sum(XValue) * Count(*))
, @GiniY = 1 - 2 * sum(YAccumInc)/(sum(YValue) * Count(*))
from X


;
WITH X AS
(
	Select YonX
	, row_number() OVER (ORDER BY YonX, schNo) UP
	, row_number() OVER (ORDER BY YonX DESC, schNo DESC ) DOWN
	from @Scatter
		WHERE isnull(XValue,0) <> 0

)
Select @avgYonX = avg(YonX)
, @stdevYonX = stdev(YonX)
, @minYonX = min(YonX)
, @maxYonX = max(YonX)
, @medianYonX = avg(case when (UP - DOWN) between -1 and 1 then YonX else null end)
, @Q1YonX = avg(case when (3*UP - DOWN) between -3 and 3 then YonX else null end)
, @Q3YonX = avg(case when (3*DOWN - UP) between -3 and 3 then YonX else null end)
from X

declare @IQX float		-- interquartile range
declare @IQY float		-- interquartile range
declare @IQYonX float


Select @IQX = @Q3X - isnull(@Q1X,0)
Select @IQY = @Q3Y - isnull(@Q1Y,0)
Select @IQYonX = @Q3YonX - isnull(@Q1YonX,0)


UPDATE @scatter
SET XQ =
 case when @IQX = 0 then 0
		when XValue > @Q3X then (XValue - @Q3X) / @IQX
		when XValue < @Q1X then (@Q1X - XValue) / @IQX
		else null
	end
, YQ = case when @IQY = 0 then 0
		 when YValue > @Q3Y then (YValue - @Q3Y) / @IQY
		when YValue < @Q1Y then (@Q1Y - YValue) / @IQY
		else null
	end
, YonXQ = case when @IQYonX = 0 then 0
		when YonX > @Q3YonX then (YonX - @Q3YonX) / @IQYonX
		when YonX < @Q1YonX then (@Q1YonX - YonX) / @IQYonX
		else null
	end
, YonXQuality =
	case when YQuality is null and XQuality is null then null
		when YQuality = 2 or XQuality = 2 then 	2
		when YQuality = 1 or XQuality = 1 then 1
		else  0
  end

--------------------------------------------------------------------------------
-- 18 10 2014 (caulfield cup!)
-- support fo kml with the @kml flag
if (@kml is null) begin
	-- first recordset is the data
	SELECT schNo schoolId	, *		-- 2 copies of school no to overcome echarts v4 bug cannot use caterogy dimension as Visual Map dimension
	, row_Number() OVER (ORDER BY schNo) - 1 PtIndex
	from @scatter
	ORDER By schNo

	-- second is the statistical values
	SELECT
	case when sum((XValue - @AvgX)*(XValue - @AvgX)) * sum((YValue - @avgY)*(YValue - @avgY)) = 0 then null
	else
	sum((XValue - @AvgX)*(YValue - @avgY))
	/ power
		(sum((XValue - @AvgX)*(XValue - @AvgX)) *sum((YValue - @avgY)*(YValue - @avgY))
		, .5)
	end correlation
	, @avgX Xavg
	, @avgY Yavg
	, @avgYonX YonXAvg

	, @stdevX Xstdev
	, @stdevY Ystdev
	, @stdevYonX YonXstdev

	, @minX Xmin
	, @minY Ymin
	, @minYonX YonXmin

	, @maxX Xmax
	, @maxY Ymax
	, @maxYonX YonXmax

	, @medianX Xmedian
	, @medianY Ymedian
	, @medianYonX YonXmedian

	, @Q1X XQ1
	, @Q1Y YQ1
	, @Q1YonX YonXQ1

	, @Q3X XQ3
	, @Q3Y YQ3
	, @Q3YonX YonXQ3

	, @XSeries XSeries
	, @BaseYear + @XSeriesOffset XYear
	, @XSeries + ' ' + convert(nvarchar(4),  @BaseYear + @XSeriesOffset)
		+ isnull(' (' + @xArg1 + ')', '') XAxisLabel

	, @YSeries YSeries
	, @BaseYear + @YSeriesOffset YYear
	, @YSeries + ' ' + convert(nvarchar(4),  @BaseYear + @YSeriesOffset)
		+ isnull(' (' + @yArg1 + ')', '') YAxisLabel
	, @GiniX XGini
	, @GiniY YGini
	, @SumX Xsum
	, @SumY Ysum
	, @XAccumTot XAccumTot
	, @YAccumTot YAccumTot
	, @XAccumIncTot XAccumIncTot
	, @YAccumIncTot YAccumIncTot
	FROM @scatter
	WHERE xValue is not null and yValue is not null


	-- third is the quality analysis
	SELECT count(schNo) schoolCount
	, sum(case when XQ > 3 then 1 else null end) XIQ3
	, sum(case when YQ > 3 then 1 else null end) YIQ3
	, sum(case when YonXQ > 3 then 1 else null end) YonXIQ3
	, sum(case when XQ > 3 then
				case when (YonXQuality is not null) then 1 else 0 end
			 else null end) XIQ3QualityAlerts

	, sum(case when YQ > 3 then
				case when (YonXQuality is not null) then 1 else 0 end
			 else null end) YIQ3QualityAlerts

	, sum(case when YonXQ > 3 then
				case when (YonXQuality is not null) then 1 else 0 end
			 else null end) YonXIQ3QualityAlerts

	-- these are points with all alerts confirmed
	, sum(case when XQ > 3 then
				case when (YonXQuality = 0) then 1 else 0 end
			 else null end) XIQ3QualityOK

	, sum(case when YQ > 3 then
				case when (YonXQuality = 0) then 1 else 0 end
			 else null end) YIQ3QualityOK

	, sum(case when YonXQ > 3 then
				case when (YonXQuality = 0) then 1 else 0 end
			 else null end) YonXIQ3QualityOK
	, sum(case when YonXQ > 3 then XValue else null end) XSumIQ3
	, sum(case when YonXQ > 3 then YValue else null end) YSumIQ3
	, sum(XValue) XSum
	, sum(YValue) YSum

	from @Scatter
	end


--------------------------------------------------------------------------------
-- 18 10 2014
-- support fo kml with the @kml flag
-- we export xml in this form to schools kml
-- <data><d key="schNo"><ExtendedData><Data name="xvalue"><displayName>....</displayName>


if (@kml is not null)begin

declare @dt TABLE
(
Tag int
, parent int
, schNo nvarchar(50)
, seq int
, name nvarchar(100)
, displayname nvarchar(100)
, value nvarchar(100)
, yr int
, est nvarchar(10)
)

-- root
INSERT INTO @dt
(Tag, Parent, SchNo, seq)
SELECT 1, null, null, 0


-- first the schools
INSERT INTO @dt
(Tag,
Parent,
Schno,
Seq
)
Select 10
, 1
, schNo
, 0
FROM @scatter

-- node for extndeddata
INSERT INTO @dt
(Tag,
Parent,
Schno,
Seq
)
Select 11
, 10			-- child of d
, schNo
, 0
FROM @scatter


-- xvalue

	-- xvalue - twice
	INSERT INTO @dt
	(Tag,
	Parent,
	Schno,
	Seq,
	name,
	displayname,
	value,
	yr,
	est
	)
	Select num
	, 11
	, schNo
	, 0
	, 'xvalue'
	, replace(@XSeries,'pSchoolRead.schoolDataSet','')
	, xvalue
	,@BaseYear + @XSeriesOffset
	, case when xEstimate = 1 then 'est' else '' end
	FROM @scatter
	-- xvalue
	CROSS JOIN metaNumbers
	WHERE num in (21,22, 23)

	-- xvalue - twice
	INSERT INTO @dt
	(Tag,
	Parent,
	Schno,
	Seq,
	name,
	displayname,
	value,
	yr,
	est
	)
	Select num
	, 11
	, schNo
	, 0
	, 'yvalue'
	, replace(@YSeries,'pSchoolRead.schoolDataSet','')
	, Yvalue
	,@BaseYear + @YSeriesOffset
	, case when yEstimate = 1 then 'est' else '' end
	FROM @scatter
	-- xvalue
	CROSS JOIN metaNumbers
	WHERE num in (31,32,33)

declare @extdata xml

Select @extdata =
(
Select Tag, Parent
	, 'schoolScatterChart' [data!1!src]
	, schNo				 [d!10!key]
	, schNo				 [ExtendedData!11!!hide]

	, name				 [Data!21!name]
	, displayname		 [Data!21!displayName!element]
	, value				 [Data!21!value!element]
	, 'xyear'			 [Data!22!name]
	, yr				 [Data!22!value!element]
	, 'xest'			 [Data!23!name]
	, est				 [Data!23!value!element]


	, name				 [Data!31!name]
	, displayname		 [Data!31!displayName!element]
	, value				 [Data!31!value!element]

	, 'yyear'			 [Data!32!name]
	, yr				 [Data!32!value!element]
	, 'yest'			 [Data!33!name]
	, est				 [Data!33!value!element]


FROM @dt
ORDER BY schNo, Tag, seq
FOR XML EXPLICIT
)

-- Now invoke SchoolsKML with the extended data, and the balloon template passed by the caller

exec dbo.schoolsKml @kml, @extdata
end
END
GO

