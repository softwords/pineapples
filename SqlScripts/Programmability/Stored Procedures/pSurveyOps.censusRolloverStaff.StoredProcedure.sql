SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 August 2021
-- Description:	Create a recordset to populate the census workbook staff page
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusRolloverStaff]
	-- Add the parameters for the stored procedure here
	@SurveyYear int
	, @schoolNo nvarchar(50) = null
	, @districtCode nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @ON nvarchar(1) = 'X';
	Declare @OFF nvarchar(1) = null;
SELECT TS.tID, SS.schNo, SS.svyYear,
nullif(ltrim(v.value('@Index', 'int')),'')                           [Index]

-- calculated
--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                           [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')                              [State]
-- pick up any name edit
--, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                        [School_Name]
, S.schName																		[School_Name]
-- calculated from the name
--, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                          [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                     [School_Type]
, nullif(ltrim(v.value('@Office', 'nvarchar(100)')),'')                             [Office]

/* name from TeacherIdentity */
--, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                         [First_Name]
--, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                        [Middle_Name]
--, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                          [Last_Name]
, TI.tGiven																			[First_Name]
, TI.tMiddleNames																	[Middle_Name]
, TI.tSurname																		[Last_Name]
-- forget about full name, calculated fields are ignored
--, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                          [Full_Name]
-- Gender requires description, not code
--, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')                             [Gender]
, G.codeDescription																	[Gender]
, TI.tDOB																			[Date_of_Birth]
--, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')                      [Date_of_Birth]
-- age will be recalculated on the workbook
--, nullif(ltrim(v.value('@Age', 'nvarchar(100)')),'')								[Age]

-- citizenship is tchCitizenship (code)
, coalesce(N.codeDescription
	, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),''))                   [Citizenship]
-- ethnicity is not 'normalised - only retrieved from the xml
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                          [Ethnicity]
-- FSM_SSN => tPayroll
, coalesce(TI.tPayroll
	, nullif(ltrim(v.value('@FSM_SSN', 'nvarchar(100)')),'')
	)																				[FSM_SSN]
-- Other_SSN => tchProvident
, coalesce(tchProvident,
	nullif(ltrim(v.value('@OTHER_SSN', 'nvarchar(100)')),'')
	)																				[OTHER_SSN]
, coalesce(Q.codeDescription,
	nullif(ltrim(v.value('@Highest_Qualification', 'nvarchar(100)')),'')
	)																				[Highest_Qualification]
, coalesce(SUBJ.subjName
	-- field_of_study is preserved if not a valid subject
	, nullif(ltrim(v.value('@Field_of_Study', 'nvarchar(100)')),'')
)																					[Field_of_Study]
, nullif(ltrim(v.value('@Year_of_Completion', 'nvarchar(100)')),'')                 [Year_of_Completion]
, coalesce(EQ.codeDescription,
	nullif(ltrim(v.value('@Highest_Ed_Qualification', 'nvarchar(100)')),'')
	)																				[Highest_Ed_Qualification]
, nullif(ltrim(v.value('@Year_Of_Completion2', 'nvarchar(100)')),'')                [Year_Of_Completion2]
-- teacher survey records only exist when emloyment status is Active...
, nullif(ltrim(v.value('@Employment_Status', 'nvarchar(100)')),'')                  [Employment_Status]
-- ..so Reason will not be copied to the new workbook
--, nullif(ltrim(v.value('@Reason', 'nvarchar(100)')),'')                             [Reason]

-- where possible, the job title is aligned with RoleGrades
, coalesce(RG.rgDescription,
-- in some case a hardcoded teacher role may have been put on TeacherSurvey - use this description
	TROLE.codeDescription,
-- otherwise job title will be preserved if it is not mapped to a RoleGrade.
-- If this value is not edited in the workbook, it will throw a validation error on upload
	nullif(ltrim(v.value('@Job_Title', 'nvarchar(100)')),''))                          [Job_Title]
--- for debugging
,	nullif(ltrim(v.value('@Job_Title', 'nvarchar(100)')),'')                          [Job_Title_Raw]
,	TROLE.codeCode																		[Role]
,	RG.rgCode																			[RoleGrade]
, coalesce(ORG.employerName,
	nullif(ltrim(v.value('@Organization', 'nvarchar(100)')),''))                       [Organization]
, nullif(ltrim(v.value('@Staff_Type', 'nvarchar(100)')),'')                         [Staff_Type]
,
	-- Teacher_Type may be present as Teacher-Type, kludge to handle that

	coalesce(
		STAT.statusDesc,
		nullif(ltrim(v.value('@Teacher_Type', 'nvarchar(100)')),''),
		nullif(ltrim(v.value('@Teacher-Type', 'nvarchar(100)')),'')
		)																			[Teacher_Type]


-- nasty bug in workbook has a trailing space in this column name
-- this is a temporary workaround
-- fix in workbook, and double check in ListObject management in C# that column labels are trimmed
-- note that this technique could be used to handle vocab driven column names in workbooks....
, coalesce( TI.tDatePSAppointed
, common.safeDate(v.value('@Date_of_Hire', 'nvarchar(100)'))
, common.safeDate(v.value('@Date_of_Hire_', 'nvarchar(100)'))
)																					[Date_of_Hire]
, coalesce(
common.safeDate(v.value('@Date_of_Exit', 'nvarchar(100)'))
, common.safeDate(v.value('@Date_of_Exit_', 'nvarchar(100)'))

)																					[Date_Of_Exit]

, nullif(ltrim(v.value('@Annual_Salary', 'nvarchar(100)')),'')                      [Annual_Salary]
, nullif(ltrim(v.value('@Funding_Source', 'nvarchar(100)')),'')                     [Funding_Source]

, case when ltrim(isnull(v.value('@PRK', 'nvarchar(10)'),'')) > '' then @ON else @OFF end PRK
, case when ltrim(isnull(v.value('@ECE', 'nvarchar(10)'),'')) > '' then @ON else @OFF end ECE
, case when ltrim(isnull(v.value('@Grade_1', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_1
, case when ltrim(isnull(v.value('@Grade_2', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_2
, case when ltrim(isnull(v.value('@Grade_3', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_3
, case when ltrim(isnull(v.value('@Grade_4', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_4
, case when ltrim(isnull(v.value('@Grade_5', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_5
, case when ltrim(isnull(v.value('@Grade_6', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_6
, case when ltrim(isnull(v.value('@Grade_7', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_7
, case when ltrim(isnull(v.value('@Grade_8', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_8
, case when ltrim(isnull(v.value('@Grade_9', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_9
, case when ltrim(isnull(v.value('@Grade_10', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_10
, case when ltrim(isnull(v.value('@Grade_11', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_11
, case when ltrim(isnull(v.value('@Grade_12', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_12
, case when ltrim(isnull(v.value('@Grade_13', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_13
, case when ltrim(isnull(v.value('@Grade_14', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_14
, case when ltrim(isnull(v.value('@Grade_15', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Grade_15

, case when ltrim(isnull(v.value('@Admin', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Admin
, case when ltrim(isnull(v.value('@Other', 'nvarchar(10)'),'')) > '' then @ON else @OFF end Other

-- dont report total days absence in rollover data - wont be constant year to year
--, nullif(ltrim(v.value('@Total_Days_Absence', 'decimal(5,1)')),'')                    [Total_Days_Absence]

, case when ltrim(isnull(v.value('@Maths', 'nvarchar(10)'),'')) > '' then @ON else @OFF end			[Maths]
, case when ltrim(isnull(v.value('@Science', 'nvarchar(10)'),'')) > '' then @ON else @OFF end		[Science]
, case when ltrim(isnull(v.value('@Language', 'nvarchar(10)'),'')) > '' then @ON else @OFF end		[Language]
, case when ltrim(isnull(v.value('@Competency', 'nvarchar(10)'),'')) > '' then @ON else @OFF end	[Competency]

, case when ltrim(isnull(v.value('@Teach_Mathematics', 'nvarchar(10)'),'')) > '' then @ON else @OFF end [Teach_Mathematics]
, case when ltrim(isnull(v.value('@Teach_Language_Arts', 'nvarchar(10)'),'')) > '' then @ON else @OFF end [Teach_Language_Arts]
, case when ltrim(isnull(v.value('@Teach_Social_Studies', 'nvarchar(10)'),'')) > '' then @ON else @OFF end [Teach_Social_Studies]
, case when ltrim(isnull(v.value('@Teach_Sciences', 'nvarchar(10)'),'')) > '' then @ON else @OFF end [Competency]

, v.query('.')																			[rowXml]

from TeacherSurvey TS
INNER JOIN TeacherIdentity TI
	ON TI.tID = TS.tID
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
INNER JOIN lkpGender G
	ON TI.tSex = G.codeCode
INNER JOIN Schools S
	ON S.schNo = SS.schNo
LEFT JOIN Islands I
	ON S.iCode = I.iCode
LEFT JOIN lkpNationality N
	ON TS.tchCitizenship = N.codeCode
LEFT JOIN lkpTeacherQual Q
	ON TS.tchQual = Q.codeCode
LEFT JOIN lkpTeacherQual EQ
	ON TS.tchEdQual = EQ.codeCode
LEFT JOIN lkpOrganizations ORG
	ON TS.tchSponsor = ORG.employerCode
LEFT JOIN lkpTeacherStatus STAT
	ON tchStatus = STAT.statusCode
CROSS APPLY TS.tcheData.nodes('row') as V(v)
LEFT JOIN lkpSubjects SUBJ
-- we only need to join on the aliases
-- if we match subjName, the xml value is passed anyway
	ON v.value('@Field_of_Study','nvarchar(100)') in (SUBJ.subjNameL1, SUBJ.subjNameL2)
LEFT JOIN RoleGrades RG
	-- this join looking in the 3 description columns will translate legacy values according to values prepopulated on RoleGrades through careful examination of existing data
	ON v.value('@Job_Title','nvarchar(100)') in (RG.rgDescriptionL1, RG.rgDescriptionL2)
	-- some roles have been hardcoded because the Job_Title is too non-standard
LEFT JOIN lkpTeacherRole TROLE
	ON TS.tchRole = TROLE.codeCode
WHERE SS.svyYear = @SurveyYear
AND (SS.schNo = @schoolNo OR @schoolNo is null)
AND (I.iGroup = @districtCode OR @DistrictCode is null)
ORDER BY SS.schNo, TI.tSurname, TI.tGiven
END
GO

