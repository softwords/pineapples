SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 18 04 2018
-- Description:	Extended read of exam for web site
-- =============================================
CREATE PROCEDURE [pExamRead].[ExamReadEx]
	@ExamID int
AS
BEGIN

	SET NOCOUNT ON;

    Select *
 	FROM Exams E
	WHERE exID = @examID

	-- TODO Any other table we could pull here?
	-- direct from Exam data, ie no intermediate warehouse table?
	Select * from pExamread.CompareLevelTyped('(nation)',null, null) WHERE examID = @examID

	-- get the exam structure (standards, benchmarks, indicators) as a single table
	-- using the view ExamFrames

	Select * from pExamRead.ExamFrames
	WHERE examID = @ExamID
	ORDER BY Code

	-- exam item analysis
	Select A.*
	, I.IndicatorKey
	, I.BenchmarkKey
	, I.StandardKey
	from warehouse.ExamItemAnalysis A
		INNER JOIN ExamItems I
		ON I.exItemCode = A.exItemCode
	WHERE I.exID = @ExamID AND A.exID = @ExamID
	ORDER BY I.exItemSeq
END
GO

