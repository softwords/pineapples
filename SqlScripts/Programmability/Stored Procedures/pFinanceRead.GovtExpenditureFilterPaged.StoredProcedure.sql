SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 3 9 2019
-- Description:
-- =============================================
CREATE PROCEDURE [pFinanceRead].[GovtExpenditureFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@fnmID int = null,
    @Year int	= null,
	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		ID int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pFinanceRead.GovtExpenditureFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	@fnmID,
    @Year,
	@District,
	@xmlFilter


-------------------------------------
-- return results

SELECT *

  FROM GovtExpenditureState GX
	INNER JOIN @Keys AS K ON GX.fnmID = K.ID
	ORDER BY K.RecNo

-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

