SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:
-- =============================================
CREATE PROCEDURE [pInspectionRead].[SchoolInspectionFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@InspID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50) = null, --'e.g. GHS100'
	@InspYear nvarchar(50) = null, --- e.g. '2016'
	@InspType nvarchar(20)  = null,  -- inspection type
	@InspBy	  nvarchar(50)  = null,  -- inspected by
	@InspResult nvarchar(20) = null, -- result

	@fileSource nvarchar(100) = null,		-- the source file - may be a google drive file ID

	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@AuthorityGroup nvarchar(10) = null,  -- govt/non-govt
	@SchoolType nvarchar(10) = null,

	@XmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		ID int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pInspectionRead.SchoolInspectionFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	@InspID,
	@School,
	@SchoolNo,
	@InspYear,
	@InspType,
	@InspBy,
	@InspResult,
	@FileSource,

	@District,
	@Authority,
	@AuthorityGroup,
	@SchoolType,

	@xmlFilter


-------------------------------------
-- return results

SELECT [inspID]
      ,[schNo]
      ,[schName]
      ,[PlannedDate]
      ,[StartDate]
      ,[EndDate]
      ,[InspectedBy]
      ,[InspectionSetID]
      ,[InspectionResult]
      ,[SourceId]
      ,[InspectionSetName]
      ,[InspectionType]
      ,[InspectionYear]
      ,[InspectionClass]
      ,[Partition]
	  ,InspTypeCode

  FROM [pInspectionRead].[SchoolInspections] SI
	INNER JOIN @Keys AS K ON SI.inspID = K.ID
	ORDER BY K.RecNo

-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

