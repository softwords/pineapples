SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 3 2021
-- Description:	Sheet A2 of UIS Survey
-- =============================================
CREATE PROCEDURE [warehouse].[uisA2]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE  @Gov Table
(
Gov nvarchar(1),
GovGroup nvarchar(10)
)


DECLARE  @d Table
(
Isced nvarchar(10),
Gov nvarchar(1),
Flag nvarchar(1),
EnrolM int,
EnrolF int,
Enrol int
)

INSERT INTO @Gov
VALUES ('G','Public'),
('N','Private')

-- build the data
-- this set ensures that all public private cells get populated
-- but only if the isced class has some data in the year
INSERT INTO @d
SELECT DISTINCT
DL.[ISCED SubClass] Isced,
AUTH.codeCode Gov,
null Flag,
0 EnrolM,
0 EnrolF,
0 Enrol

from warehouse.tableEnrol E
INNER join DimensionLevel DL on E.ClassLevel = DL.LevelCode
CROSS join lkpAuthorityGovt AUTH
WHERE surveyYear = @year


-- 2) this set supplies the actual records - this is sparse
INSERT INTO @d
SELECT
DL.[ISCED SubClass] Isced,
DA.AuthorityGroupCode Gov,
convert(nvarchar(1),null) Flag,
case E.GenderCode when 'M' then Enrol end EnrolM,
case E.GenderCode when 'F' then Enrol end EnrolF,
Enrol Enrol
from warehouse.tableEnrol E
left join DimensionLevel DL on E.ClassLevel = DL.LevelCode
left join DimensionAuthority DA on E.AuthorityCode = DA.AuthorityCode

where E.SurveyYear = @year AND E.Enrol IS NOT NULL


-- 3) Missing data (M flag)
-- this identifies the ISCED codes are in DimmensionLevel
-- (ie there is a ClassLevel defined for that Isced level)
-- but there is no enrolment data for that ISCED level at all.
-- therefore we go with M - missing
INSERT INTO @d
SELECT
ilsCode Isced,
AUTH.codeCode Gov,
'M' Flag,
null EnrolM,
null EnrolF,
null Enrol

FROM ISCEDLevelSub I
INNER JOIN DimensionLevel DL
ON I.ilsCode = DL.[ISCED SubClass]
CROSS join lkpAuthorityGovt AUTH
WHERE DL.LevelCode not in (select E.ClassLevel from warehouse.EnrolNation E where surveyYear = @year)

-- 4) Not applicablt ( Z flag)
-- this identifies ISCED codes that are not in DimmensionLevel
-- ie there are no Grades defined that use these codes.
-- therefore we go with Z - not applicable
INSERT INTO @d
SELECT
ilsCode Isced,
AUTH.codeCode Gov,

'Z' Flag,
null EnrolM,
null EnrolF,
null Enrol

FROM (ISCEDLevelSub I
LEFT JOIN DimensionLevel DL
ON I.ilsCode = DL.[ISCED SubClass])
CROSS join lkpAuthorityGovt AUTH
WHERE DL.LevelCode is null


-- Aggregate the results

Select Isced,
GovGroup,
max(Flag) Flag,
sum(EnrolM) EnrolM,
sum(EnrolF) EnrolF,
sum(Enrol) Enrol

FROM
@d D
INNER JOIN @Gov G
ON D.Gov = G.Gov

group by Isced, GovGroup
order by isced, GovGroup


END
GO

