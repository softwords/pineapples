SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 26 04 2021
-- Description:	Filter of school inspection links
-- =============================================
CREATE PROCEDURE [pInspectionRead].[SchoolInspectionLinkFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@LinkID				int = null,		-- the unique idenitifer of the
	@InspectionID		int = null,
	@DocumentID			uniqueidentifier = null,
	@Keyword			nvarchar(50) = null,
	@Function			nvarchar(50) = null,
	@DateStart			datetime = null,
	@DateEnd			datetime = null,
	@DocumentSource		nvarchar(100) = null,
	@DocType			nvarchar(10) = null,
	@DocCloudID			nvarchar(50) = null,
	@DocCloudRemoved	int = null,
	@IsImage			int = null
AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pInspectionRead.SchoolInspectionLinkFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

		, @LinkID
		, @InspectionID
		, @DocumentID
		, @Keyword
		, @Function
		, @DateStart
		, @DateEnd
		, @DocumentSource
		, @DocType
		, @DocCloudID
		, @DocCloudRemoved
		, @IsImage


--- column sets ----
	Select lnkID
	, IL.inspID
	, IL.docID
	, lnkFunction
	, lnkHidden
	, docTitle
	, docDescription
	, docSource
	, docDate
	, docRotate
	, docTags
	, docType
	, docCloudID
	, docCloudRemoved
	, IL.pCreateUser
	, IL.pCreateDateTime
	, IL.pEditUser
	, IL.pEditDateTime
	, IL.pRowversion
	, SI.schName
	, SI.StartDate
	, SI.EndDate
	, SI.InspectedBy
	, SI.InspectionYear

	FROM pInspectionRead.InspectionLinks IL
		INNER JOIN @keys K
			ON IL.lnkID = K.ID
		LEFT JOIN SchoolInspections SI
			ON IL.inspID = SI.inspID
	ORDER BY recNo


--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

