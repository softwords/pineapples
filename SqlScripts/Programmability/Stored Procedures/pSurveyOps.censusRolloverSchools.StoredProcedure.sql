SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 August 2021
-- Description:	Create a recordset to populate the census workbook school page
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusRolloverSchools]
	-- Add the parameters for the stored procedure here
	@SurveyYear int
	, @schoolNo nvarchar(50) = null
	, @districtCode nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @ON nvarchar(1) = 'X';
	Declare @OFF nvarchar(1) = null;

-- decode items in Resources
-- The load process creates Resource records to represent various hardcoded resource types
-- Theoretically, we should support the possibility that these records may be edited, giving a more up-to-date
-- version of the value. This is the philosphy of the rollover from the database - that the 'best' values held in the database
-- are rolled over, not just those collected the previous year

DECLARE @resources TABLE
(
	ssID int
	, Telephone nvarchar(1)
	, Fax nvarchar(1)
	, [HF Radio] nvarchar(1)
	, Copier nvarchar(1)
	, Internet nvarchar(1)
	, [Computer Lab] nvarchar(1)
	, [Use of Technology] nvarchar(1)
)
INSERT INTO @resources
SELECT ssID
  , Telephone , Fax, [HF Radio], Copier, Internet, [Computer Lab], [Use of Technology]
FROM
(
Select ssID, resSplit, case resAvail when -1 then 'Y' when 0 then 'N' else null end Avail
FROM
Resources
WHERE resSplit in ('Telephone','Fax','HF Radio', 'Copier', 'Internet','Computer Lab','Use Of Technology')
) SRC
PIVOT
(
  max(avail)
  FOR resSplit in (Telephone , Fax, [HF Radio], Copier, Internet, [Computer Lab], [Use of Technology])
) PIV
ORDER BY ssID


SELECT SS.schNo, SS.svyYear,
nullif(ltrim(v.value('@Index', 'int')),'')                           [Index]
--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                         [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')                           [State]
, schName			[School_Name]
--, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                        [School_Name]
--, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                          [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                     [School_Type]
-- convert the budget back to an int (safely)
, nullif(convert(int, round(v.value('@School_Budget', 'decimal(17,5)'),0)),0)	    [School_Budget]
, nullif(ltrim(v.value('@Date_School_Begins', 'nvarchar(100)')),'')	RawDate

-- Date_School_Begins : may as well leave this out because it can never be the same!

-- try to be safe with the date school begins - get a string and try_ convert to int
--, common.try_convert_int(
--	nullif(ltrim(v.value('@Date_School_Begins', 'nvarchar(100)')),''))
--																					[Date_School_Begins]
, coalesce(LANG.langName,
	 nullif(ltrim(v.value('@Language_of_Instruction', 'nvarchar(100)')),'')
	 )																		[Language_of_Instruction]
, nullif(v.value('@Num_Buildings', 'int'),0)								[Num_Buildings]
, nullif(v.value('@Num_Classrooms', 'int'),0)								[Num_Classrooms]
, nullif(v.value('@Num_Classrooms_in_poor_condition', 'int'),0)				[Num_Classrooms_in_poor_condition]
, nullif(ltrim(v.value('@Electricity', 'nvarchar(100)')),'')                        [Electricity]
, nullif(ltrim(v.value('@Electricity_Source', 'nvarchar(100)')),'')                 [Electricity_Source]
, nullif(ltrim(v.value('@HIV_Education', 'nvarchar(100)')),'')                      [HIV_Education]
, nullif(ltrim(v.value('@Sexuality_Education', 'nvarchar(100)')),'')                [Sexuality_Education]
, nullif(ltrim(v.value('@COVID-19_Education', 'nvarchar(100)')),'')                 [COVID-19_Education]
, nullif(ltrim(v.value('@SpEd_Infrastructure', 'nvarchar(100)')),'')                 [SpEd_Infrastructure]
, Coalesce(RESOURCES.Telephone,
	nullif(ltrim(v.value('@Telephone', 'nvarchar(100)')),'')
	)																				[Telephone]
, Coalesce(RESOURCES.Fax,
	nullif(ltrim(v.value('@Fax', 'nvarchar(100)')),'')
	)																				[Fax]
, Coalesce(RESOURCES.[HF Radio],
	nullif(ltrim(v.value('@HF_Radio', 'nvarchar(100)')),'')
	)																				[HF_Radio]
, Coalesce(RESOURCES.Copier,
	nullif(ltrim(v.value('@Copier', 'nvarchar(100)')),'')
	)																				[Copier]
, Coalesce(RESOURCES.Internet,
	nullif(ltrim(v.value('@Internet', 'nvarchar(100)')),'')
	)																				[Internet]
, Coalesce(RESOURCES.[Computer Lab],
	nullif(ltrim(v.value('@Computer_Lab', 'nvarchar(100)')),'')
	)															                    [Computer_Lab]
, common.try_convert_int(
	 nullif(ltrim(v.value('@Num_Computers', 'nvarchar(100)')),'')
)																					[#_Computers]

, Coalesce(RESOURCES.[Use of Technology],
	nullif(ltrim(v.value('@Use_of_Technology', 'nvarchar(100)')),'')
	)																				[Use_of_Technology]
, v.query('.')			-- returns the xml object v from its root node 'row'
from SchoolSurvey SS
INNER JOIN Schools S
	ON S.schNo = SS.schNo
LEFT JOIN @resources RESOURCES
	ON SS.ssID = RESOURCES.ssID
LEFT JOIN Islands I
	ON S.iCode = I.iCode
LEFT JOIN lkpLanguage LANG
	ON SS.ssLang = LANG.langCode

OUTER APPLY SS.ssData.nodes('row') as V(v)
WHERE SS.svyYear = @SurveyYear
AND (SS.schNo = @schoolNo OR @schoolNo is null)
AND (I.iGroup = @districtCode OR @DistrictCode is null)

END
GO

