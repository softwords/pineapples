SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 8 2020
-- Description:	Report various table data created from a census workbook.
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusOutput]
	-- Add the parameters for the stored procedure here
	@fileReference uniqueidentifier			-- guid from the filedb of the census book
AS
BEGIN
	exec pSurveyOps.censusStudentOutput @fileReference
	exec pSurveyOps.censusStaffOutput @fileReference
END
GO

