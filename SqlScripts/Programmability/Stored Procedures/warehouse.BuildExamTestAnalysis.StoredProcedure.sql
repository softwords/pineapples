SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:
-- =============================================
CREATE PROCEDURE [warehouse].[BuildExamTestAnalysis]
	-- Add the parameters for the stored procedure here
	@examID int = null
AS
BEGIN
	SET NOCOUNT ON;

Declare @t TABLE
(
exID int
, item nvarchar(100)
, A int NULL
, B int NULL
, C int NULL
, D int NULL
, E int NULL
, F int NULL
, Other int NULL
, Answer nvarchar(1)
, Responses int
, Correct int
, CorrectPerc decimal(6,3)
, VarianceP float
, TopScore int
, TopCount int
, BottomScore int
, BottomCount int
, DI float
)


INSERT INTO @t
(
exID
, item
, A
, B
, C
, D
, E
, F
, Other
, Answer
, Responses
, Correct
, CorrectPerc
, VarianceP
)
select exID
--, substring(exItemCode,10,8)
, exItemCode
, sum(case exciResponse when 'A' then 1 end) A
, sum(case exciResponse when 'B' then 1 end) B
, sum(case exciResponse when 'C' then 1 end) C
, sum(case exciResponse when 'D' then 1 end) D
, sum(case exciResponse when 'E' then 1 end) E
, sum(case exciResponse when 'F' then 1 end) F
, sum(case when exciREsponse not in ('A','B','C','D','E','F') then 1 end) Other
, min(case when exciResult = 1 then exciResponse end) Answer
, count(*) Responses
, sum(exciResult) Correct
, convert(float,sum(exciResult) * 100)/count(*) CorrectPerc
, varp(exciResult) VarianceP			--using population variance
from ExamCandidateItems I
	INNER JOIN ExamCandidates C
		ON I.excID = C.excID
WHERE (exID = @examID or @examID is null)
GROUP BY exID, exItemCode
ORDER BY exID, exItemCode


; WITH TB (Grp, exID, excID, TotalScore)
AS
(
Select *
FROM
(
Select TOP 27 PERCENT 'TOP' Grp, *
FROM
(
Select exID, I.excID, sum(exciResult) TotalScore
	from ExamCandidateItems I
	INNER JOIN ExamCandidates EC
	ON I.excID = EC.excID

WHERE (exID = @examID or @examID is null)
GROUP BY exID, I.excID
) SUB
ORDER BY TotalScore DESC, excID DESC
) S2
UNION ALL

Select *
from
(
Select TOP 27 PERCENT 'BOTTOM' Grp, *
FROM
(
Select exID, I.excID, sum(exciResult) TotalScore
	from ExamCandidateItems I
	INNER JOIN ExamCandidates EC
	ON I.excID = EC.excID

WHERE (exID = @examID or @examID is null)
GROUP BY exID, I.excID
) SUB
ORDER BY  TotalScore ASC , excID ASC
)S2
)
UPDATE @t
SET TopScore = TT.topScore
, TopCount = TT.TopCount
, BottomScore = TT.BOTTOMScore
, BottomCount = Tt.BOTTOMCount
,  DI = convert(float, TT.topscore) / TT.topcount - convert(float,TT.bottomscore) / TT.bottomcount
FROM
(
Select exItemCode
, sum(case when grp = 'TOP' then exciResult end) TOPScore
, sum(case when grp = 'TOP' then 1 end) TOPCount
, sum(case when grp = 'BOTTOM' then exciResult end) BOTTOMScore
, sum(case when grp = 'BOTTOM' then 1 end) BOTTOMCount

from TB
INNER JOIN ExamCandidateItems EC
ON TB.excID = EC.excID
GROUP BY exItemCode
) TT
WHERE TT.exItemCode = [@T].item

DELETE FROM warehouse.ExamItemAnalysis
WHERE (exID = @examID or @examID is null)

raiserror('%i records deleted from warehouse.ExamItemAnalysis',0,1,@@rowcount)
INSERT INTO warehouse.ExamItemAnalysis
(exID
,exitemCode
,A
,B
,C
,D
,E
,F
,Other
,iaAnswer
,iaResponses
,iaCorrect
,iaCorrectPerc
,iaVarianceP
, iaTopScore
, iaTopCount
, iaBottomScore
, iaBottomCount
, iaDI
)
Select * from @t
raiserror('%i records inserted into warehouse.ExamItemAnalysis',0,1,@@rowcount)

-- update the population variance of total scores
declare @varp float


Select @varp = varp(TotalScore)
FROM
(
Select I.excID, sum(exciResult) TotalScore
	from ExamCandidateItems I
	INNER JOIN ExamCandidates EC
	ON I.excID = EC.excID
WHERE exID = @examID
GROUP BY I.excID
) SUB

UPDATE Exams
SET exVarianceP = @varp
WHERE exID = @examID


Select * from Exams
WHERE exID = @examID

Select * from warehouse.ExamItemAnalysis
WHERE (exID = @examID or @examID is null)

END
GO

