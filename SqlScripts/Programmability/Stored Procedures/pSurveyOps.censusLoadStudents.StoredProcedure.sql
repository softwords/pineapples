SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Procedure
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of student data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusLoadStudents]
@xml xml
, @filereference uniqueidentifier
, @user nvarchar(50) = null
, @ValidationMode int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- set up the user and date time of this operation
	SELECT @user = coalesce(@user, original_login())
	declare @opDateTime datetime = getutcDate()

DECLARE @ndoe TABLE
(
RowIndex int
-- Start Generated Code
--	          , SchoolYear                   nvarchar(100) NULL
--              , [State]                      nvarchar(100) NULL
              , School_Name                nvarchar(100) NULL
              , School_No                  nvarchar(100) NULL
--              , School_Type                nvarchar(100) NULL
              , National_Student_ID        nvarchar(100) NULL
              , First_Name                 nvarchar(100) NULL
              , Middle_Name                nvarchar(100) NULL
              , Last_Name                  nvarchar(100) NULL
              , Full_Name                  nvarchar(100) NULL
              , Gender                     nvarchar(100) NULL
              , inDate_of_Birth            nvarchar(100) NULL
              , inDoB_Estimate             nvarchar(50) NULL
              , Age                        int NULL
              , Citizenship                nvarchar(100) NULL
              , Ethnicity                  nvarchar(100) NULL
              , Grade_Level                nvarchar(100) NULL
              , [From]                       nvarchar(100) NULL
              , Transferred_From_which_school   nvarchar(100) NULL
              , inTransfer_In_Date         nvarchar(100) NULL
              , SpEd_Student               nvarchar(100) NULL
			  , [IDEA_SA_ECE]				nvarchar(100) NULL
			  , [IDEA_SA_School_Age]		nvarchar(100) NULL
			  , Disability					nvarchar(100) NULL
			  , English_Learner				nvarchar(100) NULL

  			  , Has_IEP						nvarchar(100) NULL
			  , Has_SBA_Accommodation		nvarchar(100) NULL
			  , Type_of_Accommodation		nvarchar(100) NULL
			  , Assessment_Type				nvarchar(100) NULL
			  , Exiting						nvarchar(100) NULL

              , Full_Name_2                nvarchar(100) NULL
              , Days_Absent                decimal(10,2) NULL
              , Completed                 nvarchar(100) NULL
              , Outcome                    nvarchar(100) NULL
              , Dropout_Reason             nvarchar(100) NULL
              , Expulsion_Reason           nvarchar(100) NULL
              , Post_secondary_study       nvarchar(100) NULL
-- END generated Code
, rowXml					 xml					-- this stores the entire raw xml of the row for posterity

, fileReference		uniqueidentifier
, studentID uniqueidentifier		-- search in the Student_ table

-- put some normalised values in here
, clsLevel nvarchar(10)			-- Grade Level is the grade name, translate back to code
, yearofEd	int					-- the realted year of ed to the class - to get the right value for SpEdEnv
, tfrSchNo nvarchar(50)			-- school number transferred from Transferred_From_which_school is the school name

, spDisability	 nvarchar(20)	-- special ed - disability code
, spEnv			 nvarchar(20)	-- special ed environment
, spEnglish		nvarchar(10)	-- special ed english learner
, spAccommodation	nvarchar(10) -- special ed accommodation
, spAssessmentType  nvarchar(10)
, spExit			nvarchar(10)

, ssID		int


-- cleanups
, Date_of_Birth          date NULL
, DoB_Estimate			 int			-- convert this from No Yes to 1 or null
, Transfer_In_Date		 date NULL
)


declare @SurveyYear int
declare @districtName nvarchar(50)
declare @firstRow int

declare @districtID nvarchar(10)
declare @stage nvarchar(200)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8
-- startpos = 8

Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
, @firstRow = v.value('@FirstRow', 'int')
From @xml.nodes('ListObject') as V(v)


Select @districtID = dID
from Districts WHERE dName = @districtName

print @SurveyYear
print @DistrictID


---------------------------------------------------------
begin try
----------------------------------------------------------
--INSERT INTO NdoeStudents_
Select @stage = 'Creating temp table'

INSERT INTO @ndoe
Select
v.value('@Index', 'int') [ RowIndex]
-- Start Gendereted Code -- a macro in the workbook can generate this from column names
--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')											[State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                                   [School_Type]
, nullif(ltrim(v.value('@National_Student_ID', 'nvarchar(100)')),'')                              [National_Student_ID]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                                       [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                                      [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                                        [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                                        [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')											[Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')								[Date_of_Birth]
, nullif(ltrim(v.value('@DoB_Estimate', 'nvarchar(50)')),'')												[DoB_Estimate]
, nullif(ltrim(v.value('@Age', 'int')),'')														  [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                                      [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                                        [Ethnicity]
, nullif(ltrim(v.value('@Grade_Level', 'nvarchar(100)')),'')                                      [Grade_Level]
, nullif(ltrim(v.value('@From', 'nvarchar(100)')),'')                               [From]
, nullif(ltrim(v.value('@Transferred_From_which_school', 'nvarchar(100)')),'')                                  [Transferred_From_which_school]
, nullif(ltrim(v.value('@Transfer_In_Date', 'nvarchar(100)')),'')                                 [Transfer_In_Date]
, nullif(ltrim(v.value('@SpEd_Student', 'nvarchar(100)')),'')                                     [SpEd_Student]

, nullif(ltrim(v.value('@IDEA_SA_ECE', 'nvarchar(100)')),'')                                    [IDEA_SA_ECE]
, nullif(ltrim(v.value('@IDEA_SA_School_Age', 'nvarchar(100)')),'')                             [IDEA_SA_School_Age]
, nullif(ltrim(v.value('@Disability', 'nvarchar(100)')),'')										 [Disability]
, nullif(ltrim(v.value('@English_Learner', 'nvarchar(100)')),'')								  [English_Learner]

, nullif(ltrim(v.value('@Has_IEP', 'nvarchar(100)')),'')											[Has_IEP]
, nullif(ltrim(v.value('@Has_SBA_Accommodation', 'nvarchar(100)')),'')								[Has_SBA_Accommodation]
, nullif(ltrim(v.value('@Type_of_Accommodation', 'nvarchar(100)')),'')								[Type_Of_Accommodation]
, nullif(ltrim(v.value('@Assessment_Type', 'nvarchar(100)')),'')									[Assessment_Type]
, nullif(ltrim(v.value('@Exiting', 'nvarchar(100)')),'')											[Exiting]

, nullif(ltrim(v.value('@Full_Name_2', 'nvarchar(100)')),'')                                      [Full_Name_2]
, nullif(ltrim(v.value('@Days_Absent', 'nvarchar(100)')),'')                                      [Days_Absent]
, nullif(ltrim(v.value('@Completed', 'nvarchar(100)')),'')                                        [Completed]
, nullif(ltrim(v.value('@Outcome', 'nvarchar(100)')),'')                                          [Outcome]
, nullif(ltrim(v.value('@Dropout_Reason', 'nvarchar(100)')),'')                                   [Dropout_Reason]
, nullif(ltrim(v.value('@Expulsion_Reason', 'nvarchar(100)')),'')                                 [Expulsion_Reason]
, nullif(ltrim(v.value('@Post_secondary_study', 'nvarchar(100)')),'')                             [Post_secondary_study]
-- End Generated Code
, v.query('.')																			[rowXml]

, @fileReference
, null	-- the student Id - set to null first, it will get filled by matches where possible, then any remaining unmatched get a NEWID()
-- placeholders for the normalised fields
, null			-- class level; from lkp levels based on class name
, null			-- year of ed
, null			-- transfer school; from schools based on name

, null			-- sp disability
, null			-- sp environment
, null			-- sp english learner
, null			-- sp accommodation
, null			-- sp assessment
, null			-- sp exit

, null			-- ssID from SchoolSurvey based on school no and year
-- cleanups
, null				-- Date_Of_Birth
, null				---Dob_Estimate
, null				-- Transfer_In_Date
from @xml.nodes('ListObject/row') as V(v)


-- poulate the normalised values
-- normalise school names for transfer schools
UPDATE @ndoe
SET tfrSchNo = S.schNo
FROM @ndoe
LEFT JOIN Schools S
	ON [@ndoe].Transferred_From_which_school in (S.schNo, S.schName)


-- date values need to be cleaned up depending  on whether they are presented as a string or number
Select @stage = 'Process date of birth'

UPDATE @ndoe
SET Date_Of_Birth =
	case ISNUMERIC(inDate_Of_Birth)
		when 0 then	case isdate(inDate_Of_Birth) when 1 then convert(date, inDate_Of_Birth) else null end
		when 1 then cast( convert(float, inDate_Of_Birth) - 2 as datetime)
	end
, DoB_Estimate =
	case inDoB_Estimate
		when 'Y' then 1
		when 'Yes' then 1
		when '1' then 1
		when 'unknown' then 1			-- allow for some confirmation we really don;t know the DoB
		else null end
, Transfer_In_Date =
	case ISNUMERIC(inTransfer_In_Date)
		when 0 then	case  isdate(inTransfer_In_Date) when 1 then convert(date, inTransfer_In_Date) else null end
		when 1 then cast( convert(float, inTransfer_In_Date) - 2 as datetime)
	end

-- normalise grade levels
Select @stage = 'Normalise grade levels'

UPDATE @ndoe
SET clsLevel = codeCode
, yearofEd = lvlYear
FROM @ndoe
LEFT JOIN lkpLEvels L
	ON [@ndoe].Grade_Level = L.codeDescription

print 'before gender'

-- normalise Gender
Select @stage = 'Normalise Gender'
UPDATE @ndoe
SET Gender = case when codeCode is null then  Gender else codeCode end		-- avoid isnull becuase it will case as nvarchar(1)
FROM @ndoe
LEFT JOIN lkpGender G
	ON [@ndoe].Gender = G.codeDescription


-- normalise ethnicity
Select @stage = 'Normalise ethnicity'
UPDATE @ndoe
SET ethnicity = codeCode
FROM @ndoe
LEFT JOIN lkpEthnicity E
	ON [@ndoe].Ethnicity = E.codeDescription

--- nrmalisations for special ed codes
Select @stage = 'Normalise special education codes'
update @ndoe
SET spDisability = codeCode
FROM @ndoe
	INNER JOIN lkpDisabilities D
		ON [@ndoe].Disability = D.codeDescription

-- special ed environment -- chose between ECE and school age fields, based on the pupil's grade
update @ndoe
SET spEnv = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdEnvironment ENV
		ON
	case when yearofed <= 0 then coalesce(IDEA_SA_ECE, IDEA_SA_School_Age)  else coalesce(IDEA_SA_School_Age, IDEA_SA_ECE) end
	 = ENV.codeDescription

-- special ed english learner
update @ndoe
SET spEnglish = codeCode
FROM @ndoe
	INNER JOIN lkpEnglishLearner E
		ON
		[@ndoe].English_Learner	 = E.codeDescription

-- special ed accommodation
update @ndoe
SET spAccommodation = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdAccommodations E
		ON
		[@ndoe].Type_of_Accommodation = E.codeDescription

		-- special ed assessment type
update @ndoe
SET spAssessmentType = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdAssessmentTypes E
		ON
		[@ndoe].Assessment_Type= E.codeDescription

-- special ed exit
update @ndoe
SET spExit = codeCode
FROM @ndoe
	INNER JOIN lkpSpEdExits E
		ON
		[@ndoe].Assessment_Type= E.codeDescription

---------------------------------------------------------
----- PROCESSING --------
---------------------------------------------------------
-- first clean up:
-- remove any enrolments for @SurveyYear for any of the schools represented in this file
 -- clear any existing entries
 print 'Deleting student enrolments'
 Select @stage = 'Deleting student enrolments'

 DELETE FROM StudentEnrolment_
 WHERE schNo in (Select school_No from @ndoe)  -- issue #648
 AND stueYear = @SurveyYear

 print @@Rowcount

 print 'Deleting students'
 -- now remove any Student_ records that have no enrolment - this allows us to recreate those records
 Select @stage = 'Deleting students'
 DELETE from Student_
 WHERE stuID not in (select stuId from StudentEnrolment_)
 AND stuID not in (select stuID from Scholarships_)
 print @@Rowcount


-- Student Level records
-- How to identify an exisitng student record:

-- Must match on National ID, PLUS 2 of Last_Name DoB School of Enrolment ( this year same as last)


-- Matchng on dob and Gender - otherwise we get incorrect enrolment totals if previous versions are used

-- StudentId family name

-- family name given name school (update student ID)

-- family name given name school transfer

-- Not matching on DOB - if everything else matches, and the Student_ record says DOB is EStimate,
--  use the Student and update the Dob


--- TEST 1 - perfect match: on student ID, DoB and gender, (family name OR reversed name )
----
UPDATE @ndoe
SET studentID = STU.stuID


FROM @ndoe
INNER JOIN Student_ STU
	ON [@ndoe].National_Student_ID = STU.stuCardID
	AND (
		[@ndoe].Last_Name = STU.stuFamilyName
		OR (
			[@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName
		)
	)
	AND	[@ndoe].Date_of_Birth = STU.stuDoB
	AND [@ndoe].Gender = STU.stuGender
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)

-- not matching on student ID - but full name or reverse full name and the current school
UPDATE @ndoe
SET studentID = STU.stuID


FROM @ndoe
INNER JOIN Student_ STU
	ON (
		( [@ndoe].First_Name = STU.stuGiven AND [@ndoe].Last_Name = STU.stuFamilyName)
		OR
		 ([@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName)
	)
	AND	[@ndoe].Date_of_Birth = STU.stuDoB
	AND [@ndoe].Gender = STU.stuGender

INNER JOIN StudentEnrolment_ STUE
	ON STU.stuID = STUE.stuID

WHERE
	[@ndoe].studentID is null
	AND STUE.stueYear = (@SurveyYear - 1)
	AND STUE.schNo = [@ndoe].School_No
	AND ( [@ndoe].[From] is null or [@ndoe].[From] in ('ECE', 'REP'))
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)

-- not matching student id - matching dob gender name and transfer school
UPDATE @ndoe
SET studentID = STU.stuID


FROM @ndoe
INNER JOIN Student_ STU
	ON
	(
		( [@ndoe].First_Name = STU.stuGiven AND [@ndoe].Last_Name = STU.stuFamilyName)
		OR
		 ([@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName)
	)
	AND	[@ndoe].Date_of_Birth = STU.stuDoB
	AND [@ndoe].Gender = STU.stuGender

INNER JOIN StudentEnrolment_ STUE
	ON STU.stuID = STUE.stuID
WHERE
	[@ndoe].studentID is null
	AND STUE.stueYear = (@SurveyYear - 1)
	AND STUE.schNo = [@ndoe].tfrSchNo
	AND (  [@ndoe].[From] in ('Transfer In'))
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)


--- LAST CASE not matching DoB - but DoB is marked as Estimated orginally, but not estimated in input
--- and everything else matches (first name, last name, student ID gender, school)
--- In this event, we can update the DoB to the new DoB, and set DoBEstimate = 0
UPDATE @ndoe
SET studentID = STU.stuID


FROM @ndoe
INNER JOIN Student_ STU
	ON [@ndoe].National_Student_ID = STU.stuCardID
	AND
	(
		( [@ndoe].First_Name = STU.stuGiven AND [@ndoe].Last_Name = STU.stuFamilyName)
		OR
		 ([@ndoe].Last_Name = STU.stuGiven AND [@ndoe].First_Name = STU.stuFamilyName)
	)
	AND	STU.stuDoBest = 1 and [@ndoe].DoB_Estimate is null
	AND [@ndoe].Gender = STU.stuGender

INNER JOIN StudentEnrolment_ STUE
	ON STU.stuID = STUE.stuID

WHERE
	[@ndoe].studentID is null
	AND STUE.stueYear = (@SurveyYear - 1)
	AND STUE.schNo = [@ndoe].School_No
	AND ( [@ndoe].[From] is null or [@ndoe].[From] in ('ECE', 'REP'))
	-- dont allocate it if it is already in use for this year
	AND studentID not in (Select stuID from StudentEnrolment_ WHERE stueYear = @surveyYear)


 -- now any retireved identities are on @ndoe
 -- other records not matched
 print 'update student'
select @stage = 'Update identified students'
 UPDATE Student_
 SET stuEthnicity = ndoe.Ethnicity
 , stuCardID = ndoe.National_Student_ID			-- we may update the student ID if we had a string enough match on everything else
 , stuDoB = ndoe.Date_of_Birth					-- we may be updating an estimate
 , stuDobEst =
	case DoB_Estimate when null then null
		when 1 then stuDobEst				-- ie the estimate flag is unchanged if the incoming data shouws estimate
		else null end
 , stuEditFileRef = @filereference
 FROM Student_
 INNER JOIN  @ndoe ndoe
 ON ndoe.studentID = Student_.stuID
 print @@rowcount
 ---populate the reamining studentIDs now with new guids

 print 'Assign guids for unknown students'
 Select @stage = 'Assign guids for unknown students'
 UPDATE @ndoe
 SET studentID = newID()
 WHERE studentID is null
 print @@rowcount
 -- Now insert any unidientified records
 print 'insert student'
 Select @stage = 'Insert new students'

 INSERT INTO Student_
 (
 stuID
 , stuGiven
 , stuFamilyName
 , stuDoB
 , stuDobEst
 , stuGender
 , stuCardID
 , stuCreateFileRef
 , stuEditFileRef
 , pCreateUser
 , pCreateDateTime
 , pEditUser
 , pEditDateTime
 )
 Select
 StudentID
 , First_Name
 , Last_Name
 , Date_of_Birth
 , case DoB_Estimate when null then null when 1 then 1 else null end
 , left(Gender,1)
 , National_Student_ID
 , @fileReference
 , @filereference
 , @user
 , @opDateTime
 , @user
 , @opDateTime
 FROM @ndoe ndoe
 WHERE ndoe.studentID not in (Select stuID from Student_)
 print @@rowcount
 -- Populate the StudentEnrolment_ records

 print 'insert student enrolment'
 Select @stage = 'Insert student enrolment'

 INSERT INTO StudentEnrolment_
(
    stuID
    ,schNo
    ,stueYear
    ,stueClass
    ,stueFrom
    ,stueFromSchool
    ,stueFromDate

    ,stueSpEd
	, stueSpEdEnv
	, stueSpEdDisability
	, stueSpEdEnglish
	, stueSpEdIEP
	, stueSpEdHasAccommodation
	, stueSpEdAccommodation
	, stueSpEdAssessment
	, stueSpEdExit

    ,stueDaysAbsent
    ,stueCompleted
    ,stueOutcome
    ,stueOutcomeReason
	,stueCreateFileRef
	,stueCreateFileRow
	, stueData		-- the row Xml goes here
 , pCreateUser
 , pCreateDateTime
 , pEditUser
 , pEditDateTime
)
SELECT
StudentID
, School_No
, @SurveyYear
, clsLevel
, case [From] when 'Repeater' then 'REP'
				when 'Transferred In' then 'TRIN'
				when 'ECE' then 'ECE' end
, tfrSchNo
, Transfer_In_Date
, case SpEd_Student when 'No' then 0 when 'Yes' then 1 else null end
-- get the correct value for spEd Env from the ECE and school age versions - only one should be not null
, spEnv
, spDisability
, spEnglish
, case has_IEP when 'Yes' then 1 when 'No' then 0 else null end
, case when Type_of_Accommodation is null then 0 else 1 end
, spAccommodation
, spAssessmentType
, spExit

, Days_Absent
, case Completed when 'No' then 'N' when 'Yes' then 'Y' else null end
, Outcome
, case outcome
	when 'Expelled' then coalesce(expulsion_reason, dropout_reason)
	else coalesce(dropout_reason, expulsion_reason)
  end
, @filereference
, RowIndex
 , rowXml
 , @user
 , @opDateTime
 , @user
 , @opDateTime
FROM @ndoe
print @@rowcount
-- create any records required in SchoolSurvey
-- but they should not be needed if we have already processed Schools
 Select @stage = 'Insert school survey'
INSERT INTO SchoolSurvey
(
svyYear
, schNo
, ssSchType
, ssAuth
, ssElectN
, ssElectL
, ssSource
)
Select DISTINCT @SurveyYear
, NDOE.School_No
, schType
, schAuth
, schElectN
, schElectL
, @filereference

FROM @ndoe NDOE
INNER JOIN Schools S
	ON NDOE.School_No = S.schNo
LEFT JOIN SchoolSurvey SS
	ON NDOE.School_No = SS.schNo
	AND @SurveyYear = SS.svyYear
WHERE SS.ssID is null

-- for convenience, put the ssID back on the @ndoe table
 Select @stage = 'Log school survey Number'
UPDATE @ndoe
SET ssID = SS.ssID
FROM @ndoe
	INNER JOIN SchoolSurvey SS
		ON SS.schNo = [@ndoe].School_No
		AND SS.svyYear = @SurveyYear

------------------------------------------------------
--- roll up the totals in Enrollments and PupilTables
--- from the StudentEnrolment_ records

-- refactored into a separate sp - 20 4 2019
Select @stage = 'Roll up enrolment numbers'
exec pSurveyOps.CensusStudentEnrolmentRollup @SurveyYear

---------- end of processing --------------------------

end try

begin catch
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error ,
		 @ErrorMessage = ERROR_MESSAGE() + ':' + @stage,
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

