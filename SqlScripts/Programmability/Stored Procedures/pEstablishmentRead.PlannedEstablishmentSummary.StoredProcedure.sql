SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 10 2009
-- Description:
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[PlannedEstablishmentSummary]
	-- Add the parameters for the stored procedure here
	@Summary int = 0
	, @EstYear int
	, @AsAtDate datetime		-- date for reporting the current position
	, @NumPointIncrements int = 0		-- number of points to increment current positions when estimating future salary
	, @AdjustMedianSalaryPoint int = 0
	, @ProjectUnderPay bit = 0		-- if this flag is on, positions currently underpaid will be assumed to stay underpaid
									-- if it is off (default), underpaid positions are recosted at the nomiual cost for the future

-- the next fields are filtering fields

	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;

declare @MaxSurveyYear int
declare @PayslipDate datetime

Select @maxSurveyYear = max(svyYear)
from
	Survey

-- to support using either the snapshot or authority date
-- get the last payroll period before the AsAtDate

Select @PayslipDate = max(tpsPeriodEnd)
from
	TeacherPayslips
WHERE tpsPeriodStart <= @AsAtDate

/*********************************************
Temporary Table Definitions
***********************************************/
-- temp table #data holds the planned establishment and totals drawn from Establishment (ie poisition count)
create table #data
(
	schNo nvarchar(50)
	, roleGrade nvarchar(10)
	, numQuota int
	, numPlanned int
	, numPositions int
	, numFilled int
	, numFilledSalaryPointComplies int
	, numFilledUnderPay int
	, NumUnfilled int
	, NumRequired int
	, FilledPayFN money
	, FuturePay money
	, FuturePaySalaryPointComplies money		-- the future pay of those who are not currently underpaid


)

-- rates applicable to each paylevel at the date fo the establishment
create table #rates
(
	spCode nvarchar(10)
	, salLevel nvarchar(5)
	, spSalary money
	, spHAllow money
	, AnnualTotal money
	, spEffective datetime
	, pointOrder int
)

-- the minium pointOrder for any salary level - used for checking if a point is in a range
create table #salaryLevelPointOrder
(
	salLevel nvarchar(5)
	, levelMinPointOrder int
)


create table #projectedEnrolStats
(
	schNo nvarchar(50)
	, ProjectedEnrol int
	, minLevelCode nvarchar(10)
	, maxLevelCode nvarchar(10)
	, NumLocations int				-- number of schools that are consolidated - ie including extension schools 1 if no extensions


)


/**************************************************************************
Populate temporary Tables
***************************************************************************/
-- get the salary rates that will be in effect for the establishment date

-- #rates

INSERT INTO #rates
SELECT
	spCode
	, salLevel
	, spSalary
	, spHAllow
	, isnull(spSalary,0) + isnull(spHAllow,0) AnnualTotal
	, spEffective
	-- this row number based on sort is so we can do an increment
	, row_number() OVER (ORDER BY spSort)
FROM
	(
	SELECT
		SalaryPointRates.*
		, spSort
		, salLevel
		-- row number to get the first pay effective date desc
		-- RowNum = 1 for the last Effective date before the Authority Date
		, row_number() over( PARTITION BY lkpSalaryPoints.spCode ORDER BY spEffective DESC)  RowNum
	FROM
		lkpSalaryPoints
		INNER JOIN SalaryPointRates
			ON LkpSalaryPoints.spCode = SalaryPointRates.spCode
		, EstablishmentControl
	WHERE
		EstablishmentControl.estcYear = @EstYear
		AND spEffective <= estcAuthorityDate
	) sub
	WHERE RowNum = 1


-- #SalaryLevelPointOrder

-- what is the minimum point order for each level?
-- we need this to know if an appointment is underpaid

INSERT INTO #salaryLevelPointOrder
SELECT
	salLevel
	, min(PointOrder)
FROM
	#rates
GROUP BY
	salLevel


-- #data

-- the crux of the SP is to build the data combining the plan with the current positions
-- and appoint/costing info
-- this is done by using a union to bring them together, then sum that union

INSERT INTO #data
SELECT
	schNo
	, RoleGrade
	, sum(Quota)
	, sum(Planned)
	, sum(Positions)
	, sum(Filled)
	, isnull(sum(Filled),0) - isnull(sum(FilledUnderPay),0)
	, sum(FilledUnderPay)
	, isnull(sum(Positions),0) - isnull(sum(Filled),0)
	, isnull(sum(Planned),0) - isnull(sum(Positions),0)
	, sum(PayFN)
	, sum(FuturePay)
	, sum(FuturePayCorrectLevel)

FROM
	(
	-- first part of the union is the plan from SchoolEstablishment
	SELECT
		SE.schNo
		, estrRoleGrade RoleGrade
		, estrQuota Quota
		, estrCount Planned
		, null	Positions
		, null	Filled
		, null FilledUnderPay
		, null	PayFN
		, null FuturePay
		, null FuturePayCorrectLevel

		FROM SchoolEstablishment SE
			INNER JOIN SchoolEstablishmentRoles SER
				on SE.estID = SER.estID
			LEFT JOIN Schools S
				ON S.SchNo = SE.SchNo
		WHERE
			estYear = @EstYear

		-- if we are after just one school, cut to the chase
			AND (@schoolNo = SE.schNo or @SchoolNo is null)
			AND (@Authority = S.schAuth or @Authority is null)
			AND (@SchoolType = S.schType or @SchoolType is null)
		UNION ALL

	-- second part gets the current actuals from Establishment and TeacherAppointments etc
	SELECT
		E.schNo
		, estpRoleGrade
		, null
		, null
		, 1
		, case when Appts.taID is null then 0 else 1 end NumFilled
		-- num filled under pay
		-- the teacher is underpaid if the pointorder of their current salary point
		-- is less than the minimum point order for the minimum level of the RoleGrade
		, case when RateNow.PointOrder < LvlPtOrder.levelMinPointOrder then 1
				else 0
		  end numFilledUnderPay
		, slips.tpsGross			-- fortnightly actual salary, from the payslip
		, RateFuture.AnnualTotal
		, case when RateNow.PointOrder >= LvlPtOrder.levelMinPointOrder then RateFuture.AnnualTotal
				else 0		--11 04 2010
		  end FuturePayCorrectPay	-- current pay rate of those whi are not underapid for their position

		FROM
			Establishment E
			LEFT JOIN Schools S
				ON S.SchNo = E.schNo
			INNER JOIN RoleGRades RG
				ON E.estpRoleGRade = RG.rgCode
			-- this is to find the minimum salary point associated to the RoleGrade of the position
			LEFT JOIN #salaryLevelPointOrder LvlPtOrder
				ON LvlPtOrder.salLevel = RG.rgSalaryLevelMin
			LEFT JOIN
					-- current appt at the AsAtDate
					( Select *
						from TeacherAppointment  Appt
						WHERE (Appt.taDate <= @asAtDate)
						AND (Appt.taEndDate >= @AsAtDate or Appt.taEndDate is null)
					) Appts
				ON Appts.estpNo = E.estpNo
			-- teacher for that appt, if any
			LEFT JOIN TEacherIdentity TI
				ON TI.tID = Appts.tID
			LEFT JOIN
					-- pay slip for the pay period of the AsAt Date
					-- we need the Payroll No from Teacher Identity to find this
					( Select *
						from TeacherPaySlips
						WHERE tpsPeriodEnd = @PayslipDate
					) Slips
				   ON Slips.tpsPayroll = TI.tPayroll
			-- use two copies of #rates
			-- the first is the current salarypaypoint, the join increments that
			LEFT JOIN #rates RateNow
				ON RateNow.spCode = slips.tpsSalaryPoint
			LEFT JOIN #rates RateFuture
				ON RateNow.pointOrder + @NumPointIncrements = RateFuture.pointOrder
		WHERE
			estpActiveDate <=@AsAtDate
			AND (@AsAtDate <=estpClosedDate or estpClosedDate is null)

			-- if we are after just one school, cut to the chase
			AND (@schoolNo = E.schNo or @SchoolNo is null)
			AND (@Authority = S.schAuth or @Authority is null)
			AND (@SchoolType = S.schType or @SchoolType is null)


	) U


	GROUP BY schNo, roleGrade


-- #projectedEnrolStats

declare @escnID int				-- the establishemtns scenario ID

SELECT @escnID = escnID
FROM
	EstablishmentControl
WHERE
	estcYear = @estYEar

-- 17 04 2010
-- support for Extension/PArent
-- this table now need to hold the sum across all schools that consolidate to the same parent
-- This may be different in the future year than current, as a school may be promoted from Exension to standalone
-- So , we have to get the EstablishmentPoint from SchoolEstablishment
INSERT INTO #projectedEnrolStats

SELECT
	PE.schNo
	, PE.ProjectedEnrol
	, min(case when lvlYEar = PE.lvlMinYr then EPD.epdLevel else null end) MinLevelCode
	, max(case when lvlYEar = PE.lvlMaxYr then EPD.epdLevel else null end) MaxLevelCode
	, PE.NumLocations
FROM
	(Select
		estEstablishmentPoint schNo
		, @escnID escnID			-- use the variable gets around the group by
		, sum(epdSum) ProjectedEnrol
		, min(lkpLevels.lvlYear) lvlMinYr		-- this is the min of year of education, but not a level code
		, max(lkpLevels.lvlYear) lvlMaxYr
		, count(DISTINCT SE.schNo) NumLocations
	from
		SchoolEstablishment SE
		LEFT JOIN EnrolmentProjection
				ON SE.estYear = EnrolmentProjection.epYear
				AND SE.schNo = EnrolmentProjection.schNo
				AND EnrolmentProjection.escnID = @escnID


		LEFT JOIN EnrolmentProjectionData
			ON enrolmentProjection.epID = enrolmentProjectionData.epID
		LEFT JOIN lkpLevels
			ON lkpLevels.codeCode = EnrolmentProjectionData.epdLevel
	WHERE
		SE.estYear = @estYear
	group by
		estEstablishmentPoint
	) PE
	-- all the rest is just to get the correct set of levels again,
	-- to get the minimum and max levels by matching to Year of Ed
	LEFT JOIN EnrolmentProjection
		ON EnrolmentProjection.escnID = PE.escnID
		AND EnrolmentProjection.schNo = PE.schNo
	LEFT JOIN enrolmentProjectionData EPD
		ON enrolmentProjection.epID = EPD.epID
	LEFT JOIN lkpLevels
		ON lkpLevels.codeCode = EPD.epdLevel
WHERE
	lkpLevels.lvlYear IN (PE.lvlMinYr, PE.lvlMaxYr)
GROUP BY
	-- this final group by is only becuase we have 2 records from lkpLevels
	PE.schNo, PE.ProjectedEnrol, PE.NumLocations

/************************************************************************
** cahced costed data
*************************************************************************/
-- this temp table adds costing
	Select

		schNo
		, roleGrade
		, RG.RoleCode [Role]
		, rgSalaryLevelMin
		, rgSalaryLevelMax
		, TR.secCode	SectorCode
		, Sectors.secSort
		, RG.rgSort RoleGradeSort
		, R.spCode NominalSalaryPoint
		, R.AnnualTotal NominalAnnualSalary
		, R.PointOrder
		, numQuota Quota
		, numPlanned Planned
		, numPositions CurrentPositions
		, numFilled CurrentFilled
		, numFilledSalaryPointComplies currentFilledSalaryPointComplies
		, numFilledUnderpay currentFilledUnderpay
		, numUnfilled CurrentVacant
		, NumRequired
		, FilledPayFN FillCurrentPayFortnight
		, (FilledPayFN / 14) * 365 FillCurrentPayAnnualised
		--, CurrentNominalAnnualPay
		, FuturePay FuturePayRetainingUnderpays
		, numFilledUnderPay * R.AnnualTotal + FuturePaySalaryPointComplies	FuturePayRecostUnderPays
		, numPlanned FinalPositions
		, case when isnull(numPlanned,0) > 0 then 1 else 0 end HasPlannedPositions
		, case when isnull(numPositions,0) > 0 then 1 else 0 end HasCurrentPositions

		, FinalFilled
		, FinalUnfilled
		, FinalNewPositions
		, case when NumRequired > 0 then NumRequired
				else null
			end PositionsGained
		,  case when NumRequired < 0 then NumRequired * -1
				else null
			end PositionsLost
	, FinalUnfilled * R.AnnualTotal FinalUnfilledCost
	,
		case when numFilled = 0 then 0
			else
				case
					when @ProjectUnderPay = 1 then
						FinalFilled *  FuturePay/numFilled
					else
						FinalFilled * (numFilledUnderPay * R.AnnualTotal + FuturePaySalaryPointComplies)/NumFilled
				end
		end FinalFilledCost
	, FinalNewPositions * R.AnnualTotal FinalNewPositionsCost


	INTO #costed


	from
	(
		-- NEST 2 - read temp table #data
		Select *
		-- calculating for lost positions
		-- how many unfilled?
		, isnull(
				case when NumRequired < 0 then
				-- e.g. suppose Budget = 5 , Pos = 7, filled = 4
				-- then numrequired = -2
						case when  (numPositions - numFilled) - (NumRequired * -1) >= 0 then (numPositions - numFilled) - (NumRequired * -1)
						else 0
						end
					 else
						(numPositions - numFilled)
				end
				 , 0) FinalUnfilled


		, isnull(
				case when NumRequired < 0 then
		-- e.g. suppose Budget = 4 , Pos = 7, filled = 5
		-- then numrequired = -3
					case when  numPositions  - (NumRequired * -1) < numFilled then numPositions  - (NumRequired * -1)
					else numFilled
					end
				 else
					numFilled
				end
				, 0)  FinalFilled
		, case when NumRequired < 0 then 0
				else NumRequired
			  end FinalNewPositions

		FROM #data
	) data
	INNER JOIN RoleGRades RG
		ON data.RoleGrade = RG.rgCode
	INNER JOIN lkpTeacherRole TR
		on RG.roleCode = TR.codeCode
	LEFT JOIN EducationSectors Sectors
		ON Sectors.secCode = TR.secCode
	LEFT JOIN #rates RUnadj
		ON RG.rgSalaryPointMedian = RUnadj.spCode
	LEFT JOIN #rates R
		on (RUnadj.PointOrder + @AdjustMedianSalaryPoint) = R.PointOrder

/************************************************************************
** OUTPUT QUERY
*************************************************************************/
-- the final output gets the nominal cost for the unfilled and new positions

-- nest the query one last time to use the definitions of FiledCost, UnfilledCost, NewPositionsCost

-- NEST 0 - gets school and authority

IF @Summary = 0 begin

	Select @estYear EstYear
		, data2.*
		, Schools.schName
		, schType
		, schAuth
		, schEst				-- established year
		, eststCode
		, estAuth
		, authName
		, EnrolStats.ProjectedEnrol
		, EnrolStats.MinLevelCode
		, EnrolStats.MaxLevelCode
		, EnrolStats.NumLocations - 1 NumExtensions
		, case when EnrolStats.NumLocations > 1 then 1 else 0 end HasExtension
		, CurrentEnrol.sumEnrol LastEnrol
		, CurrentEnrol.EnrolYear LastEnrolYear
		, TRRoleGrades.rgDescription
		, TRTeacherRole.codeDescription TeacherRole
		, case when isnull(schType,'')<> isnull( eststCode,'') then 1 else 0 end SchoolTypeChange
		, case when isnull(schAuth,'')<> isnull( estAuth,'') then 1 else 0 end AuthorityChange
		, isnull(FinalunfilledCost,0)
			+ isnull(FinalFilledCost,0)
			+ isnull(FinalNewPositionsCost,0) FinalCost
		, pSchoolRead.fnSchoolPayAllowances(data2.schNo, @EstYear) PayAllowanceList
		, SchoolAllowances.TotalAllowancePayable
		, SchoolTypes.stSort
		, SchoolTypes.stDescription

	-- INTRODUCE ONE LAST STAGING IN ORDER TO Support Summary

	from #costed  data2		-- final nesting to calculate totalcost

	INNER JOIN Schools
		ON data2.schNo = Schools.Schno
	INNER JOIN TRRoleGrades
		ON data2.roleGrade = TRRoleGrades.rgCode
	INNER JOIN TRTeacherRole
		ON TRTeacherRole.codeCode = TRRoleGRades.roleCode
	LEFT JOIN SchoolEstablishment
		ON data2.schNo = SchoolEstablishment.schNo
			AND SchoolEstablishment.estYear = @estYear
	LEFT JOIN Authorities
		ON Authorities.authCode = SchoolEstablishment.estAuth


	LEFT JOIN

		-- this subquery totals the projected enrolment in the enrolment scenario that the Estabioshment Plan uses

		#projectedEnrolStats EnrolStats
		ON data2.schNo = EnrolStats.SchNo

	LEFT JOIN
		(
		-- note the use of SchoolEstablishment EstablishmentPoint to be sure we pick up the right set of extensions

			select SE.estEstablishmentPoint
			, sum(bestEnrol) sumEnrol
			, min(X.bestYear) EnrolYear
			from SchoolEstablishment SE
			INNER JOIN dbo.tfnEstimate_BestSurveyEnrolments() X
				ON SE.schno = X.schNo

			WHERE X.LifeYear = @MaxSurveyYear
			and SE.estYear = @EstYear
			GROUP BY SE.estEstablishmentPoint
		) CurrentEnrol
		ON CurrentEnrol.estEstablishmentPoint = data2.schNo

	LEFT JOIN SchoolTypes
		ON SchoolEstablishment.eststCode = SchoolTypes.stCode
	-- now apply remaining filter criteria - remember that SchoolNo was appalied at the stage of building #data

	--- allowances are added to show the total allowance for the school
	--- DON't TRY to add this up cos its duplicated on every record for the school
	LEFT JOIN pEstablishmentRead.SchoolAllowancesTotal(@EstYear) SchoolAllowances
		ON SchoolAllowances.schNo = data2.schNo
	WHERE
		( isnull(currentPositions,0) > 0 or isnull(FinalPositions,0) > 0)
		AND

		(estAuth = @Authority or @Authority is null)
		AND (eststCode = @SchoolType or @SchoolType is null)

	ORDER BY schNo,  secSort, SectorCode, roleGradeSort, PointOrder DESC, RoleGrade


end

If @Summary = 1 begin

	-- by authority


	Select estAuth
		, authName
		, count(CurrentSchools) CurrentSchools
		, count(NumSchools) NumSchools
		, sum(CurrentPositions) CurrentPositions
		, sum(CurrentFilled) CurrentFilled
		, sum(Quota) Quota
		, sum(FinalPositions) FinalPositions
		, sum(FinalCost) FinalCost
		, sum(TotalAllowancePayable) TotalAllowancePayable

	FROM
		(
		SELECT schNo
		, max(case when CurrentPositions > 0 then schNo else null end)CurrentSchools
		, max(case when FinalPositions > 0 then schNo else null end) NumSchools
		, sum(CurrentPositions) CurrentPositions
		, sum(CurrentFilled) CurrentFilled
		, sum(Quota) Quota
		, sum(FinalPositions) FinalPositions
		, isnull(sum(FinalunfilledCost),0)
			+ isnull(sum(FinalFilledCost),0)
			+ isnull(sum(FinalNewPositionsCost),0) FinalCost
		FROM #costed
		GROUP BY schNo
		) SchoolCost

	LEFT JOIN pEstablishmentRead.SchoolAllowancesTotal(@EstYear) SchoolAllowances
		ON SchoolAllowances.schNo = SchoolCost.schNo
	LEFT JOIN SchoolEstablishment
		ON SchoolCost.schNo = SchoolEstablishment.schNo
			AND SchoolEstablishment.estYear = @estYear
	LEFT JOIN Authorities
		ON Authorities.authCode = SchoolEstablishment.estAuth
	GROUP BY estAuth, authName

end

END
GO

