SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 6 2021
-- Description:	Prepopulate the Classroom in intaractive PDF application
-- creates an xfdf file with field names matching those used in the classrooms
-- section of the interactive pdf.
-- Invoked from the Generator in the SurveyManager.
-- =============================================
CREATE PROCEDURE [pSurveyRead].[xfdfClassroomRead]
	-- Add the parameters for the stored procedure here
		@schNo nvarchar(50),
		@svyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @ssID int


declare @srcYear int

Select TOP 1 @ssID = SS.ssId
, @srcYear = svyYear
from Rooms R
	INNER JOIN SchoolSurvey SS
	ON R.ssID = SS.ssID
WHERE SS.schNo = @schNo
AND (ss.svyYear = @svyYear or @svyYear is null)
ORDER BY svyYear DESC

declare @StartNo int

Select @startNo = min(rmNo)
from Rooms
WHERE ssID = @ssID

-- classroom fields in Survey PDF have this hierarchy
-- Room.Class.D.<nn>.<item>
-- e.g. Room.Class.D.01.Mat => material code for classroom 1
-- furnitre adds alternate levels
-- e.g. Room.Class.F.<row>.<nn>		(bit inconsistent unfortunately)


declare @u TABLE (
Tag int,
Parent int,
[field!1!name] nvarchar(50), --Room
[field!2!name] nvarchar(50), --Class
[field!3!name] nvarchar(50),		-- D, F, FS, FT
[field!4!name] nvarchar(50),   -- id e.g. 00
[field!5!name] nvarchar(50),	   -- field name , Level Size Mat
[field!5!value!element] nvarchar(50),	   -- value , Level Size Mat
-- furniture
[field!6!name] nvarchar(50),		-- D for Furniture node
[field!7!name] nvarchar(50),		-- counter d=for furniure row
[field!7!fcode] nvarchar(50),		-- furniture code used for the row - for debugging
[field!8!name] nvarchar(50),		-- room number again
[field!8!value!element] int	   -- furniture quantity

)

INSERT INTO @u
(Tag, Parent, [field!1!name])
VALUES(1, null, 'Room')

INSERT INTO @u
(Tag, Parent, [field!1!name], [field!2!name])
VALUES(2,1, 'Room', 'Class')


INSERT INTO @u
(Tag, Parent, [field!1!name], [field!2!name], [field!3!name])
VALUES(3,2,'Room', 'Class', 'D'),
	(3,2,'Room', 'Class', 'F'),
	(3,2,'Room', 'Class', 'FT'),
	(3,2,'Room', 'Class', 'FS')

INSERT INTO @u
(Tag, Parent, [field!1!name], [field!2!name], [field!3!name], [field!4!name])
Select 4, 3, 'Room', 'Class', 'D', format(rmNo - @startNo,'00')
from Rooms
WHERE ssId = @ssID


INSERT INTO @u
(Tag, Parent, [field!1!name], [field!2!name], [field!3!name],[field!4!name],[field!5!name], [field!5!value!element])
Select * from
(
Select 5 Tag, 4 Parent, 'Room' F1, 'Class' F2, 'D' F3, format(rmNo - @startNo,'00') F4,
case num
	when 0 then 'Level'		-- class level taught
	when 1 then 'Size'		-- room size
	when 2 then 'Mat'		-- material code
	when 3 then 'C'			-- condition
	when 4 then 'Door'		-- secure door
	when 5 then 'Window'	-- secure door
	when 6 then 'WChair'	-- wheelchair accessible
	when 7 then 'Year'		-- year of construction
	when 8 then 'Title'		-- name used for room
	when 9 then 'Length'	-- in metres
	when 10 then 'Width'	-- in metres
end Item,
case num
	when 0 then rmLevel			-- class level taught
	when 1 then convert(nvarchar(50), rmSize)			-- room size
	when 2 then rmMaterials		-- material code
	when 3 then rmCondition		-- condition
	when 4 then case rmSecureDoor	when 1 then 'Y' when -1 then 'Y' when 0 then 'N' else null end  -- secure door
	when 5 then case rmSecureWindows	when 1 then 'Y'  when -1 then 'Y' when 0 then 'N' else null end			-- secure door
	when 6 then case rmWheelchair	when 1 then 'Y'  when -1 then 'Y' when 0 then 'N' else null end			-- wheelchair accessible
	when 7 then convert(nvarchar(50), rmYear)				-- year of construction
	when 8 then rmTitle
	when 9 then convert(nvarchar(50), rmLength)
	when 10 then convert(nvarchar(50), rmWidth)
end Value

from Rooms
, metaNumbers
WHERE ssId = @ssID
and num between 0 and 10
) SUB
Where value is not null

-- furniture
-- the quantity of a furniture item in the room is placed in field
-- e.g. Room.Class.F.D.01.04
-- where F is the furnture group (either F FT or FS furniture, teacher furniture, storage)
-- 01 is the furniture row, and 04 is the room no
-- the mapping from a given furniture group-row to a furniture code is made by the value of the
-- hidden field Room.Class.F.01.K

-- for now, we duplicate this mapping here hardcoded, maybe it could mve to lkpFurnitureTypes
-- you can find however find this by looking at any xfdf export from the form,
-- of from a Wondershare PDFelement export to csv

declare @fm table
(
fg nvarchar(2),			-- the group on the form
row nvarchar(2),		-- the row idenifier looks like 00, 01, 02 etc
fcode nvarchar(20)		-- the furniture code
)
-- here's the hardcoded mapping
INSERT INTO @fm
VALUES ('F','00','DESK1'),
('F','01','DESK2'),
('F','02','LOWD'),
('F','03','LOWT'),
('F','04','LOWC'),
('F','05','CHAIR'),
('F','06','DESK'),
('F','07','BENCH4'),
('F','08','BENCH6'),
('FT','00','TEACHT'),
('FT','01','TEACHC'),
('FT','02','BLACKBRD'),
('FT','03','WHITEBRD'),
('FS','00','LOCKERS'),
('FS','01','CUPBOARD'),
('FS','02','BOOKCASE'),
('FS','03','FILINGCAB')

-- add a record for furniture type /Data 'D'
INSERT INTO @u
(Tag, Parent, [field!1!name], [field!2!name], [field!3!name], [field!6!name])
Select DISTINCT 6, 3, 'Room', 'Class', fg, 'D'
from @fm

-- add a record for furniture rows
INSERT INTO @u
(Tag, Parent, [field!1!name], [field!2!name], [field!3!name], [field!6!name], [field!7!name], [field!7!fCode])
Select DISTINCT 7, 6, 'Room', 'Class', fg, 'D', [row],fCode
from @fm

-- now the data
-- add a record for each room
INSERT INTO @u
(Tag, Parent, [field!1!name], [field!2!name], [field!3!name], [field!6!name], [field!7!name], [field!8!name], [field!8!value!element])
Select DISTINCT 8, 7, 'Room', 'Class', fg, 'D', [row], format(rmNo - @startNo,'00'), fNum
from @fm FM
INNER JOIN Furniture F
	ON fm.fcode = F.fCode
INNER JOIN Rooms R
	ON R.rmID = F.rmID
WHERE R.ssId = @ssID

-- finally create the xml
declare @xml xml
select @xml =
(
Select *
, @srcYear [field!2!sourceYear]

from @u u
ORDER BY

[field!1!name], [field!2!name], [field!3!name],
[field!4!name],
[field!5!name],
[field!6!name],
[field!7!name],
[field!8!name]
FOR XML EXPLICIT
)

-- wrap the <field> node in <fields>
Select @xml =
(
Select @xml
FOR XML PATH('fields')
)

-- create the <xfdf> form fields node (applies the fxdf namespace)
Select @xml = common.xfdf(@xml)

Select @xml xfdf

END
GO

