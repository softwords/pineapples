SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 03 2018
-- Description:	Filter of student
--              TODO - Add ability to filter by currently enrolled school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[StudentFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@StudentID uniqueidentifier = null,
	@StudentCardID nvarchar(20) = null,
    @StudentGiven nvarchar(50) = null,
    @StudentFamilyName  nvarchar(50) = null,
    @StudentDoB  date = null,
	@StudentGender nvarchar(1) = null,
	@StudentEthnicity nvarchar(200) = null,

	-- selection based on enrolment
	@EnrolledAt nvarchar(50) = null,
	@EnrolYear int = null,
	@EnrolLevel nvarchar(10) = null,
	@EnrolDistrict nvarchar(10) = null,
	@EnrolAuthority nvarchar(10) = null
AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id uniqueidentifier PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int


	INSERT INTO @keys
	EXEC pSchoolRead.StudentFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

		, @StudentID
	    , @StudentCardID
        , @StudentGiven
        , @StudentFamilyName
        , @StudentDoB
		, @StudentGender
		, @StudentEthnicity
		, @EnrolledAt
		, @EnrolYear
		, @EnrolLevel
		, @EnrolDistrict
		, @EnrolAuthority

--- column sets ----
if @columnset = 0 begin
	SELECT S.stuID
	, S.stuCardID
	, S.stuGiven
	, S.stuFamilyName
	, S.stuDoB
	, S.stuGender
	, S.stuEthnicity
	FROM Student_ S
		INNER JOIN @keys K
			ON S.stuID = K.ID
	ORDER BY recNo
end


if (@ColumnSet = 1) begin
	declare @T TABLE
	(
	stuID uniqueidentifier
	, dupID uniqueidentifier
	, RecNo int
	, stuMaxYear int
	, stuMinYear int
	, dupMaxYear int			-- dups not used, for consistency with deduper
	, dupMinYear int
	, stuMaxSchNo nvarchar(50)  -- school on last enrolment record
	, stuMinSchNo nvarchar(50)  -- school on first enrolment record
	, dupMaxSchNo nvarchar(50)
	, dupMinSchNo nvarchar(50)
	, stuMaxClassLevel nvarchar(50)  -- class level on last enrolment record
	, stuMinClassLevel nvarchar(50)  -- class level on first enrolment record
	, dupMaxClassLevel nvarchar(50)
	, dupMinClassLevel nvarchar(50)

	, countConflictYears int
	)

	INSERT INTO @t
	(stuID, recNo)
	Select K.ID, K.recNo
	FROM @Keys K

	UPDATE @t
		SET stuMaxYear = MaxYr
		, stuMinYear = MinYr
		FROM @t
		INNER JOIN
		(
		Select stUID , max(stueYear) maxyr, min(stueYear) minYr
		From StudentEnrolment_ SE
		GROUP BY stuID
		) SS
		ON [@t].stuID = SS.stuID

	-- get the first and last school for stu

	UPDATE @t
		SET stuMinSchNo = schNo
		, stuMinClassLevel = stueClass

		FROM @t
		INNER JOIN
		StudentEnrolment_ SE
		ON [@t].stuID = SE.stuID
		AND SE.stueYear = stuMinYear
	UPDATE @t
		SET stuMaxSchNo = schNo
		, stuMaxClassLevel = stueClass
		FROM @t
		INNER JOIN
		StudentEnrolment_ SE
		ON [@t].stuID = SE.stuID
		AND SE.stueYear = stuMaxYear

		-- look for conflict years in enrolment histories
	UPDATE @t
		set CountConflictYears = NumDups
		FROM
		@t
		INNER JOIN
		(
		Select recNo, count(stueYear) NumDups
		FROM
		(
		Select recNo, stueYear, count(*) NumStudents
		FROM StudentEnrolment_  SE
		INNER JOIN @t T
		ON SE.stuID in (T.stuID, T.dupID)
		group by recNo, stueYear
		) SUB
		WHERE numStudents > 1
		group by recNo
		) SUB2
		ON [@t].recNo = SUB2.recNo


		Select S.stuID
		, S.stuNamePrefix
		, S.stuGiven
		, S.stuMiddleNames
		, S.stuFamilyName
		, S.stuNameSuffix
		, S.stuCardID
		, S.stuDoB
		, S.stuGender

		, T.stuMinYear
		, T.stuMaxYear
		, T.stuMinSchNo
		, T.stuMaxSchNo
		, T.stuMinClassLevel
		, T.stuMaxClassLevel
		, T.countConflictYears
		FROM Student_ S
			INNER JOIN @T AS T
				ON S.stuID = T.stuID
		ORDER BY T.RecNo

end


--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

