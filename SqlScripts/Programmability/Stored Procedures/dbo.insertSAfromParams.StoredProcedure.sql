SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 28 05 2021
-- Description:	A procedure to bulk insert/update
-- school accreditation data from parameters.
-- This was necessary to load some past data that
-- was of the new format but that either was not
-- correctly entered in the Education Survey Tool
-- tablet app or was somehow lost but is available in
-- excel. It's also used to update based on their
-- official excel based data which unfortunately is
-- not 100% inline with what they had on tablets.
--
-- History Log:
--   * 12/12/2022, Ghislain Hachey, support to retrieve survey year according to new school year parameter rule (and not just calendar year)
-- =============================================

CREATE Procedure [dbo].[insertSAfromParams]

@schNo nvarchar(50),
@date datetime,

@SE11a int,
@SE11b int,
@SE11c int,
@SE11d int,

@SE12a int,
@SE12b int,
@SE12c int,
@SE12d int,

@SE13a int,
@SE13b int,
@SE13c int,
@SE13d int,

@SE14a int,
@SE14b int,
@SE14c int,
@SE14d int,

@SE21a int,
@SE21b int,
@SE21c int,
@SE21d int,

@SE22a int,
@SE22b int,
@SE22c int,
@SE22d int,

@SE23a int,
@SE23b int,
@SE23c int,
@SE23d int,

@SE24a int,
@SE24b int,
@SE24c int,
@SE24d int,

@SE31a int,
@SE31b int,
@SE31c int,
@SE31d int,

@SE32a int,
@SE32b int,
@SE32c int,
@SE32d int,

@SE33a int,
@SE33b int,
@SE33c int,
@SE33d int,

@SE34a int,
@SE34b int,
@SE34c int,
@SE34d int,

@SE41a int,
@SE41b int,
@SE41c int,
@SE41d int,

@SE42a int,
@SE42b int,
@SE42c int,
@SE42d int,

@SE43a int,
@SE43b int,
@SE43c int,
@SE43d int,

@SE44a int,
@SE44b int,
@SE44c int,
@SE44d int,

@SE51a int,
@SE51b int,
@SE51c int,
@SE51d int,

@SE52a int,
@SE52b int,
@SE52c int,
@SE52d int,

@SE53a int,
@SE53b int,
@SE53c int,
@SE53d int,

@SE54a int,
@SE54b int,
@SE54c int,
@SE54d int,

@SE61a int,
@SE61b int,
@SE61c int,
@SE61d int,

@SE62a int,
@SE62b int,
@SE62c int,
@SE62d int,

@SE63a int,
@SE63b int,
@SE63c int,
@SE63d int,

@SE64a int,
@SE64b int,
@SE64c int,
@SE64d int,

@CO11a int,
@CO11b int,
@CO11c int,
@CO11d int,
@CO11e int,

@CO12a int,
@CO12b int,
@CO12c int,
@CO12d int,
@CO12e int,

@CO21a int,
@CO21b int,
@CO21c int,
@CO21d int,
@CO21e int,

@CO22a int,
@CO22b int,
@CO22c int,
@CO22d int,
@CO22e int,

@CO23a int,
@CO23b int,
@CO23c int,
@CO23d int,
@CO23e int,

@CO24a int,
@CO24b int,
@CO24c int,
@CO24d int,
@CO24e int,

@CO31a int,
@CO31b int,
@CO31c int,
@CO31d int,
@CO31e int,

@CO32a int,
@CO32b int,
@CO32c int,
@CO32d int,
@CO32e int,

@CO33a int,
@CO33b int,
@CO33c int,
@CO33d int,
@CO33e int,

@CO34a int,
@CO34b int,
@CO34c int,
@CO34d int,
@CO34e int,

@CO41a int,
@CO41b int,
@CO41c int,
@CO41d int,
@CO41e int,

@CO51a int,
@CO51b int,
@CO51c int,
@CO51d int,
@CO51e int,
@note nvarchar(100) = null,

@StartDate datetime = null,
@EndDate datetime = null,
@Principal nvarchar(100) = null,
@InspectedBy nvarchar(200) = 'N/A',
@user nvarchar(200) = 'system@pacific-emis.org',
@country nvarchar(10) = 'FSM'

AS

BEGIN

DECLARE @SE11aVal nvarchar(10);
DECLARE @SE11bVal nvarchar(10);
DECLARE @SE11cVal nvarchar(10);
DECLARE @SE11dVal nvarchar(10);

DECLARE @SE12aVal nvarchar(10);
DECLARE @SE12bVal nvarchar(10);
DECLARE @SE12cVal nvarchar(10);
DECLARE @SE12dVal nvarchar(10);

DECLARE @SE13aVal nvarchar(10);
DECLARE @SE13bVal nvarchar(10);
DECLARE @SE13cVal nvarchar(10);
DECLARE @SE13dVal nvarchar(10);

DECLARE @SE14aVal nvarchar(10);
DECLARE @SE14bVal nvarchar(10);
DECLARE @SE14cVal nvarchar(10);
DECLARE @SE14dVal nvarchar(10);

DECLARE @SE21aVal nvarchar(10);
DECLARE @SE21bVal nvarchar(10);
DECLARE @SE21cVal nvarchar(10);
DECLARE @SE21dVal nvarchar(10);

DECLARE @SE22aVal nvarchar(10);
DECLARE @SE22bVal nvarchar(10);
DECLARE @SE22cVal nvarchar(10);
DECLARE @SE22dVal nvarchar(10);

DECLARE @SE23aVal nvarchar(10);
DECLARE @SE23bVal nvarchar(10);
DECLARE @SE23cVal nvarchar(10);
DECLARE @SE23dVal nvarchar(10);

DECLARE @SE24aVal nvarchar(10);
DECLARE @SE24bVal nvarchar(10);
DECLARE @SE24cVal nvarchar(10);
DECLARE @SE24dVal nvarchar(10);

DECLARE @SE31aVal nvarchar(10);
DECLARE @SE31bVal nvarchar(10);
DECLARE @SE31cVal nvarchar(10);
DECLARE @SE31dVal nvarchar(10);

DECLARE @SE32aVal nvarchar(10);
DECLARE @SE32bVal nvarchar(10);
DECLARE @SE32cVal nvarchar(10);
DECLARE @SE32dVal nvarchar(10);

DECLARE @SE33aVal nvarchar(10);
DECLARE @SE33bVal nvarchar(10);
DECLARE @SE33cVal nvarchar(10);
DECLARE @SE33dVal nvarchar(10);

DECLARE @SE34aVal nvarchar(10);
DECLARE @SE34bVal nvarchar(10);
DECLARE @SE34cVal nvarchar(10);
DECLARE @SE34dVal nvarchar(10);

DECLARE @SE41aVal nvarchar(10);
DECLARE @SE41bVal nvarchar(10);
DECLARE @SE41cVal nvarchar(10);
DECLARE @SE41dVal nvarchar(10);

DECLARE @SE42aVal nvarchar(10);
DECLARE @SE42bVal nvarchar(10);
DECLARE @SE42cVal nvarchar(10);
DECLARE @SE42dVal nvarchar(10);

DECLARE @SE43aVal nvarchar(10);
DECLARE @SE43bVal nvarchar(10);
DECLARE @SE43cVal nvarchar(10);
DECLARE @SE43dVal nvarchar(10);

DECLARE @SE44aVal nvarchar(10);
DECLARE @SE44bVal nvarchar(10);
DECLARE @SE44cVal nvarchar(10);
DECLARE @SE44dVal nvarchar(10);

DECLARE @SE51aVal nvarchar(10);
DECLARE @SE51bVal nvarchar(10);
DECLARE @SE51cVal nvarchar(10);
DECLARE @SE51dVal nvarchar(10);

DECLARE @SE52aVal nvarchar(10);
DECLARE @SE52bVal nvarchar(10);
DECLARE @SE52cVal nvarchar(10);
DECLARE @SE52dVal nvarchar(10);

DECLARE @SE53aVal nvarchar(10);
DECLARE @SE53bVal nvarchar(10);
DECLARE @SE53cVal nvarchar(10);
DECLARE @SE53dVal nvarchar(10);

DECLARE @SE54aVal nvarchar(10);
DECLARE @SE54bVal nvarchar(10);
DECLARE @SE54cVal nvarchar(10);
DECLARE @SE54dVal nvarchar(10);

DECLARE @SE61aVal nvarchar(10);
DECLARE @SE61bVal nvarchar(10);
DECLARE @SE61cVal nvarchar(10);
DECLARE @SE61dVal nvarchar(10);

DECLARE @SE62aVal nvarchar(10);
DECLARE @SE62bVal nvarchar(10);
DECLARE @SE62cVal nvarchar(10);
DECLARE @SE62dVal nvarchar(10);

DECLARE @SE63aVal nvarchar(10);
DECLARE @SE63bVal nvarchar(10);
DECLARE @SE63cVal nvarchar(10);
DECLARE @SE63dVal nvarchar(10);

DECLARE @SE64aVal nvarchar(10);
DECLARE @SE64bVal nvarchar(10);
DECLARE @SE64cVal nvarchar(10);
DECLARE @SE64dVal nvarchar(10);

DECLARE @CO11aVal nvarchar(10);
DECLARE @CO11bVal nvarchar(10);
DECLARE @CO11cVal nvarchar(10);
DECLARE @CO11dVal nvarchar(10);
DECLARE @CO11eVal nvarchar(10);

DECLARE @CO12aVal nvarchar(10);
DECLARE @CO12bVal nvarchar(10);
DECLARE @CO12cVal nvarchar(10);
DECLARE @CO12dVal nvarchar(10);
DECLARE @CO12eVal nvarchar(10);

DECLARE @CO21aVal nvarchar(10);
DECLARE @CO21bVal nvarchar(10);
DECLARE @CO21cVal nvarchar(10);
DECLARE @CO21dVal nvarchar(10);
DECLARE @CO21eVal nvarchar(10);

DECLARE @CO22aVal nvarchar(10);
DECLARE @CO22bVal nvarchar(10);
DECLARE @CO22cVal nvarchar(10);
DECLARE @CO22dVal nvarchar(10);
DECLARE @CO22eVal nvarchar(10);

DECLARE @CO23aVal nvarchar(10);
DECLARE @CO23bVal nvarchar(10);
DECLARE @CO23cVal nvarchar(10);
DECLARE @CO23dVal nvarchar(10);
DECLARE @CO23eVal nvarchar(10);

DECLARE @CO24aVal nvarchar(10);
DECLARE @CO24bVal nvarchar(10);
DECLARE @CO24cVal nvarchar(10);
DECLARE @CO24dVal nvarchar(10);
DECLARE @CO24eVal nvarchar(10);

DECLARE @CO31aVal nvarchar(10);
DECLARE @CO31bVal nvarchar(10);
DECLARE @CO31cVal nvarchar(10);
DECLARE @CO31dVal nvarchar(10);
DECLARE @CO31eVal nvarchar(10);

DECLARE @CO32aVal nvarchar(10);
DECLARE @CO32bVal nvarchar(10);
DECLARE @CO32cVal nvarchar(10);
DECLARE @CO32dVal nvarchar(10);
DECLARE @CO32eVal nvarchar(10);

DECLARE @CO33aVal nvarchar(10);
DECLARE @CO33bVal nvarchar(10);
DECLARE @CO33cVal nvarchar(10);
DECLARE @CO33dVal nvarchar(10);
DECLARE @CO33eVal nvarchar(10);

DECLARE @CO34aVal nvarchar(10);
DECLARE @CO34bVal nvarchar(10);
DECLARE @CO34cVal nvarchar(10);
DECLARE @CO34dVal nvarchar(10);
DECLARE @CO34eVal nvarchar(10);

DECLARE @CO41aVal nvarchar(10);
DECLARE @CO41bVal nvarchar(10);
DECLARE @CO41cVal nvarchar(10);
DECLARE @CO41dVal nvarchar(10);
DECLARE @CO41eVal nvarchar(10);

DECLARE @CO51aVal nvarchar(10);
DECLARE @CO51bVal nvarchar(10);
DECLARE @CO51cVal nvarchar(10);
DECLARE @CO51dVal nvarchar(10);
DECLARE @CO51eVal nvarchar(10);

IF @SE11a = 0
BEGIN
	SELECT @SE11aVal = 'NEGATIVE'
END
ELSE IF @SE11a = 1
BEGIN
	SELECT @SE11aVal = 'POSITIVE'
END
IF @SE11b = 0
BEGIN
	SELECT @SE11bVal = 'NEGATIVE'
END
ELSE IF @SE11b = 1
BEGIN
	SELECT @SE11bVal = 'POSITIVE'
END
IF @SE11c = 0
BEGIN
	SELECT @SE11cVal = 'NEGATIVE'
END
ELSE IF @SE11c = 1
BEGIN
	SELECT @SE11cVal = 'POSITIVE'
END
IF @SE11d = 0
BEGIN
	SELECT @SE11dVal = 'NEGATIVE'
END
ELSE IF @SE11d = 1
BEGIN
	SELECT @SE11dVal = 'POSITIVE'
END

IF @SE12a = 0
BEGIN
	SELECT @SE12aVal = 'NEGATIVE'
END
ELSE IF @SE12a = 1
BEGIN
	SELECT @SE12aVal = 'POSITIVE'
END
IF @SE12b = 0
BEGIN
	SELECT @SE12bVal = 'NEGATIVE'
END
ELSE IF @SE12b = 1
BEGIN
	SELECT @SE12bVal = 'POSITIVE'
END
IF @SE12c = 0
BEGIN
	SELECT @SE12cVal = 'NEGATIVE'
END
ELSE IF @SE12c = 1
BEGIN
	SELECT @SE12cVal = 'POSITIVE'
END
IF @SE12d = 0
BEGIN
	SELECT @SE12dVal = 'NEGATIVE'
END
ELSE IF @SE12d = 1
BEGIN
	SELECT @SE12dVal = 'POSITIVE'
END

IF @SE13a = 0
BEGIN
	SELECT @SE13aVal = 'NEGATIVE'
END
ELSE IF @SE13a = 1
BEGIN
	SELECT @SE13aVal = 'POSITIVE'
END
IF @SE13b = 0
BEGIN
	SELECT @SE13bVal = 'NEGATIVE'
END
ELSE IF @SE13b = 1
BEGIN
	SELECT @SE13bVal = 'POSITIVE'
END
IF @SE13c = 0
BEGIN
	SELECT @SE13cVal = 'NEGATIVE'
END
ELSE IF @SE13c = 1
BEGIN
	SELECT @SE13cVal = 'POSITIVE'
END
IF @SE13d = 0
BEGIN
	SELECT @SE13dVal = 'NEGATIVE'
END
ELSE IF @SE13d = 1
BEGIN
	SELECT @SE13dVal = 'POSITIVE'
END

IF @SE14a = 0
BEGIN
	SELECT @SE14aVal = 'NEGATIVE'
END
ELSE IF @SE14a = 1
BEGIN
	SELECT @SE14aVal = 'POSITIVE'
END
IF @SE14b = 0
BEGIN
	SELECT @SE14bVal = 'NEGATIVE'
END
ELSE IF @SE14b = 1
BEGIN
	SELECT @SE14bVal = 'POSITIVE'
END
IF @SE14c = 0
BEGIN
	SELECT @SE14cVal = 'NEGATIVE'
END
ELSE IF @SE14c = 1
BEGIN
	SELECT @SE14cVal = 'POSITIVE'
END
IF @SE14d = 0
BEGIN
	SELECT @SE14dVal = 'NEGATIVE'
END
ELSE IF @SE14d = 1
BEGIN
	SELECT @SE14dVal = 'POSITIVE'
END

IF @SE21a = 0
BEGIN
	SELECT @SE21aVal = 'NEGATIVE'
END
ELSE IF @SE21a = 1
BEGIN
	SELECT @SE21aVal = 'POSITIVE'
END
IF @SE21b = 0
BEGIN
	SELECT @SE21bVal = 'NEGATIVE'
END
ELSE IF @SE21b = 1
BEGIN
	SELECT @SE21bVal = 'POSITIVE'
END
IF @SE21c = 0
BEGIN
	SELECT @SE21cVal = 'NEGATIVE'
END
ELSE IF @SE21c = 1
BEGIN
	SELECT @SE21cVal = 'POSITIVE'
END
IF @SE21d = 0
BEGIN
	SELECT @SE21dVal = 'NEGATIVE'
END
ELSE IF @SE21d = 1
BEGIN
	SELECT @SE21dVal = 'POSITIVE'
END

IF @SE22a = 0
BEGIN
	SELECT @SE22aVal = 'NEGATIVE'
END
ELSE IF @SE22a = 1
BEGIN
	SELECT @SE22aVal = 'POSITIVE'
END
IF @SE22b = 0
BEGIN
	SELECT @SE22bVal = 'NEGATIVE'
END
ELSE IF @SE22b = 1
BEGIN
	SELECT @SE22bVal = 'POSITIVE'
END
IF @SE22c = 0
BEGIN
	SELECT @SE22cVal = 'NEGATIVE'
END
ELSE IF @SE22c = 1
BEGIN
	SELECT @SE22cVal = 'POSITIVE'
END
IF @SE22d = 0
BEGIN
	SELECT @SE22dVal = 'NEGATIVE'
END
ELSE IF @SE22d = 1
BEGIN
	SELECT @SE22dVal = 'POSITIVE'
END

IF @SE23a = 0
BEGIN
	SELECT @SE23aVal = 'NEGATIVE'
END
ELSE IF @SE23a = 1
BEGIN
	SELECT @SE23aVal = 'POSITIVE'
END
IF @SE23b = 0
BEGIN
	SELECT @SE23bVal = 'NEGATIVE'
END
ELSE IF @SE23b = 1
BEGIN
	SELECT @SE23bVal = 'POSITIVE'
END
IF @SE23c = 0
BEGIN
	SELECT @SE23cVal = 'NEGATIVE'
END
ELSE IF @SE23c = 1
BEGIN
	SELECT @SE23cVal = 'POSITIVE'
END
IF @SE23d = 0
BEGIN
	SELECT @SE23dVal = 'NEGATIVE'
END
ELSE IF @SE23d = 1
BEGIN
	SELECT @SE23dVal = 'POSITIVE'
END

IF @SE24a = 0
BEGIN
	SELECT @SE24aVal = 'NEGATIVE'
END
ELSE IF @SE24a = 1
BEGIN
	SELECT @SE24aVal = 'POSITIVE'
END
IF @SE24b = 0
BEGIN
	SELECT @SE24bVal = 'NEGATIVE'
END
ELSE IF @SE24b = 1
BEGIN
	SELECT @SE24bVal = 'POSITIVE'
END
IF @SE24c = 0
BEGIN
	SELECT @SE24cVal = 'NEGATIVE'
END
ELSE IF @SE24c = 1
BEGIN
	SELECT @SE24cVal = 'POSITIVE'
END
IF @SE24d = 0
BEGIN
	SELECT @SE24dVal = 'NEGATIVE'
END
ELSE IF @SE24d = 1
BEGIN
	SELECT @SE24dVal = 'POSITIVE'
END

IF @SE31a = 0
BEGIN
	SELECT @SE31aVal = 'NEGATIVE'
END
ELSE IF @SE31a = 1
BEGIN
	SELECT @SE31aVal = 'POSITIVE'
END
IF @SE31b = 0
BEGIN
	SELECT @SE31bVal = 'NEGATIVE'
END
ELSE IF @SE31b = 1
BEGIN
	SELECT @SE31bVal = 'POSITIVE'
END
IF @SE31c = 0
BEGIN
	SELECT @SE31cVal = 'NEGATIVE'
END
ELSE IF @SE31c = 1
BEGIN
	SELECT @SE31cVal = 'POSITIVE'
END
IF @SE31d = 0
BEGIN
	SELECT @SE31dVal = 'NEGATIVE'
END
ELSE IF @SE31d = 1
BEGIN
	SELECT @SE31dVal = 'POSITIVE'
END

IF @SE32a = 0
BEGIN
	SELECT @SE32aVal = 'NEGATIVE'
END
ELSE IF @SE32a = 1
BEGIN
	SELECT @SE32aVal = 'POSITIVE'
END
IF @SE32b = 0
BEGIN
	SELECT @SE32bVal = 'NEGATIVE'
END
ELSE IF @SE32b = 1
BEGIN
	SELECT @SE32bVal = 'POSITIVE'
END
IF @SE32c = 0
BEGIN
	SELECT @SE32cVal = 'NEGATIVE'
END
ELSE IF @SE32c = 1
BEGIN
	SELECT @SE32cVal = 'POSITIVE'
END
IF @SE32d = 0
BEGIN
	SELECT @SE32dVal = 'NEGATIVE'
END
ELSE IF @SE32d = 1
BEGIN
	SELECT @SE32dVal = 'POSITIVE'
END

IF @SE33a = 0
BEGIN
	SELECT @SE33aVal = 'NEGATIVE'
END
ELSE IF @SE33a = 1
BEGIN
	SELECT @SE33aVal = 'POSITIVE'
END
IF @SE33b = 0
BEGIN
	SELECT @SE33bVal = 'NEGATIVE'
END
ELSE IF @SE33b = 1
BEGIN
	SELECT @SE33bVal = 'POSITIVE'
END
IF @SE33c = 0
BEGIN
	SELECT @SE33cVal = 'NEGATIVE'
END
ELSE IF @SE33c = 1
BEGIN
	SELECT @SE33cVal = 'POSITIVE'
END
IF @SE33d = 0
BEGIN
	SELECT @SE33dVal = 'NEGATIVE'
END
ELSE IF @SE33d = 1
BEGIN
	SELECT @SE33dVal = 'POSITIVE'
END

IF @SE34a = 0
BEGIN
	SELECT @SE34aVal = 'NEGATIVE'
END
ELSE IF @SE34a = 1
BEGIN
	SELECT @SE34aVal = 'POSITIVE'
END
IF @SE34b = 0
BEGIN
	SELECT @SE34bVal = 'NEGATIVE'
END
ELSE IF @SE34b = 1
BEGIN
	SELECT @SE34bVal = 'POSITIVE'
END
IF @SE34c = 0
BEGIN
	SELECT @SE34cVal = 'NEGATIVE'
END
ELSE IF @SE34c = 1
BEGIN
	SELECT @SE34cVal = 'POSITIVE'
END
IF @SE34d = 0
BEGIN
	SELECT @SE34dVal = 'NEGATIVE'
END
ELSE IF @SE34d = 1
BEGIN
	SELECT @SE34dVal = 'POSITIVE'
END

IF @SE41a = 0
BEGIN
	SELECT @SE41aVal = 'NEGATIVE'
END
ELSE IF @SE41a = 1
BEGIN
	SELECT @SE41aVal = 'POSITIVE'
END
IF @SE41b = 0
BEGIN
	SELECT @SE41bVal = 'NEGATIVE'
END
ELSE IF @SE41b = 1
BEGIN
	SELECT @SE41bVal = 'POSITIVE'
END
IF @SE41c = 0
BEGIN
	SELECT @SE41cVal = 'NEGATIVE'
END
ELSE IF @SE41c = 1
BEGIN
	SELECT @SE41cVal = 'POSITIVE'
END
IF @SE41d = 0
BEGIN
	SELECT @SE41dVal = 'NEGATIVE'
END
ELSE IF @SE41d = 1
BEGIN
	SELECT @SE41dVal = 'POSITIVE'
END

IF @SE42a = 0
BEGIN
	SELECT @SE42aVal = 'NEGATIVE'
END
ELSE IF @SE42a = 1
BEGIN
	SELECT @SE42aVal = 'POSITIVE'
END
IF @SE42b = 0
BEGIN
	SELECT @SE42bVal = 'NEGATIVE'
END
ELSE IF @SE42b = 1
BEGIN
	SELECT @SE42bVal = 'POSITIVE'
END
IF @SE42c = 0
BEGIN
	SELECT @SE42cVal = 'NEGATIVE'
END
ELSE IF @SE42c = 1
BEGIN
	SELECT @SE42cVal = 'POSITIVE'
END
IF @SE42d = 0
BEGIN
	SELECT @SE42dVal = 'NEGATIVE'
END
ELSE IF @SE42d = 1
BEGIN
	SELECT @SE42dVal = 'POSITIVE'
END

IF @SE43a = 0
BEGIN
	SELECT @SE43aVal = 'NEGATIVE'
END
ELSE IF @SE43a = 1
BEGIN
	SELECT @SE43aVal = 'POSITIVE'
END
IF @SE43b = 0
BEGIN
	SELECT @SE43bVal = 'NEGATIVE'
END
ELSE IF @SE43b = 1
BEGIN
	SELECT @SE43bVal = 'POSITIVE'
END
IF @SE43c = 0
BEGIN
	SELECT @SE43cVal = 'NEGATIVE'
END
ELSE IF @SE43c = 1
BEGIN
	SELECT @SE43cVal = 'POSITIVE'
END
IF @SE43d = 0
BEGIN
	SELECT @SE43dVal = 'NEGATIVE'
END
ELSE IF @SE43d = 1
BEGIN
	SELECT @SE43dVal = 'POSITIVE'
END

IF @SE44a = 0
BEGIN
	SELECT @SE44aVal = 'NEGATIVE'
END
ELSE IF @SE44a = 1
BEGIN
	SELECT @SE44aVal = 'POSITIVE'
END
IF @SE44b = 0
BEGIN
	SELECT @SE44bVal = 'NEGATIVE'
END
ELSE IF @SE44b = 1
BEGIN
	SELECT @SE44bVal = 'POSITIVE'
END
IF @SE44c = 0
BEGIN
	SELECT @SE44cVal = 'NEGATIVE'
END
ELSE IF @SE44c = 1
BEGIN
	SELECT @SE44cVal = 'POSITIVE'
END
IF @SE44d = 0
BEGIN
	SELECT @SE44dVal = 'NEGATIVE'
END
ELSE IF @SE44d = 1
BEGIN
	SELECT @SE44dVal = 'POSITIVE'
END

IF @SE51a = 0
BEGIN
	SELECT @SE51aVal = 'NEGATIVE'
END
ELSE IF @SE51a = 1
BEGIN
	SELECT @SE51aVal = 'POSITIVE'
END
IF @SE51b = 0
BEGIN
	SELECT @SE51bVal = 'NEGATIVE'
END
ELSE IF @SE51b = 1
BEGIN
	SELECT @SE51bVal = 'POSITIVE'
END
IF @SE51c = 0
BEGIN
	SELECT @SE51cVal = 'NEGATIVE'
END
ELSE IF @SE51c = 1
BEGIN
	SELECT @SE51cVal = 'POSITIVE'
END
IF @SE51d = 0
BEGIN
	SELECT @SE51dVal = 'NEGATIVE'
END
ELSE IF @SE51d = 1
BEGIN
	SELECT @SE51dVal = 'POSITIVE'
END

IF @SE52a = 0
BEGIN
	SELECT @SE52aVal = 'NEGATIVE'
END
ELSE IF @SE52a = 1
BEGIN
	SELECT @SE52aVal = 'POSITIVE'
END
IF @SE52b = 0
BEGIN
	SELECT @SE52bVal = 'NEGATIVE'
END
ELSE IF @SE52b = 1
BEGIN
	SELECT @SE52bVal = 'POSITIVE'
END
IF @SE52c = 0
BEGIN
	SELECT @SE52cVal = 'NEGATIVE'
END
ELSE IF @SE52c = 1
BEGIN
	SELECT @SE52cVal = 'POSITIVE'
END
IF @SE52d = 0
BEGIN
	SELECT @SE52dVal = 'NEGATIVE'
END
ELSE IF @SE52d = 1
BEGIN
	SELECT @SE52dVal = 'POSITIVE'
END

IF @SE53a = 0
BEGIN
	SELECT @SE53aVal = 'NEGATIVE'
END
ELSE IF @SE53a = 1
BEGIN
	SELECT @SE53aVal = 'POSITIVE'
END
IF @SE53b = 0
BEGIN
	SELECT @SE53bVal = 'NEGATIVE'
END
ELSE IF @SE53b = 1
BEGIN
	SELECT @SE53bVal = 'POSITIVE'
END
IF @SE53c = 0
BEGIN
	SELECT @SE53cVal = 'NEGATIVE'
END
ELSE IF @SE53c = 1
BEGIN
	SELECT @SE53cVal = 'POSITIVE'
END
IF @SE53d = 0
BEGIN
	SELECT @SE53dVal = 'NEGATIVE'
END
ELSE IF @SE53d = 1
BEGIN
	SELECT @SE53dVal = 'POSITIVE'
END

IF @SE54a = 0
BEGIN
	SELECT @SE54aVal = 'NEGATIVE'
END
ELSE IF @SE54a = 1
BEGIN
	SELECT @SE54aVal = 'POSITIVE'
END
IF @SE54b = 0
BEGIN
	SELECT @SE54bVal = 'NEGATIVE'
END
ELSE IF @SE54b = 1
BEGIN
	SELECT @SE54bVal = 'POSITIVE'
END
IF @SE54c = 0
BEGIN
	SELECT @SE54cVal = 'NEGATIVE'
END
ELSE IF @SE54c = 1
BEGIN
	SELECT @SE54cVal = 'POSITIVE'
END
IF @SE54d = 0
BEGIN
	SELECT @SE54dVal = 'NEGATIVE'
END
ELSE IF @SE54d = 1
BEGIN
	SELECT @SE54dVal = 'POSITIVE'
END

IF @SE61a = 0
BEGIN
	SELECT @SE61aVal = 'NEGATIVE'
END
ELSE IF @SE61a = 1
BEGIN
	SELECT @SE61aVal = 'POSITIVE'
END
IF @SE61b = 0
BEGIN
	SELECT @SE61bVal = 'NEGATIVE'
END
ELSE IF @SE61b = 1
BEGIN
	SELECT @SE61bVal = 'POSITIVE'
END
IF @SE61c = 0
BEGIN
	SELECT @SE61cVal = 'NEGATIVE'
END
ELSE IF @SE61c = 1
BEGIN
	SELECT @SE61cVal = 'POSITIVE'
END
IF @SE61d = 0
BEGIN
	SELECT @SE61dVal = 'NEGATIVE'
END
ELSE IF @SE61d = 1
BEGIN
	SELECT @SE61dVal = 'POSITIVE'
END

IF @SE62a = 0
BEGIN
	SELECT @SE62aVal = 'NEGATIVE'
END
ELSE IF @SE62a = 1
BEGIN
	SELECT @SE62aVal = 'POSITIVE'
END
IF @SE62b = 0
BEGIN
	SELECT @SE62bVal = 'NEGATIVE'
END
ELSE IF @SE62b = 1
BEGIN
	SELECT @SE62bVal = 'POSITIVE'
END
IF @SE62c = 0
BEGIN
	SELECT @SE62cVal = 'NEGATIVE'
END
ELSE IF @SE62c = 1
BEGIN
	SELECT @SE62cVal = 'POSITIVE'
END
IF @SE62d = 0
BEGIN
	SELECT @SE62dVal = 'NEGATIVE'
END
ELSE IF @SE62d = 1
BEGIN
	SELECT @SE62dVal = 'POSITIVE'
END

IF @SE63a = 0
BEGIN
	SELECT @SE63aVal = 'NEGATIVE'
END
ELSE IF @SE63a = 1
BEGIN
	SELECT @SE63aVal = 'POSITIVE'
END
IF @SE63b = 0
BEGIN
	SELECT @SE63bVal = 'NEGATIVE'
END
ELSE IF @SE63b = 1
BEGIN
	SELECT @SE63bVal = 'POSITIVE'
END
IF @SE63c = 0
BEGIN
	SELECT @SE63cVal = 'NEGATIVE'
END
ELSE IF @SE63c = 1
BEGIN
	SELECT @SE63cVal = 'POSITIVE'
END
IF @SE63d = 0
BEGIN
	SELECT @SE63dVal = 'NEGATIVE'
END
ELSE IF @SE63d = 1
BEGIN
	SELECT @SE63dVal = 'POSITIVE'
END

IF @SE64a = 0
BEGIN
	SELECT @SE64aVal = 'NEGATIVE'
END
ELSE IF @SE64a = 1
BEGIN
	SELECT @SE64aVal = 'POSITIVE'
END
IF @SE64b = 0
BEGIN
	SELECT @SE64bVal = 'NEGATIVE'
END
ELSE IF @SE64b = 1
BEGIN
	SELECT @SE64bVal = 'POSITIVE'
END
IF @SE64c = 0
BEGIN
	SELECT @SE64cVal = 'NEGATIVE'
END
ELSE IF @SE64c = 1
BEGIN
	SELECT @SE64cVal = 'POSITIVE'
END
IF @SE64d = 0
BEGIN
	SELECT @SE64dVal = 'NEGATIVE'
END
ELSE IF @SE64d = 1
BEGIN
	SELECT @SE64dVal = 'POSITIVE'
END

IF @CO11a = 0
BEGIN
	SELECT @CO11aVal = 'NEGATIVE'
END
ELSE IF @CO11a = 1
BEGIN
	SELECT @CO11aVal = 'POSITIVE'
END
IF @CO11b = 0
BEGIN
	SELECT @CO11bVal = 'NEGATIVE'
END
ELSE IF @CO11b = 1
BEGIN
	SELECT @CO11bVal = 'POSITIVE'
END
IF @CO11c = 0
BEGIN
	SELECT @CO11cVal = 'NEGATIVE'
END
ELSE IF @CO11c = 1
BEGIN
	SELECT @CO11cVal = 'POSITIVE'
END
IF @CO11d = 0
BEGIN
	SELECT @CO11dVal = 'NEGATIVE'
END
ELSE IF @CO11d = 1
BEGIN
	SELECT @CO11dVal = 'POSITIVE'
END
IF @CO11e = 0
BEGIN
	SELECT @CO11eVal = 'NEGATIVE'
END
ELSE IF @CO11e = 1
BEGIN
	SELECT @CO11eVal = 'POSITIVE'
END

IF @CO12a = 0
BEGIN
	SELECT @CO12aVal = 'NEGATIVE'
END
ELSE IF @CO12a = 1
BEGIN
	SELECT @CO12aVal = 'POSITIVE'
END
IF @CO12b = 0
BEGIN
	SELECT @CO12bVal = 'NEGATIVE'
END
ELSE IF @CO12b = 1
BEGIN
	SELECT @CO12bVal = 'POSITIVE'
END
IF @CO12c = 0
BEGIN
	SELECT @CO12cVal = 'NEGATIVE'
END
ELSE IF @CO12c = 1
BEGIN
	SELECT @CO12cVal = 'POSITIVE'
END
IF @CO12d = 0
BEGIN
	SELECT @CO12dVal = 'NEGATIVE'
END
ELSE IF @CO12d = 1
BEGIN
	SELECT @CO12dVal = 'POSITIVE'
END
IF @CO12e = 0
BEGIN
	SELECT @CO12eVal = 'NEGATIVE'
END
ELSE IF @CO12e = 1
BEGIN
	SELECT @CO12eVal = 'POSITIVE'
END

IF @CO21a = 0
BEGIN
	SELECT @CO21aVal = 'NEGATIVE'
END
ELSE IF @CO21a = 1
BEGIN
	SELECT @CO21aVal = 'POSITIVE'
END
IF @CO21b = 0
BEGIN
	SELECT @CO21bVal = 'NEGATIVE'
END
ELSE IF @CO21b = 1
BEGIN
	SELECT @CO21bVal = 'POSITIVE'
END
IF @CO21c = 0
BEGIN
	SELECT @CO21cVal = 'NEGATIVE'
END
ELSE IF @CO21c = 1
BEGIN
	SELECT @CO21cVal = 'POSITIVE'
END
IF @CO21d = 0
BEGIN
	SELECT @CO21dVal = 'NEGATIVE'
END
ELSE IF @CO21d = 1
BEGIN
	SELECT @CO21dVal = 'POSITIVE'
END
IF @CO21e = 0
BEGIN
	SELECT @CO21eVal = 'NEGATIVE'
END
ELSE IF @CO21e = 1
BEGIN
	SELECT @CO21eVal = 'POSITIVE'
END

IF @CO22a = 0
BEGIN
	SELECT @CO22aVal = 'NEGATIVE'
END
ELSE IF @CO22a = 1
BEGIN
	SELECT @CO22aVal = 'POSITIVE'
END
IF @CO22b = 0
BEGIN
	SELECT @CO22bVal = 'NEGATIVE'
END
ELSE IF @CO22b = 1
BEGIN
	SELECT @CO22bVal = 'POSITIVE'
END
IF @CO22c = 0
BEGIN
	SELECT @CO22cVal = 'NEGATIVE'
END
ELSE IF @CO22c = 1
BEGIN
	SELECT @CO22cVal = 'POSITIVE'
END
IF @CO22d = 0
BEGIN
	SELECT @CO22dVal = 'NEGATIVE'
END
ELSE IF @CO22d = 1
BEGIN
	SELECT @CO22dVal = 'POSITIVE'
END
IF @CO22e = 0
BEGIN
	SELECT @CO22eVal = 'NEGATIVE'
END
ELSE IF @CO22e = 1
BEGIN
	SELECT @CO22eVal = 'POSITIVE'
END

IF @CO23a = 0
BEGIN
	SELECT @CO23aVal = 'NEGATIVE'
END
ELSE IF @CO23a = 1
BEGIN
	SELECT @CO23aVal = 'POSITIVE'
END
IF @CO23b = 0
BEGIN
	SELECT @CO23bVal = 'NEGATIVE'
END
ELSE IF @CO23b = 1
BEGIN
	SELECT @CO23bVal = 'POSITIVE'
END
IF @CO23c = 0
BEGIN
	SELECT @CO23cVal = 'NEGATIVE'
END
ELSE IF @CO23c = 1
BEGIN
	SELECT @CO23cVal = 'POSITIVE'
END
IF @CO23d = 0
BEGIN
	SELECT @CO23dVal = 'NEGATIVE'
END
ELSE IF @CO23d = 1
BEGIN
	SELECT @CO23dVal = 'POSITIVE'
END
IF @CO23e = 0
BEGIN
	SELECT @CO23eVal = 'NEGATIVE'
END
ELSE IF @CO23e = 1
BEGIN
	SELECT @CO23eVal = 'POSITIVE'
END

IF @CO24a = 0
BEGIN
	SELECT @CO24aVal = 'NEGATIVE'
END
ELSE IF @CO24a = 1
BEGIN
	SELECT @CO24aVal = 'POSITIVE'
END
IF @CO24b = 0
BEGIN
	SELECT @CO24bVal = 'NEGATIVE'
END
ELSE IF @CO24b = 1
BEGIN
	SELECT @CO24bVal = 'POSITIVE'
END
IF @CO24c = 0
BEGIN
	SELECT @CO24cVal = 'NEGATIVE'
END
ELSE IF @CO24c = 1
BEGIN
	SELECT @CO24cVal = 'POSITIVE'
END
IF @CO24d = 0
BEGIN
	SELECT @CO24dVal = 'NEGATIVE'
END
ELSE IF @CO24d = 1
BEGIN
	SELECT @CO24dVal = 'POSITIVE'
END
IF @CO24e = 0
BEGIN
	SELECT @CO24eVal = 'NEGATIVE'
END
ELSE IF @CO24e = 1
BEGIN
	SELECT @CO24eVal = 'POSITIVE'
END

IF @CO31a = 0
BEGIN
	SELECT @CO31aVal = 'NEGATIVE'
END
ELSE IF @CO31a = 1
BEGIN
	SELECT @CO31aVal = 'POSITIVE'
END
IF @CO31b = 0
BEGIN
	SELECT @CO31bVal = 'NEGATIVE'
END
ELSE IF @CO31b = 1
BEGIN
	SELECT @CO31bVal = 'POSITIVE'
END
IF @CO31c = 0
BEGIN
	SELECT @CO31cVal = 'NEGATIVE'
END
ELSE IF @CO31c = 1
BEGIN
	SELECT @CO31cVal = 'POSITIVE'
END
IF @CO31d = 0
BEGIN
	SELECT @CO31dVal = 'NEGATIVE'
END
ELSE IF @CO31d = 1
BEGIN
	SELECT @CO31dVal = 'POSITIVE'
END
IF @CO31e = 0
BEGIN
	SELECT @CO31eVal = 'NEGATIVE'
END
ELSE IF @CO31e = 1
BEGIN
	SELECT @CO31eVal = 'POSITIVE'
END

IF @CO32a = 0
BEGIN
	SELECT @CO32aVal = 'NEGATIVE'
END
ELSE IF @CO32a = 1
BEGIN
	SELECT @CO32aVal = 'POSITIVE'
END
IF @CO32b = 0
BEGIN
	SELECT @CO32bVal = 'NEGATIVE'
END
ELSE IF @CO32b = 1
BEGIN
	SELECT @CO32bVal = 'POSITIVE'
END
IF @CO32c = 0
BEGIN
	SELECT @CO32cVal = 'NEGATIVE'
END
ELSE IF @CO32c = 1
BEGIN
	SELECT @CO32cVal = 'POSITIVE'
END
IF @CO32d = 0
BEGIN
	SELECT @CO32dVal = 'NEGATIVE'
END
ELSE IF @CO32d = 1
BEGIN
	SELECT @CO32dVal = 'POSITIVE'
END
IF @CO32e = 0
BEGIN
	SELECT @CO32eVal = 'NEGATIVE'
END
ELSE IF @CO32e = 1
BEGIN
	SELECT @CO32eVal = 'POSITIVE'
END

IF @CO33a = 0
BEGIN
	SELECT @CO33aVal = 'NEGATIVE'
END
ELSE IF @CO33a = 1
BEGIN
	SELECT @CO33aVal = 'POSITIVE'
END
IF @CO33b = 0
BEGIN
	SELECT @CO33bVal = 'NEGATIVE'
END
ELSE IF @CO33b = 1
BEGIN
	SELECT @CO33bVal = 'POSITIVE'
END
IF @CO33c = 0
BEGIN
	SELECT @CO33cVal = 'NEGATIVE'
END
ELSE IF @CO33c = 1
BEGIN
	SELECT @CO33cVal = 'POSITIVE'
END
IF @CO33d = 0
BEGIN
	SELECT @CO33dVal = 'NEGATIVE'
END
ELSE IF @CO33d = 1
BEGIN
	SELECT @CO33dVal = 'POSITIVE'
END
IF @CO33e = 0
BEGIN
	SELECT @CO33eVal = 'NEGATIVE'
END
ELSE IF @CO33e = 1
BEGIN
	SELECT @CO33eVal = 'POSITIVE'
END

IF @CO34a = 0
BEGIN
	SELECT @CO34aVal = 'NEGATIVE'
END
ELSE IF @CO34a = 1
BEGIN
	SELECT @CO34aVal = 'POSITIVE'
END
IF @CO34b = 0
BEGIN
	SELECT @CO34bVal = 'NEGATIVE'
END
ELSE IF @CO34b = 1
BEGIN
	SELECT @CO34bVal = 'POSITIVE'
END
IF @CO34c = 0
BEGIN
	SELECT @CO34cVal = 'NEGATIVE'
END
ELSE IF @CO34c = 1
BEGIN
	SELECT @CO34cVal = 'POSITIVE'
END
IF @CO34d = 0
BEGIN
	SELECT @CO34dVal = 'NEGATIVE'
END
ELSE IF @CO34d = 1
BEGIN
	SELECT @CO34dVal = 'POSITIVE'
END
IF @CO34e = 0
BEGIN
	SELECT @CO34eVal = 'NEGATIVE'
END
ELSE IF @CO34e = 1
BEGIN
	SELECT @CO34eVal = 'POSITIVE'
END

IF @CO41a = 0
BEGIN
	SELECT @CO41aVal = 'NEGATIVE'
END
ELSE IF @CO41a = 1
BEGIN
	SELECT @CO41aVal = 'POSITIVE'
END
IF @CO41b = 0
BEGIN
	SELECT @CO41bVal = 'NEGATIVE'
END
ELSE IF @CO41b = 1
BEGIN
	SELECT @CO41bVal = 'POSITIVE'
END
IF @CO41c = 0
BEGIN
	SELECT @CO41cVal = 'NEGATIVE'
END
ELSE IF @CO41c = 1
BEGIN
	SELECT @CO41cVal = 'POSITIVE'
END
IF @CO41d = 0
BEGIN
	SELECT @CO41dVal = 'NEGATIVE'
END
ELSE IF @CO41d = 1
BEGIN
	SELECT @CO41dVal = 'POSITIVE'
END
IF @CO41e = 0
BEGIN
	SELECT @CO41eVal = 'NEGATIVE'
END
ELSE IF @CO41e = 1
BEGIN
	SELECT @CO41eVal = 'POSITIVE'
END

IF @CO51a = 0
BEGIN
	SELECT @CO51aVal = 'NEGATIVE'
END
ELSE IF @CO51a = 1
BEGIN
	SELECT @CO51aVal = 'POSITIVE'
END
IF @CO51b = 0
BEGIN
	SELECT @CO51bVal = 'NEGATIVE'
END
ELSE IF @CO51b = 1
BEGIN
	SELECT @CO51bVal = 'POSITIVE'
END
IF @CO51c = 0
BEGIN
	SELECT @CO51cVal = 'NEGATIVE'
END
ELSE IF @CO51c = 1
BEGIN
	SELECT @CO51cVal = 'POSITIVE'
END
IF @CO51d = 0
BEGIN
	SELECT @CO51dVal = 'NEGATIVE'
END
ELSE IF @CO51d = 1
BEGIN
	SELECT @CO51dVal = 'POSITIVE'
END
IF @CO51e = 0
BEGIN
	SELECT @CO51eVal = 'NEGATIVE'
END
ELSE IF @CO51e = 1
BEGIN
	SELECT @CO51eVal = 'POSITIVE'
END

declare @debug int = 0
declare @now datetime = getUtcDate()
declare @nowJson nvarchar(30) = FORMAT(GETUTCDATE(),'yyyy-MM-ddTHH:mm:ss.fffZ')
declare @inspsetId int
declare @inspID int
declare @inspSetName nvarchar(50)
declare @result nvarchar(50)
declare @answerExists int
declare @schName nvarchar(200)
declare @version int = 3


-- Get the inspection set for the year of the date
SELECT @inspsetID = max(inspsetID)
FROM InspectionSet
WHERE inspsetType = 'SCHOOL_ACCREDITATION'
AND inspsetYear = common.SchoolYearInt(@date, null)

if @inspsetId is null begin
	print 'no inspection set'
	return
end

PRINT @inspSetID

SELECT @inspSetName = inspsetName
FROM InspectionSet
WHERE inspsetType = 'SCHOOL_ACCREDITATION' AND inspsetID = @inspID

declare @inspDate datetime
declare @template xml

-- Get school name
SELECT @schName = schName FROM Schools WHERE schNo = @schNo;

-- The surveyTag is a special date used in tablet surveys. It is usually based on the
-- first day the data collection starts on the field. It is used by the tablet as a means
-- to filter out surveys when using the offline bluetooth merging feature. Here, since there is
-- no way to know what the surveyTag would be (nor will it even be useful from here on)
-- we simple assign a surveyTag of the first day of the year of this schools accreditation.
declare @surveyTag nvarchar(10) = CONCAT(year(@date),'-01-01')

-- Check if the school inspection already exists (inserting or updating)
SELECT @inspID = inspID FROM [pInspectionRead].[SchoolInspections] WHERE schNo = @schNo and InspectionYear = year(@date)

IF @@ROWCOUNT = 0 BEGIN
	IF @Debug = 1 BEGIN
		print 'Inserting record'
	END
	-- If inserting a new record insert first using the default template
	SELECT @template = intyTemplate
	FROM lkpInspectionTypes
	WHERE intyCode = 'SCHOOL_ACCREDITATION'

	-- Inserting new record
	INSERT INTO [pInspectionRead].[SchoolInspections]
	(
		[pCreateTag]
		, [schNo]
		, [PlannedDate]
		, [StartDate]
		, [EndDate]
		, [Note]
		, [InspectedBy]
		, [InspectionSetID]
		, [InspectionContent]
		, [InspectionSetName]
		, [InspectionYear]
		, [InspectionResult]
		, [SourceId]
		, [InspectionType]
		, [pCreateUser]
		, [pCreateDateTime]
		, [pEditUser]
		, [pEditDateTime]
	)
	VALUES
	(
	   NEWID()
	   , @schNo
	   , NULL
	   , @StartDate
	   , @EndDate
	   , CONCAT('[Loaded from manual record] ', @note)
	   , @InspectedBy
	   , @inspSetID
	   , @template
	   , @inspSetName
	   , year(@date)
	   , NULL
	   , NULL
	   , 'SCHOOL_ACCREDITATION'
	   , @user
	   , @now
	   , @user
	   , @now
	);

	-- Get the inspID of the school accreditation just inserted
	SELECT @inspID = inspID FROM [pInspectionRead].[SchoolInspections] WHERE schNo = @schNo and InspectionYear = year(@date)
	IF @Debug = 1 BEGIN
		SELECT @inspID;
	END
END

-- Now...whether we just inserted new one or it was already there we proceed to update.
-- The only thing we need to update is the XML survey content and the inspection result
-- which may change (if there was an existing record)
IF @Debug = 1 BEGIN
	PRINT 'Updating record'
END

-- From this point on we are querying the XML directly from InspectionContent
-- Whether it was just inserted from a default (@template above) with no answer state data
-- or it was already there from a previously loaded tablet survey. Keeping the loaded
-- XML survey from the tablet is important to not
-- loose additional details on the XML file such as photos, comments meta data, etc..

IF @Debug = 1 BEGIN
	SELECT @inspID AS InspectionID;
END

-- Modify the XML data.
-- I am sure this is ugly but the only way I got it to work (GH).
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.1.a"]/answer/state/text())[1] with sql:variable("@SE11aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE11aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.1.b"]/answer/state/text())[1] with sql:variable("@SE11bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE11bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.1.c"]/answer/state/text())[1] with sql:variable("@SE11cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE11cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.1.d"]/answer/state/text())[1] with sql:variable("@SE11dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE11dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.1.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.2.a"]/answer/state/text())[1] with sql:variable("@SE12aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE12aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.2.b"]/answer/state/text())[1] with sql:variable("@SE12bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE12bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.2.c"]/answer/state/text())[1] with sql:variable("@SE12cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE12cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.2.d"]/answer/state/text())[1] with sql:variable("@SE12dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE12dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.2.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.3.a"]/answer/state/text())[1] with sql:variable("@SE13aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE13aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.3.b"]/answer/state/text())[1] with sql:variable("@SE13bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE13bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.3.c"]/answer/state/text())[1] with sql:variable("@SE13cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE13cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.3.d"]/answer/state/text())[1] with sql:variable("@SE13dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE13dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.3.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.4.a"]/answer/state/text())[1] with sql:variable("@SE14aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE14aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.4.b"]/answer/state/text())[1] with sql:variable("@SE14bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE14bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.4.c"]/answer/state/text())[1] with sql:variable("@SE14cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE14cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.1.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.1.4.d"]/answer/state/text())[1] with sql:variable("@SE14dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE14dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.1.4.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.1.a"]/answer/state/text())[1] with sql:variable("@SE21aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE21aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.1.b"]/answer/state/text())[1] with sql:variable("@SE21bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE21bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.1.c"]/answer/state/text())[1] with sql:variable("@SE21cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE21cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.1.d"]/answer/state/text())[1] with sql:variable("@SE21dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE21dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.1.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.2.a"]/answer/state/text())[1] with sql:variable("@SE22aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE22aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.2.b"]/answer/state/text())[1] with sql:variable("@SE22bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE22bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.2.c"]/answer/state/text())[1] with sql:variable("@SE22cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE22cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.2.d"]/answer/state/text())[1] with sql:variable("@SE22dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE22dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.2.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.3.a"]/answer/state/text())[1] with sql:variable("@SE23aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE23aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.3.b"]/answer/state/text())[1] with sql:variable("@SE23bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE23bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.3.c"]/answer/state/text())[1] with sql:variable("@SE23cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE23cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.3.d"]/answer/state/text())[1] with sql:variable("@SE23dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE23dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.3.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.4.a"]/answer/state/text())[1] with sql:variable("@SE24aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE24aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.4.b"]/answer/state/text())[1] with sql:variable("@SE24bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE24bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.4.c"]/answer/state/text())[1] with sql:variable("@SE24cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE24cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.2.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.2.4.d"]/answer/state/text())[1] with sql:variable("@SE24dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE24dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.2.4.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.1.a"]/answer/state/text())[1] with sql:variable("@SE31aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE31aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.1.b"]/answer/state/text())[1] with sql:variable("@SE31bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE31bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.1.c"]/answer/state/text())[1] with sql:variable("@SE31cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE31cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.1.d"]/answer/state/text())[1] with sql:variable("@SE31dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE31dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.1.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.2.a"]/answer/state/text())[1] with sql:variable("@SE32aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE32aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.2.b"]/answer/state/text())[1] with sql:variable("@SE32bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE32bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.2.c"]/answer/state/text())[1] with sql:variable("@SE32cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE32cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.2.d"]/answer/state/text())[1] with sql:variable("@SE32dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE32dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.2.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.3.a"]/answer/state/text())[1] with sql:variable("@SE33aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE33aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.3.b"]/answer/state/text())[1] with sql:variable("@SE33bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE33bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.3.c"]/answer/state/text())[1] with sql:variable("@SE33cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE33cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.3.d"]/answer/state/text())[1] with sql:variable("@SE33dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE33dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.3.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.4.a"]/answer/state/text())[1] with sql:variable("@SE34aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE34aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.4.b"]/answer/state/text())[1] with sql:variable("@SE34bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE34bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.4.c"]/answer/state/text())[1] with sql:variable("@SE34cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE34cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.3.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.3.4.d"]/answer/state/text())[1] with sql:variable("@SE34dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE34dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.3.4.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.1.a"]/answer/state/text())[1] with sql:variable("@SE41aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE41aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.1.b"]/answer/state/text())[1] with sql:variable("@SE41bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE41bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.1.c"]/answer/state/text())[1] with sql:variable("@SE41cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE41cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.1.d"]/answer/state/text())[1] with sql:variable("@SE41dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE41dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.1.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.2.a"]/answer/state/text())[1] with sql:variable("@SE42aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE42aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.2.b"]/answer/state/text())[1] with sql:variable("@SE42bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE42bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.2.c"]/answer/state/text())[1] with sql:variable("@SE42cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE42cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.2.d"]/answer/state/text())[1] with sql:variable("@SE42dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE42dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.2.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.3.a"]/answer/state/text())[1] with sql:variable("@SE43aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE43aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.3.b"]/answer/state/text())[1] with sql:variable("@SE43bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE43bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.3.c"]/answer/state/text())[1] with sql:variable("@SE43cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE43cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.3.d"]/answer/state/text())[1] with sql:variable("@SE43dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE43dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.3.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.4.a"]/answer/state/text())[1] with sql:variable("@SE44aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE44aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.4.b"]/answer/state/text())[1] with sql:variable("@SE44bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE44bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.4.c"]/answer/state/text())[1] with sql:variable("@SE44cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE44cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.4.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.4.4.d"]/answer/state/text())[1] with sql:variable("@SE44dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE44dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.4.4.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.1.a"]/answer/state/text())[1] with sql:variable("@SE51aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE51aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.1.b"]/answer/state/text())[1] with sql:variable("@SE51bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE51bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.1.c"]/answer/state/text())[1] with sql:variable("@SE51cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE51cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.1.d"]/answer/state/text())[1] with sql:variable("@SE51dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE51dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.1.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.2.a"]/answer/state/text())[1] with sql:variable("@SE52aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE52aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.2.b"]/answer/state/text())[1] with sql:variable("@SE52bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE52bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.2.c"]/answer/state/text())[1] with sql:variable("@SE52cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE52cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.2.d"]/answer/state/text())[1] with sql:variable("@SE52dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE52dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.2.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.3.a"]/answer/state/text())[1] with sql:variable("@SE53aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE53aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.3.b"]/answer/state/text())[1] with sql:variable("@SE53bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE53bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.3.c"]/answer/state/text())[1] with sql:variable("@SE53cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE53cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.3.d"]/answer/state/text())[1] with sql:variable("@SE53dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE53dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.3.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.4.a"]/answer/state/text())[1] with sql:variable("@SE54aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE54aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.4.b"]/answer/state/text())[1] with sql:variable("@SE54bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE54bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.4.c"]/answer/state/text())[1] with sql:variable("@SE54cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE54cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.5.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.5.4.d"]/answer/state/text())[1] with sql:variable("@SE54dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE54dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.5.4.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.1.a"]/answer/state/text())[1] with sql:variable("@SE61aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE61aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.1.b"]/answer/state/text())[1] with sql:variable("@SE61bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE61bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.1.c"]/answer/state/text())[1] with sql:variable("@SE61cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE61cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.1.d"]/answer/state/text())[1] with sql:variable("@SE61dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE61dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.1.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.2.a"]/answer/state/text())[1] with sql:variable("@SE62aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE62aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.2.b"]/answer/state/text())[1] with sql:variable("@SE62bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE62bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.2.c"]/answer/state/text())[1] with sql:variable("@SE62cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE62cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.2.d"]/answer/state/text())[1] with sql:variable("@SE62dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE62dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.2.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.3.a"]/answer/state/text())[1] with sql:variable("@SE63aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE63aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.3.b"]/answer/state/text())[1] with sql:variable("@SE63bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE63bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.3.c"]/answer/state/text())[1] with sql:variable("@SE63cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE63cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.3.d"]/answer/state/text())[1] with sql:variable("@SE63dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE63dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.3.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.4.a"]/answer/state/text())[1] with sql:variable("@SE64aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE64aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.4.b"]/answer/state/text())[1] with sql:variable("@SE64bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE64bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.4.c"]/answer/state/text())[1] with sql:variable("@SE64cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE64cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="SE.6.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="SE.6.4.d"]/answer/state/text())[1] with sql:variable("@SE64dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@SE64dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="SE.6.4.d"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.a"]/answer/state/text())[1] with sql:variable("@CO11aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO11aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.b"]/answer/state/text())[1] with sql:variable("@CO11bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO11bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.c"]/answer/state/text())[1] with sql:variable("@CO11cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO11cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.d"]/answer/state/text())[1] with sql:variable("@CO11dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO11dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.1.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.e"]/answer/state/text())[1] with sql:variable("@CO11eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO11eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.a"]/answer/state/text())[1] with sql:variable("@CO12aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO12aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.b"]/answer/state/text())[1] with sql:variable("@CO12bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO12bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.c"]/answer/state/text())[1] with sql:variable("@CO12cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO12cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.d"]/answer/state/text())[1] with sql:variable("@CO12dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO12dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.1.2.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.e"]/answer/state/text())[1] with sql:variable("@CO12eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO12eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.a"]/answer/state/text())[1] with sql:variable("@CO21aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO21aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.b"]/answer/state/text())[1] with sql:variable("@CO21bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO21bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.c"]/answer/state/text())[1] with sql:variable("@CO21cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO21cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.d"]/answer/state/text())[1] with sql:variable("@CO21dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO21dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.1.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.e"]/answer/state/text())[1] with sql:variable("@CO21eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO21eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.a"]/answer/state/text())[1] with sql:variable("@CO22aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO22aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.b"]/answer/state/text())[1] with sql:variable("@CO22bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO22bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.c"]/answer/state/text())[1] with sql:variable("@CO22cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO22cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.d"]/answer/state/text())[1] with sql:variable("@CO22dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO22dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.2.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.e"]/answer/state/text())[1] with sql:variable("@CO22eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO22eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.a"]/answer/state/text())[1] with sql:variable("@CO23aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO23aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.b"]/answer/state/text())[1] with sql:variable("@CO23bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO23bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.c"]/answer/state/text())[1] with sql:variable("@CO23cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO23cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.d"]/answer/state/text())[1] with sql:variable("@CO23dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO23dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.3.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.e"]/answer/state/text())[1] with sql:variable("@CO23eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO23eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.a"]/answer/state/text())[1] with sql:variable("@CO24aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO24aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.b"]/answer/state/text())[1] with sql:variable("@CO24bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO24bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.c"]/answer/state/text())[1] with sql:variable("@CO24cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO24cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.d"]/answer/state/text())[1] with sql:variable("@CO24dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO24dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.2.4.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.e"]/answer/state/text())[1] with sql:variable("@CO24eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO24eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.a"]/answer/state/text())[1] with sql:variable("@CO31aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO31aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.b"]/answer/state/text())[1] with sql:variable("@CO31bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO31bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.c"]/answer/state/text())[1] with sql:variable("@CO31cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO31cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.d"]/answer/state/text())[1] with sql:variable("@CO31dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO31dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.1.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.e"]/answer/state/text())[1] with sql:variable("@CO31eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO31eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.2.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.a"]/answer/state/text())[1] with sql:variable("@CO32aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO32aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.2.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.b"]/answer/state/text())[1] with sql:variable("@CO32bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO32bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.2.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.c"]/answer/state/text())[1] with sql:variable("@CO32cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO32cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.2.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.d"]/answer/state/text())[1] with sql:variable("@CO32dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO32dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.2.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.e"]/answer/state/text())[1] with sql:variable("@CO32eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO32eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.3.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.a"]/answer/state/text())[1] with sql:variable("@CO33aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO33aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.3.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.b"]/answer/state/text())[1] with sql:variable("@CO33bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO33bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.3.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.c"]/answer/state/text())[1] with sql:variable("@CO33cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO33cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.3.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.d"]/answer/state/text())[1] with sql:variable("@CO33dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO33dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.3.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.e"]/answer/state/text())[1] with sql:variable("@CO33eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO33eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.4.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.a"]/answer/state/text())[1] with sql:variable("@CO34aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO34aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.4.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.b"]/answer/state/text())[1] with sql:variable("@CO34bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO34bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.4.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.c"]/answer/state/text())[1] with sql:variable("@CO34cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO34cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.4.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.d"]/answer/state/text())[1] with sql:variable("@CO34dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO34dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.3.4.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.e"]/answer/state/text())[1] with sql:variable("@CO34eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO34eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.4.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.a"]/answer/state/text())[1] with sql:variable("@CO41aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO41aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.4.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.b"]/answer/state/text())[1] with sql:variable("@CO41bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO41bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.4.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.c"]/answer/state/text())[1] with sql:variable("@CO41cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO41cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.4.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.d"]/answer/state/text())[1] with sql:variable("@CO41dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO41dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.4.1.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.e"]/answer/state/text())[1] with sql:variable("@CO41eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO41eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.e"])[1]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.5.1.a"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.a"]/answer/state/text())[1] with sql:variable("@CO51aVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO51aVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.a"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.5.1.b"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.b"]/answer/state/text())[1] with sql:variable("@CO51bVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO51bVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.b"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.5.1.c"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.c"]/answer/state/text())[1] with sql:variable("@CO51cVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO51cVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.c"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.5.1.d"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.d"]/answer/state/text())[1] with sql:variable("@CO51dVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO51dVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.d"])[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/category/standard/criteria/subcriteria[id="CO.5.1.e"]/answer/state') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.e"]/answer/state/text())[1] with sql:variable("@CO51eVal")')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>{sql:variable("@CO51eVal")}</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.e"])[1]')
	WHERE inspID = @inspID;
END

-- The second classroom observation is not known when inserting from parameters
-- Since only the lowest one count I'll include a dummy one with all POSITIVE
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.1.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.1.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.1.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.1.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.1.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.1.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.1.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.2.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.2.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.2.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.2.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.1.2.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.1.2.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.1.2.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.1.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.1.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.1.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.1.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.1.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.1.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.1.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.2.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.2.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.2.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.2.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.2.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.2.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.2.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.3.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.3.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.3.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.3.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.3.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.3.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.3.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.4.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.4.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.4.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.4.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.2.4.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.2.4.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.2.4.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.1.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.1.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.1.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.1.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.1.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.1.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.1.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.2.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.2.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.2.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.2.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.2.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.2.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.2.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.3.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.3.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.3.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.3.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.3.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.3.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.3.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.4.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.4.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.4.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.4.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.3.4.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.3.4.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.3.4.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.4.1.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.4.1.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.4.1.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.4.1.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.4.1.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.4.1.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.4.1.e"])[2]')
	WHERE inspID = @inspID;
END

SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.5.1.a"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.a"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.a"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.5.1.b"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.b"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.b"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.5.1.c"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.c"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.c"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.5.1.d"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.d"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.d"])[2]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('(/survey/category/standard/criteria/subcriteria[id="CO.5.1.e"]/answer/state)[2]') = 1 AND inspID = @inspID;
IF @@ROWCOUNT != 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('replace value of (/survey/category/standard/criteria/subcriteria[id="CO.5.1.e"]/answer/state/text())[2] with "POSITIVE"')
	WHERE inspID = @inspID;
END
ELSE
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <answer><state>POSITIVE</state></answer> into (/survey/category/standard/criteria/subcriteria[id="CO.5.1.e"])[2]')
	WHERE inspID = @inspID;
END

-- Update the survey's meta data
-- Presumably the survey's meta data--if already there--does not need
-- to be updated and this may have been actually done on the tablet
-- but just minor correction done in the data (above)
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/completeDate') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <completeDate>{sql:variable("@nowJson")}</completeDate> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/createDate') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <createDate>{sql:variable("@nowJson")}</createDate> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/createUser') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <createUser>{sql:variable("@user")}</createUser> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/lastEditedUser') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <lastEditedUser>{sql:variable("@user")}</lastEditedUser> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/country') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <country>{sql:variable("@country")}</country> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/schoolNo') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <schoolNo>{sql:variable("@schNo")}</schoolNo> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/schoolName') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <schoolName>{sql:variable("@schName")}</schoolName> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/surveyCompleted') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <surveyCompleted>POSITIVE</surveyCompleted> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/surveyTag') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <surveyTag>{sql:variable("@surveyTag")}</surveyTag> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/type') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <type>SCHOOL_ACCREDITATION</type> into (/survey)[1]')
	WHERE inspID = @inspID;
END
SELECT @answerExists = inspID FROM [pInspectionRead].[SchoolInspections] WHERE [InspectionContent].exist('/survey/version') = 1 AND inspID = @inspID;
IF @@ROWCOUNT = 0
BEGIN
	UPDATE [pInspectionRead].[SchoolInspections]
	SET [InspectionContent].modify('insert <version>{sql:variable("@version")}</version> into (/survey)[1]')
	WHERE inspID = @inspID;
END

-- Update result (which is possibly different then before)
SELECT @result = dbo.CalcInspectionResult_SSA(@inspID);

IF @Debug = 1 BEGIN
	SELECT @result AS Result
END

UPDATE [pInspectionRead].[SchoolInspections]
SET [InspectionResult] = @result
WHERE inspID = @inspID;

-- Update the changed tracked edit fields to keep an audit
-- of any adjustment made through this stored procedure.
UPDATE [pInspectionRead].[SchoolInspections]
SET [pEditUser] = @user, [pEditDateTime] = @now
WHERE inspID = @inspID;

END
GO

