SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 6 2021
-- Description:	Proc to customize generation of pdf using
--				cenopdf - this is applied to classrooms and extra classrooms pages
--				to change the dropdown of class levels
-- Refer to http://www.lystech.com/webhelp/default.htm 'Exporting to PDF from a Database'
-- =============================================
CREATE PROCEDURE [cenopdf].[Classrooms]
	@formtype nvarchar(10),	-- either PRI or SEC
	@targetFile nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @t TABLE
	(
		formtype nvarchar(10),
		schooltype nvarchar(10),
		targetfile nvarchar(500),
		classlevels nvarchar(max),
		teacherroles nvarchar(max)
	)

	INSERT INTO @t (formtype, schooltype)
	Select tt, st from
	(
		-- this implicitly maps the formtype to a school type
		-- to use for getting levels and teacher roles etc
		Select 'SEC' tt, 'CS' st UNION ALL Select 'PRI' tt, 'P' st
	) U
	WHERE @formtype = U.tt or nullif(@formtype, '') is null

	UPDATE @t
	SET targetfile = replace(@targetfile,'%formtype%', formtype),
	classlevels = cenopdf.ClassLevelItems(schoolType),		-- schooltype, not formtype
	teacherroles = cenopdf.TeacherRoleItems(schoolType)

	Select
	classlevels [Room.Class.D.00.Level_ItemsText]
	, classlevels [Room.Class.D.01.Level_ItemsText]
	, classlevels [Room.Class.D.02.Level_ItemsText]
	, classlevels [Room.Class.D.03.Level_ItemsText]
	, classlevels [Room.Class.D.04.Level_ItemsText]
	, classlevels [Room.Class.D.05.Level_ItemsText]
	, classlevels [Room.Class.D.06.Level_ItemsText]
	, classlevels [Room.Class.D.07.Level_ItemsText]
	, classlevels [Room.Class.D.08.Level_ItemsText]
	, classlevels [Room.Class.D.09.Level_ItemsText]
	, targetFile targetFile
	FROM @t


END
GO

