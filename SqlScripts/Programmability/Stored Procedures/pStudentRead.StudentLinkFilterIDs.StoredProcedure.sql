SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 26 04 2021
-- Description:	Student Link filter
-- =============================================
CREATE PROCEDURE [pStudentRead].[StudentLinkFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@LinkID				int = null,		-- the unique idenitifer of the
	@StudentID			uniqueidentifier = null,
	@StudentName		nvarchar(100) = null,		-- matched with like if
	@DocumentID			uniqueidentifier = null,
	@Keyword			nvarchar(50) = null,
	@Function			nvarchar(50) = null,
	@DateStart			datetime = null,
	@DateEnd			datetime = null,
	@DocumentSource		nvarchar(100) = null,
	@DocType			nvarchar(10) = null,
	@IsImage			int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
	selectedID int
	, recNo int IDENTITY PRIMARY KEY
	)
	INSERT INTO @keys
	(selectedID)
	Select lnkID
	From pStudentRead.StudentLinks SL
		LEFT JOIN Student_ S
			ON SL.stuID = S.stuID
	WHERE
	(lnkID = @linkID or @linkID is null)
	AND (SL.stuID = @StudentID or @StudentID is null)
	AND (S.stuGiven = @StudentName or @StudentName is null)
	AND (docID = @DocumentID or @DocumentID is null)
	AND (lnkFunction = @Function or @Function is null)
	AND (docDate >= @DateStart or @DateStart is null)
	AND (common.dropTime(docDate) <= @DateEnd or @DateEnd is null)
	AND (docSource = @documentSource OR @documentSource is null)
	AND (docType = @docType or @docType is null)
	AND (isnull(@IsImage, 0) = 0 or docType in ('jpg','jpeg','bmp','png','gif') )

	AND (docTags like '%' + @Keyword + '%' or
			docDescription like '%' + @Keyword + '%'
			or @Keyword is null)

	ORDER BY
		case @sortColumn			-- strings

			WHEN 'Student' then stuGiven
			WHEN 'StudentName' then stuGiven
			WHEN 'sSurname' then stuGiven

			WHEN 'Document' then docID
			WHEN 'docID' then docID

			WHEN 'Function' then lnkFunction
			WHEN 'lnkFunction' then lnkFunction


			WHEN 'Source' then docSource
			WHEN 'docSource' then docSource

			WHEN 'docType' then docType
			ELSE null
			end,
		case @sortColumn -- numbers

			When 'StudentID' then SL.stuID
			WHEN 'stuID' then SL.stuID
			ELSE null
		end,
		case @sortColumn -- numbers
			WHEN 'docDate' then docDate
			ELSE null
		end,
		case @sortColumn -- numbers
			WHEN 'Document' then docID
			WHEN 'docID' then docID
			ELSE null
		end,

		lnkID


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedID
		, RecNo
		FROM
		(
			Select selectedID
			, @NumMatches - RecNo + 1 RecNo
			FROM @Keys
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)
				)
		ORDER BY RecNo
	end
	else begin

		SELECT selectedID
		, RecNo
		FROM @Keys
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
END
GO

