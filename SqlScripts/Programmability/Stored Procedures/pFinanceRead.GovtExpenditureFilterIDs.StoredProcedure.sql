SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	Filter financia data (GovtExpenditure)
-- =============================================
CREATE PROCEDURE [pFinanceRead].[GovtExpenditureFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@fnmID int = null,
    @Year int	= null,
	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

-- if filter params come from XML
if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	    @fnmID = isnull(@fnmID, fnmID),
		@Year = isnull(@Year, Year),
		@District = isnull(@District, District)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	fnmID int '@fnmID',
	Year int '@Year',
	District nvarchar(10) '@District'
	)
end

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)

INSERT INTO @KeysAll (ID)
SELECT fnmID
from GovtExpenditureState GX -- view
WHERE
(fnmID = @fnmID OR @fnmID IS NULL)
AND (fnmYear = @Year or @Year is null)
AND (fnmDistrict = @District OR @District is null)

OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

