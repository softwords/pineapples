SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 11 2007
-- Description:	data for teacher subjects taught
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVSubjectsTaughtData]
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- only a select statement in here, probably doesn't need to be an sp?
SELECT
	TeacherSurvey.tchsID AS TeacherID,
	TeacherSurvey.ssID,
	TeacherSurvey.tchEdQual AS EdQualification,
	TeacherSurvey.tchQual AS Qualification,
	case
		when (qual.codeQualified = 1 Or qualE.codequalified = 1) then 1
		else 0
	end AS Qualified,
	case
		when (qual.codeCertified = 1 Or qualE.codeCertified = 1) then 1
		else 0
	end AS Certified

FROM (TeacherSurvey
			LEFT JOIN lkpTeacherQual AS Qual
				ON TeacherSurvey.tchQual = Qual.codeCode)
		LEFT JOIN lkpTeacherQual AS QualE
			ON TeacherSurvey.tchEdQual = QualE.codeCode


END
GO
GRANT EXECUTE ON [dbo].[sp_PIVSubjectsTaughtData] TO [pSchoolRead] AS [dbo]
GO

