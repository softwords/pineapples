SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3/6/2009
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SurveyControlFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0

--filter parameters
	, @SurveyYear int = null
	, @District nvarchar(10) = null
	, @Island nvarchar(10)  = null
	, @SchoolType nvarchar(10) = null
	, @Authority nvarchar(10)= null
	, @FileLocation nvarchar(30)= null
	, @Status int = -1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)

INSERT INTO @keysAll (ID)
SELECT SYH.syID


FROM
	SchoolYearHistory SYH
		INNER JOIN Schools ON Schools.schNo = SYH.schNo
		INNER JOIN Islands on Islands.iCode = Schools.iCode
		INNER JOIN Survey  ON Survey.svyYear = SYH.syYear
		LEFT JOIN SchoolSurvey ON (SYH.schNo = SchoolSurvey.schNo) AND (SYH.syYear = SchoolSurvey.svyYear)
		LEFT JOIN pSurveyRead.ssIDQuality QUAL ON SchoolSurvey.ssID = QUAL.ssID
		LEFT JOIN Districts D
			ON Islands.iGRoup = D.dID

WHERE
	(syYear = @SurveyYear or @SurveyYear is null)
	and (iGroup = @District or @District is null)
	and (Schools.iCode = @Island or @Island is null)
	and (schType = @SchoolType or @SchoolType is null)
	and (schAuth = @Authority or @Authority is null)
	and (saFileLocn = @FileLocation or @FileLocation is null)

	and (
			(@Status <= 0)
		or	(@Status = 1 and saSent is null )
		or	(@Status = 2 and saReceived is null )
		or	(@Status = 3 and saEntered is null )
		or	(@Status = 4 and saAudit = 1  )
		or	(@Status = 5 and saAudit = 1  and saAuditDate is null )
		or	(@Status = 6 and saAuditDate is not null )
		or	(@Status = 7 and saAuditResult = 'F' )
		or	(@Status = 8 and MaxLevel in (1,2) )
		or	(@Status = 9 and MaxLevel > 0 )
		or	(@Status = 10 and saSent is not null )
		or	(@Status = 11 and saReceived is not null )
		or	(@Status = 12 and saEntered is not null )
		or	(@Status = 13 and saEntered is null and SchoolSurvey.ssID is not null )
		or	(@Status = 14 and syDormant = 1 )
		)
	ORDER BY

		CASE @sortColumn			-- strings

				when 'schNo' then SYH.schNo
				when 'schName' then schName
				when 'saCompletedby' then saCompletedby
				when 'saAuditBy' then saAuditBy
				when 'saAuditResult' then saAuditResult
				when 'saFileLocn' then saFileLocn

				when 'SchoolNo' then SYH.schNo
				when 'SchoolName' then schName
				when 'Completedby' then saCompletedby
				when 'AuditBy' then saAuditBy
				when 'AuditResult' then saAuditResult
				when 'FileLocation' then saFileLocn

			ELSE null
			END,
		CASE @sortColumn			-- date

				when 'saSent' then saSent
				when 'saCollected' then saCollected
				when 'saReceived' then saReceived
				when 'saReceivedTarget' then saReceivedTarget
				when 'saEntered' then saEntered
				when 'saAuditDate' then saAuditDate
				when 'saReceivedD' then saReceivedD

				when 'Sent' then saSent
				when 'Collected' then saCollected
				when 'Received' then saReceived
				when 'ReceivedTarget' then saReceivedTarget
				when 'Entered' then saEntered
				when 'AuditDate' then saAuditDate
				when 'ReceivedD' then saReceivedD
			ELSE null
			END,
		CASE @sortColumn			-- numbers

				when 'saAudit' then saAudit
				when 'syDormant' then syDormant

				when 'Audit' then saAudit
				when 'Dormant' then syDormant
			ELSE null
			END

SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end


END
GO

