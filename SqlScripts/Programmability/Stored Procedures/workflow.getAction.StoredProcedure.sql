SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 10 2009
-- Description:	can the current user action the specified POR, given its current status?
-- =============================================
CREATE PROCEDURE [workflow].[getAction]
	-- Add the parameters for the stored procedure here
	@PORID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select *
	FRom
		workflow.ActionList A
	WHERE
		porID = @PorID

END
GO

