SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 5 2022
-- Description:	Write a document record with a school link for an uploaded survey
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfLogSurveyDocument]
	@surveyID int
	, @fileID uniqueidentifier
	, @extension nvarchar(50)
	, @xml xml
	, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @oldFileId nvarchar(100)
	declare @dt datetime = getUTCDate()

	Select @oldFileID = ssSource
	FROM SchoolSurvey
	WHERE ssID = @surveyID

	if (@oldFileId is not null) begin
		DELETE from pSchoolRead.SchoolLinks
		WHERE docID = @oldFileID

		DELETE from Documents_
		WHERE docID = @oldFileID
	end

    -- Insert statements for procedure here
	INSERT INTO pSchoolRead.SchoolLinks
	(
		schNo
		, docID
		, lnkFunction
		, docTitle
		, docDescription
		, docType
		, pCreateUser
		, pCreateDateTime
	)
	SELECT SS.schNo
	, upper(@fileID)
	, 'SURVEY'
	, schName + ' Survey ' + convert(nvarchar(4), svyYear) + '.' + @extension
	, schName + ' Survey ' + convert(nvarchar(4), svyYear)
	, Upper(@extension)
	, @user
	, @dt
	FROM SchoolSurvey SS
		LEFT JOIN Schools S
		ON SS.schNo = S.schNo
	WHERE ssID = @surveyID

	UPDATE SchoolSurvey
	SET ssSource = @fileID
	WHERE ssID = @surveyID

	-- update the xfdf image
	-- this is convenient to have as a database field so that any mistakes in processing can be fixed with Sql directly
	-- however, its too big to put in sseData on School survey - it would cause a problem in daya to day simple queries against his table
	-- by returning masses of data

	INSERT INTO SchoolSurveyXml_
	(ssID, ssXml, pCreateUser, pCreateDateTime)
	SELECT @surveyID, @xml, @user, @dt
	WHERE NOT EXISTS (Select ssID from SchoolSurveyXml_ WHERE ssID = @surveyID)
	if (@@ROWCOUNT = 0) begin
		UPDATE schoolSurveyXml_
		set ssXml = @xml
		, pEditDateTime = @dt
		, pEditUser = @user
		WHERE ssID = @surveyID
	end


	-- return the value of the previous file ID - this allows the cller to clen aup by deleting that file
	SELECT @oldFileID oldFileID

END
GO

