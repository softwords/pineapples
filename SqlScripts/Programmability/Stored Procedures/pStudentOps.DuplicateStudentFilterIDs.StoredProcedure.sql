SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 30 6 2020
-- Edit date: 11 09 2020
-- Description:	Identify duplicate students
-- =============================================
CREATE PROCEDURE [pStudentOps].[DuplicateStudentFilterIDs]
	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	 @MatchGiven int = NULL
	, @MatchSurname int = NULL
	, @MatchCard int = NULL
	, @MatchDoB int = null
	, @MatchGender int = null
	, @Given nvarchar(100) = null
	, @Surname nvarchar(100) = null
	, @District nvarchar(5) = null

	, @xmlfilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @keysAll TABLE
	(
	stuID uniqueidentifier,
	dupID uniqueidentifier,
	recNo int IDENTITY PRIMARY KEY
	)

	-- use a helper table for District
	DECLARE @studentDistricts TABLE
	(
		stuId uniqueidentifier
	)

if (@District is not null) begin
    -- If a student ever had an enrolment with a district it should come up
	INSERT INTO @studentDistricts
	( stuID )
	SELECT DISTINCT stuID FROM StudentEnrolment_ SE
INNER JOIN Schools S
		ON SE.schNo = S.schNo
	INNER JOIN lkpIslands I
		ON S.iCode = I.iCode
	WHERE I.iGroup = @District
end

-- adapt the given and surname fields to support * ? as well as % _

Select @given = replace(replace(@given,'*','%'),'?','_')
, @surname = replace(replace(@surname,'*','%'),'?','_')

	INSERT INTO @KeysAll (stuID, DupID)
Select S.stuID
, DUP.stuID
FROM Student_ S
INNER JOIN Student_ DUP
ON (@MatchGiven is null
--or (@MatchGiven = 1 AND common.stripcompare(S.stuGiven, DUP.stuGiven, default) = 1)
or (@MatchGiven = 1 AND S.stuGiven =  DUP.stuGiven)
or (@MatchGiven = 2 and S.stuGiven <> DUP.stuGiven		-- don;t include complete matches in fuzzy
	and (
			S.stuGivenSoundex = DUP.stuGivenSoundex
			OR S.stuGiven = DUP.stuFamilyName
			-- OR common.stripcompare(S.stuGiven, DUP.stuGiven, default) = 1
		)
	)
OR (@MatchGiven = 0 AND isnull(S.stuGiven,'') <> isnull(DUP.stuGiven,''))
)
AND (@MatchSurname is null
or (@MatchSurname = 1 AND common.stripcompare(S.stuFamilyName, DUP.stuFamilyName, default) = 1)
or (@MatchSurname = 2 and S.stuFamilyName <> DUP.stuFamilyName		-- don;t include complete matches in fuzzy
	and (
			S.stuFamilySoundex = DUP.stuFamilySoundex
			OR S.stuFamilyName = DUP.stuGiven
			-- OR common.stripcompare(S.stuFamilyName, DUP.stuFamilyName, default) = 1
		)
	)
OR (@MatchSurname = 0 AND isnull(S.stuFamilyName,'') <> isnull(DUP.stuFamilyName,''))
)
AND (@MatchCard is null
or (@MatchCard = 1 AND S.stuCardID = DUP.stuCardID)
OR (@MatchCard = 0 AND isnull(S.stuCardID,'') <> isnull(DUP.stuCardID,''))
)
AND (@MatchDoB is null
or (@MatchDoB = 1 AND S.stuDoB = DUP.stuDoB)
OR (@MatchDoB = 0 AND isnull(S.stuDoB,'1901-01-01') <> isnull(DUP.stuDoB,'1901-01-01'))
)
AND (@MatchGender is null
or (@MatchGender = 1 AND S.stuGender = DUP.stuGender)
OR (@MatchGender = 0 AND isnull(S.stuGender,'') <> isnull(DUP.stuGender,''))
)
AND (@Given is null
OR S.stuGiven like @Given + '%'
OR DUP.stuGiven like @Given + '%'
)
AND (@Surname is null
OR S.stuFamilyName like @Surname + '%'
OR DUP.stuFamilyName like @Surname + '%'
)
AND (@District is null
	OR ( S.stuID in (Select stuID from @studentDistricts)
	AND DUP.stuID in (Select stuID from @studentDistricts)
	)
)

AND Coalesce(@MatchGiven, @MatchSurname, @MatchCard,
	@MatchDoB, @MatchGender) is not null

WHERE S.stuID > DUP.stuID

ORDER BY
-- strings
		case @SortColumn
			when 'Given' then S.stuGiven
			when 'Surname' then S.stuFamilyName
			when 'CardID' then S.stuCardID
			when 'Gender' then S.stuGender

			when 'stuGiven' then S.stuGiven
			when 'stuFamilyName' then S.stuFamilyName
			when 'stuCardID' then S.stuCardID
			when 'stuGender' then S.stuGender

			when 'stuEthnicity' then S.stuEthnicity
		end,
		--dates
		case @sortColumn
			when 'DoB' then S.stuDoB
			when 'stuDoB' then S.stuDoB
		end,
		S.stuID
OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT stuID
		, DupID
		, RecNo
		FROM
		(
			Select stuID, DupID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT stuID, DupID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

