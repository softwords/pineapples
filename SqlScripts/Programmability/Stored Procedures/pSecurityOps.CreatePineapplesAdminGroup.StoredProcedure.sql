SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 03 2010
-- Description:	For initial system setup, make a nominated Domain group
-- the Pineapples admin group
-- =============================================
-- the schema here servers no securitypurpose becuase you have to be
-- securityadmin and db_securityadmin to run this....
-- just helps to keep it in a known place
CREATE PROCEDURE [pSecurityOps].[CreatePineapplesAdminGroup]
	-- Add the parameters for the stored procedure here

	@groupName	sysname
	, @systemName sysname = null


AS
BEGIN
	SET NOCOUNT ON;

	declare @AdminRole sysname
	declare @AdminUser sysname
-- maybe the only time you'd supply this optional arg is in a traning db?
	if (@systemName is null)
		select @systemName = db_name()

	select @AdminRole = @systemName + 'Admin'

	select @AdminUser = @systemName + 'AdminUser'

-- you are a security admin, and a local db security admin running this
if (IS_SRVROLEMEMBER('sysadmin') = 0) begin
		IF (IS_SRVROLEMEMBER ('securityadmin') = 0 )
			raiserror('You must be a member of the server role ''securityadmin''to run this procedure',16,10)
		IF (IS_MEMBER ('db_securityadmin') = 0 )
			raiserror('You must be a member of the database role ''db_securityadmin'' to run this procedure',16,10)
		IF (IS_MEMBER ('db_accessadmin') = 0 )
			raiserror('You must be a member of the database role ''db_accessadmin'' to run this procedure',16,10)
end


begin try


begin transaction
			-- ensure pSecurityOps is db_securityadmin

		exec sp_addrolemember 'db_securityadmin', 'pSecurityOps'
		exec sp_addrolemember 'db_accessadmin', 'pSecurityOps'  -- and accessadmin too


-- create our new Role, it's called <app>Admin

		exec pSecurityOps.createOrgRole @AdminRole

-- create our user and add it to this role

		exec pSecurityOps.createPineapplesUser @AdminUser, @groupName, @AdminRole

-- finally we just have to assign the basic permissions we want for an admin group
-- all basic reads, and all admins

	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Admin', 'RW','RW', 'RW','Y','Y'

	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Enrolment', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Establishment', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Exam', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Finance', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Infrastructure', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Inspection', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'IST', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'School', 'R',null, null,'Y'

	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Security', null,null, null,null,'Y'	-- ops

	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Survey', 'R',null, null,'Y'
	exec pSecurityOps.configureOrgUnitPermissions @AdminRole,'Teacher', 'R',null, null,'Y'

	commit transaction

	-- make this user a member of securityadmin
		-- this has to be outside any transaction
		-- note that sp_addsrvrolemember has the arguments
		-- the other way round from sp_addrolemember!
		exec sp_addsrvrolemember  @GroupName, 'securityadmin'


end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err

end catch

END
GO

