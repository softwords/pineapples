SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 August 2021
-- Description:	Create a recordset to populate the census workbook staff page
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusRolloverWash]
	-- Add the parameters for the stored procedure here
	@SurveyYear int
	, @schoolNo nvarchar(50) = null
	, @districtCode nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @ON nvarchar(1) = 'X';
	Declare @OFF nvarchar(1) = null;
SELECT SS.schNo, SS.svyYear,

v.value('@Index', 'int') [ RowIndex]
--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')                              [State]

-- use the current school name
, S.schName	[School_Name]
--, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
-- calculated
--, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                                   [School_Type]
, nullif(ltrim(v.value('@Main_Source_Drinking_Water', 'nvarchar(100)')),'')                       [Main_Source_Drinking_Water]
, nullif(ltrim(v.value('@Currently_Available', 'nvarchar(100)')),'')                              [Currently_Available]
, nullif(ltrim(v.value('@Toilet_Type', 'nvarchar(100)')),'')                                      [Toilet_Type]
, common.try_convert_int(
	v.value('@Girls_Toilets_Total', 'nvarchar(100)')
	)																[Girls_Toilets_Total]
, common.try_convert_int(
	v.value('@Girls_Toilets_Usable', 'nvarchar(100)')
	)																[Girls_Toilets_Usable]
, common.try_convert_int(
	v.value('@Boys_Toilets_Total', 'nvarchar(100)')
	)									                            [Boys_Toilets_Total]
, common.try_convert_int(
	v.value('@Boys_Toilets_Usable', 'nvarchar(100)')
	)									                            [Boys_Toilets_Usable]
, common.try_convert_int(
	v.value('@Common_Toilets_Total', 'nvarchar(100)')
	)										                        [Common_Toilets_Total]
, common.try_convert_int(
	v.value('@Common_Toilets_Usable', 'nvarchar(100)')
	)						                                          [Common_Toilets_Usable]
, nullif(ltrim(v.value('@Available', 'nvarchar(100)')),'')                                        [Available]
, nullif(ltrim(v.value('@Soap_and_Water', 'nvarchar(100)')),'')                                   [Soap_and_Water]

, v.query('.')			-- returns the xml object v from its root node 'row'
from SchoolSurvey SS
INNER JOIN Schools S
	ON S.schNo = SS.schNo
LEFT JOIN Islands I
	ON S.iCode = I.iCode
OUTER APPLY SS.ssWashData.nodes('row') as V(v)
WHERE SS.svyYear = @SurveyYear
AND (SS.schNo = @schoolNo OR @schoolNo is null)
AND (I.iGroup = @districtCode OR @DistrictCode is null)

END
GO

