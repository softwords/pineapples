SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 12 2017
-- Description:	Refresh the warehouse school counts
-- =============================================
CREATE PROCEDURE [warehouse].[buildSchoolCounts]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;


	print 'warehouse.BuildSchoolCounts'
	print '--------------------------'
	print ''

---- warehouse.SchoolActive
---- 1 row per school, year, with active flag (1 = active, 0 or no record = not active
	DELETE
	FROM warehouse.SchoolActive
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

	print 'warehouse.SchoolActive deletes - rows:' + convert(nvarchar(10), @@rowcount)

	-- *** RULE ***
	-- a school is active in a year if:
	------ the year is >= schEst
	------ the year is strictly less than schClosed
	------ the school is not marked as dormant that year

	INSERT INTO warehouse.SchoolActive
	(
		SurveyYear
		, schNo
		, active
	)
	Select SVY.svyYear
	, S.schNo
	, case when SYH.syDormant = 1 then 'D' else 'A' end
	from Survey SVY
		INNER JOIN Schools S
			ON SVY.svyYear >= isnull(schEst,0)
			AND SVY.svyYear < isnull(nullif(schClosed,0),9999)
		LEFT JOIN SchoolYearHistory SYH				-- this is unlikely to appear
			ON SYH.schNo = S.schNo
			AND SYH.syYear = SVY.svyYEar
	WHERE (SVY.svyYear >= @StartFromYear or @StartFromYear is null)

	print 'warehouse.SchoolActive inserts - rows:' + convert(nvarchar(10), @@rowcount)

	-- mark the year of closure specifically
	INSERT INTO warehouse.SchoolActive
	(
		SurveyYear
		, schNo
		, active
	)
	Select svyYear
	, S.schNo
	, 'X'
	from Survey SVY
		INNER JOIN Schools S
			ON svyYear = schClosed
	WHERE (SVY.svyYear >= @StartFromYear or @StartFromYear is null)

	print 'warehouse.SchoolActive insert closure - rows:' + convert(nvarchar(10), @@rowcount)

--- now the most common aggregates are saved

	DELETE
	FROM warehouse.SchoolCountTable
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)

	print 'warehouse.SchoolCountTable deletes - rows:' + convert(nvarchar(10), @@rowcount)

	INSERT INTO warehouse.SchoolCountTable
	(
		SurveyYear
		, DistrictCode
		, District
		, AuthorityCode
		, Authority
		, AuthorityGovtCode
		, AuthorityGovt
		, SchoolTypeCode
		, SchoolType
		, Region
		, NumSchools
	)
	Select
		WSA.SurveyYear
		, DistrictCode
		, District
		, AuthorityCode
		, Authority
		, AuthorityGovtCode
		, AuthorityGovt
		, SchoolTypeCode
		, SchoolType
		, Region
		,  count(WSA.schNo) NumSchools
		FROM
		warehouse.SchoolActive WSA
			LEFT JOIN warehouse.BestSurvey BS
				ON WSA.SurveyYear = BS.surveyYear
				AND WSA.schNo = BS.schNo
			LEFT JOIN warehouse.DimensionSchoolSurvey DSS
				ON BS.SurveyDimensionID = DSS.[Survey ID]
		WHERE WSA.active = 'A'
		AND (WSA.SurveyYear >= @StartFromYear or @StartFromYEar is null)
		GROUP BY
			WSA.SurveyYear
			, DistrictCode
			, District
			, AuthorityCode
			, Authority
			, AuthorityGovtCode
			, AuthorityGovt
			, SchoolTypeCode
			, SchoolType
			, Region
		print 'warehouse.SchoolCountTable inserts - rows:' + convert(nvarchar(10), @@rowcount)


	exec warehouse.logVersion

	declare @versionID nvarchar(50)
	declare @versionDateTime dateTime

	Select @VersionId = versionID
	, @VersionDateTime = versionDateTime
	from warehouse.versionInfo

	print ''
	print 'warehouse.BuildSchoolCounts completed'
	print '------------------------------------'
	print ''
	print 'Warehouse version: ' + @versionId + ' ' + convert(nvarchar(40), @versionDateTime) + ' (UTC)'

END
GO

