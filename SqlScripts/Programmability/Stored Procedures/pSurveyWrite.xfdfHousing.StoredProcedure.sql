SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	Update simple review of teacher housing provision
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfHousing]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Housing block

	<field name="Funding">
		<field name="Rev">
			<field name="Fundraising" />
			<field name="DonationPriv" />
			<field name="DonationChurch" />
			<field name="DonationOther" />
			<field name="GovGrant" />
			<field name="Total" />
		</field>
		<field name="Exp">
			<field name="Supplies" />
			<field name="Books" />
			<field name="Other" />
			<field name="Total" />
		</field>
		<field name="Total" />
	</field>

this is updated directly onto the SchoolSurvey Table
*/
begin try

	UPDATE schoolSurvey
		SET ssTchHouse = House
		, ssTchHouseOnSite = OnSite
		, ssTchHouseOffSite = OffSite
		, ssTchHouseM	= Maintenance
		, ssTchHouseN = NotProvided
	FROM OPENXML(@idoc, '/x:field',2)		-- base is the Housing Node
	WITH
	(
		House				int			'x:field[@name="House"]/x:value'
		, OnSite				int			'x:field[@name="OnSite"]/x:value'
		, OffSite				int			'x:field[@name="OffSite"]/x:value'
		, Maintenance				int			'x:field[@name="M"]/x:value'
		, NotProvided				int			'x:field[@name="N"]/x:value'
	) X
	WHERE SchoolSurvey.ssID = @SurveyID
	exec audit.xfdfInsert @SurveyID, 'Survey updated','TeacherHousing',@@rowcount
end try

begin catch
   DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,'TeacherHousing'

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err

end catch
END
GO

