SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 12 2017
-- Description:	Refresh the warehouse aggregate cohort table
-- =============================================
CREATE PROCEDURE [warehouse].[buildCohort]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
---- school level flow model

		begin transaction
		IF OBJECT_ID('warehouse.cohort', 'U') IS NULL begin
			Select SurveyYear
			, avg(Estimate) Estimate -- one valueis non-null
			, YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(RepNY) RepNY
			, sum(TroutNY) TroutNY
			, sum(EnrolNYNextLevel) EnrolNYNextLevel
			, sum(RepNYNextLevel) RepNYNextLevel
			, sum(TrinNYNextLevel) TrinNYNextLevel
			INTO warehouse.cohort
			FROM
			(
			Select
			SurveyYear
			, Estimate
			, lvlYear YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, Enrol
			, Rep
			, null RepNY
			, null TroutNY
			, null EnrolNYNextLevel
			, null RepNYNextLevel
			, null TrinNYNextLevel

			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
			WHERE (Enrol is not null or Rep is not null)
			UNION ALL
			-- data for next year, same class level
			Select
			SurveyYear - 1
			, null Estimate
			, lvlYear YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, null Enrol
			, null Rep
			, Rep RepNY
			, Trout TroutNY
			, null EnrolNYNextLevel
			, null RepNYNextLevel
			, null TrinNYNextLevel
			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
			WHERE (Rep is not null or Trout is not null)
			UNION ALL
			-- data for next year, same class level
			Select
			SurveyYear - 1
			, null Estimate
			, lvlYear - 1
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, null Enrol
			, null Rep
			, null RepNY
			, null TroutNY
			, Enrol EnrolNYNextLevel
			, Rep RepNYNextLevel
			, Trin TrinNYeExtLevel
			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
					WHERE (Enrol is not null or Rep is not null or Trin is not null)
			) U

			GROup BY
			SurveyYear
			, YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode

		end else begin
			DELETE FROM warehouse.cohort
			WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
			print 'warehouse.cohort deletes - rows:' + convert(nvarchar(10), @@rowcount)

			INSERT INTO warehouse.cohort
			select SurveyYear
			, avg(Estimate) Estimate -- one valueis non-null
			, YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(RepNY) RepNY
			, sum(TroutNY) TroutNY
			, sum(EnrolNYNextLevel) EnrolNYNextLevel
			, sum(RepNYNextLevel) RepNYNextLevel
			, sum(TrinNYNextLevel) TrinNYNextLevel

			FROM
			(
			Select
			SurveyYear
			, Estimate
			, lvlYear YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, Enrol
			, Rep
			, null RepNY
			, null TroutNY
			, null EnrolNYNextLevel
			, null RepNYNextLevel
			, null TrinNYNextLevel

			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
			WHERE (Enrol is not null or Rep is not null)
			UNION ALL
			-- data for next year, same class level
			Select
			SurveyYear - 1
			, null Estimate
			, lvlYear YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, null Enrol
			, null Rep
			, Rep RepNY
			, Trout TroutNY
			, null EnrolNYNextLevel
			, null RepNYNextLevel
			, null TrinNYNextLevel
			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
			WHERE (Rep is not null or Trout is not null)
			UNION ALL
			-- data for next year, same class level
			Select
			SurveyYear - 1
			, null Estimate
			, lvlYear - 1
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, null Enrol
			, null Rep
			, null RepNY
			, null TroutNY
			, Enrol EnrolNYNextLevel
			, Rep RepNYNextLevel
			, Trin TrinNYeExtLevel
			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
					WHERE (Enrol is not null or Rep is not null or Trin is not null)
			) U
			WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
			GROup BY
			SurveyYear
			, YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			print 'warehouse.cohort inserts - rows:' + convert(nvarchar(10), @@rowcount)

		end
		commit transaction

END
GO

