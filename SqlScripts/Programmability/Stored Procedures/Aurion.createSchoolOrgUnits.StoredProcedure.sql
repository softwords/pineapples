SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Aurion].[createSchoolOrgUnits]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try


	create table #OrgUnits
	(
			OrgUnitNumber int
			, OrgUnitLevel int
			, OrgUnitName nvarchar(25)
			, OrgUnitShortName nvarchar(8)
			, OrgUnitLongDescription nvarchar(50)
			, OrgUnitParent int
			, OrgUnitReference nvarchar(10)
	)


	exec Aurion.createSchoolTypeOrgUnits @SendAll, 1		-- nested

	begin transaction
	-- list the schools that need an org unit number
		Select DISTINCT S.schNo
		INTO #tmp
		FROM Establishment E
			INNER JOIN Schools S
				ON E.schNo = S.SchNo
		WHERE
			schOrgUnitNumber is null
		-- 29 10 2009 avoid these
			AND S.schAuth is not null


		declare @ToAllocate int

		-- this is the number of org unit numbers we need
		Select @ToAllocate = count(*)
		from #tmp

		-- this is where we'll put them
		declare @counters table
		(
		counter int
		, maxCounters int
		, seq int
		, char nvarchar(6)
		)

		IF (@ToAllocate > 0 ) begin


		-- get the numbers

			insert into @counters
			exec getCounter 'Aurion_OrgUnit', @ToAllocate


			UPDATE Schools
				SET schOrgUnitNumber = counter

			FROM
				Schools S
					INNER JOIN
						(Select row_number() over (ORDER BY schNo) Pos
						, SchNo
						from #tmp
						) ORDERED
						ON S.schNo = ORDERED.schNo
					INNER JOIN @counters C
						ON ORDERED.Pos = C.seq

			-- are the parent levels OK?


		end


	commit transaction

	declare @TopOrgUnit int
	declare @TopOrgLevel int

	Select @TopOrgUnit = common.SysParamInt('Aurion_TopLevelOrgNumber',0)
	Select @TopOrgLevel = common.SysParamInt('Aurion_TopLevelOrgLevel',4)


	INSERT INTO #OrgUnits
	(
		OrgUnitNumber
		, OrgUnitLevel
		, OrgUnitName
		, OrgUnitShortName
		, ORgUnitLongDescription
		, OrgUnitParent
		, OrgUnitReference
	)
	SELECT
		schOrgUnitNumber
		, @TopOrgLevel + 4						-- don;t like to hard code this
		, left(schName,25)
		, S.schNo
		, left(S.schNo + ': ' + schName, 50)
		, STOrg.OrgUnitNumber
		, S.schNo

	FROM Schools S
		LEFT JOIN Aurion.SchoolTypeOrgUnits STOrg
			ON S.schAuth = STOrg.authCode
			AND S.schType = STOrg.stCode
		LEFT JOIN #tmp
			ON S.SchNo = #tmp.SchNo
	WHERE
		schOrgUnitNumber is not null
		AND
		(#tmp.SchNo is not null
			OR @SendAll = 1)

	exec Aurion.createSectorCodeOrgUnits @SendAll, 1		-- nested

	-- return the ones we added
	Select *
	FROM
		#OrgUnits
	ORDER BY orgUnitLevel
			, orgUnitNumber

	-- return any bindings we need too

	SELECT GLSA.*
	FROM
		Aurion.EffectiveGLSalaryAccount GLSA
	WHERE
		GLSA.orgUnitNumber in (Select OrgUnitNumber FROM #OrgUnits)


	drop table #tmp
	drop table #OrgUnits

end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

