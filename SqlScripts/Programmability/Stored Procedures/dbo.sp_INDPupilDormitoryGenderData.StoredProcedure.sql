SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 11 2007
-- Description:	Data for pupil  dormitory ratio
-- =============================================
CREATE PROCEDURE [dbo].[sp_INDPupilDormitoryGenderData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- there are 3 data sources - enrolment, boarders and dormitories
-- attemtped this with outer join, union-and-sum is much faster


create table #data
(
LifeYear int,
schNo nvarchar(50),
surveyDimensionssID int,
Estimate int,
bestYearEnrol int,
offsetEnrol int,
EnrolM int,
EnrolF int,
brdM int,
brdF int,
capM int,
capF int,
showersM int,
ShowersF int,
toiletsM int,
toiletsF int,
NumRooms int,
TotSize int,
minSize int,
[MaxSize] int,
SizeSupplied int,
sizeOK int,
RoomDataSupplied int
)

SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT *
INTO #ebr
FROM dbo.tfnESTIMATE_BestSurveyRoomType('DORM')

INSERT INTO #data
(LifeYear, SchNo, surveyDimensionssID, EnrolM, EnrolF)
SELECT
	E.LifeYear,
	E.schNo,
	E.surveyDimensionssID,
	SS.ssEnrolM,
	SS.ssEnrolF
FROM #ebse E
	INNER JOIN SchoolSurvey SS
		ON E.bestssID = ss.ssID

-- boarders
INSERT INTO #data
(LifeYear, SchNo,brdM, brdF)
SELECT
	E.LifeYear,
	E.schNo,
	B.brdM,
	B.brdF
FROM #ebse E
	INNER JOIN (Select
						ssID,
						sum(ptM) brdM,
						sum(ptF) brdF
					FROM vtblBoarders
					GROUP BY ssID
					) B
		ON E.bestssID = B.ssID


-- dormitories
INSERT INTO #data
(
LifeYear, schNo,
capM, capF,
showersM, ShowersF,
toiletsM, toiletsF,
NumRooms,
TotSize,
minSize,
MaxSize,
SizeSupplied,
sizeOK,
RoomDataSupplied
)
Select
E.LifeYear, E.schNo,
capM, capF,
showersM, ShowersF,
toiletsM, toiletsF,
NumRooms,
TotSize,
minSize,
MaxSize,
SizeSupplied,
sizeOK,
1 as RoomDataSupplied
FROM #ebr E
	INNER JOIN ssIDDormitories D
		ON E.bestssID = D.ssID

-- now group
Select Q.LifeYear [Survey Year],
E.surveyDimensionssID,
E.Estimate [Estimate Enrol],
E.bestYear [Year of Enrol Data],
E.offset [Age of Enrol Data],
ER.Estimate [Estimate Dorm],
ER.bestYear [Year of Dorm Data],
ER.offset [Age of Dorm Data],
EnrolM, EnrolF,
brdM, brdF,
capM, capF,
showersM, ShowersF,
toiletsM, toiletsF,
NumRooms,
TotSize,
minSize,
MaxSize,
SizeSupplied,
sizeOK,
RoomDataSupplied
from
(
Select LifeYear,
schNo,
sum(EnrolM) EnrolM,
sum(EnrolF) EnrolF,
sum(brdM) brdM,
sum(brdF) brdF,
sum(capM) capM,
sum(capF) capF,
sum(showersM) showersM,
sum(showersF) showersF,
sum(toiletsM) toiletsM,
sum(toiletsF) toiletsF,
sum(NumRooms) NumRooms,
sum(TotSize) TotSize,
sum(minSize) minSize,
sum([MaxSize]) [MaxSize],
sum(SizeSupplied) SizeSupplied,
sum(sizeOK) sizeOK,
sum(RoomDataSupplied) RoomDataSupplied
from #data D
GROUP BY LifeYear,schNo
) Q
LEFT JOIN #ebse E
	ON Q.LifeYear = E.LifeYEar and Q.schNo = E.schNo
LEFT JOIN #ebr ER
	ON Q.LifeYear = ER.LifeYEar and Q.schNo = ER.schNo

END
GO
GRANT EXECUTE ON [dbo].[sp_INDPupilDormitoryGenderData] TO [pSchoolRead] AS [dbo]
GO

