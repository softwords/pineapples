SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[convertSA_RMI]
@inspID int

AS

BEGIN

declare @debug int = 1
declare @inspDate datetime


declare @template xml

Select @template = intyTemplate
FROM lkpInspectionTypes
WHERE intyCode = 'SCHOOL_ACCREDITATION'


--- firest collecthe the hierarchy of the template xml
DECLARE @subcriteria TABLE
(
	category nvarchar(200)
	, categoryType nvarchar(200)
	, standard nvarchar(200)
	, criteria nvarchar(50)
	, subcriteria nvarchar(50)
)
INSERT INTO @subcriteria
Select
v.value('(../../../name)[1]', 'nvarchar(50)') Category
, v.value('(../../../type)[1]', 'nvarchar(50)') CategoryType
, v.value('(../../id)[1]', 'nvarchar(50)') Standard
, v.value('(../id)[1]', 'nvarchar(50)') Criteria
, v.value('id[1]', 'nvarchar(50)') SubCriteria
FROM @template.nodes('survey/category/standard/criteria/subcriteria') as V(v)

if @Debug = 1 begin
	Select * from @subcriteria
end

DECLARE @s TABLE
(
	inspID int
	, category nvarchar(100)
	, category_type nvarchar(100)
	, standard nvarchar(50)
	, criteria nvarchar(50)
	, Value int
	, MaxScore int
)

INSERT INTO @s
( inspID, category, category_type, standard, criteria, value)
Select S.*
from
(
Select
saID
, case Q.num
	when 7 then
		case N.num
			when 1 then 'Classroom Observation 1'
			when 2 then 'Classroom Observation 2'
		end
	else 'School Evaluation'
end Category
, case Q.num
	when 7 then
		'CLASSROOM_OBSERVATION'
	else 'SCHOOL_EVALUATION'
end Category_Type

, case Q.num
	when 1 then 'SE.1'
	when 2 then 'SE.2'
	when 3 then 'SE.3'
	when 4 then 'SE.4'
	when 5 then 'SE.5'
	when 6 then 'SE.6'
	else null
end standard
, case Q.Num
	when 7 then  null
	else N.num
end Criteria
, case Q.num
	when 1 then
		case N.num
			when 1 then saL1
			when 2 then saL2
			when 3 then saL3
			when 4 then saL4
		end
	when 2 then
		case N.num when 1 then saT1
					when 2 then saT2
					when 3 then saT3
					when 4 then saT4
		end
	when 3 then
		case N.num when 1 then saD1
					when 2 then saD2
					when 3 then saD3
					when 4 then saD4
		end
	when 4 then
		case N.num when 1 then saN1
					when 2 then saN2
					when 3 then saN3
					when 4 then saN4
		end
	when 5 then
		case N.num when 1 then saF1
					when 2 then saF2
					when 3 then saF3
					when 4 then saF4
		end
	when 6 then
		case N.num when 1 then saS1
					when 2 then saS2
					when 3 then saS3
					when 4 then saS4
		end
	when 7 then
		case N.num when 1 then saCO1
					when 2 then saCO2
		end
end Value

FROM SchoolAccreditation SA
INNER JOIN metaNumbers N
ON N.num between 1 and 4
INNER JOIN metaNumbers Q
ON Q.num between 1 and 7
where saID = @inspID
) S
WHERE Category is not null

-- now update the max scores
-- first at criteria level
UPDATE @s
Set MaxScore = CountSubs
FROM @s
	INNER JOIN
( Select Criteria, count(*) CountSubs
FROM @subcriteria
GROUP BY Criteria
) SC

	ON [@s].standard + '.' + [@s].criteria = SC.Criteria
-- at standard level
UPDATE @s
Set MaxScore = CountSubs
FROM @s
	INNER JOIN
( Select Standard, count(*) CountSubs
FROM @subcriteria
GROUP BY Standard
) SC
ON [@s].standard  = SC.Standard
WHERE [@s].Criteria is null

-- at category level
-- HERE IS THE BIGGSET change in that the saCO1 and saCO2
-- are scored out of 4 - we just have to 'know' this
-- because it does not relate to the new format
UPDATE @s
Set MaxScore =	4	-- literal 4  -- CountSubs
FROM @s
	INNER JOIN
( Select Category, count(*) CountSubs
FROM @subcriteria
GROUP BY Category
) SC
ON [@s].Category  = SC.Category
WHERE [@s].Standard is null


if @debug = 1 begin
	Select * from @s
	order by category, standard, criteria
end

declare @t TABLE
(
	Tag int,
	Parent int,
	inspId int,
	category nvarchar(100),
	category_type nvarchar(100),
	standard nvarchar(50),
	criteria nvarchar(50),
	score int,
	maxscore int
)

--DECLARE @sm TABLE
--(
--	s nvarchar(10)
--	, id nvarchar(10)

--)


--INSERT INTO @sm
--VALUES
--('L','SE1')
--, ('T','SE2')
--, ('D','SE3')
--, ('N','SE4')
--, ('F','SE5')
--, ('S','SE6')
--, ('CO1','CO1')
--, ('CO2','CO2')

--if @debug = 1 begin
--	SELECT * from @sm
--end

DECLARE @criteriaNames TABLE
(
	id nvarchar(10)
	, name nvarchar(500)
)

INSERT INTO @criteriaNames
SELECT DISTINCT
v.value('id[1]', 'nvarchar(20)')
, v.value('name[1]', 'nvarchar(200)')
FROM @template.nodes('survey/category/standard/criteria') as V(v)

if @debug = 1 begin
	Select * from @criteriaNames
end

DECLARE @standardNames TABLE
(
	id nvarchar(10)
	, name nvarchar(100)
)

INSERT INTO @standardNames
SELECT DISTINCT
v.value('id[1]', 'nvarchar(20)')
, v.value('name[1]', 'nvarchar(200)')
FROM @template.nodes('survey/category/standard') as V(v)

if @debug = 1 begin
	Select * from @standardNames
end


-- survey element
INSERT INTO @t
select distinct
1 Tag
, null Parent
, inspID
, null
, null
, null
, null
, null
, null
FROM @s

INSERT INTO @t
select distinct
2 Tag
, 1 Parent
, inspID
, category
, category_type
, null
, null
, value
, MaxScore
FROM @s
WHERE standard is null

-- category node does not have Score or maxScore when there is a child standard
INSERT INTO @t
select distinct
2 Tag
, 1 Parent
, inspID
, category
, category_type
, null
, null
, null
, null
FROM @s
WHERE standard is not null

INSERT INTO @t
select distinct
3 Tag
, 2 Parent
, inspID
, category
, category_type
, standard
, null
, null
, null
FROM @s
where standard is not null

INSERT INTO @t
select distinct
4 Tag
, 3 Parent
, inspID
, category
, category_type
, standard
, criteria
, Value
, MaxScore
FROM @s
where criteria is not null

if @debug = 1 begin
	select * from @t
end
declare @x xml
Select @x =
(
Select
Tag
, Parent
, inspID					[survey!1!!HIDE]
, 'SCHOOL_ACCREDITATION'	[survey!1!type!ELEMENT]
, @inspDate					[survey!1!surveyDate!ELEMENT]
, 0							[survey!1!version!ELEMENT]
, category					[category!2!!HIDE]
, category			[category!2!name!ELEMENT]
, category_type		[category!2!type!ELEMENT]
, convert(decimal(3,1), maxscore * (ptMax + ptMin) / 200)	[category!2!score!ELEMENT]
, maxscore			[category!2!maxscore!ELEMENT]
, score				[category!2!resultvalue!ELEMENT]
, P.ptName			[category!2!result!ELEMENT]
, standard			[standard!3!!HIDE]
, standard			[standard!3!id!ELEMENT]
, sn.name			[standard!3!name!ELEMENT]
, criteria			[criteria!4!!HIDE]
, CN.id				[criteria!4!id!ELEMENT]
, CN.name			[criteria!4!name!ELEMENT]
-- we now backcalculate the 'score' from the result

, convert(decimal(3,1), maxscore * (ptMax + ptMin) / 200)	[criteria!4!score!ELEMENT]
, maxscore			[criteria!4!maxscore!ELEMENT]
, score				[criteria!4!resultvalue!ELEMENT]
, P.ptName			[criteria!4!result!ELEMENT]

from @t
	LEFT JOIN @standardNames SN
		on [@t].standard = SN.id
	LEFT JOIN @criteriaNames CN
		on SN.id + '.' + [@t].criteria = CN.id
	LEFT JOIN Partitions P
		ON P.ptSet = 'SSA'
		AND P.ptValue = [@t].score
order by inspID, category, standard, criteria
for XML explicit
)

-- now update the xml on the inspection record
UPDATE SchoolInspection
	SEt inspXML = @x
	WHERE inspID = @inspID

Select @x x
END
GO

