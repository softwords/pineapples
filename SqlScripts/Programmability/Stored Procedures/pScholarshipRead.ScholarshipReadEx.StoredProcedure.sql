SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2020
-- Description:	Extended read of scholarship
-- =============================================
CREATE PROCEDURE [pScholarshipRead].[ScholarshipReadEx]
	@scholarshipID int
AS
BEGIN

	SET NOCOUNT ON;

    Select *
 	from Scholarships TI
	WHERE schoID = @scholarshipID

	-- scholarship study records
	Select * from ScholarshipStudy_
	WHERE schoID = @scholarshipID

	-- supporting documents
	Select * from pScholarshipRead.ScholarshipLinks
	WHERE schoID = @scholarshipID

	-- audit
	Select * from ScholarshipAudit_
	WHERE schoID = @scholarshipID
	ORDER BY pCreateDateTime DESC

END
GO

