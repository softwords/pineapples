SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 6 2009
-- Description:	Related to TeacherSurveyValidationList, this is the list of teachers who are best
-- candidates to teach a class in a school in a year
-- These may be:
--			-- teachers with an opptionment at this school in the year
--			-- teachers teaching at the school in the year
--			-- teachers teaching at the school in the previous year
--			-- same for the parent school
-- =============================================
CREATE PROCEDURE [pTeacherRead].[ClassTeacherSelectionList]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50),
	@srcYear int = 0,
	@targetYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  declare @ParentSchool nvarchar(50)

declare @SurveyID int
declare @ParentSurveyId int			-- survey Id at parent school

declare @EndDate datetime
declare @StartDate datetime

declare  @Tids TABLE
(
tID int
)


-- get the parent school

Select @ParentSchool = syEstablishmentPoint
From SchoolYearHistory SYH
WHERE syYear = @targetYear			-- this just point back to the current school if not an extension
AND SchNo = @schNo

-- this schools survey
Select @SurveyID = ssID
FROM SchoolSurvey
WHERE schNo = @schNo
	AND svyYear = @srcYear


Select @EndDate = common.dateserial(@TargetYear,12,31)
	   , @StartDate = common.dateserial(@TargetYear,1,1)

INSERT INTO @tIDS
(tID)
SELECT DISTINCT tID
FROM TeacherSurvey 	TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
	-- school or parent, in source or target year
WHERE schNo in (@schNo, @ParentSchool)
AND svyYear in (@SrcYear, @TargetYear)

UNION
-- any app[ointments
Select DISTINCT tID
FROM TeacherAppointment TA
INNER JOIN Establishment E
	ON TA.estpNo = E.estpNo
WHERE E.schNo in (@SchNo, @ParentSchool)
AND taDate <= @EndDate
AND (taEndDate >= @StartDate or  taEndDate is null)

UNION

-- anybody already teaching a class at the school or parent
Select DISTINCT tID
FROM ClassTeacher CT
	INNER JOIN Classes C
		ON CT.pcID = C.pcID
	INNER JOIN SchoolSurvey SS
		ON SS.ssID = C.ssID
WHERE
	schNo in (@schNo, @ParentSchool)
	AND
		svyYear in (@srcYear, @TargetYear)


Select TIDS.tID
, dbo.ReverseName(tGiven, tSurname) reverseName
, tPayroll
, tDOB
, tSex
, tFullName
, tGiven
, tSurname
, srcY.tchsID
, srcY.schNo			-- which may <> @SchNo if an appointment is a tranfers
, SrcY.tchRole
, srcY.schName
, NY.schNo NextSchNo
, NY.schName NextSchoolName
, NY.tchRole NextRole
, NY.tchsID NextTeacherSurvey
, Appts.estpNo
, Appts.estpRoleGrade
, Appts.taDate
, Appts.taEndDate

from @tIDS TIDS
INNER JOIN TeacherIdentity TI
	ON TI.tID = TIDS.tID
-- gives the teacher's whereabouts in the target year
LEFT JOIN (Select tID, tchsID, SS.schNo, schName, tchRole
				from TeacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				INNER JOIN Schools S
					ON SS.schNo = S.schNo
				WHERE SS.svyYEar = @TargetYear
			) NY
	ON NY.tID = TIDS.tID
-- teadchers' appointment in the target year
LEFT JOIN (Select tID
			, E.estpNo
			, E.estpRoleGrade
			, TA.taDate
			, TA.taEndDate
			FROM TeacherAppointment TA
			INNER JOIN Establishment E
				ON TA.estpNo = E.estpNo
			WHERE E.schNo in (@schNo, @ParentSchool)
				AND taDate <= @EndDate
				AND (taEndDate >= @StartDate or  taEndDate is null)
			) Appts

	ON Appts.tID = TIDS.tID
-- teacher's whereabouts in the source year
LEFT JOIN (Select tID, TS.tchsID, SS.schNo, schName, tchRole
				from TeacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				INNER JOIN Schools S
					ON SS.schNo = S.schNo
				WHERE SS.svyYEar = @SrcYear

			) SrcY
	ON srcY.tID = TIDS.tID
ORDER BY tSurname ASC, tGiven ASC
END
GO

