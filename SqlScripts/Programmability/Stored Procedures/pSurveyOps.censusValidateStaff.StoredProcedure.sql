SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Validate table of teacher data from ndoe (fsm) excel workbook
-- returns a table of validation errors
-- if this table is present in the response to the upload, the data is not processed
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusValidateStaff]
@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- totals by education level

declare @ndoeStaff TABLE (
	rowIndex int
--    , SchoolYear                 nvarchar(100) NULL
--    , State                      nvarchar(100) NULL
    , School_Name                nvarchar(100) NULL
    , School_No                  nvarchar(100) NULL
--    , School_Type                nvarchar(100) NULL
    , Office                     nvarchar(100) NULL
    , First_Name                 nvarchar(100) NULL
    , Middle_Name                nvarchar(100) NULL
    , Last_Name                  nvarchar(100) NULL
    , Full_Name                  nvarchar(100) NULL
    , Gender                     nvarchar(100) NULL
    , inDate_of_Birth            nvarchar(100) NULL
	, Age                        nvarchar(100) NULL
    , Citizenship                nvarchar(100) NULL		-- => tchCitizenship
    , Ethnicity                  nvarchar(100) NULL		--
    , FSM_SSN                    nvarchar(100) NULL		-- => tPayroll
    , OTHER_SSN                  nvarchar(100) NULL		-- => tchProvident ?
    , Highest_Qualification      nvarchar(100) NULL
    , Field_of_Study                           nvarchar(100) NULL
    , Year_of_Completion                       nvarchar(100) NULL
    , Highest_Ed_Qualification                 nvarchar(100) NULL
    , Year_Of_Completion2                      nvarchar(100) NULL
    , Employment_Status                        nvarchar(100) NULL
    , Reason                     nvarchar(100) NULL
    , Job_Title                  nvarchar(100) NULL
    , Organization               nvarchar(100) NULL					-- to tchSponsor, used for organisation that employs/pays the teacher
    , Staff_Type                 nvarchar(100) NULL
    , Teacher_Type               nvarchar(100) NULL			--- send to tchStatus, which has tradionally been used for e.g. 'Probationary' TRainee, etc
    , inDate_of_Hire             nvarchar(100) NULL
    , inDate_Of_Exit             nvarchar(100) NULL
    , Annual_Salary              nvarchar(100) NULL
    , Funding_Source             nvarchar(100) NULL
	, PRK						 int NULL
    , ECE                        int NULL
    , Grade_1                    int NULL
    , Grade_2                    int NULL
    , Grade_3                    int NULL
    , Grade_4                    int NULL
    , Grade_5                    int NULL
    , Grade_6                    int NULL
    , Grade_7                    int NULL
    , Grade_8                    int NULL
    , Grade_9                    int NULL
    , Grade_10                   int NULL
    , Grade_11                   int NULL
    , Grade_12                   int NULL
	, Grade_13                   int NULL
	, Grade_14                   int NULL
	, Grade_15                   int NULL
    , Admin                      int NULL
    , Other                      int NULL
    , Total_Days_Absence         nvarchar(20) NULL			-- collect nvarchar and validate it later
    , Maths                      int NULL
    , Science                    int NULL
    , Language                   int NULL
    , Competency                 int NULL
    , fileReference				 uniqueidentifier
	, rowXml					 xml					-- this stores the entire raw xml of the row for posterity
	--
	, ssID						 int
	, tID						 int
	--
    , Date_of_Birth              date NULL
    , Date_of_Hire               date NULL
    , Date_Of_Exit               date NULL
	, minYearTaught			     int
	, maxYearTaught				 int
	, classMin					 nvarchar(10)
	, classMax					 nvarchar(10)
	, TAM						 nvarchar(1)		-- teacher / admin / mixed => tchTAM
	, teacherRole				 nvarchar(50)		-- from job title, via lkpTeacherRole => tchRole
	, sector					 nvarchar(10)		-- calculated from the school type, and the levels taught => tchSector
)

declare @validations TABLE
(
	field nvarchar(50),
	errorValue nvarchar(100),
	valMsg nvarchar(200),
	severity nvarchar(20)
	, NumRows int
	, FirstRow int
	, LastRow int
)
-- use try / catch
begin try

declare @counter int
declare @debug int = 1

declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8

declare @firstRow int

-- startpos = 8
Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
, @firstRow = v.value('@FirstRow', 'int')
From @xml.nodes('ListObject') as V(v)

/*-----------------------------------------------------------
VALIDATIONS

validation errors will be reported back to the client, and no processing will take place
*/-----------------------------------------------------------

INSERT INTO @ndoeStaff
(
rowIndex
--, SchoolYear
--, State
, School_Name
, School_No
--, School_Type
, Office
, First_Name
, Middle_Name
, Last_Name
, Full_Name
, Gender
, inDate_of_Birth
, Age
, Citizenship
, Ethnicity
, FSM_SSN
, OTHER_SSN
, Highest_Qualification
, Field_of_Study
, Year_of_Completion
, Highest_Ed_Qualification
, Year_Of_Completion2
, Employment_Status
, Reason
, Job_Title
, Organization
, Staff_Type
, Teacher_Type
, inDate_of_Hire
, inDate_Of_Exit
, Annual_Salary
, Funding_Source
, PRK
, ECE
, Grade_1
, Grade_2
, Grade_3
, Grade_4
, Grade_5
, Grade_6
, Grade_7
, Grade_8
, Grade_9
, Grade_10
, Grade_11
, Grade_12
, Grade_13
, Grade_14
, Grade_15
, Admin
, Other
, Total_Days_Absence
, Maths
, Science
, Language
, Competency
, filereference
, rowXml
)
SELECT
nullif(ltrim(v.value('@Index', 'int')),'')                           [Index]

--, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                           [SchoolYear]
--, nullif(ltrim(v.value('@District', 'nvarchar(100)')),'')                              [State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                        [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                          [School_No]
--, nullif(ltrim(v.value('@Authority_Govt', 'nvarchar(100)')),'')                     [School_Type]
, nullif(ltrim(v.value('@Office', 'nvarchar(100)')),'')                             [Office]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                         [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                        [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                          [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                          [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')                             [Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')                      [Date_of_Birth]
, nullif(ltrim(v.value('@Age', 'nvarchar(100)')),'')                                [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                        [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                          [Ethnicity]
, nullif(ltrim(v.value('@FSM_SSN', 'nvarchar(100)')),'')                            [FSM_SSN]
, nullif(ltrim(v.value('@OTHER_SSN', 'nvarchar(100)')),'')                          [OTHER_SSN]
, nullif(ltrim(v.value('@Highest_Qualification', 'nvarchar(100)')),'')              [Highest_Qualification]
, nullif(ltrim(v.value('@Field_of_Study', 'nvarchar(100)')),'')                     [Field_of_Study]
, nullif(ltrim(v.value('@Year_of_Completion', 'nvarchar(100)')),'')                 [Year_of_Completion]
, nullif(ltrim(v.value('@Highest_Ed_Qualification', 'nvarchar(100)')),'')           [Highest_Ed_Qualification]
, nullif(ltrim(v.value('@Year_Of_Completion2', 'nvarchar(100)')),'')                [Year_Of_Completion2]
, nullif(ltrim(v.value('@Employment_Status', 'nvarchar(100)')),'')                  [Employment_Status]
, nullif(ltrim(v.value('@Reason', 'nvarchar(100)')),'')                             [Reason]
, nullif(ltrim(v.value('@Job_Title', 'nvarchar(100)')),'')                          [Job_Title]
, nullif(ltrim(v.value('@Organization', 'nvarchar(100)')),'')                       [Organization]
, nullif(ltrim(v.value('@Staff_Type', 'nvarchar(100)')),'')                         [Staff_Type]
, nullif(ltrim(v.value('@Teacher_Type', 'nvarchar(100)')),'')                       [Teacher_Type]
, nullif(ltrim(v.value('@Date_of_Hire', 'nvarchar(100)')),'')                       [Date_of_Hire]
, nullif(ltrim(v.value('@Date_of_Exit', 'nvarchar(100)')),'')                       [Date_Of_Exit]
, nullif(ltrim(v.value('@Annual_Salary', 'nvarchar(100)')),'')                      [Annual_Salary]
, nullif(ltrim(v.value('@Funding_Source', 'nvarchar(100)')),'')                     [Funding_Source]

, case when ltrim(isnull(v.value('@PRK', 'nvarchar(10)'),'')) > '' then 1 else 0 end PRK
, case when ltrim(isnull(v.value('@ECE', 'nvarchar(10)'),'')) > '' then 1 else 0 end ECE
, case when ltrim(isnull(v.value('@Grade_1', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_1
, case when ltrim(isnull(v.value('@Grade_2', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_2
, case when ltrim(isnull(v.value('@Grade_3', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_3
, case when ltrim(isnull(v.value('@Grade_4', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_4
, case when ltrim(isnull(v.value('@Grade_5', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_5
, case when ltrim(isnull(v.value('@Grade_6', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_6
, case when ltrim(isnull(v.value('@Grade_7', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_7
, case when ltrim(isnull(v.value('@Grade_8', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_8
, case when ltrim(isnull(v.value('@Grade_9', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_9
, case when ltrim(isnull(v.value('@Grade_10', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_10
, case when ltrim(isnull(v.value('@Grade_11', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_11
, case when ltrim(isnull(v.value('@Grade_12', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_12
, case when ltrim(isnull(v.value('@Grade_13', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_13
, case when ltrim(isnull(v.value('@Grade_14', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_14
, case when ltrim(isnull(v.value('@Grade_15', 'nvarchar(10)'),'')) > '' then 1 else 0 end Grade_15

, case when ltrim(isnull(v.value('@Admin', 'nvarchar(10)'),'')) > '' then 1 else 0 end Admin
, case when ltrim(isnull(v.value('@Other', 'nvarchar(10)'),'')) > '' then 1 else 0 end Other

, nullif(ltrim(v.value('@Total_Days_Absence', 'nvarchar(20)')),'')                      [Total_Days_Absence]
, nullif(ltrim(v.value('@Maths', 'int')),'')											[Maths]
, nullif(ltrim(v.value('@Science', 'int')),'')											[Science]
, nullif(ltrim(v.value('@Language', 'int')),'')                                         [Language]
, nullif(ltrim(v.value('@Competency', 'int')),'')                                       [Competency]
, null --@filereference
, v.query('.')																			[rowXml]
from @xml.nodes('ListObject/row') as V(v)

print 'after load'

----- DEal with the DOC staff up front
----- Delete them!
----- to do - further discussion about handling these

-- find a better way to do this...
-- the teacher location on this page is a 'pay point', more general than a school

DELETE
FROM @ndoeStaff
WHERE School_No like '%%%DOE'

-- decode DoB and gender
UPDATE @ndoeStaff
SET Date_Of_Birth =
	case ISNUMERIC(inDate_Of_Birth)
		when 0 then	case isdate(inDate_Of_Birth) when 1 then convert(date, inDate_Of_Birth) else null end
		when 1 then cast( convert(float, inDate_Of_Birth) - 2 as datetime)
	end
, Date_Of_Hire =
	case ISNUMERIC(inDate_Of_Hire)
		when 0 then	case isdate(inDate_Of_Hire) when 1 then convert(date, inDate_Of_Hire) else null end
		when 1 then cast( convert(float, inDate_Of_Hire) - 2 as datetime)
	end
, Date_Of_Exit =
	case ISNUMERIC(inDate_Of_Exit)
		when 0 then	case isdate(inDate_Of_Exit) when 1 then convert(date, inDate_Of_Exit) else null end
		when 1 then cast( convert(float, inDate_Of_Exit) - 2 as datetime)
	end
, Gender = case Gender when 'Male' then 'M' when 'Female' then 'F' else Gender end


--- first off, check that there is a survey control record for year @SurveyYear
Select @SurveyYear = svyYear
FROM Survey
WHERE svyYEar = @SurveyYear

if (@@RowCount = 0) begin
	INSERT INTO @validations
	(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Survey Year'
, @SurveyYear
, 'No survey record created for this survey year'
, 'Critical'
,  1
, null
, null

	print 'No survey record created for survey year ' + cast(@SurveyYear as nvarchar(10))
end


if @DistrictName is null or @DistrictName = 'ALL'
	Select @DistrictID = 'ALL'
else
	Select @districtID = dID
	from Districts WHERE dName = @districtName

if @DistrictID is null begin
INSERT INTO @validations
	(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'District'
, @districtName
, 'Name not valid'
, 'Critical'
,  1
, null
, null
	print 'District name not available ' + cast(@SurveyYear as nvarchar(10))
end


print @SurveyYear
print @DistrictID
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'School'
, School_No
, 'School not in ' + @DistrictName
, 'Critical'
, count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeStaff ndoe
WHERE
School_No not in
	(Select schNo from Schools
	 INNER JOIN Islands I
	 ON Schools.iCode = I.iCode
 		WHERE I.iGroup = @districtID
		)
-- allow for a full national upload by setting the workbook district to 'ALL'
and @districtID <> 'ALL'
and School_No in (Select schNo from Schools)
GROUP BY School_No
HAVING count(*) > 0

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Highest Qualification'
, highest_qualification
, ' undefined code'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeStaff ndoe
	LEFT JOIN lkpTeacherQual Q
		ON NDOE.Highest_Qualification in (codeCode, codeDescription)
WHERE highest_qualification is not null
AND Highest_Qualification <> 'None'			-- none is a special value
AND Q.codeCode is null
GROUP BY highest_qualification

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Highest Qualification'
, highest_qualification
, ' Classified as an Education Qualification (codeTeach=1)'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherQual Q
		ON NDOE.Highest_Qualification in (codeCode, codeDescription)
WHERE highest_qualification is not null
AND Highest_Qualification <> 'None'
AND Q.codeTeach = 1
GROUP BY highest_qualification

--- check any teacher qualification values
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Highest Ed Qualification'
, highest_ed_qualification
, ' undefined code'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoeStaff ndoe
	LEFT JOIN lkpTeacherQual Q
		ON NDOE.Highest_ed_Qualification in (codeCode, codeDescription)
WHERE highest_ed_qualification is not null
AND Highest_Ed_Qualification <> 'None'
AND Q.codeCode is null
GROUP BY highest_ed_qualification

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Highest Ed Qualification'
, highest_ed_qualification
, ' not classified as an Education Qualification (codeTeach=0)'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)
FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherQual Q
		ON NDOE.Highest_ed_Qualification in (codeCode, codeDescription)
WHERE highest_ed_qualification is not null
AND Highest_Ed_Qualification <> 'None'
AND Q.codeTeach = 0
GROUP BY highest_ed_qualification

-- citizenship, ethinicity and gender, role, field_of_study should get validated

---------------------
-- Role/Grade table:RoleGrades
---------------------
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Teacher Role'
, Job_Title
, ' not found in teacher role/grade'
, 'Information'
,  count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoeStaff ndoe
	LEFT JOIN RoleGrades R
	-- some aliases from legacy data added into rgDescriptionL1, rgDescriptionL2
		ON NDOE.Job_Title in (R.rgCode, R.rgDescription, R.rgDescriptionL1, R.rgDescriptionL2)
WHERE job_title is not null
AND job_title <> 'None'
AND R.rgCode is null
GROUP BY Job_Title

-----------------------------------
--- FieldOfStudy lkpSubjects
-----------------------------------
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Field Of Study'
, Field_Of_Study
, ' not found in Subjects table'
, 'Information'
,  count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoeStaff ndoe
	LEFT JOIN lkpSubjects SUBJ
	-- some aliases from legacy data added into rgDescriptionL1, rgDescriptionL2
		ON NDOE.Field_Of_Study in (SUBJ.subjCode, SUBJ.subjName, SUBJ.subjNameL1, SUBJ.subjNameL2)
WHERE Field_Of_Study is not null
AND Field_Of_Study <> 'None'
AND SUBJ.subjCode is null
GROUP BY Field_Of_Study

-----------------------------------
--- Citizenship lkpNationality
-----------------------------------
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Citizenship'
, Citizenship
, ' not found in Nationality table'
, 'Information'
,  count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoeStaff ndoe
	LEFT JOIN lkpNationality N
	-- some aliases from legacy data added into rgDescriptionL1, rgDescriptionL2
		ON NDOE.Citizenship in (N.codeCode, N.codeDescription, N.codeDescriptionL1, N.codeDescriptionL2)
WHERE Citizenship is not null
AND N.codeCode is null
GROUP BY Citizenship

-----------------------------------
--- Ethnicity lkpEthnicity
-----------------------------------
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
Select
'Ethnicity'
, Ethnicity
, ' not found in Ehtnicity table'
, 'Information'
,  count(*)
, min(rowIndex)
, max(rowIndex)

FROM @ndoeStaff ndoe
	LEFT JOIN lkpEthnicity N
	-- some aliases from legacy data added into rgDescriptionL1, rgDescriptionL2
		ON NDOE.Ethnicity in (N.codeCode, N.codeDescription, N.codeDescriptionL1, N.codeDescriptionL2)
WHERE Ethnicity is not null
AND N.codeCode is null
GROUP BY Ethnicity

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Duplicate Row'
, first_name + ' ' + last_name
, 'duplicate must be removed before loading'
, 'Information'
,  count(*)
, min(rowIndex)
, max(rowIndex)
From @ndoeStaff ndoe
WHERE employment_status = 'Active'
GROUP BY school_No
, first_name
, last_name
, inDate_of_Birth
-- , Job_Title -- treat as a dup even if different roles
HAVING count(*) > 1

-- different school, but the same teacher
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Multiple Teacher Record'
, first_name + ' ' + last_name
, 'duplicate row must be resolved before loading'
, 'Information'
,  count(*)
, min(rowIndex)
, max(rowIndex)
From @ndoeStaff ndoe
WHERE employment_status = 'Active'
GROUP BY first_name
, last_name
, inDate_of_Birth
--, isnull(FSM_SSN,'')  -- omit this test on FSM_SSN
HAVING count(*) > 1

-- need some validations against teachers that may already be on file
-- these would have to match the teacher info, according to the critieria in loadStaff sp
-- but be at a school NOT in this workbook (becuase those will get recreated)

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Existing Teacher Record'
, first_name + ' ' + last_name
, 'Already recorded at ' + TS.schNo + ' in ' + cast(@SurveyYear as nvarchar(4))
, 'Information'
, 1
, (rowIndex)
, (rowIndex)
From @ndoeStaff ndoe
	INNER JOIN pTeacherRead.TeacherSurveyV TS
		ON ndoe.First_Name = TS.tGiven
		AND ndoe.Last_Name = TS.tSurname
		AND ndoe.Date_of_Birth = TS.tDoB
		AND TS.svyYear = @SurveyYear
WHERE TS.schNo not in (Select schNo from @ndoeStaff WHERE schNo is not null)


-- check for malformed dates
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Date of Birth'
, inDate_Of_Birth
, 'invalid date'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE inDate_Of_Birth is not null
AND isnumeric(inDate_Of_Birth) = 0
AND isdate(inDate_Of_Birth) = 0

-- dates cannot include time - this willl appear as a number value with a fraction e.g. 23089.44
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Date of Birth'
, cast(convert(float, inDate_Of_Birth) - 2 as datetime)
, 'date must not include time'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE inDate_Of_Birth is not null
AND isnumeric(inDate_Of_Birth) = 1
AND (inDate_Of_Birth - floor(inDate_Of_Birth)) <> 0

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Date of Hire'
, inDate_Of_Hire
, 'invalid date'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE inDate_Of_Hire is not null
AND isnumeric(inDate_Of_Hire) = 0
AND isdate(inDate_Of_Hire) = 0

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Date of Hire'
, cast(convert(float, inDate_Of_Hire) - 2 as datetime) -- this will show the data and time
, 'date must not include time'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE inDate_Of_Hire is not null
AND isnumeric(inDate_Of_Hire) = 1
AND (inDate_Of_Hire - floor(inDate_Of_Hire)) <> 0

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Date of Exit'
, inDate_Of_Exit
, 'invalid date'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE inDate_Of_Exit is not null
AND isnumeric(inDate_Of_Exit) = 0
AND isdate(inDate_Of_Exit) = 0


-- Funding Source must be present
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Funding Source'
, Funding_Source
, 'Funding Source missing'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)
From @ndoeStaff ndoe
WHERE Funding_Source is null
GROUP BY Funding_Source


-- annual salary is loaded as nvarchar(100)
-- confirm it is numeric
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Annual Salary'
, Annual_Salary
, 'Salary missing'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)
From @ndoeStaff ndoe
WHERE Annual_Salary is null and isnull(Funding_Source,'') not in ('Other')
GROUP BY Annual_Salary


-- confirm it is numeric
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Annual Salary'
, Annual_Salary
, 'Salary must be numeric, 0, 1 or 2 decimal places'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Annual_Salary is not null and isnumeric(Annual_Salary) = 0

-- confirm it is numeric
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Annual Salary'
, Annual_Salary
, 'Salary must be numeric, 0, 1 or 2 decimal places'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Annual_Salary is not null and isnumeric(Annual_Salary) = 1
AND convert(decimal(13,5) , convert(decimal(8,2), Annual_Salary)) <>   convert(decimal(13,5), Annual_Salary)

-- Total Days Absence
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Total Days Absence'
, Total_Days_Absence
, 'Total Days Absence not recorded'
, 'Critical'
,  count(*)
, min(rowIndex)
, max(rowIndex)
From @ndoeStaff ndoe
WHERE Total_Days_Absence is null
GROUP BY Total_Days_Absence


INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Total Days Absence'
, Total_Days_Absence
, 'Total Days Absence must be numeric, 0, 1 or 2 decimal places'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Total_Days_Absence is not null and isnumeric(Total_Days_Absence) = 1
AND convert(decimal(13,5) , convert(decimal(8,2), Total_Days_Absence)) <>   convert(decimal(13,5), Total_Days_Absence)

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Total Days Absence'
, Total_Days_Absence
, 'Total Days Absence must be numeric, 0, 1 or 2 decimal places'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Total_Days_Absence is not null and isnumeric(Total_Days_Absence) = 0

-- some validations on activities:

-- non-teaching staff cannot specify a grade level
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Staff Type'
, Staff_Type
, 'Non-teaching staff cannot teach a Grade Level. Remove the Grades, or change to Teaching Staff'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Staff_Type like 'Non%teaching staff' and
(PRK + ECE + Grade_1 + Grade_2 + Grade_3 + Grade_4 + Grade_5 + Grade_6 + Grade_7 + Grade_8 + Grade_9 +
		Grade_10 + Grade_11+ Grade_12 + Grade_13 + Grade_14 + Grade_15) > 0

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Staff Type'
, Staff_Type
, 'Non-teaching staff must record Admin or Other duties'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Staff_Type like 'Non%teaching staff' and
(Admin + Other = 0)

-- non-teaching staff cannot specify a grade level
INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Staff Type'
, Staff_Type
, 'Staff with Admin or Other duties, but no grade level duties cannot be Teaching Staff. Specify a grade level, change the staff type or remove the Admin/Other duties'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Staff_Type = 'Teaching staff' and
(PRK + ECE + Grade_1 + Grade_2 + Grade_3 + Grade_4 + Grade_5 + Grade_6 + Grade_7 + Grade_8 + Grade_9 +
		Grade_10 + Grade_11+ Grade_12 + Grade_13 + Grade_14 + Grade_15) = 0
AND (Admin + Other = 1)

INSERT INTO @validations
(
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow
	, LastRow
)
SELECT
'Staff Type'
, Staff_Type
, 'Staff Type missing'
, 'Critical'
,  1
, rowIndex
, rowIndex
From @ndoeStaff ndoe
WHERE Staff_Type is null

print 'at end'

Select
	field
	, errorValue
	, valMsg
	, severity
	, NumRows
	, FirstRow  + @FirstRow FirstRow
	, LastRow + @FirstRow   LastRow
from @Validations

end try

begin catch
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

