SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Data used by PIVSurveyControl pivot table
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVSurveyControlData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT SurveyControl.saYear AS [Survey Year],
 Survey.svyCensusDate AS CensusDate,
Survey.svyCollectionDate AS CollectionDate,
SurveyControl.schNo AS School,
Schools.schName AS [Name of School],
SurveyControl.saSent AS SentDate,
--IIf(IsNull([sentdate]),0,1) AS Sent,
case when saSent is null then 0 else 1 end Sent,

-- collected
saCollected AS CollectedDate,
case when saCollected is null then 0 else 1 end Collected,   --IIf(IsNull([Collecteddate]),0,1) AS Collected,
case when saCollected <= svyCollectionDate then 1 else 0 end CollectedOnTime, --IIf([CollectedDate]<=[svyCollectionDate],1,0) AS CollectedOnTime,

-- received
saReceived AS ReceivedDate,
convert(nvarchar(7), saReceived , 120) as ReceivedDateYYYYMM, -- Format([saReceived],"yyyy-mm") AS ReceivedDateYYYYMM,
-- Format([saReceived],"yyyy-ww") AS ReceivedDateYYYYww,
case when saReceived is null then 0 else 1 end Received,
case when saReceived <= saReceivedTarget then 1 else 0 end ReceivedOnTime, -- IIf([ReceivedDate]<=[saReceivedTarget],1,0) AS ReceivedOnTime,
case when saReceived>saReceivedTarget then datediff(d,saReceivedTarget, saReceived) else null end ReceivedDaysLate, -- IIf([ReceivedDate]>[saReceivedTarget],[ReceivedDate]-[saReceivedTarget],Null) AS ReceivedDaysLate,
datediff(d,saSent, saReceived) AS ReceivedDaysSinceSent,  --[saReceived]-[saSent] AS ReceivedDaysSinceSent,

-- entered
saEntered AS EnteredDate,
convert(nvarchar(7), saEntered , 120) AS EnteredDateYYYYMM,
-- Format([saEntered],"yyyy-ww") AS EnteredDateYYYYww,
case when saEntered is null then 0 else 1 end Entered,
datediff(d,saSent, saEntered) AS EnteredDaysSinceSent,
datediff(d,saReceived, saEntered) AS EnteredDaysSinceReceived,
--audit
case when saAudit = 1 then 1 else 0 end as AuditReq,
saAuditDate AS AuditedDate,
case when saAuditDate is null then 0 else 1 end Audited,
saAuditResult AS AuditResult,
saFileLocn AS FileLocn,
ebse.surveyDimensionssID
FROM Schools INNER JOIN
Survey INNER JOIN SurveyControl ON Survey.svyYear = SurveyControl.saYear
LEFT JOIN #ebse ebse
ON (SurveyControl.saYear = ebse.LifeYear) AND (SurveyControl.schNo = ebse.schNo)
ON Schools.schNo = SurveyControl.schNo

drop table #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVSurveyControlData] TO [pSurveyRead] AS [dbo]
GO

