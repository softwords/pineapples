SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 9 2022
-- Description:	Copy standards benchmarks and indicators from an earlier instance of the same exam type
-- =============================================
CREATE PROCEDURE [pExamWrite].[CloneExamFrame]
	-- Add the parameters for the stored procedure here
	@examCode nvarchar(20)
	, @fromYear int
	, @toYear int
	, @returnResults int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
begin try
begin transaction


declare @toexID int
declare @fromexID int

declare @toN int
declare @fromN int
declare @distinctN int

declare @SEVERITY_INFO int = 0
declare @SEVERITY_NOT_FOUND int = 11
declare @SEVERITY_GENERAL int = 16

if @examCode is null begin
	raiserror('Exam code not specified: %s', @SEVERITY_NOT_FOUND, 1, @examCode)
end

if @toYear is null begin
	raiserror('Target year not specified: %i', @SEVERITY_NOT_FOUND, 1, @toYear)
end

if @fromYear is null begin
	raiserror('Source year not specified: %i', @SEVERITY_NOT_FOUND, 1, @fromYear)
end


Select @toexID = exID
from Exams WHERE exCode = @examCode and exYear = @toYear

Select @fromexID = exID
from Exams WHERE exCode = @examCode and exYear = @fromYear


---Select @examCode, @toYear toYear, @fromYear fromYear, @toexID toExID, @fromexID fromExID

if @toExID is null begin
	raiserror('Target exam not found: %s %i', @SEVERITY_NOT_FOUND, 1, @examCode, @toYear)
end

if @fromExID is null begin
	raiserror('Source exam not found: %s %i', @SEVERITY_NOT_FOUND, 1, @examCode, @fromYear)
end


-- test standards
Select @toN = sum(case when exID = @toexID then 1 end)
, @fromN = sum(case when exID = @fromexID then 1 end)
, @distinctN = count(Distinct exstdCode)
from ExamStandards
where exID in (@toexID, @fromexID)

if @toN  <> @fromN or @toN <> @distinctN begin
	raiserror('Standards do not align: Target count=%i: Source Count=%i : Distinct Count=%i', @SEVERITY_GENERAL, 1, @toN, @fromN, @distinctN)
end


-- test benchmarks
Select @toN = sum(case when exID = @toexID then 1 end)
, @fromN = sum(case when exID = @fromexID then 1 end)
, @distinctN = count(Distinct exbnchCode)
from ExamBenchmarks
where exID in (@toexID, @fromexID)

if @toN  <> @fromN or @toN <> @distinctN begin
	raiserror('Benchmarks do not align: Target count=%i: Source Count=%i : Distinct Count=%i', @SEVERITY_GENERAL, 1, @toN, @fromN, @distinctN)
end


-- test indicators
Select @toN = sum(case when exID = @toexID then 1 end)
, @fromN = sum(case when exID = @fromexID then 1 end)
, @distinctN = count(Distinct exindCode )
from ExamIndicators
where exID in (@toexID, @fromexID)

if @toN  <> @fromN or @toN <> @distinctN begin
	raiserror('Indicators do not align: Target count=%i: Source Count=%i : Distinct Count=%i', @SEVERITY_GENERAL, 1, @toN, @fromN, @distinctN)
	return
end

-- test achievement levels, however do not abort if we cannot copy these safely

declare @CopyAchievementLevels int = 1

Select @toN = sum(case when exID = @toexID then 1 end)
, @fromN = sum(case when exID = @fromexID then 1 end)
, @distinctN = count(Distinct exalVal )
from ExamAchievementLevels
where exID in (@toexID, @fromexID)

if @toN  <> @fromN or @toN <> @distinctN begin
	Select @CopyAchievementLevels = 0
	raiserror('Achievement levels do not align and will not be copied: Target count=%i: Source Count=%i : Distinct Count=%i', @SEVERITY_INFO, 1, @toN, @fromN, @distinctN)
end
---------------------------------
-- end validations
---------------------------------

-- set up standards
print 'Cloning standards...'
-- reset, for testing / demonstration purposes


UPDATE ExamStandards
SET exstdDescription = exstdCode
WHERE exID = @toExID

UPDATE ExamStandards
SET exstdDescription = F.exstdDescription
FROM examStandards
INNER JOIN examStandards F
	ON examStandards.exstdCode = F.exstdCode
	AND examStandards.exID = @toExID
	AND F.exID = @fromExID

-- benchmarks
print 'Cloning benchmarks...'
-- reset, for testing / demonstration purposes

UPDATE ExamBenchmarks
SET exbnchDescription = exbnchCode
WHERE exID = @toExID

-- copy from source
UPDATE ExamBenchmarks
SET exbnchDescription = F.exbnchDescription
FROM ExamBenchmarks
INNER JOIN ExamBenchmarks F
	ON ExamBenchmarks.exbnchCode = F.exbnchCode
	AND ExamBenchmarks.exID = @toExID
	AND F.exID = @fromExID

-- indicators
print 'Cloning indicators...'

UPDATE ExamIndicators
SET exindDescription = F.exindDescription
FROM ExamIndicators
INNER JOIN ExamIndicators F
	ON ExamIndicators.exindCode = F.exindCode
	AND ExamIndicators.exID = @toExID
	AND F.exID = @fromExID

if @CopyAchievementLevels = 1 begin
	print 'Cloning achievement levels...'

	UPDATE ExamAchievementLevels
	SET exalDescription = F.exalDescription
	FROM ExamAchievementLevels
	INNER JOIN ExamAchievementLevels F
		ON ExamAchievementLevels.exalVal = F.exalVal
		AND ExamAchievementLevels.exID = @toExID
		AND F.exID = @fromExID
end

if @returnResults > 0 begin
	Select *
	from pExamRead.ExamFrames
	WHERE ExamID = @toExID
end
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()
	if @@trancount = 1
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end
	if @@trancount > 1
		begin
			commit transaction
			select @errorMessage = @errorMessage + ' *** Internal transaction was committed - enclosing transaction responsible for rollback.'
		end


    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit
if @@trancount > 0
	commit transaction

return


END
GO

