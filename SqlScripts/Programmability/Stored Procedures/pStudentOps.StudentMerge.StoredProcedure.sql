SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 6 2020
-- Description:	Student Merge
--					SourceID is deleted
-- =============================================
CREATE PROCEDURE [pStudentOps].[StudentMerge]
	-- Add the parameters for the stored procedure here
	@TargetID uniqueidentifier = null,
	@SourceID uniqueidentifier = null
WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	begin transaction

	-- there may be 2 student enrolment records in the one year
	-- if so, we'll get a duplicate key
	-- one has to go
	declare @counter int = 0
	declare @DupInYear TABLE
	(
		stueYear int
		, SourcestueID int NULL
		, TargetstueID int NULL
	)

	INSERT INTO @DupInYear
	SELECT stueYear
		, min(case when stuID = @SourceID then stueID else null end) SourcestueID
		, min(case when stuID = @TargetID then stueID else null end) TargetstueID

	FROM StudentEnrolment_
	WHERE stuID in (@SourceID, @TargetID)
	GROUP BY stueYear

	-- now delete the source's StudentEnrolment where there is a dup in the same year
	DELETE FROM StudentEnrolment_
	FROM StudentEnrolment_
		INNER JOIN @DupInYear
			ON sourcestueID = StudentEnrolment_.stueID
	WHERE TargetstueID is not null

	-- whatever remains can safely be moved
	UPDATE StudentEnrolment_
		SET stuID = @TargetID
		WHERE stuID = @SourceID

---- now deal with scholarships
	declare @DupScholInYear TABLE
	(
		schrYear int
		, scholCode nvarchar(10)
		, SourceschoID int NULL
		, TargetschoID int NULL
		, countStudies	int NULL
	)
	INSERT INTO @DupScholInYear
	SELECT schrYear
		, scholCode
		, min(case when stuID = @SourceID then Scholarships.schoID else null end) SourcestueID
		, min(case when stuID = @TargetID then Scholarships.schoID else null end) TargetstueID
		-- this will be > 0 if the sourCeID has more Study Records than the target
		, sum(case when posID is null then null
				else
					case when stuID = @SourceID then 1 when stuID = @targetID then -1 else 0 end
				end) CountStudies
		FROM Scholarships		-- the view, not the table
		LEFT JOIN ScholarshipStudy_ SS
			ON scholarships.schoID = SS.schoID
	WHERE stuID in (@SourceID, @TargetID)
	GROUP BY schrYear
	, scholCode
	-- in each pair of scholarships, delete the one with the least study records

	-- first move any documents attached to the source scholarship to the target scholarship
	UPDATE DocumentLinks_
		SET schoID = targetSchoID
		, stuID = @TargetID
	FROM DocumentLinks_
		INNER JOIN @DupScholInYear D
			ON DocumentLinks_.schoID = D.SourceschoID
	WHERE SourceschoID is not null and targetSchoID is not null
	if @@rowcount > 0
		raiserror('%i Scholarships Documents moved', 0,1,@@rowcount)

	DELETE FROM Scholarships_
	WHERE schoID in
	(
		Select case when CountStudies >= 0 then targetSchoID
			when countStudies < 0 then sourceSchoID
			when sourceschoID > targetSchoId then targetSchoID
			else sourceSchoID end ID
		FROM @DupScholInYear D
		WHERE SourceschoID is not null and targetSchoID is not null
	)
	if @@rowcount > 0
		raiserror('%i Scholarships deleted', 0,1,@@rowcount)

	UPDATE Scholarships_
		SET stuID = @TargetID
		WHERE stuID = @sourceID
	if @@rowcount > 0
		raiserror('%i Scholarships updated', 0,1,@@rowcount)


	-- move any student documents --
	UPDATE DocumentLinks_
		SET stuID = @TargetID
		WHERE stuID = @sourceID

	-- move any examination candidate records
	-- to do: check these for duplicates?
	UPDATE ExamCandidates
		SET stuID = @TargetID
		WHERE stuID = @sourceID

	--delete the Source
	DELETE from Student_
		WHERE stuID = @SourceID


	commit transaction


end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

