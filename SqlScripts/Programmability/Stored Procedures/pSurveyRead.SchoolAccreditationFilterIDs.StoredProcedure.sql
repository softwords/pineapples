SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	Filter school accreditation IDs
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SchoolAccreditationFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@SAID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50) = null, --'e.g. GHS100'
	@InspYear nvarchar(50) = null, --- e.g. '2016'
	@InspResult nvarchar(20) = null,
	@FilterColumn nvarchar(10) = null,
	@filterValue int = null,

	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@AuthorityGroup nvarchar(10) = null,  -- govt/non-govt
	@SchoolType nvarchar(10) = null,

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

-- if filter params come from XML
if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	    @SAID = isnull(@SAID, saID),
		@School = isnull(@School, School),
		@SchoolNo = isnull(@SchoolNo, SchoolNo),
		@InspYear = isnull(@InspYear, InspYear),
		@InspResult = isnull(@InspResult, InspResult),
		@FilterColumn = isnull(@FilterColumn, FilterColumn),
		@FilterValue = isnull(@FilterValue, FilterValue),

		@District = isnull(@District, District),
		@Authority = isnull(@Authority, Authority),
		@AuthorityGroup = isnull(@AuthorityGroup, AuthorityGroup),
		@SchoolType = isnull(@SchoolType, SchoolType)


	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	saID int '@SAID',
	School nvarchar(50) '@School',
	SchoolNo nvarchar(50) '@SchoolNo',
	InspYear nvarchar(50) '@InspYear',
	InspResult nvarchar(50) '@InspResult',
	District nvarchar(10) '@District',
	Authority nvarchar(10) '@Authority',
	AuthorityGroup nvarchar(10) '@AuthorityGroup',
	SchoolType nvarchar(10)	'@SchoolType',
	FilterColumn nvarchar(10) '@FilterColumn',
	FilterValue int '@FilterValue'

	)
end

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)

-- improve the performance in this qery by avoiding recourse to the xml unless we strictly need it
--
if (@filterColumn is not null and @filterValue is not null) begin

	INSERT INTO @KeysAll (ID)
	SELECT inspID
	from pInspectionRead.SchoolAccreditations SA -- view
		LEFT JOIN Schools S
			ON SA.schNo = S.schNo
		LEFT JOIN DimensionAuthority AUTH
			ON AUTH.AuthorityCode = S.schAuth
		LEFT JOIN lkpIslands I
			ON S.iCode = I.iCode
	WHERE
	(inspID = @SAID OR @SAID IS NULL)
	AND (InspectionYear = @InspYear OR @InspYear IS NULL)
	AND (InspectionResult = @InspResult or @InspResult is null)
	AND (SA.schNo = @SchoolNo OR @SchoolNo IS NULL)
	AND (S.schName like '%' +  @School + '%' OR @School IS NULL)
	AND (S.schAuth = @Authority OR @authority is null)
	AND (AUTH.AuthorityGroupCode = @AuthorityGroup OR @AuthorityGroup is null)
	AND (I.iGroup = @District OR @District is null)
	AND (S.schType = @SchoolType or @SchoolType is null)
	AND case @filterColumn
			when 'se.1' then [SE.1]
			when 'se.2' then [SE.2]
			when 'se.3' then [SE.3]
			when 'se.4' then [SE.4]
			when 'se.5' then [SE.5]
			when 'se.6' then [SE.6]

			when 'co.1' then [CO.1]
		end = @FilterValue


	ORDER BY

		CASE @sortColumn			-- strings

			WHEN 'schNo' then SA.schNo
			WHEN 'schName' then SA.schName
			WHEN 'InspectedBy' then InspectedBy
			WHEN 'InspectionResult' then InspectionResult

			ELSE null
			END,
		CASE @sortColumn -- dates
			WHEN 'StartDate' then StartDate
			WHEN 'EndDate' then EndDate
			ELSE Null
		END,
		CASE @sortColumn
			WHEN 'InspectionYear' then InspectionYear
			ELSE null
		END,
		inspID

end

if (@filterColumn is null OR @filterValue is null) begin

	INSERT INTO @KeysAll (ID)
	SELECT inspID
	from pInspectionRead.SchoolInspections SA -- view
		LEFT JOIN Schools S
			ON SA.schNo = S.schNo
		LEFT JOIN DimensionAuthority AUTH
			ON AUTH.AuthorityCode = S.schAuth
		LEFT JOIN lkpIslands I
			ON S.iCode = I.iCode
	WHERE
	(inspID = @SAID OR @SAID IS NULL)
	AND (InspectionYear = @InspYear OR @InspYear IS NULL)
	AND (InspectionResult = @InspResult or @InspResult is null)
	AND (SA.schNo = @SchoolNo OR @SchoolNo IS NULL)
	AND (S.schName like '%' +  @School + '%' OR @School IS NULL)
	AND (S.schAuth = @Authority OR @authority is null)
	AND (AUTH.AuthorityGroupCode = @AuthorityGroup OR @authorityGroup is null)
	AND (I.iGroup = @District OR @District is null)
	AND (S.schType = @SchoolType or @SchoolType is null)

	ORDER BY

		CASE @sortColumn			-- strings

			WHEN 'schNo' then SA.schNo
			WHEN 'schName' then SA.schName
			WHEN 'InspectedBy' then InspectedBy
			WHEN 'InspectionResult' then InspectionResult

			ELSE null
			END,
		CASE @sortColumn -- dates
			WHEN 'StartDate' then StartDate
			WHEN 'EndDate' then EndDate
			ELSE Null
		END,
		CASE @sortColumn
			WHEN 'InspectionYear' then InspectionYear
			ELSE null
		END,
		inspID

end

SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

