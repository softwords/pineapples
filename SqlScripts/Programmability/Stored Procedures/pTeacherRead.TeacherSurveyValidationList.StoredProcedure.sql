SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 6 2009
-- Description:	TEacher at a selected school in a given year, and their whereabouts in a future year
-- supercedes sp_wherearetheynow by including any teacher with an appointment at the school.
-- dec 2014: expanded to support prepopulation of e-survey form
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherSurveyValidationList]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50),
	@srcYear int = 0,
	@targetYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

-- we want to include any tID that is in the survey list for the source year
-- or the appointments for the target year

declare @SurveyID int

declare @EndDate datetime
declare @StartDate datetime

declare  @Tids TABLE
(
tID int
)

if (isnull(@srcYear,0) = 0 )
	-- get the most recent survey prior to @targetyear
	Select @srcYear = max(svyYear)
	FROM SchoolSurvey SS
		INNER JOIN TeacherSurvey TS
			ON SS.ssID = Ts.ssID
	WHERE SS.schNo = @schNo
	AND SS.svyYEar < @TargetYear

Select @SurveyID = ssID
FROM SchoolSurvey
WHERE schNo = @schNo
	AND svyYear = @srcYear


Select @EndDate = common.dateserial(@TargetYear,12,31)
	   , @StartDate = common.dateserial(@TargetYear,1,1)

INSERT INTO @tIDS
(tID)
SELECT tID
FROM TeacherSurvey 	TS
WHERE ssID = @SurveyID

UNION

Select tID
FROM TeacherAppointment TA
INNER JOIN Establishment E
	ON TA.estpNo = E.estpNo
WHERE E.schNo = @SchNo
AND taDate <= @EndDate
AND (taEndDate >= @StartDate or  taEndDate is null)

Select TIDS.tID
, tPayroll
, tDOB
, tSex
, tFullName
, tGiven
, tSurname
, srcY.tchsID
, srcY.schNo			-- which may <> @SchNo if an appointment is a tranfers
, SrcY.tchRole
, srcY.schName
, NY.schNo NextSchNo
, NY.schName NextSchoolName
, NY.tchRole NextRole
, NY.tchsID NextTeacherSurvey
, Appts.estpNo
, Appts.estpRoleGrade
, Appts.taDate
, Appts.taEndDate
, tPayroll PayrollNo
, tSurname FamilyName
, tGiven FirstName
, tSex Gender
, tDoB DoB
, SrcY.tchRole Role
, SrcY.tchTAM Duties
, case SrcY.tchHouse when -1 then 'Y' when 0 then 'N' when 1 then 'Y' else null end House
, tchFullPart FP

from @tIDS TIDS
INNER JOIN TeacherIdentity TI
	ON TI.tID = TIDS.tID
LEFT JOIN (Select tID, tchsID, SS.schNo, schName, tchRole
				from TeacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				INNER JOIN Schools S
					ON SS.schNo = S.schNo
				WHERE SS.svyYEar = @TargetYear
			) NY
	ON NY.tID = TIDS.tID

LEFT JOIN (Select tID
			, E.estpNo
			, E.estpRoleGrade
			, TA.taDate
			, TA.taEndDate
			FROM TeacherAppointment TA
			INNER JOIN Establishment E
				ON TA.estpNo = E.estpNo
			WHERE E.schNo = @SchNo
				AND taDate <= @EndDate
				AND (taEndDate >= @StartDate or  taEndDate is null)
			) Appts

	ON Appts.tID = TIDS.tID

LEFT JOIN (Select tID, TS.tchsID, SS.schNo, schName, tchRole , tchTAM, tchHouse, tchFullPart
				from TeacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				INNER JOIN Schools S
					ON SS.schNo = S.schNo
				WHERE SS.svyYEar = @SrcYear

			) SrcY
	ON srcY.tID = TIDS.tID
ORDER BY tSurname, tGiven
END
GO

