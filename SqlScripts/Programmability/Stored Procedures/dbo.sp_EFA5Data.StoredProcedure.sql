SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 12 2007
-- Description:	Core data for the calculation of EFA1 3 4 5 6
-- =============================================
-- 16 10 2012 bdl temp table replaced with table variable to insulate against collation conflicts with temp db

CREATE PROCEDURE [dbo].[sp_EFA5Data]
	@NationSummary int = 0,
	@StartYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try
-- many things can go wrong in this SP, wret to various flags not being set up properly
-- the SP will report these for ease of debugging

-----------------------------------------------------
-- begin by testing some prerequisites

declare @checkPop int		-- number of population models
declare @popModCode nvarchar(10)			-- the pop model we will use
declare @popModName nvarchar(50)
declare @popModDefault int

Select @checkPop = count(*)
, @popModCode = min(popModCode)
From PopulationModel


if (@checkPop=0) begin
	raiserror('There is no Population Model to use. ',
				16,10)
end

if (@checkPop > 1) begin
	declare @checkEFA int
	Select @checkEFA = count(*)
	-- SRSI0045 get the correct value for @popModCode
	, @popModCode =  min(popModCode)
	From PopulationModel

	WHERE popModEFA = 1

	if (@checkEFA <> 1) begin
		raiserror('You must specify exactly one Population Model to use for EFA reporting. ',
					16,10)
	end


end


	Select *
	into #ebse
	from dbo.tfnESTIMATE_BestSurveyenrolments()
	WHERE LifeYear >= @StartYear

	-- the data
	declare @data table
	(
	svyYear int,
	Estimate int,
	[Year Of Data] int,
	[Age of Data] int,
	dID nvarchar(10),
	iCode nvarchar(10),
	ElN nvarchar(10),
	ElL nvarchar(10),
	schNo nvarchar(50),
	Age int,
	LevelCode nvarchar(10),
	enrolM int,
	enrolF int,
	repM int,
	repF int,
	popM int,
	popF int,
	popModCode nvarchar(50)
	)

-- first add the population records
-- the trickies part is to determine a 'level code' to put on this population record
-- we want this so we can in the usual case show enrol and population by level
-- however, if there is more than one level at a specific year of education, we cannot double up
-- the population numbers, so we want to put only the usual or default level on the pop record
-- recporting on other levels will have to be done by year of education


	-- work out the set of level code to use for each year of ed
	--
	-- if there is onlyone level at each year of ed, no problem
	create table #popLevels
	(
	levelCode nvarchar(10),
	lvlYear int
	)
	declare @dupLevelYears int;
	declare @check int;

	Select @DupLevelYears = count(lvlYEar)
	from
	(
	Select lvlYear, count(codeCode) NumLevels from lkpLevels group by lvlyear having count(codeCode) > 1
	) q

	if @DupLevelYears = 0
		INSERT INTO #popLevels
		Select codeCode, lvlYear
		from lkpLevels
	else
		BEGIN

			Select @check = count(*)
			From EducationPaths
			WHERE pathDefault = 1

			if (@check <> 1) begin
				raiserror('There are multiple at class levels at one or more years of education, so you must set up exactly one default Education Path to pick one level for each Year of Education',
							16,10)
			end

			INSERT INTO #popLevels
			Select codeCode levelCode,lvlYear
			from lkpLevels
				INNER JOIN EducationPathLevels EPL
					ON lkpLevels.codecode = EPL.levelCode
				INNER JOIN EducationPaths EP
					ON EP.pathCode = EPL.pathCode
			WHERE
				pathDefault = 1


			Select @check = count(*)
			From #popLevels

			if (@check = 0) begin
				raiserror('There are no class level associated to the default education path',
							16,10)
			end


		END

----declare @minAge int
----declare @maxAge int

----Select @minAge = min(enAge)
----, @MaxAge = max(enAge) from Enrollments

insert into @data
(svyYear, Estimate, dID, iCode, ElN, ElL, Age, levelCode,  popM, popF, popmodCode)
Select
-- put 0 in for estimate
popYear, 0 , dID, iCode, ElN, ElL, popAge, DPL.LevelCode,  popM, popF, pop.popmodCode
from [Population] pop
	INNER JOIN Survey
		ON pop.popYear = survey.svyYear
	LEFT JOIN #popLevels DPL	-- default path levels
		ON DPL.lvlYEar = pop.popAge - Survey.svyPSAge 	+ 1
WHERE pop.popmodCode = @popModCode
and popYear >=@StartYear
--and popAge between @minAge and @maxAge
 -- if there's only one, use it


-- now the enrolments

insert into @data
(svyYear,
Estimate,
[Year of Data],
[Age of Data],
dID, iCode, ElN, ElL, schNo,
Age, LevelCode,
EnrolM, enrolF,
popmodCode)
Select
E.LifeYear,
E.Estimate,
E.bestYear,
E.offset,
I.iGroup,
S.iCode,
SS.ssElectN,
SS.ssElectL,
S.schNo,
enAge,
enLevel,
enM,
enF,
@popmodCode
from #EBSE E
INNER JOIN Enrollments SSE
	On SSE.ssID = E.bestssID
LEFT JOIN SchoolSurvey SS
	On SS.ssID = E.surveydimensionssID			-- a subtle point of compatibility!
LEFT JOIN Schools S
	ON S.schNo = SS.schNo
LEFT JOIN Islands I
	ON I.iCode = s.Icode

-- repeaters
insert into @data
(svyYear, Estimate,
dID, iCode, ElN, ElL, schNo,
Age, LevelCode,
repM, repF,
popmodCode)
Select
E.LifeYear,
E.Estimate,
I.iGroup,
S.iCode,
SS.ssElectN,
SS.ssElectL,
S.schNo,
ptAge,
ptLevel,
ptM,
ptF,
@popmodCode
from #EBSE E
INNER JOIN vtblRepeaters SSE
	On SSE.ssID = E.bestssID
LEFT JOIN SchoolSurvey SS
	On SS.ssID = E.surveydimensionssID			-- a subtle point of compatibility!
LEFT JOIN Schools S
	ON S.schNo = SS.schNo
LEFT JOIN Islands I
	ON I.iCode = s.Icode


--Select *
--into #DimensionAge
--from DimensionAge
----WHERE atEdLevelAge = 1
----	or atEdLevelAltAge = 1
----	or atEdLevelAlt2Age = 1

select @popModName = popModName
, @popModDefault = popModDEfault
from TRPopulationModel
WHERE popModCode = @popModCode


-----------OUTPUTS------------------------------------------------------
-- NationSummary = 0 - this feeds into the pivot table

If @NationSummary = 0 begin
	Select
		subData.svyYear [Survey Year],
		Estimate,
		DistrictCode [District Code],
		dName District,
		IslandCode [Island Code],
		iName Island,
		ElN ElectorateN,
		ElL ElectorateL,
		subData.Age ,
		subData.LevelCode,
		enrolM,
		enrolF,
		repM,
		repF,
		popM,
		popF,
		case
			when enrolM is null and repM is null then null
			else isnull(enrolM,0) - isnull(repM,0)
		end IntakeM,
		case
			when enrolF is null and repF is null then null
			else isnull(enrolF,0) - isnull(repF,0)
		end IntakeF,
		subData.popModCode,
		@popModName popModName,
		@popmodDefault popModDefault

		,isnull(DAge.AtLevelAge,0) AtLevelAge
		, isnull(DAge.AtEdLevelAge,0) AtEdLevelAge
		, isnull(DAge.AtEdLevelAltAge,0) AtEdLevelAltAge
		,  isnull(DAge.AtEdLevelAlt2Age,0) AtEdLevelAlt2Age
		, edLevelCode
		,edLevelAltCode
		,edLevelAlt2Code

		from
		(
		Select
		svyYear ,
		Estimate,					-- grouped
		D.dID DistrictCode,
		D.iCode IslandCode,
		ElN ,
		ElL ,
		Age ,
		LevelCode,
		sum(enrolM) enrolM,
		sum(enrolF) enrolF,
		sum(repM) repM,
		sum(repF) repF,
		sum(popM) popM,
		sum(popF) popF,
		popModCode
		from @data D

		GROUP BY
		svyYear, D.dID,  D.iCode,
		D.elN, D.elL, Estimate,
		D.Age, D.LevelCode,
		popModCode
		) subData
			LEFT JOIN DimensionAge DAge
				ON subData.levelCode = DAge.levelCode
						AND subData.Age = DAge.Age
						AND subData.svyYear = DAge.svyYear
			LEFT JOIN Districts
				ON subData.DistrictCode = Districts.dID
			LEFT JOIN Islands
				ON subData.IslandCode = Islands.iCode
end

-- consolidated
If @NationSummary = 1 begin
	Select
		subData.svyYear [Survey Year],
		Estimate,
		subData.Age ,
		subData.LevelCode,
		enrolM,
		enrolF,
		repM,
		repF,
		popM,
		popF,
		case
			when enrolM is null and repM is null then null
			else isnull(enrolM,0) - isnull(repM,0)
		end IntakeM,
		case
			when enrolF is null and repF is null then null
			else isnull(enrolF,0) - isnull(repF,0)
		end IntakeF,
		subData.popModCode,
		@popModName popModName,
		@popmodDefault popModDefault

		,isnull(DAge.AtLevelAge,0) AtLevelAge
		, isnull(DAge.AtEdLevelAge,0) AtEdLevelAge
		, isnull(DAge.AtEdLevelAltAge,0) AtEdLevelAltAge
		,  isnull(DAge.AtEdLevelAlt2Age,0) AtEdLevelAlt2Age
		, edLevelCode
		,edLevelAltCode
		,edLevelAlt2Code

		from
		(
		Select
		svyYear ,
		Estimate,					-- grouped
		Age ,
		LevelCode,
		sum(enrolM) enrolM,
		sum(enrolF) enrolF,
		sum(repM) repM,
		sum(repF) repF,
		sum(popM) popM,
		sum(popF) popF,
		popModCode
		from @data D

		GROUP BY
		svyYear,  Estimate,
		D.Age, D.LevelCode,
		popModCode
		) subData
			LEFT JOIN DimensionAge DAge
				ON subData.levelCode = DAge.levelCode
						AND subData.Age = DAge.Age
						AND subData.svyYear = DAge.svyYear
end


end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO
GRANT EXECUTE ON [dbo].[sp_EFA5Data] TO [pSchoolRead] AS [dbo]
GO

