SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[getmetaPupilTableDef]
	-- Add the parameters for the stored procedure here
	@tableCode nvarchar(20)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT *
		from metaPupilTableDefs
		where
			tdefCode = @tableCode
    -- Insert statements for procedure here
	RETURN 0
END
GO
GRANT EXECUTE ON [dbo].[getmetaPupilTableDef] TO [public] AS [dbo]
GO

