SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [warehouse].[schoolDataSetAccreditation]
	-- Add the parameters for the stored procedure here
	@year int
	, @dataItem nvarchar(100)
	, @filter nvarchar(10) = null
	, @schoolNo nvarchar(50) = null
	, @parameter nvarchar(100) = null
AS
BEGIN
	SET NOCOUNT ON;

	Select schNo
	,  convert(int, right(InspectionResult,1)) DataValue
	, null Estimate
	, null Quality
	from warehouse.bestinspection
	WHERE InspectionTypeCode = @DataItem
	AND surveyYear = @year

END
GO

