SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[logUserLogOut]
	-- Add the parameters for the stored procedure here
	@WindowsUserName nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update UserInfo
		SET uActive = 0
	WHERE ulogin = @WindowsUserName

	exec LogActivity 'Logout', null
END
GO
GRANT EXECUTE ON [dbo].[logUserLogOut] TO [public] AS [dbo]
GO

