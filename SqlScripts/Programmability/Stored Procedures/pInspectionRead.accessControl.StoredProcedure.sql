SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 10 2018
-- Description:	check access control on any inspectin type
-- =============================================
CREATE PROCEDURE [pInspectionRead].[accessControl]
	-- Add the parameters for the stored procedure here
	@inspID int
	, @district nvarchar(10) = null
	, @authority nvarchar(20) = null
	, @userSchool nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @s nvarchar(50)
	SELECT @s = INSP.schNo
	FROM SchoolInspection INSP
		INNER JOIN Schools S
			ON S.schNo = INSP.schNo
		INNER JOIN lkpIslands I
			ON S.iCode = I.iCode
	WHERE INSP.inspID = @inspID
		AND (schAuth = @authority or @authority is null)
		AND (I.iGroup = @district or @district is null)
		AND (INSP.schNo = @userSchool or @userSchool is null)

	if @@ROWCOUNT = 0 begin
		Raiserror('Forbidden', 16,1);
	end

END
GO

