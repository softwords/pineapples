SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 3 2021
-- Description:	Sheet A9 of UIS Survey:
--	number of classroom teachers
-- =============================================
CREATE PROCEDURE [warehouse].[uisA9]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE  @Gov Table
(
Gov nvarchar(1),
GovGroup nvarchar(10)
)


DECLARE  @d Table
(
Isced nvarchar(10),
Gov nvarchar(1),
GenderCode nvarchar(1),

Flag nvarchar(1),
NumTeachers int,
NumNewTeachers int,
Qualified int,
Certified int,
InService int,
FTE decimal(12,6),
FTEQ decimal(12,6),
FTEC decimal(12,6),

FTPT decimal(12,6),				-- for full time part time
FTPTQ decimal(12,6),
FTPTC decimal(12,6),
FTPTNew decimal(12,6),
FTPTInSvc decimal(12,6)

-- note that the UIS Survey doe not analyse NewHire and Inservice by FTE so we dont capture this,
-- but sum( Inservice * FTE) would provide it
)

INSERT INTO @Gov
VALUES ('G','Public'),
('N','Private')

-- build the data
-- this set ensures that all public private cells get populated
-- but only if the isced class has some enrolment in the year
--
-- subclass
INSERT INTO @d
SELECT DISTINCT
DL.[ISCED SubClass] Isced,
AUTH.codeCode Gov,
GenderCode,
null Flag,
0 NumTeachers,
0 NumNewTeachers,
0 Qualified,
0 Certified,
0 InService,
0 FTE,
0 FTEQ,
0 FTEC,
0 FTPT,			-- full time part time is weighted across teaching activities
0 FTPTQ,
0 FTPTC,
0 FTPTNew,
0 FTPTInSvc

from warehouse.tableEnrol E
INNER join DimensionLevel DL on E.ClassLevel = DL.LevelCode
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender
WHERE surveyYear = @year

-- isced top level
INSERT INTO @d
SELECT DISTINCT
DL.[Isced Level] Isced,
AUTH.codeCode Gov,
GenderCode,
null Flag,
0 NumTeachers,
0 NumNewTeachers,
0 Qualified,
0 Certified,
0 InService,
0 FTE,
0 FTEQ,
0 FTEC,
0 FTPT,
0 FTPTQ,
0 FTPTC,
0 FTPTNew,
0 FTPTInSvc

from warehouse.tableEnrol E
INNER join DimensionLevel DL on E.ClassLevel = DL.LevelCode
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender
WHERE surveyYear = @year

-- need the same for the special subtotals 'SECONDARY4' 'SECONDARY5'
INSERT INTO @d
SELECT DISTINCT
'SECONDARY4' Isced,
AUTH.codeCode Gov,
GenderCode,
null Flag,
0 NumTeachers,
0 NumNewTeachers,
0 Qualified,
0 Certified,
0 InService,
0 FTE,
0 FTEQ,
0 FTEC,
0 FTPT,
0 FTPTQ,
0 FTPTC,
0 FTPTNew,
0 FTPTInSvc
from warehouse.tableEnrol E
INNER join DimensionLevel DL
	on E.ClassLevel = DL.LevelCode
	AND DL.[ISCED SubClass] in ('ISCED 24', 'ISCED 34')
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender
WHERE surveyYear = @year

INSERT INTO @d
SELECT DISTINCT
'SECONDARY5' Isced,
AUTH.codeCode Gov,
GenderCode,
null Flag,
0 NumTeachers,
0 NumNewTeachers,
0 Qualified,
0 Certified,
0 InService,
0 FTE,
0 FTEQ,
0 FTEC,
0 FTPT,
0 FTPTQ,
0 FTPTC,
0 FTPTNew,
0 FTPTInSvc
from warehouse.tableEnrol E
INNER join DimensionLevel DL
	on E.ClassLevel = DL.LevelCode
	AND DL.[ISCED SubClass] in ('ISCED 25', 'ISCED 35')
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender
WHERE surveyYear = @year


-- 2) this set supplies the actual records - this is sparse
-- 2A -- subclass
INSERT INTO @d
( Isced, Gov, GenderCode, Flag, NumTeachers, NumNewTeachers, Qualified, Certified, InService
, FTE, FTEQ, FTEC, FTPT, FTPTQ, FTPTC, FTPTNew, FTPTInSvc)
Select IscedSubClass
, AuthorityGovt
, GenderCode
, null Flag
, count(DISTINCT tID) NumTeachers
, count(DISTINCT case when NewHire = 1 then tID end) NumNewTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when InService = 1 then tID end)  InService
, sum(W) FTE
, sum(WQ) FTEQ
, sum(WC) FTEC
, sum(WTeach) FTPT
, sum(WTeachQ) FTPTQ
, sum(WTeachC) FTPTC
, sum(WTeach * NewHire) FTPTNew
, sum(WTeach * InService) FTPTInSvc

from warehouse.TeacherActivityWeights
where surveyYear = @year
group by IscedSubClass, AuthorityGovt, GenderCode
order by IscedSubClass, AuthorityGovt, GenderCode

-- 2B -- isced level
INSERT INTO @d
( Isced, Gov, GenderCode, Flag, NumTeachers, NumNewTeachers, Qualified, Certified, InService
, FTE, FTEQ, FTEC, FTPT, FTPTQ, FTPTC, FTPTNew, FTPTInSvc)
Select II.ilCode
, AuthorityGovt
, GenderCode
, null Flag
, count(DISTINCT tID) NumTeachers
, count(DISTINCT case when NewHire = 1 then tID end) NumNewTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when InService = 1 then tID end)  InService
, sum(W) FTE
, sum(WQ) FTEQ
, sum(WC) FTEC
, sum(WTeach) FTPT
, sum(WTeachQ) FTPTQ
, sum(WTeachC) FTPTC
, sum(WTeach * NewHire) FTPTNew
, sum(WTeach * InService) FTPTInSvc

from warehouse.TeacherActivityWeights W
INNER JOIN ISCEDLevelSub II
	ON W.IscedSubClass = II.ilsCode
where surveyYear = @year
group by II.ilCode, AuthorityGovt, GenderCode
order by II.ilCode, AuthorityGovt, GenderCode


-- introduce subtotals on ISCED General/Vocational
INSERT INTO @d
( Isced, Gov, GenderCode, Flag, NumTeachers, NumNewTeachers, Qualified, Certified, InService
, FTE, FTEQ, FTEC, FTPT, FTPTQ, FTPTC, FTPTNew, FTPTInSvc)
Select 'SECONDARY4'
, AuthorityGovt
, GenderCode
, null Flag
-- since we may have 2 records in here for the same teacher, be caerful to count distinct
, count(DISTINCT tID) NumTeachers

, count(DISTINCT case when NewHire = 1 then tID end) NumNewTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when InService = 1 then tID end)  InService
, sum(W) FTE		-- the FTE weights can be accumulated safely
, sum(WQ) FTEQ
, sum(WC) FTEC
, sum(WTeach) FTPT
, sum(WTeachQ) FTPTQ
, sum(WTeachC) FTPTC
, sum(WTeach * NewHire) FTPTNew
, sum(WTeach * InService) FTPTInSvc

from warehouse.TeacherActivityWeights W
where surveyYEar = @year
AND W.IscedSubClass in ('ISCED 24','ISCED 34')
group by AuthorityGovt, GenderCode
order by AuthorityGovt, GenderCode

INSERT INTO @d
( Isced, Gov, GenderCode, Flag, NumTeachers, NumNewTeachers, Qualified, Certified, InService
, FTE, FTEQ, FTEC, FTPT, FTPTQ, FTPTC, FTPTNew, FTPTInSvc)
Select 'SECONDARY5'
, AuthorityGovt
, GenderCode
, null Flag
, count(DISTINCT tID) NumTeachers

, count(DISTINCT case when NewHire = 1 then tID end) NumNewTeachers
, count(DISTINCT case when Qualified = 1 then tID end)  Qualified
, count(DISTINCT case when Certified = 1 then tID end)  Certified
, count(DISTINCT case when InService = 1 then tID end)  InService

, sum(W) FTE
, sum(WQ) FTEQ
, sum(WC) FTEC
, sum(WTeach) FTPT
, sum(WTeachQ) FTPTQ
, sum(WTeachC) FTPTC
, sum(WTeach * NewHire) FTPTNew
, sum(WTeach * InService) FTPTInSvc

from warehouse.TeacherActivityWeights W
where surveyYEar = @year
AND W.IscedSubClass in ('ISCED 25','ISCED 35')
group by AuthorityGovt, GenderCode
order by AuthorityGovt, GenderCode


-- 3) Missing data (M flag)
-- this identifies the ISCED codes are in DimmensionLevel
-- (ie there is a ClassLevel defined for that Isced level)
-- but there is no enrolment data for that ISCED level at all.
-- therefore we go with M - missing

-- 3A - Isced sub Levels
INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
ilsCode Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'M' Flag

FROM ISCEDLevelSub I
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
-- there are class levels for this isced subclass
WHERE I.ilsCode in (select [ISCED SubClass] from DimensionLevel)
-- but none of those class levels have enrolments this year
AND I.ilsCode NOT in
(select DL.[ISCED SubClass]
from warehouse.EnrolNation E
	INNER JOIN DimensionLevel DL
	ON E.ClassLevel = DL.LevelCode
	where surveyYear = @year
)


-- 3B Isced
INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
ilCode Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'M' Flag

FROM ISCEDLevel I
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
-- there are class levels for this isced subclass
WHERE I.ilCode in (select [ISCED Level] from DimensionLevel)
-- but none of those class levels have enrolments this year
AND I.ilCode NOT in
(select DL.[ISCED Level]
from warehouse.EnrolNation E
	INNER JOIN DimensionLevel DL
	ON E.ClassLevel = DL.LevelCode
	where surveyYear = @year
)

--3c
INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
'SECONDARY4' Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'M' Flag
FROM lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
-- there are class levels for this isced subclass in Isced 24 ISCED 34
WHERE exists (
	select levelCode from DimensionLevel DL WHERE DL.[ISCED SubClass] in ('ISCED 24','ISCED 34')
)
-- but none of those class levels have enrolments this year
AND NOT exists
(
	select DL.[ISCED Level]
	from warehouse.EnrolNation E
	INNER JOIN DimensionLevel DL
		ON E.ClassLevel = DL.LevelCode
	where surveyYear = 2021 --@year
		AND DL.[ISCED SubClass] in ('ISCED 24','ISCED 34')
)

INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
'SECONDARY5' Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'M' Flag
FROM lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
-- there are class levels for this isced subclass in Isced 25 ISCED 35
WHERE exists (
	select levelCode from DimensionLevel DL WHERE DL.[ISCED SubClass] in ('ISCED 25','ISCED 35')
)
-- but none of those class levels have enrolments this year
AND NOT exists
(
	select DL.[ISCED Level]
	from warehouse.EnrolNation E
	INNER JOIN DimensionLevel DL
		ON E.ClassLevel = DL.LevelCode
	where surveyYear = 2021 --@year
		AND DL.[ISCED SubClass] in ('ISCED 25','ISCED 35')
)
-- 4) Not applicable ( Z flag)
-- this identifies ISCED codes that are not in DimmensionLevel
-- ie there are no Grades defined that use these codes.
-- therefore we go with Z - not applicable
INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
ilsCode Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'Z' Flag

FROM (ISCEDLevelSub I
LEFT JOIN DimensionLevel DL
ON I.ilsCode = DL.[ISCED SubClass])
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
WHERE I.ilsCode not in (select [ISCED SubClass] from DimensionLevel)

-- 4B - isced level
INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
ilCode Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'Z' Flag

FROM ISCEDLevel I
CROSS join lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
WHERE I.ilCode not in (select [Isced Level] from DimensionLevel)

-- support Z for the special values SECONDARY4, SECONDARY5
INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
'SECONDARY4' Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'Z' Flag
FROM lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
WHERE not exists (
select levelCode from DimensionLevel WHERE [ISCED SubClass] in ('ISCED 24','ISCED 34')
)

INSERT INTO @d
( Isced, Gov, GenderCode, Flag)
SELECT DISTINCT
'SECONDARY5' Isced,
AUTH.codeCode Gov,
G.codeCode GenderCode,
'Z' FlagC

FROM lkpAuthorityGovt AUTH
CROSS JOIN lkpGender G
WHERE not exists (
select levelCode from DimensionLevel WHERE [ISCED SubClass] in ('ISCED 25','ISCED 35')
)


-- introduce totals across Public /Private
INSERT INTO @d
Select D.Isced
, null Govt
, GenderCode
, max(Flag)
, sum(NumTeachers)
, sum(NumNewTeachers)
, sum(Qualified)
, sum(Certified)
, sum(Inservice) InService
, sum(FTE)
, sum(FTEQ)
, sum(FTEC)
, sum(FTPT)
, sum(FTPTQ)
, sum(FTPTC)
, sum(FTPTNew)
, sum(FTPTInSvc)
from @d D
GROUP BY Isced, GenderCode
-- Aggregate the results

Select @year SurveyYear,
Isced,
GovGroup,
max(Flag) Flag,

sum(case GenderCode when 'M' then NumTeachers end) NumTeachersM,
sum(case GenderCode when 'F' then NumTeachers end) NumTeachersF,
sum(case GenderCode when 'M' then NumNewTeachers end) NumNewTeachersM,
sum(case GenderCode when 'F' then NumNewTeachers end) NumNewTeachersF,
sum(case GenderCode when 'M' then Qualified end) QualifiedM,
sum(case GenderCode when 'F' then Qualified end) QualifiedF,
sum(case GenderCode when 'M' then Certified end) CertifiedM,
sum(case GenderCode when 'F' then Certified end) CertifiedF,
sum(case GenderCode when 'M' then InService end) InServiceM,
sum(case GenderCode when 'F' then InService end) InServiceF,

sum(case GenderCode when 'M' then FTE end) FteM,
sum(case GenderCode when 'F' then FTE end) FteF,
sum(case GenderCode when 'M' then FTEQ end) FteQualifiedM,
sum(case GenderCode when 'F' then FTEQ end) FteQualifiedF,
sum(case GenderCode when 'M' then FTEC end) FteCertifiedM,
sum(case GenderCode when 'F' then FTEC end) FteCertifiedF,


sum(case GenderCode when 'M' then FTPT end) FtPtM,
sum(case GenderCode when 'F' then FTPT end) FtPtF,
sum(case GenderCode when 'M' then FTPTQ end) FtPtQualifiedM,
sum(case GenderCode when 'F' then FTPTQ end) FtPtQualifiedF,
sum(case GenderCode when 'M' then FTPTC end) FtPtCertifiedM,
sum(case GenderCode when 'F' then FTPTC end) FtPtCertifiedF,
sum(case GenderCode when 'M' then FTPTNew end) FtPtNewM,
sum(case GenderCode when 'F' then FTPTNew end) FtPtNewF,
sum(case GenderCode when 'M' then FTPTInSvc end) FtPtInServiceM,
sum(case GenderCode when 'F' then FTPTInSvc end) FtPtInServiceF

FROM
@d D
LEFT JOIN @Gov G
ON D.Gov = G.Gov

group by Isced, GovGroup
order by isced, GovGroup


END
GO
GRANT EXECUTE ON [warehouse].[uisA9] TO [dbreadonly] AS [dbo]
GO

