SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 3 2021
-- Description:	Sheet A3 of UIS Survey
-- =============================================
CREATE PROCEDURE [warehouse].[uisA3]
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE  @AgeGroups Table
(
Age int,
AgeGRoup nvarchar(10)
)


DECLARE  @d Table
(
Isced nvarchar(10),
Age int,
Flag nvarchar(1),
EnrolM int,
EnrolF int,
Enrol int
)

INSERT INTO @AgeGroups
Select DISTINCT Age,
case when Age =  -1 then 'Unknown'
when Age > 29 then '>29'
else
	case age
		when -1 then 'Unknown'
		when 0 then '<2'
		when 1 then '<2'
		when 25 then '25-29'
		when 26 then '25-29'
		when 27 then '25-29'
		when 28 then '25-29'
		when 29 then '25-29'
		else convert(nvarchar(2), Age)
	end
end AgeGroup
from
(Select isnull(Age, -1) Age from warehouse.tableEnrol
UNION
Select num from metaNumbers WHERE num between -1 and 30
) U

-- build the data
-- this set ensures that all age cells get populated
-- but only if the isced class has some data in the year
INSERT INTO @d
SELECT DISTINCT
DL.[ISCED SubClass] Isced,
AG.Age,
null Flag,
0 EnrolM,
0 EnrolF,
0 Enrol

from warehouse.tableEnrol E
INNER join DimensionLevel DL on E.ClassLevel = DL.LevelCode
CROSS JOIN @AgeGroups AG
WHERE surveyYear = @year

-- 2) this set supplies the actual records - this is sparse
INSERT INTO @d
SELECT
DL.[ISCED SubClass] Isced,
isnull(Age, -1) Age,
convert(nvarchar(1),null) Flag,
case E.GenderCode when 'M' then Enrol end EnrolM,
case E.GenderCode when 'F' then Enrol end EnrolF,
Enrol Enrol
from warehouse.tableEnrol E
left join DimensionLevel DL on E.ClassLevel = DL.LevelCode

where E.SurveyYear = @year AND E.Enrol IS NOT NULL


-- 3) Missing data (M flag)
-- this identifies the ISCED codes are in DimmensionLevel
-- (ie there is a ClassLevel defined for that Isced level)
-- but there is no enrolment data for that ISCED level at all.
-- therefore we go with M - missing
INSERT INTO @d
SELECT
ilsCode Isced,
Age,
'M' Flag,
null EnrolM,
null EnrolF,
null Enrol

FROM ISCEDLevelSub I
INNER JOIN DimensionLevel DL
ON I.ilsCode = DL.[ISCED SubClass]
CROSS JOIN @AgeGroups

WHERE DL.LevelCode not in (select E.ClassLevel from warehouse.EnrolNation E where surveyYear = @year)

-- 4) Not applicablt ( Z flag)
-- this identifies ISCED codes that are not in DimmensionLevel
-- ie there are no Grades defined that use these codes.
-- therefore we go with Z - not applicable
INSERT INTO @d
SELECT
ilsCode Isced,
num Age,

'Z' Flag,
null EnrolM,
null EnrolF,
null Enrol

FROM (ISCEDLevelSub I
LEFT JOIN DimensionLevel DL
ON I.ilsCode = DL.[ISCED SubClass])
LEFT JOIN metaNumbers
ON num between -1 and 30
WHERE DL.LevelCode is null

-- Aggregate the results

Select Isced,
AgeGroup,
max(Flag) Flag,
sum(EnrolM) EnrolM,
sum(EnrolF) EnrolF,
sum(Enrol) Enrol

FROM
@d D
INNER JOIN @AgeGroups AG
ON D.Age = AG.Age

group by Isced, AgeGroup
order by isced,  min(AG.age)


END
GO

