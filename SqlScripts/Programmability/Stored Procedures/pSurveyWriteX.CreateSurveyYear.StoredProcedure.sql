SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 05 2010
-- Description:	Initiate a new survey year
-- Updated for Pacific EMIS 22 9 2021
-- =============================================
CREATE PROCEDURE [pSurveyWriteX].[CreateSurveyYear]
	-- Add the parameters for the stored procedure here
	@SurveyYear int
	, @CensusDate datetime
	, @CollectionDate datetime
	, @OfficialStartAge int
	, @user nvarchar(100)
WITH EXECUTE AS 'pineapples'		-- becuase we need to be able to disable a trigger
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

declare @maxYear int
declare @minYear int
select @maxYear = max(svyYear)
, @minYear = min(svyYear) from Survey

-- validations
	-- cannot go backwards
	if (@SurveyYear < @maxYear) begin
				RAISERROR('<ValidationError field="svyYear">
					Cannot create survey year before current year
					</ValidationError>', 16,1)
	end
	if (@SurveyYear - @maxYear > 1) begin
				RAISERROR('<ValidationError field="svyYear">
					Cannot skip a survey year
					</ValidationError>', 16,1)
	end
	if @CensusDate is null begin
				RAISERROR('<ValidationError field="svyCensusDate">
					No census date specified
					</ValidationError>', 16,1)
	end

	if @CollectionDate is null begin
				RAISERROR('<ValidationError field="svyCollectionDate">
					No collection date specified
					</ValidationError>', 16,1)
	end

	if @OfficialStartAge is null begin
				RAISERROR('<ValidationError field="svyPSAge">
					No official start age specified
					</ValidationError>', 16,1)
	end

-- deal with the case of a simple update
begin transaction

declare @date datetime = getUtcDate()

if @maxYear = @SurveyYear begin
	UPDATE Survey
	SET svyCensusDate = @CensusDate
	, svyCollectionDate = @CollectionDate
	, svyPSAge = @OfficialStartAge
	, pEditDateTime = @date
	, pEditUser = @user
	WHERE svyYEar = @SurveyYear
	-- don;t do anything else

end
else begin

-- a new Survey record has been created
	INSERT INTO Survey
	(	svyYear
		,svyCensusDate
		,svyCollectionDate
		, svyPSAge
		, pCreateUser
		, pCreateDateTime
		, pEditUser
		, pEditDateTime
	) VALUES

	(	@SurveyYear
		, @CensusDate
		, @CollectionDate
		, @OfficialStartAge
		, @user
		, @date
		, @user
		, @date

	)
end


   -- copy all the school type definitions to the new year from the last
   INSERT INTO [SurveyYearSchoolTypes]
           ([svyYear]
           ,[stCode]
           ,[ytForm]
           ,[ytTeacherForm]
           ,[ytAgeMin]
           ,[ytAgeMax]
           ,[ytConfig]
		   , ytEForm)
    SELECT
			@SurveyYear
           ,ST.[stCode]
           ,SYST.[ytForm]
           ,SYST.[ytTeacherForm]
           ,SYST.[ytAgeMin]
           ,SYST.[ytAgeMax]
           ,SYST.[ytConfig]
		   , replace(SYST.ytEForm, convert(nvarchar(4), SYST.svyYear), convert(nvarchar(4), @SurveyYear))
    FROM SchoolTypes ST
		LEFT JOIN (Select stCode, max(svyYear) y
			from SurveyYearSchoolTypes GROUP BY stCode) MaxY
			ON ST.stCode = MaxY.stCode
		LEFT JOIN SurveyYearSchoolTypes SYST
			ON MaxY.stCode = SYST.stCode
			AND MaxY.y = SYST.svyYear
		LEFT JOIN SurveyYearSchoolTypes CUR
			ON ST.stCode = CUR.stCode
			AND CUR.svyYear = @SurveyYear
	    WHERE CUR.svyYear is null

 -- teacher qualifications are now smarter in that they find the most recent record
 -- in or prior to the reporting year, so its actually better not to copy everything over
 -- In fact, we can reduce any row that is a duplicate of the year preceding it

	DECLARE @loopYear int = @maxYear
	Select @loopYear = max(svyYear) from Survey WHERE svyYear < @loopYear
	while @loopYear is not null begin

		DELETE from SurveyYearTeacherQual
		from SurveyYearTeacherQual
		INNER JOIN
		(
		Select DISTINCT ytqSector, ytqQual,  ytqCertified, ytqQualified
		, min(SYTQ.svyYear) minYear, max(SYTQ.svyYear) maxYear
		FROM SurveyYearTeacherQual SYTQ
		WHERE SYTQ.svyYear in (@loopYear, @loopYear + 1)
		GROUP BY ytqSector, ytqQual ,ytqCertified, ytqQualified
		HAVING count(*) = 2
		and min(svyYear) + 1 = max(svyYear)
		) copied
		ON SurveyYearTeacherQual.ytqQual = copied.ytqQual
		AND isnull(SurveyYearTeacherQual.ytqSector,'') = isnull(copied.ytqSector,'')
		AND SurveyYearTeacherQual.svyYEar = copied.maxyear

		Select @loopYear = max(svyYear) from Survey WHERE svyYear < @loopYear
	end

 -- however, we will create a record for the earliest available survey year /Qual/ null Sector
 -- if no record for Qual/null sector exists in any year

    INSERT INTO [SurveyYearTeacherQual]
           ([svyYear]
           ,[ytqSector]
           ,[ytqQual]
           ,[ytqQualified]
           ,[ytqCertified])
     SELECT
			@minYear
		  , null
		  ,TQ.[codeCode]
		  , 0
		  , 0
	  FROM lkpTEacherQual TQ
		LEFT JOIN SurveyYearTeacherQual CUR
			ON TQ.CodeCode = CUR.ytqQual
			AND CUR.ytqSector is null
	    WHERE CUR.svyYear is null


	-- any settings that are in SchoolHistory history already for the new year
	-- (ie future values for authority, school type, parent, dormant)
	-- become the current values on Schools
	-- this applies to Projections used to generate teacher workforce planning
	-- not used for... a long time
	;			-- note without this ; DISABLE is a compile error
	DISABLE TRIGGER dbo.Schools_RelatedData on Schools -- becuase this tries to update SchoolYearHistory
	UPDATE Schools
		-- beware any nulls error in SI 22 03 2011
		SET schType = isnull(systCode, schools.schType)
		, schAuth = isnull(syAuth, Schools.schAuth)
		, schParent = Schools.schParent		-- so if this is cleared to be a standalone, that comes into force
		, schDormant = syDormant
		, pEditContext = 'Create Survey Year' + cast(@SurveyYear as nvarchar(4))
	FROM Schools
		INNER JOIN SchoolYEarHistory SYH
		On Schools.schNo = SYH.schNo
		AND SYH.syYear = @SurveyYear

	;
	ENABLE TRIGGER dbo.Schools_RelatedData on Schools;

	-- now populate SchoolYearHistory
	-- (the table formerly known as SurveyControl)

	INSERT INTO SchoolYearHistory
	( syYear
		, schNo
		, systCode
		, syAuth
		, syParent
		, syDormant

	)
	SELECT @SurveyYear
	, Schools.schNo
	, schType
	, schAuth
	, schParent
	, schDormant
	FROM Schools
		LEFT JOIN SchoolYEarHistory SYH
		On Schools.schNo = SYH.schNo
		AND SYH.syYear = @SurveyYear
	WHERE SYH.syID is null		-- ie no exisitng record in the year
	AND (schClosed = 0 or schClosed > @SurveyYear)

	-- and should we delete anything that is closed?

	DELETE
	FROM SchoolYEarHistory
	WHERE SchoolYearHistory.schNo in
		(Select Schools.schno from Schools
			WHERE (schClosed <> 0 and schClosed <= @surveyYear)
		)
	AND SchoolYearHistory.syYEar = @SurveyYear

commit transaction

exec pSurveyRead.CensusReadEx @surveyYear

end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()

	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	-- it is of utmost importance the triggers are on
	;
	ENABLE TRIGGER dbo.Schools_RelatedData on Schools;

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

end
GO

