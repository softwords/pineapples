SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Number of teachers at the school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolDataSetTeacherQualCertCount]
	-- Add the parameters for the stored procedure here
	@Year int
	, @SchoolNo nvarchar(50) = null
	, @GovtOther nvarchar(10) = null
	, @ReportQualCert nvarchar(1) = 'Q'			-- or 'C'
	WITH EXECUTE AS 'pineapples'
AS
BEGIN

	Select * from pSchoolRead.tfnSchoolDataSetTeacherQualCertCount(
			@Year, @SchoolNo, @GovtOther, @ReportQualCert)


END
GO

