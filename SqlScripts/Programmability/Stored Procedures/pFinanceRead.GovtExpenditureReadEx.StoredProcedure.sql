SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Brian Lewis
-- Create date: 13 08 2019
-- Description: Read multiple reordsets about school
-- =============================================
CREATE PROCEDURE [pFinanceRead].[GovtExpenditureReadEx]
 -- Add the parameters for the stored procedure here
 @fnmID int
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

 SELECT * from GovtExpenditureState WHERE fnmID = @fnmID
 SELECT * from EdExpenditureState WHERE fnmID = @fnmID

END
GO

