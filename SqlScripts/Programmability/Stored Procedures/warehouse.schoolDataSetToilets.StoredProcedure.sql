SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 9 11 2022
-- Description:	Retrieve toilet info for XY charts using warehouse
-- =============================================
CREATE PROCEDURE [warehouse].[schoolDataSetToilets]
	-- Add the parameters for the stored procedure here
	@year int
	, @dataItem nvarchar(100)
	, @filter nvarchar(10) = null
	, @schoolNo nvarchar(50) = null
	, @parameter nvarchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	if (@filter is not null) begin
		if not exists(Select codeCode from lkpLevels WHERE codeCode = @filter) begin
			raiserror('Invalid level code: %s', 16,1,@filter)
			return
		end
	end

	if not exists(select * from warehouse.washToilets WHERE surveyYear = @year) begin
		raiserror('No toilet data for year: %i', 16,1,@year)
		return
	end


	select schNo
	, isnull(sum(
	case @dataItem
		when 'usable' then Usable
		when 'total' then Total
		when 'usableM' then UsableM
		when 'usableF' then UsableF
		when 'usableC' then UsableC
		when 'totalM' then totalM
		when 'totalF' then totalF
		when 'totalC' then totalC

	end),0) DataValue
	, null Estimate
	, null Quality
	from warehouse.WashToilets
	WHERE (schNo = @schoolNo OR @schoolNo is null)
	AND surveyYear = @Year
	GROUP BY schNo
	ORDER BY schNo

END
GO

