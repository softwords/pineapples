SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2021
-- Description:
--	For censusworkbook: Mappings between local names that appear on the workbook,
--	and internal (ie canonical) names used in the database
--	On load of the censusworkbook, these translations are made in the construction
--	of the XMl that is passed to the stored proc; therefore it is the translated names that
--	appear on the xml image of the data row (e.g. tcheData on TeacherSurvey, ssData, ssWashData on SchoolSurvey
--	stueData on StudentEnrolment_ )
--
--  On creation of the censusworkbook, the column names in the sheet are translated according to these
-- mappings. the result is the name of column in the datatable used to populate this column. Ie the datatables
--  (produced by the pSurveyOps.censusRolloverXXX procs) show the canonical column names - translation is handled
-- when the workbook is created. See mergeTable in CensusWorkboook.cs for specifics
-- =============================================
CREATE PROCEDURE [pSurveyOps].[CensusWorkbookColumnMapping]
	-- Add the parameters for the stored procedure here
	@sheetName nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @OUT TABLE
	(
		ColumnName nvarchar(100)		-- the name that appears in the workbook
		, Mapping nvarchar(100)			-- the internal field name
	)

	INSERT INTO @OUT
	(ColumnName, Mapping)
	SELECT ColumnName, mapping
	FROM CensusWorkbookColumnMaps
	WHERE sheetName = @sheetName
	AND ColumnName not in (select columnName from @out)

	INSERT INTO @OUT
	(ColumnName, Mapping)
	SELECT ColumnName, mapping
	FROM CensusWorkbookColumnMaps
	WHERE sheetName is null
	AND ColumnName not in (select columnName from @out)


	-- finally get any vocab items
	INSERT INTO @OUT
	(ColumnName, Mapping)
	SELECT vocabTerm, vocabName
	FROM sysvocab
	WHERE vocabName in ('District','School No','School Type')
	AND vocabTerm not in (select columnName from @out)

	Select * from @OUT
END
GO

