SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2 12 2017
-- Description:	Polymorphic read of school inspection - returning Xml
-- When used by the WebAPI, this Xml translate3s to a structred Json object
-- The inspXml (InspectionContent) Xml object becomes a node in the resulting Xml,
-- and hence a structred property when converted to Json.
-- This delviers surveys to the client to be efficiently rendered.
-- =============================================
CREATE PROCEDURE [pInspectionRead].[WASH_ReadXml]
	-- Add the parameters for the stored procedure here
	@inspID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @photoList xml
	Select @photoList =
		(
			Select v.value('.', 'nvarchar(50)') photoId
			, v.value('../../../id[1]', 'nvarchar(10)') sourceId
			, v.value('../../../name[1]', 'nvarchar(1000)') caption
			from SchoolInspection
			CROSS APPLY inspXml.nodes('survey/group/subgroup/question[flags="PHOTO"]/answer/photo/remotePath') V(v)
			WHERE inspID = @inspID
			FOR XML PATH('photo')
		)

	Select @photoList =
	( select @photoList FOR XML PATH('photos'))

 	Select inspID
	, schNo
	, schName
	, StartDate
	, EndDate
	, InspectionYear
	, InspectedBy
	, inspTypeCode
	, InspectionType
	, InspectionResult
	, SourceId
	, InspectionClass
	, Partition
	, pCreateTag
	, pCreateUser
	, pCreateDateTime
	, pEditUser
	, pEditDateTime
	, InspectionContent
	, @photolist
	FROM pInspectionRead.SchoolInspections
	WHERE inspID = @inspID
	FOR XML PATH('inspection')
END
GO

