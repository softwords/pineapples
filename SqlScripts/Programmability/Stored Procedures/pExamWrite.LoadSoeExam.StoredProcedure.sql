SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 9 2021
-- Description:	Load an exam from Soe format Excel document
-- ExamData is the Xml representation of the List object on the first sheet of the workbook
--
-- History Log:
--   * 24/01/2022, Ghislain Hachey, Change audit trail to use @user param instead of the original_login() function
--   * 07/07/2022, Brian Lewis, Gender is in 'Gender', not 'Sex'
--   * 12/08/2022, Brian Lewis, #1198, Use common.fnParseName, not (dbo.)fnParseName
--   * 15/08/2022, Brian Lewis, Gender validation
--   * 06/09/2022, Brian Lewis, fix Missing Gender bug; incorporate structure clone
-- =============================================
CREATE PROCEDURE [pExamWrite].[LoadSoeExam]
	-- Add the parameters for the stored procedure here
	@examData xml
	, @fileReference uniqueidentifier
	, @user nvarchar(50)
	, @examYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- get the results for each item at student level
DECLARE @SoeResults TABLE
(
	rowID int
	, item nvarchar(100)
	, answer nvarchar(100)
)

DECLARE @items TABLE
(
	item nvarchar(100)
	, itemSeq int
	, Subject nvarchar(1)
	, SC nvarchar(1)
	, ExamCode nvarchar(3)
	, YearOfEd int
	, Standard nvarchar(2)
	, Benchmark nvarchar(2)
	, Indicator nvarchar(2)
	, SeqBM int
	, emd nvarchar(1)
	, answerKey nvarchar(1)
	, StandardKey nvarchar(20)
	, BenchmarkKey nvarchar(20)
	, IndicatorKey nvarchar(20)
)
DECLARE @Soe TABLE
(
	rowID int
	, item nvarchar(100)
	, answer nvarchar(10)
	, itemSeq int
	, Subject nvarchar(1)
	, SC nvarchar(1)
	, ExamCode nvarchar(3)
	, YearOfEd int
	, Standard nvarchar(2)
	, Benchmark nvarchar(2)
	, Indicator nvarchar(2)
	, SeqBM int
	, emd nvarchar(1)
	, answerKey nvarchar(1)
	, StandardKey nvarchar(20)
	, BenchmarkKey nvarchar(20)
	, IndicatorKey nvarchar(20)
	, result int
)
DECLARE @tblStandards TABLE
(
	standardID nvarchar(20)
	, standardDescription nvarchar(200)
)

DECLARE @tblBenchmarks TABLE
(
	standardID nvarchar(20)
	, benchmarkID nvarchar(20)
	, benchmarkDescription nvarchar(200)
)

DECLARE @tblIndicators TABLE
(
	standardID nvarchar(20)
	, benchmarkID nvarchar(20)
	, IndicatorID nvarchar(20)
	, IndicatorDescription nvarchar(200)
)
DECLARE @tblAchievement TABLE
(
	achievementID int
	, achievementDescription nvarchar(200)
)

DECLARE @tblStudents TABLE
(
	StudentName nvarchar(500)
	, Gender nvarchar(10)
	, StudentID nvarchar(20)
	, schNo nvarchar(50)
	, district nvarchar(100)
	, teacher nvarchar(100)
	, candidateID	uniqueidentifier
	, results xml
	, seq	int NOT NULL
	, GivenName nvarchar(100)
	, FamilyName nvarchar(100)
	, stuID uniqueidentifier
	, SchoolMatched int
	, index IX_Seq NONCLUSTERED(seq)
)
-- temp table used to accumulate indicator, benchmark and standard scores by candidate
-- using the 'item count' method
DECLARE @candidateResults TABLE
(
	CandidateID			uniqueidentifier
	, IndicatorID		int			-- indicator
	, BenchmarkID		int			-- benchmark
	, StandardID		int			-- standard
	, ExamCode			nvarchar(10)
	, AchievementLevel	int			-- achivement level
	, LevelCount		int			-- number of indicators in the standard/benchmark/indicator
									-- at the achivement level
	, Weight			decimal (10,8)		-- weighting of level count / all applicable indicators
	, StudentCount		int		-- always 1
)

DECLARE @errors TABLE
(
	rowID int
	, errorValue nvarchar(100)
	, errorMessage nvarchar(500)
)

DECLARE @warnings TABLE
(
	rowID int
	, errorValue nvarchar(100)
	, errorMessage nvarchar(500)
)
declare @numErrors int
declare @numWarnings int

DECLARE @audit TABLE
(
	clock datetime
	, msg nvarchar(500)
)


	-- what is the exam? do we need to define it?
	--
	declare @examYearStr nvarchar(10)
	declare @examTypeCreated int = 0

	declare @examID int						-- the ID of the exam in the exams table
	declare @examCode nvarchar(20)
	declare @examScoresUpdated int = 0

	declare @NumCandidates int				-- number of distinct candidates
	declare @NumResults int

	declare @FirstRow int = 0;  -- the first row of the table data in the source worksheet

--
-- establish the examYear
if (@examYear is null)
	Select @examYear = @examData.value('*[1]/@examYear', 'int')

-- examCode is now embedded in the data june 24
Select @examCode = @examData.value('*[1]/@examCode', 'nvarchar(50)')
print @examCode

-- in testing FirstRow may not always be there.... default to 1
Select @FirstRow = isnull(@examData.value('*[1]/@FirstRow', 'int'),1)

INSERT INTO @audit
SELECT getUtcDate(), 'start'
print 'Start'

INSERT INTO @SoeResults
exec pExamWrite.ParseSoeResults @examData

INSERT INTO @audit
SELECT getUtcDate(), 'parse results complete'
print 'Parse results complete'

-- verify the gender of students
INSERT INTO @errors
Select rowID
, genderCode
, 'Invalid gender - must be M or F'
from
(
SELECT s.value('@Index', 'int')	RowID
, nullif(ltrim(rtrim(s.value('(./attribute::*[lower-case(local-name(.)) = "gender"
	or lower-case(local-name(.)) = "sex"])[1]/.', 'nvarchar(10)'))),'') as GenderCode
FROM
@examData.nodes('//row') as ST(s)
) SUB
WHERE GenderCode is not null
and left(GenderCode,1) not in ('F','M')

INSERT INTO @warnings
Select rowID
, genderCode
, 'Missing gender'
from
(
SELECT s.value('@Index', 'int')	RowID
, nullif(ltrim(rtrim(s.value('(./attribute::*[lower-case(local-name(.)) = "gender"
	or lower-case(local-name(.)) = "sex"])[1]/.', 'nvarchar(10)'))),'') as GenderCode
FROM
@examData.nodes('//row') as ST(s)
) SUB
WHERE GenderCode is null

-- get a unique list of the items
INSERT INTO @items
(item)
Select DISTINCT item
FROM @soeResults

-- verify the item names
INSERT INTO @errors
Select null
, item
, 'Malformed item code. The item name must begin Item_, be 26 characters long, and conform to the Soe specification.'
FROM @items
WHERE (
	item not like 'Item[_][0-9][0-9][0-9]_[A-Z][SC][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][emdh][_][A-J][A-J][A-J]'
	OR right(item,4) not in ('_AAA','_BBB','_CCC','_DDD','_EEE','_FFF','_GGG','_HHH','_III','_JJJ')
)

INSERT INTO @errors
SELECT rowID
	, answer + ' (' + item + ')'
	, 'invalid answer - must be a single letter, or BLANK or MULT'
FROM @soeResults
WHERE answer is null or
(len(answer) = 1 and answer not like '[A-Z]') or
(len(answer) > 1 and answer not in ('blank','mult'))

-- if we have malformed items we'll probably get exceptions trying to proceed, so throw this error now

Select @numErrors = count(*) from @errors
if @numErrors > 0 begin
	print 'Errors encountered - exiting'
	Select rowID + @FirstRow rowID
	, errorValue
	, errorMessage
	from @errors
	ORDER BY rowID

	Select rowID + @FirstRow rowID
	, errorValue
	, errorMessage
	from @warnings
	ORDER BY rowID

	-- caller is expecting 3 recordsets to come back, so create a dummy 'data'
	Select null NoData
	WHERE 1=0
	Select * from @audit
	return
end

-- its safe to parse the items so we can look for inconsistencies
UPDATE @items
SET ItemSeq = convert(int, substring(item,6,3))
, [Subject] = substring(item,10,1)
, SC = substring(item,11,1)
, ExamCode = substring(item,10,1) + substring(item,12,2)
, YearOfEd = convert(int, substring(item,12,2))
, Standard = substring(item,14,2)
, Benchmark = substring(item,16,2)
, Indicator = substring(item,18,2)
, SeqBM = convert(int, substring(item,20,2))
, emd = substring(item,22,1)
, answerKey = upper(substring(item,24,1))


	INSERT INTO @warnings
	Select null
	, 'Multiple subject identifiers'
	, 'Multiple subject identifiers appear in item names e.g.: ' + min(Subject) + ', ' + max(subject) +
		' This is acceptable (and expected) only in multi-subject exams.'
	FROM @items
	HAVING count(DISTINCT subject) > 1

	INSERT INTO @errors
	Select null
	, 'Inconsistent grades'
	, 'The grade identifier in the Item Name is not consistent: ' + convert(nvarchar(2), min(YearOfEd))
	+ ', ' + convert(nvarchar(2), max(YearOfEd))
	FROM @items
	HAVING count(DISTINCT YearOfEd) > 1

-- proceeding, the item codes are OK

INSERT INTO @audit
SELECT getUtcDate(), 'items examined complete'

print 'Items examined complete'

INSERT INTO @soe
(
	rowID
	, item
	, answer
	, itemSeq
	, Subject
	, SC
	, ExamCode
	, YearOfEd
	, Standard
	, Benchmark
	, Indicator
	, SeqBM
	, emd
	, answerKey
	, StandardKey
	, BenchmarkKey
	, IndicatorKey
	, result
)
Select *
, subject + '.' + convert(nvarchar(2), YearOfEd)
		+ '.' + convert(nvarchar(2), convert(int, Standard))
		StandardKey
, subject + '.' + convert(nvarchar(2), YearOfEd)
		+ '.' + convert(nvarchar(2), convert(int, Standard))
		+ '.' + convert(nvarchar(2), convert(int, Benchmark))
		BenchmarkKey

, subject + '.' + convert(nvarchar(2), YearOfEd)
		+ '.' + convert(nvarchar(2), convert(int, Standard))
		+ '.' + convert(nvarchar(2), convert(int, Benchmark))
		+ '.' + convert(nvarchar(2), convert(int, Indicator))
		IndicatorKey

, case when charindex(answer, answerKey) > 0 then 1 else 0 end result
from
(
Select rowID, item, answer
, convert(int, substring(item,6,3)) ItemSeq
, substring(item,10,1) [Subject]
, substring(item,11,1) SC
, coalesce(@ExamCode, substring(item,10,1) + substring(item,12,2)) ExamCode
, convert(int, substring(item,12,2)) YearOfEd
, substring(item,14,2) Standard
, substring(item,16,2) Benchmark
, substring(item,18,2) Indicator
, convert(int, substring(item,20,2))  SeqBM
, substring(item,22,1) emd
, upper(substring(item,24,1)) answerKey
from @SoeResults
) SUB
ORDER BY rowID, Standard, Benchmark, Indicator, SeqBM, ItemSeq

-- get the number of candidates and the totall number of results
-- 1 result = 1 item x 1 candidate
Select @numCandidates = max(rowID) + 1		-- becuase rowID is 0-based
, @numResults = count(*)
from @soe

-- the examCode is the subject combined with the year of Ed - or the exam code passed in
-- if it wasn't defined on input, its now derived from the Item structure
Select TOP 1 @examCode = ExamCode
from @soe

print @examCode
INSERT INTO @audit
SELECT getUtcDate(), 'soe temp table loaded'
--
	--- Step 1 pick up the standards, benchmark and achievement definition
	INSERT INTO @tblStandards
	SELECT DISTINCT StandardKey
		, StandardKey
	FROM @Soe


	INSERT INTO @tblBenchmarks
	SELECT DISTINCT StandardKey
		, BenchmarkKey
		, BenchmarkKey
	FROM @Soe

	INSERT INTO @tblIndicators
	SELECT DISTINCT StandardKey
		, BenchmarkKey
		, IndicatorKey
		, IndicatorKey
	FROM @Soe

	INSERT INTO @tblAchievement
	VALUES( 1, 'Beginning')
	, (2, 'Developing')
	, (3, 'Proficient')
	, (4, 'Advanced')

	INSERT INTO @tblStudents
	(
	StudentName
	, Gender
	, StudentID
	, schNo
	, district
	, teacher
	, candidateID
	, results
	, seq
	)
	Select
		replace(s.value('@FullName','varchar(500)'),char(34),'') AS StudentName
		-- we have already verified that left(gender, 1) is F or M
		, left(nullif(ltrim(rtrim(s.value('(./attribute::*[lower-case(local-name(.)) = "gender"
			or lower-case(local-name(.)) = "sex"])[1]/.', 'nvarchar(10)'))),''),1) as GenderCode
		, s.value('@Student_ID', 'nvarchar(20)') as StudentID -- ignored??
		, nullif(s.value('@SchID', 'nvarchar(50)'),'') as schNo
		-- version 2 files do not contain state, but derive it from the school
		-- however, if the state name is provided, we can at least verify it
		-- against the EMIS version and report errors
		, nullif(s.value('@Atoll_Island', 'nvarchar(50)') , '')  -- ignored
		, nullif(s.value('@Teacher', 'nvarchar(100)') , '')  -- ignored

		, newid()

		, s.query('.')				-- store the whole xml row
		, s.value('@Index', 'int')	-- sequence ie the row number into the table
	FROM @examData.nodes('//row') as ST(s)

	UPDATE @tblStudents
		SET GivenName = N.FirstName
		, FamilyName = N.LastName
		FROm @tblStudents
		CROSS APPLY common.fnParseName(StudentName) N
INSERT INTO @audit
SELECT getUtcDate(), 'students name parsed'

--- Student Name Matching

DECLARE @StudentMatches TABLE
(
	seq int,
	stuID uniqueidentifier,
	examSchool nvarchar(50),		-- school in the exams data - a copy of @tblStudents
	enrolSchool	nvarchar(50)		-- Studentenrolment school in exam year
)

-- collect possible student matches

INSERT INTO @StudentMatches
(seq, stuID, ExamSchool, EnrolSchool)
SELECT seq, S.stuID, [@tblStudents].schNo, SE.schNo
from @tblStudents
	INNER JOIN Student_ S
	ON (
		GivenName = stuGiven
		AND FamilyName = stuFamilyName
		)
	OR
		(
		GivenName = stuFamilyName
		AND FamilyName = stuGiven
		)
	INNER JOIN StudentEnrolment_ SE
		ON S.stuID= SE.stuID
		AND SE.stueYear = @examYear


INSERT INTO @audit
SELECT getUtcDate(), 'collect student name matches'

-- update the student number field for exact matches of name and school

	UPDATE @tblStudents
	SET stuID = S.stuID
	, SchoolMatched = case when EnrolSchool = ExamSchool then 1 else 0 end

	from @tblStudents
	INNER JOIN @StudentMatches S
	ON [@tblStudents].seq = S.seq

INSERT INTO @audit
SELECT getUtcDate(), 'students exact matches identified'


INSERT INTO @audit
SELECT getUtcDate(), 'students load complete'


	-- some preliminary verifications
	-- exam year must be specified
	INSERT INTO @errors
	Select null
	, 'Missing Exam Year'
	, 'The Year of the exam is not supplied. Create a new worksheet in the workbook, named "ExamYear", and enter the 4-digit year (e.g. "2019") in cell A1 of that sheet'
	WHERE @examYear is null or @examYear = 0

	-- are the schools OK?
	INSERT INTO @errors
	Select DISTINCT T.seq
	, '<missing school>'
	, 'Missing school number'
	FROM @tblStudents T
	WHERE schNo is null

	INSERT INTO @audit
	SELECT getUtcDate(), 'log missing schools'

	INSERT INTO @errors
	Select DISTINCT T.seq
	, schNo
	, 'unknown school number'
	FROM @tblStudents T
	WHERE schNo is not null
	AND schNo not in (Select schNo from Schools)

	INSERT INTO @audit
	SELECT getUtcDate(), 'log unknown schools'

	-- Teachers
	INSERT INTO @errors
	Select seq RowID
	, '<missing teacher>'
	, 'Teacher name is blank'
	FROM @tblStudents T
	WHERE Teacher is null

	INSERT INTO @audit
	SELECT getUtcDate(), 'log missing teachers'

	-- a duplicated student is an error
	INSERT INTO @errors
	Select T.seq
	, T.StudentName + ' (' + T.schNo + ')'
	, 'Duplicate of student name within school'
	FROM
	@tblStudents T
	INNER JOIN
	(
	Select StudentName, schNo
	FROM @tblStudents T
	GROUP BY StudentName, schNo
	HAVING count(*) > 1
	) SUB
	ON T.StudentName = SUB.StudentName
	AND T.SchNo = SUB.schNo

	INSERT INTO @audit
	SELECT getUtcDate(), 'log duplicate pupils'

	INSERT INTO @warnings
	Select *
	FROM
	(
	Select min(Seq) RowID
	, '<Student Name>' Item
	, 'Student name not provided on ' + convert(nvarchar(4), count(*)) + ' rows'  msg
	FROM @tblStudents T
	WHERE StudentName is null
	) SUB
	WHERE rowID is not null

	INSERT INTO @audit
	SELECT getUtcDate(), 'log missing students'

	INSERT INTO @warnings
	Select seq
	, T.StudentName + ' (' + T.schNo + ') Given: '  + isnull(T.GivenName,'') + ' Family: ' + isnull(T.FamilyName,'')
	, 'No student matching this name could be found in the Students list.'
	FROM
	@tblStudents T
	WHERE stuID is null

	INSERT INTO @audit
	SELECT getUtcDate(), 'log unknown students'
	-- student name not matched with *any* enrolment at the nominated school

	INSERT INTO @warnings
	Select DISTINCT seq					-- DISTINCT becuase there may be matching Student_
	, T.StudentName + ' (' + T.schNo + ')'
	, 'No enrolment for any student named ' + T.StudentName + ' at school ' + T.schNo
	FROM
	@tblStudents T
	WHERE stuID is not null
	AND SchoolMatched = 0

	INSERT INTO @audit
	SELECT getUtcDate(), 'log student not at reported school'

-- report the validations. Note that the collected row item is the 'logical' (0-based0 row of data in the table
-- to convert this to a Row number in the source Excel, add the value of FirstRow
	Select rowID + @FirstRow rowID
	, errorValue
	, errorMessage
	from @errors
	ORDER BY rowID
	Select @numErrors = @@ROWCOUNT

	Select rowID + @FirstRow rowID
	, errorValue
	, errorMessage
	from @warnings
	ORDER BY rowID
	Select @numWarnings = @@ROWCOUNT

INSERT INTO @audit
SELECT getUtcDate(), 'Error reporting complete'

	-- abort here if validations failed
	if (@numErrors > 0 ) begin
		-- caller is expecting 4 recordsets to come back, so create a dummy 'data'
		Select null NoData
		WHERE 1=0
		Select * from @audit

		return 1
	end
	-- moving beyond the verifications

	-- the examCode needs to be defined in the lkpExamType table
	if not exists (Select exCode from lkpExamTypes WHERE exCode = @examCode) begin
	-- insert it
		INSERT INTO lkpExamTypes
		(exCode, exName)
		VALUES (@examCode, @examCode + '??')

		SELECT @examTypeCreated = 1
	end

	-- does the exam instance exists in Exams table ? If so, we are reloading everything
	-- either way we get @examID - the exId into Exams table
	SELECT @examID = exID
	FROM Exams
	WHERE exCode = @examCode
	AND exYear = @examYear
	if @@ROWCOUNT = 0 begin
		INSERT INTO Exams
		(exCode, exYear, exUser, exDate, exCandidates, exMarks, docID)
		VALUES
		( @examCode
		, @examYear
		, @user
		, getdate()
		, @numCandidates
		, @NumResults
		, @fileReference
		)

		SELECT @examID = SCOPE_IDENTITY()
	end
	else begin
	UPDATE Exams
		SET exUser = @user
		, exDate = getdate()
		, exCandidates = @numCandidates
		, exMarks = @numResults
		, docID = @fileReference
		WHERE exID = @examID
	end

	-- if proceding, delete any existing records from target tables

	DELETE FROM ExamCandidateItems
	WHERE excID in (Select excID from ExamCandidates WHERE exID =  @examID)

	DELETE FROM ExamCandidateResults
	WHERE excID in (Select excID from ExamCandidates WHERE exID =  @examID)

	DELETE FROM ExamCandidates
	WHERE exID = @examID

	-- Standards, Benchmarks, Indicators, items
	DELETE from ExamItems
	WHERE exID = @examID

	DELETE from ExamIndicators
	WHERE exID = @examID

	DELETE FROM ExamBenchmarks
	WHERE exID = @examID

	DELETE from ExamStandards
	WHERE exID = @examID

	DELETE From ExamAchievementLevels
	WHERE exID = @examID


	-- start to save data

	INSERT INTO ExamStandards
	( exstdCode
	, exID
	, exstdDescription
	)
	Select StandardID
	, @examID
	, StandardDescription
	FROM @tblStandards

	INSERT INTO ExamBenchmarks
	( exbnchCode
	, exID
	, exbnchDescription
	, exstdID
	)
	Select BenchmarkID
	, @examID
	, BenchmarkDescription
	, exStdID
	FROM @tblBenchmarks B
	INNER JOIN ExamStandards STD
		ON B.StandardID = STD.exstdCode
		AND STD.exID = @examID

	INSERT INTO ExamIndicators
	( exindCode
	, exID
	, exindDescription
	, exbnchID
	, exStdID
	)
	Select IndicatorID
	, @examID
	, IndicatorDescription
	, exbnchID
	, exStdID
	FROM @tblIndicators I
	INNER JOIN ExamBenchmarks BNCH
		ON I.BenchmarkID = BNCH.exbnchCode
		AND BNCH.exID = @examID

	INSERT INTO ExamItems
	( exitemCode
		, exID
		, StandardKey
		, BenchmarkKey
		, IndicatorKey
		, exitemSeq
		, exitemBMSeq
	)
	SELECT DISTINCT item
	, @examID
	, StandardKey
	, BenchmarkKey
	, IndicatorKey
	, ItemSeq
	, SeqBM
	FROM @Soe
		INNER JOIN ExamIndicators IND
		ON IND.exID = @examID
		AND IND.exindDescription = IndicatorKey


	INSERT INTO ExamAchievementLevels
	( exID
	, exalVal
	, exalDescription
	)
	Select @examID
	, achievementID
	, achievementDescription
	FROM @tblAchievement

	-- we can now clone the frameset from an earlier exam of this type
	-- wrap all this in a try-catch becuase we dont want to fail the upload on account of this.....
	begin try
		-- pass null as the fromYear will clone from the last year before @examYear
		-- set returnResults to false in this case so we don't get an extra recordset
		exec pExamWrite.CloneExamFrameBulk @examCode, null, @examYear, 0
		-- I learned that raiserror with Severity 0 is a neat way to print a message, supporting printf-style substitution
		raiserror('Exam clone successful', 0,1)
	end try
	begin catch
		declare @s nvarchar(500) = ERROR_MESSAGE()
		raiserror('Exam Clone failed: %s' ,0,1, @s)
	end catch

	-- Candidates - the set of candidates for the exam
	-- this is one-to-one with rows of the input xml

	INSERT INTO ExamCandidates
	( excID
	, studentID
	, exID
	, excGiven
	, excMiddleNames
	, excFamilyName
	, excGender
	, dID
	, schNo
	, excData
	, excSeq
	, stuID
	)
	SELECT candidateID
	, nullif(StudentID , '')
	, @examID
	, N.FirstName
	, N.MiddleName
	, N.LastName
	, left(Gender,1)
	, D.dID
	, T.schNo
	, T.results
	, T.seq
	, SM.stuID
	FROM @tblStudents T
	LEFT JOIN Schools S
		ON T.schNo = S.schNo
		LEFT JOIN Islands I
			ON S.iCode = I.iCode
		LEFT JOIN Districts D
			ON I.iGroup = D.dID
	CROSS APPLY common.fnParseName(T.StudentName) N
	LEFT JOIN @StudentMatches SM
		ON T.seq = SM.seq
		AND SM.examSchool = SM.enrolSchool


	INSERT INTO @audit
SELECT getUtcDate(), 'ExamCandidates loaded'

	------ try to find the stuID
	------ match the name, the year and the school
	----UPDATE ExamCandidates
	----SET stuID = SE.stuID
	----from ExamCandidates
	----INNER JOIN Exams EX
	----	ON ExamCandidates.exID = EX.exID
	----INNER JOIN StudentEnrolment SE
	----ON ExamCandidates.excGiven = stuGiven
	----AND excFamilyName = stuFamilyName
	----AND ExamCandidates.schNo = SE.schNo
	----AND (ex.exYear = SE.stueYear)
	----WHERE ExamCandidates.exID = @examID

INSERT INTO @audit
SELECT getUtcDate(), 'ExamCandidates stuID update'

	-- try to find the teacher
	UPDATE ExamCandidates
	SET tID = TS.tID
	from ExamCandidates
		CROSS APPLY common.fnParseName(ExamCandidates.excData.value('(//row/@Teacher)[1]','nvarchar(200)')) N
	INNER JOIN Exams EX
		ON ExamCandidates.exID = EX.exID
	INNER JOIN pTeacherRead.TeacherSurveyV TS
	ON ExamCandidates.excGiven = N.FirstName
	AND excFamilyName = N.LastName
	AND ExamCandidates.schNo = TS.schNo
	AND (ex.exYear = TS.svyYear)
	WHERE ExamCandidates.exID = @examID

	INSERT INTO @audit
	SELECT getUtcDate(), 'ExamCandidates teacher match completed'
	-- now the grande finale
	-- write the results

	INSERT INTO ExamCandidateItems
	( excID
	, exitemCode
	, exciResponse
	, exciResult
	)
	Select EC.excID
	, R.item
	, R.answer
	, R.result
	from @soe R
	INNER JOIN ExamCandidates EC
		ON R.rowID = EC.excSeq
		AND EC.exID = @examID

	INSERT INTO @audit
	SELECT getUtcDate(), 'ExamCandidatesItems'

-- Now record the candidate Levels at the indicator benchmark and standard points
-- first indicator
	INSERT INTO @CandidateResults
	( CandidateID, IndicatorID, AchievementLevel, LevelCount, Weight, StudentCount)

	Select SUB.excID, exindID
	, case when Result = 0 then 1 else convert(int, ceiling((Result * NumLevels )/ NumItems)) end Level
	, 1			-- 1 = number of indicators in this indicator at this level ; ie trivially 1
	, 1			-- weight is one at this level out of 1 total
	, 1			-- studentcount is always 1
	FROM
	(
	Select EC.exID
	, ECI.excID
	, exindID
	, sum(1.00) NumItems
	, sum(ECI.excIResult) Result

	from ExamCandidateItems ECI
		INNER JOIN ExamCandidates EC
			ON ECI.excID = EC.excID
		INNER JOIN ExamItems I
			ON I.exID = EC.exID
			AND I.exitemCode = ECI.exitemCode
		INNER JOIN ExamIndicators EXI
			ON EXI.exindCode = I.IndicatorKey
			AND EXI.exID = EC.exID
	WHERE EC.exID = @examID
	GROUP BY EC.exID, ECI.excID, exindID

	) SUB
	LEFT JOIN (Select exID, count(*) NumLevels from ExamAchievementLevels GROUP BY exID) NL
		ON NL.exID = SUB.exID

-- benchmark - 'itemcount' indicator
	INSERT INTO @CandidateResults
	( CandidateID, BenchmarkID, AchievementLevel, StudentCount)

	Select SUB.excID, exbnchID
	, case when Result = 0 then 1 else convert(int, ceiling((Result * NumLevels )/ NumItems)) end Level
	, 1
	FROM
	(
	Select EC.exID
	, ECI.excID
	, exbnchID
	, sum(1.00) NumItems
	, sum(ECI.exciResult) Result

	from ExamCandidateItems ECI
		INNER JOIN ExamCandidates EC
			ON ECI.excID = EC.excID
		INNER JOIN ExamItems I
			ON I.exID = EC.exID
			AND I.exitemCode = ECI.exitemCode
		INNER JOIN ExamIndicators EXI
			ON EXI.exindCode = I.IndicatorKey
			AND EXI.exID = EC.exID
	WHERE EC.exID = @examID
	GROUP BY EC.exID, ECI.excID, EXI.exbnchID

	) SUB
	LEFT JOIN (Select exID, count(*) NumLevels from ExamAchievementLevels GROUP BY exID) NL
		ON NL.exID = SUB.exID

-- standard - 'itemcount' indicator
	INSERT INTO @CandidateResults
	( CandidateID, StandardID, AchievementLevel, StudentCount)

	Select SUB.excID, exstdID
	, case when Result = 0 then 1 else convert(int, ceiling((Result * NumLevels )/ NumItems)) end Level
	, 1
	FROM
	(
	Select EC.exID
	, ECI.excID
	, exstdID
	, sum(1.00) NumItems
	, sum(ECI.exciResult) Result

	from ExamCandidateItems ECI
		INNER JOIN ExamCandidates EC
			ON ECI.excID = EC.excID
		INNER JOIN ExamItems I
			ON I.exID = EC.exID
			AND I.exitemCode = ECI.exitemCode
		INNER JOIN ExamIndicators EXI
			ON EXI.exindCode = I.IndicatorKey
			AND EXI.exID = EC.exID

	WHERE EC.exID = @examID
	GROUP BY EC.exID, ECI.excID, EXI.exstdID

	) SUB
	LEFT JOIN (Select exID, count(*) NumLevels from ExamAchievementLevels GROUP BY exID) NL
		ON NL.exID = SUB.exID


-- candidate result for the entire exam
	INSERT INTO @CandidateResults
	( CandidateID, ExamCode, AchievementLevel, StudentCount)

	Select SUB.excID, @examCode
	, case when Result = 0 then 1 else convert(int, ceiling((Result * NumLevels )/ NumItems)) end Level
	, 1
	FROM
	(
	Select EC.exID
	, ECI.excID
	, sum(1.00) NumItems
	, sum(ECI.exciResult) Result

	from ExamCandidateItems ECI
		INNER JOIN ExamCandidates EC
			ON ECI.excID = EC.excID
	WHERE EC.exID = @examID
	GROUP BY EC.exID, ECI.excID

	) SUB
	LEFT JOIN (Select exID, count(*) NumLevels from ExamAchievementLevels GROUP BY exID) NL
		ON NL.exID = SUB.exID

	-- now record the Soe-style 'weighted' candidate-benchmarks and candidate-standard
	-- Level count  and Weighted totals for indicators are a trivial case - always 1
	-- this has already been added...
	-- so now add weighted benchmarks...
	INSERT INTO @CandidateResults
	( CandidateID
	, BenchmarkID
	, AchievementLevel
	, LevelCount
	, Weight
	)

	Select ECR.CandidateID
	, XI.exbnchID
	, ECR.AchievementLevel
	, count(*) CountAtLevel
	, count(*) / convert(float, NumIndicators) CountAtLevelW
	from @CandidateResults ECR
		INNER JOIN ExamCandidates EC
			ON EC.excID = ECR.CandidateID
		INNER JOIN ExamIndicators XI
			ON Ec.exID = XI.exID
			AND ECR.IndicatorID = XI.exindID
		INNER JOIN
		(
		-- this subquery is just to get the number of indicators within the benchmark
		-- allow this to depend on the student
		Select ECR.CandidateID
		, XI.exbnchID
		, count(*) NumIndicators
		from @candidateResults ECR
			INNER JOIN ExamCandidates EC
				ON EC.excID = ECR.CandidateID
			INNER JOIN ExamIndicators XI
				ON Ec.exID = XI.exID
				AND ECR.IndicatorID = XI.exindID
		GROUP BY ECR.CandidateID
		, XI.exbnchID
		) SUBNumIndicators
			ON ECR.CandidateID = SUBNumIndicators.CandidateID
			AND XI.exbnchID = SUBNumIndicators.exbnchID
	GROUP BY ECR.CandidateID
	, XI.exbnchID
	, ECR.AchievementLevel
	, NumIndicators

	INSERT INTO @audit
	SELECT getUtcDate(), '@CandidateResults benchmarks completed'

	-- same logic gives the weighted totals by standard
	INSERT INTO @CandidateResults
	( CandidateID
	, StandardID
	, AchievementLevel
	, LevelCount
	, Weight
	)
	Select ECR.CandidateID
	, XI.exstdID
	, ECR.AchievementLevel
	, count(*) CountAtLevel
	, count(*) / convert(float, NumIndicators) CountAtLevelW
	from @candidateResults ECR
		INNER JOIN ExamCandidates EC
			ON EC.excID = ECR.CandidateID
		INNER JOIN ExamIndicators XI
			ON Ec.exID = XI.exID
			AND ECR.IndicatorID = XI.exindID
		INNER JOIN
		(
		-- this subquery is just to get the number of indicators within the benchmark
		-- allow this to depend on the student
		Select ECR.CandidateID
		, XI.exstdID
		, count(*) NumIndicators
		from @CandidateResults ECR
			INNER JOIN ExamCandidates EC
				ON EC.excID = ECR.CandidateID
			INNER JOIN ExamIndicators XI
				ON Ec.exID = XI.exID
				AND ECR.IndicatorID = XI.exindID
		GROUP BY ECR.CandidateID
		, XI.exstdID
		) SUBNumIndicators
			ON ECR.CandidateID = SUBNumIndicators.CandidateID
			AND XI.exstdID = SUBNumIndicators.exstdID
	GROUP BY ECR.CandidateID
	, XI.exstdID
	, ECR.AchievementLevel
	, NumIndicators

	INSERT INTO @audit
	SELECT getUtcDate(), '@CandidateResults standards completed'

		-- finally levelcoutn and weight across the whole exam
	INSERT INTO @CandidateResults
	( CandidateID
	, ExamCode
	, AchievementLevel
	, LevelCount
	, Weight
	)
	Select ECR.CandidateID
	, @examCode
	, ECR.AchievementLevel
	, count(*) CountAtLevel
	, count(*) / convert(float, NumIndicators) CountAtLevelW
	from @candidateResults ECR
		INNER JOIN ExamCandidates EC
			ON EC.excID = ECR.CandidateID
		INNER JOIN ExamIndicators XI
			ON Ec.exID = XI.exID
			AND ECR.IndicatorID = XI.exindID
		INNER JOIN
		(
		-- this subquery is just to get the number of indicators within the benchmark
		-- allow this to depend on the student
		Select ECR.CandidateID
		, count(*) NumIndicators
		from @CandidateResults ECR
			INNER JOIN ExamCandidates EC
				ON EC.excID = ECR.CandidateID
			INNER JOIN ExamIndicators XI
				ON Ec.exID = XI.exID
				AND ECR.IndicatorID = XI.exindID
		GROUP BY ECR.CandidateID
		) SUBNumIndicators
			ON ECR.CandidateID = SUBNumIndicators.CandidateID
	GROUP BY ECR.CandidateID
	, ECR.AchievementLevel
	, NumIndicators

	INSERT INTO @audit
	SELECT getUtcDate(), '@CandidateResults Exam totals completed'

	-- combine the ItemCount and LevelCount values on the same record
	-- to produce the final output
	INSERT INTO ExamCandidateResults
	( excID
	, exindID
	, exbnchID
	, exstdID
	, exCode
	, excrLevel
	, excrLevelCount
	, excrWeight
	, excrCandidateCount
	)
	Select CandidateID
	, IndicatorID
	, BenchmarkID
	, StandardID
	, ExamCode
	, AchievementLevel
	, sum(ECR.LevelCount)
	, sum(ECR.Weight)
	, sum(ECR.StudentCount)
	FROM @candidateResults ECR
	GROUP BY
	CandidateID
	, IndicatorID
	, BenchmarkID
	, StandardID
	, ExamCode
	, AchievementLevel

	INSERT INTO @audit
	SELECT getUtcDate(), 'ExamCandidateResults written'

	-- build the warehouse table entries for this exam
	-- Note that a full warehouse build may entirely recreate these tables

	exec warehouse.buildExamResults @examID, null

	INSERT INTO @audit
	SELECT getUtcDate(), 'warehouse build completed'


	-- build the legacy table ExamStandardTest
	DELETE FROM ExamStandardTest
	WHERE exId = @examID

	-- easiest to do this using the warehouse crosstab, which is closest to
	-- the shape of ExamStandardTest

	INSERT INTO ExamStandardTest
	( exID
	, schNo
	, stschName
	, stGender
	, stSubject		-- IMPORTANT we cant sum components ( ie benchmarks) to get subjects (ie standards)
	, stComponent
	, st0
	, st1
	, st2
	, st3
	, st4
	, st5
	, st6
	, st7
	, st8
	, st9
	)
Select examID
, schNo
, schoolName
, Gender
, S.exstdCode   --  here for historical compatibility, but this value should not be used to get 'totals by standard'
, [Key] -- benchmark
, sum(case when achievementLevel = 0 then candidateCount end) [0]
, sum(case when achievementLevel = 1 then candidateCount end) [1]
, sum(case when achievementLevel = 2 then candidateCount end) [2]
, sum(case when achievementLevel = 3 then candidateCount end) [3]
, sum(case when achievementLevel = 4 then candidateCount end) [4]
, sum(case when achievementLevel = 5 then candidateCount end) [5]
, sum(case when achievementLevel = 6 then candidateCount end) [6]
, sum(case when achievementLevel = 7 then candidateCount end) [7]
, sum(case when achievementLevel = 8 then candidateCount end) [8]
, sum(case when achievementLevel = 9 then candidateCount end) [9]

From warehouse.examSchoolResultsTyped T
INNER JOIN examBenchmarks B
	ON B.exbnchID = T.ID
	AND B.exID = T.examID
INNER JOIN ExamStandards S
	ON B.exstdID = S.exstdID
	AND S.exID = B.exID
WHERE RecordType = 'Benchmark'
AND examID = @examID
GROUP BY examID
, schNo
, schoolName
, Gender
, S.exstdCode
, [Key] -- benchmark


	INSERT INTO @audit
	SELECT getUtcDate(), 'Examstandardtest populated'

	-- finally add to the Documents_ table
	declare @logDate datetime = getUTCDate()
	UPDATE Documents_
	SET docTitle = 'Exam data ' + @examCode + ' ' + @examYearStr
	, pEditDateTime = @logDate
	, pEditUser = @user

	WHERE docID = @fileReference

	if @@ROWCOUNT = 0 begin
		INSERT INTO Documents_
		(
			docID
			, docTitle
			, docDescription
			, docDate
			, docTags
			, pCreateUser
			, pCreateDateTime
			, pEditUser
			, pEditDateTime
		)
		VALUES
		(
			@fileReference
			, 'Exam data ' + @examCode + ' ' + @examYearStr
			, null
			, @logDate
			, 'EXAM ' + @examCode + convert(nchar(4), @examYear)
			, @user
			, @logDate
			, @user
			, @logDate
		)
	end

	INSERT INTO @audit
	SELECT getUtcDate(), '@Endor processing'

	--- Return the Result
	--Select * from warehouse.ExamSchoolResultsX
	-- this will produce totals by district, and nation, but not school
	Select *
	from pExamRead.CompareLevelTyped('nation',null, null)
	WHERE examID = @examID

	INSERT INTO @audit
	SELECT getUtcDate(), 'Results returned'

	Select * from @audit
END
GO

