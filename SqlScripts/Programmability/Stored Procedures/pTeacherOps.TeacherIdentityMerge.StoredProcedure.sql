SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 10 2009
-- Description:	Teacher Identity Merge
--					SourceID is deleted
-- =============================================
CREATE PROCEDURE [pTeacherOps].[TeacherIdentityMerge]
	-- Add the parameters for the stored procedure here
	@TargetID int = 0,
	@SourceID int = 0
WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try
	declare @overlapAppt int

	;
	with X as
	(
		Select * from TeacherAppointment
		WHERE tID in (@SourceID, @TargetID)
	)
	Select @OverlapAppt = count(DISTINCT X1.taID)
	FROM X X1
	, X X2
	WHERE
	-- a timehonoured formulation for overlapping time periods
	(X1.taEndDate >= X2.taDate or X1.taEnddate is null)
		AND
	(X2.taEndDate >= X1.taDate or X2.taEnddate is null)
		AND X1.taID <> X2.taID

	If (@OverlapAppt > 0 )
		RAISERROR ('<ValidationError>
					Both teacher records have appointments that will overlap. Delete or close Appointments before attempting to merge
					</ValidationError>'
					, 16,1
					)
	begin transaction

	-- move all related records across

	-- there may be 2 records in the one year
	-- if so, we'll get a duplicate key (if in same school)
	-- or a trigger rollback (if in same year different schools)
	-- one has to go
	-- if in the same school,
	-- update any class teacher records from the Source to the Target
	-- to prepare to delete the source
	declare @DupInSchool TABLE
	(
		ssID int
		, schNo nvarchar(50)
		, svyYear int
		, Flag int
		, SourcetchsID int NULL
		, TargettchsID int NULL
	)

		declare @DupInYear TABLE
	(
		svyYear int
		, SourcetchsID int NULL
		, TargettchsID int NULL
		, NumSourceClasses int NULL
		, NumTargetClasses int NULL
	)

	INSERT INTO @DupInSchool
	SELECT ssID
		, schNo
		, svyYear
		,sum(case when tID = @SourceID then 1 else 2 end) Flag
		, min(case when tID = @SourceID then tchsID else null end) SourcetchsID
		, min(case when tID = @TargetID then tchsID else null end) TargettchsID

	FROM pTeacherRead.TeacherSurveyV
	WHERE tID in (@SourceID, @TargetID)
	GROUP BY ssID, schNo, svyYear

	INSERT INTO @DupInYear
	SELECT svyYear

		, min(case when TeacherSurvey.tID = @SourceID then TeacherSurvey.tchsID else null end) SourcetchsID
		, min(case when TeacherSurvey.tID = @TargetID then TeacherSurvey.tchsID else null end) TargettchsID
		, count( case when TeacherSurvey.tID = @SourceID then pctID else null end) NumSourceClasses
		, count( case when TeacherSurvey.tID = @TargetID then pctID else null end) NumTargetClasses

	FROM TeacherSurvey
		INNER JOIN SchoolSurvey
			ON TeacherSurvey.ssId = SchoolSurvey.ssID
		LEFT JOIN ClassTeacher CT
			ON TeacherSurvey.tchsID = CT.tchsID
	WHERE TeacherSurvey.tID in (@SourceID, @TargetID)
	GROUP BY svyYear

	Select * from @DupInSchool
	Select * from @DupInYEar

	select *
	FROM ClassTeacher
	INNER JOIN Classes
		ON ClassTeacher.pcID = Classes.pcID
	INNER JOIN	@DupInSchool DD
		ON Classes.ssID = DD.ssID
		AND ClassTeacher.tchsID = DD.SourcetchsID
	WHERE DD.Flag = 3		-- which gaurantees there is a Target tchsID not null

	Select *
	FROM TeacherSurvey
		INNER JOIN @DupInSchool DD
			ON DD.SourceTchsID = TeacherSurvey.tchsID
	WHERE DD.Flag = 3
	Select * from ClassTeacher
	WHERE tId in (@SourceID, @TargetID)
	-- if there is a TeacherSurvey at the same school for both records
	-- we'll have to delete the source. Before we do that, update any references to that
	-- tchsId to the target's tchsID

	-- FORMAT has changed on this table in that we now hold the tID, not the tchsID
	-- so its actually easier to updte this now, ust globally change the sourec tID to target TID
	UPDATE ClassTeacher
		SET tchsID = null
		, tID = @TargetID
	WHERE tID = @SourceID

	--UPDATE ClassTeacher
	--	SET tchsID = TargettchsID

	--FROM ClassTeacher
	--INNER JOIN Classes
	--	ON ClassTeacher.pcID = Classes.pcID
	--INNER JOIN	@DupInSchool DD
	--	ON Classes.ssID = DD.ssID
	--	AND ClassTeacher.tchsID = DD.SourcetchsID
	--WHERE DD.Flag = 3		-- which gaurantees there is a Target tchsID not null

	print @@rowcount
	print 'ClassTeacher updated'
		select *
	FROM ClassTeacher
	INNER JOIN Classes
		ON ClassTeacher.pcID = Classes.pcID
	INNER JOIN	@DupInSchool DD
		ON Classes.ssID = DD.ssID
		AND ClassTeacher.tchsID = DD.SourcetchsID
	WHERE DD.Flag = 3		-- which gaurantees there is a Target tchsID not null
	-- now delete the source's TeacherSurvey where there is a dup in the same year
	-- possible references to ClassTeacher have just been fixed
	DELETE FROM TeacherSurvey
	FROM TeacherSurvey
		INNER JOIN @DupInSchool DD
			ON DD.SourceTchsID = TeacherSurvey.tchsID
	WHERE DD.Flag = 3

	-- Also, we can't have 2 surveys in the same year
	-- if we do, we have to decide which one to keep
	-- if one has Classes and the other doesn;t keep it; otherwise, keep the target

	-- source has no classes, and there is a target
	-- or there is a target with classes =>Delete the source

	-- that means, we have to delete any links to that source

	-- delete the source if the target has classes
	DELETE From ClassTeacher
	FROM ClassTeacher
		INNER JOIN @DupInYear DY
			ON ClassTeacher.tchsId = DY.SourcetchsID
	WHERE  isnull(DY.NumTargetClasses,0) > 0


	-- 2 cases in this select
	DELETE FROM TeacherSurvey
	FROM TeacherSurvey
		INNER JOIN @DupInYear DY
			ON TeacherSurvey.tchsId = DY.SourcetchsID
	WHERE  isnull(DY.NumTargetClasses,0) > 0		-- there are classes against the target
	OR (DY.TargettchsID is not null and isnull(DY.NumSourceClasses,0) = 0)


	-- source has classes, target exists but does not have classes
	-- => delete the target
		print @@rowcount
		print 'After teacher survey update'

	DELETE FROM TeacherSurvey
	FROM TeacherSurvey
		INNER JOIN @DupInYear DY
			ON TeacherSurvey.tchsId = DY.TargettchsID
	WHERE  isnull(DY.NumTargetClasses,0) = 0
		AND DY.SourcetchsID is not null

	-- whatever remains can safely be moved
	UPDATE TeacherSurvey
		SET tID = @TargetID
		WHERE tID = @SourceID


	begin try
		begin transaction tryAppt		-- note we need to name the transaction
		UPDATE TeacherAppointment
			SET tID = @TargetID
			WHERE tID = @SourceID


		commit transaction tryAppt
	end try
	begin catch
		-- each of the 2 sets is OK in its own right, but when we put them together, its messy
		rollback transaction tryAppt  --else rollback rolls all the way back
	end catch


	UPDATE TeacherAttendance
		SET tID = @TargetID
		WHERE tID = @SourceID


		-- iprove handling of teachertraining - don;t create duplicates
		DELETE FROM TeacherTraining
		FROM TeacherTraining
		INNER JOIN TeacherTraining DUP
			ON TeacherTraining.tID = @SourceID
			AND DUP.tID = @TargetID
			AND TeacherTraining.trQual = DUP.trQual
			-- if they are both null, or both the same, we don't need to retain
			-- if the target has a value, and the soure doesn't dont retain
			AND coalesce(TeacherTraining.trYear, DUP.trYear ,0) = coalesce( DUP.trYear ,0)
			AND coalesce(TeacherTraining.trMajor, DUP.trMajor ,'') = coalesce( DUP.trMajor ,'')
			AND coalesce(TeacherTraining.trInstitution, DUP.trInstitution ,'') = coalesce( DUP.trInstitution ,'')
			AND coalesce(TeacherTraining.trExpirationDate, DUP.trExpirationDate ,'') = coalesce( DUP.trExpirationDate ,'')
			AND coalesce(TeacherTraining.trInstitution, DUP.trInstitution ,'') = coalesce( DUP.trInstitution ,'')

			-- now delete from the other side ie we may have some values supplied on the source not in the target
		DELETE FROM TeacherTraining
		FROM TeacherTraining
		INNER JOIN TeacherTraining DUP
			ON TeacherTraining.tID =@TargetID
			AND DUP.tID = @SourceID
			AND TeacherTraining.trQual = DUP.trQual
			-- if they are both null, or both the same, we don't need to retain
			-- if the target has a value, and the soure doesn't dont retain
			AND coalesce(TeacherTraining.trYear, DUP.trYear ,0) = coalesce( DUP.trYear ,0)
			AND coalesce(TeacherTraining.trMajor, DUP.trMajor ,'') = coalesce( DUP.trMajor ,'')
			AND coalesce(TeacherTraining.trInstitution, DUP.trInstitution ,'') = coalesce( DUP.trInstitution ,'')
			AND coalesce(TeacherTraining.trExpirationDate, DUP.trExpirationDate ,'') = coalesce( DUP.trExpirationDate ,'')
			AND coalesce(TeacherTraining.trInstitution, DUP.trInstitution ,'') = coalesce( DUP.trInstitution ,'')

	UPDATE TeacherTraining
		SET tID = @TargetID
		WHERE tID = @SourceID

	UPDATE ISTEnrol
		SET tID = @TargetID
		WHERE tID = @SourceID


	UPDATE ISTWaitList
		SET tID = @TargetID
		WHERE tID = @SourceID


	UPDATE DocumentLinks_
		SET tID = @TargetID
		WHERE tID = @SourceID


	UPDATE TeacherNotes
		SET tID = @TargetID
		WHERE tID = @SourceID

	UPDATE PAAssessment_
		SET tID = @TargetID
		WHERE tID = @SourceID

	UPDATE TeacherAttendance
		SET tID = @TargetID
		WHERE tID = @SourceID


	--delete the identity
	DELETE from TeacherIdentity
		WHERE tID = @SourceID


	-- touch the target record to trigger logging
	--UPDATE TeacherIdentity
	--	set pEditContext = 'MERGE'
	--WHERE tID = @targetID

	commit transaction


end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

