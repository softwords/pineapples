SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 6 2021
-- Description:	Utility to restrospectively put the xml from
--				the census workbook on the SchoolSurvey
-- =============================================
CREATE PROCEDURE [pSurveyOps].[censusSaveXml]
@schoolXml xml			-- xml representation of the schools sheet of the workbook
, @washXml xml
, @filereference uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @schoolXml is not null begin
	UPDATE SchoolSurvey
		set ssData = null
		WHERE ssSource = @filereference

	UPDATE SchoolSurvey
	SET ssData = d
	, ssSource = @filereference
	FRom SchoolSurvey
	INNER JOIN
	(
	Select r.value('@School_No','nvarchar(50)') schNo
	, r.value('@SchoolYear','nvarchar(50)') Y
	, right(r.value('@SchoolYear','nvarchar(50)'),4) svyYear
	, r.query('.') d
	from @schoolXml.nodes('ListObject/row') L(r)
	) SUB
	ON SchoolSurvey.schNo = SUB.schNo AND SchoolSurvey.svyYEar = SUB.svyYEar
end

if @washXml is not null begin

	UPDATE SchoolSurvey
		set ssWashData = null
		WHERE ssSource = @filereference

	UPDATE SchoolSurvey
	SET ssWashData = d
	, ssSource = @filereference
	FRom SchoolSurvey
	INNER JOIN
	(
	Select r.value('@School_No','nvarchar(50)') schNo
	, r.value('@SchoolYear','nvarchar(50)') Y
	, right(r.value('@SchoolYear','nvarchar(50)'),4) svyYear
	, r.query('.') d
	from @washXml.nodes('ListObject/row') L(r)
	) SUB
	ON SchoolSurvey.schNo = SUB.schNo AND SchoolSurvey.svyYEar = SUB.svyYear
end
END
GO

