SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [pSchoolRead].[stdSchoolFilter]
	-- Add the parameters for the stored procedure here

	@Filter XML

AS
BEGIN

	SET NOCOUNT ON;


	declare @SchoolNo   nvarchar(50)
	declare @SchoolName nvarchar(50)
    declare @fSearchAlias int -- 1 = search 2 = search for exclusion
    declare @AliasSource nvarchar(50)
    declare @SchoolType nvarchar(10)
	declare @SchoolClass nvarchar(10)
	declare @District nvarchar(10)
    declare @Island nvarchar(10)
	declare @ElectorateN nvarchar(10)
	declare @ElectorateL nvarchar(10)
	declare @Authority nvarchar(10)
	declare @Language nvarchar(10)
	declare @InfrastructureClass nvarchar(10)
	declare @InfrastructureSize nvarchar(10)
	declare @ShipService nvarchar(50)
	declare @BankService nvarchar(50)
	declare @PostService nvarchar(50)
	declare @AirService nvarchar(50)
	declare @Clinicservice nvarchar(50)
	declare @HospitalService nvarchar(50)


-- peel the values from the XML
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @filter

-- populate the table of grid values

	Select @schoolNo = SchoolNo
	,  @SchoolName = SchoolName
    ,  @fSearchAlias = fSearchAlias
    ,  @AliasSource = AliasSource
    ,  @SchoolType = SchoolType
	,  @SchoolClass = SchoolClass
	,  @District = District
    ,  @Island = Island
	,  @ElectorateN = ElectorateN
	,  @ElectorateL = ElectorateL
	,  @Authority = Authority
	,  @Language = theLanguage
	,  @InfrastructureClass = InfrastructureClass
	,  @InfrastructureSize =InfrastructureSize
	,  @ShipService = ShipService
	,  @BankService = BankService
	,  @PostService = PostService
	,  @AirService = AirService
	,  @Clinicservice = ClinicService
	,  @HospitalService = HospitalService
	from OPENXML (@idoc, '/SchoolFilter',2)
		WITH
		(

			SchoolNo   nvarchar(50)			'@SchoolNo'
			, SchoolName nvarchar(50)		'@SchoolName'
			, fSearchAlias int				'@fSearchAlias'
			, AliasSource nvarchar(50)		'@AliasSource'
			, SchoolType nvarchar(10)		'@SchoolType'
			, SchoolClass nvarchar(10)		'@SchoolClass'
			, District nvarchar(10)			'@District'
			, Island nvarchar(10)			'@Island'
			, ElectorateN nvarchar(10)		'@ElectorateN'
			, ElectorateL nvarchar(10)		'@ElectorateL'
			, Authority nvarchar(10)			'@Authority'
			, theLanguage nvarchar(10)			'@Language'
			, InfrastructureClass nvarchar(10)'@InfrastructureClass'
			, InfrastructureSize nvarchar(10)	'@InfrastructureSize'
			, ShipService nvarchar(50)			'@ShipService'

			, BankService nvarchar(50)			'@BankService'
			, PostService nvarchar(50)			'@PostService'
			, AirService nvarchar(50)			'@AirService'
			, Clinicservice nvarchar(50)		'@ClinicService'
			, HospitalService nvarchar(50)		'@HospitalService'

		)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	declare @sql nvarchar(2000)
	declare @Params nvarchar(500)
	declare @TablesToUse int
	select @TablesToUse = 0

-- rules/cases for alias search
-- no name specified, search alias flag on no action (return any school, or any school with any alias)
-- name specified, alias flag on - match schname, or any alias
-- name specified alias flag on, specific item - match name or alias in the specific list
-- no name specified, alias flag on, specific item - any item in specific alias list
-- name specified, alias flag on, specific item, NOT : any school with name like name' not incuded in the specified alias list
-- no name specified alias flag on, specific item, NOT :any school not in the specific alias list
declare @AliasMode int
select @AliasMode = 0
declare @fForceAlias int
select @fForceAlias = 0

declare @UseDistrict int
select @UseDistrict = 0

declare @UseSurvey int
select @UseSurvey = 0


create table #alias
(
	schNo nvarchar(50)
)

create table #district
(
	schNo nvarchar(50)
)

create table #survey
(
	schNo nvarchar(50)
)


if (@fSearchAlias = 1)
	select @AliasMode = 4
if (@fSearchAlias = 2)
	Select @AliasMode = 8
if (@SchoolName is not null)
	Select @AliasMode =@AliasMode + 2
if (@AliasSource is not null)
	select @AliasMode = @AliasMode +1


if (@AliasMode = 9 or @AliasMode = 11)
	-- name is blank, not in specified list
	-- SELECTED school MUST be in #ALIAS
BEGIN
	insert into #alias
	select schNo
	From Schools WHERE
		schNo not in (Select schNo from SchoolAlias WHERE saSrc = @AliasSource)
	select @fForceAlias = 1
END

if @AliasMode = 5
	-- search , in specific alias list, no name
	--
BEGIN

	insert into #alias
	select schNo
	From SchoolAlias
	WHERE saSrc = @AliasSource
	select @fForceAlias = 1
END


if  not (@District is null)
	BEGIN
		insert into #district
		select SchNo
		from
			Schools
				INNER JOIN Islands
				ON Schools.iCode = Islands.icode
		WHERE
			igroup = @District
		select @UseDistrict = 1
	end

if ((@ShipService is not null) or
	(@BankService is not null) or
	(@PostService is not null) or
	(@AirService is not null) or
	(@ClinicService is not null) or
	(@InfrastructureClass is not null) or
	(@InfrastructureSize is not null))
		BEGIN
			INSERT INTO #survey
			Select S.schNo
			from Schools S
				inner join SchoolSurvey SS on S.schNo = ss.SchNo
			WHERE
				SS.svyYear = (Select max(svyYear) from SchoolSurvey ss2 where ss2.schno = S.schNo)
				AND (ssSvcAir = @AirService or @AirService is null)
				AND (ssSvcPost = @PostService or @PostService is null)
				AND (ssSvcShip = @ShipService or @ShipService is null)
				AND (ssSvcBank = @BankService or @BankService is null)
				AND (ssSvcClinic = @ClinicService or @ClinicService is null)
				AND (ssSvcHospital = @HospitalService or @HospitalService is null)
				AND (ssISClass = @InfrastructureClass or @InfrastructureClass is null)
				AND (ssISSize = @InfrastructureClass or @InfrastructureClass is null)

				select @UseSurvey = 1
		end


Select S.schNo
from Schools S
	LEFT OUTER JOIN Islands I
		on S.iCode = I.iCode
	LEFT OUTER JOIN Districts D
		on D.dID = I.iGroup
WHERE
	(schNo = @SchoolNo or @SchoolNo is null)
	AND
		(


			(@SchoolName is null or
				(schName like @SchoolName + '%' or
					(@fSearchAlias = 1 and
						S.SchNo in
							(Select schNo from SchoolAlias WHERE saAlias like (@SchoolName + '%')
								AND (saSrc=@AliasSource OR @AliasSource is null))
					)

				)
			)
		)

	AND (schType = @schoolType or @SchoolType is null)
	AND (SchClass = @SchoolClass or @SchoolClass is null)
	AND (S.iCode = @Island or @Island is null)
	AND (schElectN = @ElectorateN or @ElectorateN is null)
	AND (schElectL = @ElectorateL or @ElectorateL is null)
	AND (schAuth = @Authority or @Authority is null)
	AND (schLang = @Language or @Language is null)

	AND (schNo = any (Select schNo from #district)
			OR @UseDistrict = 0)

	AND (schNo = any (Select schNo from #survey)
			OR @UseSurvey = 0)
	AND (schNo = any (Select schNo from #alias)
			OR @fForceAlias = 0)


if @fForceAlias <> 0
	drop table #alias
if @UseSurvey <> 0
	drop table #survey
if @UseDistrict <> 0
	drop table #district


return


END
GO

