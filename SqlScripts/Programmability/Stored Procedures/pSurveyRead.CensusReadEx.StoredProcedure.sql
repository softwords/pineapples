SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 9 2021
-- Description:	Read a Survey record, with related data
-- =============================================
CREATE PROCEDURE [pSurveyRead].[CensusReadEx]
	@surveyYear int
AS
BEGIN

-- return same data whether updating or inserting
Select * from Survey
WHERE svyYear = @SurveyYear

Select * from SurveyYearSchoolTypes
WHERE svyYear = @SurveyYear

Select * from SurveyYearTeacherQual
WHERE svyYear = @SurveyYear

-- effective TeacherQual records
exec pSurveyRead.CensusEffectiveTeacherQuals @surveyYear

END
GO

