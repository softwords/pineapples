SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------------------------------------------------------
CREATE PROCEDURE [warehouse].[buildWash]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS ON;

	print 'warehouse.Wash'
	print '--------------------------'
	print ''

	declare @ok int
	declare @counter int

	Select @counter = count(*) from pInspectionREad.SchoolInspections
	WHERE inspTypeCode = 'WASH'

	print convert(nvarchar(5), @counter) + ' Wash inspections (total)'

	if @startFromYear is not null begin
		Select @counter = count(*) from pInspectionREad.SchoolInspections
			WHERE inspTypeCode = 'WASH'
			AND InspectionYEar >= @startFRomYear

			print convert(nvarchar(5), @counter) + ' Wash inspections >= year ' + convert(nvarchar(5), @startFromYear)

	end


	-- create the table to hold the itemised responses from the survey
	DELETE from warehouse.WashDetail
	WHERE inspectionYear >= @StartFromYear or @StartFromYear is null

	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.WashDetail'

-- first add the singlevalued questions -- binary, ternary, choose single, input numeric
-- don;t bother with input or input phone - there is no meaningful way to aggregate these

INSERT INTO warehouse.WashDetail
	(schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num)
Select schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num
FROM
(
Select SI.schNo
, inspID
, InspectionYear
, BSS.surveyDimensionID
, v.value('id[1]', 'nvarchar(50)') question
, case v.value('flags[1]', 'nvarchar(50)')
	when 'CHOOSE|SINGLE' then
		v.value('(answer/item)[1]','nvarchar(100)')
	else null
end response
, null item
-- store YES NO UNKNOWN as answer
, case v.value('flags[1]', 'nvarchar(50)')
	when 'BINARY' then
		v.value('(answer/b_answer)[1]','nvarchar(100)')
	when 'TERNARY' then
		v.value('(answer/t_answer)[1]','nvarchar(100)')
	else null
end answer
, case v.value('flags[1]', 'nvarchar(50)')
	when 'INPUT|NUMERIC' then
		v.value('(answer/inputText)[1]','int')
	else 1
end num
, v.value('(flags)[1]','nvarchar(50)') flags

from
	pInspectionRead.SchoolInspections SI
	CROSS APPLY InspectionContent.nodes('//subgroup/question') as V(v)
	LEFT JOIN warehouse.bestSurvey BSS
		ON BSS.schNo = SI.schNo
		AND BSS.SurveyYear = SI.InspectionYear
	WHERE InspTypeCode = 'WASH'
	AND InspectionYear >= @StartFromYear or @StartFromYear is null
) SUB
WHERE ( Flags = ('CHOOSE|SINGLE') and nullif(response,'') is not null)
OR (Flags in ('BINARY','TERNARY') and nullif(answer,'') is not null)
OR (Flags = 'INPUT|NUMERIC' and num is not null)

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.WashDetail (scalar values)'
 -- now choose | multiple
 -- add each chsen value as a 'Response' to the question, with Num = 1 - ie
 -- they are counted when accumulated

INSERT INTO warehouse.WashDetail
	(schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num)
Select schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num
FROM
(
Select SI.schNo
, inspID
, InspectionYear
, BSS.surveyDimensionID
, v.value('../../id[1]', 'nvarchar(50)') question
, v.value('.', 'nvarchar(100)') response
, null item
, null answer
, 1 num
from
	pInspectionRead.SchoolInspections SI
	CROSS APPLY InspectionContent.nodes('//subgroup/question[flags="CHOOSE|MULTIPLE"]/answer/item') as V(v)
	LEFT JOIN warehouse.bestSurvey BSS
		ON BSS.schNo = SI.schNo
		AND BSS.SurveyYear = SI.InspectionYear
	WHERE InspTypeCode = 'WASH'
	AND InspectionYear >= @StartFromYear or @StartFromYear is null
) SUB
WHERE nullif(response,'') is not null

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.WashDetail (multiple choice)'
 -- now var|binary
 -- add each chosen value as a 'Response' , variant_item becomes item, and answer (YES/NO) is answer
 -- they are counted when accumulated

INSERT INTO warehouse.WashDetail
	(schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num)
Select schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num
FROM
(
Select SI.schNo
, inspID
, InspectionYear
, BSS.surveyDimensionID
, v.value('../../../id[1]', 'nvarchar(50)') question
, v.value('../name[1]', 'nvarchar(100)') response
, v.value('(name)[1]', 'nvarchar(100)') item
, v.value('(answer)[1]', 'nvarchar(100)') answer
, 1 num
from
	pInspectionRead.SchoolInspections SI
	CROSS APPLY InspectionContent.nodes('//subgroup/question[flags="VAR|BINARY"]/answer/variant/variant_item') as V(v)
	LEFT JOIN warehouse.bestSurvey BSS
		ON BSS.schNo = SI.schNo
		AND BSS.SurveyYear = SI.InspectionYear
	WHERE InspTypeCode = 'WASH'
	AND InspectionYear >= @StartFromYear or @StartFromYear is null
) SUB
WHERE nullif(answer,'') is not null

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.WashDetail (var/binary)'

 -- now var|input|numeric an array of numeric values
 -- add each chosen value as a 'Response' , variant_item becomes item, and answer (the numeric value)
 -- goes to num
 -- they are added when accumulated

INSERT INTO warehouse.WashDetail
	(schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num)
Select schNo
    ,inspID
    ,InspectionYear
    ,SurveyDimensionID
    ,Question
    ,Response
    ,Item
    ,Answer
    ,Num
FROM
(
Select SI.schNo
, inspID
, InspectionYear
, BSS.surveyDimensionID
, v.value('../../../id[1]', 'nvarchar(50)') question
, v.value('../name[1]', 'nvarchar(100)') response
, v.value('(name)[1]', 'nvarchar(100)') item
, null answer
, v.value('(answer)[1]', 'int') num
from
	pInspectionRead.SchoolInspections SI
	CROSS APPLY InspectionContent.nodes('//subgroup/question[flags="VAR|INPUT|NUMERIC"]/answer/variant/variant_item') as V(v)
	LEFT JOIN warehouse.bestSurvey BSS
		ON BSS.schNo = SI.schNo
		AND BSS.SurveyYear = SI.InspectionYear
	WHERE InspTypeCode = 'WASH'
	AND InspectionYear >= @StartFromYear or @StartFromYear is null
) SUB
WHERE nullif(num,'') is not null

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.WashDetail (numeric array)'

-- some sanity checks

Select @counter = count(*) from
(
Select DISTINCT schNo, inspID from warehouse.WashDetail
) S
print convert(nvarchar(5), @counter) + ' Distinct school inspections in WashDetail'

Select @counter = count(*) from
(
Select DISTINCT schNo, InspectionYear from warehouse.WashDetail
) S
print convert(nvarchar(5), @counter) + ' Distinct school/inspection year in WashDetail'

print ''
print 'warehouse.WashTable'
print ''

DELETE from warehouse.WashTable
WHERE SurveyYear >= @StartFromYear or @StartFromYear is null

print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.WashTable'

INSERT INTO warehouse.WashTable
(
SurveyYear
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
, Question
, Response
, Item
, Answer
, Num
, NumThisYear
)
Select  SurveyYear
, DSS.[District Code]
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, WASH.Question
, WASH.Response
, WASH.Item
, WASH.Answer
, sum(WASH.Num) Num
, sum(case when BI.SurveyYear = BI.InspectionYear then WASH.Num end) NumThisYear

from warehouse.BestInspection BI
INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON BI.SurveyDimensionID = DSS.[Survey ID]
INNER JOIN warehouse.WashDetail WASH
	ON WASH.inspID = BI.inspID
WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
AND BI.InspectionTypeCode = 'WASH'

GROUP BY
SurveyYear , DSS.[District Code]
, DSS.District
, DSS.AuthorityCode
, DSS.Authority
, DSS.AuthorityGovtCode
, DSS.AuthorityGovt
, DSS.SchoolTypeCode
, DSS.SchoolType
, WASH.Question
, WASH.Response
, WASH.Item
, WASH.Answer


print convert(nvarchar(5), @@ROWCOUNT) + ' inserts to warehouse.WashTable'


---- wash.Toilets

----- processing of toilet data moved to separate procedure

exec warehouse.BuildWashToilets @startFromYear

---- wash.Water

print ''
print 'warehouse.WashWater'
print ''

--- This allows the generation of school level water source numbers, and relies on
--- hardcoded values from the WASH survey.
--- Created as a table rather than a view ? to facilitate aggregations
--- No need to incorporate enrolment data like with toilet I think


--- here are the hardcoded strings from the WASH survey Xml that we rely on
--- These are items in question CW.W.1

declare @waterQuestionTag nvarchar(30) = 'CW.W.1'

declare @pipedWaterSupplyTag nvarchar(30) = 'Piped water supply'
declare @protectedWellSpringTag nvarchar(30) = 'Protected well/spring'
declare @unprotectedWellSpringTag nvarchar(30) = 'Unprotected well/spring'
declare @rainwaterTag nvarchar(30) = 'Rainwater'
declare @bottledWaterTag nvarchar(30) = 'Bottled water'
declare @tankerTruckCartTag nvarchar(30) = 'Tanker/Truck or cart'
declare @surfacedWaterTag nvarchar(50) = 'Surfaced water (Lake, River, Stream)'

declare @currentlyAvailableTag nvarchar(100) = 'Currently Available'
declare @usedForDrinkingTag nvarchar(100) = 'Used for Drinking'

--declare @ok int
Select @ok = count(*)
from warehouse.washDetail
where Question = @waterQuestionTag
	and (
		( Response is not null
			and Response not in (@pipedWaterSupplyTag, @protectedWellSpringTag, @unprotectedWellSpringTag, @rainwaterTag, @bottledWaterTag, @tankerTruckCartTag, @surfacedWaterTag)
		) OR
		( item is not null
			and item not in (@currentlyAvailableTag, @usedForDrinkingTag)
		)
	)

	if isnull(@OK,0) > 0 begin
	print ''
	print '****************************'
		print 'Invalid reponses or items were found for Water data in WashDetails'
		print 'Check the master template in lkpInspectionTypes on record WASH'
		print '****************************'
		print ''

	end

	DELETE from warehouse.WashWater
	WHERE SurveyYear >= @StartFromYear or @StartFromYear is null

	print convert(nvarchar(5), @@ROWCOUNT) + ' deletes from warehouse.WashWater'


-- outer query to insert into table
INSERT INTO warehouse.WashWater
(schNo
, SurveyYear
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType

, inspID -- the school inspection ID for the wash survey providing the toilet data
, InspectionYear
, PipedWaterSupplyCurrentlyAvailable
, PipedWaterSupplyUsedForDrinking
, ProtectedWellCurrentlyAvailable
, ProtectedWellUsedForDrinking
, UnprotectedWellSpringCurrentlyAvailable
, UnprotectedWellSpringUsedForDrinking
, RainwaterCurrentlyAvailable
, RainwaterUsedForDrinking
, BottledWaterCurrentlyAvailable
, BottledWaterUsedForDrinking
, TankerTruckCartCurrentlyAvailable
, TankerTruckCartUsedForDrinking
, SurfacedWaterCurrentlyAvailable
, SurfacedWaterUsedForDrinking
)
Select schNo
, SurveyYear
, max(DistrictCode) DistrictCode
, max(District) District
, max(AuthorityCode) AuthorityCode
, max(Authority) Authority
, max(AuthorityGovtCode) AuthorityGovtCode
, max(AuthorityGovt) AuthorityGovt
, max(SchoolTypeCode) SchoolTypeCode
, max(SchoolType) SchoolType
, max(inspID) inspID -- the school inspection ID for the wash survey providing the toilet data
, max(InspectionYear) InspectionYear
, max(PipedWaterSupplyCurrentlyAvailable) PipedWaterSupplyCurrentlyAvailable
, max(PipedWaterSupplyUsedForDrinking) PipedWaterSupplyUsedForDrinking
, max(ProtectedWellCurrentlyAvailable) ProtectedWellCurrentlyAvailable
, max(ProtectedWellUsedForDrinking) ProtectedWellUsedForDrinking
, max(UnprotectedWellSpringCurrentlyAvailable) UnprotectedWellSpringCurrentlyAvailable
, max(UnprotectedWellSpringUsedForDrinking) UnprotectedWellSpringUsedForDrinking
, max(RainwaterCurrentlyAvailable) RainwaterCurrentlyAvailable
, max(RainwaterUsedForDrinking) RainwaterUsedForDrinking
, max(BottledWaterCurrentlyAvailable) BottledWaterCurrentlyAvailable
, max(BottledWaterUsedForDrinking) BottledWaterUsedForDrinking
, max(TankerTruckCartCurrentlyAvailable) TankerTruckCartCurrentlyAvailable
, max(TankerTruckCartUsedForDrinking) TankerTruckCartUsedForDrinking
, max(SurfacedWaterCurrentlyAvailable) SurfacedWaterCurrentlyAvailable
, max(SurfacedWaterUsedForDrinking) SurfacedWaterUsedForDrinking
FROM
-- inner union query to join wash data to some school aggregates
-- assumes enrolment warehouse is up to date, uses view warehouse.EnrolSchoolR in case it might be desirable to include enrolments
(
Select SurveyYear
, BI.schNo
, null DistrictCode
, null District
, null AuthorityCode
, null Authority
, null AuthorityGovtCode
, null AuthorityGovt
, null SchoolTypeCode
, null SchoolType
, BI.inspID
, BI.InspectionYear
, Question
, max(case Response
		when @pipedWaterSupplyTag then
			case Item
				when @currentlyAvailableTag then Answer else 'No'
			end
		end) PipedWaterSupplyCurrentlyAvailable
, max(case Response
		when @pipedWaterSupplyTag then
			case Item
				when @usedForDrinkingTag then Answer else 'No'
			end
		end) PipedWaterSupplyUsedForDrinking
, max(case Response
		when @protectedWellSpringTag then
			case Item
				when @currentlyAvailableTag then Answer  else 'No'
			end
		end) ProtectedWellCurrentlyAvailable
, max(case Response
		when @protectedWellSpringTag then
			case Item
				when @usedForDrinkingTag then Answer else 'No'
			end
		end) ProtectedWellUsedForDrinking
, max(case Response
		when @UnprotectedWellSpringTag then
			case Item
				when @currentlyAvailableTag then Answer else 'No'
			end
		end) UnprotectedWellSpringCurrentlyAvailable
, max(case Response
		when @UnprotectedWellSpringTag then
			case Item
				when @usedForDrinkingTag then Answer else 'No'
			end
		end) UnprotectedWellSpringUsedForDrinking
, max(case Response
		when @rainwaterTag then
			case Item
				when @currentlyAvailableTag then Answer else 'No'
			end
		end) RainwaterCurrentlyAvailable
, max(case Response
		when @rainwaterTag then
			case Item
				when @usedForDrinkingTag then Answer else 'No'
			end
		end) RainwaterUsedForDrinking
, max(case Response
		when @bottledWaterTag then
			case Item
				when @currentlyAvailableTag then Answer else 'No'
			end
		end) BottledWaterCurrentlyAvailable
, max(case Response
		when @bottledWaterTag then
			case Item
				when @usedForDrinkingTag then Answer else 'No'
			end
		end) BottledWaterUsedForDrinking
, max(case Response
		when @tankerTruckCartTag then
			case Item
				when @currentlyAvailableTag then Answer else 'No'
			end
		end) TankerTruckCartCurrentlyAvailable
, max(case Response
		when @tankerTruckCartTag then
			case Item
				when @usedForDrinkingTag then Answer else 'No'
			end
		end) TankerTruckCartUsedForDrinking
, max(case Response
		when @surfacedWaterTag then
			case Item
				when @currentlyAvailableTag then Answer else 'No'
			end
		end) SurfacedWaterCurrentlyAvailable
, max(case Response
		when @surfacedWaterTag then
			case Item
				when @usedForDrinkingTag then Answer else 'No'
			end
		end) SurfacedWaterUsedForDrinking
from warehouse.BestInspection BI
INNER JOIN warehouse.WashDetail W
	ON BI.inspID = W.inspID
where Question = @waterQuestionTag
GROUP BY
SurveyYear
, BI.schNo
, BI.inspID
, BI.InspectionYear
, Question
UNION
Select SurveyYear
, schNo
, DistrictCode
, District
, AuthorityCode
, Authority
, AuthorityGovtCode
, AuthorityGovt
, SchoolTypeCode
, SchoolType
, null, null, null, null
, null, null, null, null
, null, null, null, null
, null, null, null, null
, null
from warehouse.enrolSchoolR
) U

GROUP BY
SchNo
, SurveyYear

print convert(nvarchar(5), @@ROWCOUNT) + ' inserts into warehouse.WashWater'

exec warehouse.logVersion

END
GO

