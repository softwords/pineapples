SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 9 2021
-- Description:	Read the calculated teacher qualifiaction status
-- for a given year
-- =============================================
CREATE PROCEDURE [pSurveyRead].[CensusEffectiveTeacherQuals]
	@surveyYear int
AS
BEGIN
-- effective TeacherQual records
Select Q.*
from SurveyYearTeacherQual Q
INNER JOIN
(Select *
, row_number() OVER (PARTITION BY ytqQual, ytqSector ORDER BY svyYear DESC) RN
from SurveyYearTeacherQual
WHERE svyYear <= @SurveyYear
) ORDERED
ON Q.ytqID = ORDERED.ytqID
AND RN = 1
	SET NOCOUNT ON;

END
GO

