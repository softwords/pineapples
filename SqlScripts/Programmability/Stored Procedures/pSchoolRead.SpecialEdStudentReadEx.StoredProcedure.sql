SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: Dec 2019
-- Description:	Extended read of special ed student for web site
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SpecialEdStudentReadEx]
	@studentID uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

    Select *
 	from SpecialEdStudent_ S
	WHERE stuID = @studentID

	Select *
	from StudentEnrolment_
	WHERE stuID = @studentID

END
GO

