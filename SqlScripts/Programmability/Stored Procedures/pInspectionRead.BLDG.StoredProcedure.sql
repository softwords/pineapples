SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pInspectionRead].[BLDG]
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select *
	from pInspectionREad.SchoolInspections
	WHERE inspID = @ID
	Select * from BuildingReview
	WHERE inspID = @ID
    -- Insert statements for procedure here

END
GO

