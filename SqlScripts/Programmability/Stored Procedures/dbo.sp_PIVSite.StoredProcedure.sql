SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 28 11 2007
-- Description:	Pivot data for school size
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- this pivot runs entirely server side becuase there is only one record per school ssurvey

select *
into #ebs
from dbo.tfnEstimate_BestSurveySize()

Select
E.LifeYear [Survey Year],
E.Estimate ,
E.bestYear [Year of Data],
E.offset [Age of Data],
E.bestssqLevel [Data Quality Level],
E.actualssqLevel [Survey Year Quality Level],

SS.ssSizeSite AS [Site Size],
SS.ssSizeFarm AS [Farm Size],
SS.ssSizeFarmUsed AS [Farm Used Size],
SS.ssSizePlayground AS [Playground Size],
SS.ssSizeRating AS [Size Rating],
ssSiteSecure AS SiteSecure,
case when isnull([ssSiteSecure],0) <> 0 then 'Y' else 'N' end AS SiteSecureYN,
DSS.*
FROM #ebs E
INNER JOIN SchoolSurvey SS
	ON E.bestssID = SS.ssID
LEFT JOIN DimensionSchoolSurveyNoYear DSS
	ON E.surveyDimensionssID = DSS.[Survey ID]

END
GO
GRANT EXECUTE ON [dbo].[sp_PIVSite] TO [pSchoolRead] AS [dbo]
GO

