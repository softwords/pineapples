SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 June 2019
-- Description:	Update the version info of the warehouse
-- =============================================
CREATE PROCEDURE [warehouse].[logVersion]
	-- Add the parameters for the stored procedure here
	@user nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@User is null)
		Select @User = original_login()
    -- Insert statements for procedure here
	begin transaction
	DELETE FROM warehouse.VersionInfo
	INSERT INTO warehouse.versionInfo
	VALUES
	(
		newID()
		, GETUTCDATE()
		, @user
	)
	commit
END
GO

