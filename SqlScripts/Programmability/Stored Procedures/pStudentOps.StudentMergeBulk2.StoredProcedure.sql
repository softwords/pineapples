SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 9 2022
-- Description:	Deduplicate student records in bulk
-- a pair will be Deduplicated iff:
-- -- 1a) the match between records is complete: given name,family name, student card, gender and dob
	-- OR 1b) the match differs only by gender, dob or card
		--		AND - the enrolments share a common school, or the aggregate enrolments form a contiguous
		--		series of years with a coherent chain of promotions
-- AND 2) there are no conflicting enrolment records (ie enrolments in same year)
-- =============================================
CREATE PROCEDURE [pStudentOps].[StudentMergeBulk2]
	-- Add the parameters for the stored procedure here
	--@PageSize int			-- maximum number to dedup - cannot be great than 1000
	--, @PageNo int = 1
	@Given nvarchar(100) = null	-- filter by given name
	, @Surname nvarchar(100) = null
	--, @Ignore nvarchar(10) = null
	, @commit int = 1			-- for testing set to 0 to rollback
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- validate the arguments


Select NE NumEnrolmentRecords, count(stuID) NumStudents
from
(
select stuID, count(*) NE
from StudentEnrolment_
GROUP BY stUID
) SUB
GROUP BY NE
ORDER BY NE


declare @K TABLE
(
stuID uniqueidentifier
, dupID uniqueidentifier
, RecNo int identity
)


declare @matchCard int = 1
declare @MatchGender int = 1
declare @MatchDoB int = 1
declare @sourceID uniqueidentifier
declare @targetID uniqueidentifier
declare @sourceS nvarchar(100)
declare @targetS nvarchar(100)

declare @lastYear int			-- for a source/target pair, the year of the most recent studentEnrolment_
declare @rec int = 1
declare @maxrec int = 1
declare @Merged int = 0;
declare @Ignored int = 0
declare @error int = 0
declare @found int

declare @Matches int

declare @rc int					-- to capture @@rowcount when necessary


INSERT INTO @K
(stuID, dupID)
Select S.stuID		-- , S.stuGiven, S.stuFamilyName, S.stueYEar, S.stueClass
, P.stuID			--, P.stuGiven, P.stuFamilyName, P.stueYEar, P.stueClass
FROM StudentEnrolment S
INNER JOIN StudentEnrolment P
ON S.stuGIven = P.stuGiven
AND S.stuFamilyName = P.stuFamilyName
AND
(
--
	(
		S.stuDoB <= dateadd(yyyy,1,P.stuDoB) and S.stuDoB >= dateadd(yyyy,-1,p.stuDoB)

	)
	OR S.stuCardID = P.stuCardID
)
--AND S.stuGender = P.stuGender
AND S.stueYEar = P.stueYEar + 1
AND (S.lvlYEar = P.lvlYEar + 1 OR (S.lvlYEar = P.lvlYEar))
AND S.stuID <> P.stuID
AND S.stuID not in (Select stuID from StudentEnrolment WHERE stueYear <= P.stueYear)
AND P.stuID not in (Select stuID from StudentEnrolment WHERE stueYear >= S.stueYear)

AND NOT EXISTS (Select stuID from StudentEnrolment WHERE
		stueYear = P.stueYear AND
		stuGiven = S.stuGiven and stuFamilyName = S.stuFamilyName AND stuID <> P.stuID
		)

AND NOT EXISTS (Select stuID from StudentEnrolment WHERE
		stueYear = S.stueYear AND
		stuGiven = S.stuGiven and stuFamilyName = S.stuFamilyName AND stuID <> S.stuID
		)
WHERE (S.stuGiven like @Given or @Given is null)
AND (S.stuFamilyName like @Surname or @surname is null)
ORDER BY S.stuFamilyName, S.stuGiven


select @found = count(*) from @K
raiserror('Student Merge Bulk2',0,1)
raiserror('%i duplicates found',0,1,@found)

if @found = 0 begin
	Select @Matches DuplicatesFound, @found SelectedToProcess, @Merged Merged, @ignored Ignored, @error Errors
	return
end

Select @rec = min(recNo)
, @maxrec = max(recNo)
from @k K

begin transaction
-- set Target to be the most recently used record, assume this one has the better set of values
Select TOP 1 @targetID = SE.stuID			-- TOP 1 not actually necessary, assigning variables from a multirow set will only ever use the first row
, @sourceID = DE.stuID
, @given = SE.stuGiven, @surname = SE.stuFamilyName
, @lastYear = SE.stueYear
from @K K
LEFT JOIN StudentEnrolment SE
	ON SE.stuID in (K.stuID)
LEFT JOIN StudentEnrolment DE
	ON DE.stuID in (K.dupID)

WHERE recNo = @rec
ORDER BY SE.stueYear DESC, SE.stuID

-- string versions

while @rec <= @maxRec begin
	select @sourceS = convert(nvarchar(100), @sourceID), @targetS = convert(nvarchar(100),@targetID)
	raiserror('%i: %s=>%s Last Enrolment=%i', 0,1, @rec, @sourceS, @targetS, @lastYear)
	if (@sourceID is not null and @TargetID is not null) begin
		begin try
				exec pStudentOps.StudentMerge @targetID, @sourceID
				raiserror('%i: %s %s %s=>%s Merged', 0,1, @rec, @given, @surname, @sourceS, @targetS)
				Select @Merged = @merged + 1
		end try
		begin catch
			declare @e nvarchar(500) = ERROR_MESSAGE()
			raiserror('%i: %s %s %s=>%s ERROR: %s', 10,1, @rec, @given, @surname, @sourceS, @e)
			Select @error = @error+1
		end catch
	end
	else begin
		raiserror('Source or target not found',0 ,1)
	end
	Select @rec = @rec + 1, @sourceID = null, @targetID = null
	Select TOP 1 @targetID = SE.stuID			-- TOP 1 not actually necessary, assigning variables from a multirow set will only ever use the first row
	, @sourceID = DE.stuID
	, @given = SE.stuGiven, @surname = SE.stuFamilyName
	, @lastYear = SE.stueYear
	from @K K
	LEFT JOIN StudentEnrolment SE
		ON SE.stuID in (K.stuID)
	LEFT JOIN StudentEnrolment DE
		ON DE.stuID in (K.dupID)

	WHERE recNo = @rec
	ORDER BY SE.stueYear DESC, SE.stuID

end


Select NE NumEnrolmentRecords, count(stuID) NumStudents
from
(
select stuID, count(*) NE
from StudentEnrolment_
GROUP BY stUID
) SUB
GROUP BY NE
ORDER BY NE

if @commit = 1 commit
else begin
	rollback
	raiserror('Rollback - no changes committed',0,1)
end

Select @Matches DuplicatesFound, @found SelectedToProcess, @Merged Merged, @ignored Ignored, @error Errors

END
GO

