SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 June 2024
-- Description:	Selector for finder control based on student
--
-- Revision History: 8 June 2024 Allow 'constraints' to limit the selection
-- attendance at a school, year
-- =============================================
CREATE PROCEDURE [pExamRead].[candidateSelector]
	@s nvarchar(50),
	@year int=null,
	@examCode nvarchar(10) =null,
	@schoolNo nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @ss nvarchar(100)
declare @sg nvarchar(100)
declare @k int
declare @twopartSearch int = 0

-- first split to see if we read on two parts of the name
-- supported formats are
-- * Family, Given
-- * Given Family
select @k =charindex(',', @s)
print @k
if (@k >= 1) begin
	select @ss = rtrim(left(@s, @k-1))
	, @sg = ltrim(substring(@s,@k + 1, len(@s) - @k + 1))
	, @twoPartSearch = 1
end
else begin
	select @k =charindex(' ', @s)
	if (@k >= 1) begin
		select @sg = ltrim(left(@s, @k-1))
		, @ss = ltrim(substring(@s,@k + 1, len(@s) - @k + 1))
		, @twoPartSearch = 2
	end
end

print @s
print @ss
print @sg

if (isnumeric(@s) = 1) begin
select top 30 stuFamilyName + ', ' + stuGiven N, stuID C, stuCardID P,
	stuFamilyName F, stuGiven G
	From Student_
	 WHERE stuCardID like @s + '%'
	 return
end

	if @twoPartSearch > 0 begin
		Select @ss = nullif(@ss,''), @sg = nullif(ltrim(rtrim(@sg)),'')

		Select TOP 30
		case @twopartSearch
			when 1 then excFamilyName + ', ' + excGiven
			when 2 then	excGiven + ' ' + excFamilyName
		end N, excID C,
		excFamilyName F, excGiven G, exCode X, schNo S, exYear Y
		from dbo.ExamCandidates EC
			INNER JOIN Exams E
				ON EC.exID = E.exID
				AND (@year = exYear or @Year is null)
				AND (@SchoolNo = EC.schNo or @SchoolNo is null)
				AND (@examCode = E.exCode or @examCode is null)
		WHERE (EC.excFamilyName like @ss +'%' or @ss is null)
		AND (excGiven like @sg + '%' or @sg is null)

		ORDER BY excFamilyName, excGiven
		select @@rowcount num

		return
	end


--Select *
--from
--(
--Select excFamilyName + ', ' + excGiven + ': ' + exCode + ' ' + convert(nvarchar(4), exYear) + ' ' + schNo N,
--excID C
--FROM ExamCandidates EXC
--INNER JOIN Exams EX
--ON EXC.exID = EX.exID
--) sUB
--WHERE
	-- if not doing a 2 part search, search n Family and given, but prioiritise family


-- support contraints based on enrolment - year / schoolNo
-- note these queries will work even when year and schoolno arae not supplied
-- but the simpler case avoids the join for performance

	-- if not doing a 2 part search, search n Family and given, but prioiritise family
	-- DISITNCT avoids multiples in StudentEnrolment_
	select DISTINCT top 30  excFamilyName + ', ' + excGiven N, excID C,  Pty,
		excFamilyName F, excGiven G,
		exCode X, schNo S, exYear Y
		From
		(
		Select TOP 5000 excFamilyName, excGiven, excID ,
		case
			when excFamilyName like @s +'%' then 0
			when excGiven like @s +'%' then 1
			when excFamilyName like '%' + @s +'%' then 2
			when excGiven like '%' + @s +'%' then 3
		end Pty,
		exCode , schNo , exYear
		from dbo.ExamCandidates EC						-- without the schema, we get pExamRead.ExamCandidates
			INNER JOIN Exams E
				ON EC.exID = E.exID
				AND (@year = exYear or @Year is null)
				AND (@SchoolNo = EC.schNo or @SchoolNo is null)
				AND (@examCode = E.exCode or @examCode is null)

		WHERE excFamilyName like '%' + @s +'%'
		or excGiven like '%' + @s +'%'
		ORDER BY excFamilyName, excGiven
		) SUB
		ORDER BY Pty, excFamilyName, excGiven
	select @@rowcount num

	return

END
GO

