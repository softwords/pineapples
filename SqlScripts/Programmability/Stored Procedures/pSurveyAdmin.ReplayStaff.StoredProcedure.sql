SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 9 2022
-- Description:	Utility routine to reprocess an exam using the shredded xml
--              held on ExamCandidates
-- =============================================
CREATE PROCEDURE [pSurveyAdmin].[ReplayStaff]
	-- Add the parameters for the stored procedure here
	@fileId uniqueidentifier
	, @year int
	, @state nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @xml xml
	Select @xml = pSurveyAdmin.getStaffXml(@fileId,@year,@state)
	exec pSurveyOps.censusLoadStaff @xml, @fileId, original_login


END
GO

