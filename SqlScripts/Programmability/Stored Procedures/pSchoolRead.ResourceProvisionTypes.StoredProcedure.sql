SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [pSchoolRead].[ResourceProvisionTypes]
	-- Add the parameters for the stored procedure here
	@SurveyForm nvarchar(5) ,
	@paramSchoolType nvarchar(10),
	@SurveyYear int
AS
/* this procedure encapsulates the rule for finding the resource provisions
 to use on a given survey
 the match can be spefiecied by form name school type and/or year

matches in descinding order of preference
	Form SchoolType Year
	Form School Type
	Form Year
	Form
	School Type
	Year
	(all nulls)
*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Count int;

		with X as
		( Select top 1 mrpSurveyName
		, stCode , svyYear
		, case when mrpSurveyName = @SurveyForm then 32 else 0 end
		+
		case when stCode = @paramSchoolType then 16 else 0 end
		+
		case when svyYear = @surveyYear then 8 else 0 end

		+ case when mrpSurveyName is null then 4 else 0 end
		+ case when stCode is null then 2 else 0 end
		+ case when svyYear is null then 1 else 0 end

		Score

		from metaSchoolTypeResProvMap
		order by score desc
		)
		Select M.mrpResource as codeCode,
		R.mresName as codeDescription
		from metaSchoolTypeResProvMap M, X
		, TRmetaResourceDefs R
		where isnull(M.mrpSurveyName,'') = isnull(X.mrpSurveyName,'')
		and  isnull(M.stCode,'') = isnull(X.stCode,'')
		and isnull(M.svyYear,0) = isnull(X.svyYear,0)
		and M.mrpResource = R.mresName
		and X.Score > 6
		order by M.mrpSort


end
GO

