SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 7 2018
-- Description:	Create teacher survey records from TEacher Appointments
-- If we have historic data in appointments but no Survey data
-- we can assume the appointment is correct, and build a survey from it
-- Caveats:
-- * Must have an existing school survey in the appropriate survey year
-- * no other teachersurvey record in that same year

-- =============================================
CREATE PROCEDURE [pTeacherAdmin].[CreateSurveysFromAppointments]
	@surveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @t TABLE
(
	svyYear int
	, schNo nvarchar(50)
	, tID int
	, ssID int
	, tchsID int
	, taID int
	, taRole nvarchar(20)
)

INSERT INTO @t

Select Survey.svyYear
, TA.schNo
, TA.tID
, SS.ssID
, TS.tchsID

, TA.taID
, TA.taRole

FROM TeacherAppointment TA
INNER JOIN Survey
	ON Survey.svyCensusDate between taDate and isnull(taEndDate, '2999-12-31')  -- an appointment may be open-ended
LEFT JOIN SchoolSurvey SS
	ON Survey.svyYear = SS.svyYear
	AND SS.schNo = TA.schNo
LEFT JOIN pTEacherRead.TeacherSurveyV TS
	ON Survey.svyYear = TS.svyYear -- these joins give us any teacher survey in the survey year
	AND TS.tID = TA.tID
WHERE (Survey.svyYear = @surveyYear or @surveyYear is null)

Select * from @t

begin transaction
INSERT INTO TeacherSurvey
(
ssID
, tID
, tchRole
, tcheSource
)
Select
ssID
, tID
, taRole
, 'Appointment: ' + convert(nvarchar(20), taID)
FROM @t
WHERE ssId is not null and tchsId is null


Select Survey.svyYear
, TA.schNo
, TA.tID
, SS.ssID
, TS.tchsID
, TS.tcheSource
, TA.*
FRom TeacherAppointment TA
INNER JOIN Survey
	ON Survey.svyCensusDate between taDate and isnull(taEndDate, '2999-12-31')
LEFT JOIN SchoolSurvey SS
	ON Survey.svyYear = SS.svyYear
	AND SS.schNo = TA.schNo
LEFT JOIN pTEacherRead.TeacherSurveyV TS
	ON Survey.svyYear = TS.svyYear
	AND TS.tID = TA.tID


Select svyYear
, tID
FRom TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssId = SS.ssID
GROUP BY svyYear
, tID
HAVING count(*) > 1

If @@ROWCOUNT > 0 begin
	rollback
	print 'Duplicate teacher surveys in years - rolling back'
end else begin
	commit
end


END
GO

