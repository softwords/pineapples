SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	update toilets
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfToilets]
	@SurveyID int
	, @xml xml
AS
BEGIN
/*


*/
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-- this is the set of row tags for the grid
	declare @rowSet table(

	r nvarchar(2)
	, rk nvarchar(100)
	, rv nvarchar(100)
	)

	-- data n the grid
	declare @data TABLE
	(

		r			nvarchar(2)
		, toiletuse	nvarchar(20)
		, M		int
		, F		int
		, tot	int
		, c		nvarchar(1)				-- condition
		, YN	nvarchar(1)

	)

	declare @codeType nvarchar(10)			-- the ptCode assocaited to this table
	declare @idoc int

	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'

	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Toilets block
 <field name="Toilets">
      <field name="R">
        <field name="00">
          <field name="V">
            <value>Flush</value>
          </field>
        </field>
        <field name="01">
          <field name="V">
            <value>Water Seal</value>
          </field>
        </field>
        <field name="02">
          <field name="V">
            <value>Composting</value>
          </field>
        </field>
      </field>
      <field name="D">
        <field name="00">
          <field name="Pupil">
            <field name="M" />
            <field name="F" />
            <field name="C" />
          </field>
          <field name="Staff">
            <field name="C" />
            <field name="All" />
          </field>
          <field name="Wheelchair">
            <field name="C" />
            <field name="All" />
          </field>
        </field>
			...

*/


-- populate the table of grid values
	INSERT INTO @data
	(r, toiletuse, M, F, tot, c, YN)
	SELECT
	r, toiletuse, M, F, tot
	-- unfortunatel there are historically all sorts of variants on encoding Good / Fair / Poor
	-- we collect it as G FP
	-- we store it as A B C
	, case c when 'G' then 'A' when 'F' then 'B' when 'P' then 'C' else c end
	, YN
	FROM OPENXML(@idoc,'/x:field/x:field[@name="D"]/x:field/x:field',2) -- base is the toilet use node
	WITH
	(

		r				nvarchar(10)		'../@name'			-- 00 01 02 etc
		, toiletuse		nvarchar(10)		'@name'				-- pupil staff wheelchair...
		, M				int					'x:field[@name="M"]/x:value'
		, F				int					'x:field[@name="F"]/x:value'
		, tot			int					'x:field[@name="All"]/x:value'
		, c				nvarchar(1)			'x:field[@name="C"]/x:value'
		, YN			nvarchar(1)			'x:field[@name="YN"]/x:value'	-- misc YN in the data?
		)
		WHERE
		(M is not null or F is not null or tot is not null or c is not null)


-- get the complete set of rows
	INSERT INTO @rowSet
		(r, rk, rv)
		select r, isnull(rk, rv), rv
		FROM OPENXML(@idoc ,'/x:field/x:field[@name="R"]/x:field',2)
		WITH
		(
			r			nvarchar(2)			'@name'
			, rk			nvarchar(20)			'x:field[@name="K"]/x:value'
			, rv			nvarchar(20)			'x:field[@name="V"]/x:value'
		)
		WHERE r <> 'T'			-- ignore any totals


begin transaction


declare @rc int			--- for row count
begin try

	-- clear any exisitng entries
	DELETE from Toilets
	WHERe ssId = @SurveyID

-- Pineapples expects separate rows for boys and girls
-- some installation have StaffM and StaffF as well
-- so generlaise this - but Pupil is a special case
	INSERT INTO Toilets
	(
		ssID
		, toiType
		, toiUse
		, toiNum
		, toiCondition
		, toiYN
	)
	SELECT
		@SurveyID
		, rk
		, case N.num when 0 then 'Boys' when 1 then 'Girls' when 2 then 'Pupil' end
		, case N.num when 0 then M when 1 then F when 2 then tot end
		, C
		, YN
	FROM @data D
		INNER JOIN @rowset R
			ON D.r = R.r
	CROSS JOIN metaNumbers N
	WHERE D.toiletuse = 'Pupil'
		AND n.Num in (0,1, 2)
		AND (case N.num when 0 then M when 1 then F when 2 then tot end) <> 0

select @rc = @@rowcount
exec audit.xfdfInsert @SurveyID, 'Pupil Toilets inserted','Toilets',@rc


-- this will write Staff StaffM staffF WheelChair WheelchairM etc depending what's in the data

	INSERT INTO Toilets
	(
		ssID
		, toiType
		, toiUse
		, toiNum
		, toiCondition
		, toiYN
	)
	SELECT
		@SurveyID
		, rk
		, toiletuse + case N.num when 0 then 'M' when 1 then 'F' when 2 then '' end
		, case N.num when 0 then M when 1 then F when 2 then tot end
		, C
		, YN
	FROM @data D
		INNER JOIN @rowset R
			ON D.r = R.r
	CROSS JOIN metaNumbers N
	WHERE D.toiletuse <> 'Pupil'
		AND n.Num in (0,1, 2)
		AND (case N.num when 0 then M when 1 then F when 2 then tot end) <> 0

exec audit.xfdfInsert @SurveyID, 'Other Toilets inserted','Toilets',@rc

end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end
	exec audit.xfdfError @SurveyID, @ErrorMessage,'Toilets'
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

-- and commit
if @@trancount > 0
	commit transaction


END
GO

