SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 6 2021
-- Description:	Proc to customize generation of pdf using
--				cenopdf - this is applied to new teacher pages
--				to change the dropdowns of class levels
-- Refer to http://www.lystech.com/webhelp/default.htm 'Exporting to PDF from a Database'
-- Support NULL schooltype to generate all (for COMMON sections)
-- =============================================
CREATE PROCEDURE [cenopdf].[NewTeachers]
	@formtype nvarchar(10),	-- either PRI or SEC
	@targetFile nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @t TABLE
	(
		formtype nvarchar(10),
		schooltype nvarchar(10),
		targetfile nvarchar(500),
		classlevels nvarchar(max),
		teacherroles nvarchar(max),
		teacherQualE nvarchar(max),
		teacherQualNonE nvarchar(max)
	)

	INSERT INTO @t (formtype, schooltype)
	Select tt, st from
	(
		-- this implicitly maps the formtype to a school type
		-- to use for getting levels and teacher roles etc
		Select 'SEC' tt, 'CS' st UNION ALL Select 'PRI' tt, 'P' st
	) U
	WHERE @formtype = U.tt or nullif(@formtype, '') is null

	UPDATE @t
	SET targetfile = replace(@targetfile,'%formtype%', formtype),
	classlevels = cenopdf.ClassLevelItems(schoolType),		-- schooltype, not formtype
	teacherroles = cenopdf.TeacherRoleItems(schoolType),
	teacherQualE = cenopdf.TeacherQualItems(1),			-- ed qualifications
	teacherQualNonE = cenopdf.TeacherQualItems(0)		-- non ed quals


	Select classlevels [T.00.Class.Min_ItemsText]
	, classlevels [T.00.Class.Max_ItemsText]
	, classlevels [T.01.Class.Min_ItemsText]
	, classlevels [T.01.Class.Max_ItemsText]
	, classlevels [T.02.Class.Min_ItemsText]
	, classlevels [T.02.Class.Max_ItemsText]
	, classlevels [T.03.Class.Min_ItemsText]
	, classlevels [T.03.Class.Max_ItemsText]
	, classlevels [T.04.Class.Min_ItemsText]
	, classlevels [T.04.Class.Max_ItemsText]
	, teacherQualE [T.00.QualEd_ItemsText]
	, teacherQualE [T.01.QualEd_ItemsText]
	, teacherQualE [T.02.QualEd_ItemsText]
	, teacherQualE [T.03.QualEd_ItemsText]
	, teacherQualE [T.04.QualEd_ItemsText]
	, teacherQualNonE [T.00.Qual_ItemsText]
	, teacherQualNonE [T.01.Qual_ItemsText]
	, teacherQualNonE [T.02.Qual_ItemsText]
	, teacherQualNonE [T.03.Qual_ItemsText]
	, teacherQualNonE [T.04.Qual_ItemsText]
	, targetFile targetFile
	FROM @t

END
GO

