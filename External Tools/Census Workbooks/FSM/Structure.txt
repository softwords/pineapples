Sheets
------

Schools                                 shtSchools
Students                                shtStudents
Student Summary                         shtStudentSummary
SchoolStaff                             shtStaff
Staff Summary                           shtStaffSummary
WASH                                    shtWash
Wash Summary                            shtWashSummary
SchoolsList                             Sheet4
SchoolsByState                          Sheet2
Merge                                   shtMerge
Rollover                                shtRollover
Settings                                shtSettings
Lists                                   shtLists


ListObject: SchoolTable:Schools
-------------------------------

SchoolYear
State
School Name
School ID
Govt or Non-govt
School Budget
Date School Begins
Language of Instruction
# Buildings
# Classrooms
# Classrooms in poor condition
Electricity
Electricity Source
HIV Education
Sexuality Education
COVID-19 Education
SpEd Infrastructure
Telephone
Fax
Copier
Internet
Internet in teaching
Computer Lab
# Computers
Computers in teaching
Use of Technology

ListObject: StudentTable:Students
---------------------------------

SchoolYear
State
School Name
School ID
Govt ? Non-govt
National Student ID
First Name
Middle Name
Last Name
Full Name
Gender
Date of Birth
DoB Estimate
Age
Citizenship
Ethnicity
Language
Grade Level
From
Transferred From which school
Transfer In Date
SpEd Student
Full Name 2
IDEA SA ECE
IDEA SA School Age
Disability
English Learner
Has IEP
Has SBA Accommodation
Type of Accommodation
Assessment Type
Exiting
Full Name 3
Days Absent
Completed?
Outcome
Dropout Reason
Expulsion Reason
Transferred To Which School
Post-secondary study
Bullied

ListObject: StaffTable:SchoolStaff
----------------------------------

SchoolYear
State
School Name
School ID
Govt or Non-govt
Office
First Name
Middle Name
Last Name
Full Name
Gender
Date of Birth
Age
Citizenship
Ethnicity
FSM SSN
Other SSN
Highest Qualification
Field of Study
Year of Completion
Highest Ed Qualification
Year Of Completion2
Employment Status
Reason
Job Title
Organization
Staff Type
Teacher-Type
Date of Hire
Date Of Exit
Annual Salary
Funding Source
ECE
Grade 1
Grade 2
Grade 3
Grade 4
Grade 5
Grade 6
Grade 7
Grade 8
Grade 9
Grade 10
Grade 11
Grade 12
Admin
Other
Total Days Absence
Maths
Science
Language
Competency
Teach Mathematics
Teach Language Arts
Teach Social Studies
Teach Sciences

ListObject: WashTable:WASH
--------------------------

SchoolYear
State
School Name
School ID
Govt or Non-govt
Main Source Drinking Water
Currently Available
Toilet Type
Girls' Toilets Total
Girls' Toilets Usable
Boys' Toilets Total
Boys' Toilets Usable
Common Toilets Total
Common Toilets Usable
Available
Soap and Water

ListObject: SchoolsList:SchoolsList
-----------------------------------

schNo
schName
schLandOwner
SchoolType
Authority
AuthorityGroup
State

ListObject: SchoolsPlusInt_List:SchoolsList
-------------------------------------------

schNo
schName

ListObject: spEDECE_List:Lists
------------------------------

code
Description

ListObject: spEDSchoolAge_List:Lists
------------------------------------

code
Description

ListObject: spEdDisability_List:Lists
-------------------------------------

code
Description

ListObject: spEdEnglishLearnerList:Lists
----------------------------------------

code
Description

ListObject: Districts_List:Lists
--------------------------------

districtCode
districtName

ListObject: SurveyYears_List:Lists
----------------------------------

svyYear
FormattedYear
FromToYear
svyPSAge
svyCensusDate

ListObject: ToiletTypes_List:Lists
----------------------------------

ToiletType

ListObject: Ethnicity_List:Lists
--------------------------------

codeCode
codeDescription

ListObject: ClassLevels_List:Lists
----------------------------------

levelName
yearOfEd
levelCode

ListObject: Qualifications_List:Lists
-------------------------------------

QualName
QualCode

ListObject: EdQualifications_List:Lists
---------------------------------------

QualName
QualCode

ListObject: Gender_List:Lists
-----------------------------

GenderCode
Gender

ListObject: Nationality_List:Lists
----------------------------------

Code
Nationality

ListObject: PowerSupply_List:Lists
----------------------------------

mresName

ListObject: Handwashing_List:Lists
----------------------------------

mresName

ListObject: WaterSupply_List:Lists
----------------------------------

mresName

ListObject: FundingSource_List:Lists
------------------------------------

sofCode
sofDesc

ListObject: Vocab_List:Lists
----------------------------

vocabName
vocabTerm

ListObject: Language_List:Lists
-------------------------------

Code
Language

ListObject: spEdExitsList:Lists
-------------------------------

code
Description

ListObject: spEdAccommodationList:Lists
---------------------------------------

code
Description

ListObject: spEdAssessmentTypeList:Lists
----------------------------------------

code
Description

Workbook Defined Names
----------------------

SchoolStaff!_FilterDatabase             =SchoolStaff!$A$3:$BA$4
Students!_FilterDatabase                =Students!$A$3:$AQ$3
_xlfn.AGGREGATE                         =#NAME?
_xlfn.IFERROR                           =#NAME?
AllOrOneSchool                          =Lists!$BH$1
booklist                                =Merge!$O$9:$O$18
censusDate                              =Settings!$B$3
censusYearDropdown                      =censusYearDropdown_
censusYearDropdown_                     =SurveyYears_List[svyYear]
Chuuk                                   =#REF!#REF!
Chuuk_JTL                               =Lists!$W$4:$W$123
colsEnrolment                           =Students!$R$2:$AA$2
colsOutcome                             =Students!$AG$2:$AN$2
colsStudent                             =Students!$F$2:$P$2
CurrentSchool                           =Settings!$B$9
DOEInStateList                          =Settings!$B$11
Dropout_reasons                         =Lists!$T$4:$T$17
Expelled_reasons                        =Lists!$U$4:$U$11
Lists!ExternalData_1                    =Lists!$BJ$5:$BK$14
SchoolsList!ExternalData_1              =SchoolsList!$A$1:$G$200
Lists!ExternalData_10                   =Lists!$AH$5:$AI$23
Lists!ExternalData_11                   =Lists!$AK$5:$AL$11
Lists!ExternalData_12                   =Lists!$B$5:$C$7
Lists!ExternalData_13                   =Lists!$E$5:$F$30
Lists!ExternalData_14                   =Lists!$AB$5:$AB$9
Lists!ExternalData_15                   =Lists!$BA$5:$BA$9
Lists!ExternalData_16                   =Lists!$BE$5:$BE$13
Lists!ExternalData_17                   =Lists!$AO$5:$AP$10
Lists!ExternalData_18                   =Lists!$CK$5:$CL$31
Lists!ExternalData_19                   =Lists!$K$5:$L$10
Lists!ExternalData_2                    =Lists!$BM$5:$BN$13
SchoolsList!ExternalData_2              =SchoolsList!$M$1:$N$197
Lists!ExternalData_20                   =Lists!$BV$5:$BW$13
Lists!ExternalData_21                   =Lists!$BY$5:$BZ$36
Lists!ExternalData_22                   =Lists!$CB$5:$CC$8
Lists!ExternalData_3                    =Lists!$BP$5:$BQ$18
Lists!ExternalData_4                    =Lists!$BS$5:$BT$7
Lists!ExternalData_5                    =Lists!$N$5:$P$18
Lists!ExternalData_6                    =Lists!$AU$5:$AV$9
Lists!ExternalData_7                    =Lists!$CE$5:$CI$15
Lists!ExternalData_8                    =Lists!$BC$5:$BC$12
Lists!ExternalData_9                    =Lists!$H$5:$I$38
FSMSchools                              =SchoolsList[schName]
Kosrae                                  =#REF!#REF!
Kosrae_JTL                              =Lists!$V$4:$V$46
ListQualifier                           =Settings!$B$12
lists                                   =Merge!$U$7:$U$11
lstAllOrOneSchool                       =Lists!$BH$4:$BH$5
lstAllSchools                           =SchoolsList[schName]
lstBullied                              =Lists!$R$12
lstCitizenship                          =Nationality_List[Nationality]
lstEdQualifications                     =EdQualifications_List[QualName]
lstEmploymentStatus                     =Lists!$AR$4:$AR$5
lstEthnicity                            =Ethnicity_List[codeDescription]
lstFieldsOfStudy                        =Lists!$AM$4:$AM$173
lstFrom                                 =Lists!$AZ$4:$AZ$7
lstFundingSources                       =FundingSource_List[sofDesc]
lstGender                               =Gender_List[Gender]
lstGrades                               =ClassLevels_List[levelName]
lstInactiveReasons                      =Lists!$AS$4:$AS$10
lstJobTitles                            =Lists!$AA$4:$AA$17
lstLanguages                            =Language_List[Language]
lstOrganisations                        =Lists!$AF$4:$AF$10
lstOrganisationsRMI                     =Lists!$AF$15:$AF$21
lstOutcomes                             =Lists!$AX$4:$AX$9
lstPostSecondary                        =Lists!$AY$4:$AY$10
lstPowerSupply                          =PowerSupply_List[mresName]
lstQualifications                       =Qualifications_List[QualName]
lstSchoolsPlusInt                       =SchoolsPlusInt_List[schName]
lstSoapWater                            =Handwashing_List[mresName]
lstSSNPrefix                            =Lists!$AT$4:$AT$7
lstStaffType                            =Lists!$AD$4:$AD$5
lstStates                               =Districts_List[districtName]
lstTeacherType                          =Lists!$AE$4:$AE$5
lstTeacherTypeRMI                       =Lists!$AE$15:$AE$20
lstToilets                              =ToiletTypes_List[ToiletType]
lstWater                                =WaterSupply_List[mresName]
lstYes                                  =Lists!$R$9
lstYesNo                                =Lists!$R$4:$R$6
lstYN                                   =Lists!$R$17:$R$18
maxYear                                 =Settings!$B$6
mergeSchools                            =mergeSchoolList(selectedWorkbookName)
MergeSelectedSchool                     =Merge!$M$6
mergeSelectedSchoolName                 =Merge!$O$4
mergeSelectedSchoolNo                   =Merge!$K$8
nm_PowerSupplyType                      =PowerSupply_List[mresName]
nm_SchoolYear                           =Settings!$B$2
nm_State                                =Settings!$B$8
nm_State_JTL                            =Settings!$B$10
openbooks                               =openworkbooks()
Pohnpei                                 =#REF!#REF!
Pohnpei_JTL                             =Lists!$X$4:$X$78
rngSchoolSelectionList                  =Settings!$M$4:$M$400
schNames                                =SchoolsList[schName]
schoolSelection                         =getSchoolSelection()
selectedlist                            =Merge!$U$1
selectedListName                        =Merge!$U$2
selectedWorkbook                        =Merge!$O$1
selectedWorkbookName                    =Merge!$O$2
spEdAccommodationDropdown               =spEdAccommodationList[Description]
spEdAssessmentTypeDropdown              =spEdAssessmentTypeList[Description]
spEdDisabilityDropdown                  =spEdDisabilityDropdown_
spEdDisabilityDropdown_                 =spEdDisability_List[Description]
spEDECEDropdown                         =spEDECEDropdown_
spEDECEDropdown_                        =spEDECE_List[Description]
spEDEnglishLearnerDropdown              =spEDEnglishLearnerDropdown_
spEDEnglishLearnerDropdown_             =spEdEnglishLearnerList[Description]
spEdExitsDropdown                       =spEdExitsList[Description]
spEDSchoolAgeDropdown                   =spEDSchoolAgeDropdown_
spEDSchoolAgeDropdown_                  =spEDSchoolAge_List[Description]
versiondate                             =Settings!$B$25
versionmsg                              =Settings!$C$25
Yap                                     =#REF!#REF!
Yap_JTL                                 =Lists!$Y$4:$Y$91
yearEnd                                 =Settings!$B$5
YearSelector                            =Settings!$B$1
yearStart                               =Settings!$B$4

