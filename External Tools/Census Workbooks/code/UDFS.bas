' UDFS
' Version date: 20201102
' Version msg: Rollover

Option Explicit

' these UDFs do lookups from the main school table to get data related to a school by its name
'
' column name here is as defined in the
Function schoolInfo(ByVal schName As String, Optional ByVal colName As String = "SchNo", Optional ByVal searchColumn As String = "schName")
    Dim lo As ListObject
    Set lo = Sheets("SchoolsList").ListObjects(1)
    
    Dim idx As Integer
    On Error GoTo ErrorHandler
    If schName = "" Then
        schoolInfo = ""
    Else
        idx = Application.Match(schName, lo.ListColumns(searchColumn).DataBodyRange, False)
        If idx > 0 Then
            schoolInfo = Application.Index(lo.ListColumns(colName).DataBodyRange, idx)
        Else
            schoolInfo = ""
        End If
    End If
Exit Function
ErrorHandler:
    schoolInfo = ""
    
End Function
Function SchoolNumberInfo(ByVal schNo As String, Optional ByVal colName As String = "schName")
SchoolNumberInfo = schoolInfo(schNo, colName, "SchNo")
End Function

Function schoolName(ByVal schoolNo As String)
schoolName = SchoolNumberInfo(schoolNo)
End Function
Function SchoolNumber(ByVal schoolName As String)
SchoolNumber = schoolInfo(schoolName)
End Function


'' this function offers three different choices for getting the school drop down list
' 1) we have a single school workbook - the list is the name of that school only
' 2) we have a district based list - all school in the nominated district are in the list
' 3) every school is in the list
Function schoolSelectionList(ByVal selectedSchool As Variant, ByVal selectedState As Variant, Optional ByVal includeDOE As Variant = 0)

Dim v As Variant
Dim v2 As Variant
Dim k As Integer

If selectedSchool <> "" Then
    ReDim v(1 To 2, 1 To 1)
    v(1, 1) = selectedSchool
    v(2, 1) = ""
    k = 2
Else
    
        
    '
    ' get all the schools belonging to this state
    Dim pv As PivotTable
    Set pv = Sheets("schoolsByState").PivotTables(1)
    
    Dim rf As PivotField
    Dim cf As PivotField
    
    Set rf = pv.RowFields(1)    ' school names
    Set cf = pv.ColumnFields(1) ' states
    
    Dim ri As PivotItem
    Dim ci As PivotItem
    On Error Resume Next
    Set ci = cf.PivotItems(CStr(selectedState))

    
    
    Dim intSize As Integer
    k = 1

    If Not (ci Is Nothing) Then

        intSize = Application.Sum(ci.DataRange)
        ReDim v(1 To intSize + 1, 1 To 1)
        
        For Each ri In rf.PivotItems
            If Intersect(ri.DataRange, ci.DataRange) = 1 Then
                If includeDOE Or InStr(ri.Name, "Department of Education") = 0 Then
                    v(k, 1) = ri.Name
                    k = k + 1
                End If
            End If
        Next
        
        v(k, 1) = ""

    End If
    If ci Is Nothing Then
        intSize = Application.Sum(rf.PivotItems.Count)
        ReDim v(1 To intSize + 1, 1 To 1)
        For Each ri In rf.PivotItems
            If includeDOE Or InStr(ri.Name, "Department of Education") = 0 Then
                v(k, 1) = ri.Name
                k = k + 1
            End If
        Next
        v(k, 1) = ""
    End If
End If
ReDim v2(1 To k, 1 To 1)
Dim i As Integer
For i = 1 To k
    v2(i, 1) = v(i, 1)
Next

Dim vo As Variant
vo = v2
schoolSelectionList = vo

End Function

Function getSchoolSelection()
Dim v As Variant
Dim r As Range

Set r = Range("rngSchoolSelectionList")
Dim i As Integer

i = 1
For i = 1 To r.Rows.Count
    If IsError(r.Cells(i, 1)) Then
        Set r = r.Resize(i - 1)
        Exit For
    End If
Next
Set getSchoolSelection = r

End Function


Function listCount(ByVal SelectedList As String)
SelectedList = Replace(SelectedList, " ", "")
Dim lo As ListObject

On Error GoTo ErrorHandler
Set lo = Sheets(SelectedList).ListObjects(1)

listCount = lo.ListRows.Count
Exit Function

ErrorHandler:
    listCount = ""
End Function

Function workbookYear(workbookname As String)

Dim wk As Workbook
Dim sh As Worksheet
For Each wk In Application.Workbooks
    If wk.FullName = workbookname Then
        Set sh = wk.Sheets("Settings")
        workbookYear = sh.Range("nm_SchoolYear")
        Exit Function
    End If
Next
    
workbookYear = 0
End Function

Function NotEmpty(v)
NotEmpty = Not IsEmpty(v)
End Function
