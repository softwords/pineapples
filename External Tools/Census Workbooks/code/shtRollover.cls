' shtRollover
' Version date: 20201102
' Version msg: Rollover

Option Explicit

Private Sub Worksheet_Activate()
' this is to make sure the list of workbooks is up to date
' this is better than having a volatile function
Application.CalculateFull
End Sub
