' Commit
' Version date: 20201102
' Version msg: Rollover

Option Explicit
'
' comments about workbook versions can be inserted here to get them into the repo as plain text
'
' 2018 10 18 Added to repo

'' Note the increased security to be navigated
''https://www.ibm.com/docs/en/planning-analytics/2.0.0?topic=users-enable-trust-access-vba-project-object-model
Sub CommitWorkbook()


shtSchools.Activate

DeleteAllRows

shtStudents.Activate
DeleteAllRows

shtStaff.Activate
DeleteAllRows

shtWash.Activate
DeleteAllRows


ExportCode
StructureSummary


End Sub

Sub ExportTemplate()
Dim strPath As String
Dim wb As Workbook
Dim strP1 As String
Dim strP2 As String
strPath = ThisWorkbook.FullName
ThisWorkbook.Save
' now unlink all the lookup lists - this is the oly way epplus is able to update them
'
Dim tbl As ListObject
For Each tbl In Worksheets("Lists").ListObjects
    tbl.Unlink
Next

Worksheets("Schools").Activate
Dim fso As New Scripting.FileSystemObject
strP1 = fso.BuildPath(ThisWorkbook.path, "..\..\..\pineapples.Client\assets\census\fsm.xlsx")
strP2 = fso.BuildPath(ThisWorkbook.path, "..\..\..\pineapples\assets\census\fsm.xlsx")
ThisWorkbook.SaveAs strP1, XlFileFormat.xlOpenXMLWorkbook
ThisWorkbook.SaveAs strP2, XlFileFormat.xlOpenXMLWorkbook

'' copy the exported modules
''

    strP1 = fso.BuildPath(fso.GetParentFolderName(strP1), "code")
    Dim fldrName As String
    fldrName = fso.GetParentFolderName(strPath)
    fldrName = fso.GetParentFolderName(fldrName) '' go up two steps
    strP2 = fso.BuildPath(fldrName, "code")
    If Not fso.FolderExists(strP2) Then
    Else
        Dim fldr As Scripting.Folder
        Set fldr = fso.GetFolder(strP2)
        Dim f As Scripting.File
        For Each f In fldr.Files
            Select Case f.ShortName
                Case "Commit.bas"
                Case "DesignTime.bas"
                Case Else
                    fso.CopyFile f.Name, strP1
            End Select
        Next
    End If

ActiveWorkbook.Close False
''fso.CopyFile strP1, strP2
Workbooks.Open strPath


End Sub

Sub ExportCode()
    
    Dim objMyProj As VBProject
    Dim objVBComp As VBComponent
    
    Set objMyProj = Application.VBE.ActiveVBProject
    
    Dim path As String
    Dim ext As String
    Dim s As New Scripting.FileSystemObject
    Dim fldrName As String
    fldrName = s.GetParentFolderName(ThisWorkbook.FullName)
    fldrName = s.GetParentFolderName(fldrName) '' go up two steps
    path = s.BuildPath(fldrName, "code")
    If Not s.FolderExists(path) Then
        s.CreateFolder path
    Else
        Dim fldr As Scripting.Folder
        Set fldr = s.GetFolder(path)
        Dim f As Scripting.File
        For Each f In fldr.Files
            f.Delete
        Next
    End If

    Dim dt As Variant
    dt = Range("versiondate")
    Dim msg As String
    msg = Range("versionmsg")
    

    For Each objVBComp In objMyProj.VBComponents
        If objVBComp.CodeModule.CountOfLines > 1 Then
            Select Case objVBComp.Type
                Case vbext_ct_StdModule:
                    ext = ".bas"
                Case vbext_ct_ClassModule:
                    ext = ".cls"
                Case vbext_ct_Document:
                    ext = ".cls"
                    
            End Select
            If ext <> "" Then
                Dim fname As String
                fname = s.BuildPath(path, objVBComp.Name & ext)
                'objVBComp.Export fname
                Dim tx As Scripting.TextStream
                Set tx = s.OpenTextFile(fname, ForWriting, True)
                Dim l As String
                Dim cm As CodeModule
                Set cm = objVBComp.CodeModule
                tx.WriteLine "' " & objVBComp.Name
                tx.WriteLine "' Version date: " & dt
                tx.WriteLine "' Version msg: " & msg
                tx.WriteLine
                Dim idx As Integer
                idx = 1
                l = cm.Lines(idx, 1)
                If l = "' " & objVBComp.Name Then
                    idx = 2
                    l = cm.Lines(idx, 1)
                    If Left(l, 9) = "' Version" Then
                        idx = 3
                        l = cm.Lines(idx, 1)
                        If Left(l, 9) = "' Version" Then
                            idx = 4
                        End If
                    End If
                End If
                tx.Write cm.Lines(idx, cm.CountOfLines)
                tx.WriteLine
                tx.Close
            End If
        End If
    Next
        
End Sub

Sub StructureSummary()
    Dim path As String
    Dim ext As String
    Dim s As New Scripting.FileSystemObject
    path = s.GetParentFolderName(ThisWorkbook.FullName)
    
    
    Dim fname As String
    fname = s.BuildPath(path, "Structure.txt")
                'objVBComp.Export fname
    Dim tx As Scripting.TextStream
    Set tx = s.OpenTextFile(fname, ForWriting, True)
    
    Dim sh As Worksheet
    Dim lo As ListObject
    Dim n As Name
    Dim str As String
    
    tx.WriteLine "Sheets"
    tx.WriteLine "------"
    tx.WriteLine
    
    For Each sh In ThisWorkbook.Worksheets
    str = Left(sh.Name + String(40, " "), 40)
        tx.WriteLine str & sh.CodeName
    Next
    tx.WriteLine
    
    For Each sh In ThisWorkbook.Worksheets
        For Each lo In sh.ListObjects
            DumpTableStructure tx, lo
        Next

    Next
    tx.WriteLine
    
    '
    ' now dump all range names and theor definitions
    '
    tx.WriteLine "Workbook Defined Names"
    tx.WriteLine "----------------------"
    tx.WriteLine
    For Each n In ThisWorkbook.Names
        str = Left(n.Name & String(40, " "), 40)
        tx.WriteLine str & n.RefersTo
    Next
    tx.WriteLine
    tx.Close
End Sub

Private Sub DumpTableStructure(tx As TextStream, lo As ListObject)
tx.WriteLine
Dim str As String
str = "ListObject: " & lo.Name & ":" & lo.Parent.Name
tx.WriteLine str
tx.WriteLine String(Len(str), "-")
tx.WriteLine
Dim lc As ListColumn
For Each lc In lo.ListColumns
    tx.WriteLine lc.Name
Next

End Sub

Sub DumpReferences()
    Dim proj As VBProject
    Set proj = Application.VBE.ActiveVBProject
    
    Dim r As Reference
    
    For Each r In proj.References
        Debug.Print "*\G" & r.GUID & "#" & r.Major & "." & r.Minor & "#"; r.Type & "#" & r.FullPath & "#" & r.Description
        Debug.Print "*\G" & r.GUID & "#" & r.Major & "." & r.Minor & "###"
    Next
End Sub
