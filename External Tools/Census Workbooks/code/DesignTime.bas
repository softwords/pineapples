' DesignTime
' Version date: 20201102
' Version msg: Rollover

Sub ColLockCell()
Dim cll As Object
ActiveSheet.Unprotect
For Each cll In Selection
    If cll.Locked = True Then
        cll.Interior.ColorIndex = 39
End If
Next cll
If Selection.Locked = False Then
MsgBox "There is no locked cell in range that you chose", vbOKOnly, "Locked Cell Checker"
End If
End Sub



'
' load a book, and extract each school in that book into a separate workbook
'
Sub AutoExport()

Dim i As Integer
Dim numSrcSchools As Integer

numSrcSchools = ThisWorkbook.MergeSchools.Rows.Count
ThisWorkbook.SelectedListName = "(all)"
For i = 1 To numSrcSchools
    ' select the next school....
    ThisWorkbook.MergeSelectedSchool = i
    ClearAllData
    doMerge
    Sheets("Schools").Activate
    Range("nm_state") = schoolInfo(ThisWorkbook.MergeSelectedSchoolName, "state")
    
    ThisWorkbook.SaveAs ThisWorkbook.BulkExportName
Next
MsgBox "Export Completed"
End Sub
'
' remove any unwanted material below the tables
Sub REmoveValidations()

Dim sht As Worksheet

Set sht = ThisWorkbook.Worksheets("SchoolStaff")
sht.Unprotect
sht.Range("a5:BD1048576").Clear
sht.Protect

Set sht = ThisWorkbook.Worksheets("Schools")
sht.Unprotect
sht.Range("a5:BD1048576").Clear
sht.Protect

Set sht = ThisWorkbook.Worksheets("Students")
sht.Unprotect
sht.Range("a5:BD1048576").Clear
sht.Protect

Set sht = ThisWorkbook.Worksheets("Wash")
sht.Unprotect
sht.Range("a5:BD1048576").Clear
sht.Protect


End Sub
