' Rollover
' Version date: 20201102
' Version msg: Rollover


Option Explicit
Option Compare Text
Sub doRollover()
Dim wkName As String
Dim src As ListObject
Dim selectedSchool As String

Dim msgResult As Integer

Dim dest As ListObject

Dim SelectedList As String

On Error GoTo ErrorHandler

' get rid of spaces - which are not in 'SchoolStaff'
' SelectedList = Replace(Range("selectedListName"), " ", "")
wkName = Range("selectedWorkbookName")
If wkName > "" Then
Else
        MsgBox "No source workbook selected. Open the source workbook, then select it form the list", vbExclamation, "Merge"
        Exit Sub
End If
If Range("AllOrOneSchool") = 2 Then
    If Range("mergeSelectedSchool") > 0 Then
        selectedSchool = Range("mergeSelectedSchoolName")
    Else
        MsgBox "Select a school from the list, or else select to roll over All Schools", vbExclamation, "Merge"
        Exit Sub
    End If
End If


' get the source school year in the form SY2016-2017
Dim srcYear As String
Dim dstYear As String
Dim calcYear As String

Dim srcDistrict As String
Dim dstDistrict As String

srcYear = getSheetObject(wkName, "Settings").Range("nm_SchoolYear")
srcDistrict = getSheetObject(wkName, "Settings").Range("nm_State")
dstYear = shtSettings.Range("YearSelector")
Dim yr As Integer

yr = Val(Right(srcYear, 4))
calcYear = "SY" & yr & "-" & (yr + 1)

If dstYear <> yr + 1 Then
    If MsgBox("School year will be updated to " & calcYear, vbInformation + vbOKCancel, "Rollover") = vbCancel Then
        Exit Sub
    End If
End If

'
' we have to find the year in the list of survey years
' if its not there, we cannot proceed
'
Dim yrIndex As Integer

On Error Resume Next
yrIndex = Application.WorksheetFunction.Match(yr + 1, Application.Range("censusYearDropdown_"), False)
On Error GoTo ErrorHandler

If yrIndex = 0 Then
    MsgBox "Survey year " & calcYear + " has not yet been set up. Create the Survey year, then perform the Rollover", vbCritical + vbOKOnly, "Rollover"
    Exit Sub
End If

If shtStudents.ListObjects(1).ListRows.Count > 1 Or shtStaff.ListObjects(1).ListRows.Count > 1 Then
   msgResult = MsgBox("There is already data in the Students / Staff pages of this workbook. Delete all rows before continuing?", vbQuestion + vbYesNoCancel + vbDefaultButton3, "Rollover")
   Select Case msgResult
        Case vbCancel
            Exit Sub
        Case vbNo
        Case vbYes
            msgResult = MsgBox("All data in this workbook will be deleted before performing the Rollover. Continue?", vbExclamation + vbOKCancel + vbDefaultButton2, "Rollover")
            Select Case msgResult
                Case vbCancel
                    Exit Sub
                Case vbOK
                    ClearAllData
            End Select
   End Select
End If

' year is there so set the dropdown to select it
Application.Range("YearSelector") = yr + 1
' and the district too
Application.Range("nm_State") = srcDistrict

Dim operation As String

SelectedList = "Students"
operation = "rollover"
' roll over the student list
shtStudents.Unprotect
Set dest = shtStudents.ListObjects(1)
Set src = getListObject(wkName, "Students")
If selectedSchool <> "" Then
    RollFilteredSchoolList dest, src, "School Name", selectedSchool
Else
    RollFilteredSchoolList dest, src
End If
doProtect shtStudents
        
' merge the other lists
SelectedList = "Staff"
operation = "rollover"

shtStaff.Unprotect
Set dest = shtStaff.ListObjects(1)
Set src = getListObject(wkName, "SchoolStaff")
' principal filter is on Employment Status - school is optional
If selectedSchool <> "" Then
    MergeFilteredListObject dest, src, "Employment Status", "Active", "School Name", selectedSchool
Else
    MergeFilteredListObject dest, src, "Employment Status", "Active"
End If
doProtect shtStaff

SelectedList = "Schools"

shtSchools.Unprotect
Set dest = shtSchools.ListObjects(1)
Set src = getListObject(wkName, "Schools")
If selectedSchool <> "" Then
    MergeFilteredListObject dest, src, "School Name", selectedSchool
Else
    MergeFilteredListObject dest, src
End If
doProtect shtSchools


shtWash.Unprotect
Set dest = shtWash.ListObjects(1)
Set src = getListObject(wkName, "Wash")
If selectedSchool <> "" Then
    MergeFilteredListObject dest, src, "School Name", selectedSchool
Else
    MergeFilteredListObject dest, src
End If
doProtect shtWash

Exit Sub

ErrorHandler:

    Application.Calculation = xlCalculationAutomatic
    Application.ScreenUpdating = True
    Application.StatusBar = False

    MsgBox "An error occured performing the rollover to " & SelectedList & ": " & vbCrLf & Err.Description, vbExclamation, "Merge Error"



End Sub

'' roll over a single row read from the src table, into the dest table

''
'' the rollover depends on these fields of the Src
'' - Completed
'' - Outcome
'' - Grade

'' COMPLETED - NO
'' -> Outcome = Repeat - Rollover Grade is current grade, school is current school
'' -> any other - no rollover

'' COMPLETED - YES
'' Grade = Last Grade -> no rollkover created
'' Otherwise - rollover grade is next grade, school is current school

'' TRANSFERS - transfers are then manually adjusted

Sub RollSchoolList(dest As ListObject, src As ListObject)

'' first get a dictionary of all the fileds  we want to copy
'' ie fields that exist in both dest and src, and are not calculated

Dim col As ListColumn
Dim colName As Variant

Dim matches As Collection
Set matches = New Collection
For Each col In dest.ListColumns
    ' is it calculated?
    Dim r As Range
    Set r = col.DataBodyRange(1, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    ' its calculated - don't copy it
    Else
        ' mark to copy if it is in the source as well
        Dim srcCol As ListColumn
        For Each srcCol In src.ListColumns
            If srcCol.Name = col.Name Then
                matches.Add col.Name
                Exit For
            End If
        Next
    End If
Next


'' now step through the rows in src, moving the data into dest

Dim srcRow As ListRow
Dim destRow As ListRow
Dim isAdding As Boolean
Dim continue As String

Dim srcRowCount As Integer
srcRowCount = src.ListRows.Count

Dim destIdx As Integer

' aim for better performance by turning off screen updates and recalc
' be sure to turn on again in event of error

On Error GoTo ErrorHandler

'Application.ScreenUpdating = False
Application.Calculation = xlCalculationManual

For Each srcRow In src.ListRows
    ' are we adding or updating? to do...
    '' first, look at the source to see if this is a row that needs to be rolled
    If srcRow.Index Mod 100 = 0 Then
        Debug.Print Now
        Application.StatusBar = "Student Rollover " & srcRow.Index & " of " & srcRowCount
        If srcRow.Index Mod 1000 = 0 Then
           Application.Calculate
        End If
        Application.ScreenUpdating = True
        DoEvents
        Application.ScreenUpdating = False
    End If
    continue = isContinuing(src, srcRow.Index)
    If continue <> "" Then
    
        Set destRow = dest.ListRows.Add(, True)
    
        '' step through the matched columns, and move the value from srcRow to destRow
        For Each colName In matches
            Select Case colName
                Case "Grade Level"
                    Select Case continue
                        Case "P" ' promoted
                            ' the next grade
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = _
                                nextGrade(src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1))
                        Case Else
                            ' copy the current grade, (for Repeaters
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = _
                                src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
                    End Select
                Case "From"     ' where did the pupil come from?
                    Select Case continue
                        Case "R"        ' repeater
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = "Repeater"
                        Case Else ' leave blank
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = ""
                    End Select
                Case Else
                ''dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) =
                    destRow.Range.Cells(1, dest.ListColumns(colName).Index) = _
                        src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
            End Select
        Next
    End If
Next
    
Application.ScreenUpdating = True
Application.CalculateFull
Application.Calculation = xlCalculationAutomatic

Exit Sub

ErrorHandler:
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "An error occured rolling over student data: " & Err.Description, vbCritical, "Rollover"
End Sub

'' roll over a single row read from the src table, into the dest table

''
'' the rollover depends on these fields of the Src
'' - Completed
'' - Outcome
'' - Grade

'' COMPLETED - NO
'' -> Outcome = Repeat - Rollover Grade is current grade, school is current school
'' -> any other - no rollover

'' COMPLETED - YES
'' Grade = Last Grade -> no rollkover created
'' Otherwise - rollover grade is next grade, school is current school

'' TRANSFERS - transfers are then manually adjusted

Sub RollFilteredSchoolList(dest As ListObject, src As ListObject, Optional filterColumn As String = "", Optional filterValue As Variant = Null)

'' first get a dictionary of all the fileds  we want to copy
'' ie fields that exist in both dest and src, and are not calculated

Dim col As ListColumn
Dim colName As Variant

Dim matches As Collection
Set matches = New Collection
For Each col In dest.ListColumns
    ' is it calculated?
    Dim r As Range
    Set r = col.DataBodyRange(1, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    ' its calculated - don't copy it
    Else
    
        Select Case col.Name
            '' dont roll over outcome fields
            Case "Days Absent"
            Case "Completed?"
            Case "Outcome"
            Case "Dropout Reason"
            Case "Expulsion Reason"
            Case "Transfer To Which School"
            Case "Post-Secondary Study"
            
            Case Else
                ' mark to copy if it is in the source as well
                Dim srcCol As ListColumn
                For Each srcCol In src.ListColumns
                    If srcCol.Name = col.Name Then
                        matches.Add col.Name
                        Exit For
                    End If
                Next
        End Select
    End If
Next


'' now step through the rows in src, moving the data into dest

Dim srcRow As ListRow
Dim destRow As ListRow
Dim isAdding As Boolean
Dim continue As String

Dim srcRowCount As Integer
srcRowCount = src.ListRows.Count

Dim destIdx As Integer

' aim for better performance by turning off screen updates and recalc
' be sure to turn on again in event of error

On Error GoTo ErrorHandler

Dim idx As Integer
If filterColumn <> "" Then
    idx = src.ListColumns(filterColumn).Index
End If

'Application.ScreenUpdating = False
Application.Calculation = xlCalculationManual

For Each srcRow In src.ListRows
    ' are we adding or updating? to do...
    '' first, look at the source to see if this is a row that needs to be rolled
    
    If srcRow.Index Mod 100 = 0 Then
        Debug.Print Now
        Application.StatusBar = "Student Rollover " & srcRow.Index & " of " & srcRowCount
        If srcRow.Index Mod 1000 = 0 Then
           Application.Calculate
        End If
        Application.ScreenUpdating = True
        DoEvents
        Application.ScreenUpdating = False
    End If
    continue = isContinuing(src, srcRow.Index)
    
    Dim targetSchool As String
    Dim schoolName As String
    
    schoolName = listValue(src, srcRow.Index, "School Name")
    
    Select Case continue
        Case "P", "R"
            targetSchool = schoolName
        Case "O"
            targetSchool = listValue(src, srcRow.Index, "Transferred To Which School")
            If targetSchool = "International" Then
                continue = "X"
            End If
            If targetSchool = "" Then
                targetSchool = schoolName
            End If
        Case Else
            targetSchool = ""
        
    End Select
    
    
    If filterColumn <> "" Then
        If filterValue <> targetSchool Then
            continue = "X"          ' mark those we are not interested in reporting as non-matches
        End If
    End If
    
    Select Case continue
    
        Case "P", "R", "O"      '' ad transfers out
            ' make sure to use an empty row at the top when starting
            If dest.ListRows.Count = 1 And listValue(dest, 1, "School Name") = "" Then
                Set destRow = dest.ListRows(1)
            Else
                Set destRow = dest.ListRows.Add(, True)
            End If
        
            '' step through the matched columns, and move the value from srcRow to destRow
            For Each colName In matches
                Select Case colName
                    Case "School Name"
                        listCell(dest, destRow.Index, colName) = targetSchool
                    Case "Grade Level"
                        Select Case continue
                            Case "P", "O"   ' promoted
                                ' the next grade
                                listCell(dest, destRow.Index, colName) = _
                                    nextGrade(listValue(src, srcRow.Index, colName))
                                    
                            Case Else
                                ' copy the current grade, (for Repeaters, and transfers
                                listCell(dest, destRow.Index, colName) = _
                                    src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
                        End Select
                    Case "From"     ' where did the pupil come from?
                        Select Case continue
                            Case "R"        ' repeater
                                listCell(dest, destRow.Index, colName) = "Repeater"
                            Case "O"        ' transferred out rollsinto a TransferredIn
                                listCell(dest, destRow.Index, colName) = "Transferred In"
                            Case Else ' leave blank
                                listCell(dest, destRow.Index, colName) = ""
                        End Select
                    Case "Transferred From Which School"
                        Select Case continue
                            Case "O"
                                ' the school of source enrolment becomes the 'from' school for the transfer
                                listCell(dest, destRow.Index, colName) = listValue(src, srcRow.Index, "School Name")
                            Case Else
                                listCell(dest, destRow.Index, colName) = ""
                        End Select
                    Case "IDEA SA (ECE)"
                        '' clear this if the grade is not ECE
                        If gradeyearOfEd(listValue(dest, destRow.Index, "Grade Level")) <> 0 Then
                            listCell(dest, destRow.Index, colName) = ""
                        Else
                            listCell(dest, destRow.Index, colName) = listCell(src, srcRow.Index, colName)
                        End If
                    Case "IDEA SA (School Age)"
                        '' clear this if the grade is ECE
                        If gradeyearOfEd(listValue(dest, destRow.Index, "Grade Level")) = 0 Then
                            listCell(dest, destRow.Index, colName) = ""
                        Else
                            listCell(dest, destRow.Index, colName) = listCell(src, srcRow.Index, colName)
                        End If
                        
                    Case Else
                    ''dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) =
                        ''Debug.Print colName
                        destRow.Range.Cells(1, dest.ListColumns(colName).Index) = _
                            src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
                End Select
            Next
    Case ""
        Debug.Print "Not Continue: ", srcRow.Index, src.ListColumns("Full Name").DataBodyRange.Cells(srcRow.Index, 1), _
        src.ListColumns("Completed?").DataBodyRange.Cells(srcRow.Index, 1), _
        src.ListColumns("Outcome").DataBodyRange.Cells(srcRow.Index, 1), _
        src.ListColumns("Grade Level").DataBodyRange.Cells(srcRow.Index, 1)
    End Select

Next
    
Application.ScreenUpdating = True
Application.CalculateFull
Application.Calculation = xlCalculationAutomatic

Exit Sub

ErrorHandler:
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "An error occured rolling over student data: " & srcRow.Index & Err.Description, vbCritical, "Rollover"
End Sub


Function gradeIndex(grade As String)

gradeIndex = Application.WorksheetFunction.Match(grade, ThisWorkbook.GradeList, False)

End Function

Function isLastGrade(grade As String)

isLastGrade = (gradeIndex(grade) = ThisWorkbook.GradeList.Rows.Count)

End Function

Function gradeyearOfEd(ByVal grade As String)
gradeyearOfEd = ThisWorkbook.ClassLevelsList.ListRows(gradeIndex(grade)).Range(1, 2)
End Function

' next grade must advance the YearOfEd - this accounts for RMI Grade 8 => GRade 9 but not to GRade Pre-9
Function nextGrade(ByVal grade As String) As String
Dim currentYearOfEd As Integer

currentYearOfEd = gradeyearOfEd(grade)
Do While currentYearOfEd = gradeyearOfEd(grade)
    If isLastGrade(grade) Then
        nextGrade = ""
        Exit Function
    Else
        grade = ThisWorkbook.GradeList.Cells(gradeIndex(grade) + 1, 1)
    End If
Loop
nextGrade = grade
End Function
'
' Returns -> P - promoted
'' R - > Repeat
'' O = Transferred Out
' '' -> not continiuing
Function isContinuing(src As ListObject, srcRow As Integer) As String

Dim completed As String
Dim outcome As String

Dim grade As String

completed = listValue(src, srcRow, "Completed?")
outcome = listValue(src, srcRow, "Outcome")
grade = listValue(src, srcRow, "Grade Level")

'' first cut out everybody whose outcome is not To Repeat or ''
'' in other words, Deceased, Expelled, Transferred Out etc

Select Case outcome
    Case "To Repeat"
        isContinuing = "R"      ' even if Completed = Yes, will be counted as a Repeater
    Case "Transferred Out"
        isContinuing = "O"
    Case ""
        Select Case completed
            Case "Yes"
                ' continuing if not in the last grade
                If (isLastGrade(grade)) Then
                    isContinuing = ""
                Else
                    isContinuing = "P"
                End If
            Case "No"
                isContinuing = ""       ' repeatere are already handled
            Case Else
                isContinuing = ""
        End Select
    Case Else
        isContinuing = ""
End Select

End Function

Function listCell(src As ListObject, ByVal srcRow As Integer, ByVal columnName As String) As Range
Set listCell = src.ListColumns(columnName).DataBodyRange.Cells(srcRow, 1)
End Function

Function listValue(src As ListObject, ByVal srcRow As Integer, ByVal columnName As String)
listValue = src.ListColumns(columnName).DataBodyRange.Cells(srcRow, 1)
End Function
'' get the school year rnage from the source workbook
Function srcYear()

End Function


