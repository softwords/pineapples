' Localise
' Version date: 20201102
' Version msg: Rollover

Option Explicit

' change the names of column headers to local equivalents

Sub Localise()
ColumnVocabs shtSchools
ColumnVocabs shtStaff
ColumnVocabs shtStudents
ColumnVocabs shtWash



End Sub


Sub ColumnVocabs(sht As Worksheet)
Dim lo As ListObject
Set lo = sht.ListObjects(1)
sht.Unprotect
'' these are the standard descriptive columns of the school
lo.ListColumns(2).Name = getVocab("District")
lo.ListColumns(4).Name = getVocab("School No")
lo.ListColumns(5).Name = getVocab("Authority Govt")

'' specific to sheets

If sht Is shtStaff Then
    ' column P = 16
    lo.ListColumns(16).Name = getVocab("Teacher Payroll No")
    lo.ListColumns(16).Name = getVocab("Teacher Provident No")
End If
doProtect sht

End Sub

Function getVocab(vocabName)

Dim term As Variant
On Error Resume Next
term = Application.WorksheetFunction.VLookup(vocabName, Application.Range("Vocab_List"), 2, False)
If Err Then
    term = vocabName
End If
getVocab = term

End Function

