﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Mvc;
using ImageProcessor.Web.Services;
using ImageProcessor.Web.Helpers;
using Pineapples.Providers;

namespace Pineapples
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //support for unity dependency injeciton
            UnityConfig.RegisterComponents();
            // usual bundling and dependencies for angular etc are in here in common bundle config
            // omit this for the time being until it is redesigned for simppler
            // model
            // Softwords.Web.CommonBundleConfig.RegisterBundles(BundleTable.Bundles);

            //any project specific libraries?
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // need this to get MVC routing
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            // see also
            //http://stackoverflow.com/questions/14228072/app-start-folder-in-asp-4-5-only-in-webapplications-projects

            // finally change the paths where we look for mvc views
            // see http://stackoverflow.com/questions/632964/can-i-specify-a-custom-location-to-search-for-views-in-asp-net-mvc
            // we include a path based on the current 'context' as defined in web.config  see issue111

            string context = System.Web.Configuration.WebConfigurationManager.AppSettings["context"] ?? String.Empty;
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new Softwords.Web.WebFormViewEngineEx(context));
            ViewEngines.Engines.Add(new Softwords.Web.RazorViewEngineEx(context));

            ImageProcessor.Web.HttpModules.ImageProcessingModule.ValidatingRequest += ImageService.ImageProcessingModule_ValidatingRequest;

            // configure the razor engine here as well
            Pineapples.RazorEngineConfig.Config(context);

        }

		// this from here:
		//http://stackoverflow.com/questions/47089/best-way-in-asp-net-to-force-https-for-an-entire-site
		protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            switch (Request.Url.Scheme)
            {
                case "https":
                    // put this to one side for a while.... 2015 01 06
                    //Response.AddHeader("Strict-Transport-Security", "max-age=300");
                    break;
                case "http":
                    string useHttps = System.Web.Configuration.WebConfigurationManager.AppSettings["useHttps"];
                    if (useHttps == null || useHttps.ToLower() != "false")
                    {
                        if (Request.Url.Host == "localhost")
                        {
                            // special case for localhost
                            // just hardcode the SSL path that appears in the project properties window -- see
                            //http://www.asp.net/web-api/overview/security/working-with-ssl-in-web-api

                            // allow to proceed with https -- note this hardcoded path has to match the properties for the project
                            var path = "https://localhost:44301" + Request.Url.PathAndQuery;
                            Response.Status = "301 Moved Permanently";
                            Response.AddHeader("Location", path);
                        }
                        else
                        {
                            var path = "https://" + Request.Url.Host + Request.Url.PathAndQuery;
                            Response.Status = "301 Moved Permanently";
                            Response.AddHeader("Location", path);
                        }
                    }
                    break;
            }
        }
    }
}