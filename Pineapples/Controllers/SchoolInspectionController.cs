﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
	public class SchoolInspectionController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		public ActionResult Dashboard()
		{
			return View();
		}

		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		public ActionResult RenderFrame()
		{
			//ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.WriteX);
			// will find in Views/Shared if not overriden in feature's folder
			return View();
		}
		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			// pass through the data needed for the model
			// refine permissions (e.g. PermissionTopicEnum.SchoolAccreditation)?
			//ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Inspection, PermissionAccess.Write);
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		// dynamic inspection type support: these are presentations of surveys
		// 
		[Route("schoolinspection/survey/{inspectionType}")]
		public ActionResult Survey(string inspectionType)
		{
			return View(string.Format("survey/{0}", inspectionType));
		}

		// These are presentations for specific inspection types
		// if there is no custom form, default to a generic
		[Route("schoolinspection/inspectiontype/{inspectionType}")]
		[LayoutInjector("EditPageLayout")]
		public ActionResult InspectionType(string inspectionType)
		{
			var vw = $"InspectionType/{inspectionType}";
			ViewEngineResult result = ViewEngines.Engines.FindView(ControllerContext, vw, null);
			if (result.View != null)
			{
				return View(vw);
			}
			return View("item");
		}

}
}