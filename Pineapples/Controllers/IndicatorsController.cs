﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
	public class IndicatorsController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		// GET: Indicators

		[Route("indicators/{format}")]
		public ActionResult Indicators(string format)
		{
			ViewBag.Context = Context;          // pass the context, so we can make minor server-side changes to a 
												// default template
			string viewname = "Indicators_" + format;
			return View(viewname);
		}

		[LayoutInjector("MaterialDialogLayout")]
		[Route("indicators/info/{indicator}")]
		public ActionResult IndicatorsInfo(string indicator)
		{
			return View(indicator);
		}

		// various drill downs into Indicators data
		[Route("indicators/drill/{dataitem}")]
		public ActionResult Drill(string dataitem)
		{
			ViewBag.Drill = dataitem;
			switch (dataitem)
			{
				case "ner":
					ViewBag.RatioName = "Net";
					dataitem = "er";
					break;
				case "ger":
					ViewBag.RatioName = "Gross";
					dataitem = "er";
					break;
				case "nir":
					ViewBag.RatioName = "Net";
					dataitem = "ir";
					break;
				case "gir":
					ViewBag.RatioName = "Gross";
					dataitem = "ir";
					break;
			}
			string viewname = "drill_" + dataitem;

			return View(viewname);
		}

		// various drill downs into Indicators data
		[Route("indicators/table/{name}")]
		public ActionResult Table(string name)
		{
			string viewname = "table/" + name;

			return View(viewname);
		}
		[Route("indicators/admin")]
		public ActionResult admin()
		{

			return View();
		}
		[Route("indicators/uissurvey")]
		public ActionResult uissurvey()
		{

			return View();
		}

		[Route("indicators/exceldownload")]
		public ActionResult exceldownload()
		{

			return View();
		}
		// pieceas to make indicators page
		[Route("indicators/arrow")]
		public ActionResult Arrow()
		{
			return View();
		}
		[Route("indicators/block")]
		public ActionResult Block()
		{
			return View();
		}
		[Route("indicators/blockhdr")]
		public ActionResult Blockhdr()
		{
			return View();
		}
		[Route("indicators/change")]
		public ActionResult Change()
		{
			return View();
		}
		[Route("indicators/row")]
		public ActionResult row()
		{
			return View();
		}

		[Route("indicators/rowx")]
		public ActionResult rowx()
		{
			return View();
		}

		[Route("indicators/dataExport")]
		public ActionResult dataExport()
		{
			return View();
		}
	}
}