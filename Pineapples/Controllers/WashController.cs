﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
	public class WashController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		public ActionResult Dashboard()
		{
			return View();
		}

		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			// pass through the data needed for the model
			// refine permissions (e.g. PermissionTopicEnum.SchoolAccreditation)?
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Inspection, PermissionAccess.Write);
			return View();
		}

		public ActionResult RenderFrame()
		{
			// no write permissions for now
			//ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Inspection, PermissionAccess.WriteX);
			//ViewBag.FrameTitle = "Reviews and Inspections";
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		// dashboard
		// children go in the 'component' subfolder
		[Route("wash/dashboard/component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("dashboard/component/{0}", componentName), "DashboardComponent");
		}

		public ActionResult dataExport()
		{
			return View();
		}

	}
}