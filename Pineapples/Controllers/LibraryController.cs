﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Http;
using Pineapples.Data;
using Pineapples.Providers;

namespace Pineapples.Controllers
{
    public class LibraryController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        private readonly Pineapples.Data.DataLayer.IDSFactory _factory;
        protected Pineapples.Data.DataLayer.IDSFactory Factory
        {
            get { return _factory; }
        }
        public LibraryController(Pineapples.Data.DataLayer.IDSFactory factory)
        {
            _factory = factory;
        }

        [HttpGet]
        [Route(@"library/{id}")]
        [Route(@"library/{id}/{rotate:int}")]
        [Route(@"library/{id}/{rotate:int}/{size:int}")]
        public async Task<FileStreamResult> GetLibraryDocument(string id, int rotate=0, int size=0)
        {
			Guid g;
			Pineapples.Data.Models.Document document = null;
			string name = null;
			if (!Guid.TryParse(id, out g))
			{
				throw new HttpException((int)System.Net.HttpStatusCode.BadRequest, $"{id} is not a valid identifier");
			}
			document = Factory.Context.Documents.Find(g);
			if (document != null)
			{
				name = document.docTitle;
			}

			if (!FileDB.FileExists(id))
			{
				// this may be ok if this is a late-populated cloud download
				if (document == null)
				{
					throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No document record or file for {id}");
				}
				if (document.docCloudID == null)
				{
					throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No file found for {id}");
				}
				// so docCloudID is supplied - now we have to get the google file, save it, then we can return it
				try
				{
					await FileDB.StoreGoogleDriveFile(document.docCloudID, document.docID);
				}
				catch
				{
					throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"Google drive file {document.docCloudID} not found for {id}");
				}

			}
			if (name == null && (document != null && document.docCloudID != null))
			{
				name = document.docCloudID + Providers.FileDB.GetExtension(id);
			}
			name = name??Providers.FileDB.GetTokenWithExtension(id);

			if (!FileDB.FileExists(id)) // probably unnecessary to check yet again
            {
                throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No file exists at {id}");
            }
			System.IO.Stream fstrm = FileDB.Get(id);
            // the fileID of a file is never reassigned, so we can safely cache any file client-side
            // note that the url supplied by the client includes the bearer token in the query string
            // so the client-side caching will not work after the current session expires, therefore, set to 1 day
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.Private);
            HttpContext.Response.Cache.SetMaxAge(TimeSpan.FromDays(1));
 
            switch (System.IO.Path.GetExtension(name.ToLower()))
            {
                // these are extension we want the browser to pick up and display, rather than download
                // note that all the image types are already handled by imageprocessor 
				// (when there are processing attributes in the Url...)
                // so special cases here are limited to pdf....?
                case ".pdf":
                    // not supplying the name argument in this format means you don't get an "attachment" content-disposition header
                    return File(fstrm, MimeMapping.GetMimeMapping(name));
            }
            // everything else allow to download with its geniune name
            return File(fstrm, MimeMapping.GetMimeMapping(name), name);
        }

        /// <summary>
        /// this method will return a thumbnail based solely on the extension
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"thumb/{extension}/{rotate?}/{size?}")]
        [Route(@"thumb/{extension}")]
        public FileStreamResult GetgetThumbnail(string extension, int? rotate, int? size)
        {
            // if we have got here, and extension is a guid, then it is because there is no file
            // imageprocessor has thrown an error, but that is not passed back to the client.
            // Instead, processing returns to the main routing and winds up here...
            Guid g = Guid.NewGuid();
            string path = String.Empty;
            if (Guid.TryParse(extension, out g))
            {
                path = "file-unknown-icon-{0}.png";
                if (size == null || size == 0)
                {
                    size = 24;
                }

            }
            else
            {
                switch (extension.ToLower().Replace(".", ""))
                {
                    case "pdf":
                        path = "file-pdf-icon-{0}.png";
                        break;
                    case "xls":
                    case "xlsx":
                    case "xlsm":
                    case "csv":
                        path = "file-excel-icon-{0}.png";
                        break;
                    case "doc":
                    case "docx":
                    case "docm":
                        path = "file-word-icon-{0}.png";
                        break;
                    case "ppt":
                    case "pptx":
                    case "pptm":
                        path = "file-powerpoint-icon-{0}.png";
                        break;
                    case "zip":
                        path = "file-zip-icon-{0}.png";
                        break;
                    case "jpeg":
                    case "jpg":
                    case "png":
                    case "gif":
                    case "bmp":
                        path = "file-image-icon-{0}.png";
                        break;
                    case "mp3":
                    case "wma":
                    case "flac":
                        path = "file-sound-icon-{0}.png";
                        break;
                    case "avi":
                    case "mp4":
                    case "mov":
                    case "flv":
                        path = "file-video-icon-{0}.png";
                        break;
                    case "txt":
                        path = "file-text-icon-{0}.png";
                        break;
                    case "xfdf":
                        path = "file-xfdf-icon-{0}.png";
                        break;
                    default:
                        path = "file-icon-{0}.png";     // no sizing options
                        break;

                }
                if (size == null || size == 0)
                {
                    size = 72;
                }
                else
				{
                    if (size < 32)
                        size = 24;
                    if (size >= 32 && size < 48)
                        size = 32;
                    if (size >= 48 && size < 64)
                        size = 48;
                    if (size >= 64 && size < 72)
                        size = 64;
                    if (size >= 72 && size < 96)
                        size = 72;
                    if (size >= 96 && size < 128)
                        size = 96;
                    if (size >= 128)
                        size = 128;

                }
            }
           
            path = string.Format(path, size);
            path = System.Web.Hosting.HostingEnvironment.MapPath(@"~\assets\img\" + path);
            // Check to see if the file exists.
            if (!System.IO.File.Exists(path))
            {
                throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No image exists at {path}");
            }
            System.IO.FileStream fstrm = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true);
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.Private);
            HttpContext.Response.Cache.SetMaxAge(TimeSpan.FromDays(1));
            return File(fstrm, MimeMapping.GetMimeMapping(path));
        }

		#region template files
		[HttpGet]
		[Route(@"library/thumbnail")]
		public ActionResult Thumbnail()
		{
			return View();
		}

		[HttpGet]
		[Route(@"library/image")]
		public ActionResult Image()
		{
			return View();
		}

		[HttpGet]
		[Route(@"library/newlink")]
		public ActionResult NewLink()
		{
			return View();
		}
		#endregion
	}
}
