﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
	public class CensusController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			// pass through the data needed for the model
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Survey, PermissionAccess.Admin);
			return View();
		}
		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Survey, PermissionAccess.WriteX);
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		public ActionResult CensusTeacherQuals()
		{
			return View();
		}


	}
}