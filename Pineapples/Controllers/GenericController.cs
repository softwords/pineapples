﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pineapples.mvcControllers
{
	public class GenericController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		#region Generic-centric components
		public ActionResult FlashFindSearchComponent()
		{
			return View();
		}

		public ActionResult PagedListParamsComponent()
		{
			return View();
		}

		public ActionResult ChartParamsComponent()
		{
			return View();
		}

		public ActionResult MapParamsComponent()
		{
			return View();
		}

		public ActionResult Panel()
		{
			return View();
		}

		public ActionResult RenderFrame()
		{
			return View();
		}

		public ActionResult RenderList()
		{
			return View();
		}

		public ActionResult Navigator()
		{
			return View();
		}
		public ActionResult MirrorChart()
		{
			return View();
		}
		public ActionResult RowColChart()
		{
			return View();
		}
		#endregion

	}
}