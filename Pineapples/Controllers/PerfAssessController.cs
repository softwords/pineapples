﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{

	public class PerfAssessController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		// GET: PerfAssess
		public ActionResult Index()
		{
			return View();
		}
		// GET: PerfAssess
		public ActionResult Entry()
		{
			return View();
		}

		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.WriteX);
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}
	}
}