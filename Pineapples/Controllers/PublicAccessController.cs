﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Softwords.Web.mvcControllers;

namespace Pineapples.Controllers
{
	// Provides virtualisation of commonly used assets
	public class PublicAccessController : mvcControllerBase
	{
		public ActionResult home()
		{
			checkPublicAccess();
			return View();
		}

		[Route("~/publicdashboards/renderframe")]
		public ActionResult pdRenderFrame()
		{
			checkPublicAccess();
			// no permissions needed on public
			ViewBag.FrameTitle = "Dashboards";
			return View("RenderFrame");
		}

		[Route("~/publicindicators/renderframe")]
		public ActionResult piRenderFrame()
		{
			checkPublicAccess();
			// no permissions needed on public
			ViewBag.FrameTitle = "Indicators";
			return View("RenderFrame");
		}

		private void checkPublicAccess()
		{
			string allow = AppSetting("allowPublicAccess");
			if (allow == "1"||allow=="true")
			{
				return;
			}
			throw new Exception("Public access is not supported");
		}
	}
}