﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class TeacherExamController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		public ActionResult Upload()
		{
			return View();
		}

		[LayoutInjector("MaterialDialogLayout")]
		public ActionResult UploadConfirm()
		{
			return View();
		}

		public ActionResult RenderFrame()
		{
			// For now we don't allow creating exams from panel. They are created on loading
			// ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Exam, PermissionAccess.WriteX);
			return View();
		}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Exam, PermissionAccess.Write);
			return View("TeacherExamItem");
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		public ActionResult TeacherExamFrame()
		{
			return View();
		}

		public ActionResult CandidateFinder()
		{
			return View();
		}
	}
}