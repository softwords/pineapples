﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
	public class SchoolAccreditationController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		public ActionResult Dashboard()
		{
			return View();
		}

		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}


		// OBSOLETE WARNING: No longer using the feature Item page.
		// Refer to Pineapples.mvcControllers.SchoolInspectionController.InspectionType
		// which uses a polymorphic mechanism to edit the entity.
		//[LayoutInjector("EditPageLayout")]
		//public ActionResult Item()
		//{
		//	// pass through the data needed for the model
		//	// refine permissions (e.g. PermissionTopicEnum.SchoolAccreditation)?
		//	ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Inspection, PermissionAccess.Write);
		//	return View();
		//}

		public ActionResult RenderFrame()
		{
			// For now, do not allow/show the Add button for School Accreditations
			// They are added using the tablet app. However, a requirement in RMI
			// will force us to allow creating of special measure "empty" school accreditation (WASC/SDA levels)
			// therefore we will need a better solution there.
			// refine permissions (e.g. PermissionTopicEnum.SchoolAccreditation)?
			//ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Inspection, PermissionAccess.WriteX);
			//ViewBag.FrameTitle = "School Accreditations";
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}


		// dashboard
		// children go in the 'component' subfolder
		[Route("schoolaccreditation/dashboard/component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("dashboard/component/{0}", componentName), "DashboardComponent");
		}

		// FormB implementations ie the summary of an accreditation survey
		// These can vary between current and 'legacy' accreditations
		[Route("schoolaccreditation/formb/{componentName}")]
		public ActionResult FormB(string componentName)
		{
			return View(string.Format("FormB/{0}", componentName));
		}
		public ActionResult dataExport()
		{
			return View();
		}

	}
}