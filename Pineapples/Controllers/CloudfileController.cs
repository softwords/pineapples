﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class CloudfileController : Softwords.Web.mvcControllers.mvcControllerBase
	{
        public ActionResult List()
        {
            return View();
        }

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}
		public ActionResult RenderFrame()
		{
			// no add facility
			//ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.WriteX);
			ViewBag.FrameTitle = "Cloudfiles";
			return View();
		}

		[Route("cloudfile/processdialog/{componentName}")]
		[LayoutInjector("MaterialProcessDialogLayout")]
		public ActionResult FormB(string componentName)
		{
			return View(string.Format("dialog/{0}", componentName));
		}

		[LayoutInjector("MaterialDialogLayout")]
		[Route("cloudfile/dialog/{componentName}")]
		public ActionResult showDialog(string componentName)
		{
			return View(string.Format("dialog/{0}", componentName) );
		}
	}
}