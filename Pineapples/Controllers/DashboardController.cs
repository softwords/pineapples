﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class DashboardController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // render the options editor
        [Route("dashboard/options/{optionsFormat}")]
        public ActionResult Options(string optionsFormat)
        {
            return View(string.Format("options/{0}", optionsFormat));
        }

        // render a widget
        [Route("dashboard/widget/{widgetName}")]
        public ActionResult Widget(string widgetName)
        {
            return View(string.Format("widget/{0}", widgetName));
        }
    }
}