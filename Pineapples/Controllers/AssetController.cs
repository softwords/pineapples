﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Softwords.Web.mvcControllers;

namespace Pineapples.Controllers
{
	// Provides virtualisation of commonly used assets
	public class AssetController : mvcControllerBase
	{
		/// <summary>
		/// Return the application logo.
		/// First look for a file with the context name, otherwise use file 'logo'.
		/// First file found matching any image extension is returned.
		/// </summary>
		/// <returns></returns>
		[Route("asset/logo")]
		public ActionResult Logo()
		{
			// support an etag to eliminate repeated downloads of the same file
			// But, for developer's convenience, the etag includes the context, so that changing the context for testing
			// will always force a download of the logo.
			string context = System.Web.Configuration.WebConfigurationManager.AppSettings["context"] ?? String.Empty;
			// If you really need to force a refresh, you can create an appSetting 'version' in web.config
			string etag = (context + System.Web.Configuration.WebConfigurationManager.AppSettings["version"] ?? String.Empty).ToLower();
			if (etag != string.Empty)
			{
				var requestedETag = Request.Headers["If-None-Match"];
				if (requestedETag == etag)
				{
					return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotModified);
				}

			}

			// ServerAndPrivate is needed otherwise etag is ignored
			// https://stackoverflow.com/questions/937668/how-do-i-support-etags-in-asp-net-mvc
			Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
			Response.Cache.SetETag(etag);

			ActionResult file = null;
			if (context != string.Empty)
			{
				// first test if there is a file with the context name
				file = vImage(context);
				if (!(file is HttpNotFoundResult))
					return file;
			}
			return vImage("logo");
		}

		// return an image
		[Route("asset/image/{filename}")]
		[Route("asset/image")]
		public ActionResult Image(string filename)
		{

			return vImage(filename);
		}
	}
}
