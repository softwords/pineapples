﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class StudentDeduperController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		[Authorize]
		public ActionResult SearcherComponent()
		{
			return View();
		}

		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Student, PermissionAccess.Write);
			return View("StudentItem");
		}

		public ActionResult RenderFrame()
		{
			return View();
		}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Surveys()
		{
			return View();
		}
		[LayoutInjector("EditPageLayout")]
		public ActionResult Merge()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Student, PermissionAccess.Ops);
			return View();
		}

	}
}