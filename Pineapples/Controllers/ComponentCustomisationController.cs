﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pineapples.mvcControllers
{
    /// <summary>
    /// servers up template fragments that customise external components,
    /// in particular, ui-grid. 
    /// </summary>
  
    public class ComponentCustomisationController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        #region ui-grid
        [Route("uigrid/{template}")]
        public ActionResult uigridTemplate(string template, string param)
        {
            // these come down repeatedly when displaying an editable ui-grid
            // so use browser caching to reduce this
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.Private);
            // one hit on the server every 10 minutes is harmless enough
            HttpContext.Response.Cache.SetMaxAge(TimeSpan.FromMinutes(10));
            ViewBag.param = param??null;
            return View("uigrid/" + template);
        }
        #endregion

    }
}