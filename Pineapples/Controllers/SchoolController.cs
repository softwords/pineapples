﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
	public class SchoolController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//	return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//	return View();
		//}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
			return View("SchoolItem");
		}

		public ActionResult LatLng()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
			return View("LatLng");
		}

		#region School-centric components
		public ActionResult surveyList()
		{
			return View();
		}

		public ActionResult SchoolExamsDashboard()
		{
			return View();
		}

		public ActionResult QuarterlyReports()
		{
			return View();
		}

		public ActionResult SchoolAccreditations()
		{
			return View();
		}
		public ActionResult SchoolStaffList()
		{
			return View();
		}

		public ActionResult SchoolExams()
		{
			return View();
		}

		// editpage layout without Permissions in the ViewBag is just a panel
		[LayoutInjector("EditPageLayout")]
		public ActionResult ClassInfo()
		{

			return View();
		}

		public ActionResult Finder()
		{
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		#region Dashboard
		// Schools dashboard
		public ActionResult Dashboard()
		{
			return View();
		}

		// Individual school dashboard
		public ActionResult SchoolDashboard()
		{
			return View();
		}

		// Individual school dashboard children go in the 'component' subfolder
		[Route("school/SchoolDashboard/Component/{componentName}")]
		public ActionResult SchoolComponent(string componentName)
		{
			return View(string.Format("SchoolDashboard/Component/{0}", componentName), "DashboardComponent");
		}

		// Schools dashboard children go in the 'component' subfolder
		[Route("school/Dashboard/Component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("Dashboard/Component/{0}", componentName), "DashboardComponent");
		}
		#endregion
		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.WriteX);
			return View();
		}
		public ActionResult LinkList()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
			ViewBag.DeletePermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
			return View();
		}

		#endregion

		public ActionResult ContextTabs()
		{
			return View();
		}

		public ActionResult dataExport()
		{
			return View();
		}

	}
}