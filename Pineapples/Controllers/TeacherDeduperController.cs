﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class TeacherDeduperController : Softwords.Web.mvcControllers.mvcControllerBase
	{

		[Authorize]
		public ActionResult SearcherComponent()
		{
			return View();
		}

		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Write);
			return View("TeacherItem");
		}

		public ActionResult RenderFrame()
		{
			return View();
		}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Surveys()
		{
			return View();
		}
		[LayoutInjector("EditPageLayout")]
		public ActionResult Merge()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Ops);
			return View();
		}

	}
}