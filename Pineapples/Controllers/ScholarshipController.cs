﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class ScholarshipController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//      {
		//          return View();
		//      }

		//      public ActionResult PagedListEditable()
		//      {
		//          return View();
		//      }

		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Scholarship, PermissionAccess.WriteX);
			return View();
		}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Scholarship, PermissionAccess.Write);
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		public ActionResult test()
		{
			return View();
		}

		#region scholarship-centric components
		// Included below as ideas for future student profile tabs
		public ActionResult EnrollmentList()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Scholarship);
			return View(ps);
		}

		[LayoutInjector("EditDialogLayout")]
		public ActionResult StudyDialog()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Scholarship);
			return View(ps);
		}

		[LayoutInjector("MaterialDialogLayout")]
		public ActionResult GetStudentDialog()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Scholarship);
			return View(ps);
		}

		public ActionResult LinkList()
		{
			return View();
		}
		public ActionResult AuditList()
		{
			return View();
		}
		#endregion

		#region Dashboard

		// dashboard
		// children go in the 'component' subfolder
		[Route("scholarship/dashboard/component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("dashboard/component/{0}", componentName), "DashboardComponent");
		}

		public ActionResult Dashboard()
		{
			return View();
		}

		#endregion

		public ActionResult dataExport()
		{
			return View();
		}

	}
}