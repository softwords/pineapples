﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class TeacherController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//	return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//	return View();
		//}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Write);
			return View("TeacherItem");
		}

		#region Teacher-centric components
		public ActionResult SearcherComponent()
		{
			return View();
		}

		public ActionResult SurveyList()
		{
			return View();
		}

		// Teachers dashboard
		public ActionResult Dashboard()
		{
			return View();
		}

		// Individual teacher dashboard
		public ActionResult TeacherDashboard()
		{
			return View();
		}

		// dashboard
		// children go in the 'component' subfolder
		[Route("teacher/dashboard/component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("dashboard/component/{0}", componentName), "DashboardComponent");
		}

		public ActionResult AppointmentList()
		{
			return View();
		}
		#region Qualifications
		public ActionResult QualificationList()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Teacher);
			return View(ps);
		}
		[LayoutInjector("EditDialogLayout")]
		public ActionResult QualificationDialog()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Teacher);
			return View(ps);
		}

		#endregion
		[LayoutInjector("MaterialDialogLayout")]
		public ActionResult PayslipDialog()
		{
			return View();
		}

		public ActionResult Payslip()
		{
			return View();
		}
		public ActionResult PayslipList()
		{

			return View();
		}
		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.WriteX);
			return View();
		}
		public ActionResult LinkList()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Write);
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Write);
			ViewBag.DeletePermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Write);
			return View();
		}

		public ActionResult Upload()
		{
			return View();
		}

		public ActionResult Finder()
		{
			return View();
		}
		#endregion

		public ActionResult dataExport()
		{
			return View();
		}
	}
}