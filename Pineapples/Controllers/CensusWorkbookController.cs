﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
    public class CensusWorkbookController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        public ActionResult Upload()
        {
            return View();
        }


        public ActionResult Create()
        {
            return View();
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult UploadConfirm()
        {
            return View();
        }

    }
}