﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{

    public class FinDataController : Softwords.Web.mvcControllers.mvcControllerBase
    {

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			// pass through the data needed for the model
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Finance, PermissionAccess.Write);
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Finance, PermissionAccess.Write);
			return View();
		}


		// related records in EdExpenditure

		#region EdExpenditure
		public ActionResult EdExpList()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Finance);
			return View(ps);
		}
		[LayoutInjector("EditDialogLayout")]
		public ActionResult EdExpDialog()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Finance);
			return View(ps);
		}
		#endregion


		#region Dashboard

		// dashboard
		// children go in the 'component' subfolder
		[Route("findata/dashboard/component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("dashboard/component/{0}", componentName), "DashboardComponent");
		}

		public ActionResult Dashboard()
		{
			return View();
		}

		#endregion
	}
}