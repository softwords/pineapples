﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class StudentController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Student, PermissionAccess.WriteX);
			return View();
		}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Student, PermissionAccess.Write);
			return View("StudentItem");
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		public ActionResult Finder()
		{
			return View();
		}

		#region Student-centric components
		// Included below as ideas for future student profile tabs
		public ActionResult EnrollmentList()
		{
			return View();
		}

		public ActionResult ScholarshipList()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Scholarship);
			return View(ps);
		}

		public ActionResult ScoreCardList()
		{
			return View();
		}

		public ActionResult DocumentList()
		{
			return View();
		}

		public ActionResult LinkList()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Student, PermissionAccess.Write);
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Student, PermissionAccess.Write);
			ViewBag.DeletePermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Student, PermissionAccess.Write);
			return View();
		}
		#endregion
	}
}