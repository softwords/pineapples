﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class ExamController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		public ActionResult Upload()
		{
			return View();
		}

		[LayoutInjector("MaterialDialogLayout")]
		public ActionResult UploadConfirm()
		{
			return View();
		}

		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		public ActionResult RenderFrame()
		{
			// For now we don't allow creating exams from panel. They are created on loading
			// ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Exam, PermissionAccess.WriteX);
			return View();
		}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Exam, PermissionAccess.Write);
			return View("ExamItem");
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		public ActionResult ExamFrame()
		{
			return View();
		}

		public ActionResult ExamAnalysis()
		{
			return View();
		}

		public ActionResult ExamItemAnalysis()
		{
			return View();
		}
		public ActionResult CandidateFinder()
		{
			return View();
		}
		#region Dashboard

		// dashboard
		// children go in the 'component' subfolder
		[Route("exam/dashboard/component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("dashboard/component/{0}", componentName), "DashboardComponent");
		}

		public ActionResult Dashboard()
		{
			return View();
		}

		[Route("exam/dashboard/chart")]
		public ActionResult ExamChart()
		{
			return View("dashboard/chart");
		}
		[Route("exam/dashboard/schoolchart")]
		public ActionResult SchoolExamChart()
		{
			return View("dashboard/schoolchart");
		}
		[Route("exam/dashboard/schooldashboardchart")]
		public ActionResult SchoolExamDashboardChart()
		{
			return View("dashboard/schooldashboardchart");
		}


		[Route("exam/dashboard/crosstab")]
		public ActionResult ExamCrosstab()
		{
			return View("dashboard/crosstab");
		}
		[Route("exam/dashboard/schoolcrosstab")]
		public ActionResult ExamSchoolCrosstab()
		{
			return View("dashboard/schoolcrosstab");
		}

		[Route("exam/dashboard/schooldashboardcrosstab")]
		public ActionResult ExamSchoolDashbaordCrosstab()
		{
			return View("dashboard/schooldashboardcrosstab");
		}
		#endregion

		public ActionResult dataExport()
		{
			return View();
		}

	}
}