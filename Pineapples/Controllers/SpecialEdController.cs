﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
	public class SpecialEdController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		public ActionResult RenderFrame()
		{
			// for now, don;t allow any path to Add on SpecialEd
			//ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.SpecialEd, PermissionAccess.WriteX);
			return View();
		}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.SpecialEd, PermissionAccess.Write);
			ViewBag.SpecialEd = "SpecialEd";
			return View("../student/studentItem");
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}

		#region Student-centric components
		// Included below as ideas for future student profile tabs
		public ActionResult EnrollmentList()
		{
			return View();
		}


		public ActionResult ScoreCardList()
		{
			return View();
		}

		public ActionResult DocumentList()
		{
			return View();
		}
		#endregion

		#region EvaluationEvents
		public ActionResult EvaluationEventList()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.SpecialEd);
			return View(ps);
		}
		[LayoutInjector("EditDialogLayout")]
		public ActionResult EvaluationEventDialog()
		{
			PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.SpecialEd);
			return View(ps);
		}
		#endregion

		#region Dashboard

		// dashboard
		// children go in the 'component' subfolder
		[Route("specialed/dashboard/component/{componentName}")]
		public ActionResult Component(string componentName)
		{
			return View(string.Format("dashboard/component/{0}", componentName), "DashboardComponent");
		}

		public ActionResult Dashboard()
		{
			return View();
		}

		#endregion
		public ActionResult dataExport()
		{
			return View();
		}

	}
}