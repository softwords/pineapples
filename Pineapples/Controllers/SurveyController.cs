﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;


namespace Pineapples.mvcControllers
{
    public class SurveyController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        // TODO: may be obsolete by following routes
        public ActionResult Grid()
        {
            return View();
        }

        public ActionResult PupilTableGrid()
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult PupilTablePanel()
        {
            // pass through the data needed for the model
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Survey, PermissionAccess.Write);
            return View();
        }

        public ActionResult ResourceListGrid()
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult ResourceListPanel()
        {
            // pass through the data needed for the model
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Survey, PermissionAccess.Write);
            return View();
        }

        public ActionResult Survey()
        {
            return View();
        }

        [Route("survey/survey/{schooltype}/{year:int}")]
        public ActionResult Survey(string schooltype, int year)
        {
            string vw = string.Empty;
            ViewEngineResult result = null;
            for (int y = year; y > 1990; y--)
			{
                vw = $"survey/survey_{schooltype}_{y}";
                result = ViewEngines.Engines.FindView(ControllerContext, vw, null);
                if (result.View != null)
				{
                    return View(vw);
				}
            }
            // look for the school type, no year
            vw = $"survey/survey_{schooltype}";
            result = ViewEngines.Engines.FindView(ControllerContext, vw, null);
            if (result.View != null)
            {
                return View(vw);
            }
            // look for the year, no school type
            vw = $"survey/survey_{year}";
            result = ViewEngines.Engines.FindView(ControllerContext, vw, null);
            if (result.View != null)
            {
                return View(vw);
            }
            vw = "survey";
            return View(vw);

        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult AuditLogDialog()
        {
            return View();
        }
        public ActionResult AuditLog()
        {
            return View();
        }
    }
}