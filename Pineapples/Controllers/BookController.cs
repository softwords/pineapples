﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
	public class BookController : Softwords.Web.mvcControllers.mvcControllerBase
	{
		// OBSOLETE WARNING: use the more flexible RenderFrame
		//public ActionResult PagedList()
		//{
		//    return View();
		//}

		//public ActionResult PagedListEditable()
		//{
		//    return View();
		//}

		[LayoutInjector("EditPageLayout")]
		public ActionResult Item()
		{
			// pass through the data needed for the model
			ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
			return View();
		}
		public ActionResult RenderFrame()
		{
			ViewBag.AddPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.WriteX);
			return View();
		}

		public ActionResult SearcherComponent()
		{
			return View();
		}
	}
}