﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using ImageProcessor.Web.Services;
using ImageProcessor.Web.Helpers;
using System.IO;

namespace Pineapples.Providers
{
    public class ImageService: IImageService
    {
        public bool IsFileLocalService => false;
        public Dictionary<string, string> Settings { get; set; }

        /// <summary>
        /// Gets or sets the white list of <see cref="System.Uri"/>.
        /// </summary>
        public Uri[] WhiteList { get; set; }

        public string Prefix { get; set; }
        public bool IsValidRequest(string path)
        {
            string[] parts = path.Split('/', '\\');
            path = parts[0];
            // path must either be a guid, or a guid with an extension <guid>.
            if (path.IndexOf(".") == 36 )
            {
                // if there is an extension specified, be sure it is an image type
                if (!ImageHelpers.IsValidImageExtension(path))
                {
                    return false;
                }
                path = path.Substring(0, 36);
            }
            Guid gOut = new Guid();
            return Guid.TryParse(path, out gOut);    // base.IsValidRequest(trimPath(path));
        }

        public async Task<byte[]> GetImage(object id)
        {
			
            string[] parts = ((string)id).Split('/', '\\');
            //id = parts[0];
			string strId = parts[0].ToString();
			if (!FileDB.FileExists(strId))
			{
				// this may be ok if this is a late-populated cloud download
				// so off we go to the database to find out
				var factory = new Pineapples.Data.DataLayer.DSFactory();
				var document = factory.Context.Documents.Find(Guid.Parse(strId));
				if (document == null)
				{
					throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No document record for {strId}");
				}
				if (document.docCloudID == null)
				{
					throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No file found for {strId}");
				}
				// so now we have to get the google file, save it, then we can return it
				await FileDB.StoreGoogleDriveFile(document.docCloudID, document.docID);
			}
			return await FileDB.GetByteArrayAsync(strId);
        }
		#region static interfaces for validating call backs
		internal static void ImageProcessingModule_ValidatingRequest(object sender, ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e)
		{
			const string library = "/library/";
			const string google = "/google/";
			const string thumb = "/thumb/";
			const string flag = "/flag/";


			// throw away the token now from the querystring so as not to confuse the caching logic
			//t is the token
			int i = e.QueryString.IndexOf("&t=");
			if (i >= 0)
				e.QueryString = e.QueryString.Substring(0, i);

			// get the requestpath
			string requestpath = e.Context.Request.Path;

			if (requestpath.Contains(flag))
			{
				return;
			}
			// requests need authentication
			//////if (!(e.Context.Request.IsAuthenticated))
			//////{
			//////    e.Cancel = true;
			//////    return;
			//////}


			// library requests may be handled either by the imageprocessor module, which can size, and transform an image etc
			// or by the LibraryController, which will just return the file
			// if there is no query string, let it fall through to library controller
			// if there is a query string, remove it and let if fall through if the document is not an image...
			// this means we need to peek at it here
			if (requestpath.Contains(library))
			{
				ValidateLibraryRequest(e);
				return;
			}
			if (requestpath.Contains(google))
			{
				ValidateGoogleRequest(e);
				return;
			}
			if (requestpath.Contains(thumb))
			{
				ValidateThumbRequest(e);
				return;
			}
		}

		private static void ValidateLibraryRequest(ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e)
		{
			// library routes may look like 
			// library/<guid> or library/<guid>/72
			// library/<guid>.ext library/<guid>.ext/48
			// 
			// handled by image processor if its an image, and there is some processing instruction
			// either in the query string, or via the size parameter in the url

			// otherwise handled by the LibraryController, and returned in default (ie original size)

			char[] separators = new char[] { '/', '\\' };
			string[] parts = e.Context.Request.Path.Split(separators, StringSplitOptions.None);
			int ididx = Array.IndexOf(parts, "library") + 1;
			string id = parts[ididx];

			if (e.QueryString == String.Empty && parts.Length == (ididx + 1))
			{
				// no image manipulation was specified, so fall through to the default handler (LibraryController)
				e.Cancel = true;
				return;
			}
			// this is not an image, fall through - note if the file is not there
			// assume it is becuase we have not fetched it yet... so proceed on into the image handler
			if (FileDB.FileExists(id) && !ImageProcessor.Web.Helpers.ImageHelpers.IsValidImageExtension(FileDB.GetExtension(id)))
			{
				e.Cancel = true;
				return;
			}
			ImageQueryRewrite(e, ididx, parts);
		}


		private static void ValidateGoogleRequest(ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e)
		{
			// google requests look like 
			// google/<fileId> or google/<fileId>/72
			// 

			char[] separators = new char[] { '/', '\\' };
			string[] parts = e.Context.Request.Path.Split(separators, StringSplitOptions.None);
			int ididx = Array.IndexOf(parts, "google") + 1;
			string id = parts[ididx];

			ImageQueryRewrite(e, ididx, parts);
		}

		private static void ValidateThumbRequest(ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e)
		{
			// thumbnail routes may look like 
			// thumb/<guid> or thumb/<guid>/0/72
			// thumb/<guid>.ext thumb/<guid>.ext/0/48
			// or
			// thumb/pdf thumb/xlsx thumb/zip/24 etc

			char[] separators = new char[] { '/', '\\' };
			string[] parts = e.Context.Request.Path.Split(separators, StringSplitOptions.None);
			int ididx = Array.IndexOf(parts, "thumb") + 1;
			string id = parts[ididx];
			string size = "36";
			string rotate = "0";

			if (parts.Length == ididx + 3)
			{
				size = parts[ididx + 2];
			}
			if (parts.Length >= ididx + 2)
			{
				rotate = parts[ididx + 1];
			}

			string ext = System.IO.Path.GetExtension(id);     // drop the dot 
			if (ext.Length > 0)
			{
				id = System.IO.Path.GetFileNameWithoutExtension(id);
			}

			bool isGuid = false;
			Guid gOut = new Guid();
			isGuid = (Guid.TryParse(id, out gOut));

			if (isGuid)
			{
				//we've been given a docID from the filedb
				// check if it exists, and what its type is
				if (!FileDB.FileExists(id))
				{
					// the file is not there,
					// its probably coming from google drive

					//allow this to be handled by the imageservice, which will retrieve the google file
					// if the id is a docId in Documents table, and it references a cloudfile
					// just set the size to 72
					ImageQueryRewrite(e, ididx, parts);
					return;
				}
				// the file is there, so what is its extension?
				ext = FileDB.GetExtension(id);
				if (ext != String.Empty && !ImageHelpers.IsValidImageExtension(ext))
				{
					// we can't render an actual thumbnail of this file
					// so drop through to get an icon for it
					e.Cancel = true;
					parts[ididx] = ext;
					e.Context.RewritePath(String.Join("/", parts));
					return;
				}
				// all the ducks are in a row now - we have a valid image file we can get a thumbnail from
				// no need to cancel; change the url to get rid of the extension
				parts[ididx] = id;
				ImageQueryRewrite(e, ididx, parts, Convert.ToInt32(size));
				return;
			}
			if (ext != string.Empty)
			{
				e.Cancel = true;
				// we got an extension with a file name not a guid - this is probably a local file upload
				// this will drop through to the /thumb/{extension} route of LibraryController
				parts[ididx] = ext;
				e.Context.RewritePath(String.Join("/", parts));
				//ImageQueryRewrite(e, ididx, parts, 72);
				return;
			}
			// case we have left is just an extension e.g. thumb/pdf, thumb/pdf/0, thumb/pdf/0/32
			// just explictly cancel imageprocessor, LibraryController will handle
			e.Cancel = true;        // in fact, even this is not needed?
		}

		// takes the url suppliedin the format of our API (library\{fileID}/<size>)
		// and transforms it into the Url format expected by ImageProcessor - library/fileID?height=<size>&width=<size>
		private static void ImageQueryRewrite(ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e
			, int ididx, string[] parts, int defaultSize = 0)
		{
			string querystring = string.Empty;
			if (parts.Length >= ididx + 2)
			{
				querystring = String.Format("&rotate={0}", parts[ididx + 1]);
			}
			// display full size unless otherwise specified
			string size = defaultSize.ToString();

			if (parts.Length >= ididx + 3)
			{
				size = parts[ididx + 2];
				if (size == string.Empty || size == "0")
				{
					size = defaultSize.ToString();
				}
			}
			if (size != string.Empty && size != "0")
			{
				querystring += String.Format("&height={0}&width={0}&mode=min", size);
			}

			Array.Resize(ref parts, ididx + 1);
			e.Context.RewritePath(String.Join("/", parts));
			e.QueryString = querystring;
		}
		#endregion
	}
}