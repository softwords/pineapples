﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Threading.Tasks;

namespace Pineapples.Providers
{
    /// <summary>
    /// a simple static class to read and write filestreams to the file system, storing htem
    /// by a random guid name
    /// the guid is split into a prefix that determines a folder, the rest is the file name
    /// this limits the number of files in each folder
    /// </summary>
    public class FileDB
    {
      
        public static string dbBase()
        {
            string path = System.Web.Configuration.WebConfigurationManager.AppSettings["FileDb"];
            return path;
        }
        /// <summary>
        /// Store a file in the FileDB
        /// </summary>
        /// <param name="strm">stream opened on the file</param>
        /// <param name="ext">extension of the file</param>
        /// <returns>the identifier of the file</returns>
        public static Guid Store(Stream strm, string ext)
        {
            // we can add the file in here
            Guid g = Guid.NewGuid();
			return Store(strm, ext, g);
        }
		public static Guid Store(Stream strm, string ext, Guid g)
		{
			// we can add the file in here
			string s = g.ToString();
			string f = s.Substring(0, 4);
			s = s.Substring(4);

			string fldr = Path.Combine(dbBase(), f);
			if (!System.IO.Directory.Exists(fldr))
			{
				Directory.CreateDirectory(fldr);
			}
			string fname = Path.Combine(fldr, s);
			fname = Path.ChangeExtension(fname, ext);
			FileStream fstrm = new FileStream(fname, FileMode.Create);
			using (fstrm)
			{
				strm.CopyTo(fstrm);
				fstrm.Close();
			}
			return g;
		}
		/// <summary>
		/// Get the file as a stream - this can be diretly sent to the client as StreamContent
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static Stream Get(string id)
        {
            string path = GetFilePath(id);
            FileStream strm = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096);
            return strm;
        }

		/// <summary>
		/// return a stream with async = true - permits async reads against the stream
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static Stream GetStreamAsync(string id)
		{
			string path = GetFilePath(id);
			FileStream strm = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true);
			return strm;
		}

		public async static Task<byte[]> GetByteArrayAsync(string id)
		{
			using (Stream strm = GetStreamAsync(id))
			{
				var buffer = new byte[strm.Length];
				await strm.ReadAsync(buffer, 0, (int)strm.Length);
				return buffer;
			}

		}

		/// <summary>
		/// Remove a file from the fileDB
		/// </summary>
		/// <param name="id"></param>
		public static void Remove(string id)
        {
            string path = GetFilePath(id);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        /// <summary>
        /// Return the file path to the file in the FileDB
        /// </summary>
        /// <param name="path">the identifier of the file possibly with the extension ( as returned by</param>
        /// <returns>the full path, including extension</returns>
        public static string GetFilePath(string path)
        {
            string fldr = path.Substring(0, 4);
            string fn = path.Substring(4);
            path = Path.Combine(dbBase(), fldr);
            // allow searching with or without extension on the filename
            if (Path.GetExtension(fn) == String.Empty && Directory.Exists(path))
            {
                string[] fnames = Directory.GetFiles(path, fn + ".*");
                if (fnames.Length > 0)
                    fn = fnames[0];
            }
            return Path.Combine(path, fn);
        }

		/// <summary>
		/// test if a file exists at the nominated id
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static bool FileExists(string path)
		{
			return File.Exists(GetFilePath(path));
		}

		public static bool FileIsImage(string path)
		{
			string abspath = GetFilePath(path);
			return ImageProcessor.Web.Helpers.ImageHelpers.IsValidImageExtension(abspath);
		}
		/// <summary>
		/// Return the filename part of the FileDB entry with a given id
		/// </summary>
		/// <param name="id">the identifier</param>
		/// <returns>the filename, with extension, without path</returns>
		public static string GetTokenWithExtension(string id)
        {
			if (Path.GetExtension(id) != String.Empty)
			{
				return id;
			}
			// therwise get the best available extension
			return id + GetExtension(id);
       }

		public static string GetExtension(string id)
		{
			if (Path.GetExtension(id) != String.Empty)
			{
				return Path.GetExtension(id);
			}
			// therwise get the best available extension
			string abspath = GetFilePath(id);
			return Path.GetExtension(abspath);
		}

		#region Google Drive interfaces

		public static async Task<Guid> StoreGoogleDriveFile( string fileID, Guid g)
		{
			MemoryStream stream = await GoogleDriveManager.getFileContent(fileID);
			string ext = "jpg";
			var file = await GoogleDriveManager.getFileMetaData(fileID);
			switch (file.MimeType)
			{
				case "image/jpeg":
					ext = "jpg";
					break;

				case "image/png":
					ext = "png";
					break;
			}
			return Store(stream, ext, g);
		}

		#endregion		
	}
}