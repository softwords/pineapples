﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using ImageProcessor.Web.Services;
using ImageProcessor.Web.Helpers;
using System.IO;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace Pineapples.Providers
{
    public class GoogleDriveImageService: IImageService
    {
        public bool IsFileLocalService => false;
        public Dictionary<string, string> Settings { get; set; }

        /// <summary>
        /// Gets or sets the white list of <see cref="System.Uri"/>.
        /// </summary>
        public Uri[] WhiteList { get; set; }

        public string Prefix { get; set; }
        public bool IsValidRequest(string path)
        {
			return true;
        }

		public async Task<byte[]> GetImage(object id)
		{
			// we cannot get the factory from Unity DI - this object is instantiated in ImageProcessor somewhere
			// but we can manually create it - at the cost of some overhead

			var factory = new Pineapples.Data.DataLayer.DSFactory();

			byte[] buffer;
			string[] parts = ((string)id).Split('/', '\\');
			id = parts[0];
			var service = GoogleDriveManager.getDriveService();

			if (id.ToString().ToLower().StartsWith("n/"))
			{
				// its a name
				FilesResource.ListRequest listRequest = service.Files.List();
				listRequest.Q = string.Format("name = '{0}'", id.ToString().Substring(2));
				listRequest.PageSize = 20;
				listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

				// List files.
				var fileList = listRequest.Execute();
				IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;


				if (files != null && files.Count > 0)
				{
					id = files[0].Id;
				}

			}
			
			string strId = id.ToString();
			string filedbID = null;
			// look u to see if this google file has been allocated a slot in the fileDb
			// note that the download is 'lazy' the record may be there but the file is not.
			var document = factory.Context.Documents.Where(d => d.docCloudID == strId).FirstOrDefault();
			if (document != null)
			{
				// the get filedbID assocaited to this google file
				filedbID = document.docID.ToString();
				// look for a cached version first
				if (FileDB.FileExists(filedbID))
				{
					// its in the filedb - don't need to go to google cloud
					byte[] file = await FileDB.GetByteArrayAsync(filedbID);
					// its in the fileDb and we have successfully retrieved it
					// its safe to remove from the cloud storage since we won't access again from there
					if (document.docCloudRemoved == 0)
					{
						try
						{
							FilesResource.DeleteRequest request = service.Files.Delete(strId);
							await request.ExecuteAsync();
							document.docCloudRemoved = 1;
							factory.Context.SaveChanges();
						}
						catch { }
					}
					return file;
				}
			}
			MemoryStream stream = await GoogleDriveManager.getFileContent(strId);
			if (filedbID != null)
			{
				// the google file has been allocated a record in filedb, but the file is not there yet
				// we have the stream now, so store it now
				string ext = "jpg";
				var file = await GoogleDriveManager.getFileMetaData(strId);
				switch (file.MimeType)
				{
					case "image/jpeg":
						ext = "jpg";
						break;

					case "image/png":
						ext = "png";
						break;
				}
				FileDB.Store(stream, ext, document.docID);
				// we could even delete the cloud file here
				// but hold off on that because it will create another
				// google api call here on top of all the downloads:
				// performance implications?
				// also - a little safer to have an overlap ; ie 
				// a period where the file is both local and on the cloud?
			}
			stream.Position = 0;

			buffer = new byte[stream.Length];
			await stream.ReadAsync(buffer, 0, (int)stream.Length);
			return buffer;
		}

    }
}