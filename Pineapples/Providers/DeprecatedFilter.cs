﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;

namespace Pineapples.Providers
{
	public enum DeprecationResponse
	{
		Continue,
		Redirect,
		RaiseError
	}
	public class Deprecated : ActionFilterAttribute
	{
		
		public Deprecated(string successor, DeprecationResponse response = DeprecationResponse.Continue)
		{
			Successor = successor;
			Response = response;

		}

		private string Successor;
		private DeprecationResponse Response;


		public override void OnActionExecuting(HttpActionContext actionContext)
		{
		}

		//https://stackoverflow.com/questions/16688248/modify-httpcontent-actionexecutedcontext-response-content-in-onactionexecuted
		public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
		{
			// if Accept-Encoding header includes the customised option "JSONDeflate"
			// and we have a dataset or data table, then deflate
			switch (Response)
			{
				case DeprecationResponse.Continue:
				
					break;
				case DeprecationResponse.Redirect:
					// to do
					break;
				case DeprecationResponse.RaiseError:
					actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.Gone);
					break;
			}
			if (actionExecutedContext.Response != null)
			{
				actionExecutedContext.Response.Headers.Add("X-Deprecated", Successor);
			}

		}


		/// <summary>
		/// In this controller, use the 'deflating' json serializer since these arae likely to be big
		/// </summary>
		/// <param name="dt">the datatable to serialize</param>
		/// <returns></returns>
		private HttpResponseMessage Deflate(System.Data.DataTable dt)
		{
			var formatter = new System.Net.Http.Formatting.JsonMediaTypeFormatter();

			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

			var stream = Softwords.Web.Json.DatasetSerializer.toStream(dt);
			response.Content = new StreamContent(stream);
			response.Content.Headers.ContentType =
				new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
			return response;
		}
		private HttpResponseMessage Deflate(System.Data.DataSet ds)
		{
			var formatter = new System.Net.Http.Formatting.JsonMediaTypeFormatter();

			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

			var stream = Softwords.Web.Json.DatasetSerializer.toStream(ds);
			response.Content = new StreamContent(stream);
			response.Content.Headers.ContentType =
				new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
			return response;
		}

	}
}