﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Data;

namespace Pineapples.ExcelOutput
{
	/// <summary>
	/// Class to genderate a Teacher Isced audit sheet
	/// </summary>
	public class EnrolIscedAudit
	{
		public EnrolIscedAudit()
		{
		}

		public ExcelWorkbook PrepareWorkbook(ExcelWorkbook wb)
		{
			return wb;
		}
		public ExcelWorkbook Create(ExcelWorkbook wb, System.Data.DataTable dt)
		{
			ExcelWorksheet ws = wb.Worksheets.Add("Enrolment Table");

			ws.View.ShowGridLines = false;

			ExcelRangeBase dataCells;
			dt.TableName = "tblEnrol";
			using (ExcelRangeBase range = ws.Cells[5, 2])
			{
				var tableStyle = TableStyles.Medium2;
				dataCells = range.LoadFromDataTable(dt, true, tableStyle);
			}
			ws = wb.Worksheets.Add("Enrol Pivot Audit");	
			ws.View.ShowGridLines = false;
			ExcelRange pvtLocation = ws.Cells["B4"];
			string pvtName = "pivotA2";
			OfficeOpenXml.Table.PivotTable.ExcelPivotTable pivotTable = ws.PivotTables.Add(pvtLocation, dataCells, pvtName);
		

			var pub = new ExcelPublisher();
			pub.ConfigPivotTable(pivotTable, "Enrol",new string[]{ "PublicPrivate","GenderCode"},"ISCED SubClass",null);
			// headers
			pivotTable.ShowHeaders = true;

			// grand total
			pivotTable.ColumnGrandTotals = true;
			pivotTable.GrandTotalCaption = "Total";

			// style
			pivotTable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium9;

			return wb;
		}
		public ExcelWorksheet CreateA2Pivot(ExcelWorksheet ws, System.Data.DataTable dt)
		{

			ExcelWorkbook wb = ws.Workbook;

			ws.View.ShowGridLines = false;

			using (ExcelRangeBase range = ws.Cells[3, 2])
			{
				var tableStyle = OfficeOpenXml.Table.TableStyles.Medium2;
				range.LoadFromDataTable(dt, true, tableStyle);

			}
			return ws;
		}


	}
}