﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Data;

namespace Pineapples.ExcelOutput
{
	/// <summary>
	/// Class to genderate a Teacher Isced audit sheet
	/// </summary>
	public class SchoolIscedAudit
	{

		public SchoolIscedAudit()
		{
		}

		public ExcelWorkbook PrepareWorkbook(ExcelWorkbook wb)
		{
			return wb;
		}
		public ExcelWorksheet Create(ExcelWorkbook wb, System.Data.DataTable dt)
		{
			ExcelWorksheet ws = wb.Worksheets.Add("School Audit");

			ws.View.ShowGridLines = false;

			ExcelRangeBase dataCells;
			dt.TableName = "SA_tblSchools";
			using (ExcelRangeBase range = ws.Cells[6, 2])
			{
				var tableStyle = TableStyles.Medium2;
				dataCells = range.LoadFromDataTable(dt, true, tableStyle);
			}
			var tbl = ws.Tables[0];
			dataCells.AutoFitColumns(10, 30);
			ws.View.FreezePanes(7, 9);
			foreach (var col in tbl.Columns)
			{

				switch (col.Name)
				{
					case "ISCED 0":
					case "ISCED 1":
					case "ISCED 2":
					case "ISCED 3":
						col.DataCellStyleName = "block";
						break;
					case "Electricity":
					case "Internet_in_teaching":
					case "Internet":
					case "HIV_Education":
					case "Sexuality_Education":
					case "SpEd_Infrastructure":
					case "Handwashing":
					case "Water_Available":
						col.DataCellStyleName = "dot";
						break;
				}
			}
			// set up the total arrays

			int r = 2;
			r = SummaryTotalBlock(ws, r, ExcelAggregateScope.All_Rows, "List Totals");

			r += 2; // leave a row

			SummaryTotalBlock(ws, r, ExcelAggregateScope.Filtered_Rows, "Filtered Rows");
			return ws;

		}
		private int SummaryTotalBlock(ExcelWorksheet ws, int r, ExcelAggregateScope filter, string header)
		{
			int c = ws.Tables[0].Address.Start.Column;
			string tblName = ws.Tables[0].Name;
			var cols = ws.Tables[0].Columns;
			int cc = 0;

			foreach (var col in ws.Tables[0].Columns)
			{
				switch (col.Name)
				{
					case "PublicPrivate":
						cc = col.Position + c;
						ws.Cells[r, cc].HAlignRight().Value = $"{header}";
						break;
					case "Electricity":
					case "Internet_in_teaching":
					case "Internet":
					case "HIV_Education":
					case "Sexuality_Education":
					case "SpEd_Infrastructure":
					case "Handwashing":
					case "Water_Available":

					case "ISCED 0":
					case "ISCED 1":
					case "ISCED 2":
					case "ISCED 3":

						cc = col.Position + c;
						ws.Cells[r, cc].Numberformat("#,##0").Formula = 
							$"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[{col.Name}])";
						break;

				}
			}

			return r;
		}
	}
}