﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using OfficeOpenXml;
using dval = OfficeOpenXml.DataValidation;
using dvalcontracts = OfficeOpenXml.DataValidation.Contracts;
using Pineapples.Models;

namespace Pineapples.ExcelOutput
{
	public class SoeSchoolReport
	{
		public ExcelPackage Create(int year, string schoolNo, string district
		, Dictionary<string, Object> reportData, DataSet lookups)
		{
			string template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/census/fsm.xlsx");
			System.IO.FileInfo info = new System.IO.FileInfo(template);
			ExcelPackage p = new ExcelPackage(info);
			return p;

		}
	}
}