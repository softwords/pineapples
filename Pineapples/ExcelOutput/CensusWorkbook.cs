﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using OfficeOpenXml;
using dval = OfficeOpenXml.DataValidation;
using dvalcontracts = OfficeOpenXml.DataValidation.Contracts;
using Pineapples.Models;
using DocumentFormat.OpenXml.Spreadsheet;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Database;
using Microsoft.Ajax.Utilities;
using System.Collections;
using Unity.Registration;

namespace Pineapples.ExcelOutput
{
	public class CensusWorkbook
	{
		/// <summary>
		/// Create a new census workbook
		/// </summary>
		/// <param name="year">year of survey data to collect. 
		/// This is the year of the closing date of the school year</param>
		/// <param name="context">system context</param>
		/// <param name="schoolNo">school no of target</param>
		/// <param name="district">District target. Book will be populated with all schools in district</param>
		/// <param name="censusData">Data object to use for prepopulating</param>
		/// <param name="lookups">Dataset of all lookup objects</param>
		/// <returns>ExcelPackage representing the created xlsm file</returns>
		public ExcelPackage Create(int year, string context, string schoolNo, string district
			, Dictionary<string, Object> censusData, DataSet lookups)
		{

			string template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/census/{context}_Census.xlsx");
			if (!System.IO.File.Exists(template))
			{
				//legacy fallback
				template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/census/FEDEMIS_Census.xlsx");
			}
			System.IO.FileInfo info = new System.IO.FileInfo(template);
			ExcelPackage p = new ExcelPackage(info);

			// configuration information for sheets
			SheetSetupInfos ssInfos = GetSheetSetup(p, context);

			if (ssInfos.IsPopulated("Schools") == 1)
			{
				p = PopulateSchools(p, (DataTable)censusData["schools"], (DataTable)censusData["schoolsMap"]);
			}
			if (ssInfos.IsPopulated("SchoolStaff") == 1)
			{
				p = PopulateStaff(p, (DataTable)censusData["staff"], (DataTable)censusData["staffMap"]);
			}
			if (ssInfos.IsPopulated("Students") == 1)
			{
				p = PopulateStudents(p, (DataTable)censusData["students"], (DataTable)censusData["studentsMap"]); 
			}
			if (ssInfos.IsPopulated("Wash") == 1)
			{
				p = PopulateWash(p, (DataTable)censusData["wash"], (DataTable)censusData["washMap"]);
			}
			ExcelRangeBase lkpData;

			// Reload data on the lists page:
			//
			// in epplus we don't get access to the queryTable / ListObject associated to the table,
			// so we don't have its OleDBConnection information.
			// To work around this, the template workbook contains 'metaData' in a table to facilitate reloading the lookup data
			// The metaData of a lookup list is:
			// * the sheet containing the table
			// * the Table Name of the list,
			// * the data table name in the lookups dataset to populate the table 
			// * optionally, a Defined Name which references a single column of this table
			//		(it would be good to be able to leave this as =TableName[ColumnName]
			//		but irritatingly epplus converts this to an absolute range spec (e.g. Lists!$AO$6:$AP$12) so we have to fix it)
			// * the column of the table to assigned this Defined Name. Usually 1
			var metaData = p.Workbook.Worksheets["metaData"].Tables[0];
			for (int i = 0; i < metaData.DataBodyRange().Rows; i++)
			{
				var row = metaData.RowRange(i);
				string shtName = row.Offset(0, 0, 1, 1).Value.ToString();
				string tblName = row.Offset(0, 1, 1, 1).Value.ToString();
				string dtName = row.Offset(0, 2, 1, 1).Value.ToString();
				string dfName = (row.Offset(0, 3, 1, 1).Value ?? String.Empty).ToString();
				int colOffset = Convert.ToInt32(row.Offset(0, 4, 1, 1).Value);
				lkpData = p.Workbook.Worksheets[shtName].Tables[tblName].Reload(lookups.Tables[dtName]);
				if (colOffset >= 0)
				{
					p.Workbook.ApplyName(dfName, lkpData.Offset(0, colOffset, lkpData.Rows, 1));
				}
			}


			// set the workbook settings by pushing into the named ranges
			// refer to Settings sheet
			if (schoolNo != null)
			{
				p.Workbook.Names["CurrentSchool"].Value =
					p.Workbook.Worksheets["SchoolsList"].Tables["Schools_List"].Lookup(schoolNo, "schName", "schNo");
			}
			else
			{
				p.Workbook.Names["CurrentSchool"].Value = null;
			}

			// this sets up the district schools list, and the Job Title List, that can vary from province to province
			if (district != null)
			{
				var d = p.Workbook.Worksheets["Lists"].Tables["Districts_List"].Lookup(district);
				p.Workbook.Names["nm_State"].Value = d;
				p.Workbook.Names["nm_State_JTL"].Value = $"{d}_JTL";
			}
			else if (schoolNo != null)
			{
				var d = p.Workbook.Worksheets["SchoolsList"].Tables["Schools_List"].Lookup(schoolNo, "State", "schNo");
				if (d != null)
				{
					p.Workbook.Names["nm_State"].Value = d;
					p.Workbook.Names["nm_State_JTL"].Value = $"{d}_JTL";
				}
			}
			else
			{

				p.Workbook.Names["nm_State"].Value = null;
				p.Workbook.Names["nm_State_JTL"].Value = null;
			}

			// set the year
			p.Workbook.Names["YearSelector"].Value = year + 1;

			// create a vb project
			p.Workbook.CreateVBAProject();

			// the code modules for sheets and 'thisWorkbook' get created by CreateVBAProject
			// set the code module names - so they match with the imported module text
			p.Workbook.Worksheets["Schools"].CodeModule.Name = "shtSchools";
			p.Workbook.Worksheets["SchoolStaff"].CodeModule.Name = "shtStaff";
			p.Workbook.Worksheets["Staff Summary"].CodeModule.Name = "shtStaffSummary";
			p.Workbook.Worksheets["Students"].CodeModule.Name = "shtStudents";
			p.Workbook.Worksheets["Student Summary"].CodeModule.Name = "shtStudentSummary";
			p.Workbook.Worksheets["WASH"].CodeModule.Name = "shtWash";
			p.Workbook.Worksheets["Wash Summary"].CodeModule.Name = "shtWashSummary";
			p.Workbook.Worksheets["Merge"].CodeModule.Name = "shtMerge";
			p.Workbook.Worksheets["Rollover"].CodeModule.Name = "shtRollover";
			p.Workbook.Worksheets["Settings"].CodeModule.Name = "shtSettings";

			string codeFolder = System.Web.Hosting.HostingEnvironment.MapPath(@"~/assets/census/code");
			p.Workbook.LoadVBAProject(codeFolder);

			// need to add the project references too if we want it all to compile in VBA.
			// not much info out there on this - this provided enough:
			//https://csharp.hotexamples.com/examples/OfficeOpenXml.VBA/ExcelVbaProtection/-/php-excelvbaprotection-class-examples.html
			/* experiments show:
			- at a minimum, you need to provide Name and Libid for CoM libraries
			- you can construct the Libid by returning to an Excel workbook and dumping the references like this:

			Sub DumpReferences()
				Dim proj As VBProject
				Set proj = Application.VBE.ActiveVBProject

				Dim r As Reference

				For Each r In proj.References
					Debug.Print "*\G" & r.GUID & "#" & r.Major & "." & r.Minor & "#"; r.Type & "#" & r.FullPath & "#" & r.Description
				Next
			End Sub

			Note only the Guid (the typeLib guid) and version are mandatory, you can get away with this:
			Libid = @"*\G{2DF8D04C-5BFA-101B-BDE5-00AA0044DE52}#2.0###"
			(Note the security setting needed to get this to work!
			https://www.ibm.com/docs/en/planning-analytics/2.0.0?topic=users-enable-trust-access-vba-project-object-model
			)

			*/
			var rdef = new OfficeOpenXml.VBA.ExcelVbaReference()
			{
				Name = "Scripting Runtime",
				// the Libid can contain a path to the DLL as a hint, but safer to leave this out
				// and rely on it being located from the COM registration (the guid in libid is the typelib)
				//Libid = @"*\G{420B2830-E718-11CF-893D-00A0C9054228}#1.0#0#C:\Windows\System32\scrrun.DLL#Microsoft Scripting Runtime"
				Libid = @"*\G{420B2830-E718-11CF-893D-00A0C9054228}#1.0###"
			};
			p.Workbook.VbaProject.References.Add(rdef);
			rdef = new OfficeOpenXml.VBA.ExcelVbaReference()
			{
				Name = "Office",
				Libid = @"*\G{2DF8D04C-5BFA-101B-BDE5-00AA0044DE52}#2.0###"
			};
			p.Workbook.VbaProject.References.Add(rdef);


			// hide any pages that get hidden
			foreach (ExcelWorksheet sht in p.Workbook.Worksheets)
			{
				if (ssInfos.IsHidden(sht.Name) == 1)
				{
					sht.Hidden = eWorkSheetHidden.Hidden;
				} else if (ssInfos.IsHidden(sht.Name) == 2)
				{
					sht.Hidden = eWorkSheetHidden.VeryHidden;
				}
			}
			// finally, open the the first vivibe page
			string[] contentSheets = new string[] { "Schools", "SchoolStaff", "Students", "WASH" };
			foreach (string shtname in contentSheets)
			{
				var sht = p.Workbook.Worksheets[shtname];
				if (sht.Hidden == eWorkSheetHidden.Visible)
				{
					sht.Select(); // always reposition n the first visible page
					sht.View.ActiveCell = sht.Tables[0].ColumnDataBodyRange("School Name").End.Address;
					
					break;
				}
			}
			p.Workbook.View.ActiveTab = 1;			// first sheet is active
			return p;

		}

		/// <summary>
		///  construct the workbook name that will go in the ContentDisposition filename=
		/// </summary>
		/// <param name="p">The epplus package created by Create()</param>
		/// <returns>name</returns>
		public string WorkbookName(ExcelPackage p, int year, string schoolNo, string district, DataSet lookups)
		{
			var wk = p.Workbook;
			string nm = "All Schools";
			if (schoolNo != null)
			{
				nm = schoolNo;
			}
			else if (district != null)
			{
				nm = (string)dtLookup(lookups.Tables["districts"], district);
			}
			string yr = (string)dtLookup(lookups.Tables["censusYears"], (Int16)(year + 1));
			return $"{nm} {DateTime.UtcNow:yyyyMMdd} BOY {yr}.xlsm";
		}

		/// <summary>
		/// Look up a value in column 1 of the data table, return value from column 0
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="searchValue">value to search for</param>
		/// <returns></returns>
		private object dtReverseLookup(DataTable dt, object searchValue)
		{
			return dtLookup(dt, searchValue, 1, 0);
		}

		/// <summary>
		/// Lookup a value in a data table
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="searchValue">value to search for</param>
		/// <param name="searchColumn">column to search in (default 0)</param>
		/// <param name="resultColumn">column to return (default 1)</param>
		/// <returns></returns>
		private object dtLookup(DataTable dt, object searchValue, int searchColumn = 0, int resultColumn = 1)
		{
			object res = dt.Rows.Cast<DataRow>()
						.Where(row => Object.Equals(row[searchColumn], searchValue))
						.Select(row => (row[resultColumn]).ToString())
						.FirstOrDefault();
			return res ?? searchValue;
		}
		/// <summary>
		/// merge a data table into an Excel table
		/// Column names are "mapped", and cleaned up to find the appropriate column in the data table.
		/// Data validations and conditional formatting are propogated into the new rows.
		/// Assumes one blank row is currently in the table. Becuase rows are added to the sheet, cannot be used
		/// when two tables are side by side
		/// </summary>
		/// <param name="tbl">table to act on</param>
		/// <param name="dt">ADO.NET DataTable to merge</param>
		/// <param name="columnNames">a data table of column name mappings</param>
		/// <returns></returns>
		private OfficeOpenXml.Table.ExcelTable mergeTable(OfficeOpenXml.Table.ExcelTable tbl
			, System.Data.DataTable dt, System.Data.DataTable columnNames)
		{
			int row = tbl.Address.End.Row;
			int col = tbl.Address.Start.Column;
			var tblSheet = tbl.WorkSheet;
			if (dt.Rows.Count == 0)
			{
				// nothing to do
				return tbl;
			}
			foreach (var dv in tbl.WorkSheet.DataValidations)
			{
				System.Diagnostics.Debug.WriteLine("{0} {1}", tbl.WorkSheet.Name, dv.Address);
			}

			string[] cleannames = tbl.Columns.Names()
							//				.Select(nm => (string)dtLookup(columnNames, nm))
							.ToArray()
							.CleanColumnNames();
			// get the columns to act on - these are ones that are not calculated in the table
			// and where the mapped and cleaned-up name is present in the DataTable (dt)
			OfficeOpenXml.Table.ExcelTableColumn[] mappedColumns = tbl.Columns
				.Where(c => (c.CalculatedColumnFormula == String.Empty) && (dt.Columns.Contains(cleannames[c.Position])))
				.ToArray();
			// add the neccessary number of rows - we already have one row,
			// so insert dt.Rows.Count - 1
			// the third argument is the row to copy formats from -
			// this is the row number *after* the new rows hacve been inserted
			// https://stackoverflow.com/questions/31853046/epplus-copy-style-to-a-range
			if (dt.Rows.Count > 1)
			{
				tblSheet.InsertRow(row, dt.Rows.Count - 1, row + dt.Rows.Count - 1);
			}
			foreach (DataRow dr in dt.Rows)
			{
				foreach (var tblCol in mappedColumns)
				{
					// cleannames[tblCol.Position] is the column name in the DataTable 
					// need to check for dbNull in the ADO.NET data - otherwise it is treated not as null, but String.Empty
					var drValue = dr[cleannames[tblCol.Position]];
					tblSheet.Cells[row, col + tblCol.Position].Value = (drValue == DBNull.Value) ? null : drValue;
				}
				row++;
			}

			// the last row of the table is the original row
			// that has been pushed down by the inserts
			// an epplus limitation is that the Validations are not picked up when inserting the
			// new rows(formulas are however).
			// So we pick up any validation from that last row and apply to the whole column of the table
			// note use of various Extension methods to make these operations reusable

			// first note that if there is only 1 row of data, then the validations are already in place
			if (dt.Rows.Count > 1)
			{
				foreach (var tblCol in tbl.Columns)
				{
					dvalcontracts.IExcelDataValidation dv
						= tbl.WorkSheet.DataValidations[tbl.ColumnDataBodyRange(tblCol).End.Address];

					if (dv != null)
					{
						// get the entire column
						var columnrng = tbl.ColumnDataBodyRange(tblCol);
						// we have to leave off the last row which is the source of the validation
						// otherwise we get 'conflicting validations' because they overlap

						columnrng = columnrng.Offset(0, 0, columnrng.Rows - 1, columnrng.Columns);
						dv.ApplyTo(columnrng);
					}
					if (tblCol.CalculatedColumnFormula != String.Empty)
					{
						tbl.ColumnDataBodyRange(tblCol).Formula = String.Empty;
					}
				}
			}

			//// now deal with the conditional formatting - which is left on the First row of the table!
			// again this is not needed if we have not added new rows
			if (dt.Rows.Count > 1)
			{
				foreach (var tblCol in tbl.Columns)
				{
					// the DataValidation may have spaces separating areas of its range
					// e.g. {N4 Q4:S4 Z4} (this type is known as ST_Sqref in the OOXML spec and is explicitly space limited)
					// epplus can't interpret this - it needs {N4,Q4:S4,Z4} to be able to do the Intersects check
					var x = tbl.WorkSheet.ConditionalFormatting
						.Where(rule => tbl.WorkSheet.Cells[rule.Address.Address.Replace(" ", ",")]
									.ContainsCell(tbl.WorkSheet.Cells[tbl.ColumnDataBodyRange(tblCol).Start.Address]))
						.FirstOrDefault();
					if (x != null)
					{
						x.Address = new ExcelAddress($"{x.Address.Address.Replace(" ", ",")},{tbl.ColumnDataBodyRange(tblCol).Address}");
					}
				}

			}
			// 

			// now lock any columns that have calculated cell, unlock otherwise
			foreach (var tblCol in tbl.Columns)
			{
				tbl.ColumnDataBodyRange(tblCol).Style.Locked = (tblCol.CalculatedColumnFormula != String.Empty);
				tblCol.Name = (string)dtReverseLookup(columnNames, tblCol.Name);
			}


			return tbl;
		}
		ExcelPackage PopulateSchools(ExcelPackage p, System.Data.DataTable schoolsData, System.Data.DataTable columnNames)
		{
			var sht = p.Workbook.Worksheets["Schools"];
			// the table
			var tbl = sht.Tables[0];
			mergeTable(tbl, schoolsData, columnNames);
			return p;

		}
		public ExcelPackage PopulateStaff(ExcelPackage p, System.Data.DataTable staffData, System.Data.DataTable columnNames)
		{
			var sht = p.Workbook.Worksheets["SchoolStaff"];
			// the table
			var tbl = sht.Tables[0];
			mergeTable(tbl, staffData, columnNames);

			// this page also has some Aggregates at the top of columns - rebuild these
			foreach (var tblCol in tbl.Columns.Where(c => c.Position >= 2 && c.CalculatedColumnFormula == String.Empty))
			{
				int col = tbl.ColumnRange(tblCol).Start.Column;
				sht.Cells[1, col].Formula =
					$"=_xlfn.AGGREGATE({ExcelAggregateFunction.CountA:D}, {ExcelAggregateScope.Filtered_Rows:D}, {tbl.Name}[{tblCol.Name}])";
			}
			return p;
		}

		public ExcelPackage PopulateStudents(ExcelPackage p, System.Data.DataTable studentData, System.Data.DataTable columnNames)
		{
			var sht = p.Workbook.Worksheets["Students"];
			// the table
			var tbl = sht.Tables[0];

			mergeTable(tbl, studentData, columnNames);
			return p;
		}


		public ExcelPackage PopulateWash(ExcelPackage p, System.Data.DataTable washData, System.Data.DataTable columnNames)
		{
			var sht = p.Workbook.Worksheets["WASH"];
			// the table
			var tbl = sht.Tables[0];

			mergeTable(tbl, washData, columnNames);
			return p;
		}
		/// <summary>
		/// Return the sheet setup table from the meataData page
		/// This describes which sheets are hidden and which are populated
		/// </summary>
		/// <param name="p"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public SheetSetupInfos GetSheetSetup(ExcelPackage p, string context)
		{
			// get the table containing sheet level customisations
			var ws = p.Workbook.Worksheets["metaData"];
			var tbl = ws.Tables["listSheetSetup"];
			// Get the table range(excluding headers)

			int startRow = tbl.Address.Start.Row + 1; // Skip header row
			int endRow = tbl.Address.End.Row;
			int startCol = tbl.Address.Start.Column;
			int contextIndex = startCol; // Adjust to actual column index

			// Extract rows as a list of arrays (assuming all columns contain string values)
			var rows = Enumerable.Range(startRow, endRow - startRow + 1)
				.Select(row => new SheetSetupInfo()
				{
					RowIndex = row,
					Context = ws.Cells[row, startCol].Text,
					SheetName = ws.Cells[row, startCol + 1].Text, // Change Text to Value for non-string
					Populated = Convert.ToInt32(ws.Cells[row, startCol + 2].Value),
					Hidden = Convert.ToInt32(ws.Cells[row, startCol + 3].Value)
				})
				.Where(r => string.Equals(r.Context, context, StringComparison.OrdinalIgnoreCase)) // Apply filter
				.ToList();

			
			return new SheetSetupInfos(rows);
		}
	} 
	public class SheetSetupInfo
	{
		public int RowIndex;
		public string Context;
		public string SheetName;
		public int Populated;
		public int Hidden;
	}
	public class SheetSetupInfos
	{

		public SheetSetupInfos(List<SheetSetupInfo> info)
		{
			this.info = info;
		}
		public List<SheetSetupInfo> info;

		public int IsPopulated(string sheetName)
		{
			return info.Where(s => s.SheetName == sheetName).FirstOrDefault()?.Populated??1;
		}
		public int IsHidden(string sheetName)
		{
			return info.Where(s => s.SheetName == sheetName).FirstOrDefault()?.Hidden??0;
		}

	}
}
