﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Pineapples.ExcelOutput
{
	/// <summary>
	/// Pacific EMIS uses two distinct techniques to manipulate Excel documents on the server.
	/// EPPlus
	/// ------
	/// EPPLus provides an object model wrapper that follows the logical hierarchy structure of an Excel workbook.
	/// EPPLus is therefore simple and intuiitive to use; however it is not available for open source use beyind version 4
	/// Using only version 4 , there are limitations and bugs, some of which can be worked around, some which can't.
	/// In particular, using v4, we cannot manipulate any workbook that contains a pivot table built on anything but worksheet data.
	/// This means we cannot create or customised "refreshable" workbooks using EPPlus
	/// DocumentFormat.OpenXML
	/// ----------------------
	/// OpenXML is Miscrosoft's wrapper around the serialised representation of an Excel document - which is a collection of 
	/// XML files inside a zip package. Theoretically, you can do what you want using this tool, but even the simplest things are quite complex 
	/// see for example setting a cell to a text value (below).
	/// So this technique is used to configure workbooks that have PivotTables based on PowerQuery
	/// (becuase EPPLus v4.5.x.x corrupts such pivot tables)
	/// 
	/// Static members in this class simplify some common operations in OpenXML.
	/// </summary>
	class OpenXMLUtils
	{
		public static WorksheetPart WorksheetPart(WorkbookPart wbkPart, string sheetname)
		{
			Sheet theSheet = wbkPart.Workbook.Descendants<Sheet>().
					Where(s => s.Name == sheetname).FirstOrDefault();

			// get the worksheetPart fron the sheet
			return (WorksheetPart)wbkPart.GetPartById(theSheet.Id);
		}

		public static SharedStringTablePart SharedStringTablePart(WorkbookPart wbkPart)
		{
			if (wbkPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
			{
				return wbkPart.GetPartsOfType<SharedStringTablePart>().First();
			}
			else
			{
				return wbkPart.AddNewPart<SharedStringTablePart>();
			}
		}
		// Given text and a SharedStringTablePart, creates a SharedStringItem with the specified text 
		// and inserts it into the SharedStringTablePart. If the item already exists, returns its index.
		// https://docs.microsoft.com/en-us/office/open-xml/how-to-insert-text-into-a-cell-in-a-spreadsheet
		public static int GetSharedStringIndex(SharedStringTablePart shareStringPart, string text )
		{
			// If the part does not contain a SharedStringTable, create one.
			if (shareStringPart.SharedStringTable == null)
			{
				shareStringPart.SharedStringTable = new SharedStringTable();
			}

			int i = 0;

			// Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
			foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
			{
				if (item.InnerText == text)
				{
					return i;
				}

				i++;
			}

			// The text does not exist in the part. Create the SharedStringItem and return its index.
			shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
			//shareStringPart.SharedStringTable.Save();

			return i;
		}
		public static void SetCellValue(WorkbookPart wbkPart, string sheetname, string address, string value)
		{
			SharedStringTablePart sp = SharedStringTablePart(wbkPart);
			SetCellValue(WorksheetPart(wbkPart, sheetname), sp, address, value);
		}
		public static void SetCellValue(WorksheetPart sheetPart, string address, string value)
		{
			Cell cell = sheetPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == address).FirstOrDefault();
			if (cell != null)
			{
				//cell.CellValue = new CellValue(index.ToString());
				cell.CellValue = new CellValue(value);
				cell.DataType = new EnumValue<CellValues>(CellValues.String);
			}
		}

		public static void SetCellValue( WorksheetPart sheetPart, SharedStringTablePart stringTablePart, string address, string value)
		{
			
			int index = GetSharedStringIndex(stringTablePart, value);
			
			Cell cell = sheetPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == address).FirstOrDefault();
			if (cell != null)
			{
				//cell.CellValue = new CellValue(index.ToString());
				cell.CellValue = new CellValue(index.ToString());
				cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);
			}
		}
	}
}