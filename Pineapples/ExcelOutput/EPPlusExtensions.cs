﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPPlusPiv = OfficeOpenXml.Table.PivotTable;
using dval = OfficeOpenXml.DataValidation;
using dvalcontracts = OfficeOpenXml.DataValidation.Contracts;
using table = OfficeOpenXml.Table;

using OfficeOpenXml;
using System.Xml;
using System.Globalization;

namespace Pineapples
{

	/// <summary>
	/// First argument to Excel AGGREGATE function
	/// </summary>
	public enum ExcelAggregateFunction
	{
		Average = 1,
		Count = 2,
		CountA = 3,
		Max = 4,
		Min = 5,
		Product = 6,
		StDevS = 7,
		StDevP = 8,
		Sum = 9,
		VarS = 10,
		VarP = 11,
		Median = 12,
		ModeSngl = 13,
		Large = 14,
		Small = 15,
		PercentileInc = 16,
		QuartileInc = 17,
		PercentileExc = 18,
		QuartileExc = 19
	}

	/// <summary>
	/// Second argument to Excel AGGREGATE function
	/// </summary>
	public enum ExcelAggregateScope
	{
		All_Rows = 0,
		Filtered_Rows = 1
		// etc...
	}

	public static class EPPlusExtensions
	{
		public static bool FilterField(this EPPlusPiv.ExcelPivotTable pivotTable, string pageFieldName, object selectedItem)
		{
			return FilterField(pivotTable, pageFieldName, new List<object> { selectedItem });
		}
		/// <summary>
		/// Set the value in a page field
		/// </summary>
		/// <param name="pivotTable"></param>
		/// <param name="pageFieldName"></param>
		/// <param name="filters"></param>
		/// <returns></returns>
		public static bool FilterField(this EPPlusPiv.ExcelPivotTable pivotTable, string pageFieldName, IEnumerable<object> filters)
		{
			// Note that EPPlus 5 has this out of the box, but 4 does not
			// 5 is not available due to licence restrictions
			// Becuase 4 gives access to Xml representation of many objects
			// many things are still doable by descending to this level - just a lot harder!
			// this from https://stackoverflow.com/questions/25808332/create-pivot-table-filters-with-epplus

			//Set the cache definitions and cache fields
			var xdCacheDefinition = pivotTable.CacheDefinition.CacheDefinitionXml;
			var xeCacheFields = xdCacheDefinition.FirstChild["cacheFields"];
			if (xeCacheFields == null)
				return false;

			//Go the field list in the definitions, note the field idx and valuesfor 
			var count = 0;
			var fieldIndex = -1;
			List<object> fieldValues = null;

			foreach (XmlElement cField in xeCacheFields)
			{
				var att = cField.Attributes["name"];
				if (att != null && att.Value.Equals(pageFieldName, StringComparison.OrdinalIgnoreCase))
				{
					//store the field data
					fieldIndex = count;
					var dataddress = new ExcelAddress(pivotTable.CacheDefinition.SourceRange.Address);
					var dataWorksheet = pivotTable.CacheDefinition.SourceRange.Worksheet;
					var valueHeader = dataWorksheet
						.Cells[dataddress.Start.Row, dataddress.Start.Column, dataddress.Start.Row, dataddress.End.Column]
						.FirstOrDefault(cell => cell.Value.ToString().Equals(pageFieldName, StringComparison.OrdinalIgnoreCase));

					if (valueHeader == null)
						return false;

					//Get the range minus the header row
					var valueObject = valueHeader.Offset(1, 0, dataddress.End.Row - dataddress.Start.Row, 1).Value;
					var values = (object[,])valueObject;

					fieldValues = values
						.Cast<object>()
						.Distinct()
						.ToList();

					//kick back if the types are mixed
					if (fieldValues.FirstOrDefault(v => v is string) != null && fieldValues.FirstOrDefault(v => !(v is string)) != null)
						throw new NotImplementedException("Filter function does not (yet) support mixed parameter types");

					//fill in the shared items for the field
					var sharedItems = cField.GetElementsByTagName("sharedItems")[0] as XmlElement;
					if (sharedItems == null)
						continue;

					//Reset the collection attributes
					sharedItems.RemoveAllAttributes();

					//Handle numerics - assume all or nothing
					var isNumeric = fieldValues.FirstOrDefault(v => v is string) == null;
					if (isNumeric)
					{
						att = xdCacheDefinition.CreateAttribute("containsSemiMixedTypes");
						att.Value = "0";
						sharedItems.Attributes.Append(att);

						att = xdCacheDefinition.CreateAttribute("containsString");
						att.Value = "0";
						sharedItems.Attributes.Append(att);

						att = xdCacheDefinition.CreateAttribute("containsNumber");
						att.Value = "1";
						sharedItems.Attributes.Append(att);

						att = xdCacheDefinition.CreateAttribute("containsInteger");
						att.Value = fieldValues.Any(v => !(v is int || v is long)) ? "0" : "1";
						sharedItems.Attributes.Append(att);
					}

					//add the count
					att = xdCacheDefinition.CreateAttribute("count");
					att.Value = fieldValues.Count.ToString(CultureInfo.InvariantCulture);
					sharedItems.Attributes.Append(att);

					//create and add the item
					foreach (var fieldvalue in fieldValues)
					{
						var item = xdCacheDefinition.CreateElement(isNumeric ? "n" : "s", sharedItems.NamespaceURI);
						att = xdCacheDefinition.CreateAttribute("v");
						att.Value = fieldvalue.ToString();
						item.Attributes.Append(att);
						sharedItems.AppendChild(item);
					}

					break;
				}

				count++;
			}

			if (fieldIndex == -1 || fieldValues == null)
				return false;

			//Now go back to the main pivot table xml and add the cross references to complete filtering
			var xdPivotTable = pivotTable.PivotTableXml;
			var xdPivotFields = xdPivotTable.FirstChild["pivotFields"];
			if (xdPivotFields == null)
				return false;

			var filtervalues = filters.ToList();
			count = 0;
			foreach (XmlElement pField in xdPivotFields)
			{
				//Find the asset type field
				if (count == fieldIndex)
				{
					var att = xdPivotTable.CreateAttribute("multipleItemSelectionAllowed");
					att.Value = "1";
					pField.Attributes.Append(att);

					var items = pField.GetElementsByTagName("items")[0] as XmlElement;
					if (items == null)
						return false;

					items.RemoveAll();

					att = xdPivotTable.CreateAttribute("count");
					att.Value = (fieldValues.Count + 1).ToString(CultureInfo.InvariantCulture);
					items.Attributes.Append(att);
					pField.AppendChild(items);

					//Add the classes to the fields item collection
					for (var i = 0; i < fieldValues.Count; i++)
					{
						var item = xdPivotTable.CreateElement("item", items.NamespaceURI);
						att = xdPivotTable.CreateAttribute("x");
						att.Value = i.ToString(CultureInfo.InvariantCulture);
						item.Attributes.Append(att);

						// this is the point that actually hides the fields 
						// we flip this over so it is the specified fields that are Visible
						//if (filtervalues.Contains(fieldValues[i]))
						if (!filtervalues.Contains(fieldValues[i]))
						{
							att = xdPivotTable.CreateAttribute("h");
							att.Value = "1";
							item.Attributes.Append(att);
						}

						items.AppendChild(item);
					}

					//Add the default
					var defaultitem = xdPivotTable.CreateElement("item", items.NamespaceURI);
					att = xdPivotTable.CreateAttribute("t");
					att.Value = "default";
					defaultitem.Attributes.Append(att);
					items.AppendChild(defaultitem);

					break;
				}
				count++;
			}
			return true;
		}

		/// <summary>
		/// get the maximum value in an integer column
		/// </summary>
		/// <param name="dt">a dataTable</param>
		/// <param name="columnName">the column (field) name</param>
		/// <returns></returns>
		public static int MaxInt(this System.Data.DataTable dt, string columnName)
		{
			int max = 0;
			foreach (System.Data.DataRow dr in dt.Rows)
			{
				if ((int)dr[columnName] > max)
				{
					max = (int)dr[columnName];
				}
			}
			return max;
		}

		#region Formatting
		// some helpers to simplify formatting, providing a fluent interface
		// e.g. instead of 
		// var cell = ws.Cells[r,c]
		// cell.Style.Border.Bottom.Style = None;
		//cell.Value = x;
		//
		// just say ws.Cells[r,c].Underline().Value = x


		/// <summary>
		/// apply a thin underline to a range
		/// </summary>
		/// <param name="rng"></param>
		/// <returns></returns>
		public static ExcelRangeBase Underline(this ExcelRangeBase rng)
		{
			return Underline(rng, OfficeOpenXml.Style.ExcelBorderStyle.Thin);
		}
		public static ExcelRangeBase Underline(this ExcelRangeBase rng, OfficeOpenXml.Style.ExcelBorderStyle style)
		{
			rng.Style.Border.Bottom.Style = style;
			return rng;
		}
		public static ExcelRangeBase Numberformat(this ExcelRangeBase rng, string format)
		{
			rng.Style.Numberformat.Format = format;
			return rng;
		}

		public static ExcelRangeBase Vertical(this ExcelRangeBase rng)
		{
			rng.Style.TextRotation = 90;
			return rng;
		}

		public static ExcelRangeBase HAlignRight(this ExcelRangeBase rng)
		{
			return HAlign(rng, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
		}

		public static ExcelRangeBase HAlign(this ExcelRangeBase rng, OfficeOpenXml.Style.ExcelHorizontalAlignment align)
		{
			rng.Style.HorizontalAlignment = align;
			return rng;
		}

		#endregion

		#region Data Validation
		public static dvalcontracts.IExcelDataValidation ApplyTo(this dvalcontracts.IExcelDataValidation dv
			, ExcelRangeBase rng)
		{
			if (dv == null)
			{
				return null;
			}

			ExcelWorksheet sht = rng.Worksheet;
			dvalcontracts.IExcelDataValidation destdv = null;

			if (dv.ValidationType == dval.ExcelDataValidationType.List)
			{
				var dvlist = (dvalcontracts.IExcelDataValidationList)dv;

				var destdvl = rng.Worksheet.DataValidations.AddListValidation(rng.Address);
				destdvl.Formula.ExcelFormula = dvlist.Formula.ExcelFormula;
				foreach (var v in dvlist.Formula.Values)
				{
					destdvl.Formula.Values.Add(v);
				}
				destdv = destdvl;
			}
			else if (dv.ValidationType == dval.ExcelDataValidationType.Whole)
			{
				var dvInt = (dvalcontracts.IExcelDataValidationInt)dv;

				var destdvInt = rng.Worksheet.DataValidations.AddIntegerValidation(rng.Address);
				destdvInt.Operator = dvInt.Operator;
				destdvInt.Formula.ExcelFormula = dvInt.Formula.ExcelFormula;
				destdvInt.Formula.Value = dvInt.Formula.Value;
				destdvInt.Formula2.ExcelFormula = dvInt.Formula2.ExcelFormula;
				destdvInt.Formula2.Value = dvInt.Formula2.Value;

				destdv = destdvInt;
			}
			else if (dv.ValidationType == dval.ExcelDataValidationType.Decimal)
			{
				var dvDec = (dvalcontracts.IExcelDataValidationDecimal)dv;

				var destdvDec = rng.Worksheet.DataValidations.AddDecimalValidation(rng.Address);
				destdvDec.Operator = dvDec.Operator;
				destdvDec.Formula.ExcelFormula = dvDec.Formula.ExcelFormula;
				destdvDec.Formula.Value = dvDec.Formula.Value;
				destdvDec.Formula2.ExcelFormula = dvDec.Formula2.ExcelFormula;
				destdvDec.Formula2.Value = dvDec.Formula2.Value;
				destdv = destdvDec;
			}
			else if (dv.ValidationType == dval.ExcelDataValidationType.DateTime)
			{
				var dvDate = (dvalcontracts.IExcelDataValidationDateTime)dv;

				var destdvDate = rng.Worksheet.DataValidations.AddDateTimeValidation(rng.Address);
				destdvDate.Operator = dvDate.Operator;
				destdvDate.Formula.ExcelFormula = dvDate.Formula.ExcelFormula;
				if (dvDate.Formula.ExcelFormula == null)
				{
					destdvDate.Formula.Value = dvDate.Formula.Value;
				}
				destdvDate.Formula2.ExcelFormula = dvDate.Formula2.ExcelFormula;
				if (dvDate.Formula2.ExcelFormula == null)
				{
					destdvDate.Formula2.Value = dvDate.Formula2.Value;
				}
				destdv = destdvDate;
			}

			if (destdv != null)
			{
				destdv.Error = dv.Error;
				destdv.ErrorStyle = dv.ErrorStyle;
				destdv.ErrorTitle = dv.ErrorTitle;
				destdv.ShowErrorMessage = dv.ShowErrorMessage;

				destdv.Prompt = dv.Prompt;
				destdv.PromptTitle = dv.PromptTitle;
				destdv.ShowInputMessage = dv.ShowInputMessage;

				destdv.AllowBlank = dv.AllowBlank;
			}
			return destdv;
		}
		#endregion

		#region Tables


		/// <summary>
		/// The range occupied by the table
		/// </summary>
		/// <param name="tbl"></param>
		/// <returns></returns>
		public static ExcelRangeBase Range(this table.ExcelTable tbl)
		{
			var nm = tbl.WorkSheet.Workbook.Names.FirstOrDefault(dn => dn.Name == $"{tbl.Name}_Range");
			if (nm != null)
			{
				return nm;
			}
			return tbl.WorkSheet.Cells[tbl.Address.Address];
		}

		/// <summary>
		/// the range occupied by the table data - ie, excludes the header row
		/// </summary>
		/// <param name="tbl"></param>
		/// <returns></returns>
		public static ExcelRangeBase DataBodyRange(this table.ExcelTable tbl)
		{
			var r = tbl.Range();
			return r.Offset(1, 0, r.Rows - 1, r.Columns);
		}
		public static ExcelRangeBase HeaderRowRange(this table.ExcelTable tbl)
		{
			return tbl.Range().Offset(0, 0, 1, tbl.Address.Columns);
		}


		/// <summary>
		/// Range occupied by column, including the header cell
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="col"></param>
		/// <returns></returns>
		public static ExcelRangeBase ColumnRange(this table.ExcelTable tbl, string columnName)
		{
			// offset from the table range:
			// row = 1 (ignore header) col = column position (0-based) rows = table rows - 1, columns = 1
			return tbl.ColumnRange(tbl.Columns[columnName].Position);
		}
		public static ExcelRangeBase ColumnRange(this table.ExcelTable tbl, table.ExcelTableColumn col)
		{
			// offset from the table range:
			// row = 1 (ignore header) col = column position (0-based) rows = table rows - 1, columns = 1
			return tbl.ColumnRange(col.Position);
		}

		public static ExcelRangeBase ColumnRange(this table.ExcelTable tbl, int position)
		{
			// offset from the table range:
			// row = 1 (ignore header) col = column position (0-based) rows = table rows - 1, columns = 1
			return tbl.Range().Offset(0, position, tbl.Address.Rows, 1);
		}
		/// <summary>
		/// Range occupied by column data - ie excluding the header cell
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="col"></param>
		/// <returns></returns>
		public static ExcelRangeBase ColumnDataBodyRange(this table.ExcelTable tbl, string columnName)
		{
			// offset from the table range:
			// row = 1 (ignore header) col = column position (0-based) rows = table rows - 1, columns = 1
			return tbl.ColumnDataBodyRange(tbl.Columns[columnName].Position);
		}
		public static ExcelRangeBase ColumnDataBodyRange(this table.ExcelTable tbl, table.ExcelTableColumn col)
		{
			// offset from the table range:
			// row = 1 (ignore header) col = column position (0-based) rows = table rows - 1, columns = 1
			return tbl.ColumnDataBodyRange(col.Position);
		}
		public static ExcelRangeBase ColumnDataBodyRange(this table.ExcelTable tbl, int position)
		{
			// offset from the table range:
			// row = 1 (ignore header) col = column position (0-based) rows = table rows - 1, columns = 1
			// use tbl.Range() - tbl.Address may not be up-to-date  #1435
			var r = tbl.Range();
			return r.Offset(1, position, r.Rows - 1, 1);
		}
		/// <summary>
		/// Return a string array of all the column names of a table
		/// </summary>
		/// <param name="cols"></param>
		/// <returns></returns>
		public static string[] Names(this IEnumerable<table.ExcelTableColumn> cols)
		{
			return (from c in cols select c.Name).ToArray<string>();
		}

		/// <summary>
		/// Range corresponding to a 0-based index into data rows
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="rowIndex"></param>
		/// <returns></returns>
		public static ExcelRangeBase RowRange(this table.ExcelTable tbl, int rowIndex)
		{
			// offset from the table range:
			// row = rowIndex + 1 (ignore header, 0-based) col = 0 rows = 1, columns = table columns
			return tbl.Range().Offset(rowIndex + 1, 0, 1, tbl.Address.Columns);
		}
		/// <summary>
		/// Lookup a value in a table
		/// </summary>
		/// <param name="tbl">Table to search</param>
		/// <param name="searchValue">Value to look for</param>
		/// <param name="columnToReturn">number or name of column to return. Default 1</param>
		/// <param name="columnToReturn">number or name of column to search. Default 0</param>
		/// <returns></returns>
		public static object Lookup(this table.ExcelTable tbl, object searchValue)
		{
			return tbl.Lookup(searchValue, 1, 0);
		}
		public static object Lookup(this table.ExcelTable tbl, object searchValue, string columnToReturn)
		{
			return tbl.Lookup(searchValue, tbl.Columns[columnToReturn].Position, 0);
		}

		public static object Lookup(this table.ExcelTable tbl, object searchValue, int columnToReturn)
		{
			return tbl.Lookup(searchValue, columnToReturn, 0);
		}
		public static object Lookup(this table.ExcelTable tbl, object searchValue
			, string columnToReturn, string columnToSearch)
		{
			return tbl.Lookup(searchValue
				, tbl.Columns[columnToReturn].Position, tbl.Columns[columnToSearch].Position);
		}
		public static object Lookup(this table.ExcelTable tbl, object searchValue
			, string columnToReturn, int columnToSearch)
		{
			return tbl.Lookup(searchValue, tbl.Columns[columnToReturn].Position, columnToSearch);
		}
		public static object Lookup(this table.ExcelTable tbl, object searchValue
			, int columnToReturn, string columnToSearch)
		{
			return tbl.Lookup(searchValue, columnToReturn, tbl.Columns[columnToSearch].Position);
		}
		public static object Lookup(this table.ExcelTable tbl, object searchValue
			, int columnToReturn, int columnToSearch)
		{
			ExcelRangeBase r = tbl.ColumnDataBodyRange(columnToSearch);
			var match = r.FirstOrDefault(cell => Object.Equals(cell.Value, searchValue));
			if (match == null)
			{
				return null;
			}
			return match.Offset(0, columnToReturn - columnToSearch, 1, 1).Value;
		}

		/// <summary>
		///  load an existing table with data from a ADO.NET DataTable
		///  ONLY WORKS with unlinked tables (ie no corresponding queryTable
		///  This appraoch does not load new rows into the worksheet, so it is safe to use when
		///  tables sit side by side. However, as a consequence, the table Address is not correctly reported
		///  after it has changed (it does get saved correctly, becuase it is changed in the table xml).
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="dt">data table - column count and type must match tbl but column names don't matter</param>
		/// <returns>the Data body range of the table; ie the range without the header row</returns>
		public static ExcelRangeBase Reload(this table.ExcelTable tbl, System.Data.DataTable dt)
		{
			tbl.DataBodyRange().Clear();
			// deal with the situation that the datatable has no rows
			// top left of the data range
			var r = tbl.DataBodyRange().Offset(0, 0, 1, 1);

			if (dt.Rows.Count > 0)
			{
				// put the data in that area without headings
				// note that epplus will not try to create a table from this
				// r is the affected data range
				r = r.LoadFromDataTable(dt, false);
				// the only way to update the table range is to go to the Xml
				
			}
			else
			{
				r = r.Offset(0, 0, 1, tbl.DataBodyRange().Columns);		// 1 empty row
			}
			var newTableRange = r.Offset(-1, 0, r.Rows + 1, r.Columns);
			tbl.TableXml.DocumentElement.SetAttribute("ref", newTableRange.Address);
			var af = tbl.TableXml.DocumentElement.GetElementsByTagName("autoFilter")[0];
			((System.Xml.XmlElement)af).SetAttribute("ref", newTableRange.Address);
			// track the real range of the table via a defined Name
			tbl.WorkSheet.Workbook.Names.ApplyName($"{tbl.Name}_Range", newTableRange);
			return r;
		}
		#endregion

		#region Ranges and Address

		public static bool ContainsCell(this ExcelRange range, ExcelRangeBase cell)
		{
			return (range.FirstOrDefault(item => item.Address == cell.Address) != null);
		}
		#endregion

		#region Defined Names

		/// <summary>
		/// Change the range associated to a name. May be a new name or existing
		/// </summary>
		/// <param name="names">names collection</param>
		/// <param name="name">name for a new or exisiting defined name</param>
		/// <param name="rng">range 'RefersTo'</param>
		/// <returns></returns>
		public static ExcelNamedRange ApplyName(this ExcelNamedRangeCollection names, string name, ExcelRangeBase rng)
		{
			var nm = names.Where(n => n.Name.ToLower() == name.ToLower())
						.FirstOrDefault();
			if (nm != null)
			{
				names.Remove(name);
			}
			return names.Add(name, rng);
		}

		public static ExcelNamedRange ApplyName(this ExcelWorkbook wk, string name, ExcelRangeBase rng)
		{
			return wk.Names.ApplyName(name, rng);
		}
		#endregion

		#region VBAProject
		// epplus is unable to work with Vba projects that it does not create - it corrupts
		// an existing project when the workbook is saved. The workaround is to construct the VbaProject
		// anew, loading the code from text files
		/// <summary>
		/// Load a VBAproject from a folder containing the code for each module
		/// </summary>
		/// <param name="wk">the workbook</param>
		/// <param name="folder"></param>
		public static OfficeOpenXml.VBA.ExcelVbaProject LoadVBAProject(this ExcelWorkbook wk, string folder)
		{
			if (wk.VbaProject == null)
			{
				wk.CreateVBAProject();
			}

			foreach (string fname in System.IO.Directory.GetFiles(folder))
			{
				var modules = wk.VbaProject.Modules;
				string modulename = System.IO.Path.GetFileNameWithoutExtension(fname);
				string ext = System.IO.Path.GetExtension(fname);
				string code = System.IO.File.ReadAllText(fname);
				if (modules.Count(m => m.Name == modulename) == 0)
				{
					switch (ext.ToLower())
					{
						case ".cls":
							var udfmod = wk.VbaProject.Modules.AddClass(modulename, true);
							udfmod.Code = System.IO.File.ReadAllText(fname);
							break;
						case ".bas":
							udfmod = wk.VbaProject.Modules.AddModule(modulename);
							udfmod.Code = System.IO.File.ReadAllText(fname);
							break;
					}
				}
				else
				{
					modules[modulename].Code = code;
				}
			}
			return wk.VbaProject;
		}
		#endregion
	}
}