﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using EPPlusPiv = OfficeOpenXml.Table.PivotTable;

namespace Pineapples.ExcelOutput
{
	public class ExcelPublisher
	{
		public ExcelPublisher()
		{
			TableSheetName = "Table";
			PivotSheetName = "Pivot";
			ChartSheetName = "Chart";
		}
		public string TableSheetName { get; set; }
		public string PivotSheetName { get; set; }
		public string ChartSheetName { get; set; }
		public ExcelPackage PublishExcelTable(System.Data.DataTable dt)
		{
			// lots of guidance from
			//https://www.codeproject.com/Articles/1194712/Advanced-Excels-With-EPPlus#bm10

			ExcelPackage ep = new ExcelPackage();
			ExcelWorkbook wb = ep.Workbook;         // a new package has a workbook...
			ExcelWorksheet ws = wb.Worksheets.Add(TableSheetName);

			using (ExcelRangeBase range = ws.Cells[3, 2])
			{
				var tableStyle = OfficeOpenXml.Table.TableStyles.Medium2;
				range.LoadFromDataTable(dt, true, tableStyle);
			}

			DateTime time = DateTime.UtcNow;
			// data table
			OfficeOpenXml.Table.ExcelTable tblData = ws.Tables[ws.Tables.Count - 1];
			tblData.Name = "tblData";

			// now add a pivot table based on this list

			// data cells
			ExcelRange dataCells = ws.Cells[tblData.Address.Address];

			ws = wb.Worksheets.Add(PivotSheetName);
			ws.View.ShowGridLines = false;
			ExcelRange pvtLocation = ws.Cells["B4"];
			string pvtName = "pivot";
			OfficeOpenXml.Table.PivotTable.ExcelPivotTable pivotTable = ws.PivotTables.Add(pvtLocation, dataCells, pvtName);

			// headers
			pivotTable.ShowHeaders = true;

			// grand total
			pivotTable.ColumnGrandTotals = true;
			pivotTable.GrandTotalCaption = "Total";

			// style
			pivotTable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium9;

			// make a pivot chart based on an indpendent pivot table
			ws = wb.Worksheets.Add(ChartSheetName);
			ws.View.ShowGridLines = false;
			// first the table
			pvtLocation = ws.Cells["M4"];
			pvtName = "chartPivot";
			pivotTable = ws.PivotTables.Add(pvtLocation, dataCells, pvtName);

			// headers
			pivotTable.ShowHeaders = true;

			// grand total
			pivotTable.ColumnGrandTotals = true;
			pivotTable.GrandTotalCaption = "Total";

			// style
			pivotTable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium9;

			// uild the chart on the new pivot
			var chart = ws.Drawings.AddChart(
				"pivotChart",
				OfficeOpenXml.Drawing.Chart.eChartType.ColumnStacked,
				pivotTable
			) as OfficeOpenXml.Drawing.Chart.ExcelBarChart;

			chart.SetPosition(1, 0, 1, 0);
			chart.Border.Fill.Style = eFillStyle.NoFill;
			chart.SetSize(600, 400);
			chart.GapWidth = 25;

			// finally annotate
			ws = wb.Worksheets.Add("About");
			ws.View.ShowGridLines = false;

			wb.Names.Add("Annotation", ws.Cells[1, 1]);
			wb.Names.Add("Version", ws.Cells[2, 1]);

			return ep;
		}

		/// <summary>
		/// For an Excel package created by PublishExcelTable
		/// return the pivot table for further configuration
		/// </summary>
		/// <param name="ep"></param>
		/// <returns></returns>
		public OfficeOpenXml.Table.ExcelTable PublishedTable(OfficeOpenXml.ExcelPackage ep)
		{
			return ep.Workbook.Worksheets[TableSheetName].Tables[0];
		}

		/// <summary>
		/// For an Excel package created by PublishExcelTable
		/// return the pivot table for further configuration
		/// </summary>
		/// <param name="ep"></param>
		/// <returns></returns>
		public OfficeOpenXml.Table.PivotTable.ExcelPivotTable PublishedPivot(OfficeOpenXml.ExcelPackage ep)
		{
			return ep.Workbook.Worksheets[PivotSheetName].PivotTables[0];
		}

		/// <summary>
		/// For an Excel package created by PublishExcelTable
		/// return the pivot chart for further configuration
		/// </summary>
		/// <param name="ep"></param>
		/// <returns></returns>
		public OfficeOpenXml.Drawing.Chart.ExcelChart PublishedChart(OfficeOpenXml.ExcelPackage ep)
		{
			return (OfficeOpenXml.Drawing.Chart.ExcelChart)ep.Workbook.Worksheets[ChartSheetName].Drawings[0];
		}

		/// <summary>
		/// For an Excel package created by PublishExcelTable
		/// return the pivot table that is the basis of the chart
		/// </summary>
		/// <param name="ep"></param>
		/// <returns></returns>
		public OfficeOpenXml.Table.PivotTable.ExcelPivotTable PublishedChartPivot(OfficeOpenXml.ExcelPackage ep)
		{
			return ep.Workbook.Worksheets[ChartSheetName].PivotTables[0];
		}
		public void AnnotateExcelPackage(ExcelPackage ep, string annotation, string version = "")
		{
			var nm = ep.Workbook.Names.First(n => n.Name == "Annotation");
			if (nm != null)
			{
				nm.Value = annotation;
				if (version != String.Empty)
				{
					nm.Offset(1, 0).Value = $"Warehouse version Id: {version}";
				}
			}
		}

		public EPPlusPiv.ExcelPivotTable ConfigPivotTable(OfficeOpenXml.ExcelPackage ep
			, object dataFields, object rowFields, object columnFields
			, string pageField, object selectedPageItem)
		{
			return ConfigPivotTable(PublishedPivot(ep), dataFields, rowFields, columnFields, pageField, selectedPageItem);
		}
		public EPPlusPiv.ExcelPivotTable ConfigPivotTable(EPPlusPiv.ExcelPivotTable piv
			, object dataFields, object rowFields, object columnFields
			, string pageField, object selectedPageItem)
		{
			
			ConfigPivotTable(piv, dataFields, rowFields, columnFields, pageField);
			piv.FilterField(pageField, selectedPageItem);
			return piv;
		}

		public EPPlusPiv.ExcelPivotTable ConfigPivotTable(OfficeOpenXml.ExcelPackage ep
			, object dataFields, object rowFields = null, object columnFields = null, object pageFields = null)
		{
			return ConfigPivotTable(PublishedPivot(ep), dataFields, rowFields, columnFields, pageFields);
		}
		/// <summary>
		/// Set the structure of a pivot table
		/// arguments may be strings or string arrays; e.g.
		/// epub.ConfigPivotTable(ep, "Enrol", "Age", new[] { "ClassLevel", "GenderCode" }, "SurveyYear");
		/// </summary>
		/// <param name="piv">the pivot table</param>
		/// <param name="dataFields">string or string array of dataFields</param>
		/// <param name="rowFields">string or string array of rowFields</param>
		/// <param name="columnFields">string or string array of dataFields</param>
		/// <param name="pageFields">string or string array of pageFields</param>
		/// <returns></returns>
		public EPPlusPiv.ExcelPivotTable ConfigPivotTable(EPPlusPiv.ExcelPivotTable piv
		, object dataFields, object rowFields = null, object columnFields = null, object pageFields = null)
		{

			var ftype = dataFields.GetType();
			if (ftype.IsArray)
			{

				foreach (string nm in (string[])dataFields)
				{
					piv.DataFields.Add(piv.Fields[nm])
						.Function = EPPlusPiv.DataFieldFunctions.Sum;
				}
			}
			else if (ftype == typeof(string))
			{
				piv.DataFields.Add(piv.Fields[(string)dataFields])
						.Function = EPPlusPiv.DataFieldFunctions.Sum;
			}

			if (rowFields != null)
			{
				ftype = rowFields.GetType();
				if (ftype.IsArray)
				{

					foreach (string nm in (string[])rowFields)
					{
						piv.RowFields.Add(piv.Fields[nm]);
					}
				}
				else if (ftype == typeof(string))
				{
					piv.RowFields.Add(piv.Fields[(string)rowFields]);
				}
			}

			if (columnFields != null)
			{
				ftype = columnFields.GetType();
				if (ftype.IsArray)
				{

					foreach (string nm in (string[])columnFields)
					{
						piv.ColumnFields.Add(piv.Fields[nm]);
					}
				}
				else if (ftype == typeof(string))
				{
					piv.ColumnFields.Add(piv.Fields[(string)columnFields]);
				}
			}

			if (pageFields != null)
			{
				ftype = pageFields.GetType();
				if (ftype.IsArray)
				{

					foreach (string nm in (string[])pageFields)
					{
						piv.PageFields.Add(piv.Fields[nm]);
					}
				}
				else if (ftype == typeof(string))
				{
					piv.PageFields.Add(piv.Fields[(string)pageFields]);
				}
			}
			return piv;

		}
		public OfficeOpenXml.Drawing.Chart.ExcelChart ConfigPivotChart(OfficeOpenXml.ExcelPackage ep
			, object dataFields, object rowFields = null, object columnFields = null, object pageFields = null)
		{

			ConfigPivotTable(PublishedChartPivot(ep), dataFields, rowFields, columnFields, pageFields);
			return PublishedChart(ep);
		}
	}
}