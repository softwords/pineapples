﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using OfficeOpenXml;

namespace Pineapples.ExcelOutput
{
	class UisWorkbook
	{
		public ExcelPackage Create(int year, Dictionary<string, Object> uisDS)
		{
			string template =
System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/UISTemplate.xlsx");
			System.IO.FileInfo info = new System.IO.FileInfo(template);
			ExcelPackage p = new ExcelPackage(info);
			PopulateUisWorkbook(p, year, uisDS);
			return p;

		}
		public ExcelPackage PopulateUisWorkbook(ExcelPackage p, int year, Dictionary<string, Object> uisDS)
		{
			ExcelWorkbook survey = p.Workbook;
			sheetA1(survey.Worksheets["VAL_A1"], year, (DataTable)uisDS["A1"]);
			sheetA2(survey.Worksheets["A2"], year, (DataTable)uisDS["A2"]);
			sheetA3(survey.Worksheets["A3"], year, (DataTable)uisDS["A3"]);
			sheetA5(survey.Worksheets["A5"], year, (DataTable)uisDS["A5"]);
			sheetA6(survey.Worksheets["A6"], year, (DataTable)uisDS["A6"]);
			sheetA9(survey.Worksheets["A9"], year, (DataTable)uisDS["A9"]);

			// A10 uses same data as A9 - it is inserted that way in the data layer
			sheetA10(survey.Worksheets["A10"], year, (DataTable)uisDS["A10"]);
			sheetA12(survey.Worksheets["A12"], year, (DataTable)uisDS["A12"]);
			sheetA13(survey.Worksheets["A13"], year, (DataTable)uisDS["A13"]);

			return p;
		}

		public ExcelPackage TeacherAudit(ExcelPackage p, int year, Dictionary<string, Object> uisDS)
		{
			ExcelWorkbook survey = p.Workbook;

			Pineapples.ExcelOutput.TeacherIscedAudit rpt = new ExcelOutput.TeacherIscedAudit();
			rpt.PrepareWorkbook(survey);

			// summary audit sheet
			ExcelWorksheet ws = survey.Worksheets.Add("Teacher Audit Summary");
			System.Data.DataTable dt = (DataTable)uisDS["Summary"];
			rpt.CreateSummary(ws, dt);

			//Create audit sheet for ISCED 0

			ws = survey.Worksheets.Add("ISCED 0 Teacher Audit");
			dt = (DataTable)uisDS["ISCED0"];
			rpt.Create(ws, dt);

			ws = survey.Worksheets.Add("ISCED 1 Teacher Audit");
			dt = (DataTable)uisDS["ISCED1"];
			rpt.Create(ws, dt);

			ws = survey.Worksheets.Add("ISCED 2 Teacher Audit");
			dt = (DataTable)uisDS["ISCED2"];
			rpt.Create(ws, dt);

			ws = survey.Worksheets.Add("ISCED 3 Teacher Audit");
			dt = (DataTable)uisDS["ISCED3"];
			rpt.Create(ws, dt);

			ws = survey.Worksheets.Add("Secondary General");
			dt = (DataTable)uisDS["SECONDARY4"];
			rpt.Create(ws, dt);

			return p;
		}

		public ExcelPackage EnrolAudit(ExcelPackage p, int year, System.Data.DataTable dt)
		{
			ExcelWorkbook survey = p.Workbook;

			Pineapples.ExcelOutput.EnrolIscedAudit rpt = new ExcelOutput.EnrolIscedAudit();
			rpt.PrepareWorkbook(survey);
			rpt.Create(survey, dt);
			return p;
		}
		public ExcelPackage SchoolAudit(ExcelPackage p, int year, System.Data.DataTable dt)
		{
			ExcelWorkbook survey = p.Workbook;

			Pineapples.ExcelOutput.SchoolIscedAudit rpt = new ExcelOutput.SchoolIscedAudit();
			rpt.PrepareWorkbook(survey);
			var sht = rpt.Create(survey, dt);
			sht.Cells[2, 3].Value = "Go to Sheet A12";
			sht.Cells[2, 3].Hyperlink = new Uri("#'A12'!A1", UriKind.Relative);

			sht.Cells[4, 3].Value = "Go to Sheet A13";
			sht.Cells[4, 3].Hyperlink = new Uri("#'A13'!A1", UriKind.Relative);

			var shtA = survey.Worksheets["A12"];
			shtA.Cells[3, 4].Hyperlink = new Uri($"#'{sht.Name}'!A1", UriKind.Relative);
			shtA.Cells[3, 4].AddComment("Click to go to Audit page", "Pacific EMIS");
			shtA = survey.Worksheets["A13"];
			shtA.Cells[3, 4].Hyperlink = new Uri($"#'{sht.Name}'!A1", UriKind.Relative);
			shtA.Cells[3, 4].AddComment("Click to go to Audit page", "Pacific EMIS");
			return p;
		}
		private void sheetA1(ExcelWorksheet sht, int year, System.Data.DataTable A1Data)
		{
			var row = A1Data.Rows[0];
			ExcelWorkbook survey = sht.Workbook;
			// set the country value - this is an index that drives a combobox
			string countryCode = row["CountryCode"].ToString();
			var lkp = survey.Names["CountryCodeLookup"];
			var countryCell = lkp.First(r => r.Value.ToString() == countryCode);
			var countryCodeIndex = countryCell.Offset(0, -1).Value;
			survey.Names["CountryIndex"].Value = countryCodeIndex;

		}
		private void sheetA2(ExcelWorksheet sht, int year, System.Data.DataTable A2Data)
		{
			const int MALE_OFFSET = 0;
			int FEMALE_OFFSET;

			FEMALE_OFFSET = sht.Names["Female"].Start.Row - sht.Names["Male"].Start.Row;

			foreach (System.Data.DataRow row in A2Data.Rows)
			{
				string nmisced = ((string)row["Isced"]).Replace(" ", "_");
				if (sht.Names.ContainsKey(nmisced))
				{
					int r = sht.Names[(string)row["GovGroup"]].Start.Row;
					int c = sht.Names[nmisced].Start.Column;
					sht.Cells[r + MALE_OFFSET, c].Value = row["EnrolM"];
					sht.Cells[r + FEMALE_OFFSET, c].Value = row["EnrolF"];
					sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
					sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];
				}
			}
		}

		private void sheetA3(ExcelWorksheet sht, int year, System.Data.DataTable A3Data)
		{

			const int MALE_OFFSET = 0;
			int FEMALE_OFFSET;

			FEMALE_OFFSET = sht.Names["Female"].Start.Row - sht.Names["Male"].Start.Row;

			foreach (System.Data.DataRow row in A3Data.Rows)
			{
				// get the offset for the age
				int r = 0;
				string nmAge = GetAgeRangeName((string)row["AgeGroup"]); // a range name for the row appropriate to the age

				string nmisced = ((string)row["Isced"]).Replace(" ", "_");
				if (sht.Names.ContainsKey(nmisced))
				{
					r = sht.Names[nmAge].Start.Row;
					int c = sht.Names[nmisced].Start.Column;
					sht.Cells[r + MALE_OFFSET, c].Value = row["EnrolM"];
					sht.Cells[r + FEMALE_OFFSET, c].Value = row["EnrolF"];
					sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
					sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];

				}
			}
		}

		private void sheetA5(ExcelWorksheet sht, int year, System.Data.DataTable A5Data)
		{
			// test
			const int MALE_OFFSET = 0;
			int FEMALE_OFFSET;

			FEMALE_OFFSET = sht.Names["Female"].Start.Row - sht.Names["Male"].Start.Row;

			foreach (System.Data.DataRow row in A5Data.Rows)
			{
				// get the offset for the age
				int r = 0;
				string nmAge = GetAgeRangeName((string)row["AgeGroup"]); // a range name for the row appropriate to the age


				if (sht.Names.ContainsKey($"Grade_{row["Grade"].ToString()}"))
				{
					r = sht.Names[nmAge].Start.Row;
					int c = sht.Names[$"Grade_{row["Grade"].ToString()}"].Start.Column;
					switch (nmAge)
					{
						case "Age_Aggregated":
							sht.Cells[r + MALE_OFFSET, c].Value = row["RepM"];
							sht.Cells[r + FEMALE_OFFSET, c].Value = row["RepF"];
							break;
						default:
							sht.Cells[r + MALE_OFFSET, c].Value = row["EnrolM"];
							sht.Cells[r + FEMALE_OFFSET, c].Value = row["EnrolF"];
							break;
					}
					sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
					sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];

				}
			}
		}

		private void sheetA6(ExcelWorksheet sht, int year, System.Data.DataTable A6Data)
		{
			// test
			const int MALE_OFFSET = 0;
			int FEMALE_OFFSET;

			FEMALE_OFFSET = sht.Names["Female"].Start.Row - sht.Names["Male"].Start.Row;

			foreach (System.Data.DataRow row in A6Data.Rows)
			{
				// get the offset for the age
				int r = 0;
				string nmAge = GetAgeRangeName((string)row["AgeGroup"]); // a range name for the row appropriate to the age

				string nmCol = string.Empty;

				if (row["Grade"] == DBNull.Value)
				{
					// this is the totals for ISCED3.4 across all grades
					nmCol = ((string)row["Isced"]).Replace(" ", "_").ToString();
				}
				else
				{
					nmCol = $"Grade_{row["Grade"]}";
				}
				if (sht.Names.ContainsKey(nmCol))
				{
					r = sht.Names[nmAge].Start.Row;
					int c = sht.Names[nmCol].Start.Column;
					switch (nmAge)
					{
						case "Age_Aggregated":
							sht.Cells[r + MALE_OFFSET, c].Value = row["RepM"];
							sht.Cells[r + FEMALE_OFFSET, c].Value = row["RepF"];
							break;
						default:
							sht.Cells[r + MALE_OFFSET, c].Value = row["EnrolM"];
							sht.Cells[r + FEMALE_OFFSET, c].Value = row["EnrolF"];
							break;
					}
					sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
					sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];

				}
			}
		}

		private void sheetA9(ExcelWorksheet sht, int year, System.Data.DataTable A9Data)
		{
			// test
			const int MALE_OFFSET = 0;
			int FEMALE_OFFSET;

			FEMALE_OFFSET = sht.Names["Female"].Start.Row - sht.Names["Male"].Start.Row;

			foreach (System.Data.DataRow row in A9Data.Rows)
			{
				string nmisced = ((string)row["Isced"]).Replace(" ", "_");
				if (sht.Names.ContainsKey(nmisced))
				{
					int c = sht.Names[nmisced].Start.Column;
					if (row["GovGroup"] != System.DBNull.Value)
					{
						int r = sht.Names[(string)row["GovGroup"]].Start.Row;
						/* prior to 24 7 2021 - absolute count of teachers
						sht.Cells[r + MALE_OFFSET, c].Value = row["NumTeachersM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["NumTeachersF"];
						*/
						/* 24 7 2021 using 'teaching weighted' values */
						sht.Cells[r + MALE_OFFSET, c].Value = row["FtPtM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FtPtF"];

						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];
					}
					else
					{
						// place the new hires and FTE if this is sum across Public/Private
						int r = sht.Names["NewHires"].Start.Row;

						/* prior to 24 7 2021 - absolute count of teachers
						sht.Cells[r + MALE_OFFSET, c].Value = row["NumNewTeachersM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["NumNewTeachersF"];
						*/
						/* 24 7 2021 using 'teaching weighted' values */
						sht.Cells[r + MALE_OFFSET, c].Value = row["FtPtNewM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FtPtNewF"];

						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];

						r = sht.Names["FTE"].Start.Row;

						sht.Cells[r + MALE_OFFSET, c].Value = row["FteM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FteF"];
						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];
					}
				}
			}
		}

		private void sheetA10(ExcelWorksheet sht, int year, System.Data.DataTable A9Data)
		{
			// test
			const int MALE_OFFSET = 0;
			int FEMALE_OFFSET;

			const int PUBLIC_OFFSET = 0;
			int PRIVATE_OFFSET;

			PRIVATE_OFFSET = sht.Names["Private"].Start.Row - sht.Names["Public"].Start.Row;
			FEMALE_OFFSET = sht.Names["Female"].Start.Row - sht.Names["Male"].Start.Row;


			foreach (System.Data.DataRow row in A9Data.Rows)
			{
				string nmisced = ((string)row["Isced"]).Replace(" ", "_");
				if (sht.Names.ContainsKey(nmisced))
				{

					int c = sht.Names[nmisced].Start.Column;
					if (row["GovGroup"] != System.DBNull.Value)
					{
						int r = sht.Names["QualifiedFTPT"].Start.Row
							+ (((string)row["GovGroup"] == "Public") ? PUBLIC_OFFSET : PRIVATE_OFFSET);

						/* prior to 24 7 2021 - absolute count of teachers
						sht.Cells[r + MALE_OFFSET, c].Value = row["QualifiedM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["QualifiedF"];
						*/
						/* 24 7 2021 using 'teaching weighted' values */
						sht.Cells[r + MALE_OFFSET, c].Value = row["FtPtQualifiedM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FtPtQualifiedF"];

						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];

						r = sht.Names["CertifiedFTPT"].Start.Row
							+ (((string)row["GovGroup"] == "Public") ? PUBLIC_OFFSET : PRIVATE_OFFSET);
						/* prior to 24 7 2021 - absolute count of teachers
						sht.Cells[r + MALE_OFFSET, c].Value = row["CertifiedM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["CertifiedF"];
						*/
						/* 24 7 2021 using 'teaching weighted' values */
						sht.Cells[r + MALE_OFFSET, c].Value = row["FtPtCertifiedM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FtPtCertifiedF"];

						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];
					}
					else
					{
						// place the Inservice and FTE if this is sum across Public/Private

						// place the new hires and FTE if this is sum across Public/Private
						int r = sht.Names["InService"].Start.Row;
						/*
						sht.Cells[r + MALE_OFFSET, c].Value = row["InServiceM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["InServiceF"];
						*/
						sht.Cells[r + MALE_OFFSET, c].Value = row["FtPtInServiceM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FtPtInServiceF"];

						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];

						r = sht.Names["QualifiedFTE"].Start.Row;
						sht.Cells[r + MALE_OFFSET, c].Value = row["FteQualifiedM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FteQualifiedF"];
						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];

						r = sht.Names["CertifiedFTE"].Start.Row;

						sht.Cells[r + MALE_OFFSET, c].Value = row["FteCertifiedM"];
						sht.Cells[r + FEMALE_OFFSET, c].Value = row["FteCertifiedF"];
						sht.Cells[r + MALE_OFFSET, c + 1].Value = row["Flag"];
						sht.Cells[r + FEMALE_OFFSET, c + 1].Value = row["Flag"];
					}
				}
			}
		}

		private void sheetA12(ExcelWorksheet sht, int year, System.Data.DataTable A12Data)
		{
			foreach (System.Data.DataRow row in A12Data.Rows)
			{
				string nmisced = ((string)row["Isced"]).Replace(" ", "_");
				if (sht.Names.ContainsKey(nmisced))
				{
					int c = sht.Names[nmisced].Start.Column;
					int r = sht.Names[(string)row["GovGroup"]].Start.Row;
					sht.Cells[r, c].Value = row["NumSchools"];
				}
			}
		}

		private void sheetA13(ExcelWorksheet sht, int year, System.Data.DataTable A13Data)
		{
			const int PUBLIC_OFFSET = 0;
			int PRIVATE_OFFSET;

			PRIVATE_OFFSET = sht.Names["Private"].Start.Row - sht.Names["Public"].Start.Row;

			foreach (System.Data.DataRow row in A13Data.Rows)
			{
				string nmisced = ((string)row["Isced"]).Replace(" ", "_");
				if (sht.Names.ContainsKey(nmisced))
				{
					int c = sht.Names[nmisced].Start.Column;
					int r = sht.Names[(string)row["Item"]].Start.Row
							+ (((string)row["GovGroup"] == "Public") ? PUBLIC_OFFSET : PRIVATE_OFFSET);
					sht.Cells[r, c].Value = row["Num"];
					sht.Cells[r, ++c].Value = row["Flag"];
				}
			}
		}
		private string GetAgeRangeName(string ageValue)
		{
			string nmAge = String.Empty;
			switch (ageValue)
			{
				case null:
					nmAge = "Age_Unknown";
					break;

				case "<2":
					nmAge = "Age_1";
					break;

				case "<4":
					nmAge = "Age_3";
					break;
				case "<10":
					nmAge = "Age_9";
					break;

				case ">24":
					nmAge = "Age_25";
					break;
				case "25-29":
					nmAge = "Age_25";
					break;

				case ">29":
					nmAge = "Age_30";
					break;
				case "Aggregated":
					nmAge = "Age_Aggregated";
					break;
				default:
					nmAge = $"Age_{ageValue}";
					break;
			}
			return nmAge;
		}

	}
}