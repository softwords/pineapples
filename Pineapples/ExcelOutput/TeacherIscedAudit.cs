﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using System.Data;

namespace Pineapples.ExcelOutput
{
	/// <summary>
	/// Class to genderate a Teacher Isced audit sheet
	/// </summary>
	public class TeacherIscedAudit
	{
		const int TABLE_LEFT = 7;

		public TeacherIscedAudit()
		{
		}

		public ExcelWorkbook PrepareWorkbook(ExcelWorkbook wb)
		{
			OfficeOpenXml.Style.XmlAccess.ExcelNamedStyleXml s = wb.Styles.CreateNamedStyle("dot");
			s.Style.Font.Name = "Wingdings";
			s.Style.Font.Color.SetColor(System.Drawing.Color.DarkGray);
			s.Style.Numberformat.Format = @"l;;";
			s.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
			s.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

			// similar with block
			s = wb.Styles.CreateNamedStyle("block");
			s.Style.Font.Name = "Wingdings";
			s.Style.Font.Color.SetColor(System.Drawing.Color.DarkBlue);
			s.Style.Numberformat.Format = @"\n;;";      // note the escape sequence becuase n has a special meaning in number format
			s.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
			s.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

			// style for fte columns - 2 decimal suppres 0
			s = wb.Styles.CreateNamedStyle("fte");
			s.Style.Numberformat.Format = "0.00;;";


			return wb;
		}
		public ExcelWorksheet Create(ExcelWorksheet ws, System.Data.DataTable dt)
		{

			ExcelWorkbook wb = ws.Workbook;

			ws.View.ShowGridLines = false;

			using (ExcelRangeBase range = ws.Cells[15, 2])
			{
				var tableStyle = OfficeOpenXml.Table.TableStyles.Medium2;
				
				// can't find a direct way to add columns to the table in epplus
				// so add empty fields to the datatable before loading
				dt.Columns.Add("M");
				dt.Columns.Add("QualifiedM");
				dt.Columns.Add("CertifiedM");

				dt.Columns.Add("F");
				dt.Columns.Add("QualifiedF");
				dt.Columns.Add("CertifiedF");

				dt.Columns.Add("Qualified FTE");
				dt.Columns.Add("Certified FTE");

				dt.Columns.Add("M FTE");
				dt.Columns.Add("QualifiedM FTE");
				dt.Columns.Add("CertifiedM FTE");

				dt.Columns.Add("F FTE");
				dt.Columns.Add("QualifiedF FTE");
				dt.Columns.Add("CertifiedF FTE");

				dt.Columns.Add("SelectionFTPT");
				dt.Columns.Add("Qualified FTPT");
				dt.Columns.Add("Certified FTPT");

				dt.Columns.Add("M FTPT");
				dt.Columns.Add("F FTPT");

				dt.Columns.Add("QualifiedM FTPT");
				dt.Columns.Add("CertifiedM FTPT");

				
				dt.Columns.Add("QualifiedF FTPT");
				dt.Columns.Add("CertifiedF FTPT");

				range.LoadFromDataTable(dt, true, tableStyle);
				// LoadFromDataTable gives the newly created table the name of the source DataTable
				var tbl = ws.Tables[0];
				tbl.Name = "TA_" + dt.TableName;

				foreach (var col in tbl.Columns)
				{
					switch (col.Name)
					{
						case "Qualified":
						case "Certified":
							col.DataCellStyleName = "block";
							break;
						case "SelectionFTE":
							col.DataCellStyleName = "fte";
							break;

						case "TPK":
						case "T00":
						case "T01":
						case "T02":
						case "T03":
						case "T04":
						case "T05":
						case "T06":
						case "T07":
						case "T08":
						case "T09":
						case "T10":
						case "T11":
						case "T12":
						case "T13":
						case "T14":
						case "T15":
							col.DataCellStyleName = "dot";
							//		var rng = ws.Cells[tbl.Address.Address];
							//			rng.Offset(1, col.Position, rng.Rows-1, 1).Style.Font.Name="Wingdings" ;

							break;
						case "M":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""M"",1,0)";
							break;
						case "F":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""F"",1,0)";
							break;

						case "QualifiedM":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([M]* [Qualified])";
							break;
						case "QualifiedF":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([F]* [Qualified])";
							break;

						case "CertifiedM":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([M]* [Certified])";
							break;
						case "CertifiedF":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([F]* [Certified])";
							break;


						case "Qualified FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTE] * [Qualified])";
							break;
						case "Certified FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTE] * [Certified])";
							break;

						case "M FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""M"",[SelectionFTE],0)";
							break;
						case "F FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""F"",[SelectionFTE],0)";
							break;

						case "QualifiedM FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTE]* [Qualified])";
							break;
						case "QualifiedF FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTE]* [Qualified])";
							break;

						case "CertifiedM FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTE]* [Certified])";
							break;
						case "CertifiedF FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTE]* [Certified])";
							break;

						case "SelectionFTPT": // the proportion of Teaching Activities in the current ISCED
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=[SelectionActivities] / ([Activities] - [A] -[X])";
							break;

						case "Qualified FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTPT] * [Qualified])";
							break;
						case "Certified FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTPT] * [Certified])";
							break;

						case "M FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""M"",[SelectionFTPT],0)";
							break;
						case "F FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""F"",[SelectionFTPT],0)";
							break;

						case "QualifiedM FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTPT]* [Qualified])";
							break;
						case "QualifiedF FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTPT]* [Qualified])";
							break;

						case "CertifiedM FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTPT]* [Certified])";
							break;
						case "CertifiedF FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTPT]* [Certified])";
							break;
					}
				}
				// set up the total arrays

				int r = 2;

				r = TotalBlock(ws, r, ExcelAggregateScope.All_Rows, "List Totals");

				r +=2; // leave a row

				TotalBlock(ws, r, ExcelAggregateScope.Filtered_Rows, "Filtered Rows");

			}
			return ws;
		}

		public ExcelWorksheet CreateSummary(ExcelWorksheet ws, System.Data.DataTable dt)
		{

			ExcelWorkbook wb = ws.Workbook;

			ws.View.ShowGridLines = false;

			using (ExcelRangeBase range = ws.Cells[6, 2])
			{
				var tableStyle = OfficeOpenXml.Table.TableStyles.Medium2;
				range.LoadFromDataTable(dt, true, tableStyle);
				// LoadFromDataTable gives the newly created table the name of the source DataTable
				var tbl = ws.Tables[0];
				tbl.Name = "TA_" + dt.TableName;
				ws.Cells[tbl.Address.Address].AutoFitColumns(10,30);
				ws.View.FreezePanes(7, 7);
				foreach (var col in tbl.Columns)
				{
					switch (col.Name)
					{
						case "Qualified":
						case "Certified":
							col.DataCellStyleName = "block";
							break;
						case "ISCED 01 FTPT":
						case "ISCED 02 FTPT":
						case "ISCED 10 FTPT":
						case "ISCED 24 FTPT":
						case "ISCED 25 FTPT":
						case "ISCED 34 FTPT":
						case "ISCED 35 FTPT":
						case "FTPT":
						case "ISCED 01 FTE":
						case "ISCED 02 FTE":
						case "ISCED 10 FTE":
						case "ISCED 24 FTE":
						case "ISCED 25 FTE":
						case "ISCED 34 FTE":
						case "ISCED 35 FTE":
						case "FTE":

						case "Admin":
						case "Other":
							col.DataCellStyleName = "fte";
							break;

						
						case "T15":
							col.DataCellStyleName = "dot";
							//		var rng = ws.Cells[tbl.Address.Address];
							//			rng.Offset(1, col.Position, rng.Rows-1, 1).Style.Font.Name="Wingdings" ;

							break;
						case "M":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""M"",1,0)";
							break;
						case "F":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""F"",1,0)";
							break;

						case "QualifiedM":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([M]* [Qualified])";
							break;
						case "QualifiedF":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([F]* [Qualified])";
							break;

						case "CertifiedM":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([M]* [Certified])";
							break;
						case "CertifiedF":
							col.DataCellStyleName = "block";
							col.CalculatedColumnFormula = @"=([F]* [Certified])";
							break;


						case "Qualified FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTE] * [Qualified])";
							break;
						case "Certified FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTE] * [Certified])";
							break;

						case "M FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""M"",[SelectionFTE],0)";
							break;
						case "F FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""F"",[SelectionFTE],0)";
							break;

						case "QualifiedM FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTE]* [Qualified])";
							break;
						case "QualifiedF FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTE]* [Qualified])";
							break;

						case "CertifiedM FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTE]* [Certified])";
							break;
						case "CertifiedF FTE":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTE]* [Certified])";
							break;

						case "SelectionFTPT": // the proportion of Teaching Activities in the current ISCED
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=[SelectionActivities] / ([Activities] - [A] -[X])";
							break;

						case "Qualified FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTPT] * [Qualified])";
							break;
						case "Certified FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([SelectionFTPT] * [Certified])";
							break;

						case "M FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""M"",[SelectionFTPT],0)";
							break;
						case "F FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=if([GenderCode]=""F"",[SelectionFTPT],0)";
							break;

						case "QualifiedM FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTPT]* [Qualified])";
							break;
						case "QualifiedF FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTPT]* [Qualified])";
							break;

						case "CertifiedM FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([M FTPT]* [Certified])";
							break;
						case "CertifiedF FTPT":
							col.DataCellStyleName = "fte";
							col.CalculatedColumnFormula = @"=([F FTPT]* [Certified])";
							break;
					}
				}
				// set up the total arrays

				int r = 2;

				r = SummaryTotalBlock(ws, r, ExcelAggregateScope.All_Rows, "List Totals");

				r += 2; // leave a row

				SummaryTotalBlock(ws, r, ExcelAggregateScope.Filtered_Rows, "Filtered Rows");
			}
			return ws;
		}
		private int SummaryTotalBlock(ExcelWorksheet ws, int r, ExcelAggregateScope filter, string header)
		{
			int c = ws.Tables[0].Address.Start.Column;
			string tblName = ws.Tables[0].Name;
			var cols = ws.Tables[0].Columns;
			int cc = 0;

			foreach(var col in ws.Tables[0].Columns)
			{
				switch (col.Name)
				{
					case "Certified":
						cc = col.Position + c;
						ws.Cells[r, cc].HAlignRight().Value = $"{header}";
						break;
					case "ISCED 01 FTPT":
					case "ISCED 02 FTPT":
					case "ISCED 10 FTPT":
					case "ISCED 24 FTPT":
					case "ISCED 25 FTPT":
					case "ISCED 34 FTPT":
					case "ISCED 35 FTPT":
					case "FTPT":

					case "ISCED 01 FTE":
					case "ISCED 02 FTE":
					case "ISCED 10 FTE":
					case "ISCED 24 FTE":
					case "ISCED 25 FTE":
					case "ISCED 34 FTE":
					case "ISCED 35 FTE":
					case "FTE":

					case "Admin":
					case "Other":
						cc = col.Position + c;
						ws.Cells[r, cc].Numberformat("#,##0.00").Formula = 
							$"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[{col.Name}])";
						break;
				}
			}

			return r;
		}

		private int TotalBlock(ExcelWorksheet ws, int r, ExcelAggregateScope filter, string header)
		{
			// headings
			int c = TABLE_LEFT;
			string tblName = ws.Tables[0].Name;
			ws.Cells[r, c-2].Value = header;
			c++;
			ws.Cells[r, c++].Vertical().Underline().Value = "FTPT";
			ws.Cells[r, c++].Vertical().Underline().Value = "Qualified FTPT";
			ws.Cells[r, c++].Vertical().Underline().Value = "Certified FTPT";
			c += 2;
			ws.Cells[r, c++].Vertical().Underline().Value = "FTE";
			ws.Cells[r, c++].Vertical().Underline().Value = "Qualified FTE";
			ws.Cells[r, c++].Vertical().Underline().Value = "Trained FTE";
			c += 2;
			ws.Cells[r, c++].Vertical().Underline().Value = "Gross FTPT";
			ws.Cells[r, c++].Vertical().Underline().Value = "Qualified Gross";
			ws.Cells[r, c++].Vertical().Underline().Value = "Trained Gross";

			r++; // M row
			c = TABLE_LEFT;
			ws.Cells[r, c++].HAlignRight().Value = "M";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[M FTPT])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[QualifiedM FTPT])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[CertifiedM FTPT])";
			//leave a column
			c++;
			ws.Cells[r, c++].HAlignRight().Value = "M";

			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[M FTE])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[QualifiedM FTE])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[CertifiedM FTE])";

			c++;
			ws.Cells[r, c++].HAlignRight().Value = "M";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[M])";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[QualifiedM])";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[CertifiedM])";
			//leave a column

			r++; // F row
			c = TABLE_LEFT;
			ws.Cells[r, c++].HAlignRight().Value = "F";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[F FTPT])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[QualifiedF FTPT])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[CertifiedF FTPT])";
			//leave a column
			c++;
			ws.Cells[r, c++].HAlignRight().Value = "F";

			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[F FTE])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[QualifiedF FTE])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[CertifiedF FTE])";
			c++;
			ws.Cells[r, c++].HAlignRight().Value = "F";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[F])";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[QualifiedF])";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[CertifiedF])";

			r++; // total
			c = TABLE_LEFT;
			ws.Cells[r, c++].HAlignRight().Value = "M + F";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[SelectionFTPT])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[Qualified FTPT])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[Certified FTPT])";
			//leave a column
			c++;
			ws.Cells[r, c++].HAlignRight().Value = "M + F";

			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[SelectionFTE])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[Qualified FTE])";
			ws.Cells[r, c++].Numberformat("#,##0.00").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[Certified FTE])";
			c++;
			ws.Cells[r, c++].HAlignRight().Value = "M + F";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = 
				$"=_xlfn.AGGREGATE({ExcelAggregateFunction.Count:D}, {filter:D}, {tblName}[tID])";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[Qualified])";
			ws.Cells[r, c++].Numberformat("#,##0").Formula = $"=_xlfn.AGGREGATE({ExcelAggregateFunction.Sum:D}, {filter:D}, {tblName}[Certified])";


			return r;
		}

		

	}
}