﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Pineapples.Models
{
    public class ReportInfo
    {

        public ReportInfo()
        {
            //  args = new List<KeyValuePair<string, object>>();
            //args = System.Web.HttpUtility.ParseQueryString(string.Empty);
        }
        public string path;        // the path to the report
        public string format;      // the render format
        public string filename;    // name to assign to the downloaded file
        //public List<KeyValuePair<string, object>> args;      // the report parameters
        public Dictionary<string, object> args;
        //public NameValueCollection args;
        //public dynamic args;

    }

    public class InputRequest
    {
        public string reportName;
    }
    public class JasperListRequest
    {
        public string folder;
    }
}