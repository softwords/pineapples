﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pineapples;

namespace Pineapples.Models
{

	public class CloudfileFilter
	{
		public CloudfileFilter()
		{
			// constructior
			filesOrPhotos = String.Empty;
		}
		private List<string> q;

		public List<string> Q()
		{
			if (q == null)
			{
				q = new List<string>();
		
				if (uploaded == true)
				{
					q.AddPropertiesSearch("uploaded", "POSITIVE");
				}
				else if (uploaded == false)
				{
					q.AddMissingPropertySearch("uploaded");
				}
				
			
				// AddPropertiesSearch will discard nulls for tidiness
				q.AddPropertiesSearch("type", fileType);
				q.AddPropertiesSearch("schoolNo", schoolNo);
				q.AddPropertiesSearch("inspectionId", inspectionId);
				
				switch (status)
				{

					case "ready":
						// this means the survey is completed
						q.AddPropertiesSearch("surveyCompleted", "POSITIVE");
						q.AddNotPropertiesSearch("uploaded","OK");
						break;
					case "inprogress":
						q.AddPropertiesSearch("surveyCompleted", "NEGATIVE");
						q.AddNotPropertiesSearch("uploaded", "OK");
						break;
					case "merged":
						q.AddPropertiesSearch("surveyCompleted", "MERGED");
						q.AddNotPropertiesSearch("uploaded", "OK");
						break;
					case "loaded":
						q.AddPropertiesSearch("uploaded", "OK");
						break;
					case "active":
						q.AddNotPropertiesSearch("uploaded", "OK");
						q.AddNotPropertiesSearch("surveyCompleted", "MERGED");
						break;

				}

				q.AddPropertiesSearch("createUser", createUser);

				if (folder != null)
				{
					q.InFolder(folder);
				}
				q.AddNameValue("mimeType", mimeType);
				q.AddNameValue("name", name);
				q.AddNameValue("id", id);
			}
			return q;
		}

		#region Searches based on custom properties
		/// <summary>
		/// custom property 'type'
		/// holds the type of a survey ; SCHOOL_ACCREDITATION, WASH etc
		/// </summary>
		public string fileType { get; set; }

		/// <summary>
		/// stored in custom properties
		/// </summary>
		public string schoolNo { get; set; }

		/// <summary>
		/// The server sets the inspectionId if when the file is successfully loaded
		/// </summary>
		public int? inspectionId { get;set;}

		public string status { get; set; }
		/// <summary>
		/// in the custom properties, surveyCompleted is POSITIVE ,  NEGATIVE or MERGED
		/// </summary>
		public bool? surveyCompleted { get; set; }

		/// <summary>
		/// this property is set by the server to indicate the file has been uploaded
		/// </summary>
		public bool? uploaded { get; set;}

		/// <summary>
		/// this property is set by the Android app to record the active user
		/// Note that the google defined property lastModifyingUser will always point to the service account
		/// </summary>
		public string createUser { get; set; }
		#endregion

		#region Searches based on fields defined in google drive File metadata
		/// <summary>
		/// folder will always be supplied by the server,
		/// to limit the search to a specific country.
		/// Allow this to be a list, since in early implementations multiple folders have been created.
		/// </summary>
		public IList<string> folder { get; set; }

		/// <summary>
		///  the google drive file unique identifier
		/// </summary>
		public string id { get; set; }

		/// <summary>
		///  the file or folder name
		/// </summary>
		public string name { get; set; }

		/// <summary>
		/// mimeType from the file metadata
		/// </summary>
		public string mimeType { get; set; }


		#endregion

		#region "hints"
		/// <summary>
		/// The client can pass this to indicate whether to retrieve files from the 
		/// base folder or from the photos folder. However, the server must interpret this to set up 
		/// the folder property
		/// </summary>
		public string filesOrPhotos { get; set; }

		public override string ToString()
		{
			return String.Join(" and ", Q());
		}
		#endregion

		private string boolProp(bool? prop)
		{
			if (prop == null)
			{
				return null;
			}

			return (bool)prop ? "POSITIVE" : "NEGATIVE";
		}

	}
}