﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Data;

namespace Pineapples.Models
{
	#region Interface
	public interface IListRow
	{
		object this[int col] { get; }
		object this[string colname] { get; }

		IListObject ListObject { get; }
	}
	/// <summary>
	/// Interface to define a 2-dimensional - row, col - object that can have values
	/// accessed by row index, column index, or row index, column name
	/// Motivation is manipulation of Excel ListObjects, but this interface is abstracted
	/// to allow other ways of constructing this object without Excel/epplus
	/// </summary>
	public interface IListObject : IEnumerable<IListRow>
	{
		/// <summary>
		/// Name of the ListObject
		/// </summary>
		string Name { get; }

		/// <summary>
		/// Number of rows in the ListObject
		/// </summary>
		int Rows { get; }

		/// <summary>
		/// Number of columns in the ListObject
		/// </summary>
		int Columns { get; }
		/// <summary>
		/// Get value using row index and column index
		/// </summary>
		/// <param name="rowindex"></param>
		/// <param name="colindex"></param>
		/// <returns></returns>
		object this[int rowindex, int colindex] { get; }
		/// <summary>
		/// Get value using row index and column name
		/// </summary>
		/// <param name="rowindex"></param>
		/// <param name="colname"></param>
		/// <returns></returns>
		object this[int rowindex, string colname] { get; }
		/// <summary>
		/// Return a row by its index
		/// </summary>
		/// <param name="rowindex"></param>
		/// <returns></returns>
		IListRow Row(int rowindex);

		/// <summary>
		/// array of the column names
		/// </summary>
		/// <returns></returns>
		string[] ColumnNames { get; set; }

		XDocument ToXml();
	}
	#endregion

	#region Implementation of ListObject
	/// <summary>
	/// this class represents an Excel ListObject
	/// for which there is not explcit support in EPPlus
	/// The ListObject class supports the enumeration of ListRows
	/// and both ListObject and ListRow support index and name indexers for columns
	/// </summary>
	/// 
	[JsonConverter(typeof(ListObjectJsonConverter))]
	public class ListObject : IListObject
	{
		private object[,] values;

		/// <summary>
		/// Constructor allows an OfficeXml ExcelTable to be handled as a ListObject
		/// </summary>
		/// <param name="tbl">the excel table representing the list object</param>
		public ListObject(OfficeOpenXml.Table.ExcelTable tbl)
		{
			_listObject(tbl);
		}
		/// <summary>
		/// This contructor assumes we have a set of named columns starting in cel A1
		/// however, these may not be formatted as a Table
		/// </summary>
		/// <param name="wk"></param>
		public ListObject(OfficeOpenXml.ExcelWorksheet wk)
		{
			OfficeOpenXml.ExcelRangeBase lastcol = wk.Cells.Last(c => c.Start.Row == 1);
			OfficeOpenXml.ExcelRangeBase lastrow = wk.Cells.Last(c => c.Start.Column == 1);

			var rng = wk.Cells[1, 1, lastrow.End.Row, lastcol.End.Column];
			OfficeOpenXml.Table.ExcelTable tbl = wk.Tables.Add(rng, "epplusTable");
			// create a ListObject from that table
			_listObject(tbl);
		}

		public ListObject(System.Data.DataTable dt)
		{
			Rows = dt.Rows.Count;
			Columns = dt.Columns.Count;
			FirstRow = 1;
			Name = dt.TableName;
			object[,] v = new object[Rows, Columns];

			for (int i = 0; i < Rows; i++)
			{
				var row = dt.Rows[i];
				for (int j = 0; j < Columns; j++)
				{
					v[i, j] = row[j];
				}
			}
			this.values = v;

			ColumnNames = new string[Columns];
			for (int i = 0; i < Columns; i++)
			{
				ColumnNames[i] = dt.Columns[i].ColumnName;
			}

		}

		/// <summary>
		/// Construct from a 2-dimensional array, (row / column) with a set of column names
		/// Not dependant on Excel or epplus
		/// </summary>
		/// <param name="values"></param>
		/// <param name="columnNames"></param>
		public ListObject(object[,] values, string[] columnNames)
		{
			_ListObject(values, columnNames);
		}
		public void _ListObject(object[,] values, string[] columnNames)
		{
			this.values = values;
			Rows = values.GetLength(0);
			FirstRow = 1;
			Columns = columnNames.Length;
			Name = String.Empty;

			// make an array from the column names
			ColumnNames = new string[Columns];
			for (int i = 0; i < columnNames.Length; i++)
			{
				var nm = columnNames[i].Trim();
				ColumnNames[i] = nm;
			}

		}
		private void _listObject(OfficeOpenXml.Table.ExcelTable tbl)
		{

			ExcelRangeBase rng = tbl.WorkSheet.Cells[tbl.Address.Address]
				.Offset(1, 0, tbl.Address.Rows - 1, tbl.Address.Columns);
			OfficeOpenXml.Table.ExcelTableColumnCollection cols = tbl.Columns;

			values = (object[,])rng.Value;
			Rows = rng.Rows;
			FirstRow = rng.Start.Row;           //the first data row

			// map column names back to their index
			ColumnNames = new string[cols.Count];
			for (int i = 0; i < cols.Count; i++)
			{
				string nm = cols[i].Name.Trim();
				ColumnNames[i] = nm;
			}
			Columns = cols.Count;
			Name = tbl.Name;
		}

		/// <summary>
		/// Name of the ListObject
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Number of rows in the ListObject
		/// </summary>
		public int Rows { get; private set; }

		/// <summary>
		/// Number of columns in the ListObject
		/// </summary>
		public int Columns { get; private set; }

		/// <summary>
		/// Return the row at the specifed position in the list
		/// </summary>
		/// <param name="idx"></param>
		/// <returns></returns>
		public IListRow Row(int idx)
		{
			return new ListRow(this, idx);
		}

		/// <summary>
		/// the row number of the first data row -- add this to the row index to convert to a row number
		/// in the source workbook
		/// </summary>
		public int FirstRow { get; private set; }

		/// <summary>
		///  Array of column names
		/// </summary>
		/// <param name="colname"></param>
		/// <returns></returns>
		public string[] ColumnNames { get; set; }

		public IEnumerator<IListRow> GetEnumerator()
		{
			return new ListObjectEnumerator(this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return new ListObjectEnumerator(this);
		}

		public object this[int rowindex, int colindex]
		{
			get
			{
				return values[rowindex, colindex];
			}
		}
		public object this[int rowindex, string colname]
		{
			get
			{
				int colindex = this.ColumnIndex(colname);
				return values[rowindex, colindex];
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public XDocument ToXml()
		{
			XDocument xd = XDocument.Parse("<ListObject/>");
			xd.Root.SetAttributeValue("FirstRow", FirstRow);
			// use the extension methods to map any column names

			XElement xrow;

			string[] cleannames = this.CleanColumnNames();

			foreach (ListRow row in this)
			{
				xrow = new XElement("row");
				xrow.SetAttributeValue("Index", row.idx);
				for (int i = 0; i < Columns; i++)
				{
					if (row[i] != null)
					{
						object v = row[i];
						if (v is String)
						{
							v = v.ToString().Replace("\v", "");
						}

						xrow.SetAttributeValue(cleannames[i], v);
					}
				}
				xd.Root.Add(xrow);
			}
			return xd;
		}
	}

	[JsonConverter(typeof(ListRowJsonConverter))]
	public class ListRow : IListRow
	{
		internal IListObject lo;
		internal int idx;
		internal ListRow(IListObject lo, int idx)
		{
			this.lo = lo;
			this.idx = idx;
		}

		public object this[int col]
		{
			get
			{
				return lo[idx, col];
			}
		}
		public object this[string colname]
		{
			get
			{
				return lo[idx, colname];
			}
		}

		public IListObject ListObject
		{
			get
			{
				return lo;
			}
		}
	}

	class ListObjectEnumerator : IEnumerator<IListRow>
	{
		private int index;
		private IListObject lo;
		internal ListObjectEnumerator(IListObject lo)
		{
			this.lo = lo;
			index = -1; // ie BOF - MoveNextis called before Current
		}

		public IListRow Current
		{
			get
			{
				return lo.Row(index);
			}
		}

		object IEnumerator.Current
		{
			get
			{
				return lo.Row(index);
			}
		}

		public void Dispose()
		{
			lo = null;
		}

		public bool MoveNext()
		{
			index++;
			if (index < lo.Rows)
				return true;
			return false;
		}

		public void Reset()
		{
			index = 0;
		}
	}
	#endregion
	/// 

	#region json support
	class ListObjectJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(ListObject));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			ListObject lo = value as ListObject;
			List<ListRow> ll = new List<ListRow>();

			foreach (ListRow row in lo)
			{
				ll.Add(row);
			}
			serializer.Serialize(writer, ll);
		}
	}
	class ListRowJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(IListRow));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			IListRow row = value as IListRow;
			Dictionary<string, object> rd = new Dictionary<string, object>();

			foreach (string colname in row.ListObject.ColumnNames)
			{
				if (row[colname] != null)
					rd.Add(colname, row[colname]);
			}
			serializer.Serialize(writer, rd);
		}


	}

	#endregion

	#region Extension methods
	/// <summary>
	/// Extension methods for IListObject - these are implementations that flow directly from the 
	/// interface definition
	/// </summary>
	public static class ListObjectExtensions
	{
		/// <summary>
		/// Use a dictionary to translate columnNames in an IListObject
		/// These are applied to the ListObject in place
		/// </summary>
		/// <param name="lo">the list object</param>
		/// <param name="columnMappings">Dictionary of translations</param>
		/// <returns>array of modified column names</returns>
		public static string[] MapColumns(this IListObject lo, Dictionary<string, string> columnMappings)
		{
			foreach (string colname in columnMappings.Keys)
			{
				int index = lo.ColumnIndex(colname);
				if (index >= 0)
				{
					lo.ColumnNames[index] = columnMappings[colname];
				}
			}
			return lo.ColumnNames;
		}

		/// <summary>
		/// Returns an array of column names "cleaned up" so as to be valid xml node names
		/// The resulting array is NOT applied to the list object permanently: to make permanent use:
		/// lo.ColumnNames = lo.CleanColumnNames();
		/// </summary>
		/// <param name="lo"></param>
		/// <returns></returns>
		public static string[] CleanColumnNames(this IListObject lo)
		{
			string[] cleannames = new string[lo.ColumnNames.Length];
			lo.ColumnNames.CopyTo(cleannames, 0);
			return cleannames.CleanColumnNames();
		}

		/// <summary>
		/// Take a column name (found in an Excel listObject) 
		/// and convert into a valid XML node name
		/// </summary>
		/// <param name="columnname"></param>
		/// <returns></returns>
		public static string CleanColumnName(this string columnname)
		{
			string nm = columnname.Trim().Replace(":", "_")
					.Replace("/", "or")
					.Replace(" ", "_").Replace("?", "").Replace("'", "").Replace("#", "Num").Replace("\v", "");
			// test if begins with numeric
			string f = nm.Substring(0, 1);
			switch (f)
			{
				case "0":
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
				case "6":
				case "7":
				case "8":
				case "9":
					nm = "NM_" + nm;
					break;
			}
			return nm;
		}
		public static string[] CleanColumnNames(this string[] columnnames)
		{
			for (int i = 0; i < columnnames.Length; i++)
			{
				columnnames[i] = columnnames[i].CleanColumnName();
			}
			return columnnames;
		}
		/// <summary>
		/// Return the index of a column from its name
		/// </summary>
		/// <param name="lo"></param>
		/// <param name="columnName"></param>
		/// <returns></returns>
		public static int ColumnIndex(this IListObject lo, string columnName)
		{
			return Array.IndexOf(lo.ColumnNames, columnName);
		}

		/// <summary>
		/// convert to Json string
		/// </summary>
		/// <param name="lo"></param>
		/// <param name="cleanColumnNames">Use 'cleaned up' column names. Note this changes the ColumnNames of the ListObject</param>
		/// <returns>Json string</returns>
		public static string ToJson(this IListObject lo, bool cleanColumnNames = false)
		{
			if (cleanColumnNames)
			{
				lo.ColumnNames = lo.CleanColumnNames();
			}
			return JsonConvert.SerializeObject(lo);
		}

		/// <summary>
		/// Save Json to a file
		/// </summary>
		/// <param name="lo">ListObject</param>
		/// <param name="filename">file name to save</param>
		/// <param name="cleanColumnNames">Use 'cleaned up' column names. Note this changes the ColumnNames of the ListObject</param>
		/// <returns></returns>
		public static string ToJsonFile(this IListObject lo, string filename, bool cleanColumnNames = false)
		{
			if (cleanColumnNames)
			{
				lo.ColumnNames = lo.CleanColumnNames();
			}
			JsonSerializer serializer = new JsonSerializer();

			using (var sw = new System.IO.StreamWriter(filename, false, System.Text.Encoding.ASCII))
			{
				using (JsonWriter writer = new JsonTextWriter(sw))
				{
					serializer.Serialize(writer, lo);
				}
				sw.Close();
				return filename;
			}

		}

		/// <summary>
		/// Convert a IListObject to a ADO.NET DataTable
		/// </summary>
		/// <param name="lo">the list object</param>
		/// <param name="cleanColumnNames">Use 'cleaned up' column names. Note this changes the ColumnNames of the ListObject</param>

		/// <returns></returns>
		public static System.Data.DataTable ToDataTable(this IListObject lo, bool cleanColumnNames = false)
		{
			if (cleanColumnNames)
			{
				lo.ColumnNames = lo.CleanColumnNames();
			}
			var dt = new System.Data.DataTable(lo.Name ?? "table");
			// set up the column definitions
			for (int i = 0; i < lo.Columns; i++)
			{
				dt.Columns.Add(lo.ColumnNames[i], typeof(object));
			}
			foreach (var row in lo)
			{
				var dr = dt.NewRow();
				foreach (string col in lo.ColumnNames)
					dr[col] = row[col];
				dt.Rows.Add(dr);
			}
			return dt;
		}

		/// <summary>
		/// This is a helper for debugging: export a ListObject to a Sql declaration
		/// of its Xml representation and save to a file
		/// </summary>
		/// <param name="lo"></param>
		/// <param name="filepath"></param>
		public static void SaveSql( this IListObject lo, string filepath)
		{
			lo.ToXml().toSql().Save(filepath);
		}

		// 
	}
	#endregion

	#region Other Extensions

	public static class XmlExtensions
	{
		public static string toSql(this XDocument x)
		{
			string s = x.ToString();
			s = s.Replace("'", "''");

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.AppendLine("declare @xml xml = convert( xml, '");
			sb.Append(s);
			sb.AppendLine();
			sb.AppendLine("')");
			sb.AppendLine();
			sb.AppendLine("exec dbo.procname @xml");
			sb.AppendLine();

			return sb.ToString();
		}
	}

	public static class StringExtensions
	{
		public static void Save(this string s, string filepath)
		{
			System.IO.File.WriteAllText(filepath, s);
		}

		public static string Sanitize(this string s)
		{
			// Get array of invalid file name characters
			char[] invalidChars = System.IO.Path.GetInvalidFileNameChars();
			// Replace any invalid character with an underscore
			return string.Join("_", s.Split(invalidChars, StringSplitOptions.RemoveEmptyEntries));
		}
	}
	#endregion
}