﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using Softwords.Web.Models;
using System.ComponentModel.DataAnnotations;
using Softwords.Web.HtmlHelpers; // helperBase has tools to examine property type data

namespace Pineapples.Models
{
	/// <summary>
	/// repreent a columnDef in the ui-grid grid options model
	/// </summary>
	public class ColumnDef
	{
		public ColumnDef() { }

		/// <summary>
		/// create a new ColumnDef from a ProprertyInfo
		/// This will be passed from an EF POCO object
		/// </summary>
		/// <param name="pi"></param>
		public ColumnDef(PropertyInfo pi)
		{
			name = pi.Name;
			field = pi.Name;

			DisplayAttribute da = pi
								 .GetCustomAttributes(typeof(DisplayAttribute), true)
								 .Cast<DisplayAttribute>()
								 .SingleOrDefault();
			if (da != null)
			{
				if (da.Name != null)
				{
					displayName = da.Name;
				}
				if (da.Description != null)
				{
					description = da.Description;
				}
			}

			StringLengthAttribute sla = pi
								 .GetCustomAttributes(typeof(StringLengthAttribute), true)
								 .Cast<StringLengthAttribute>()
								 .SingleOrDefault();
			if (sla != null)
			{
				maxlength = sla.MaximumLength;
				// if its a string, make the width proportional to the number of chars
				width = maxlength * 10;
				if (maxlength > 20)
				{
					width = 200;
				}
				if (maxlength < 5)
				{
					width = 50;
				}
			}
			else
			{
				width = 100;
			}
			// ClientLookup is an attribute to define the lookup list to use in the editable grid
			ClientLookupAttribute cla = pi
											.GetCustomAttributes(typeof(ClientLookupAttribute), true)
											.Cast<ClientLookupAttribute>()?
											.SingleOrDefault();
			if (cla != null)
			{
				lookup = cla.Lookup;
				// width of description will not correspond to width of the foreign key field
				width = 150;
			}

			Type targetType = HelperBase.IsNullableType(pi.PropertyType) ? Nullable.GetUnderlyingType(pi.PropertyType) : pi.PropertyType;
			switch (Type.GetTypeCode(targetType))
			{
				case TypeCode.Byte:
				case TypeCode.SByte:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Single:
					type = "number";
					break;
				case TypeCode.Boolean:
					type = "boolean";
					break;
				case TypeCode.Char:
				case TypeCode.DateTime:
					type = "datetime";
					break;
			}
			DataTypeAttribute dta = pi
							.GetCustomAttributes(typeof(DataTypeAttribute), true)
							?.Cast<DataTypeAttribute>()?
							.SingleOrDefault();
			if (dta != null)
			{
				// we can set the type of the columndef by usiong the semantic 'datatype' of the property
				switch (dta.DataType)
				{
					case DataType.Date:
						type = "date";
						break;
					case DataType.EmailAddress:
						type = "email";
						break;
				}
			}
		}
		public string cellClass;
		public string cellFilter;
		public string cellTemplate;
		public bool cellTooltip;
		public string displayName;
		public bool enableColumnMenu;
		public bool enableColumnMenus;
		public bool enableFiltering;
		public bool enableHiding;
		public bool enableSorting;
		public string field;
		public object filter;
		public bool filterCellFiltered;
		public bool filterHeaderTemplate;
		public object filters;
		public string footerCellClass;
		public string footerCellFilter;
		public string footerCellTemplate;
		public string headerCellClass;
		public string headerCellFilter;
		public string headerCellTemplate;
		public string headerTooltip;
		public int maxWidth;
		public int minWidth;
		public string name;
		public bool sortCellFiltered;
		public bool suppressRemoveSort;
		public string type;
		public bool visible = true;
		public object width;

		// from Edit extension to ColumnDef
		public bool cellEditableCondition;
		public string editDropdownFilter;
		public string editDropdownIdLabel;
		public string editDropdownValueLabel;
		public string editableCellTemplate;
		public string enableCellEdit;
		public bool enableCellEditOnFocus;

		// these are added for our own purposes - to be available to the template
		public int? maxlength;
		public string lookup;
		public string description;
	}
	public class GridOptions
	{
		public GridOptions()
		{
			columnDefs = new List<ColumnDef>();
			enablePagination = false;
			enablePaginationControls = false;
		}
		public List<ColumnDef> columnDefs;
		/// <summary>
		/// Get any ChangeTracking attribute for the property
		/// </summary>
		/// <param name="piDest"></param>
		/// <returns>the change tracking attribute enum if any associated to this property</returns>
		private static ChangeTrackingAttribute getChangeTrackAttribute(System.Reflection.PropertyInfo piDest)
		{
			return (ChangeTrackingAttribute)piDest.GetCustomAttributes(true).FirstOrDefault(a => a is ChangeTrackingAttribute);
		}
		public bool enablePagination;
		public bool enablePaginationControls;


		public static GridOptions fromType(Type typ)
		{
			GridOptions gopts = new GridOptions();

			return gopts;
		}
	}
	/// <summary>
	/// Class repesenting a ViewMode; which is a gridOptions object defined by ui-grid component,
	/// with some custom exension properties. 
	/// Client-side the superGrid component - a wrapper enriching the ui-grid - can interpret, extend and install a 
	/// passed-in ViewMode.
	/// This class can create a default, rich ViewMode by examining the properties of an Entity Framework 
	/// POCO (plain-old-csharp-object) object,
	/// and the attributes of those properties (EF-defined attributes and some custom attributes).
	/// Going further such POCO objects may be constructed from a database table using the POCO Generator tool.
	/// Thus there is a direct and largely automated path from:
	/// -- database table or view, TO
	/// -- POCO object (POCO Generator) TO
	/// -- ViewMode (this class) TO
	/// -- user interface (superGrid.component)
	/// 
	/// The final construction of the ViewMode combines these options:
	/// SERVER: 
	///			may use a hardcoded ViewMode; ie a json file in the ViewModes folder tree
	///			if no hardcoded viewMode, uses this routine to dynamically create the ViewMode
	///				Note that since the dynamic ViewMode is derived from the EF POCO object the dynamic viewMode can be tailored
	///				by carefully setting the attributes of each property.
	///	CLIENT:
	///			viewMode is installed into the ui-grid by superGrid.Component. 
	///				Some further default values may be supplied at this stage.
	/// </summary>
	public class ViewMode
	{
		public GridOptions gridOptions;
		public bool editable = true;
		public bool deletable = true;
		public bool insertable = true;

		public ViewMode(Type typ)
		{
			gridOptions = new GridOptions();
			foreach (System.Reflection.PropertyInfo piDest in typ.GetProperties())
			{
				if (getChangeTrackAttribute(piDest) != null)
				{
					// don;t add for the change tracked fields - client side we add these
					continue;
				}
				if (isRowversion(piDest))
				{
					// don't add the timestamp
					continue;
				}
				{
					gridOptions.columnDefs.Add(new ColumnDef(piDest));
				}
			}
		}
		private ChangeTrackingAttribute getChangeTrackAttribute(System.Reflection.PropertyInfo piDest)
		{
			return (ChangeTrackingAttribute)piDest.GetCustomAttributes(true).FirstOrDefault(a => a is ChangeTrackingAttribute);
		}

		private bool isRowversion(System.Reflection.PropertyInfo piDest)
		{
			return piDest.GetCustomAttributes(true).Any(a => a is TimestampAttribute);
		}

	}

}
