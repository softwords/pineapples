﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using static iText.IO.Codec.TiffWriter;

namespace Pineapples.Models
{
	public class XfdfDocument
	{
		private XmlNamespaceManager nspm;

		public static XNamespace xfdfNamespace = "http://ns.adobe.com/xfdf/";

		public XfdfDocument(XDocument xfdf)
		{
			nspm = new XmlNamespaceManager(new NameTable());
			nspm.AddNamespace("x", "http://ns.adobe.com/xfdf/");

			Document = xfdf;
			FieldsNode = xfdf.Root.XPathSelectElement("x:fields", nspm);
		}

		public XDocument Document { get; private set; }
		public XElement FieldsNode { get; private set; }

		public XElement GetElement(string fieldPath)
		{
			string xp = fieldToXPath(fieldPath);
			return FieldsNode.XPathSelectElement(xp, nspm);
		}
		private string fieldToXPath(string fieldPath)
		{
			// first split the input string to an array, 
			string[] nodes = fieldPath.Split('.');

			for (int i = 0; i < nodes.Length; i++)
			{
				string n = nodes[i];
				nodes[i] = string.Format("x:field[@name=\"{0}\"]", n);
			}
			return string.Join("/", nodes);
		}
		public XElement GetChild(XElement element, string fieldPath)
		{
			string xp = fieldToXPath(fieldPath);
			return element.XPathSelectElement(xp, nspm);
		}

		public string GetValue(string fieldPath)
		{
			
			XElement x = GetElement(fieldPath);
			if (x == null)
				return null;
			XElement xv = x.XPathSelectElement("x:value", nspm);
			if (xv == null)
			{
				return null;
			}
			return xv.Value;
		}
		public string SetValue(string fieldPath, object value)
		{

			XElement x = GetElement(fieldPath);
			return SetValue(x, value);
		}
		public string SetValue(XElement fieldElement, object value)
		{
			if (fieldElement == null)
				return null;
			XElement xv = fieldElement.XPathSelectElement("x:value", nspm);
			if (xv == null)
			{
				if (!(value == null || value is DBNull))
				// don't add the value element if null
				{
					xv = new XElement(xfdfNamespace + "value", value.ToString());
					fieldElement.Add(xv);
				}
			}
			else
			{
				if (!(value == null || value is DBNull))
				{
					xv.Value = value.ToString();
				}
				else
				// remove the value element if null
				{
					xv.Remove();
				}

					
			}
			return value.ToString();
		}

		public System.IO.Stream ToStream()
		{
			string xfdfString = Document.ToString();
			byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(xfdfString);

			// Create a MemoryStream and write the byte array to it
			MemoryStream xfdfStream = new MemoryStream();
			xfdfStream.Write(byteArray, 0, byteArray.Length);
			xfdfStream.Position = 0;
			return xfdfStream;

		}
	}
}