﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using System.Collections.ObjectModel;       //for obvablecollection
using Pineapples.Data;
using System.Security.Claims;

namespace Pineapples.Models
{
	/// <summary>
	/// wrapper around the Pdf Form extracted from a survey Pdf
	/// </summary>
	public class PdfSurvey : PdfForm
	{

		public PdfSurvey(string PdfFileName) : base(PdfFileName)
		{
			xfdf = new XfdfDocument(Xfdf());
			surveySections_ = new List<string>();
			foreach (var fld in Fields.Where(fld => String.IsNullOrEmpty(fld.Value.ParentPath) &&
				!fld.Key.StartsWith("bookmark", StringComparison.CurrentCultureIgnoreCase)))
			{
				switch (fld.Key)
				{
					case "Resource":
					case "Room":
						foreach (var child in Fields.Where(ff => ff.Value.ParentPath == fld.Key))
						{
							surveySections_.Add(child.Value.Path);
						}
						break;
					default:
						surveySections_.Add(fld.Value.Path);          // path and Name are the same when Parent = -1
						break;
				}
			}
		}
		public string SchoolName
		{
			get
			{
				return GetField("Survey.SchoolName")?.Value;
			}
		}
		public string SchoolNo
		{
			get
			{
				return GetField("Survey.SchoolNo")?.Value;
			}
		}
		public int? SurveyYear
		{
			get
			{
				return Convert.ToInt32(GetField("Survey.SurveyYear")?.Value);
			}
		}

		public bool IsSurvey
		{
			get
			{
				return (GetField("Survey") != null);
			}
		}

		private XfdfDocument xfdf;

		public void Process(IDSPdfSurvey ds, string fileId, ClaimsIdentity user)
		{
			XElement sectionxml = xfdf.GetElement("Survey");
			int? surveyID = null;
			surveyID = ds.xfdfSurvey(sectionxml);

			// step throu ght sections of the survey, get the xml and process

			foreach (string section in SurveySections())
			{
				try
				{
					sectionxml = xfdf.GetElement(section);
					if (sectionxml != null)
					{
						switch (section)
						{
							case "Survey":
								// already done at the start
								break;
							case "Funding":
								// simple income and expenditure statement
								// Use : KEMIS PRI
								//

								ds.xfdfFunding(surveyID, sectionxml);
								break;
							case "PC":
								ds.xfdfParentCommittee(surveyID, sectionxml);
								break;
							case "DT":      // distance transport
								ds.xfdfDistanceTransport(surveyID, sectionxml);
								break;
							case "Enrol":
								ds.xfdfEnrolment(surveyID, sectionxml);
								break;
							case "DisabNA":
								ds.xfdfDisabilityNA(surveyID, sectionxml);
								break;
							case "Site":
								ds.xfdfSite(surveyID, sectionxml);
								break;
							case "Housing":
								ds.xfdfHousing(surveyID, sectionxml);
								break;
							case "Preschool":
								ds.xfdfPreschool(surveyID, sectionxml);
								break;
							case "Supplies":
								ds.xfdfSupplies(surveyID, sectionxml);
								break;

							case "Class":
							case "ClassJ":
								// do in second pass after teachers
								break;
							case "Toilets":
								ds.xfdfToilets(surveyID, sectionxml);
								break;
							case "Water":
								// water rating question only
								ds.xfdfWater(surveyID, sectionxml);
								break;
							case "Room.Class":
								// in the first instance we'll try to do rooms in one go
								ds.xfdfRooms(surveyID, sectionxml);
								break;
							case "TL":
							case "T":
								ds.xfdfTeachers(surveyID, sectionxml);

								break;
							default:
								if (section.StartsWith("Resource"))
								{
									ds.xfdfResourceList(surveyID, sectionxml);
									break;
								}

								// lookup the code in the metaPupilTables
								// if its there, call grid
								if (ds.isTableDef(section))
								{
									// use the grid updater
									ds.xfdfGrid(surveyID, sectionxml);
									break;
								}
								Console.WriteLine("{0} not handled", section);
								break;
						}
					}
				}
				catch(Exception ex)
				{
					ds.xfdfError((int)surveyID, section, ex);
				}
			}

			// handle depencendies with a second pass
			Console.WriteLine("Pass 2");
			foreach (string section in SurveySections())
			{
				Console.WriteLine(section);
				sectionxml = xfdf.GetElement(section);
				if (sectionxml != null)
				{
					try
					{
						switch (section)
						{
							case "Class":
							case "ClassJ":
								ds.xfdfClass(surveyID, sectionxml);
								break;

							default:
								break;
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine("{0}: ERROR : {1}", section, ex.Message);
						ds.xfdfError((int)surveyID, section, ex);
					}
				}
			}
			// log the document against the school
			string extension = System.IO.Path.GetExtension(FileName).Substring(1); // filename is set in constructor
			string oldFileId = ds.xfdfLogSurveyDocument(surveyID, fileId, extension, xfdf.Document, user);
			if (!String.IsNullOrEmpty(oldFileId) && oldFileId != fileId)		//oldFileId = new fielId if we are reprocessing
			{
				Providers.FileDB.Remove(oldFileId);
			}
		}
		/// <summary>
		/// buld up a list of sections in the survey - these are the direct child nodes
		/// of the <fields></fields> node
		/// as well as direct children of <fields><field name="Resource"></field></fields>
		/// </summary>
		IList<string> surveySections_;
		private IList<string> SurveySections()
		{
			return surveySections_;
		}

		private XElement GetSection(string sectionname)
		{
			return xfdf.GetElement(sectionname);
		}



	}
}
