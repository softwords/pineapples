﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Softwords.Web;
using OfficeOpenXml;
using Microsoft.AspNet.SignalR;
using Pineapples.Hubs;


namespace Pineapples.Controllers
{
	[RoutePrefix("api/censusworkbook")]
	[System.Web.Http.Authorize]
	public class CensusWorkbookController : PineapplesApiController
	{
		public CensusWorkbookController(DataLayer.IDSFactory factory) : base(factory) { }


		/// <summary>
		/// uploads and stores the file
		/// </summary>
		/// <returns>a summary of the data in the file, together with the file ID</returns>
		[HttpPost]
		[Route(@"upload")]
		[PineapplesPermission(PermissionTopicEnum.Survey, PermissionAccess.Ops)]
		public async Task<object> Upload()
		{
			if (!Request.Content.IsMimeMultipartContent())
				throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

			var provider = new MultipartFormDataStreamProvider(AppDataPath);

			var files = await Request.Content.ReadAsMultipartAsync(provider);
			CensusInfo censusInfo = new CensusInfo();

			foreach (MultipartFileData fd in files.FileData)
			{
				string filename = fd.Headers.ContentDisposition.FileName;
				filename = filename.Replace("\"", "");          // tends to com back with quotes around
				string ext = System.IO.Path.GetExtension(filename);

				Guid g;
				System.IO.Stream fstrm;

				if (ext.ToLower() == ".zip")
				{
					string entryname = string.Empty;
					fstrm = Unzip(fd.LocalFileName, out entryname);
					filename = entryname;
					ext = System.IO.Path.GetExtension(filename);
				}
				else
				{
					fstrm = new System.IO.FileStream(fd.LocalFileName, System.IO.FileMode.Open);
				}
				// extension may have changed if a zip file was passed
				switch (ext.ToLower())
				{
					case ".xlsm":
					case ".xml":
						break;
					default:
						var message = "The Zip file did not contain an Excel workbook or Xml census file as its only entry.";
						return Request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
				}
				ICensusHandler handler;

				using (fstrm)
				{
					g = Providers.FileDB.Store(fstrm, ext);
					// add a document record for this file
					handler = CensusHandlerFactory.GetHandler(ext);
					handler.Context = Context;
					// create an Excel workbook object from the stream
					censusInfo = await handler.LoadFile(fstrm, g, Factory);
				};
				// we can get rid of the local file now
				System.IO.File.Delete(fd.LocalFileName);

				IDataResult dataresult = null;
				// validate students
				if (censusInfo.SheetSetupInfos.IsPopulated("Students") == 1) {
					dataresult = await Ds.CensusWorkbookValidateStudentsAsync(
						prepXml(handler.Students(), censusInfo));
					// two tables are returned - Errors and Warnings
					System.Data.DataTableCollection result = dataresult.ResultSet as System.Data.DataTableCollection;
					censusInfo.studentValidations = result[0];
					censusInfo.studentWarnings = result[1];
				}

				if (censusInfo.SheetSetupInfos.IsPopulated("SchoolStaff") == 1)
				{
					// validate staff before trying to load - this is where most problems will be
					dataresult = await Ds.CensusWorkbookValidateStaffAsync(
					prepXml(handler.Staff(), censusInfo));
					censusInfo.staffValidations = dataresult.ResultSet as System.Data.DataTable;
				}
				// wash


				//schools.Root.Add(collectSchools(lo, wk.Name));
				if (censusInfo.SheetSetupInfos.IsPopulated("WASH") == 1)
				{
					dataresult = await Ds.CensusWorkbookValidateWashAsync(
					prepXml(handler.Wash(), censusInfo));
					censusInfo.washValidations = dataresult.ResultSet as System.Data.DataTable;
				}

				dataresult = await Ds.CensusWorkbookValidateSchoolsAsync(
					prepXml(handler.AllSchools(), censusInfo));
				censusInfo.validations =  dataresult.ResultSet as System.Data.DataTable;
				
			}
			return censusInfo;
			
		}

		/// <summary>
		/// Processes the nominated file
		/// </summary>
		/// <param name="fileId">guid to the Excel document in the FileDB</param>
		/// <returns></returns>
		[HttpGet]
		[Route(@"process/{fileId}")]
		public async Task<object> Process(string fileId)
		{
			Guid g = new Guid(fileId);
			IDataResult results;

			// first get the file based on its Id
			string ext = Providers.FileDB.GetExtension(fileId);
			CensusInfo info = new CensusInfo();

			ICensusHandler handler = CensusHandlerFactory.GetHandler(ext);
			handler.Context = Context;
			using (var fstrm = Providers.FileDB.Get(fileId))
			{
				info = await handler.LoadFile(fstrm, g, Factory);
			}

			// prepare to signalr
			WorkbookProcessState state = new WorkbookProcessState();

			// process schools.....
			// send to the database
			state.schools = "loading...";
			Hub.Clients.Group(fileId).progress(fileId, state);
			if (info.SheetSetupInfos.IsHidden("Schools") == 0) {
				await Ds.CensusWorkbookSchoolsAsync(prepXml(handler.Schools(), info), g, User.Identity.Name);
			}
			// tell the client
			state.schools = "Completed";
			Hub.Clients.Group(fileId).progress(fileId, state);

			// staff...
			state.staff = "loading...";
			Hub.Clients.Group(fileId).progress(fileId, state);
			if (info.SheetSetupInfos.IsHidden("SchoolStaff") == 0)
			{
				await Ds.CensusWorkbookStaffAsync(prepXml(handler.Staff(), info), g, User.Identity.Name);
			}
			state.staff = "Completed";
			Hub.Clients.Group(fileId).progress(fileId, state);

			// students...
			state.students = "loading...";
			Hub.Clients.Group(fileId).progress(fileId, state);

			if (info.SheetSetupInfos.IsHidden("Students") == 0)
			{
				await Ds.CensusWorkbookStudentsAsync(prepXml(handler.Students(), info), g, User.Identity.Name);
			}
			state.students = "Completed";
			Hub.Clients.Group(fileId).progress(fileId, state);

			// wash...
			state.wash = "loading...";
			Hub.Clients.Group(fileId).progress(fileId, state);
			if (info.SheetSetupInfos.IsHidden("WASH") == 0)
			{
				await Ds.CensusWorkbookWashAsync(prepXml(handler.Wash(), info), g, User.Identity.Name); 
			}
			
			state.wash = "Completed";
			Hub.Clients.Group(fileId).progress(fileId, state);

			// get the summary data
			results = await Ds.CensusWorkbookOutputAsync(g);
			return results;         // we are interested in the student results
		}

		/// <summary>
		/// This is a fix-up routine to apply the xml rows from Schools and Wash pages
		/// to the SchoolSurvey table. This is an historical fix since this data was not alwasy saved
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		[HttpGet]
		[Route(@"savexml/{fileId}")]
		public async Task<object> SaveXml(string fileId)
		{
			Guid g = new Guid(fileId);
			System.Xml.Linq.XDocument xdSchool = null;
			System.Xml.Linq.XDocument xdWash = null;
			IDataResult results;
			string ext = Providers.FileDB.GetExtension(fileId);
			CensusInfo info = new CensusInfo();

			ICensusHandler handler = CensusHandlerFactory.GetHandler(ext);
			using (var fstrm = Providers.FileDB.GetStreamAsync(fileId))
			{
				info = await handler.LoadFile(fstrm, g, Factory);
			}

			xdSchool = handler.Schools();
			// wash...
			xdWash = handler.Wash();

			// get the summary data
			results = await Ds.SaveXmlToSurvey(xdSchool, xdWash, g);
			return results;         // we are interested in the student results
		}

		/// <summary>
		/// This is a utility routine to construct an xml representation of a survey from its Excel workbook
		/// This is for testing of the xml upload
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		[HttpGet]
		[Route(@"exportexcel/{fileId}")]
		[AllowAnonymous]
		public async Task<HttpResponseMessage> ExportExcelToXml(string fileId)
		{
			Guid g = new Guid(fileId);
			string ext = Providers.FileDB.GetExtension(fileId);
			CensusInfo info = new CensusInfo();

			ICensusHandler handler = CensusHandlerFactory.GetHandler(ext);
			using (var fstrm = Providers.FileDB.GetStreamAsync(fileId))
			{
				info = await handler.LoadFile(fstrm, g, Factory);
			}

			XDocument census = XDocument.Parse("<census><schools/><staff/><students/><wash/></census>");

			census.Root.SetAttributeValue("district", info.District);
			census.Root.SetAttributeValue("schoolYear", info.SchoolYear);
			census.Root.Element("schools").Add(handler.Schools().Root);
			census.Root.Element("staff").Add(handler.Staff().Root);
			census.Root.Element("students").Add(handler.Students().Root);
			census.Root.Element("wash").Add(handler.Wash().Root);

			var filename = $"Census_{info.SchoolYear}_{info.District}.xml";

			var response = Request.CreateResponse();
			response.Content = new StringContent(census.ToString());
			response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
			response.Content.Headers.ContentDisposition.FileName = filename;
			response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/xml");

			return response;
			
		}


		[HttpGet]
		[Route(@"remove/{fileId}")]
		public void Remove(string fileId)
		{
			Providers.FileDB.Remove(fileId);
		}

		/// <summary>
		/// Return a Table from an Excel workbook in the filedb
		/// </summary>
		/// <param name="fileId">id of the file</param>
		/// <param name="sheetname">name of the sheet holding the table</param>
		/// <returns></returns>
		[HttpGet]
		[Route(@"getdata/{fileId}/{sheetname}")]
		public object getData(string fileId, string sheetname)
		{
			ExcelPackage p;

			// first get the file based on its Id
			string path = Providers.FileDB.GetFilePath(fileId);
			using (System.IO.FileStream fstrm = new System.IO.FileStream(path, System.IO.FileMode.Open))
			{
				// create an Excel workbook object from the stream
				p = new ExcelPackage(fstrm);
			};
			ExcelWorksheet wk = p.Workbook.Worksheets[sheetname];
			IListObject lo = new ListObject(wk.Tables[0]);
			lo.ColumnNames = lo.CleanColumnNames();
			var dt = lo.ToDataTable(true); // sample code
			return dt;
			//return lo;
		}

		readonly Lazy<IHubContext<IWorkbookProcessCallbacks>> hub = new Lazy<IHubContext<IWorkbookProcessCallbacks>>(
		() => GlobalHost.ConnectionManager.GetHubContext<IWorkbookProcessCallbacks>(WorkbookProcessState.WorkbookProcessHubName)
	);

		protected IHubContext<IWorkbookProcessCallbacks> Hub
		{
			get { return hub.Value; }
		}

		private IDSSurveyEntry Ds
		{
			get { return Factory.SurveyEntry(); }
		}

		#region helpers

		/// <summary>
		/// add the district and survey year as attributes to the root node
		/// a convenient way to get these into the stored proc
		/// </summary>
		/// <param name="xd"></param>
		private XDocument prepXml(XDocument xd, CensusInfo info)
		{
			xd.Root.SetAttributeValue("state", info.District);
			xd.Root.SetAttributeValue("schoolYear", info.SchoolYear);
			return xd;
		}

		private System.IO.Stream Unzip(string zipfile, out string filename)
		{
			filename = null;
			using (System.IO.FileStream fstrm = new System.IO.FileStream(zipfile, System.IO.FileMode.Open))
			{
				using (var reader = SharpCompress.Readers.ReaderFactory.Open(fstrm))
				{
					if (reader.MoveToNextEntry()) // not empty
					{
						var entry = reader.Entry;
						filename = entry.Key.Replace("\"", "");          // tends to com back with quotes around
						string ext = System.IO.Path.GetExtension(filename).Replace(".", "");
						var entryStream = reader.OpenEntryStream();
						var mstrm = new System.IO.MemoryStream();		// return a stream we can use once fstrm is disposed
						entryStream.CopyTo(mstrm);
						mstrm.Position = 0;
						return mstrm;
					}
					else
					{
						throw new Exception("Zip file contained no etry");
					}
				}
			};
		}


		#endregion


		#region Construct Workbook

		[HttpGet]
		[Route(@"create")]
		[Route(@"create/{year:int}")]
		[PineapplesPermission(PermissionTopicEnum.Survey, PermissionAccess.Ops)]
		public async Task<HttpResponseMessage> CreateCensusWorkbook(int year, string schoolNo = null, string district = null)
		{
			var lookups = Factory.Lookups().CensusWorkbookLookups();
			var censusData = (await Ds.CensusRollover(year, schoolNo, district)).ResultSet as Dictionary<string, object>;
			var svy = new Pineapples.ExcelOutput.CensusWorkbook();
			ExcelPackage p = svy.Create(year, Context, schoolNo, district, censusData, lookups);

			string workbookname = svy.WorkbookName(p, year, schoolNo, district, lookups);
			return SendExcelPackage(p, workbookname);
		}
		#endregion
	}
}
