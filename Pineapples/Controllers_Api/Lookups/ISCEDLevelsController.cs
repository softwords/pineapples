﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Security.Claims;
using Pineapples.Data.Models;
using System.Data.Entity;
using Pineapples.Models;

namespace Pineapples.Controllers
{

	[RoutePrefix("api/iscedlevels")]
	public class ISCEDLevelsController : TableMaintenanceController<ISCEDLevel, string>
	{
		public ISCEDLevelsController(DataLayer.IDSFactory factory) : base("LookupAdmins", factory) { }
	}
}
