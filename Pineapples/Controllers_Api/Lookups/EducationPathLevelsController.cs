﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Security.Claims;
using Pineapples.Data.Models;
using System.Data.Entity;
using Pineapples.Models;

namespace Pineapples.Controllers
{

	[RoutePrefix("api/educationpathlevels")]
	public class EducationPathLevelsController : TableMaintenanceController<EducationPathLevel, int>
	{
		public EducationPathLevelsController(DataLayer.IDSFactory factory) : base("LookupAdmins", factory) { }
	}
}
