﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Security.Claims;
using Pineapples.Data.Models;
using System.Data.Entity;


namespace Pineapples.Controllers
{
    [RoutePrefix("api/educationlevelsalt2")]
    public class EducationLevelsAlt2Controller : TableMaintenanceController<EducationLevelAlt2, string>
    {
        public EducationLevelsAlt2Controller(DataLayer.IDSFactory factory) : base(factory) { }

    }
}
