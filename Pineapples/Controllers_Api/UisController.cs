﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Softwords.Web;
using OfficeOpenXml;
using Microsoft.AspNet.SignalR;
using Pineapples.Hubs;

namespace Pineapples.Controllers
{
	[RoutePrefix("api/uis")]
	public class UisController : PineapplesApiController
	{
		public UisController(DataLayer.IDSFactory factory) : base(factory) { }

		[HttpGet]
		[Route("{year:int}")]

		public HttpResponseMessage UisForYear(int year)
		{
			const bool audit = true;
			var uisDS = Ds.AllSheets(year).ResultSet as Dictionary<string, object>;

			var svy = new Pineapples.ExcelOutput.UisWorkbook();
			ExcelPackage p = svy.Create(year, uisDS);

			if (audit)
			{
				uisDS = Ds.TeacherAudit(year).ResultSet as Dictionary<string, object>;
				svy.TeacherAudit(p, year, uisDS);

				var dt = Ds.EnrolAudit(year).ResultSet as System.Data.DataTable;
				svy.EnrolAudit(p, year, dt);

				dt = Ds.SchoolAudit(year).ResultSet as System.Data.DataTable;
				svy.SchoolAudit(p, year, dt);

			}

			p.Workbook.Worksheets.First().Select(); // always reposition n the first page
			p.Workbook.Worksheets[1].View.ActiveCell = "A1";
			return SendExcelPackage(p, $"UIS Survey {year}.xlsx");
		}
		private IDSUisSurvey Ds
		{
			get { return Factory.UisSurvey(); }
		}
	}
}
