﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pineapples.Data.DataLayer;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using Pineapples.ExcelOutput;

namespace Pineapples.Controllers
{
	public interface ICensusHandler
	{
		Task<CensusInfo> LoadFile(System.IO.Stream fstrm, Guid fileId, IDSFactory Factory);
		XDocument Schools();
		XDocument Staff();

		/// <summary>
		/// XDocument list of students. Corresponds to the format derived from ListObject
		/// </summary>
		/// <returns></returns>
		XDocument Students();

		XDocument Wash();

		XDocument AllSchools();

		string Context { get; set; }
		System.Data.DataTable GetDataTable(string tableName);


	}

	public class CensusInfo
	{ 
		public int NumSchools{get;set;}
		public int NumStaff { get; set; }
		public int NumStudents { get; set; }
		public int NumWash { get; set; }
		public string District { get; set; }
		public string SchoolYear { get; set; }
		public Guid id { get; set; }

		public System.Data.DataTable studentValidations { get; set; }
		public System.Data.DataTable studentWarnings { get; set; }
		public System.Data.DataTable staffValidations { get; set; }
		public System.Data.DataTable washValidations { get; set; }
		public System.Data.DataTable validations { get; set; }

		public SheetSetupInfos SheetSetupInfos { get; set; }



	}
	public static class CensusHandlerFactory
	{
		public static ICensusHandler GetHandler(string extension)
		{
			switch(extension.ToLower())
			{
				case ".xml":
					return new XmlCensusHandler();
				case ".xlsx":
				case ".xlsm":
					return new ExcelCensusHandler();
			}
			throw new Exception($"Files of extension {extension} not supported");
		}
	}

}