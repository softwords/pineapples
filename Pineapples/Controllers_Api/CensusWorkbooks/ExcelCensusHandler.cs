﻿using Pineapples.Data.DataLayer;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using OfficeOpenXml;
using Pineapples.Models;
using Pineapples.Data;


namespace Pineapples.Controllers
{
	public class ExcelCensusHandler : ICensusHandler
	{
		private ExcelPackage p;
		ListObject loSchools;
		ListObject loStaff;
		ListObject loStudents;
		ListObject loWash;

		ExcelOutput.SheetSetupInfos SheetSetupInfos;

		private IDSSurveyEntry Ds;
		public async Task<CensusInfo> LoadFile(Stream fstrm, Guid fileId,IDSFactory Factory)
		{
			
			//throw new NotImplementedException();
			p = new ExcelPackage(fstrm);
			Ds = Factory.SurveyEntry();

			SheetSetupInfos = new ExcelOutput.CensusWorkbook().GetSheetSetup(p, Context);
			CensusInfo info =  await ParseWorkbook();
			info.id = fileId;
			return info;
		}

		private async Task<CensusInfo> ParseWorkbook()
		{


			// get the listobjects
			loSchools = await ListObjectfromSheet("Schools");
			loStaff = await ListObjectfromSheet("SchoolStaff");
			loStudents = await ListObjectfromSheet("Students");
			loWash = await ListObjectfromSheet("WASH");

			string statename_ = (string)p.Workbook.Names["nm_State"].Value;
			string schoolyear_ = (string)p.Workbook.Names["nm_SchoolYear"].Value;
			string schoolname_ = (string)p.Workbook.Names["CurrentSchool"].Value;

			return new CensusInfo
			{
				District = statename_,
				SchoolYear = schoolyear_,
				NumSchools = loSchools.Count(),
				NumStaff = loStaff.Count(),
				NumStudents = loStudents.Count(),
				NumWash = loWash.Count(),
				SheetSetupInfos = SheetSetupInfos
			};
		}


		public XDocument Schools()
		{
			return loSchools.ToXml();

		}

		public XDocument Staff()
		{
			return loStaff.ToXml();
		}

		public XDocument Students()
		{
			return loStudents.ToXml();
		}

		public XDocument Wash()
		{
			return loWash.ToXml();
		}

		public XDocument AllSchools()
		{
			XDocument schools = XDocument.Parse("<Schools/>");
			if (SheetSetupInfos.IsPopulated("Schools") == 1)
			{
				schools.Root.Add(collectSchools(loSchools, "Schools"));

			}
			if (SheetSetupInfos.IsPopulated("SchoolStaff") == 1)
			{
				schools.Root.Add(collectSchools(loStaff, "SchoolStaff"));
			}
			if (SheetSetupInfos.IsPopulated("Students") == 1)
			{
				schools.Root.Add(collectSchools(loStudents, "Students"));
			}
			if (SheetSetupInfos.IsPopulated("WASH") == 1)
			{
				schools.Root.Add(collectSchools(loWash, "Wash"));
			}

			
			return schools;
		}

		/// <summary>
		/// The system context, as passed up from web.config, available in all API controllers
		/// </summary>
		public string Context { get; set; }

		public System.Data.DataTable GetDataTable(string tablename)
		{

			switch (tablename.ToLower())
			{
				case "schools":
					return loSchools.ToDataTable(true);
				case "staff":
				case "schoolstaff":
					return loStaff.ToDataTable(true);
				case "students":
					return loStudents.ToDataTable(true);
				case "wash":
					return loWash.ToDataTable(true);
				default:
					return null;
			}
		}
		private XElement collectSchools(ListObject lo, string sheetName)
		{
			XElement xrow;
			XElement xsheet;
			xsheet = new XElement("Sheet");
			xsheet.SetAttributeValue("name", sheetName);
			foreach (ListRow r in lo)
			{

				xrow = new XElement("School_No", r["School No"]);
				xrow.SetAttributeValue("Index", r.idx + lo.FirstRow);

				// it turns out that to get the sheet name by backtracking up the xml hierarchy
				// is monumentally slow
				// so we add it on each and every node :(
				xrow.SetAttributeValue("Sheet", sheetName);
				xrow.SetAttributeValue("Name", r["School Name"]);
				xsheet.Add(xrow);
			}
			return xsheet;
		}

		private async Task<ListObject> ListObjectfromSheet(string sheetName)
		{
			var wk = p.Workbook.Worksheets[sheetName];
			var tbl = wk.Tables[0];
			var lo = new ListObject(tbl);
			var mappings = await Ds.CensusWorkbookColumnMapping(sheetName);
			lo.MapColumns(mappings);
			return lo;
			//results.Add("schools", lo.Count());
		}


	}
}