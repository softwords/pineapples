﻿using Pineapples.Data.DataLayer;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using OfficeOpenXml;
using Pineapples.Models;
using System.Data;

namespace Pineapples.Controllers
{
	public class XmlCensusHandler : ICensusHandler
	{
		XDocument census;       // the entire census represented in Xml

		/// <summary>
		/// <census @district="..." @schoolYear="">
		///		<schools><ListObject>....</ListObject></schools>
		///		<staff><ListObject>....</ListObject></staff>
		///		<students><ListObject>....</ListObject></students>
		///		<wash><ListObject>...</ListObject></wash>
		/// </census>
		/// </summary>
		/// <param name="fstrm"></param>
		/// <param name="fileId"></param>
		/// <param name="Factory"></param>
		/// <returns></returns>
		/// <exception cref="NotImplementedException"></exception>
		public async Task<CensusInfo> LoadFile(Stream fstrm, Guid fileId, IDSFactory Factory)
		{
			fstrm.Position = 0;
			census = XDocument.Load(fstrm);
			CensusInfo info = new CensusInfo();
			info.NumSchools = Schools().Root.Elements("row").Count();
			info.NumStaff = Staff().Root.Elements("row").Count();
			info.NumStudents = Students().Root.Elements("row").Count();
			info.NumWash = Wash().Root.Elements("row").Count();
			info.District = (string)census.Root.Attribute("district");
			info.SchoolYear = (string)census.Root.Attribute("schoolYear");
			info.id = fileId;

			return info;
		}

		public XDocument Schools()
		{
			
			return GetListObject("schools");
		}

		public XDocument Staff()
		{
			return GetListObject("staff");
		}
		public XDocument Students()
		{
			return GetListObject("students");
		}
		public XDocument Wash()
		{
			return GetListObject("wash");
		}
		private XDocument GetListObject(string parent)
		{
			return new XDocument(census.Root.Element(parent).Element("ListObject"));
		}

		/// <summary>
		/// The system context, as passed up from web.config, available in all API controllers
		/// </summary>
		public string Context { get; set; }

		public XDocument AllSchools()
		{
			XDocument schools = XDocument.Parse("<Schools/>");
			schools.Root.Add(collectSchools(Schools().Root, "Schools"));
			schools.Root.Add(collectSchools(Staff().Root, "SchoolStaff"));
			schools.Root.Add(collectSchools(Students().Root, "Students"));
			schools.Root.Add(collectSchools(Wash().Root, "Wash"));
			return schools;
		}
		private XElement collectSchools(XElement lo, string sheetName)
		{
			XElement xrow;
			XElement xsheet;
			xsheet = new XElement("Sheet");
			xsheet.SetAttributeValue("name", sheetName);
			foreach (XElement r in lo.Elements())
			{

				xrow = new XElement("School_No", (string)r.Attribute("School_No"));
				xrow.SetAttributeValue("Index", (string)r.Attribute("idx"));

				// it turns out that to get the sheet name by backtracking up the xml hierarchy
				// is monumentally slow
				// so we add it on each and every node :(
				xrow.SetAttributeValue("Sheet", sheetName);
				xrow.SetAttributeValue("Name", (string)r.Attribute("School_Name"));
				xsheet.Add(xrow);
			}
			return xsheet;
		}
		public DataTable GetDataTable(string tablename)
		{
			return ListObjectToDataTable(GetListObject(tablename));
		}

		private DataTable ListObjectToDataTable(XDocument listobject)
		{
			throw new NotImplementedException();
		}
	}
}