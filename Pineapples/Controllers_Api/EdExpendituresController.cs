﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Pineapples.Data.Models;
using Softwords.Web;
using Softwords.Web.Models;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/edexpenditures")]
	[Authorize]
	public class EdExpendituresController : TableMaintenanceController<EdExpenditure, int>
    {
        public EdExpendituresController(DataLayer.IDSFactory factory) : base("LookupAdmins", factory)
		{
			Tag = "edexpenditures";
		}
		
	}
}
