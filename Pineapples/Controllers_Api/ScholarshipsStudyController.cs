﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using System.Threading.Tasks;

using System.Collections.Generic;
using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Softwords.Web;

using Softwords.DataTools;
using Softwords.Web.Models;
using System.Data.Entity;
using System.Reflection;
using Pineapples.Data.Models;


namespace Pineapples.Controllers
{
	[RoutePrefix("api/scholarshipsstudy")]
	[Authorize]
	public class ScholarshipStudyController : TableMaintenanceController<ScholarshipStudy, int?> //PineapplesApiController
	{
		public ScholarshipStudyController(DataLayer.IDSFactory factory)
			: base((int)PermissionTopicEnum.Scholarship, factory) {
			Tag = "scholarshipstudy";
		}

		[HttpGet]
		[Route("~/api/scholarships/{scholarshipID:int}/scholarshipsstudy/new")]
		public HttpResponseMessage newStudy(int scholarshipID)
		{
			ScholarshipStudy study = new ScholarshipStudy()
			{
				schoID = scholarshipID
			};
			IDataResult result = Factory.Scholarship().StudyDefaults(scholarshipID);
			HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result.ResultSet);
			response.Headers.Add("X-Tag", Tag);
			return response;
		}

		[HttpPost]
		[Route("~/api/scholarships/{scholarshipID:int}/scholarshipsstudy")]
		public async Task<HttpResponseMessage> fromScholarship(DynamicBinder<ScholarshipStudy> binder)
		{
			return await Create(binder);
		}


	}
}
