﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Pineapples.Data.Models;
using Softwords.Web;
using Softwords.Web.Models;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/findata")]
	[Authorize]
	public class FinancialDataController : TableMaintenanceController<GovtExpenditure, int>
    {
        public FinancialDataController(DataLayer.IDSFactory factory) : base((int)PermissionTopicEnum.Finance, factory)
		{
			Tag = "findata";
		}

		[HttpGet]
		// [Route(@"{ID}")] route inheritance
		[TablePermission(Softwords.Web.TableOperation.Read)]
		public async override Task<HttpResponseMessage> Read(int ID)
		{
			var item = await Factory.GovtExpenditure().Read(ID);

			HttpResponseMessage response = Request.CreateResponse<IDataResult>(HttpStatusCode.OK, item);
			response.Headers.Add("X-Tag", Tag);
			return response;
		}

		[HttpPost]
		[Route("collection/filter")]
		public object filterPaged(GovtExpenditureFilter fltr)
		{
			return Factory.GovtExpenditure().Filter(fltr);
		}


		[HttpGet]
		[Route(@"", Order = -1)]
		public object FilterGet([FromUri] GovtExpenditureFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new GovtExpenditureFilter();
			}
			return Factory.GovtExpenditure().Filter(fltr);
		}
	}
}
