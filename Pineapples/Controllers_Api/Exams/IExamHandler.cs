﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pineapples.Data.DataLayer;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pineapples.Controllers
{
	public interface IExamHandler
	{
		Task<System.Dynamic.ExpandoObject> LoadFile(System.IO.FileStream fstrm, Guid fileId, MultipartFileData fd, IDSFactory Factory);
		Task<Softwords.DataTools.IDataResult> Process(string fileId, IDSFactory Factory, string username);
	}


	public static class ExamHandlerFactory
	{

		/// <summary>
		/// Retrieves an appropriate exam handler based on the file extension and format.
		/// </summary>
		/// <param name="extension">The file extension to determine the handler (e.g., ".xml" (older aggregate format exported by Phil Geeves' 4D, ".xlsx" (raw excel data), etc.).</param>
		/// <param name="format">The format of the exam, with a default value of "soe".</param>
		/// <returns>An implementation of <see cref="IExamHandler"/> that can handle the specified file extension and format.</returns>
		/// <exception cref="Exception">Thrown when the extension is not an XML or Excel file extension or empty.</exception>
		public static IExamHandler GetHandler(string extension, string format = "soe")
		{
			switch (extension.ToLower())
			{
				case ".xml":
					return new StdExamHandler();
				case ".xlsx":
				case ".xlsm":
					switch (format)
					{
						case "soe":
							return new SoeExamHandler();
						case "tao":
							return new TaoExamHandler();
					}
					break;
			}
			throw new Exception($"Files of extension {extension} not supported");
		}
	}

}