﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web;
using System.IO;
using Pineapples.Providers;
using Pineapples.Data.DataLayer;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pineapples.Controllers
{
	public class StdExamHandler:IExamHandler
	{
		public async Task<System.Dynamic.ExpandoObject> LoadFile(System.IO.FileStream fstrm
            , Guid fileId, MultipartFileData fd, IDSFactory Factory)
		{
			string ExamCodeAttr = "test_group_ID";
			string ExamNamePath = $"Test_name";
			string ExamYear = $"Year";

			dynamic result = new System.Dynamic.ExpandoObject();

			XDocument exam = XDocument.Load(fstrm);
			XElement examCodeElement = exam.XPathSelectElement("//Test") ?? exam.XPathSelectElement("//NMCT_test");

			result.examCode = examCodeElement.Attribute(ExamCodeAttr).Value;
			result.examName = examCodeElement.XPathSelectElement(ExamNamePath).Value;
			result.examYear = examCodeElement.XPathSelectElement(ExamYear).Value;
			result.students = exam.XPathSelectElements("//Students/Student").Count();
			result.results = exam.XPathSelectElements("//Students/Student/Results/Result").Count();
			result.id = fileId;

			return result;
		}

		public async Task<Softwords.DataTools.IDataResult> Process(string fileId, IDSFactory Factory, string username)
		{
			Guid g = new Guid(fileId);
			// get the document back from the Id
			Stream strm = FileDB.Get(fileId);
			XDocument exam = XDocument.Load(strm);
			exam = doTransform(exam);
			return await Factory.Exam().Upload(exam, g, username);
		}

		       /// <summary>
        /// adds a sequence number attribute to each Student node
        /// </summary>
        /// <param name="examData">the XDocument of exam data</param>
        /// <returns></returns>
        private XDocument doTransform(XDocument examData)
        {

            string xslMarkup = @"<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
    xmlns:msxsl='urn:schemas-microsoft-com:xslt' exclude-result-prefixes='msxsl'>
    <xsl:output method='xml' indent='yes'/>
    <xsl:template match='@* | node()'>
        <xsl:copy>
            <xsl:apply-templates select='@* | node()'/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match='Student'>
      <Student>
        <xsl:attribute name='seq'>
          <xsl:value-of select='position()'/>
        </xsl:attribute>
        <xsl:apply-templates select='@* | node()'/>
      
      </Student>
    </xsl:template>
</xsl:stylesheet>
";

            XDocument transformed = new XDocument();
            using (System.Xml.XmlWriter writer = transformed.CreateWriter())
            {
                // Load the style sheet.  
                System.Xml.Xsl.XslCompiledTransform xslt = new System.Xml.Xsl.XslCompiledTransform();
                xslt.Load(System.Xml.XmlReader.Create(new StringReader(xslMarkup)));

                // Execute the transform and output the results to a writer.  
                xslt.Transform(examData.CreateReader(), writer);
            }
            return transformed;
        }

	}
}