﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Net.Http;
using System.Web;
using OfficeOpenXml;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;
using Pineapples.Models;
using Pineapples.Data.DataLayer;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace Pineapples.Controllers
{
	public class TaoExamHandler : IExamHandler
	{
		public async Task<System.Dynamic.ExpandoObject> LoadFile(System.IO.FileStream fstrm, Guid fileId, MultipartFileData fd, IDSFactory Factory)
		{
			OfficeOpenXml.ExcelPackage p = new ExcelPackage(fstrm);
			dynamic result = new System.Dynamic.ExpandoObject();

			// get the first sheet
			ExcelWorksheet wk = p.Workbook.Worksheets[1];
			// get the first table off that sheet...
			if (wk.Tables.Count == 0)
			{
				// make a table from the full range of the worksheet
				wk.Tables.Add(wk.Dimension, "results");
			}
			OfficeOpenXml.Table.ExcelTable tbl = wk.Tables[0];


			// create a ListObject from that table
			ListObject lo = new ListObject(tbl);

			// Quick check to make sure this at least looks like a TAO exported results file
			int numItems = lo.ColumnNames.Count(n => n.ToLower().EndsWith("-score"));
			if (numItems == 0)
			{
				throw new Exception("No columns ending with -SCORE were found in the first sheet of the workbook. This is not a compliant teacher exam file.");
			}

			// get the exam year and code
			string examYear = getWorkbookMetadata(p, "examyear");
			result.examCode = getWorkbookMetadata(p, "examcode");

			if (examYear == string.Empty || result.examCode == string.Empty)
			{
				// Stop everything right here if either of these are not supplied.
				throw new Exception(@"TAO assessment result export files have no way to derive the year and code of exams. 
									You MUST add the ExamYear and ExamCode sheets and include the year and code in cell A1 respectively.");
			}

			var teacherExamType = Factory.Context.TeacherExamTypes.Find(result.examCode);
			result.examName = (teacherExamType != null ? teacherExamType.TeacherExamName : "Exam Code Not Found");
			result.examYear = examYear;
			result.teachers = lo.Rows;
			result.results = lo.Rows * numItems;
			result.id = fileId;

			return result;
		}

		public async Task<Softwords.DataTools.IDataResult> Process(string fileId, IDSFactory Factory, string username)
		{
			// retrieve the xlsx document containing all the candidate scores
			dynamic result = new System.Dynamic.ExpandoObject();

			Guid g = new Guid(fileId);
			// get the document back from the Id
			OfficeOpenXml.ExcelPackage p;
			using (Stream strm = FileDB.Get(fileId))
			{
				p = new ExcelPackage(strm);
			}
			// get the first sheet
			ExcelWorksheet wk = p.Workbook.Worksheets[1];
			// ..and the table on that sheet
			// get the first table off that sheet...
			if (wk.Tables.Count == 0)
			{
				// make a table from the full range of the worksheet
				wk.Tables.Add(wk.Dimension, "results");
			}
			ListObject lo;
			if (wk.Tables.Count > 0)
			{
				OfficeOpenXml.Table.ExcelTable tbl = wk.Tables[0];
				lo = new ListObject(tbl);
			}
			else
			{
				lo = new ListObject(wk);
			}
			// map various column names and change case
			var map = new Dictionary<string, string>();
			map.Add("Test Taker", "TeacherFullName");
			map.Add("Delivery origin", "ExamName");
			map.Add("First Name", "TeacherFirstName");
			map.Add("Last Name", "TeacherLastName");
			lo.MapColumns(map);

			// convert to Xml 
			XDocument examData = lo.ToXml();

			// get the exam year and code
			string examYear = getWorkbookMetadata(p, "examyear");
			string examCode = getWorkbookMetadata(p, "examcode");

			// There is a annoyingly long data in column "Compilation Directory" in a TAO result export file.
			// It is currently not used so I remove it here to tidy up the XML output a bit.
			var rows = examData.Descendants("row")
							.Where(row => row.Attribute("Compilation_Directory") != null)
							.ToList();

			foreach (var row in rows)
			{
				row.Attribute("Compilation_Directory")?.Remove();
				row.Attribute("Delivery_container_serial")?.Remove();
			}

			// embed the year into the Xml
			prepXml(examData, examYear, examCode);

			return await Factory.TeacherExam().UploadTao(examData, g, username, examCode, int.Parse(examYear));

		}

		private string getWorkbookMetadata(ExcelPackage p, string sheetName)
		{
			string examData = string.Empty;

			// look for a sheet $sheetName and take cell A1 - this is the simplest way to adapt 
			// an Tao-format workbook to provide additional metadata (e.g. examCode, examYear)
			// Note: these sheets needs to be added to an exported TAO result file.
			if (String.IsNullOrEmpty(examData))
			{
				var s = p.Workbook.Worksheets.FirstOrDefault(sht => sht.Name.ToLower() == sheetName);
				if (s != null)
					examData = s.Cells[1, 1].Value.ToString();
			}
			return examData;
		}

		private XDocument prepXml(XDocument xd, string examYear, string examCode)
		{
			xd.Root.SetAttributeValue("examYear", examYear);
			xd.Root.SetAttributeValue("examCode", examCode);
			return xd;
		}
	}
}