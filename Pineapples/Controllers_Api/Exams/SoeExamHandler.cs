﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Net.Http;
using System.Web;
using OfficeOpenXml;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;
using Pineapples.Models;
using Pineapples.Data.DataLayer;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace Pineapples.Controllers
{
	public class SoeExamHandler : IExamHandler
	{
		public async Task<System.Dynamic.ExpandoObject> LoadFile(System.IO.FileStream fstrm, Guid fileId, MultipartFileData fd, IDSFactory Factory)
		{
			OfficeOpenXml.ExcelPackage p = new ExcelPackage(fstrm);
			dynamic result = new System.Dynamic.ExpandoObject();

			// get the first sheet
			ExcelWorksheet wk = p.Workbook.Worksheets[1];
			// get the first table off that sheet...
			if (wk.Tables.Count == 0)
			{
				// make a table from the full range of the worksheet
				wk.Tables.Add(wk.Dimension,"results");
			}
			OfficeOpenXml.Table.ExcelTable tbl = wk.Tables[0];

			
			// create a ListObject from that table
			ListObject lo = new ListObject(tbl);


			// get the first "item" column and parse it to get the Exam code and Year of Ed

			

			int numItems = lo.ColumnNames.Count(n => n.ToLower().StartsWith("item_"));
			if (numItems == 0)
			{
				throw new Exception("No item columns were found in the first sheet of the workbook. This is not a compliant exam file.");
			}

			string itemname = lo.ColumnNames.FirstOrDefault(n => n.ToLower().StartsWith("item_"));
			SoeItemInfo info = new SoeItemInfo(itemname);

			// get the exam year
			string examYear = getWorkbookExamYear(p);

			// ****** determine the examCode - loook for it in ttestid and if its not there, assmeble it from the Item names
			result.examCode = getWorkbookExamCode(lo, info);
			var examType = Factory.Context.ExamTypes.Find(result.examCode);
			result.examName = (examType != null ? examType.ExamName: "Exam Code Not Found");
			result.examYear = examYear;
			result.students = lo.Rows;
			result.results = lo.Rows * numItems;
			result.id = fileId;

			return result;
		}

		public async Task<Softwords.DataTools.IDataResult> Process(string fileId, IDSFactory Factory, string username)
		{
			// retrieve the xlsx document containing all the candidate scores
			dynamic result = new System.Dynamic.ExpandoObject();

			Guid g = new Guid(fileId);
			// get the document back from the Id
			OfficeOpenXml.ExcelPackage p;
			using (Stream strm = FileDB.Get(fileId))
			{
				p = new ExcelPackage(strm);
			}
			// get the first sheet
			ExcelWorksheet wk = p.Workbook.Worksheets[1];
			// ..and the table on that sheet
			// get the first table off that sheet...
			if (wk.Tables.Count == 0)
			{
				// make a table from the full range of the worksheet
				wk.Tables.Add(wk.Dimension, "results");
			}
			ListObject lo;
			if (wk.Tables.Count > 0 )
			{
				OfficeOpenXml.Table.ExcelTable tbl = wk.Tables[0];
				lo = new ListObject(tbl);
			} else
			{
				lo = new ListObject(wk);
			}
			// map various column names and change case
			var map = new Dictionary<string, string>();
			map.Add("SCHOOLID", "SchID");
			map.Add("schid", "SchID");
			map.Add("SCHID", "SchID");
			map.Add("TEACHERNAME", "Teacher");
			map.Add("TEACHER", "Teacher");
			map.Add("STUDENTNAME", "FullName");
			map.Add("TESTID", "TestID");
			lo.MapColumns(map);
			
			// convert to Xml 
			XDocument examData = lo.ToXml();

			string itemname = lo.ColumnNames.FirstOrDefault(n => n.ToLower().StartsWith("item_"));
			SoeItemInfo info = new SoeItemInfo(itemname);

			// get the exam year (again)
			string examYear = getWorkbookExamYear(p);

			string examCode = getWorkbookExamCode(lo, info);


			// embed the year into the Xml
			prepXml(examData, examYear, examCode);

			return await Factory.Exam().UploadSoe(examData, g, username);
		
		}

		private string getWorkbookExamCode(ListObject lo, SoeItemInfo info)
		{
			// ****** determine the examCode - loook for it in ttestid and if its not there, assmeble it from the Item names

			// look for a column "TestID" as the definitive exam code
			string examCode = String.Empty;
			if (lo.ColumnNames.Count(n => n.ToLower().StartsWith("testid")) > 0)
			{
				string itemname = lo.ColumnNames.FirstOrDefault(n => n.ToLower().StartsWith("testid"));
				examCode = (string) lo.Row(0)[itemname];
			}
			if (String.IsNullOrEmpty(examCode))
			{
				// look in the item_ structure
				examCode = info.ExamCode;
			}

			return examCode;
		}

		private string getWorkbookExamYear(ExcelPackage p)
		{
			string examYear = string.Empty;

			// ****** determine the examYear
			// see if we can get the exam year back from a defined name
			if (p.Workbook.Names.ContainsKey("examYear"))
			{
				examYear = (string)(p.Workbook.Names["examYear"].Value ?? string.Empty);
			}

			// if no defined name for examYear, look for a sheet 'ExamYear' and take cell A1 - this is the simplest way to adapt 
			// an Soe-format workbook to provide this
			if (String.IsNullOrEmpty(examYear))
			{
				var s = p.Workbook.Worksheets.FirstOrDefault(sht => sht.Name.ToLower() == "examyear");
				if (s != null)
					examYear = s.Cells[1, 1].Value.ToString();
			}
			return examYear;
		}

		/// <summary>
		/// item information encoded in an Soe item name
		/// see pExamWrite.LoadSoeData for the Sql equivalent
		/// </summary>
		internal class SoeItemInfo
		{
			public SoeItemInfo(string itemName)
			{
				ItemSeq = Convert.ToInt32(itemName.Substring(5, 3));
				Subject = itemName.Substring(9, 1);
				SC = itemName.Substring(10, 1);
				YearOfEd = Convert.ToInt32(itemName.Substring(11, 2));
				ExamCode = Subject + itemName.Substring(11, 2);

				Standard = itemName.Substring(13, 2);
				Benchmark = itemName.Substring(15, 2);
				Indicator = itemName.Substring(17, 2);
				SeqBM = Convert.ToInt32(itemName.Substring(19, 2));
				emd = itemName.Substring(21, 1).ToLower();
				AnswerKey = itemName.Substring(23,1).ToUpper();
			}

			public string ExamCode;
			public int ItemSeq;
			public string Subject;
			public string SC;
			public int YearOfEd;
			public string Standard;
			public string Benchmark;
			public string Indicator;
			public int SeqBM;
			public string emd;
			public string AnswerKey;

			//, convert(int, substring(item, 6, 3)) ItemSeq
			//, substring(item,10,1) [Subject]
			//, substring(item,11,1) SC
			//, convert(int, substring(item,12,2)) YearOfEd
			//, substring(item,14,2) Standard
			//, substring(item,16,2) Benchmark
			//, substring(item,18,2) Indicator
			//, convert(int, substring(item,20,2))  SeqBM
			//, substring(item,22,1) emd
			//, upper(substring(item,24,1)) answerKey
		}

		private XDocument prepXml(XDocument xd, string examYear, string examCode)
		{
			xd.Root.SetAttributeValue("examYear", examYear);
			xd.Root.SetAttributeValue("examCode", examCode);
			return xd;
		}
	}
}