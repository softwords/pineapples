using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Security.Claims;
using Pineapples.Data.Models;
using System.Data.Entity;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.Web.Models;

namespace Pineapples.Controllers
{
	[AllowAnonymous]
	[RoutePrefix("api/examframes")]
	public class ExamFrameController : TableMaintenanceController<ExamFrame, string>
	{
		public ExamFrameController(DataLayer.IDSFactory factory) : base(factory) { }


		[HttpGet]
		[Route(@"~/api/exams/{examID:int}/examframes")]
		[Route(@"", Order=-1)]
		public object ReadExamFrame(int examID)
		{
			return Factory.Context.ExamFrames.Where(x => x.ExamID == examID)
				.OrderBy(x => x.Code);
		}

		[HttpPut]
		[Route(@"~/api/exams/{examID:int}/examframes/{id}")]
		public async Task<DynamicBinder<ExamFrame>> UpdateByExam(int examID, string id, DynamicBinder<ExamFrame> binder)
		{
			return await this.Update(id, binder);
		}

		#region viewmode
		[HttpGet]
		[Route(@"info/viewmode", Order=-1)]
		public override object getViewMode()
		{
			Type tt = typeof(ExamFrame);
			object json = viewMode("exams", tt.Name);
			if (json == null)
			{
				Pineapples.Models.ViewMode vm = new Pineapples.Models.ViewMode(tt);
				return vm;
			}
			return json;
		}
		#endregion

		#region copy operations

		[HttpGet]
		[Route(@"~/api/examframes/clone")]
		public async Task<Object> cloneExamFrame(string examCode, int from, int to)
		{
			var result = await this.Factory.Exam().CloneExamFrame(examCode, from, to);
			return result;
		}

		#endregion
	}
}
