﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController : PineapplesApiController
	{
		#region EdLevelAge


		[HttpGet]
		[Deflatable]
		[Route("edlevelage")]
		[Route("edlevelage/table")]
		[Route("edlevelage/nation")]
		[Route("edlevelage/school/{filterCode?}")]
		[Route("edlevelage/district/{filterCode?}")]
		[Route("edlevelage/authority/{filterCode?}")]
		[Route("edlevelage/authoritygovt/{filterCode?}")]
		[Route("edlevelage/schooltype/{filterCode?}")]
		
		public HttpResponseMessage EdLevelAge(string filterCode = null
			, bool? report = false)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string grp = GetGroupFromUri();
			report = (report == null) ? true : report;

			IDataResult ds = Ds.EdLevelAge(grp, filterCode, report);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#endregion
	}
}