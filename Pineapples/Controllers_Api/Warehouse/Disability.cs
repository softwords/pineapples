﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.ExcelOutput;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region  -- Disability

		#region Disability by School

		[HttpGet]
		[Deflatable]
		[Route("disability/school/{schoolNo?}")]
		public HttpResponseMessage SchoolDisabilityXr(string schoolNo = null, int? year = null,
			bool? report = false, string xl = "", string pq="", bool? byAge = false, bool? byClassLevel = true)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			if (pq != String.Empty)
			{
				
				return PQPublish(pq??"Disability_School (PowerQuery)");
			}
			
			bool r = (bool)(report == null ? true : report);
			bool age = (bool)(byAge == null ? true : byAge);
			bool classLevel = (bool)(byClassLevel == null ? true : byClassLevel);
			
			IDataResult ds = Ds.DisabilitySchool(schoolNo, r, age, classLevel);

			if (xl != string.Empty)
			{
				string filename = (xl == null ? "Disability_School" : xl);
				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "SchoolNo";
				string pageField = "SurveyYear";
				if (schoolNo != null)
				{
					PivotDefaults(age, classLevel, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportDisabilityExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client

				return SendExcelPackage(ep, ExcelFileName(filename));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion Disability by School

		#region Excel export

		private OfficeOpenXml.ExcelPackage ExportDisabilityExcel(ExcelPublisher epub
			, System.Data.DataTable dt, string eTagValue)
		{
			return ExportDisabilityExcel(epub, dt, eTagValue, "ClassLevel", "GenderCode", "SurveyYear");
		}
		private OfficeOpenXml.ExcelPackage ExportDisabilityExcel(ExcelPublisher epub, System.Data.DataTable dt, string eTagValue
	, object columnFields)
		{
			return ExportDisabilityExcel(epub, dt, eTagValue, columnFields, "GenderCode", "SurveyYear");
		}
		private OfficeOpenXml.ExcelPackage ExportDisabilityExcel(ExcelPublisher epub, System.Data.DataTable dt, string eTagValue
			, object columnFields, object rowField, string pageField)
		{


			OfficeOpenXml.ExcelPackage ep = epub.PublishExcelTable(dt);
			// fine tune the pivottable

			if (pageField == "SurveyYear")
			{
				int maxYear = dt.MaxInt("SurveyYear"); // extension function to get maximum value
				epub.ConfigPivotTable(ep, "Disab", rowField, columnFields, pageField, maxYear);
			}
			else
			{
				epub.ConfigPivotTable(ep, "Disab", rowField, columnFields, pageField);
			}

			epub.ConfigPivotChart(ep, "Disab", pageField, "GenderCode");

			// stamp with version
			epub.AnnotateExcelPackage(ep, this.ExcelPackageAnnotation(), eTagValue);
			// send to the client
			return ep;
		}

		#endregion


		#endregion Disability

		#region openxml

		HttpResponseMessage PQPublish(string filename)
		{
			// create an instance of the powerquery-ready workbook
			// set the current warehouse url as host,
			var q = Request.RequestUri.ParseQueryString();
			var sb = new System.Text.StringBuilder();
			String join = String.Empty;
			foreach (var n in q.Keys)
			{
				switch (n.ToString().ToLower())
				{
					case "xl":
					case "pq":
						break;

					default:
						string v = q[n.ToString()];
						sb.Append(join);
						join = "&";
						if (!String.IsNullOrWhiteSpace(v))
						{
							sb.Append($"{n}={v}");
						}
						else
						{
							sb.Append($"{n}");
						}
						break;
				}
			}
			//string endpoint = $"api/warehouse/disability/school?{sb.ToString()}";
			int api = Request.RequestUri.AbsoluteUri.IndexOf("/api");
			string b = Request.RequestUri.AbsoluteUri.Substring(0, api + 1);        // the base url for the site

			api = Request.RequestUri.AbsolutePath.IndexOf("/api");          // not the host or the query
			string endpoint = $"{ Request.RequestUri.AbsolutePath.Substring(api + 1)}";
			string qry = sb.ToString();
			if (qry != "")
			{
				endpoint += $"?{qry}";
			}
			string template =
				System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/PowerQueryTools.xlsx");
			byte[] byteArray = File.ReadAllBytes(template);
			MemoryStream mem = new MemoryStream();
			mem.Write(byteArray, 0, (int)byteArray.Length);

			using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(mem, true))

			{
				// get the worksheetPart for the Settings sheet
				WorksheetPart worksheetPart = OpenXMLUtils.WorksheetPart(spreadSheet.WorkbookPart, "Settings");
				SharedStringTablePart stbl = OpenXMLUtils.SharedStringTablePart(spreadSheet.WorkbookPart);
				// set the endpoint
				OpenXMLUtils.SetCellValue(worksheetPart, stbl,"D12", endpoint);
				// set the host indirectly
				worksheetPart = OpenXMLUtils.WorksheetPart(spreadSheet.WorkbookPart, "_lists");
				OpenXMLUtils.SetCellValue(worksheetPart,  "C1", Context);
				OpenXMLUtils.SetCellValue(worksheetPart, "D1", b);

				https://stackoverflow.com/questions/2668643/openxml-sdk-make-excel-recalculate-formula
				spreadSheet.WorkbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
				spreadSheet.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
			}           // using spreadhseetdocument

			// send the updated stream
			return SendExcelStream(mem, ExcelFileName(filename));
		}
	}
	#endregion
}
