﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region Accreditations

		/// <summary>
		/// // returns accreditation data
		/// </summary>
		/// <param name="filterCode">code to select a single value of the aggregation</param>
		/// <param name="result">Accreditation data format</param>
		/// <param name="byStandard">results by standard</param>
		/// <param name="performance">criteria and standard - 1 row per result level</param>
		/// <param name="report"></param>
		/// <returns></returns>
		[HttpGet]
		[Deflatable]
		[Route("accreditations")]
		[Route("accreditations/table")]
		[Route("accreditations/nation")]
		[Route("accreditations/school/{filterCode?}")]
		[Route("accreditations/district/{filterCode?}")]
		[Route("accreditations/authority/{filterCode?}")]
		[Route("accreditations/authoritygovt/{filterCode?}")]
		[Route("accreditations/schooltype/{filterCode?}")]

		public HttpResponseMessage AccreditationsFiltered(string filterCode = null
			, bool? report = false
			, bool? result = false, bool? performance = false
			, bool? byStandard = false
			//, bool? byCriteria = false
			//, bool? bySubCriteria = false
			)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string uri = Request.RequestUri.ToString();
			string grp = (uri.Contains("accreditations/authoritygovt")) ? "authoritygovt" :
			(uri.Contains("accreditations/authority")) ? "authority" :
			(uri.Contains("accreditations/schooltype")) ? "schooltype" :
			(uri.Contains("accreditations/district")) ? "district" :
			(uri.Contains("accreditations/nation")) ? "nation" :
			(uri.Contains("accreditations/table")) ? "table" :
			(uri.Contains("accreditations/school")) ? "school" : "table";

			bool rep = (bool)(report == null ? true : report);
			string content = getAccreditationContentFormat(result, performance, byStandard);

			IDataResult ds = Ds.Accreditations(content, grp, filterCode, rep);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		private string getAccreditationContentFormat(bool? result, bool? performance
			, bool? byStandard = false
			, bool? byCriteria = false          // for future use? see versions prior to 24 9 2019
			, bool? bySubCriteria = false)
		{
			bool perf = (bool)(performance == null ? true : performance);
			bool res = (bool)(result == null ? true : result);
			bool s = (bool)(byStandard == null ? true : byStandard);
			bool c = (bool)(byCriteria == null ? true : byCriteria);
			bool sub = (bool)(bySubCriteria == null ? true : bySubCriteria);

			return (res ? "accreditation" :
							perf ? "accreditationperformance" :     // to match change in view names
							s ? "AccreditationByStandard" :
							c ? "detail" :                          // not supported
							sub ? "detail" :
							"accreditation");
		}

		/// <summary>
		/// A stndard filtering construct on the warehouse table of accreditations
		/// Intended for 'drill-downs' into warehouse accreditation reports
		/// </summary>
		/// <param name="fltr">The schoolAccreditationFilter</param>
		/// <returns></returns>
		[HttpPost]
		[Route("accreditations/filter")]
		public HttpResponseMessage AccreditationsFilter(SchoolAccreditationFilter fltr)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.AccreditationsFilter(fltr);
			return SendETaggedContent(ds, eTagValue);
		}
		#endregion

	}
}