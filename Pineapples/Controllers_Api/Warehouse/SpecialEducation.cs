﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region Special Education
		[HttpGet]
		[Deflatable]
		[Route("specialeducation")]

		public HttpResponseMessage SpecialEducation(bool? report = false)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			bool rep = (bool)(report == null ? true : report);

			IDataResult ds = Ds.SpecialEducation(rep);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion


	}
}