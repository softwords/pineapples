﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.ExcelOutput;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;
using Pineapples.Data.Models;
using Pineapples.Data.DB;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region  -- Enrolment

		#region Enrolment by School

		[HttpGet]
		[Deflatable]
		[Route("enrol/school/{schoolNo?}")]
		public HttpResponseMessage SchoolEnrolXr(string schoolNo = null
			, bool? report = false, string xl = "", string pq = ""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			CheckWellFormedQuery();
			
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = $"\"Enrolment_{schoolNo ?? "School"})";

			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolSchool(schoolNo, r, byAgeBool, byClassLevelBool, year, classLevel);

			// static excel output
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "SchoolNo";
				string pageField = "SurveyYear";
				if (schoolNo != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl == null ? "Enrol_School" : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion School

		#region Enrol by district
		[HttpGet]
		[Deflatable]
		[Route("enrol/district/r")]
		public HttpResponseMessage DictrictEnrolXr(string district = null, string xl = "", string pq = ""
			, bool? byAge = false, bool? byClassLevel = true)
		{
			return DistrictEnrolResponse(true, district, xl, pq, byAge, byClassLevel);
		}

		[HttpGet]
		[Deflatable]
		[Route("enrol/district/{district?}")]
		public HttpResponseMessage DictrictEnrolX(string district = null, bool? report = false, string xl = "", string pq = ""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			return DistrictEnrolResponse(report, district, xl, pq, byAge, byClassLevel, year, classLevel);
		}

		private HttpResponseMessage DistrictEnrolResponse(bool? report, string district = null
			, string xl = "", string pq = ""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)


		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = $"\"Enrolment_{district ?? Vocab("District")}".Sanitize();

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}

			IDataResult ds = Ds.EnrolDistrict(district, r, byAgeBool, byClassLevelBool, year, classLevel);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "District";
				string pageField = "SurveyYear";
				if (district != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl == null ? "Enrol_District" : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#endregion

		#region Enrolment by Authority

		[HttpGet]
		[Deflatable]
		[Route("enrol/authority/{authority?}")]
		public HttpResponseMessage AuthorityEnrol(string authority = null, bool? report = false, string xl = "", string pq = ""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = $"Enrolment_{authority ?? Vocab("Authority")}".Sanitize();
			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
				
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolAuthority(authority, r, byAgeBool, byClassLevelBool, year, classLevel);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "Authority";
				string pageField = "SurveyYear";
				if (authority != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl == null ? "Enrol_Authority" : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion

		#region Enrolment by ElectorateL

		[HttpGet]
		[Deflatable]
		[Route("enrol/electoratel/{electorate?}")]
		public HttpResponseMessage ElectorateLEnrol(string electorate = null, bool? report = false, string xl = "", string pq=""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(pq ?? "Enrolment_ElectorateL (PowerQuery)");
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolElectorateL(electorate, r, byAgeBool, byClassLevelBool, year, classLevel);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "Local Electorate";
				string pageField = "SurveyYear";
				if (electorate != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl == null ? "Enrol_ElectorateL" : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion

		#region Enrolment by ElectorateN

		[HttpGet]
		[Deflatable]
		[Route("enrol/electoraten/{electorate?}")]
		public HttpResponseMessage ElectorateNEnrol(string electorate = null, bool? report = false, string xl = "", string pq=""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = $"Enrolment_{electorate ?? Vocab("ElectorateN")}".Sanitize();

			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolElectorateN(electorate, r, byAgeBool, byClassLevelBool, year, classLevel);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "National Electorate";
				string pageField = "SurveyYear";
				if (electorate != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl == null ? "Enrol_ElectorateN" : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion

		#region Enrolment by Region

		[HttpGet]
		[Deflatable]
		[Route("enrol/region/{region?}")]
		public HttpResponseMessage RegionEnrol(string region = null, bool? report = false, string xl = "", string pq=""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = $"Enrolment_{region ?? Vocab("Region")}".Sanitize();
			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolRegion(region, r, byAgeBool, byClassLevelBool, year, classLevel);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "Region";
				string pageField = "SurveyYear";
				if (region != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				return SendExcelPackage(ep, ExcelFileName(xl ?? $"{xlName}"));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion

		#region Enrolment by Island

		[HttpGet]
		[Deflatable]
		[Route("enrol/island/{island?}")]
		public HttpResponseMessage IslandEnrol(string island = null, bool? report = false, string xl = "", string pq=""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			string xlName = $"Enrolment_{island ?? Vocab("Island")}".Sanitize(); 
			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolIsland(island, r, byAgeBool, byClassLevelBool, year, classLevel);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "Island";
				string pageField = "SurveyYear";
				if (island != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				
				return SendExcelPackage(ep, ExcelFileName(xl ?? $"{xlName}"));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion

		#region Enrolment by SchoolType

		[HttpGet]
		[Deflatable]
		[Route("enrol/schooltype/{schooltype?}")]
		public HttpResponseMessage SchoolTypeEnrol(string schooltype = null
			, bool? report = false, string xl = "", string pq=""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = $"Enrolment_{schooltype ?? Vocab("SchoolType")}".Sanitize();
			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolSchoolType(schooltype, r, byAgeBool, byClassLevelBool, year, classLevel);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = "ClassLevel";
				string rowField = "SchoolType";
				string pageField = "SurveyYear";
				if (schooltype != null)
				{
					PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl == null ? "Enrol_SchoolType" : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion SchoolType


		#region Enrolment - Nation totals
		[HttpGet]
		[Deflatable]
		[Route("enrol/nation")]
		public HttpResponseMessage NationEnrol(bool? report = false, string xl = "", string pq=""
			, bool? byAge = false, bool? byClassLevel = true
			, int? year = null, string classLevel = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = "Enrolment_Nation";
			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}

			bool r = (bool)(report == null ? true : report);
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolNation(r, byAgeBool, byClassLevelBool, year, classLevel);

			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;

				int maxYear = dt.MaxInt("SurveyYear"); // extension function to get maximum value

				var epub = new ExcelPublisher();
				// fine tune the pivottable
				// row/column for pivot depends on byAge byClassLevel
				// TRUE/TRUE Row=Age, Column= Class Gender
				// FALSE/TRUE (default) Row=Gender Column=CLass
				// TRUE/FALSE Row=Age, Column= Gender
				// FALSE /FALSE Row = SurveyYear Col = Gender ( page = nothing)
				object columnFields = "ClassLevel";
				string rowField = "GenderCode";
				string pageField = "SurveyYear";

				PivotDefaults(byAgeBool, byClassLevelBool, out rowField, out columnFields);

				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl??"Enrol_Nation");
				return SendExcelPackage(ep, ExcelFileName(xl ?? $"{xlName}"));
			}
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		[HttpGet]
		[Deflatable]
		[Route("enrol/nation/r")]
		public HttpResponseMessage NationEnrolDeprecated(bool? byAge = false, bool? byClassLevel = true)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			bool byAgeBool = (bool)(byAge == null ? true : byAge);
			bool byClassLevelBool = (bool)(byClassLevel == null ? true : byClassLevel);

			IDataResult ds = Ds.EnrolNation(true, byAgeBool, byClassLevelBool);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#endregion Nation

		#region Excel support

		
		#endregion

		#region Uis Survey data
		[HttpGet]
		[Deflatable]
		[Route("enrol/isced/{year:int}")]

		public HttpResponseMessage IscedEnrol(int year, string xl = "", string pq = "")
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string xlName = $"Enrolment_ISCED".Sanitize();
			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}
			IDataResult ds = Ds.EnrolISCED(year);
			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				object columnFields = "ISCED SubClass";
				string[] rowField = new string[] { "PublicPrivate", "GenderCode" };
				string pageField = "ISCED Level";

				OfficeOpenXml.ExcelPackage ep = this.ExportEnrolmentExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField);

				// send to the client
				string filename = (xl == null ? "Enrol_ISCED" : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion

		#region Enrol - GridMaker

		[HttpGet]
		[Deflatable]
		[Route("enrol/grid/school/{schoolNo?}")]
		public HttpResponseMessage EnrolGridMaker(string schoolNo = null, int? startYear = null, int? endYear = null,
		int? ignoreAge = null, int? rowTotalsOnly = null, int? age = null, string edLevelCode = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			string dataItem = "enrol";
			string groupBy = "S"; // school only, for now
			string filter = "1=1 ";
			if (schoolNo != null)
			{
				filter = $"schoolNo='{schoolNo}'";
			}
			if (age != null)
			{
				filter += $" AND age={age}";
			}
			if (edLevelCode != null)
			{
				filter += $" AND edLevelCode='{edLevelCode}'";
			}

			IDataResult ds = Ds.EnrolGridMaker(groupBy, startYear, endYear, filter, dataItem, ignoreAge, rowTotalsOnly);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("enrol/list/school/{schoolNoOrDistrict?}")]
		public HttpResponseMessage EnrolListMaker(string schoolNoOrDistrict = null, int? startYear = null, int? endYear = null,
	int? ignoreAge = null, int? age = null, string edLevelCode = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			string dataItem = "enrol";
			int? rowTotalsOnly = 1;
			string groupBy = "EE.SchNo, [School Name], ClassLevel"; // school only, for now
			string filter = "1=1 ";
			if (schoolNoOrDistrict != null)
			{
				filter = $"'{schoolNoOrDistrict}' in (schNo, DSS.DistrictCode)";
			}
			if (age != null)
			{
				filter += $" AND age={age}";
			}
			if (edLevelCode != null)
			{
				filter += $" AND edLevelCode='{edLevelCode}'";
			}

			IDataResult ds = Ds.EnrolGridMaker(groupBy, startYear, endYear, filter, dataItem, ignoreAge, rowTotalsOnly);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#endregion


		#region Table - deprecated?

		[HttpGet]
		[Deflatable]
		[Route("tableenrol")]
		public object TableEnrol()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.TableEnrol(false);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("tableenrol/r")]
		public object TableEnrolR()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.TableEnrol(true);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("tableenrolx")]
		public object TableEnrolX()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.TableEnrolX(false);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("tableenrolx/r")]
		public object TableEnrolXR()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.TableEnrolX(true);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		#endregion

		#region Excel export
		private void PivotDefaults(bool byAge, bool classLevel, out string rowField, out object columnFields)
		{
			switch ((classLevel ? 2 : 0) + (byAge ? 1 : 0))
			{
				case 3:     //true/true:
					rowField = "Age";
					columnFields = new[] { "ClassLevel", "GenderCode" };
					break;
				case 2:     // true false
					columnFields = "ClassLevel";
					rowField = "GenderCode";

					break;
				case 1:     // false true
					rowField = "Age";
					columnFields = "GenderCode";
					break;
				default:
					rowField = null;
					columnFields = "GenderCode";
					break;
			}
		}
		private OfficeOpenXml.ExcelPackage ExportEnrolmentExcel(ExcelPublisher epub
			, System.Data.DataTable dt, string eTagValue)
		{
			return ExportEnrolmentExcel(epub, dt, eTagValue, "ClassLevel", "GenderCode", "SurveyYear");
		}
		private OfficeOpenXml.ExcelPackage ExportEnrolmentExcel(ExcelPublisher epub, System.Data.DataTable dt, string eTagValue
	, object columnFields)
		{
			return ExportEnrolmentExcel(epub, dt, eTagValue, columnFields, "GenderCode", "SurveyYear");
		}
		private OfficeOpenXml.ExcelPackage ExportEnrolmentExcel(ExcelPublisher epub, System.Data.DataTable dt, string eTagValue
			, object columnFields, object rowField, string pageField)
		{


			OfficeOpenXml.ExcelPackage ep = epub.PublishExcelTable(dt);
			// fine tune the pivottable

			if (pageField == "SurveyYear")
			{
				int maxYear = dt.MaxInt("SurveyYear"); // extension function to get maximum value
				epub.ConfigPivotTable(ep, "Enrol", rowField, columnFields, pageField, maxYear);
			}
			else
			{
				epub.ConfigPivotTable(ep, "Enrol", rowField, columnFields, pageField);
			}

			epub.ConfigPivotChart(ep, "Enrol", pageField, "GenderCode");

			// stamp with version
			epub.AnnotateExcelPackage(ep, this.ExcelPackageAnnotation(), eTagValue);
			// send to the client
			return ep;
		}

		#endregion

		#endregion Enrolment

	}
}