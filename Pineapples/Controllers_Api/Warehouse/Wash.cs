﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region Wash

		/// <summary>
		/// // returns accreditation data
		/// </summary>
		/// <param name="filterCode">code to select a single value of the aggregation</param>
		/// <param name="report"></param>
		/// <returns></returns>
		[HttpGet]
		[Deflatable]
		[Route("wash")]
		[Route("wash/table")]
		[Route("wash/nation")]
		[Route("wash/school/{filterCode?}")]
		[Route("wash/district/{filterCode?}")]
		[Route("wash/authority/{filterCode?}")]
		[Route("wash/authoritygovt/{filterCode?}")]
		[Route("wash/schooltype/{filterCode?}")]

		public HttpResponseMessage WashFiltered(string filterCode = null
			, bool? report = false
			, bool? result = false, bool? performance = false
			, bool? byStandard = false
			//, bool? byCriteria = false
			//, bool? bySubCriteria = false
			)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string uri = Request.RequestUri.ToString();
			string grp = (uri.Contains("wash/authoritygovt")) ? "authoritygovt" :
			(uri.Contains("wash/authority")) ? "authority" :
			(uri.Contains("wash/schooltype")) ? "schooltype" :
			(uri.Contains("wash/district")) ? "district" :
			(uri.Contains("wash/nation")) ? "nation" :
			(uri.Contains("wash/table")) ? "table" :
			(uri.Contains("wash/school")) ? "school" : "table";

			bool rep = (bool)(report == null ? true : report);
			string content = getWashContentFormat(result, performance, byStandard);

			IDataResult ds = Ds.Wash(content, grp, filterCode, rep);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		private string getWashContentFormat(bool? result, bool? performance
			, bool? byStandard = false
			, bool? byCriteria = false          // for future use? see versions prior to 24 9 2019
			, bool? bySubCriteria = false)
		{
			bool perf = (bool)(performance == null ? true : performance);
			bool res = (bool)(result == null ? true : result);
			bool s = (bool)(byStandard == null ? true : byStandard);
			bool c = (bool)(byCriteria == null ? true : byCriteria);
			bool sub = (bool)(bySubCriteria == null ? true : bySubCriteria);

			return (res ? "accreditation" :
							perf ? "accreditationperformance" :     // to match change in view names
							s ? "AccreditationByStandard" :
							c ? "detail" :                          // not supported
							sub ? "detail" :
							"accreditation");
		}

		[HttpGet]
		[Route("wash/questions")]
		public HttpResponseMessage WashQuestions()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.WashQuestions();
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpPost]
		[Deflatable]
		[Route("wash/responses")]
		public object WashResponses(WashResponsesFilter fltr)
		{
			// don't allow the user to drill into school level detail they are not permitted to see
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			IDataResult ds = Ds.WashResponses(fltr);
			return ds.ResultSet;
		}

		[HttpGet]
		[Deflatable]
		[Route("wash/toilets")]
		public object WashToilets()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.WashToilets();
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("wash/water")]
		public object WashWater()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.WashWater();
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#endregion
	}
}