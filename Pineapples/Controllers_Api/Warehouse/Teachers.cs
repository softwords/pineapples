﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.ExcelOutput;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{

		[HttpGet]
		[Deflatable]
		[Route("teachers")]
		[Route("teachers/table")]
		[Route("teachers/nation")]
		[Route("teachers/school/{filterCode?}")]
		[Route("teachers/district/{filterCode?}")]
		public object Teachers(string filterCode = null, bool? report = false, string xl = "", string pq = "")
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string grp = GetGroupFromUri();
			report = (report == null) ? true : report;

			IDataResult ds = Ds.TeacherCount(grp, filterCode, report);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#region Teacher Numbers
		[HttpGet]
		[Deprecated("api/warehouse/teachers/school/[schoolno]?report")]
		[Deflatable]
		[Route("schoolteachercount/{schoolNo}")]
		public object SchoolTeacherCount(string schoolNo)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.TeacherCount("school", schoolNo, true);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deprecated("api/warehouse/teachers/school?report")]
		[Deflatable]
		[Route("schoolteachercount")]
		public object AllSchoolTeacherCount()
		{
			IDataResult ds = Ds.SchoolTeacherCount(null);
			return ds.ResultSet;
		}

		[HttpGet]
		[Deprecated("api/warehouse/teachers?report")]
		[Deflatable]
		[Route("teachercount")]         // use 
		public object TeacherCount()
		{
			IDataResult ds = Ds.TeacherCount();
			return ds.ResultSet;
		}

		// Support for extended version is not yet fully implemented
		// Teachers dashboard is still using this deprecated API (i.e. /api/warehouse/teachercount)
		// so for now we just move to /api/warehouse/teachercountx which to be consistent will also be
		// deprecated. 
		// Note that the [Deprecated("api/warehouse/teachers?report&extended")] is not yet implemented
		[HttpGet]
		[Deprecated("api/warehouse/teachers?report&extended")]
		[Deflatable]
		[Route("teachercountx")]         // use 
		public object TeacherCountX()
		{
			IDataResult ds = Ds.TeacherCountX();
			return ds.ResultSet;
		}

		[HttpGet]
		[Deflatable]
		[Route("teacherqual")]
		public object TeacherQual()
		{
			IDataResult ds = Ds.TeacherQual();
			return ds.ResultSet;
		}
		#endregion

		#region Teacher activity
		[HttpGet]
		[Deflatable]
		[Route("teacheractivity")]
		[Route("teacheractivity/table")]
		[Route("teacheractivity/nation")]
		[Route("teacheractivity/school/{filterCode?}")]
		[Route("teacheractivity/district/{filterCode?}")]
		[Route("teacheractivity/authority/{filterCode?}")]
		[Route("teacheractivity/authoritygovt/{filterCode?}")]
		[Route("teacheractivity/schooltype/{filterCode?}")]
		[Route("teacheractivity/island/{filterCode?}")]
		[Route("teacheractivity/region/{filterCode?}")]
		public HttpResponseMessage TeacherActivity(string filterCode = null
			, bool? report = false, string xl = "", string pq=""
			, bool? byActivity = false, int? year = null
			, string columnField = "Activity", string dataField = "Staff")
		{
			return TeacherResponse("Activity", filterCode, report, xl, pq,  byActivity, year, columnField, dataField);
		}
		#endregion

		#region Teacher Job
		[HttpGet]
		[Deflatable]
		[Route("teacherjob")]
		[Route("teacherjob/table")]
		[Route("teacherjob/nation")]
		[Route("teacherjob/school/{filterCode?}")]
		[Route("teacherjob/district/{filterCode?}")]
		[Route("teacherjob/authority/{filterCode?}")]
		[Route("teacherjob/authoritygovt/{filterCode?}")]
		[Route("teacherjob/schooltype/{filterCode?}")]
		[Route("teacherjob/island/{filterCode?}")]
		[Route("teacherjob/region/{filterCode?}")]
		public HttpResponseMessage TeacherJobs(string filterCode = null
			, bool? report = false, string xl = "", string pq=""
			, bool? byActivity = false, int? year = null
			, string columnField = "JobTitle", string dataField = "NumStaff")
		{
			return TeacherResponse("Job", filterCode, report, xl, pq, byActivity, year, columnField, dataField);
		}
		#endregion

		#region Teacher education level
		[HttpGet]
		[Deflatable]
		[Route("teacheredlevel")]
		[Route("teacheredlevel/table")]
		[Route("teacheredlevel/nation")]
		[Route("teacheredlevel/school/{filterCode?}")]
		[Route("teacheredlevel/district/{filterCode?}")]
		[Route("teacheredlevel/authority/{filterCode?}")]
		[Route("teacheredlevel/authoritygovt/{filterCode?}")]
		[Route("teacheredlevel/schooltype/{filterCode?}")]
		[Route("teacheredlevel/island/{filterCode?}")]
		[Route("teacheredlevel/region/{filterCode?}")]
		public HttpResponseMessage TeacherEdLevel(string filterCode = null
			, bool? report = false, string xl = "", string pq=""
			, bool? byActivity = false, int? year = null
			, string columnField = "edLevelCode", string dataField = "NumTeachers")
		{
			return TeacherResponse("Edlevel", filterCode, report, xl, pq, byActivity, year, columnField, dataField);
		}
		#endregion

		#region Teacher housing
		// No need to support all the "grouping" (their warehouse table/views are not there anyway)
		[HttpGet]
		[Deflatable]
		[Route("teacherhousing")]
		[Route("teacherhousing/x")]
		//[Route("teacherhousing/table")]
		//[Route("teacherhousing/nation")]
		[Route("teacherhousing/school/{filterCode?}")]
		//[Route("teacherhousing/district/{filterCode?}")]
		//[Route("teacherhousing/authority/{filterCode?}")]
		//[Route("teacherhousing/authoritygovt/{filterCode?}")]
		//[Route("teacherhousing/schooltype/{filterCode?}")]
		//[Route("teacherhousing/island/{filterCode?}")]
		//[Route("teacherhousing/region/{filterCode?}")]
		public HttpResponseMessage TeacherHousing(string filterCode = null
			, bool? report = false, string xl = "", string pq = ""
			, bool? byActivity = false, int? year = null
			, string columnField = "District", string dataField = "TotalTeachers")
		{
			return TeacherResponse("Housing", filterCode, report, xl, pq, byActivity, year, columnField, dataField);
		}
		#endregion

		#region General Teachers responses handling
		/// <summary>
		/// Handles the Teacher APIs endpoints response in a more general and flexible way. Could supersede
		/// DoActivity.
		/// </summary>
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <param name="filterCode">The selected entity (schNo CHK1002, IslandCode 01, etc.)</param>
		/// <param name="report">Whether to produce the report format (i.e. Gender denormalized)</param>
		/// <param name="xl">Whether to produce an Excel spreadsheet with Pivot table</param>
		/// <param name="byActivity">Optional field used in fine tuning data for pivot tables for the excel export</param>
		/// <param name="year">Optional year for which the data should be exclusively produced, also used in the Excel exports</param>
		/// <param name="columnField">Optional columnField to use with Pivot Table (only used in an Excel export)</param>
		/// <param name="dataField">Optional dataField to use with Pivot Table (only used in an Excel export)</param>
		/// <returns></returns>
		public HttpResponseMessage TeacherResponse(string content, string filterCode = null
			, bool? report = false, string xl = "", string pq=""
			, bool? byActivity = false, int? year = null
			, string columnField = "District", string dataField = "Staff")
		{

			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string grp = GetGroupFromUri();
			string xlName = $"TeacherActivity_{Vocab("grp")}".Sanitize();

			// powerquery excel output
			if (pq != String.Empty)
			{
				return PQPublish(ExcelFileName(pq ?? $"{xlName} (PowerQuery)"));
			}
		

			
			bool r = (bool)(report == null ? true : report);

			// TODO This only applies to TeacherActivity. Need to rethink this a bit more generally
			// It has repercussions all the way down the data layer.
			bool byActivityBool = (bool)(byActivity == null ? true : byActivity);

			IDataResult ds = Ds.TeacherData(content, grp, filterCode, r, xl, byActivityBool, year);

			if (xl != string.Empty)
			{

				System.Data.DataTable dt = ds.ResultSet as System.Data.DataTable;
				var epub = new ExcelPublisher();
				// fine tune the pivottable

				object columnFields = columnField;
				// When producing an export of extended nature
				// (i.e. warehouse views ending with 'X' and not a particular grouping such as Nation, District, etc.)
				// there is no obvious column to use as the rowField(s). We'll just default with a popular one: AuthorityGroup
				string rowField = grp == "X" ? "AuthorityGroup" : grp;
				string pageField = "SurveyYear";
				if (filterCode != null | grp == "Nation")
				{
					rowField = null;
					//TeacherActivityPivotDefaults(byActivityBool, out rowField, out columnFields);
				}
				OfficeOpenXml.ExcelPackage ep = this.ExportTeacherDataExcel(epub
					, dt, eTagValue, columnFields, rowField, pageField, dataField);

				// send to the client
				string filename = (xl == null ? String.Format("Teacher{0}_{1}", content, grp) : xl);
				return SendExcelPackage(ep, ExcelFileName(filename));
			}

			return SendETaggedContent(ds.ResultSet, eTagValue);

		}

		/// <summary>
		/// Parse a standard warehouse Uri (i.e. /api/warehouse/Topic{Grouper}) 
		/// </summary>
		/// 
		/// <returns>Grouper (e.g. Nation, District, Island, Region) </returns>
		string GetGroupFromUri()
		{
			string[] parts = Request.RequestUri.AbsolutePath.ToLower().Split('/');
			int i = Array.IndexOf(parts, "warehouse");
			if (parts.Length <= i + 2)
			{
				return "Table";             // ie table is the default grouping
			}
			return char.ToUpper(parts[i + 2][0]) + parts[i + 2].Substring(1); ;
		}

		#endregion

		#region Excel export

		//private void TeacherActivityPivotDefaults(bool byActivity, out string rowField, out object columnFields)
		//{

		//}

		private OfficeOpenXml.ExcelPackage ExportTeacherDataExcel(ExcelPublisher epub
			, System.Data.DataTable dt, string eTagValue, object columnFields, object rowField, string pageField
			, string dataField)
		{
			// TODO perhaps add support for multiple data fields for the pivot table
			// But this is low priority, users can very well do their own pivot table with the resulting exported workbook.
			// (i.e. change string dataField to object dataFields)

			OfficeOpenXml.ExcelPackage ep = epub.PublishExcelTable(dt);
			// fine tune the pivottable

			if (pageField == "SurveyYear")
			{
				int maxYear = dt.MaxInt("SurveyYear"); // extension function to get maximum value
				epub.ConfigPivotTable(ep, dataField, rowField, columnFields, pageField, maxYear);
			}
			else
			{
				epub.ConfigPivotTable(ep, dataField, rowField, columnFields, pageField);
			}

			epub.ConfigPivotChart(ep, dataField, pageField, null);

			// stamp with version
			epub.AnnotateExcelPackage(ep, this.ExcelPackageAnnotation(), eTagValue);
			// send to the client
			return ep;
		}
		#endregion

	}
}