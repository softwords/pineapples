﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{

		#region -- Flow --

		[HttpGet]
		[Deflatable]
		[Route("flow/school/{schoolNo}")]
		public HttpResponseMessage FlowSchool(string schoolNo = null, bool? report = false, bool? asPerc = false)
		{

			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			bool r = (bool)(report == null ? true : report);
			bool perc = (bool)(asPerc == null ? true : asPerc);


			IDataResult ds = Ds.FlowSchool(schoolNo, r, perc);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("flow/district/{district?}")]
		public HttpResponseMessage FlowDistrict(string district = null, bool? report = false, bool? asPerc = false)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			bool r = (bool)(report == null ? true : report);
			bool perc = (bool)(asPerc == null ? true : asPerc);


			IDataResult ds = Ds.FlowDistrict(district, r, perc);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}
		[HttpGet]
		[Deflatable]
		[Route("flow/nation")]
		public HttpResponseMessage FlowNation(bool? report = false, bool? asPerc = false)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			bool r = (bool)(report == null ? true : report);
			bool perc = (bool)(asPerc == null ? true : asPerc);


			IDataResult ds = Ds.FlowNation(r, perc);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("flowrates")]
		public object FlowRates()
		{
			IDataResult ds = Ds.FlowRates();
			return ds.ResultSet;
		}
		#endregion
	}
}