﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController : PineapplesApiController
	{

		[HttpGet]
		[Deflatable]
		[Route("pupilteacherratio")]
		[Route("pupilteacherratio/table")]
		[Route("pupilteacherratio/nation")]
		[Route("pupilteacherratio/school/{filterCode?}")]
		[Route("pupilteacherratio/district/{filterCode?}")]

		public object PupilTeacherRatio(string filterCode = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string grp = GetGroupFromUri();
			bool report = false;

			IDataResult ds = Ds.PupilTeacherRatio(grp, filterCode, report);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#region  deprecated
		[HttpGet]
		[Deflatable]
		[Route("teacherpupilratio")]
		public object TeacherPupilRatio()
		{
			IDataResult ds = Ds.TeacherPupilRatio();
			return ds.ResultSet;
		}


		[HttpGet]
		[Deflatable]
		[Route("schoolteacherpupilratio")]
		public object SchoolTeacherPupilRatio()
		{
			IDataResult ds = Ds.SchoolTeacherPupilRatio();
			return ds.ResultSet;
		}
		#endregion
	}
}