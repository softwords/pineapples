﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region Exams

		[HttpGet]
		[Deflatable]
		[Route("exams/table")]
		public object ExamsTable()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			IDataResult ds = Ds.ExamTableTyped();
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("exams/tablex")]
		public object ExamsTableX()
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			IDataResult ds = Ds.ExamTableTypedX();
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("exams/district/{districtCode?}/r")]
		public object ExamsDistrictResultsR(string districtCode = null, bool? reportFormat = false)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			bool r = (bool)(reportFormat == null ? true : reportFormat);

			IDataResult ds = Ds.ExamDistrictResults(districtCode, r, true);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("exams/district/{districtCode?}")]
		public object ExamsDistrictResults(string districtCode = null, bool? reportFormat = false, bool? report = false)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			bool x = (bool)(reportFormat == null ? true : reportFormat);
			bool r = (bool)(report == null ? true : report);

			IDataResult ds = Ds.ExamDistrictResults(districtCode, x, r);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}


		[HttpGet]
		[Deflatable]
		[Route("examsschoolresults")]
		public object ExamsAllResults()
		{
			IDataResult ds = Ds.ExamAllSchoolResults();
			return ds.ResultSet;
		}

		[HttpGet]
		[Deflatable]
		[Route("exams/school/{schoolNo?}/r")]
		public object ExamsSchoolResults(string schoolNo = null, bool? reportFormat = false) // Report seems to mean 'X' extended with exams, inconsistency needs to be fixed.
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			bool x = (bool)(reportFormat == null ? true : reportFormat);

			IDataResult ds = Ds.ExamSchoolResults(schoolNo, x, true);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("examsschoolresults/{schoolNo?}")]           // deprecated
		[Route("exams/school/{schoolNo?}")]
		public object ExamsSchoolResults(string schoolNo = null, bool? reportFormat = false, bool? report = false) // see inconsistency comment above
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			bool x = (bool)(reportFormat == null ? true : reportFormat);
			bool r = (bool)(report == null ? true : report);

			IDataResult ds = Ds.ExamSchoolResults(schoolNo, x, r);
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#endregion
		// this section support end points in exams, and warehouse
		#region analysis 
		[HttpGet]
		[Deflatable]
		[Route(@"exams/contextdata")]
		[Route(@"~/api/exams/contextdata")]
		[PineapplesPermission(PermissionTopicEnum.Exam, Softwords.Web.PermissionAccess.Write)]
		public async Task<HttpResponseMessage> ContextData(string schoolNo, string examCode, int? examYear = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = await Factory.Exam().ContextData(schoolNo, examCode, examYear);
			return SendETaggedContent(ds.ResultSet, eTagValue);

		}
		#endregion

		#region Deprecated
		// DEPRECATED - this route leads to a customised query hardcoding FSM achievementLevel names
		[HttpGet]
		[Deflatable]
		[Deprecated("api/warehouse/exams/district/{districtCode?}/r")]
		[Route("examsdistrictresults")]
		public object ExamsDistrictResultsLegacy()
		{
			// to do - what should this return??
			IDataResult ds = Ds.ExamDistrictResultsv1();
			return ds.ResultSet;
		}
		#endregion
	}
}