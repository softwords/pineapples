﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;
// using System.Web.Http.Cors; 

namespace Pineapples.Controllers.Warehouse
{
    [RoutePrefix("api/warehouse")]
	//[EnableCors(origins: "*", headers: "*", methods: "*")] example enable cors on controller
	public partial class WarehouseController : PineapplesApiController
    {
        public WarehouseController(DataLayer.IDSFactory factory) : base(factory) { }

 		/// <summary>
		/// return true if the client has supplied an etag that matches the current warehouse version
		/// </summary>
		/// <returns></returns>
		private bool checkETag( out string eTagValue)
		{

			eTagValue = Ds.WarehouseVersion();
			// wrap in qutes before testing the versions from the client
			string currentETag = String.Format("\"{0}\"", eTagValue); 
			
			ICollection<EntityTagHeaderValue> etagsFromClient = Request.Headers.IfNoneMatch;
			if (etagsFromClient.Count > 0)
			{
				return (etagsFromClient.Any(t => t.Tag == currentETag));
			}
			return false;
		}

		private IDSWarehouse Ds
        {
            get { return Factory.Warehouse(); }
        }
    }
}
