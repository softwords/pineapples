﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		[HttpGet]
		[Deflatable]
		[Route(@"enrolbyschool/{schoolNo}")]

		//[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
		// Deprecated - use enrol/school/schoolNo?report
		public object GetSchool(string schoolNo)
		{
			//AccessControl(schoolNo);
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.EnrolSchool(schoolNo, true, false, true);
			return SendETaggedContent(ds.ResultSet, eTagValue);

		}

		#region Enrolment
		// Deprecated routing - use enrol/district
		[HttpGet]
		[Deflatable]
		[Route("districtenrol")]
		public object DistrictEnrol()
		{
			return DistrictEnrolResponse(false);
		}

		// Deprecated routing - use enrol/district/r
		[HttpGet]
		[Deflatable]
		[Route("districtenrol/r")]
		public object DistrictEnrolr()
		{
			return DistrictEnrolResponse(true);
		}
		#endregion

		#region Flow Rates
		// deprecated - use school flow
		[HttpGet]
		[Deflatable]
		[Route("schoolflowrates")]
		public object AllSchoolFlowRates()
		{
			IDataResult ds = Ds.AllSchoolFlowRates();
			return ds.ResultSet;
		}
		[HttpGet]
		[Deflatable]
		[Route("schoolflowrates/{schoolNo}")]
		public object SchoolFlowRates(string schoolNo)
		{
			IDataResult ds = Ds.SchoolFlowRates(schoolNo);
			return ds.ResultSet;
		}
		#endregion

		#region ed level age
		[HttpGet]
		[Deflatable]
		// this route does not appear to be used anywhere Changing to avoid conflict with new routes
		// replace with edlevelage?report
		[Route("v0/edlevelage")]
		public object EdLevelAge()
		{
			IDataResult ds = Ds.EdLevelAge();
			return ds.ResultSet;
		}
		#endregion
	}
}