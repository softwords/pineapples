﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region Scholarships

		[HttpGet]
		[Deflatable]
		[Route("scholarships/rounds")]
		public object ScholarshipRounds()
		{
			// will we write scholarships to warehouse tables, or source from operational data?
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				//				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.ScholarshipRounds();
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		[HttpGet]
		[Deflatable]
		[Route("scholarships/payments")]
		public object ScholarshipPayments()
		{
			// will we write scholarships to warehouse tables, or source from operational data?
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				//				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			IDataResult ds = Ds.ScholarshipPayments();
			return SendETaggedContent(ds.ResultSet, eTagValue);
		}

		#endregion

	}
}