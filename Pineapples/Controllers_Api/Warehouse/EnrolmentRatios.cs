﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{

		[HttpGet]
		[Deflatable]
		[Route("classleveler")]
		public object ClassLevelER()
		{
			IDataResult ds = Ds.ClassLevelER();
			return ds.ResultSet;
		}

		[HttpGet]
		[Deflatable]
		[Route("edleveler")]
		public object EdLevelER()
		{
			IDataResult ds = Ds.EdLevelER();
			return ds.ResultSet;
		}



	}
}