﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers.Warehouse
{
	public partial class WarehouseController
	{
		#region FinancialData

		[HttpGet]
		[Deflatable]
		[Route("finance")]
		[Route("finance/nation")]
		[Route("finance/district/{filterCode?}")]
		[Route("finance/sector/{filterCode?}")]
		[Route("finance/costcentre/{filterCode?}")]

		public object EdExpenditure(string filterCode = null)
		{
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}
			string uri = Request.RequestUri.ToString();
			string grp = (uri.Contains("finance/nation")) ? "nation" :
			(uri.Contains("finance/district")) ? "district" :
			(uri.Contains("finance/sector")) ? "sector" :
			(uri.Contains("finance/costcentre")) ? "costcentre" : "table";

			IDataResult ds = Ds.EdExpenditure(grp, filterCode);
			return SendETaggedContent(ds.ResultSet, eTagValue);

		}

		#endregion

	}
}