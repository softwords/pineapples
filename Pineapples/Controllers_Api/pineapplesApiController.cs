﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using OfficeOpenXml;
using System.Security.Claims;
using Pineapples.Models;

namespace Pineapples.Controllers
{
    public class PineapplesApiController : Softwords.Web.Controllers.apiControllerBase
	{
        private readonly DataLayer.IDSFactory _factory;
        protected PineapplesApiController(DataLayer.IDSFactory factory)
        {
            _factory = factory;
        }
        protected DataLayer.IDSFactory Factory
        {
            get { return _factory; }
        }

        protected string AppDataPath
        {
            get
            {
                return System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data");
            } 
        }



		protected HttpResponseMessage SendETaggedContent(object content, string eTagValue)
		{
			HttpResponseMessage response = null;
			var acceptHeader = Request.Headers.Accept.Where(
				a => a.MediaType.Contains("application/xml")|| a.MediaType.Contains("text/xml"));
			if (acceptHeader.Any())
			{
				// we want to return Xml from our content
				System.IO.Stream strm = new System.IO.MemoryStream();
				var dt = (System.Data.DataTable)content;	// Only DataTable supported yet
				dt.TableName = "row";
				dt.DataSet.DataSetName = "warehouse";		// TO DO needs to be more general than this
				dt.WriteXml(strm);
				strm.Position = 0;
				System.Xml.Linq.XDocument x = System.Xml.Linq.XDocument.Load(strm);
				var xmlContent = new Softwords.Web.Content.XmlContent(x);
				response = new HttpResponseMessage() { Content = xmlContent };

			}
			else
			{
				response = Request.CreateResponse(content);
			}
			string quotedEtag = String.Format("\"{0}\"", eTagValue);
			response.Headers.ETag = new System.Net.Http.Headers.EntityTagHeaderValue(quotedEtag);
			return response;

		}

		#region Excel Export

		protected HttpResponseMessage SendExcelPackage(ExcelPackage ep, string filename)
		{
			var strm = new System.IO.MemoryStream();
			ep.SaveAs(strm);
			return SendExcelStream(strm, filename);
		}
		protected HttpResponseMessage SendExcelStream(System.IO.MemoryStream strm, string filename)
		{
			strm.Position = 0;

			var result = new HttpResponseMessage(HttpStatusCode.OK);

			string mimetype = String.Empty;

			// this header is set clientside (ExcelDownload.Component.ts) to indicate this is a download through code
			if (Request.Headers.Accept.Contains(new MediaTypeWithQualityHeaderValue("binary/base64encoded")))
			{
				//https://stackoverflow.com/questions/34993292/how-to-save-xlsx-data-to-file-as-a-blob/35713609#35713609
				// explains how to get the xlsx successfully saved on the client, by sending it as Base64String
				// and manipulating it to create the blob
				// see HttpFileDownloadInterceptor.ts for client side
				//
				
				string content = Convert.ToBase64String(strm.ToArray());
				result.Content = new StringContent(content);
				mimetype = "binary/base64encoded";
				//"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				result.Content.Headers.ContentType =
					new System.Net.Http.Headers.MediaTypeHeaderValue(mimetype);
				result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
				{
					FileName = filename
				};
				return result;
			}
			// However, we still need to support direct downloads of an Excel object e.g. via 
			// the browser address bar. The browser will treat this as a download, and the downloaded
			// file is a valid excel workbook
			result.Content = new StreamContent(strm);
			mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			result.Content.Headers.ContentType =
				new System.Net.Http.Headers.MediaTypeHeaderValue(mimetype);
			result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
			{
				FileName = filename
			};
			return result;
		}

		protected string ExcelPackageAnnotation()
		{
			DateTime time = DateTime.UtcNow;
			return $"Data retrieved from {Request.RequestUri} ({Context}) on {time:yyyy-MMM-dd HH:mm} UTC";

		}
		public string ExcelFileName(string template)
		{
			// if the template has an extension, just it verbatim ( strip any path)
			string filename = string.Empty;
			if (System.IO.Path.GetExtension(template) != String.Empty)
			{
				filename = System.IO.Path.GetFileName(template);
			}
			else
			{
				filename = $"{Context.ToUpper()} {System.IO.Path.GetFileName(template)} {DateTime.UtcNow:yyyyMMdd HHmm}.xlsx";
			}
			return filename.Sanitize();
		}

		
		#endregion

		/// <summary>
		/// Helper function to test topic level permissions when more control is needed than 
		/// simply allowing or disalalowing the route entirely
		/// e.g. an endpoint may include or omit certain data based on a permission flag
		/// </summary>
		/// <param name="topic"></param>
		/// <param name="access"></param>
		/// <returns></returns>
		public bool HasPermission(Pineapples.Models.PermissionTopicEnum topic, 
			Softwords.Web.PermissionAccess access)
		{
			return Pineapples.Models.PineapplesPermissionAttribute.HasPermission((ClaimsPrincipal)User
				, topic, access);

		}

		/// <summary>
		/// check that the query string is well-formed - empty tokens (e.g. ?&year=2020)
		/// will set all unsupplied arguments to null, ignoring the defaults
		/// </summary>
		/// <exception cref="HttpResponseException"></exception>
		protected void CheckWellFormedQuery()
		{
			if (Request.RequestUri.ParseQueryString().AllKeys.Contains(String.Empty))
			{
				var resp = Request.CreateErrorResponse(HttpStatusCode.BadRequest, $"Query may not contain empty token: {Request.RequestUri.Query}");
				throw new HttpResponseException(resp);
			}
		}

		/// <summary>
		/// Translate a vocab term
		/// Usually this is done client-side; however it is useful to expose on the server
		/// when naming Excel workbooks sent to the client
		/// </summary>
		/// <param name="term"></param>
		/// <returns>the translated term, or term if no translation</returns>
		protected string Vocab(string term)
		{
			return Factory.Context.Vocab.Find(term)?.Description??term;
		}

	}
}