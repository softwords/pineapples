﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using System.Threading.Tasks;

using System.Collections.Generic;
using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Softwords.Web;

using Softwords.DataTools;
using Softwords.Web.Models;
using System.Data.Entity;
using System.Reflection;
using Pineapples.Data.Models;
using System.Linq;


// TODO: Keep this file as a home for school survey maintenance activities

namespace Pineapples.Controllers
{
    [RoutePrefix("api/schoolsurveys")]
	[Authorize]
	public class SchoolSurveysController : TableMaintenanceController<SchoolSurvey, int?> 
    {
        public SchoolSurveysController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpGet]
        [Route(@"{schoolNo}/{year:int}")]
        public async Task<HttpResponseMessage> ReadBySchoolAndYear(string schoolNo, int year)
        {
            HttpResponseMessage response;
            SchoolSurvey ss = await Factory.Context.SchoolSurveys
                    .Where(s => s.schNo == schoolNo && s.svyYear == year)
                    .FirstOrDefaultAsync();
            if (ss == null)
			{
                School s = Factory.Context.Schools.Find(schoolNo);
                if (s == null)
				{
                   
                    response = new HttpResponseMessage(HttpStatusCode.NotFound);
                    response.Content = new StringContent($"School {schoolNo} not found");
                    return response;
				}
                ss = new SchoolSurvey()
                {
                    schNo = schoolNo,
                    svyYear = (short)year,
                    ssSchType = s.schType
                };
			}
            response = Request.CreateResponse<SchoolSurvey>(HttpStatusCode.OK, ss);
            response.Headers.Add("X-Tag", "schoolsurvey");
            return response;
        }

        #region Pupil Table
        [HttpGet]
        [Route(@"pupiltable/{schoolNo}/{year:int}/{tableName}")]
        public object PupilTable(string schoolNo, int year, string tableName)
        {
            return Factory.SurveyEntry().PupilTable(schoolNo, year, tableName); //.ResultSet;
        }

        [HttpPost]
        [Route(@"pupiltable")]
        public object SavePupilTable(PupilTable pt)
        {
            return Factory.SurveyEntry().SavePupilTable(pt, this.User.Identity.Name); //.ResultSet;
        }
        #endregion

        #region ResourceList
        [HttpGet]
        [Route(@"resourcelist/{schoolNo}/{year:int}/{category}")]
        public object ResourceList(string schoolNo, int year, string category)
        {
            return Factory.SurveyEntry().ResourceList(schoolNo, year, category); //.ResultSet;
        }

        [HttpPost]
        [Route(@"resourcelist")]
        public object SaveResourceList(ResourceList rl)
        {
            return Factory.SurveyEntry().SaveResourceList(rl, this.User.Identity.Name); //.ResultSet;
        }
        #endregion
    }
}
