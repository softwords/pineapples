﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using System.Threading.Tasks;

using System.Collections.Generic;
using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Softwords.Web;

using Softwords.DataTools;
using Softwords.Web.Models;
using System.Data.Entity;
using System.Reflection;
using Pineapples.Data.Models;
using System.Linq;

namespace Pineapples.Controllers
{
	[RoutePrefix("api/scholarships")]
	[Authorize]
	public class ScholarshipsController : TableMaintenanceController<Scholarship, int?> //PineapplesApiController
	{
		public ScholarshipsController(DataLayer.IDSFactory factory)
			: base((int)PermissionTopicEnum.Scholarship, factory) { }

		#region Collection methods
		[HttpPost]
		[Route("collection/filter")]
		public object Filter(ScholarshipFilter fltr)
		{
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}

		[HttpGet]
		[Route(@"", Order = -1)]		//add the order to 'steal' the route from the base class
		public object FilterGet([FromUri] ScholarshipFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new ScholarshipFilter();
			}
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}

		[HttpPost]
		[Route("collection/table")]
		public object Table(Models.ScholarshipTableBindingModel model)
        {
            ScholarshipFilter fltr = model.filter;
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Table(model.row, model.col, fltr);
        }

        [HttpPost]
		[Route("collection/geo")]
		public object GeoData(StudentFilter fltr)
        {
            return Factory.Student().Geo("POINT", fltr); //.ResultSet;
        }
		#endregion

		#region Scholarship CRUD methods overrides
		// TableMaintenanceController exposes EF implentations of the CRUD operatios
		// using the DynamicBinder<T> as intermediate wrapper
		// override read to return a richer structure including related tables 
		
		[HttpGet]
        [Route(@"{scholarshipID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Scholarship, Softwords.Web.PermissionAccess.Read)]
        public override async Task<HttpResponseMessage> Read(int? scholarshipID)
        {
			//AccessControl(scholarshipID);
			var scholarship = await Ds.Read(scholarshipID);
			HttpResponseMessage response = Request.CreateResponse<IDataResult>(HttpStatusCode.OK, scholarship);
			return response;
        }

		// Route and Http attributes are not required, since attribute inheritance is in operation
		// refer to comments in TableMainteanceController
		[PineapplesPermission(PermissionTopicEnum.Scholarship, Softwords.Web.PermissionAccess.WriteX)]
		public async override Task<HttpResponseMessage> Create(DynamicBinder<Scholarship> binder)
		{
			try
			{
				var scholarship = await binder
				   .Identity((ClaimsIdentity)User.Identity)
				   .Context(Factory.Context)
				   .Create();
				return await Read(scholarship.schoID);

			}

			catch (System.Data.SqlClient.SqlException ex)
			{
				// return the object as the body
				var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
				throw new HttpResponseException(resp);
			}
		}

		// override inherits routes
		[PineapplesPermission(PermissionTopicEnum.Scholarship, Softwords.Web.PermissionAccess.Write)]
		public async override Task<DynamicBinder<Scholarship>> Update(int? ID, DynamicBinder<Scholarship> binder)
		{
			try
			{
				Scholarship sch = Factory.Context.Scholarships
									.Where(s => s.schoID == ID)
									.Single();
				await binder
					.Identity((ClaimsIdentity)User.Identity)
					.Context(Factory.Context)
					.Update(sch);
				return binder;

			}

			catch (System.Data.SqlClient.SqlException ex)
			{
				// return the object as the body
				var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
				throw new HttpResponseException(resp);
			}


		}
		#endregion

		#region Create paths
		[HttpGet]
		[Route(@"new")]
		[PineapplesPermission(PermissionTopicEnum.Scholarship, Softwords.Web.PermissionAccess.Read)]
		public async Task<HttpResponseMessage> NewScholarship(Guid? studentID=null)
		{
			if (studentID == null)
			{
				var scholarship = await Ds.Read(0);
				HttpResponseMessage response = Request.CreateResponse<IDataResult>(HttpStatusCode.OK, scholarship);
				return response;
			}
			return await FromStudent((Guid)studentID);

		}

		[HttpGet]
		[Route(@"~/api/students/{studentID:guid}/scholarships/new")]
		[Route(@"new/{studentID:guid}")]
		[TablePermission(Softwords.Web.TableOperation.Update)]
		[PineapplesPermission(PermissionTopicEnum.Scholarship, Softwords.Web.PermissionAccess.WriteX)]
		public async Task<HttpResponseMessage> FromStudent(Guid studentID)
		{
			var scholarship = await Ds.NewFromStudent(studentID);
			HttpResponseMessage response = Request.CreateResponse<IDataResult>(HttpStatusCode.OK, scholarship);
			return response;
		}
		#endregion

		#region audit refresh

		[HttpGet]
		[Route(@"{scholarshipID:int}/audit")]
		[PineapplesPermission(PermissionTopicEnum.Scholarship, Softwords.Web.PermissionAccess.Read)]
		public async Task<HttpResponseMessage> ReadAudit(int? scholarshipID)
		{
			//AccessControl(scholarshipID);
			var auditdata = await Ds.ReadAudit(scholarshipID);
			HttpResponseMessage response = Request.CreateResponse<IDataResult>(HttpStatusCode.OK, auditdata);
			return response;
		}
		#endregion
		#region AccessControl

		protected override void AccessControl(int? scholarshipID)
		{
			Ds.AccessControl((int)scholarshipID, (ClaimsIdentity)User.Identity);
		}
		#endregion

		private IDSScholarship Ds
        {
            get { return Factory.Scholarship(); }
        }
    }
}
