﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
//using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Drive.v3.Data;
using System.Xml.Linq;
using Pineapples.Data.Models;
using Newtonsoft.Json;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/schoolinspections")]
	[Authorize]
	public class SchoolInspectionsController : PineapplesApiController
    {
        public SchoolInspectionsController(DataLayer.IDSFactory factory) : base(factory) { }

		#region CRUD methods
		[HttpGet]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
        public object Read(int saID)
        {
			AccessControl(saID);
            return Ds.Read(saID);
        }
                
		#endregion


		#region SchoolInspection Collection methods
		[HttpPost]
        [Route("collection/filter")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
        public object Filter(SchoolInspectionFilter fltr)
        {
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
        }
		#endregion

		[HttpGet]
		[Route(@"")]   
		public object FilterGet([FromUri] SchoolInspectionFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new SchoolInspectionFilter();
			}
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}


		#region Access Control

		private void AccessControl(int qrID)
        {
           Ds.AccessControl(qrID, (ClaimsIdentity)User.Identity);
        }
        #endregion

        private IDSSchoolInspection Ds
        {
            get { return Factory.SchoolInspection(); }
        }

    }
}
