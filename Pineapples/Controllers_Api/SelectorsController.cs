﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;

namespace Pineapples.Controllers
{
	[RoutePrefix("api/selectors")]
	public class SelectorsController : PineapplesApiController
	{
		public SelectorsController(DataLayer.IDSFactory factory) : base(factory) { }

		[HttpPost]
		[Route("school")]
		public object School([FromBody] string search)
		{
			return Factory.Selectors().Schools(search);
		}

		[HttpPost]
		[Route("teacher")]
		public object Teacher([FromBody] string search)
		{
			return Factory.Selectors().Teachers(search);
		}

		[HttpPost]
		[Route("student")]
		public object Student([FromBody] string search , int? year=null, string schoolNo=null)
		{
			return Factory.Selectors().Students(search, year, schoolNo);
		}

		[HttpPost]
		[Route("examcandidate")]
		public object examCandidate([FromBody] string search, int? year = null, string examCode = null, string schoolNo = null)
		{
			return Factory.Selectors().ExamCandidates(search, year, examCode, schoolNo);
		}
	}
}
