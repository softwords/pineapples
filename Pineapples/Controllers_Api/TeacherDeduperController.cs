﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Models;
using Pineapples.Data;

using Softwords.Web;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/teacherdeduper")]
	[Authorize]
    public class TeacherDeduperController : PineapplesApiController
    {
        public TeacherDeduperController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpPost]
        [Route("collection/filter")]
        public object Filter(TeacherDeduperFilter fltr)
        {
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
        }

		[HttpGet]
		[Route(@"")]
		public object FilterGet([FromUri] TeacherDeduperFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new TeacherDeduperFilter();
			}
			return Ds.Filter(fltr);
		}

		[HttpGet]
		[Route("surveys/{id1}/{id2}")]
		public object Surveys(int id1, int id2)
		{
			//fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Surveys(id1,id2);
		}

		[HttpGet]
		[Route("merge/{targetID}/{sourceID}")]
		public object Merge(int targetID, int sourceID)
		{
			//fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Merge(targetID, sourceID);
		}
		#region AccessControl
		// removed?

		#endregion
		private IDSTeacherDeduper Ds
        {
            get { return Factory.TeacherDeduper(); }
        }
    }
}
