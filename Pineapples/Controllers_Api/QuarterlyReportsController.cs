﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/quarterlyreports")]
    public class QuarterlyReportsController : PineapplesApiController
    {
        public QuarterlyReportsController(DataLayer.IDSFactory factory) : base(factory) { }

        #region QuarterlyReport methods
        [HttpPost]
        [Route(@"")]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
        public HttpResponseMessage Create(QuarterlyReportBinder qr)
        {
            // validate that we are allowed to act on this school
            Factory.School().AccessControl((string)qr.definedProps["schNo"], (ClaimsIdentity)User.Identity);
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(qr, (ClaimsIdentity)User.Identity));
                return response;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
        public object Read(int qrID)
        {
            return Ds.Read(qrID);
        }
                
        [HttpPut]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
        public object Update(QuarterlyReportBinder qr)
        {
            try
            {
                return Ds.Update(qr, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }

        }

        [HttpDelete]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
        public object Delete([FromBody] QuarterlyReportBinder qr)
        {
            // TO DO
            AccessControl((int)qr.ID);
            return qr.definedProps;
        }
        #endregion

        #region QuarterlyReport Collection methods
        [HttpPost]
        [Route("collection/filter")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
        public object Filter(QuarterlyReportFilter fltr)
        {
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
        }

		[HttpGet]
		[Route(@"")]
		public object FilterGet([FromUri] QuarterlyReportFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new QuarterlyReportFilter();
			}
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}
		#endregion

		#region Access Control

		private void AccessControl(int qrID)
        {
            Factory.SchoolInspection().AccessControl(qrID, (ClaimsIdentity)User.Identity);
        }
        #endregion


        private IDSQuarterlyReport Ds
        {
            get { return Factory.QuarterlyReport(); }
        }

    }
}
