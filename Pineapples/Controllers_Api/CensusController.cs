﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using System.Threading.Tasks;

using Softwords.Web;
using Softwords.Web.Models;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Pineapples.Data.Models;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/census")]
	[Authorize]
	public class CensusController : TableMaintenanceController<Census, int>
    {
        public CensusController(DataLayer.IDSFactory factory) : base(factory) { 
        }

        [HttpGet]
        [Route(@"{surveyYear:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public override async Task<HttpResponseMessage>  Read(int surveyYear)
        {
            var result =  await Ds.Read(surveyYear);
            return Request.CreateResponse(result);
        }
        [HttpGet]
        [Route(@"current")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public async Task<HttpResponseMessage> ReadCurrent()
        {
            var result = await Ds.ReadCurrent();
            return Request.CreateResponse(result);
        }

        #region Survey Year create

         public override async Task<HttpResponseMessage> Create(Softwords.Web.Models.DynamicBinder<Census> binder)
        {
            var c = new Census();
            binder
                .Identity((ClaimsIdentity)User.Identity)
                .SetValues(c);
            var resp = await Ds.CreateCensus(c, (ClaimsIdentity)User.Identity);
            return Request.CreateResponse(resp);
        }

		#endregion


		#region Related SchoolType records
        [HttpPut]
        [Route(@"{surveyYear:int}/censusschooltypes/{stCode}")]
        public async Task<DynamicBinder<CensusSchoolType>> UpdateSchoolType(int surveyYear, string stCode, DynamicBinder<CensusSchoolType> binder)
        {
            try
            {
                // we'll use the version of bnder Update that expects an entity - becuase we have a complex key to navigate
                var cst = Factory.Context.CensusSchoolTypes.Find(surveyYear, stCode);
                return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Update(cst);

            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpPost]
        [Route(@"{surveyYear:int}/censusschooltypes/{stCode}")]
        [Route(@"{surveyYear:int}/censusschooltypes/")]
        public async Task<CensusSchoolType> CreateSchoolType(int surveyYear, string stCode, DynamicBinder<CensusSchoolType> binder)
        {
            try
            {
                // we'll use the version of bnder Update that expects an entity - becuase we have a complex key to navigate
                binder
                    .definedProps.Add("svyYear", surveyYear);
                return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Create();

            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        #endregion

        #region Related teacher qualification records
        [HttpPut]
        [Route(@"{surveyYear:int}/censusteacherquals/{id:int}")]
        [Route(@"~api/censusteacherquals/{id:int}")]
        public async Task<DynamicBinder<CensusTeacherQual>> UpdateTeacherQual(int surveyYear, int id, DynamicBinder<CensusTeacherQual> binder)
        {
            try
            {
                return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Update(id);

            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpPost]
        [Route(@"{surveyYear:int}/censusteacherquals")]
        public async Task<CensusTeacherQual> CreateTeacherQual(int surveyYear, DynamicBinder<CensusTeacherQual> binder)
        {
            try
            {
                binder
                    .definedProps.Add("svyYear", surveyYear);

                return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Create();
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }
        // support a direct route, as well as the hierarchical route
        // from census. The difference is that here we assume surveyYear is provided
        // when invoking from census, we take the year from the census
        [HttpPost]
        [Route(@"~api/censusteacherquals")]
        public async Task<CensusTeacherQual> CreateTeacherQual(DynamicBinder<CensusTeacherQual> binder)
        {
            try
            {
                return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Create();
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }
        [HttpDelete]
        [Route(@"{surveyYear:int}/censusteacherquals/{id:int}")]
        [Route(@"~api/censusteacherquals/{id:int}")]
        public async Task<CensusTeacherQual> CreateTeacherQual(int surveyYear, int id, DynamicBinder<CensusTeacherQual> binder)
        {
            try
            {
 
                return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Delete(id);
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route(@"{surveyYear:int}/effectiveteacherquals")]

        public async Task<Softwords.DataTools.IDataResult> EffectiveTeacherQuals(int surveyYear)
		{
            var resp = await Ds.EffectiveTeacherQuals(surveyYear);
            return resp;
		}

        #endregion  1300249268
        private IDSCensus Ds
        {
            get { return Factory.Census(); }
        }
    }
}
