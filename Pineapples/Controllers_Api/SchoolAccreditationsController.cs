﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
//using System.IO;
using System.Threading;
using Softwords.DataTools;
using System.Threading.Tasks;
using Google.Apis.Drive.v3.Data;
using System.Xml.Linq;
using Pineapples.Data.Models;
using Newtonsoft.Json;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/schoolaccreditations")]
	[Authorize]
	public class SchoolAccreditationsController : PineapplesApiController
    {
        public SchoolAccreditationsController(DataLayer.IDSFactory factory) : base(factory) { }

        #region SchoolAccreditation methods
        [HttpPost]
        [Route(@"")]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
        public HttpResponseMessage Create(SchoolAccreditationBinder sa)
        {
            // validate that we are allowed to act on this school
            Factory.School().AccessControl((string)sa.definedProps["schNo"], (ClaimsIdentity)User.Identity);

            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(sa, (ClaimsIdentity)User.Identity));
                return response;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
        public object Read(int saID)
        {
			AccessControl(saID);
			IDataResult result = Ds.Read(saID);
			return result;
			//var resultset = (Dictionary<string, object>)(result.ResultSet);
			//string xmlstr = resultset.Values.ToArray()[0].ToString();
			//var xmldoc = (System.Xml.Linq.XDocument)(result.ResultSet);
			//var jsonobject = JsonConvert.SerializeXNode(xmldoc);
			//return RawJsonResponse(jsonobject);
		}
                
        [HttpPut]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
        public object Update(int saID, [FromBody] SchoolAccreditationBinder sa)
        {
			AccessControl(saID);
            try
            {
                return Ds.Update(sa, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpDelete]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.WriteX)]
        public object Delete(int saID, [FromBody] SchoolAccreditationBinder sa)
        {
			// TO DO
			AccessControl(saID);
			return sa.definedProps;
        }
		#endregion

		//==============================================================================
		#region methods from Android app Xml upload

		/// <summary>
		/// Get a list of available school accreditations
		/// </summary>
		/// <param name="fileID"></param>
		/// <returns></returns>
		/// 
		private  async Task<FileList> GetProcessableFiles(bool includeUpdated)
		{
			// open the google drive
			var service = GoogleDriveManager.getDriveService();
			// set up the google file search object
			List<string> q = new List<string>();
			q.AddPropertiesSearch("DocumentKind", "SCHACCR");
			if (!includeUpdated)
			{
				string s = @"(not (properties has { key='Uploaded' and value = 'POSITIVE'}))";
				q.Add(s);
			}
			// execute the search and return the results
			return await GoogleDriveManager.FilesQuery(q);
		}

		[HttpGet]
		[Route(@"fromxml/{fileID}")]
		public async Task<XDocument> LoadFromXml(string fileID)
		{
			// get the downloaded files as an xml object
			// get the school number and assessment type from the properties metadata
			var service = GoogleDriveManager.getDriveService();
			// get the metadata, including the google file properties
			File file = await GoogleDriveManager.getFileMetaData(fileID);
			// check the properties of the file are complete and correct
			// --- is there a school no?
			// --- valid inspection type

			var stream = await GoogleDriveManager.getFileContent(fileID);
			XDocument vermdata = XDocument.Load(stream);

			// now assemble the School Inspection
			string schoolNo = file.Properties["schoolNo"];
			var binder = new Softwords.Web.Models.DynamicBinder<SchoolAccreditation>();
			var context = Factory.Context;
			dynamic dyn = binder.Properties();
			dyn.schoolNo = file.Properties["schoolNo"];

			//file.Properties
			//binder.LoadValues
			return vermdata;
		}
		#endregion

		#region SchoolAccreditation Collection methods
		[HttpPost]
        [Route("collection/filter")]
        [PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
        public object Filter(SchoolAccreditationFilter fltr)
        {
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
        }

		[HttpGet]
		[Route(@"")]      
		public object FilterGet([FromUri] SchoolAccreditationFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new SchoolAccreditationFilter();
			}
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}

		#endregion

		#region Access Control
		private void AccessControl(int qrID)
        {
            Factory.SchoolInspection().AccessControl(qrID, (ClaimsIdentity)User.Identity);
        }
        #endregion

        private IDSSchoolAccreditation Ds
        {
            get { return Factory.SchoolAccreditation(); }
        }

    }
}
