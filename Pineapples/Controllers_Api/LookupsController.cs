﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;

namespace Pineapples.Controllers
{
	public class LookupsController : PineapplesApiController
	{
		public LookupsController(DataLayer.IDSFactory factory) : base(factory) { }

		[HttpGet]
		[Route("api/lookups/collection/core")]
		public object CoreLookups()
		{
			var lookups = Ds.CoreLookups();
			// add any web config we may need to sysParams
			var sysParams = lookups.Tables["sysParams"];
			string allow = AppSetting("allowPublicAccess");
			int i = (allow == "1" || allow == "true" ? 1 : 0);
			sysParams.Rows.Add(new object[] { "allowPublicAccess", allow, i });

			return lookups;
		}

		[HttpGet]
		[Route("api/lookups/collection/pa")]
		public object PaLookups()
		{
			return Ds.PaLookups();
		}

		[HttpGet]
		[Route("api/lookups/collection/censusworkbook")]
		public object CensusWorkbookLookups()
		{
			return Ds.CensusWorkbookLookups();
		}

		[HttpGet]
		[Route("api/lookups/collection/censuspdf")]
		public object CensusPDFLookups()
		{
			return Ds.CensusPDFLookups();
		}

		[HttpGet]
		[Route("api/lookups/collection/student")]
		public object StudentLookups()
		{
			return Ds.StudentLookups();
		}

		[HttpGet]
		[Route("api/lookups/collection/scholarship")]
		public object ScholarshipLookups()
		{
			return Ds.ScholarshipLookups();
		}

		[HttpGet]
		[Route("api/lookups/collection/findata")]
		public object FinDataLookups()
		{
			return Ds.FinDataLookups();
		}

		[HttpGet]
		[Route("api/lookups/getlist/{name}")]
		public object GetList(string name)
		{
			string[] lookups = new string[1] { name };
			return Ds.getLookupSets(lookups);
		}

		[HttpPost]
		[Route("api/lookups/{listName}/{code}")]
		public object Save(string listName, string code, Pineapples.Data.Models.LookupEntry entry)
		{

			return null;
		}

		/// <summary>
		/// Return info on the loookup lists that can be edited
		/// To do - push this into the database
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("api/lookups/editabletables")]
		public object editableTables()
		{
			//TO DO - implement this in the database similar to desktop pineapples
			List<string> tables = new List<string>();

			tables.Add("Authorities");
			tables.Add("AuthorityGovts");
			tables.Add("AuthorityTypes");
			tables.Add("Banks");
			tables.Add("BookTGs");
			tables.Add("BuildingMaterials");
			tables.Add("BuildingSizeClasses");
			tables.Add("BuildingSubTypes");
			tables.Add("BuildingTypes");
			tables.Add("BuildingValuationClasses");
			tables.Add("CensusColumns");
			tables.Add("ChildProtections");
			tables.Add("CivilStatusValues");
			tables.Add("ConditionCodes");
			tables.Add("CostCentres");
			tables.Add("Currencies");
			tables.Add("DistanceCodes");
			tables.Add("EducationLevels");
			tables.Add("EducationLevelsAlt");
			tables.Add("EducationLevelsAlt2");
			tables.Add("EducationPaths");
			tables.Add("EducationPathLevels");
			tables.Add("EducationPrograms");
			tables.Add("EducationSectors");
			tables.Add("Ethnicities");
			tables.Add("EthnicityGroups");
			tables.Add("ExamTypes");
			tables.Add("FundingSources");
			tables.Add("FundingSourceGroups");
			tables.Add("ISCEDFieldGroups");
			tables.Add("ISCEDFieldGroupsBroad");
			tables.Add("ISCEDFields");
			tables.Add("ISCEDLevels");
			tables.Add("ISCEDLevelsSub");
			tables.Add("InspectionTypes");
			tables.Add("LandOwnerships");
			tables.Add("LandUses");
			tables.Add("Languages");
			tables.Add("LanguageGroups");
			tables.Add("Levels");
			tables.Add("LocalElectorates");
			tables.Add("MaterialTypes");
			tables.Add("MetaResourceCategories");
			tables.Add("MetaResourceDefinitions");
			tables.Add("NationalElectorates");
			tables.Add("Nationalities");
			tables.Add("Navigations");
			tables.Add("Organizations");
			tables.Add("PayAllowances");
			tables.Add("PNAReasons");
			tables.Add("PrimaryTransitions");
			tables.Add("Publishers");
			tables.Add("PupilTableTypes");
			tables.Add("QualityIssues");
			tables.Add("RandMs");
			tables.Add("Regions");
			tables.Add("RoleGrades");
			tables.Add("RoomFunctions");
			tables.Add("RoomTypes");
			tables.Add("RubbishDisposals");
			tables.Add("SalaryLevels");
			tables.Add("SalaryPoints");
			tables.Add("SchoolBuildingTypes");
			tables.Add("SchoolClasses");
			tables.Add("SchoolLinkTypes");
			tables.Add("SchoolRegistrations");
			tables.Add("SecondaryTransitionItems");
			tables.Add("SecondaryTransitions");
			tables.Add("ServiceTypes");
			tables.Add("Shifts");
			tables.Add("SiteHosts");
			tables.Add("StudentLinkTypes");
			tables.Add("Subjects");
			tables.Add("TeacherExamTypes");
			tables.Add("TeacherHousingTypes");
			tables.Add("TeacherLinkTypes");
			tables.Add("TeacherQuals");
			tables.Add("TeacherRoles");
			tables.Add("TeacherStatusValues");
			tables.Add("TeacherTrainingTypes");
			tables.Add("ToiletTypes");
			tables.Add("WaterSupplyTypes");
			tables.Add("WorkItemProgressValues");
			tables.Add("WorkItemTypes");
			tables.Add("WorkOrderStatusValues");

			return tables;
		}

		[HttpGet]
		[Route("api/lookups/spededitabletables")]
		public object SpEdeditableTables()
		{
			//TO DO - implement this in the database similar to desktop pineapples
			List<string> tables = new List<string>();

			tables.Add("SpEdEnvironments");
			tables.Add("SpEdAccommodations");
			tables.Add("SpEdAssessmentTypes");
			tables.Add("SpEdEvaluationEventTypes");
			tables.Add("SpEdExits");
			tables.Add("SpEdEnglishLearnerModes");
			tables.Add("Disabilities");

			return tables;

		}

		[HttpGet]
		[Route("api/lookups/scholarshipeditabletables")]
		public object Scholarshipeditabletables()
		{
			//TO DO - implement this in the database similar to desktop pineapples
			List<string> tables = new List<string>();

			tables.Add("ScholarshipTypes");
			tables.Add("ScholarshipStatus");
			tables.Add("FieldsOfStudy");
			tables.Add("FieldOfStudyGroups");
			tables.Add("TertiaryGrades");
			tables.Add("ScholarshipRounds");
			tables.Add("Nationalities");
			tables.Add("CivilStatusValues");
			tables.Add("ScholarshipLinkTypes");
			tables.Add("ScholarshipInstitutions");

			return tables;

		}
		private IDSLookups Ds
		{
			get { return Factory.Lookups(); }
		}
	}
}
