﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/exams")]
	[Authorize]
	public class ExamsController : PineapplesApiController
    {
        public ExamsController(DataLayer.IDSFactory factory) : base(factory) { }

        #region Collection methods
        [HttpPost]
        [Route("collection/filter")]
        public object Filter(ExamFilter fltr)
        {
            return Ds.Filter(fltr);
        }


		[HttpGet]
		[Route(@"")]
		public object FilterGet([FromUri] ExamFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new ExamFilter();
			}
			return Ds.Filter(fltr);
		}
		#endregion

		[HttpPost]
        [Route("schoolcompare")]
        public object SchoolCompare(ExamFilter fltr)
        {
            if ( fltr.ExamID == null )
            {
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError);
                throw new HttpResponseException(resp);
            }
            return Factory.Exam().SchoolExamData(fltr.SchoolNo, (int)fltr.ExamID);
        }

        [HttpPost]
        [Route("schoollist")]
        public object SchoolList(string schoolNo)
        {
            return Factory.Exam().SchoolExamList(schoolNo);
        }

        [HttpGet]
        [Route("candidate")]

        #region upload
        [HttpPost]
        [Route(@"upload")]
        public async Task<object> Upload()
        {

            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartFormDataStreamProvider(AppDataPath);

            var files = await Request.Content.ReadAsMultipartAsync(provider);

            dynamic result = null;
            foreach (MultipartFileData fd in files.FileData)
            {
                string filename = fd.Headers.ContentDisposition.FileName;
                filename = filename.Replace("\"", "");          // tends to com back with quotes around
                string ext = Path.GetExtension(filename);

                IExamHandler handler = ExamHandlerFactory.GetHandler(ext);
                
                using (FileStream fstrm = new FileStream(fd.LocalFileName, System.IO.FileMode.Open))
                {

                    Guid g = Providers.FileDB.Store(fstrm, ext);
                    fstrm.Position = 0;
                    result = await handler.LoadFile(fstrm, g, fd, Factory);
                   
                }
                // delete the local file, or else app_data is strewn with BodyParts! 'using' ensures the stream is disposed
                System.IO.File.Delete(fd.LocalFileName);

                // Need to support the FSM / RMIU way of defining year (2018-2019)
                // as well as a simple value, for Kiribati
                int examYear = result.examYear.Length > 4 ?
                                    Convert.ToInt32(((string)(result.examYear)).Substring(0, 4)) + 1 :
                                    (result.examYear.Length == 4 ?
                                    Convert.ToInt32((string)(result.examYear)) : 0); 
                string examCode = result.examCode;
                // as well we'll check if there is already a record
                Data.Models.Exam ex = Factory.Context.Exams.FirstOrDefault(exam => exam.exCode == examCode && exam.exYear == examYear);
                result.exam = ex;
            }
            return result;
        }

        /// <summary>
        /// process an uploaded file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"process/{fileId}")]
        public async Task<Softwords.DataTools.IDataResult> Process(string fileId)
        {
            try
            {
                string ext = FileDB.GetExtension(fileId);
                IExamHandler handler = ExamHandlerFactory.GetHandler(ext);
                IDataResult dr = await handler.Process(fileId, Factory, User.Identity.Name);
                // check whether there are validation errors, if so, kill the upload file
                try
                {
                    System.Data.DataSet ds = dr.ResultSet as System.Data.DataSet;
                    System.Data.DataTable dt = ds.Tables["validationErrors"];
                    if (dt.Rows.Count > 0)
                    {
                        CancelProcess(fileId);
                    }
                }
                catch { }
                return dr;

            }
            catch(Exception ex)
			{
                // clean up this file we wont ever access it again if the process has failed
                try
				{
                    CancelProcess(fileId);
				}
                finally
				{
                    throw ex;
				}
			}

        }

        /// <summary>
        /// If the process fails validation tests,he file is deleted.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(@"cancelprocess/{fileId}")]
        public void CancelProcess(string fileId)
        {
            Guid g;
            if (Guid.TryParse(fileId, out g))
            {
                // cancel should only be allowed to delete a file that is not referenced in the Documents table
                // so, id we find the key FileId in the table Documents - do not remove
                // otherwise, we effectively have a url that lets you trash the FileDb
                if (!Factory.Context.Documents.Any(doc => doc.docID == g))
                {
                    FileDB.Remove(fileId);
                }
            }
        }



 
        #endregion

        #region Exam CRUD methods
        [HttpGet]
        [Route(@"{examID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Exam, PermissionAccess.Read)]
        public object Read(int examID)
        {
            return Ds.Read(examID); //, ((ClaimsIdentity)User.Identity).hasPermission((int)PermissionTopicEnum.School, PermissionAccess.ReadX));
        }

        [HttpPost]
        [Route(@"")]
        // [Route(@"{examID}")] this route can never make sense because examID is an identity and so
        // cannot be specified by the client
        [PineapplesPermission(PermissionTopicEnum.Exam, Softwords.Web.PermissionAccess.WriteX)]
        public object Create(ExamBinder examIdentity)
        {
            try
            {
                return Ds.Create(examIdentity, (ClaimsIdentity)User.Identity);
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpPut]
        [Route(@"{examID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Exam, Softwords.Web.PermissionAccess.Write)]
        public object Update(ExamBinder examIdentity)
        {
            try
            {
                return Ds.Update(examIdentity, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }
        #endregion

        private IDSExam Ds
        {
            get { return Factory.Exam(); }
        }
    }
}
