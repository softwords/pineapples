﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using System.Threading.Tasks;


using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Softwords.Web;

using Softwords.DataTools;
using Softwords.Web.Models;
using System.Data.Entity;
using System.Reflection;
using Pineapples.Data.Models;


namespace Pineapples.Controllers
{
	[RoutePrefix("api/specialedstudents")]
	public class SpecialEdStudentsController : TableMaintenanceController<SpecialEdStudent, Guid?> //PineapplesApiController
	{
		public SpecialEdStudentsController(DataLayer.IDSFactory factory)
			: base((int)PermissionTopicEnum.SpecialEd, factory) { }

		#region Collection methods
		[HttpPost]
		[Route("collection/filter")]
		public object Filter(SpecialEdStudentFilter fltr)
		{
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}

		// GET version
		[HttpGet]
		[Route(@"", Order = -1)]    //add the order to 'steal' the route from the base class
		public object FilterGet([FromUri] SpecialEdStudentFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new SpecialEdStudentFilter();
			}
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
			//  return fltr.Search();
		}


		[HttpPost]
		[Route("collection/geo")]
		public object GeoData(StudentFilter fltr)
		{
			return Factory.Student().Geo("POINT", fltr); //.ResultSet;
		}
		#endregion

		#region Student CRUD methods
		[HttpPost]
		[Route(@"")]
		// [Route(@"{studentID}")] this route can never make sense because studentID is an identity and so
		// cannot be specified by the client
		[PineapplesPermission(PermissionTopicEnum.SpecialEd, Softwords.Web.PermissionAccess.WriteX)]
		public object Create(StudentBinder student)
		{
			try
			{
				return Ds.Create(student, (ClaimsIdentity)User.Identity);
			}
			catch (System.Data.SqlClient.SqlException ex)
			{
				// return the object as the body
				var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
				throw new HttpResponseException(resp);
			}
		}

		[HttpGet]
		[Route(@"{stuID:guid}")]
		[PineapplesPermission(PermissionTopicEnum.SpecialEd, Softwords.Web.PermissionAccess.Read)]
		public async override Task<HttpResponseMessage> Read(Guid? stuID)
		{
			AccessControl(stuID);
			IDataResult result = await Ds.Read(stuID); //, ((ClaimsIdentity)User.Identity).hasPermission((int)PermissionTopicEnum.SpecialEd, PermissionAccess.ReadX));
			HttpResponseMessage resp = Request.CreateResponse(result);
			return resp;
		}

		//[HttpPut]
		//[Route(@"{stuID:guid}")]
		//[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
		//public object Update(StudentBinder student)
		//{
		//    try
		//    {
		//        return Ds.Update(student, (ClaimsIdentity)User.Identity).definedProps;
		//    }
		//    catch (System.Data.SqlClient.SqlException ex)
		//    {
		//        // return the object as the body
		//        var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
		//        throw new HttpResponseException(resp);
		//    }
		//}

		//[HttpPut]
		//[Route(@"{ID}")]
		////[TablePermission(Softwords.Web.TableOperation.Update)]
		//public async Task<DynamicBinder<Student>> Update(Guid? ID, DynamicBinder<Student> binder)
		//{
		//	try
		//	{
		//		return await binder
		//			.Identity((ClaimsIdentity)User.Identity)
		//			.Context(Factory.Context)
		//			.Update(ID);

		//	}

		//	catch (System.Data.SqlClient.SqlException ex)
		//	{
		//		// return the object as the body
		//		var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
		//		throw new HttpResponseException(resp);
		//	}
		//}


		[HttpDelete]
		[Route(@"{stuID:guid}")]
		[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
		public object Delete([FromBody] StudentBinder student)
		{
			// TO DO
			return student.definedProps;
		}
		#endregion

		#region AccessControl

		protected override void AccessControl(Guid? studentID)
		{
			Ds.AccessControl((Guid)studentID, (ClaimsIdentity)User.Identity);
		}
		#endregion

		private IDSSpecialEdStudent Ds
		{
			get { return Factory.SpecialEdStudent(); }
		}
	}
}
