﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using System.Threading.Tasks;

using System.Collections.Generic;
using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Softwords.Web;

using Softwords.DataTools;
using Softwords.Web.Models;
using System.Data.Entity;
using System.Reflection;
using Pineapples.Data.Models;


namespace Pineapples.Controllers
{
	[RoutePrefix("api/students")]
	[Authorize]
	public class StudentsController : TableMaintenanceController<Student, Guid?> //PineapplesApiController
	{
		public StudentsController(DataLayer.IDSFactory factory)
			: base((int)PermissionTopicEnum.Student, factory) { }

		#region Collection methods
		[HttpPost]
		[Route("collection/filter")]
		public object Filter(StudentFilter fltr)
		{
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}

		[HttpGet]
		[Route(@"", Order = -1)]		//add the order to 'steal' the route from the base class
		public object FilterGet([FromUri] StudentFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new StudentFilter();
			}
			fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Filter(fltr);
		}

		[HttpPost]
		[Route("collection/table")]
		public object Table(Models.StudentTableBindingModel model)
        {
            StudentFilter fltr = model.filter;
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Table(model.row, model.col, fltr);
        }

        [HttpPost]
		[Route("collection/geo")]
		public object GeoData(StudentFilter fltr)
        {
            return Factory.Student().Geo("POINT", fltr); //.ResultSet;
        }
		#endregion

		#region Student CRUD methods overrides
		// TableMaintenanceController exposes EF implentations of the CRUD operatios
		// using the DynamicBinder<Student> as intermediate wrapper
		// override read to return a richer structure including related tables 
		
		[HttpGet]
        [Route(@"{stuID:guid}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public override async Task<HttpResponseMessage> Read(Guid? stuID)
        {
			AccessControl(stuID);
			var student = await Ds.Read(stuID);
			HttpResponseMessage response = Request.CreateResponse<IDataResult>(HttpStatusCode.OK, student);
			return response;
        }


		#endregion

		#region AccessControl

		protected override void AccessControl(Guid? studentID)
		{
			Ds.AccessControl((Guid)studentID, (ClaimsIdentity)User.Identity);
		}
		#endregion

		private IDSStudent Ds
        {
            get { return Factory.Student(); }
        }
    }
}
