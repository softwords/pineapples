﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using Pineapples.Data.Models;
using System.Threading.Tasks;
using Softwords.Web;
using Softwords.Web.Models;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/scholarshiplinks")]
	[Authorize]
	public class ScholarshipLinksController : PineapplesApiController
    {
        public ScholarshipLinksController(DataLayer.IDSFactory factory) : base(factory) { }

        #region ScholarshipLink methods


        [HttpPost]
        [Route(@"")]
        [Route(@"{linkID:int}")]
        //[PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Write)]
        public HttpResponseMessage Create(DynamicBinder<ScholarshipLink> binder)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(binder, (ClaimsIdentity)User.Identity));
                return response;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpPut]
        [Route(@"{linkID:int}")]
        //[PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Write)]
        public async Task<HttpResponseMessage> Update(DynamicBinder<ScholarshipLink> binder)
        {
            try
            {
				await Ds.Update(binder, (ClaimsIdentity)User.Identity);
				HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, binder.definedProps);
				response.Headers.Add("X-Tag", "scholarshiplinks");

				return response;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpGet]
        [Route(@"{linkID:int}")]
        //[PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Read)]
        public object Read(int linkID)
        {
            return Ds.Read(linkID);
        }

        [HttpDelete]
        [Route(@"{linkID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Write)]
        public object Delete(int linkID)
        {
            // its CreateTagged (because its a view) so we can't just Find here...
            Pineapples.Data.Models.ScholarshipLink link = Factory.Context.ScholarshipLinks.FirstOrDefault( e => e.lnkID == linkID);
            if (link == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            string clientVersion = null;
            if (Request.Headers.IfMatch != null)
            {
                clientVersion = Request.Headers.IfMatch.ToString();
                {
                    if (Convert.ToBase64String(link.pRowversion.ToArray()) != clientVersion)
                    {
                        // optimistic concurrency error
                        // we return the most recent version of the record

                        throw ConcurrencyException.Make("pRowversion", link);
                    }
                }
            }
                Providers.FileDB.Remove(link.docID.ToString());
                Factory.Context.ScholarshipLinks.Remove(link);
                Factory.Context.SaveChanges();
                return link;
            }
		#endregion

		#region Upload
		/// <summary>
		/// Upload a file, and link to a teacher. The file can be anything - a photo, image, document, pdf spreadsheet etc
		/// If it is an image type, it will be rendered as such in the web client.
		/// The descriptive info about the file is passed as Json, in the form field called model.
		/// </summary>
		/// <returns>the document record</returns>
		[HttpPost]
        [Route(@"upload")]
        public async Task<Softwords.DataTools.IDataResult> Upload()
        {
            Softwords.DataTools.IDataResult result = null;

            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartFormDataStreamProvider(AppDataPath);

            var files = await Request.Content.ReadAsMultipartAsync(provider);

            foreach (MultipartFileData fd in files.FileData)
            {
                Guid g;

                string filename = fd.Headers.ContentDisposition.FileName;
                filename = filename.Replace("\"", "");          // tends to com back with quotes around
                string ext = System.IO.Path.GetExtension(filename);

                using (System.IO.FileStream fstrm = new System.IO.FileStream(fd.LocalFileName, System.IO.FileMode.Open))
                {
                    g = Providers.FileDB.Store(fstrm, ext);
                }
                // delete the local file, or else app_data is strewn with BodyParts! 'using' ensures the stream is disposed
                System.IO.File.Delete(fd.LocalFileName);

                // add the Teacher Link record in Pineapples
                //
                // rather than passing the supporting data in other form fields, a single field is populated with the Json version
                // of these values. The client makes this using Json.Stringify(). Thus we don't need any new techniques to get at the 
                // url-form-encoded data - just deserialize the one piece of Json
                DynamicBinder<ScholarshipLink> binder = Newtonsoft.Json.JsonConvert.DeserializeObject<DynamicBinder<ScholarshipLink >> (provider.FormData["model"]);

                object scholarshipID = binder.definedProps["schoID"];
                if (scholarshipID == null)
                {
                    throw RecordNotFoundException.Make(scholarshipID);
                }

                // the ID fields needs to be there, so it will be populated from the binder fromDB call
                binder.definedProps.Add("docID", g);
                binder.ID = null;
                try
                {
                    result = await Ds.Create(binder, (ClaimsIdentity)User.Identity);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    // return the object as the body
                    var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    throw new HttpResponseException(resp);
                }
            }
            return result;      // only the last one? but we expect only one at a time.....?
        }
		#endregion

		private IDSScholarshipLink Ds
        {
            get { return Factory.ScholarshipLink(); }
        }
    }
}
