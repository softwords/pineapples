﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
//using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Google.Apis.Drive.v3.Data;
using System.Xml.Linq;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace Pineapples.Controllers
{
	/// <summary>
	/// https://localhost:44301/google/1CUdq8bGBq4DoGw63XafDRW0kqUg-iAQn
	/// </summary>
	[RoutePrefix("api/cloudfiles")]
	[Authorize]
	public class CloudfilesController : PineapplesApiController
	{
		public CloudfilesController(DataLayer.IDSFactory factory) : base(factory) { }


		[HttpGet]
		[Route("{fileID}/metadata")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
		public async Task<File> getFileMetaData(string fileID)
		{
			return await GoogleDriveManager.getFileMetaData(fileID);
		}

		[HttpGet]
		[Route("")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
		public async Task<IList<Google.Apis.Drive.v3.Data.File>> getFiles()
		{
			CloudfileFilter fltr = new CloudfileFilter();

			return await getFiles(fltr);
		}

		[HttpPost]
		[Route("")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
		public async Task<IList<Google.Apis.Drive.v3.Data.File>> getFiles(CloudfileFilter fltr)
		{

			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();

			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();
			if (ContextFolderId.Count == 0)
			{
				// no context folder was found - throw a bad request
				throw new HttpException((int)HttpStatusCode.BadRequest, $"No folder was found for {Context}");
			}

			fltr = fltr ?? new CloudfileFilter();
			fltr.folder = (fltr.filesOrPhotos.ToLower() == "photos" ? PhotoFolderId : ContextFolderId);
			listRequest.Q = fltr.ToString();
			listRequest.PageSize = 1000;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, parents, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";
			//listRequest.Q = "mimeType = 'image/png'";
			//listRequest.Q = "(properties has { key='Uploaded' and value = 'POSITIVE'})";
			//listRequest.Q = "kind='drive#folder'";
			// List files.
			var fileList = await listRequest.ExecuteAsync();
			return fileList.Files;
		}

		/// <summary>
		/// When the client has approved a file, load that into 
		/// SchoolInspections
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("{fileId}/approve")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
		public async Task<object> ApproveFile(string fileId)
		{
			// first get the Xml as an Xdocument

			var stream = await GoogleDriveManager.getFileContent(fileId);
			XDocument x = XDocument.Load(stream);

			// and get the metadata from the google file properties
			var file = await GoogleDriveManager.getFileMetaData(fileId);

			// Validations:
			// 1) the survey must be complete (surveyCompleted="POSITIVE")
			// 2) the school no must be present and valid
			// 3) must not be uploaded already.
			// 4) must not have an identical survey (same school and same surveyTag) loaded (different from simply checking inspID)

			// Validation 1)
			if (file.Properties["surveyCompleted"] != "POSITIVE")
			{
				throw new HttpException((int)HttpStatusCode.Forbidden, "Cannot approve a file that is not Completed.");
			}

			// Validation 2)
			// Is this needed?

			// Validation 3)
			if (file.Properties.ContainsKey("uploaded"))
			{
				// do nothing ? 
				// it's ok to reupload - will overwrite based on fileId which is held in inspSource
			}

			//the following values are supplied as properties for each of searching/filtering
			// schoolNo - the school no.
			// type - the assessment/review/survey type - this is defined in lkpInspectionTypes
			// createDate - date on which the survey was conducted, 
			string schoolNo = x.Root.Element("schoolNo").Value;
			string type = x.Root.Element("type").Value;
			//string dateformat = "yyyy-MM-dd HH:mm:ss.f 'GMT'zzz";
			string dateformat = "yyyy-MM-ddTHH:mm:ss.fffZ";
			DateTime dt = DateTime.ParseExact(x.Root.Element("createDate").Value
							, dateformat
							, System.Globalization.CultureInfo.InvariantCulture).ToUniversalTime();
			string surveyTag = x.Root.Element("surveyTag").Value;
			// 18 9 2019 some confusion abut what 'completion date' may be recorded in the survey or metadata properties
			XElement srcEndDate = x.Root.Element("completeDate");
			DateTime dtEnd = dt; // if completedate is not there use createdate
			if (srcEndDate != null)
			{
				dtEnd = DateTime.ParseExact(x.Root.Element("completeDate").Value
								, dateformat
								, System.Globalization.CultureInfo.InvariantCulture).ToUniversalTime();
			}
			Pineapples.Data.DB.PineapplesEfContext context = Factory.Context;


			// Validation 4)
			// The reason for including a lower level SQL query here is because of the need
			// to query data from an XML column in SQL Server. This is not supported in Entity Framework
			// at the moment https://stackoverflow.com/questions/1103620/entity-framework-query-xml
			// The alternative to below was pulling the XML data as a string and converting to XDocument
			// or having changes on the SQL database putting the surveyTag on a column.
			string query = $@"SELECT [inspID] ID
									,[schNo]
									,[schName]
									,[PlannedDate]
									,[StartDate]
									,[EndDate]
									,[Note]
									,[InspectedBy]
									,[InspectionSetID]
									,[InspectionContent]
									,[InspectionSetName]
									,[InspectionYear]
									,[InspectionResult]
									,[SourceId]
									,[InspTypeCode]
									,[InspectionType]
									,[pCreateUser]
									,[pCreateDateTime]
									,[pEditUser]
									,[pEditDateTime]
									,[pRowversion]
									,[pCreateTag]
								FROM pInspectionRead.SchoolInspections SI
								WHERE SI.schNo = '{schoolNo}' AND SI.InspectionContent.value('(/survey/surveyTag)[1]', 'nvarchar(20)') = '{surveyTag}'";

			var inspDuplicate = context.SchoolInspections.SqlQuery(query).FirstOrDefault();

			// check if there is already an inspection for this fileId
			// Not the same as validation 4) above (though used for validation 4).
			// This is the case of trying to reload an previously loaded
			// inspection which is ok and simply updates it.
			var insp = context.SchoolInspections.FirstOrDefault(y => y.SourceId == fileId);

			// Check to make sure there was not a previously loaded accreditation *from a duplicate cloud file*
			// This would have a same schoolID and surveyTag and would have already been loaded.
			// Another instance where this can now happen is when loaded files have been migrated from the previously
			// shared Google Service Accounts. They are in effect duplicates with different google IDs and thus
			// not worth the effort to allow "reloading".
			if (inspDuplicate != null && insp == null)
			{
				string err = $@"There is already a loaded inspection for school ID {schoolNo}
								and survey date {surveyTag}. There are two possible reasons for this:
								1) This cloudfile is a duplicate of one that was already loaded (should be examined and likely removed.)
								2) This cloudfile was migrated from another cloud drive and thus reloading is no longer supported.";
				throw new HttpException((int)HttpStatusCode.BadRequest, err);
			}

			// ChangedTracked data added here (as opposed to the common automatic get/set when user the entity binder)
			// Since Approval is in essence the creation of the inspection we set the pCreateDateTime/User when it is
			// a new inspection. The Reload is essentially an edit of the inspection we set the pEditDateTime/User
			// when re-writting below.
			if (insp == null)
			{
				insp = new Pineapples.Data.Models.SchoolInspection();
				insp.pCreateTag = Guid.NewGuid();
				insp.SourceId = fileId;
				context.SchoolInspections.Add(insp);
				insp.pCreateDateTime = DateTime.UtcNow;
				insp.pCreateUser = User.Identity.Name;
			}

			// Here we're simply re-writting (updating any changes) if the files was already loaded.
			insp.InspectionContent = x.ToString();
			insp.schNo = schoolNo;
			insp.InspTypeCode = type;
			insp.StartDate = dt.Date;
			if (dtEnd.Date == dt.Date)
			{
				insp.EndDate = null;
			}
			else
			{
				insp.EndDate = dtEnd.Date;
			}
			// leave the inspection Year null - the year will be the 'School Year' derived from the StartDate
			//insp.InspectionYear = dt.Year;
			insp.InspectedBy = file.Properties["createUser"];
			insp.pEditDateTime = DateTime.UtcNow;
			insp.pEditUser = User.Identity.Name;

			await context.SaveChangesAsync();

			// we need to calculate the 'result' to put on the schl inspection record
			// this involves a formula that will vary from country to country, and between types
			// we implement the formula as a Sql scalar function that takes a single argument
			// inspID
			// The name of this function is on the lkpInspectionType record
			// we can only do this after the record is saved - in order to have access to the values
			// (implementing as a trigger would require specific view for each inspectionType)

			insp.InspectionResult = Factory.SchoolInspection().InspectionResult((int)insp.ID);
			await context.SaveChangesAsync();

			// create document records for each photo on the survey - this is done entirely in SQL
			// but note that although the record is created (and a docID allocated to the photo)
			// the photo is not brought local until it is referenced in either the google or library image processor

			Factory.SchoolInspection().RegisterInspectionPhotos((int)insp.ID);

			// Fire off the saving of all photos without waiting for it.
			Task notWaiting = Task.Run(() => SavePhotos(insp));

			// now set the properties on the source googlefile to indicate it has been uploaded
			IDictionary<string, string> propertiesToSet = new Dictionary<string, string>();
			propertiesToSet.Add("uploaded", "OK");
			propertiesToSet.Add("inspectionId", insp.ID.ToString());
			GoogleDriveManager.SetProperties(fileId, propertiesToSet);

			// send back the file metadata, which includes the new properties
			return await GoogleDriveManager.getFileMetaData(fileId); ;
		}

		/// <summary>
		/// utility to remove the uploaded and inspectionId properties
		/// for testing
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("{fileId}/reset")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.WriteX)]
		public async Task<File> ResetProperties(string fileId)
		{
			// now set the properties on the source googlefile to indicate it has been uploaded
			IDictionary<string, string> propertiesToSet = new Dictionary<string, string>();
			propertiesToSet.Add("uploaded", null);
			propertiesToSet.Add("inspectionId", null);
			GoogleDriveManager.SetProperties(fileId, propertiesToSet);
			// send back the file metadata, which inlcudes the new properties
			return await GoogleDriveManager.getFileMetaData(fileId); ;
		}

		[HttpGet]
		[Route("upload")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
		public object Upload(string filename)
		{


			string fname = System.IO.Path.GetFileName(filename);
			string mimeType = System.Web.MimeMapping.GetMimeMapping(filename);
			var fileMetadata = new File()
			{
				Name = fname
			};
			fileMetadata.Parents = new List<string>();
			fileMetadata.Parents.Add(ContextFolderId[0]);
			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();

			FilesResource.CreateMediaUpload request;
			using (var stream = new System.IO.FileStream(
				System.Web.Hosting.HostingEnvironment.MapPath(filename),
									System.IO.FileMode.Open))
			{
				request = service.Files.Create(
					fileMetadata, stream, mimeType);
				request.Fields = "id";
				request.Upload();
			}
			var file = request.ResponseBody;
			return file;
		}

		/// <summary>
		/// remove a file from the folder
		/// </summary>
		/// <param name="fileID"></param>
		[HttpDelete]
		[Route("{fileId}")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Write)]
		public async Task<string> Remove(string fileId)
		{
			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();
			// TO DO check the file is actually in the folder we are allowed to work with!

			//https://stackoverflow.com/questions/15118145/delete-a-file-using-google-drive-api


			var file = await GoogleDriveManager.getFileMetaData(fileId);
			if (file.FileExtension == "xml" && file.Properties["surveyCompleted"] != "MERGED")
			{
				XDocument x;
				// if it is an xml file and it is not a Merged survey, get the xml survey and look for any remotePath
				// ie associated photos
				// Merged xml surveys are excluded as they contain the same photo IDs as their primary survey which they
				// merged into. Deleting those associated before loaded the primary survey would delete the photos before 
				// they would be loaded into the system. Those photos will then be cleaned up if and when deleting the 
				// primary survey into which those merged surveys were merged into.
				var stream = await GoogleDriveManager.getFileContent(fileId);
				x = XDocument.Load(stream);
				foreach (XElement rp in x.Descendants("remotePath"))
				{
					// don't throw an error if we are unable to clean up related photos
					try
					{
						await Remove(rp.Value);
					}
					catch
					{

					}
				}
			}
			FilesResource.DeleteRequest request = service.Files.Delete(fileId);
			await request.ExecuteAsync();
			return "Removed";

		}

		/// <summary>
		/// get the json is the basic opertion on fileId
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("{fileId}")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
		public async Task<HttpResponseMessage> DownloadJson(string fileId)
		{



			try
			{
				// Create Drive API service.
				var service = GoogleDriveManager.getDriveService();
				// get the metaData so we have the mime type
				var file = await GoogleDriveManager.getFileMetaData(fileId);
				//var request = service.Files.Get(fileId);
				var stream = await GoogleDriveManager.getFileContent(fileId);
				// instead of passing the stream back to the client, load it into an XDocument,
				// then convert to Json
				var x = XDocument.Load(stream);
				var jsonobject = JsonConvert.SerializeXNode(x);
				return RawJsonResponse(jsonobject);
			}
			catch (Google.GoogleApiException ex)
			{
				// assume its filenot found
				switch (ex.HttpStatusCode)
				{
					case HttpStatusCode.NotFound:
						throw new Softwords.Web.RecordNotFoundException(fileId, "File not found on google drive", ex);
					default:
						throw new HttpException((int)ex.HttpStatusCode, $"Error reading file {fileId} on google drive: {ex.Message}", ex);
				}

			}
			catch (Exception ex)
			{
				throw new HttpException((int)HttpStatusCode.InternalServerError, ex.Message, ex);
			}
		}

		[HttpGet]
		[Route("{fileId}/download")]
		[PineapplesPermission(PermissionTopicEnum.Inspection, Softwords.Web.PermissionAccess.Read)]
		public async Task<HttpResponseMessage> Download(string fileId)
		{

			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();
			// get the metaData so we have the mime type
			var file = await GoogleDriveManager.getFileMetaData(fileId);
			var request = service.Files.Get(fileId);
			var stream = await GoogleDriveManager.getFileContent(fileId);
			var response = new HttpResponseMessage()
			{
				Content = new StreamContent(stream)
			};
			response.Content.Headers.ContentDisposition =
				new ContentDispositionHeaderValue("attachment");
			response.Content.Headers.ContentDisposition.FileName = file.Name;
			response.Content.Headers.ContentType = new MediaTypeHeaderValue(file.MimeType);
			return response;
		}


		[HttpGet]
		[Route("prepfolder")]
	
		public string PrepFolders()
		{
			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();

			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();
			listRequest.Q = string.Format("mimeType = 'application/vnd.google-apps.folder' " +
				"and name='{0}'", Context);
			listRequest.PageSize = 1;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

			// List files.
			var fileList = listRequest.Execute();
			IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;


			if (files != null && files.Count > 0)
			{
				return files[0].Id;
			}
			else
			{
				File fileMetadata = new File()
				{
					Name = Context,
					MimeType = "application/vnd.google-apps.folder"

				};

				FilesResource.CreateRequest request;
				request = service.Files.Create(fileMetadata);
				request.Fields = "id";
				var file = request.Execute();
				return file.Id;
			}
		}

		#region


		/// <summary>
		/// Remove the Uploaded and InspectinId propties to allow a file to be resued in
		/// different testing scenarios
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("{fileId}/untag")]
		public async Task<object> UntagFile(string fileId)
		{
			// now set the properties on the source googlefile to indicate it has been uploaded
			IDictionary<string, string> propertiesToSet = new Dictionary<string, string>();
			propertiesToSet.Add("uploaded", null);
			propertiesToSet.Add("inspectionId", null);
			GoogleDriveManager.SetProperties(fileId, propertiesToSet);

			// send back the file metadata, which includes the new properties
			return await GoogleDriveManager.getFileMetaData(fileId); ;
		}


		#endregion

		IList<string> contextFolderId = null;
		IList<string> ContextFolderId
		{

			get
			{
				if (contextFolderId == null)
				{
					contextFolderId = new List<string>();
					// add a dummy entry to the list to ensure that there is always a filter on folder
					// real folder Ids are joined with OR, so the dummy entry has no effect if real entries are there
					// but if there are no real entries, the dummy will prevent anything being returned
					//contextFolderId.Add(new string('0',33));	// google ids are 33 chars
					var service = GoogleDriveManager.getDriveService();
					FilesResource.ListRequest listRequest = service.Files.List();
					var f = new CloudfileFilter();
					listRequest.Q = string.Format("mimeType = 'application/vnd.google-apps.folder' and name='{0}'", Context);
					//listRequest.Q = "mimeType = 'application/vnd.google-apps.folder'";
					listRequest.PageSize = 50;
					listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

					// List files.
					var fileList = listRequest.Execute();
					IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;
					if (files != null)
					{
						foreach (var gf in files)
						{
							contextFolderId.Add(gf.Id);
						}
					}
				}
				System.Diagnostics.Debug.Assert(contextFolderId.Count > 0, $"No folders found matching {Context}");
				return contextFolderId;

			}
		}

		#region Photos

		/// <summary>
		/// Photos are stored in a photos folder.
		/// To get a photo, we need the photo folder id
		/// </summary>
		IList<string> photoFolderId = null;
		IList<string> PhotoFolderId
		{

			get
			{
				if (photoFolderId == null)
				{
					photoFolderId = new List<string>();

					var service = GoogleDriveManager.getDriveService();
					FilesResource.ListRequest listRequest = service.Files.List();

					var f = new CloudfileFilter();
					f.folder = ContextFolderId;
					f.mimeType = "application/vnd.google-apps.folder";
					f.name = "photos";

					listRequest.Q = f.ToString();
					listRequest.PageSize = 1;
					listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

					// List files.
					var fileList = listRequest.Execute();
					IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;
					if (files != null)
					{
						foreach (var gf in files)
						{
							photoFolderId.Add(gf.Id);
						}
					}
				}
				return photoFolderId;
			}

		}

		[HttpGet]
		[Route("photos")]
		public async Task<IList<Google.Apis.Drive.v3.Data.File>> getPhotos()
		{
			CloudfileFilter fltr = new CloudfileFilter();


			return await getPhotos(fltr);
		}
		[HttpPost]
		[Route("photos")]
		public async Task<IList<Google.Apis.Drive.v3.Data.File>> getPhotos(CloudfileFilter fltr)
		{

			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();

			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();

			fltr = fltr ?? new CloudfileFilter();
			fltr.folder = PhotoFolderId;
			listRequest.Q = fltr.ToString();
			listRequest.PageSize = 100;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, parents, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

			var fileList = await listRequest.ExecuteAsync();
			return fileList.Files;
		}
		#endregion


		#region Housekeeping

		/// <summary>
		/// Delete files from cloud storage that have already been downloaded
		/// </summary>
		/// <param name="limit">
		/// Maximum number of file to attempt to delete (default 1)
		/// </param>
		/// <returns>Array of docIDs and corresponding cloud ids - error message if error</returns>

		[HttpGet]
		[Route(@"purge")]
		[AllowAnonymous]
		public async Task<Array> ToPurge([FromUri] int limit = 1)
		{
			var result = new List<object>();

			var docs = Factory.Context.Documents
				.Where(doc => doc.docCloudID != null && doc.docCloudRemoved == 0)
				// get ALL of these Documents - its a temporary set so should not be too many 
				.ToList()
				// this where is executed on the list, not by Entity Framework
				// so it can use the C# function
				.Where(doc => Pineapples.Providers.FileDB.FileExists(doc.docID.ToString()))
				// now get up to <limit> number of these that have been downloaded
				.Take(limit);

			var service = GoogleDriveManager.getDriveService();
			foreach (Pineapples.Data.Models.Document doc in docs)
			{
				try
				{
					FilesResource.DeleteRequest request = service.Files.Delete(doc.docCloudID);
					await request.ExecuteAsync();
					doc.docCloudRemoved = 1;
					result.Add(new { docID = doc.docID, cloudID = doc.docCloudID, result = "" });
				}
				catch (Google.GoogleApiException ex)
				{
					result.Add(new { docID = doc.docID, cloudID = doc.docCloudID, result = ex.Message });
					switch (ex.HttpStatusCode)
					{
						case HttpStatusCode.NotFound:
							doc.docCloudRemoved = 1;        // mark as removed cos its not there anyway
							break;
					}
				}
				catch (Exception ex)
				{
					result.Add(new { docID = doc.docID, cloudID = doc.docCloudID, result = ex.Message });
				}
			}
			await Factory.Context.SaveChangesAsync();
			return result.ToArray();  // will be a json array even if only one element returned
		}
		/// <summary>
		/// Delete files from cloud storage that have been loaded to create inspections
		/// </summary>
		/// <param name="limit">
		/// Maximum number of file to attempt to delete (default 1)
		/// </param>
		/// <returns>Array of InspectionIDs and corresponding cloud ids - error message if error</returns>
		[HttpGet]
		[Route(@"purgeinspections")]
		[AllowAnonymous]
		public async Task<Array> ToPurgeInspections([FromUri] int limit = 1)
		{
			var result = new List<object>();

			var docs = Factory.Context.SchoolInspections
				.Where(insp => insp.SourceId != null)   //&& doc.docCloudRemoved == 0)
				.Take(limit);
			// now get up to <limit> number of these that have been downloaded

			var service = GoogleDriveManager.getDriveService();
			foreach (Pineapples.Data.Models.SchoolInspection doc in docs)
			{
				try
				{
					FilesResource.DeleteRequest request = service.Files.Delete(doc.SourceId);
					await request.ExecuteAsync();
					//doc.docCloudRemoved = 1;
					result.Add(new { inspID = doc.ID, cloudID = doc.SourceId, result = "" });
				}
				catch (Google.GoogleApiException ex)
				{
					result.Add(new { inspID = doc.ID, cloudID = doc.SourceId, result = ex.Message });
					switch (ex.HttpStatusCode)
					{
						case HttpStatusCode.NotFound:
							//doc.docCloudRemoved = 1;        // mark as removed cos its not there anyway
							break;
					}
				}
				catch (Exception ex)
				{
					result.Add(new { inspID = doc.ID, cloudID = doc.SourceId, result = ex.Message });
				}
			}
			await Factory.Context.SaveChangesAsync();
			return result.ToArray();  // will be a json array even if only one element returned
		}
		/// <summary>
		/// Downloads all photos of an inspection from the cloud and
		/// saves them to fileDB. This happens asynchronously and is currently
		/// called by the Approval of a cloud file inspection routine. The approval process
		/// does not for for this which continues in the background since it could take a lot of
		/// time on slow Internet links. Any error is captured and an email sent to the admin configured
		/// in the web.config file.
		/// 
		/// </summary>
		/// <param name="insp">the school inspection</param>
		/// <returns>nothing</returns>
		public async Task SavePhotos(Pineapples.Data.Models.SchoolInspection insp)
		{
			var inspDocs = Factory.Context.SchoolInspectionLinks.Where(sil => sil.inspID == insp.ID && sil.docCloudRemoved != 1)
				// get ALL of the inspection's Documents (photos)
				.ToList();

			foreach (Pineapples.Data.Models.SchoolInspectionLink doc in inspDocs)
			{
				try
				{
					// If the photo was not saved to fileDB yet
					string strId = doc.docID.ToString();
					if (!Pineapples.Providers.FileDB.FileExists(strId))
					{
						// Get the google file and save it
						await Pineapples.Providers.FileDB.StoreGoogleDriveFile(doc.docCloudID, doc.docID);
					}
				}
				catch (Google.GoogleApiException ex)
				{
					switch (ex.HttpStatusCode)
					{
						case HttpStatusCode.NotFound:
							// Do nothing, no need to send an email. This is an expected error in the following use case
							// 1) An inspection is approved triggering all the photos to be downloaded
							// 2) A purge takes places removing the cloud files downloaded in step 1)
							// 3) A reload of the same inspection takes place (a rare case of a desired edit). This triggers a download
							//    of all the files but they have already been purged in step 2) and only the XML survey is left. 

							// This is in effect similar to doing a purge of files that were already purged and getting this error
							// TODO Perhaps before attempting to download (i.e. here) or purge (i.e. in ToPurge method) checking
							// for the existance of the cloud file as a first step?
							break;
					}
				}
				catch (Exception ex)
				{
					// Something went wrong fetching cloud file and saving it to disk. Notify
					// user by email.
					var smtpClient = new SmtpClient();
					string sender = System.Web.Configuration.WebConfigurationManager.AppSettings["emailSenderAddress"] ?? "no-reply@national.doe.fm";
					string receiver = System.Web.Configuration.WebConfigurationManager.AppSettings["emailReceiverAddress"] ?? "sysmail@doe.fm";

					smtpClient.Send(sender,
						receiver,
						"Error downloading cloud photo",
						"Error downloading cloud photo with cloud ID: " + doc.docCloudID + "and document ID: " + doc.docID + "The error was: " + ex);
				}
			}
		}

		#endregion

		#region Unused

		//UNUSED - to remove?
		[HttpGet]
		//[Route("searchforfolders")]
		public object searchForFolders()
		{

			// Create Drive API service.
			var service = GoogleDriveManager.getDriveService();

			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();
			listRequest.Q = "mimeType = 'application/vnd.google-apps.folder'";
			listRequest.PageSize = 20;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, isAppAuthorized, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";

			// List files.
			var fileList = listRequest.Execute();
			IList<Google.Apis.Drive.v3.Data.File> files = fileList.Files;


			if (files != null && files.Count > 0)
			{
				foreach (File file in files)
				{
					//	Console.WriteLine("{0} ({1})", file.Name, file.Id);
				}
			}
			else
			{
				//Console.WriteLine("No files found.");
			}

			return fileList;
		}

		// UNUSED - move to GoogleDriveManager if we want this facility at some time?
		[HttpGet]
		//[Route("movetoFolder")]
		public object Move(string fileId, string newFolder)
		{
			//https://developers.google.com/drive/api/v3/folder
			var service = GoogleDriveManager.getDriveService();
			// Define parameters of request.
			var folderId = PrepFolders();
			// Retrieve the existing parents to remove
			var getRequest = service.Files.Get(fileId);
			getRequest.Fields = "parents";
			var file = getRequest.Execute();
			var previousParents = String.Join(",", file.Parents);
			// Move the file to the new folder
			var updateRequest = service.Files.Update(new File(), fileId);
			updateRequest.Fields = "id, parents";
			updateRequest.AddParents = folderId;
			updateRequest.RemoveParents = previousParents;
			file = updateRequest.Execute();
			return file;
		}


		// UNUSED
		[HttpGet]
		//[Route("subfolder")]
		public object createSubFolder()
		{
			{
				File fileMetadata = new File()
				{
					Name = "fedemis",
					MimeType = "application/vnd.google-apps.folder"

				};

				// Create Drive API service.
				var service = GoogleDriveManager.getDriveService();

				FilesResource.CreateRequest request;
				request = service.Files.Create(fileMetadata);
				request.Fields = "id, name";
				var file = request.Execute();
				return file;
			}
		}
		#endregion


	}

}
