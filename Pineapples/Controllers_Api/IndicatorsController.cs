﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Softwords.DataTools;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;


namespace Pineapples.Controllers
{
    public class IndicatorsController : PineapplesApiController
    {
        public IndicatorsController(DataLayer.IDSFactory factory) : base(factory) { }

		/// <summary>
		/// Get the current national indicator table from the database
		/// </summary>
		/// <returns>indicator table as xml</returns>
		[HttpGet]
		[Route(@"api/indicators")]
		public async Task<HttpResponseMessage> indicatorsTable()
		{
			string warehouseversion = Factory.Warehouse().WarehouseVersion();
			string indicatorversion = Factory.Indicators().WarehouseVersion();

			XDocument xverm = null;

			if (indicatorversion != warehouseversion)
			{
				xverm = await Factory.Indicators().makeIndicatorTable(null);
				// indicatorversion is now current warehouseversion

			}
			string eTagValue = null;
			if (checkETag(out eTagValue))
			{
					return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			if (xverm == null)
			{
				xverm = await Factory.Indicators().readIndicatorTable();
			}
			
			Softwords.Web.Content.XmlContent content = new Softwords.Web.Content.XmlContent(xverm);

			var response = Request.CreateResponse(HttpStatusCode.OK);
			response.Content = content;

			string quotedEtag = String.Format("\"{0}\"", eTagValue);
			response.Headers.ETag = new System.Net.Http.Headers.EntityTagHeaderValue(quotedEtag);
			return response;
		}

		/// <summary>
		/// Get a district indicator table from the database
		/// </summary>
		/// <param name="domain">district code</param>
		/// <returns>indicator table as xml</returns>
		[HttpGet]
		[Route(@"api/indicators/{domain}")]
		public async Task<HttpResponseMessage> indicatorsTable(string domain)
		{
			string warehouseversion = Factory.Warehouse().WarehouseVersion();
			string indicatorversion = Factory.Indicators().WarehouseVersion(domain);

			XDocument xverm = null;

			if (indicatorversion != warehouseversion)
			{
				xverm = await Factory.Indicators().makeIndicatorTable(domain, null);
				// indicatorversion is now current warehouseversion
			}

			string eTagValue = null;
			if (checkETag(out eTagValue, domain))
			{
				return new HttpResponseMessage(HttpStatusCode.NotModified);
			}

			XDocument x = await Factory.Indicators().readIndicatorTable(domain);

			Softwords.Web.Content.XmlContent content = new Softwords.Web.Content.XmlContent(x);

			var response = Request.CreateResponse(HttpStatusCode.OK);
			response.Content = content;
			string quotedEtag = String.Format("\"{0}\"", eTagValue);
			response.Headers.ETag = new System.Net.Http.Headers.EntityTagHeaderValue(quotedEtag);
			return response;
		}

        [HttpGet]
        [Route(@"api/indicators/makewarehouse/{year:int?}")]
        [PineapplesPermission(PermissionTopicEnum.Survey, Softwords.Web.PermissionAccess.Admin)]
        public async Task<IDataResult> makeWarehouse(int? year = null)
        {
            return await Factory.Indicators().makeWarehouse(year);
        }

		#region construction of indicators
		[HttpGet]
        [Route(@"api/indicators/refresh/{year:int?}")]
        [PineapplesPermission(PermissionTopicEnum.Survey,Softwords.Web.PermissionAccess.Admin)]
        public async Task<HttpResponseMessage> MakeIndicatorTable(int? year = null)
        {
            System.Xml.Linq.XDocument xverm = await Factory.Indicators().makeIndicatorTable(year);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }


        [HttpGet]
        [Route(@"api/indicators/refresh/{district}/{year:int?}")]
        [PineapplesPermission(PermissionTopicEnum.Survey,Softwords.Web.PermissionAccess.Admin)]
        public async Task<HttpResponseMessage> MakeIndicatorTable(string district, int? year = null)
        {
            System.Xml.Linq.XDocument xverm = await Factory.Indicators().makeIndicatorTable(district, year);
            // don't return the content - we'll just rely on the warehouse eTag
            // at the other end to force a re-read when needed
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
		#endregion

		#region etag management
		/// <summary>
		/// Check whether warehouse version passed in as eTag matches the current warehouse version of the indicator file
		/// Note that this checks only that the user has the most recent version of the indicators file.
		/// It does NOT check that the indicators file has been constructed from the current version of the warehouse.
		/// </summary>
		/// <param name="eTagValue"></param>
		/// <param name="domain"></param>
		/// <returns></returns>
		private bool checkETag(out string eTagValue, string domain = null)
		{
			if (domain == null)
			{
				eTagValue = Factory.Indicators().WarehouseVersion();
			} else
			{
				eTagValue = Factory.Indicators().WarehouseVersion(domain);
			}

			// wrap in qutes before testing the versions from the client
			string currentETag = String.Format("\"{0}\"", eTagValue);

			ICollection<EntityTagHeaderValue> etagsFromClient = Request.Headers.IfNoneMatch;
			if (etagsFromClient.Count > 0)
			{
				return (etagsFromClient.Any(t => t.Tag == currentETag));
			}
			return false;
		}
		#endregion
	}
}
