﻿using System;
using System.Web.Http;
using System.Security.Claims;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Data.Models;

using Softwords.Web;
using Softwords.Web.Models;
using System.Threading.Tasks;


namespace Pineapples.Controllers
{
    [RoutePrefix("api/studentdeduper")]
	[Authorize]
    public class StudentDeduperController : PineapplesApiController
    {
        public StudentDeduperController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpPost]
        [Route("collection/filter")]
        public object Filter(StudentDeduperFilter fltr)
        {
            //fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
        }

		[HttpGet]
		[Route(@"")]
		public object FilterGet([FromUri] StudentDeduperFilter fltr)
		{
			if (fltr == null)
			{
				fltr = new StudentDeduperFilter();
			}
			return Ds.Filter(fltr);
		}

		[HttpGet]
		[Route("surveys/{id1:guid}/{id2:guid}")]
		public object Surveys(Guid id1, Guid id2)
		{
			//fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Surveys(id1,id2);
		}

		[HttpGet]
		[Route("merge/{targetID:guid}/{sourceID:guid}")]
		public object Merge(Guid targetID, Guid sourceID)
		{
			//fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return Ds.Merge(targetID, sourceID);
		}

		#region Enrolments
		[HttpDelete]
		[Route("enrolments/{ID:int}")]
		public async Task<StudentEnrolment> DeleteEnrolment(int ID, DynamicBinder<StudentEnrolment> binder)
		{
			//fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
			return await binder
				.Identity((ClaimsIdentity)User.Identity)
				.Context(Factory.Context)
				.Delete(ID);
		}
		#endregion

		#region AccessControl
		// removed?

		#endregion
		private IDSStudentDeduper Ds
        {
            get { return Factory.StudentDeduper(); }
        }
    }
}
