﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using System.Threading.Tasks;
using Pineapples.Models;
using System.Xml.Linq;
using SharpCompress;
using iText.Forms;
using iText.Forms.Xfdf;
using iText.Kernel.Pdf;
using iText.Forms.Fields;
using System.Diagnostics;
using System.IO;
using Pineapples.Data.Models;
using System.Net.Http.Headers;

using System.Xml.XPath;
using System.Xml;
using Pineapples.Data.DB;
using iText.Kernel.Font;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Xobject;
using iText.Kernel.Geom;
using iText.IO.Font.Constants;
using iText.Kernel.Colors;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Database;
using iText.Forms.Fields.Properties;
using Microsoft.AspNet.Identity;
using iText.IO.Font;
using DocumentFormat.OpenXml.Drawing.Charts;
using iText.Kernel.Pdf.Annot;
using Microsoft.Ajax.Utilities;
using System.Runtime.CompilerServices;
using iText.Kernel.Pdf.Layer;
using System.Text.RegularExpressions;
using System.Security.AccessControl;
using Softwords.Web.Models;
using System.Configuration;
using iText.Kernel.Pdf.Navigation;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Office.CustomUI;
using System.Web.WebPages.Instrumentation;
using iText.IO.Font.Otf.Lookuptype8;

namespace Pineapples.Controllers
{
	/// <summary>
	/// Class contains endpoints that expose the xfdf loading functions used when loading from an interactive
	/// pdf. 
	/// </summary>

	[RoutePrefix("api/pdfSurvey")]
	public class PdfSurveyController : PineapplesApiController
	{
		public PdfSurveyController(DataLayer.IDSFactory factory) : base(factory) { }

		/// <summary>
		/// uploads and stores the file
		/// </summary>
		/// <returns>a summary of the data in the file, together with the file ID</returns>
		[HttpPost]
		[Route(@"upload")]
		public async Task<object> Upload()
		{

			if (!Request.Content.IsMimeMultipartContent())
				throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

			var provider = new MultipartFormDataStreamProvider(AppDataPath);

			var files = await Request.Content.ReadAsMultipartAsync(provider);
			Dictionary<string, object> results = new Dictionary<string, object>();

			foreach (MultipartFileData fd in files.FileData)
			{
				Guid g = Guid.NewGuid();


				string filename = fd.Headers.ContentDisposition.FileName;
				filename = filename.Replace("\"", "");          // tends to com back with quotes around
				string ext = System.IO.Path.GetExtension(filename);

				// if it is a zip file, unwrap it... this allows much smaller uploads
				if (ext.ToLower() == ".zip")
				{
					using (System.IO.FileStream fstrm = new System.IO.FileStream(fd.LocalFileName, System.IO.FileMode.Open))
					{
						using (var reader = SharpCompress.Readers.ReaderFactory.Open(fstrm))
						{
							if (reader.MoveToNextEntry()) // not empty
							{
								var entry = reader.Entry;
								var entryStream = reader.OpenEntryStream();
								filename = entry.Key.Replace("\"", "");          // tends to com back with quotes around
								ext = System.IO.Path.GetExtension(filename);
								switch (ext.ToLower())
								{
									case ".pdf":
									case ".xfdf":
										g = Providers.FileDB.Store(entryStream, ext);
										break;
									default:
										var message = "The Zip file did not contain a PDF file or xfdf file as its only entry.";
										return Request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
								}

							}
						}
					};
				}
				else
				{
					using (System.IO.FileStream fstrm = new System.IO.FileStream(fd.LocalFileName, System.IO.FileMode.Open))
					{
						g = Providers.FileDB.Store(fstrm, ext);
						// add a document record for this file
					}
				}

				// get the info about the survey 
				PdfSurvey survey = new PdfSurvey(Providers.FileDB.GetFilePath(g.ToString()));
				// we can get rid of the local file now
				System.IO.File.Delete(fd.LocalFileName);

				results.Add("SchoolName", survey.SchoolName);
				results.Add("SchoolNo", survey.SchoolNo);
				results.Add("SurveyYear", survey.SurveyYear);
				results.Add("Enrolment", survey.GetField("Enrol.T.T.T.T")?.Value);
				results.Add("id", g);
			}
			return results;      // only the last one? but we expect only one at a time.....?
		}



		/// <summary>
		/// Processes the nominated file
		/// </summary>
		/// <param name="fileId">guid to the Excel document in the FileDB</param>
		/// <returns></returns>
		[HttpGet]
		[Route(@"process/{fileId}")]
		public object Process(string fileId)
		{
			// first get the file based on its Id
			string path = Providers.FileDB.GetFilePath(fileId);
			if (!System.IO.File.Exists(path))
			{
				throw new Softwords.Web.RecordNotFoundException(fileId, "No file found for File ID", null);
			}
			PdfSurvey survey = new PdfSurvey(path);
			survey.Process(Factory.PdfSurvey(), fileId, (ClaimsIdentity)User.Identity);

			return Factory.Survey().AuditLog(survey.SchoolNo, (int)survey.SurveyYear);         // return the loaded inspections
		}

		/// <summary>
		/// reprocess the survey using the file stored in the FileDb and recorded in the Survey record
		/// </summary>
		/// <param name="SurveyID">guid to the Excel document in the FileDB</param>
		/// <returns></returns>
		[HttpGet]
		[Route(@"reprocess/{surveyID}")]
		public object Reprocess(int surveyID)
		{
			Pineapples.Data.Models.SchoolSurvey ss = Factory.Context.SchoolSurveys.Find(surveyID);
			if (ss == null)
			{
				throw new Softwords.Web.RecordNotFoundException(surveyID, "Survey ID not found", null);
			}

			string fileId = ss.ssSource;
			if (fileId == null)
			{
				throw new Softwords.Web.RecordNotFoundException(surveyID, "File ID not recorded on survey", null);
			}
			return Process(fileId);
		}
		/// <summary>
		/// reprocess the survey using the file stored in the FileDb and recorded in the Survey record
		/// </summary>
		/// <param name="SurveyID">guid to the Excel document in the FileDB</param>
		/// <returns></returns>
		[HttpGet]
		[Route(@"reprocess/{schoolNo}/{surveyYear}")]
		public object Reprocess(string schoolNo, int surveyYear)
		{
			Pineapples.Data.Models.SchoolSurvey ss = Factory.Context.SchoolSurveys
				.Where(x => x.schNo == schoolNo && x.svyYear == surveyYear)
				.FirstOrDefault();
			if (ss == null)
			{
				throw new Softwords.Web.RecordNotFoundException(schoolNo, $"Survey not found for {schoolNo}/{surveyYear}", null);
			}
			int surveyID = (int)ss.ssID;
			string fileId = ss.ssSource;
			if (fileId == null)
			{
				throw new Softwords.Web.RecordNotFoundException(surveyID, "File ID not recorded on survey", null);
			}
			return Process(fileId);
		}
		[HttpGet]
		[Route(@"xfdf/{schoolNo}/{surveyYear}")]
		public HttpResponseMessage Xfdf(string schoolNo, int surveyYear)
		{
			Pineapples.Data.Models.SchoolSurvey ss = Factory.Context.SchoolSurveys
				.Where(x => x.schNo == schoolNo && x.svyYear == surveyYear)
				.FirstOrDefault();
			if (ss == null)
			{
				throw new Softwords.Web.RecordNotFoundException(schoolNo, $"Survey not found for {schoolNo}/{surveyYear}", null);
			}
			return getXfdf(ss);
		}

		[HttpGet]
		[Route(@"xfdf/{surveyID}")]
		public HttpResponseMessage Xfdf(int surveyID)
		{
			Pineapples.Data.Models.SchoolSurvey ss = Factory.Context.SchoolSurveys.Find(surveyID);
			if (ss == null)
			{
				throw new Softwords.Web.RecordNotFoundException(surveyID, "Survey ID not found", null);
			}
			return getXfdf(ss);
		}

		private HttpResponseMessage getXfdf(Pineapples.Data.Models.SchoolSurvey ss)
		{


			string fileId = ss.ssSource;
			if (fileId == null)
			{
				throw new Softwords.Web.RecordNotFoundException(ss.ssID, "File ID not recorded on survey", null);
			}
			string path = Providers.FileDB.GetFilePath(fileId);
			if (!System.IO.File.Exists(path))
			{
				throw new Softwords.Web.RecordNotFoundException(fileId, "No file found for File ID", null);
			}
			PdfSurvey survey = new PdfSurvey(path);
			var xmlContent = new Softwords.Web.Content.XmlContent(survey.Xfdf());
			return new HttpResponseMessage() { Content = xmlContent };

		}
		/// <summary>
		/// If the process fails validation tests,he file is deleted.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route(@"cancelprocess/{fileId}")]
		public void CancelProcess(string fileId)
		{
			Guid g;
			if (Guid.TryParse(fileId, out g))
			{
				// cancel should only be allowed to delete a file that is not referenced in the Documents table
				// so, id we find the key FileId in the table Documents - do not remove
				// otherwise, we effectively have a url that lets you trash the FileDb
				if (!Factory.Context.Documents.Any(doc => doc.docID == g))
				{
					Providers.FileDB.Remove(fileId);
				}
			}
		}

		[HttpGet]
		[Route(@"reload/{schoolNo}/{surveyYear}")]
		[AllowAnonymous]
		public async Task<HttpResponseMessage> ReloadSurvey(string schoolNo, int surveyYear, int? targetYear = null, int? fromData = null)
		{

			targetYear = targetYear ?? surveyYear;
			fromData = fromData ?? 0;

			var school = await Factory.Context.Schools.FindAsync(schoolNo);
			if (school == null)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, $"School {schoolNo} not found.");
			}

			Pineapples.Data.Models.SchoolSurvey ss = Factory.Context.SchoolSurveys
				.Where(x => x.schNo == schoolNo && x.svyYear == surveyYear)
				.FirstOrDefault();
			if (ss == null)
			{
				throw new Softwords.Web.RecordNotFoundException(schoolNo, $"Survey not found for {schoolNo}/{surveyYear}", null);
			}
			int surveyID = (int)ss.ssID;

			// get the xfdf either directly from the pdf, or from the SchoolSurveyXml_ table

			string xfdfString = null;
			if (fromData == 1)
			{
				var ssXml = await Factory.Context.SchoolSurveysXml.FindAsync(surveyID);
				xfdfString = ssXml.ssXml;
			}
			else
			{
				string fileId = ss.ssSource;
				if (fileId == null)
				{
					throw new Softwords.Web.RecordNotFoundException(surveyID, "File ID not recorded on survey", null);
				}
				string path = Providers.FileDB.GetFilePath(fileId);
				if (!System.IO.File.Exists(path))
				{
					throw new Softwords.Web.RecordNotFoundException(fileId, "No file found for File ID", null);
				}
				PdfSurvey survey = new PdfSurvey(path);
				XDocument xfdf = survey.Xfdf();
				xfdfString = xfdf.ToString();
			}

			//
			// now get the current template for the school and year
			// the template specifer is derived from, but not equal to , the school type
			string template = GetSchoolForm(school, targetYear ?? surveyYear);

			ReaderProperties rprops = new ReaderProperties()
				.SetPassword(System.Text.Encoding.ASCII.GetBytes("kiri"));
			WriterProperties wprops = new WriterProperties()
				.SetCompressionLevel(CompressionConstants.NO_COMPRESSION);

			MemoryStream mstrm = new MemoryStream();
			{

				using (PdfReader reader = new PdfReader(new FileStream
					(template, FileMode.Open, FileAccess.Read, FileShare.Read), rprops))
				using (PdfWriter writer = new PdfWriter(mstrm, wprops))
				{
					reader.SetCloseStream(true);
					writer.SetCloseStream(false);
					using (PdfDocument pdfDocument = new PdfDocument(reader, writer))
					{
						pdfDocument.SetCloseWriter(false);

						byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(xfdfString);

						// Create a MemoryStream and write the byte array to it
						using (MemoryStream xfdfStream = new MemoryStream())
						{
							xfdfStream.Write(byteArray, 0, byteArray.Length);

							// Optional: Reset the position to the beginning if you want to read from it later
							xfdfStream.Position = 0;

							XfdfObject xfdfData = new XfdfObjectFactory().CreateXfdfObject(xfdfStream);
							// push the xfdf into the document
							xfdfData.MergeToPdf(pdfDocument, template);

						}
						// if needs appearance is on, seems to recalc all the standardized checkboxes??
						PdfAcroForm form = PdfAcroForm.GetAcroForm(pdfDocument, false);
						form.SetNeedAppearances(false);
						// Standardise the checkboxes after pushing in the data... they seem to get out of kilter otherwise
						// see https://bitbucket.org/softwords/pineapples/issues/1410/pdf-survey-anomaly
						StandardizeCheckboxes(pdfDocument);
						StandardiseCombos(pdfDocument);
					} // dispose the document
					reader.Close();
				} //Dispose the writer and writer

				mstrm.Position = 0;
				// Create the response message with the file data
				HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
				{
					Content = new StreamContent(mstrm)
				};

				// Set the content type and disposition
				response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
				response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
				{
					FileName = $"{school.schNo} {school.schName} {Context} Survey {targetYear}.pdf"
				};
				return response;

			}
		}

		[HttpGet]
		[Route(@"generate/{schoolNo}/{year}")]
		[AllowAnonymous]
		public async Task<HttpResponseMessage> GenerateSurvey(string schoolNo, int year)

		{
			// first get the school

			var school = await Factory.Context.Schools.FindAsync(schoolNo);
			if (school == null)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, $"School {schoolNo} not found.");
			}

			// now get the current template for the school and year
			// the template specifer is derived from, but not equal to , the school type
			string template = GetSchoolForm(school, year);

			PdfSurvey survey = new PdfSurvey(template);
			XfdfDocument xfdf = new XfdfDocument(survey.Xfdf());
			// set up the namespace
			XmlNamespaceManager nsp = new XmlNamespaceManager(new NameTable());
			nsp.AddNamespace("x", "http://ns.adobe.com/xfdf/");
			XNamespace defns = XNamespace.Get(@"http://ns.adobe.com/xfdf/");

			/// build the xfdf file by manipulating an XDocument

			// now teachers
			var teacherResult = Factory.PdfSurvey().prepopTeachers(schoolNo, year);
			var teachers = (System.Data.DataTable)(teacherResult.ResultSet);
			XElement nodeTL = xfdf.GetElement("TL");

			if (!(nodeTL == null))
			{
				int index = 0;
				// now we are going to step through this array, pushing matching field names into the xml

				foreach (System.Data.DataRow tl in teachers.Rows)
				{
					XElement nodeT = xfdf.GetElement($"TL.{index:D2}");
					if (nodeT == null)          // this can be an overflow - more teachers than spaces
					{
						////warnings.Add(String.Format("Teacher {0} {1} was not added to Expected Teachers List", tl.FirstName, tl.FamilyName));
					}
					else
					{
						foreach (XElement tlf in nodeT.Elements())
						{
							string fieldName = tlf.Attribute("name").Value;
							object val = null;
							switch (fieldName)
							{
								case "InService":
									val = tl["InService_Topic"];
									if (!(val is DBNull || val == null))
									{
										//Inservice topic and year are childrenpf inservice
										var tlf2 = xfdf.GetChild(tlf, "Topic");
										xfdf.SetValue(tlf2, val);
									}
									val = tl["InService_Year"];
									if (!(val is DBNull || val == null))
									{
										var tlf2 = xfdf.GetChild(tlf, "Year");
										xfdf.SetValue(tlf2, val);
									}
									val = null;
									break;


								case "DoB":
									if (tl[fieldName] is DBNull || tl[fieldName] == null)
										val = null;
									else
									{
										val = ((DateTime)tl[fieldName]).ToString("yyyy-MM-dd");
									}
									break;
								default:
									if (tl.Table.Columns.Contains(fieldName))
									{
										val = tl[fieldName];
									}
									break;
							}
							if (val != null)
							{
								xfdf.SetValue(tlf, val);
							}
						}
						index++;

					}
				} // end of teacher rows

				// Toilets
				var result = Factory.PdfSurvey().prepopToilets(schoolNo, year - 1);
				var dataTable = (System.Data.DataTable)(result.ResultSet);
				foreach (System.Data.DataRow rw in dataTable.Rows)
				{
					foreach (string s in new[] { "M", "F", "All" })
					{
						string fieldPath = $"Toilets.D.{rw["ToiletType"]}.{rw["UsedBy"]}.{s}";
						xfdf.SetValue(fieldPath, rw[s]);
					}
				}

				//Water Supply
				result = Factory.PdfSurvey().prepopWaterSupply(schoolNo, year - 1);
				dataTable = (System.Data.DataTable)(result.ResultSet);
				foreach (System.Data.DataRow rw in dataTable.Rows)
				{
					foreach (string s in new[] { "Num", "Qty", "OK" })
					{
						string fieldPath = $"Resource.Water.D.{rw["R"]}.{s}";
						xfdf.SetValue(fieldPath, rw[s]);
					}
				}

				//Classrooms
				result = Factory.PdfSurvey().prepopClassrooms(schoolNo, year - 1);
				dataTable = (System.Data.DataTable)(result.ResultSet);
				int classTotal = 0;
				foreach (System.Data.DataRow rw in dataTable.Rows)
				{
					string fieldPath = $"Survey.ClassroomCount{rw["Material"]}.Num";
					xfdf.SetValue(fieldPath, rw["Num"]);
					classTotal += rw["Num"] is DBNull ? 0 : int.Parse(rw["Num"].ToString());
				}
				string fp = $"Survey.ClassroomCount";
				xfdf.SetValue(fp, classTotal);

				// set up the school, and survey year cannot be changed
				xfdf.SetValue("Survey.SchoolName", school.schName);
				xfdf.SetValue("Survey.SchoolNo", school.schNo);
				xfdf.SetValue("Survey.SurveyYear", year);

			}

			// open the pdf, push the xfdf and save to memory stream
			MemoryStream mstrmPdf = new MemoryStream();


			ReaderProperties rprops = new ReaderProperties()
				.SetPassword(System.Text.Encoding.ASCII.GetBytes("kiri"));
			WriterProperties wprops = new WriterProperties();
			//.SetCompressionLevel(CompressionConstants.NO_COMPRESSION)
			//.SetFullCompressionMode(false);

			using (FileStream fs = new FileStream(template, FileMode.Open, FileAccess.Read, FileShare.Read))
			using (PdfReader reader = new PdfReader(fs, rprops))
			using (PdfWriter writer = new PdfWriter(mstrmPdf, wprops))
			{
				reader.SetCloseStream(true);
				writer.SetCloseStream(false);
				using (PdfDocument pdfDocument = new PdfDocument(reader, writer))
				{
					pdfDocument.SetCloseWriter(false);

					// Create a MemoryStream and write the byte array to it
					using (System.IO.Stream xfdfStream = xfdf.ToStream())
					{
						XfdfObject xfdfData = new XfdfObjectFactory().CreateXfdfObject(xfdfStream);
						// push the xfdf into the document
						xfdfData.MergeToPdf(pdfDocument, template);
					}

					// Part 2 -- specific detailed finetuning
					// clean ups after the data is in place
					PdfAcroForm form = PdfAcroForm.GetAcroForm(pdfDocument, true);
					PdfOutline root = pdfDocument.GetOutlines(false); // Get the root outline

					int existingTeachers = teachers.Rows.Count;
					//*********************************************************
					// move the NewStaff bookmark
					string name = $"TL.{existingTeachers:00}.OnStaff";

					var fld = form.GetField(name);
					if (fld != null)
					{
						var w = fld.GetWidgets()[0];
						PdfPage p = w.GetPage();
						Rectangle wlocation = w.GetRectangle().ToRectangle();


						PdfOutline o = findBookmark(root, "New Staff");
						// we add the etra 22 to get the heading row in the view
						PdfExplicitDestination destination =
							PdfExplicitDestination.CreateXYZ(p, 0, wlocation.GetTop() + 22, 1);
						o.AddDestination(destination);
					}
					#region hide on staff button for new staff


					int i = existingTeachers;
					do
					{
						name = $"TL.{i:00}.OnStaff";

						fld = form.GetField(name);
						if (fld == null)
						{
							break;
						}

						foreach (var w in fld.GetWidgets())
						{
							w.SetFlag(PdfAnnotation.HIDDEN);
						}
						i++;
					} while (true);

					#endregion

					#region Bookmarks for expected Teachers
					//***********************************************
					// Create a bookmark for each expected teacher
					// first get the Expected Staff List node
					// **********************************************

					var ssi = findBookmark(root, "School Staff Information");
					var esl = ssi.GetAllChildren()[0];

					for (int j = 0; j < existingTeachers; j++)
					{
						var familyName = form.GetField($"TL.{j:00}.FamilyName");
						if (familyName == null)
						{
							break;  // there are more existing teachers than slots on the form
						}
						var firstName = form.GetField($"TL.{j:00}.FirstName");

						var w = familyName.GetWidgets()[0];
						PdfPage p = w.GetPage();
						Rectangle wlocation = w.GetRectangle().ToRectangle();
						name = $"{familyName.GetValue()}, {firstName.GetValue()}";
						PdfOutline teacherOutline = esl.AddOutline(name);
						PdfExplicitDestination destination =
							PdfExplicitDestination.CreateXYZ(p, 0, wlocation.GetTop() + 22, 1);
						teacherOutline.AddDestination(destination);
					}
					#endregion

					#region Lock existing teachers
					// 
					IDictionary<string, PdfFormField> fields = form.GetAllFormFields();
					var matchingFields = fields
						.Where(kvp => kvp.Key.StartsWith("TL.")
							&& int.TryParse(kvp.Key.Split('.')[1], out int nn)
							&& nn >= 0 && nn < existingTeachers)
						.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

					foreach (var kvp in matchingFields)
					{

						var tfld = kvp.Value;
						string id = kvp.Key.Split('.').Last();

						switch (tfld.GetFormType().GetValue())
						{
							case "Btn":
								if (id != "OnStaff")
								{
									tfld.SetReadOnly(true);
								}
								break;
							default:
								// other field type
								switch (id)
								{
									case "tID":
										break; // its hidden, doesn;t have any annotation or widget
									case "FamilyName":
									case "FirstName":
										// read-only, and white background
										tfld.SetReadOnly(true);
										tfld.GetFirstFormAnnotation().SetBackgroundColor(ColorConstants.WHITE);
										break;
									default:
										// everything else is readonly and grey
										tfld.SetReadOnly(true);
										tfld.GetFirstFormAnnotation().SetBackgroundColor(ColorConstants.LIGHT_GRAY);
										break;
								}
								break;
						}
						if (tfld is PdfChoiceFormField dropdown)
						{
							// this is a data fix that should bechecked in future if
							// it is still required.
							// the issue is that when the Code value gets pushed into
							// the form via xfdf, it does not change the display 
							// to the display value (it will change when the fields are
							// unlocked and hence regenerated)
							// Here we find the value in the Options string, and get its
							// corresponding description
							PdfArray options = dropdown.GetPdfObject().GetAsArray(PdfName.Opt);
							string key = dropdown.GetValueAsString();
							string displayValue = null;

							foreach (PdfObject option in options)
							{
								if (option.IsArray())
								{
									PdfArray keyValuePair = (PdfArray)option;
									string optionKey = keyValuePair.GetAsString(0).ToString();
									if (key == optionKey)
									{
										displayValue = keyValuePair.GetAsString(1).ToString();
										break;
									}
								}
							}
							if (displayValue != null)
							{
								dropdown.SetValue(displayValue);
							}

						}
					}
					#endregion

					#region tidy dropdown lists

					#endregion

					#region left and right footers
					int idx = 2; // Footers start at page 2 since iText numbers from 1
					string surveyFooter = $"{pdfDocument.GetDocumentInfo().GetTitle()} {year}";
					string schFooter = $"{school.schNo} {school.schName}";
					while (true)
					{
						// Construct the field name with zero-padded index
						string fieldName = $"Footer.{idx:00}";

						// Retrieve the field from the form
						if (!form.GetAllFormFields().TryGetValue(fieldName, out PdfFormField field))
						{

							break;
						}

						// Determine the page number associated with the field
						int pageNumber = pdfDocument.GetPageNumber(field.GetWidgets()[0].GetPage());

						// Set the field value based on whether the page number is even or odd
						if (pageNumber % 2 == 1) // odd page number
						{
							if (field.GetValueAsString() != surveyFooter)
							{
								field.SetValue(surveyFooter);
							}
						}
						else // Odd page number
						{
							if (field.GetValueAsString() != schFooter)
							{
								field.SetValue(schFooter);
							}
						}

						idx++;
					}
					#endregion

					form.SetNeedAppearances(false);
					//lock these fields 
					form.GetField("Survey.SchoolNo").SetReadOnly(true)
						.GetFirstFormAnnotation().SetBackgroundColor(null);

					form.GetField("Survey.SchoolName").SetReadOnly(true)
						.GetFirstFormAnnotation().SetBackgroundColor(null);

					StandardizeCheckboxes(pdfDocument);


				} // dispose the document
				reader.Close();
			} //Dispose the writer and writer


			mstrmPdf.Position = 0;
			// Create the response message with the file data
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = new StreamContent(mstrmPdf)
			};

			// Set the content type and disposition
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
			response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = $"{school.schNo} {school.schName} {Context} Survey {year}.pdf"
			};

			return response;
		}

		private PdfOutline findBookmark(PdfOutline root, String title)
		{
			foreach (PdfOutline outline in root.GetAllChildren())
			{
				if (title.Equals(outline.GetTitle()))
				{
					return outline;
				}
				PdfOutline found = findBookmark(outline, title);
				if (found != null)
				{
					return found;
				}
			}
			return null;
		}

		[HttpGet]
		[Route(@"fix/{year}")]
		[AllowAnonymous]
		public async Task<HttpResponseMessage> FixChecks(int year, string version = "PRI")
		{

			string template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/pdfsurvey/{Context}/{Context} {year} {version}.pdf");

			//template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/pdfsurvey/{Context}/kiri2024.pdf");

			if (!System.IO.File.Exists(template))
			{
				//legacy fallback
				template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/pdfSurvey/KEMIS/KEMIS 2024 PRI.PDF");
			}

			MemoryStream mstrmPdf = new MemoryStream();

			ReaderProperties rprops = new ReaderProperties()
				.SetPassword(System.Text.Encoding.ASCII.GetBytes("kiri"));
			WriterProperties wprops = new WriterProperties()
				.SetCompressionLevel(CompressionConstants.NO_COMPRESSION)
				.SetFullCompressionMode(false);


			using (PdfReader reader = new PdfReader(new FileStream
				(template, FileMode.Open, FileAccess.Read, FileShare.Read), rprops))
			using (PdfWriter writer = new PdfWriter(mstrmPdf, wprops))
			{
				reader.SetCloseStream(true);
				writer.SetCloseStream(false);
				using (PdfDocument pdfDocument = new PdfDocument(reader, writer))
				{
					pdfDocument.SetCloseWriter(false);
					PdfAcroForm form = PdfAcroForm.GetAcroForm(pdfDocument, false);

					//this section will fix up checkboxes that are not numbered correctly for their group
					// e.g numbered T.00.Duties but should be T.10.Duties
					// this targets specifically KEMIS surveys with 5 teachers per page new staff or 15 eisting staff
					// exisitng staff was resolved earlier in the pipeline, suppport here is for future insurance
					const int NUM_TEACHERS_PER_PAGE_T = 5;
					const int NUM_TEACHERS_PER_PAGE_TL = 15;

					// change this to reactivate this code
					string[] sectionsToCheck = { }; //{ "T" ,"TL"};

					foreach (string section in sectionsToCheck)
					{
						int tpp = (section == "T" ? NUM_TEACHERS_PER_PAGE_T : NUM_TEACHERS_PER_PAGE_TL);

						string[] fieldsToCheckT = { "Duties", "Gender", "Role", "House", "FP" };
						string[] fieldsToCheckTL = { "Duties", "Gender", "OnStaff", "House" };

						string[] fieldsToCheck = (section == "T" ? fieldsToCheckT : fieldsToCheckTL);
						foreach (string fieldName in fieldsToCheck)
						{


							for (int j = 0; j < tpp; j++)
							{
								int pageIndex = 1;

								do
								{
									// use familyname field to check if the page exists with page number pageIndex
									// and if so, get the page object
									var tFamily = form.GetField($"{section}.{pageIndex * tpp + j:D2}.FamilyName");

									if (tFamily == null)
									{
										break;      // exit this do - no more pages
									}
									// get the page from the FmilyName field, and get the Indirect Reference Number for that page
									var dic2 = tFamily.GetPdfObject();
									var targetpage = dic2.Get(PdfName.P); //the page object containing the incorrect fields
									int targetPageNo = targetpage.GetIndirectReference().GetObjNumber();

									// set up the source field ie the field on the first page
									string sourceName = $"{section}.{j:D2}.{fieldName}";
									var tSource = form.GetField(sourceName);
									//var widg = new PdfArray();

									// the tagret field is the one missing widgets, becuase they are numbered incorrectly
									string targetName = $"{section}.{pageIndex * tpp + j:D2}.{fieldName}";
									var tTarget = form.GetField(targetName);

									bool changeFound = false;
									foreach (var k in tSource.GetWidgets())
									{
										// check that the page the widget is on is the page containing the target field
										if (k.GetPage().GetPdfObject().GetIndirectReference().GetObjNumber()
											== targetPageNo
										)
										{
											// the widget is on the page we are testing
											// it should be part of the group under tTarget
											changeFound = true;
											Debug.WriteLine($"{j} {pageIndex} {sourceName} {targetName}");
											k.SetParent(tTarget.GetPdfObject());
											tTarget.AddKid(k);
											tSource.GetKids().Remove(k.GetPdfObject());
										}
									}
									if (changeFound)
									{
										// this makes visual how the widgets are being reallocated from the Kids collection
										// of the source field
										Debug.WriteLine($"Target: {targetName} {tTarget.GetKids()}");
										Debug.WriteLine($"Source: {sourceName} {tSource.GetKids()}");
									}
									pageIndex++;
								} while (true);  // break from this loop when we can find no more pages of T / TL fields
							}

						}
					}
					// this section unscrambles Checkbox fields that are incorrectly nested.
					// For example
					// FIELD
					// -- Widget 1
					// -- FIELD
					// -----Widget 2
					// should be
					// FIELD
					// -- Widget 1
					// ---Widget 2

					var ifielddic = form.GetAllFormFields().Keys
						.Where(n => form.GetField(n).GetType().Equals(typeof(PdfButtonFormField)))
						.ToArray();

					foreach (string n in ifielddic)
					{
						PdfButtonFormField btn = form.GetField(n) as PdfButtonFormField;

						var children = btn.GetChildFormFields().OfType<PdfFormField>().ToArray<PdfFormField>();
						if (children.Count() > 0)
						{

							foreach (var sub in children)
							{
								var grandchildren = sub.GetChildFields().ToArray();
								if (grandchildren.Count() == 1
									&& grandchildren[0].GetPdfObject().GetIndirectReference().GetObjNumber()
									== sub.GetPdfObject().GetIndirectReference().GetObjNumber()
									)
								{
									// self reference
									Debug.WriteLine($"{btn.GetFieldName()} LOOP");
									PdfDictionary dic = sub.GetPdfObject();
									dic.Remove(PdfName.H);
									dic.Remove(PdfName.DA);

									sub.SetParent(btn);
								}
								else
								{
									Debug.WriteLine(btn.GetFieldName());
									btn.RemoveChild(sub);
									foreach (var widg in grandchildren)
									{
										widg.SetParent(btn);
										btn.AddKid(widg);
									}
								}
							}

						}
					}
					// for debugging
					for (int i = 0; i < 60; i++)
					{
						string n = $"TL.{i:D2}.Gender";
						var fld = form.GetField(n);
						Debug.WriteLine($"{n} ChildFields: {fld.GetChildFields().Count()} Kids: {fld.GetKids().Count()} {fld.GetKids().ToString()}");

					}
					form.SetNeedAppearances(false);
					// now standardise all the checkboxes - we run through the collection again
					StandardizeCheckboxes(pdfDocument);
					// reduce the drop down lists
					StandardiseCombos(pdfDocument);
				}
				reader.Close();
			}
			mstrmPdf.Position = 0;
			// Create the response message with the file data
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = new StreamContent(mstrmPdf)
			};

			// Set the content type and disposition
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
			response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = System.IO.Path.GetFileName(template)
			};
			return response;

		}

		#region xfdf Manipulation

		#endregion

		#region itext support

		private static PdfDocument StandardizeCheckboxes(PdfDocument pdfDocument)
		{
			// make the default Y and N appearances
			// Load ZapfDingbats font for the symbols
			PdfFont dingbatFont = PdfFontFactory.CreateFont(StandardFonts.ZAPFDINGBATS);


			var tickAppearance = PdfForm.CheckSymbol(pdfDocument, dingbatFont
									, PdfDingbat.Tick, new DeviceRgb(0, 128, 0)); // Tick (✓) - green
			var crossAppearance = PdfForm.CheckSymbol(pdfDocument, dingbatFont
									, PdfDingbat.Cross, new DeviceRgb(128, 0, 0));   // Cross (✗) - red
			var uncheckedAppearance = PdfForm.CheckEmpty(pdfDocument); // Empty square

			// Down (/D) versions
			var tickAppearanceDown = PdfForm.CheckSymbolDown(pdfDocument, dingbatFont, PdfDingbat.Tick); // Tick (✓) - green
			var crossAppearanceDown = PdfForm.CheckSymbolDown(pdfDocument, dingbatFont, PdfDingbat.Cross);
			var uncheckedAppearanceDown = PdfForm.CheckEmptyDown(pdfDocument); // Empty squa

			var tickMK = PdfForm.CheckMK(PdfDingbat.Tick);
			var crossMK = PdfForm.CheckMK(PdfDingbat.Cross);
			var blockMK = PdfForm.CheckMK(PdfDingbat.Square);

			tickAppearance.MakeIndirect(pdfDocument);
			crossAppearance.MakeIndirect(pdfDocument);
			uncheckedAppearance.MakeIndirect(pdfDocument);
			tickAppearanceDown.MakeIndirect(pdfDocument);
			crossAppearanceDown.MakeIndirect(pdfDocument);
			uncheckedAppearanceDown.MakeIndirect(pdfDocument);

			// BLOCKS

			PdfFormXObject GoldBlockAppearance = PdfForm.CheckBlock(pdfDocument, new DeviceRgb(255, 215, 0));   // 
			PdfFormXObject BlackBlockAppearance = PdfForm.CheckBlock(pdfDocument, new DeviceRgb(0, 0, 0));
			PdfFormXObject GreyBlockAppearance = PdfForm.CheckBlock(pdfDocument, new DeviceRgb(96, 96, 96));
			PdfFormXObject RedBlockAppearance = PdfForm.CheckBlock(pdfDocument, new DeviceRgb(128, 0, 0));
			PdfFormXObject BlockAppearanceDown = PdfForm.CheckBlockDown(pdfDocument);

			GoldBlockAppearance.MakeIndirect(pdfDocument);
			BlackBlockAppearance.MakeIndirect(pdfDocument);
			GreyBlockAppearance.MakeIndirect(pdfDocument);
			RedBlockAppearance.MakeIndirect(pdfDocument);
			BlockAppearanceDown.MakeIndirect(pdfDocument);


			PdfAcroForm form = PdfAcroForm.GetAcroForm(pdfDocument, true);
			var ifielddic = form.GetAllFormFields();
			foreach (string n in ifielddic.Keys)
			{
				PdfFormField f = ifielddic[n];

				if (f is PdfButtonFormField btn)
				{
					if ((btn.GetFieldFlags() & 0x10000) == 0) // not a push button
					{
						string[] apstates = btn.GetAppearanceStates();

						foreach (var widg in btn.GetWidgets())
						{
							//SizeWidget(widg, 13, 13);

							PdfDictionary appearanceDict = widg.GetAppearanceDictionary();
							// Check if the appearance dictionary has normal appearances (default visual state)
							PdfDictionary currentNormal = widg.GetNormalAppearanceObject();
							PdfDictionary normalAppearance = new PdfDictionary();
							PdfDictionary downAppearance = new PdfDictionary();

							PdfDictionary mk = new PdfDictionary();


							{
								//	some GOOD FAIR POOR appear as G F P
								//	Resource provision NONE SOME..... ALL
								//	Make sure the blocks are all correcllty colored
								//	

								foreach (PdfName key in currentNormal.KeySet())
								{
									string keyValue = key.GetValue();

									switch (keyValue)
									{
										case "Off":
											normalAppearance.Put(key, uncheckedAppearance.GetPdfObject());
											break;
										case "Y":
											normalAppearance.Put(key, tickAppearance.GetPdfObject());
											break;
										case "N":
											normalAppearance.Put(key, crossAppearance.GetPdfObject());
											break;

										// GOOD FAIR POOR // G F P // NONE SOME ALLL...
										// note FAIR, SOME etc are all Grey Blocks (default)
										case "G":
										case "GOOD":
										case "ALL":
											normalAppearance.Put(key, GoldBlockAppearance.GetPdfObject());
											break;
										case "P":
										case "POOR":
										case "NONE":
											normalAppearance.Put(key, RedBlockAppearance.GetPdfObject());
											break;
										case "F":
										case "FAIR":
											normalAppearance.Put(key, GreyBlockAppearance.GetPdfObject());
											break;
										default:
											//everything else is a grey block
											normalAppearance.Put(key, GreyBlockAppearance.GetPdfObject());
											break;
									}
									// treat down appearance
									switch (keyValue)
									{
										case "Off":
											downAppearance.Put(key, uncheckedAppearanceDown.GetPdfObject());
											break;

										case "Y":
											downAppearance.Put(key, tickAppearanceDown.GetPdfObject());
											mk = tickMK;
											break;
										case "N":
											downAppearance.Put(key, crossAppearanceDown.GetPdfObject());
											mk = crossMK;
											break;

										default:
											// all of these blocks, whatever colour, use the same down state
											downAppearance.Put(key, BlockAppearanceDown.GetPdfObject());
											mk = blockMK;
											break;
									}

								}
								// appply the new appearances to the widget
								appearanceDict.Put(PdfName.N, normalAppearance);
								appearanceDict.Put(PdfName.D, downAppearance);      // for now make the Down appearance the same
								widg.SetAppearanceCharacteristics(mk);
							}
						}
					}
				}
			}
			return pdfDocument;
		}

		private static void SizeWidget(PdfWidgetAnnotation widg, int newWidth, int newHeight)
		{
			// Set the resized rectangle back to the widget
			PdfArray coords = SizePdfRectangle(widg.GetRectangle(), newWidth, newHeight);
			widg.SetRectangle(coords);
		}


		private static PdfArray SizePdfRectangle(PdfArray coords, int newWidth, int newHeight)
		{


			// Resize the rectangle (e.g., increase width and height by 10 units)
			float llx = coords.GetAsNumber(0).FloatValue(); // lower-left x
			float lly = coords.GetAsNumber(1).FloatValue(); // lower-left y
			float urx = coords.GetAsNumber(2).FloatValue(); // upper-right x
			float ury = coords.GetAsNumber(3).FloatValue(); // upper-right y

			// Create the resized rectangle
			PdfArray resizedRec = new PdfArray();
			resizedRec.Add(new PdfNumber(llx)); // lower-left x (unchanged)
			resizedRec.Add(new PdfNumber(lly)); // lower-left y (unchanged)
			resizedRec.Add(new PdfNumber(llx + newWidth)); // new upper-right x
			resizedRec.Add(new PdfNumber(lly + newHeight)); // new upper-right y

			return resizedRec;
		}

		private void StandardiseCombos(PdfDocument pdfDocument)
		{

			PdfObject qualOpts = null;
			PdfObject edqualOpts = null;
			PdfObject roleOpts = null;

			PdfObject citizenshipOpts = null;
			PdfObject maritalStatusOpts = null;
			PdfObject paidByOpts = null;
			PdfObject empStatusOpts = null;
			PdfObject classOpts = null;

			var form = PdfAcroForm.GetAcroForm(pdfDocument, false);
			var ch = form.GetField("TL.00.Qual");
			if (ch != null)
			{
				qualOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}

			if ((ch = form.GetField("TL.00.QualEd")) != null)
			{
				edqualOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}
			if ((ch = form.GetField("TL.00.Role")) != null)
			{
				roleOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}
			if ((ch = form.GetField("T.00.MaritalStatus")) != null)
			{
				maritalStatusOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}
			if ((ch = form.GetField("T.00.Citizenship")) != null)
			{
				citizenshipOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}
			if ((ch = form.GetField("T.00.Class.Min")) != null)
			{
				classOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}
			if ((ch = form.GetField("T.00.EmpStatus")) != null)
			{
				empStatusOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}
			if ((ch = form.GetField("T.00.PaidBy")) != null)
			{
				paidByOpts = ch.GetPdfObject().GetAsArray(PdfName.Opt).MakeIndirect(pdfDocument);
			}

			int teachers = form.GetField("TL").GetChildFields().Count;
			for (int i = 0; i < teachers; i++)
			{
				PdfDictionary z = form.GetField($"TL.{i:D2}.Qual").GetPdfObject();
				z.Put(PdfName.Opt, qualOpts);
				z = form.GetField($"TL.{i:D2}.QualEd").GetPdfObject();
				z.Put(PdfName.Opt, edqualOpts);
				z = form.GetField($"TL.{i:D2}.Role").GetPdfObject();
				z.Put(PdfName.Opt, roleOpts);
			}

			// new staff
			Regex regex = new Regex(@"T\.\d{2}");

			teachers = form.GetField("T").GetChildFormFields()
				.Where(chld => (regex.IsMatch(chld.GetFieldName().ToString()))).Count();
			for (int i = 0; i < teachers; i++)
			{
				form.GetField($"T.{i:D2}.Qual").GetPdfObject().Put(PdfName.Opt, qualOpts);
				form.GetField($"T.{i:D2}.QualEd").GetPdfObject().Put(PdfName.Opt, edqualOpts);

				form.GetField($"T.{i:D2}.MaritalStatus").GetPdfObject().Put(PdfName.Opt, maritalStatusOpts);
				form.GetField($"T.{i:D2}.Citizenship").GetPdfObject().Put(PdfName.Opt, citizenshipOpts);
				form.GetField($"T.{i:D2}.Class.Min").GetPdfObject().Put(PdfName.Opt, classOpts);
				form.GetField($"T.{i:D2}.Class.Max").GetPdfObject().Put(PdfName.Opt, classOpts);

				form.GetField($"T.{i:D2}.EmpStatus").GetPdfObject().Put(PdfName.Opt, empStatusOpts);
				form.GetField($"T.{i:D2}.PaidBy").GetPdfObject().Put(PdfName.Opt, paidByOpts);
			}

		}
		#endregion

		#region utilities

		private string GetSchoolForm(Pineapples.Data.Models.School school, int year)
		{
			string formType = "SEC";
			switch (school.schType)
			{
				case "P":
					formType = "PRI";
					break;
				case "ECE":
					formType = "ECE";
					break;

			}
			string template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/pdfsurvey/{Context}/{Context} {year} {formType}.pdf");
			//template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/pdfsurvey/{Context}/kiri2024.pdf");
			if (!System.IO.File.Exists(template))
			{
				//legacy fallback
				template = System.Web.Hosting.HostingEnvironment.MapPath($"~/assets/pdfSurvey/KEMIS/KEMIS 2024 PRI.PDF");
			}
			return template;
		}
		#endregion
	}

}

