﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

using Newtonsoft.Json.Linq;
using DataLayer = Pineapples.Data.DataLayer;

namespace Pineapples.Controllers
{
    [Authorize]
    public class GeoController : PineapplesApiController
    {
        public GeoController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpGet]
        [Route("api/geo/topo/{id}")]
        public object TopoJson( string id)
        {
            // this method will return a preexisting topojson object from the assets/maps folder
            string path = "~/assets_local/maps/world.topo.json";
            switch (id)
            {
                case "world":
                    path = "~/assets_local/maps/world.topo.json";
                    break;
                case "adm0":
                case "countries":
                    path = "~/assets_local/maps/adm0.topo.json";
                    break;

                case "adm1":
                    // first administrative levels
                    path = "~/assets_local/maps/adm1.topo.json";
                    break;
                case "adm2":
                    path = "~/assets_local/maps/adm2.topo.json";
                    break;
            }
            path = System.Web.HttpContext.Current.Server.MapPath(path);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new System.IO.FileStream(path, System.IO.FileMode.Open,System.IO.FileAccess.Read);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = System.IO.Path.GetFileName(path);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            result.Content.Headers.ContentLength = stream.Length;
            return result;
        }


    }
}
