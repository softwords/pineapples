﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSStudentDeduper 
    {
        IDataResult Filter(StudentDeduperFilter fltr);
		IDataResult Surveys(Guid id1, Guid id2);

		// perform the merge
		IDataResult Merge(Guid targetID, Guid sourceID);
	}
}
