﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSGovtExpenditure
    {
		Task<IDataResult> Read(int inspectionID);

		// may consider this for state level data?
		//void AccessControl(int ID, ClaimsIdentity identity);
		IDataResult Filter(GovtExpenditureFilter fltr);
	}
}
