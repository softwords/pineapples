﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;

namespace Pineapples.Data
{
	public interface IDSWarehouse
	{
		IDataResult TableEnrol(bool reportFormat = false);
		IDataResult TableEnrolX(bool reportFormat = false);

		// flow rates
		/// <summary>
		/// all flow rate data for a specified school
		/// </summary>
		/// <param name="schoolNo"></param>
		/// <returns></returns>
		IDataResult SchoolFlowRates(string schoolNo);
		// flow rates
		/// <summary>
		/// all flow rate data for all schools
		/// </summary>
		/// <param name="schoolNo"></param>
		/// <returns></returns>
		IDataResult AllSchoolFlowRates();

		IDataResult ClassLevelER();
		IDataResult EdLevelER();
		IDataResult FlowRates();

		#region Teachers
		IDataResult SchoolTeacherPupilRatio();

		//deprecated 
		IDataResult TeacherCount();
		//deprecated 
		IDataResult TeacherCountX();
		// deprecated
		IDataResult AllSchoolTeacherCount();
		//deprecated
		IDataResult SchoolTeacherCount(string schoolNo);

		IDataResult TeacherCount(string grouping = null, string groupValue = null
									, bool? report = false);

		IDataResult TeacherQual();
		// teacher pupil ratio is deprecated - use PupilTeacherRatio
		IDataResult TeacherPupilRatio();
		// More general Teacher data results (i.e. instead of TeacherActivityNation, TeacherJobDistrict, etc.)
		IDataResult TeacherData(string content = null, string grouping = null, string filterCode = null
							, bool? report = false, string xl = ""
							, bool byActivity = false, int? selectedYear = null);
		#endregion

		#region Enrolment      
		IDataResult EnrolDistrict(string selectedDistrict = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);
		IDataResult EnrolAuthority(string selectedAuthority = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);
		IDataResult EnrolSchoolType(string selectedSchoolType = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);
		IDataResult EnrolElectorateL(string selectedElectorate = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);
		IDataResult EnrolElectorateN(string selectedElectorate = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);
		IDataResult EnrolIsland(string selectedIsland = null
										, bool reportFormat = false
										, bool byAge = false
	 									, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);
		IDataResult EnrolRegion(string selectedRegion = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);

		IDataResult EnrolNation(bool reportFormat = false
								, bool byAge = false
								, bool byClassLevel = false
								, int? selectedYear = null
								, string selectedClassLevel = null);


		IDataResult EnrolSchool(string selectedSchool = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null);

		IDataResult EnrolISCED(int year);
		IDataResult EnrolGridMaker(string groupBy, int? startYear, int? endYear, string filter, string dataItem,
			int? ignoreAge, int? rowTotalsOnly);
		#endregion

		#region Flow
		IDataResult FlowSchool(string selectedSchool = null
										, bool reportFormat = false
										, bool asPerc = false);

		IDataResult FlowDistrict(string selectedDistrict = null
										, bool reportFormat = false
										, bool asPerc = false);

		IDataResult FlowNation(bool reportFormat = false
								, bool asPerc = false);
		#endregion flow

		#region Accreditations

		IDataResult Accreditations(string content = null
										, string grouping = null, string groupValue = null
										, bool? report = false);

		IDataResult AccreditationsFilter(SchoolAccreditationFilter fltr);
		#endregion

		#region EdLevelAge

		/**
		 * Report on edLevelAge and class level official / over/ under age
		 * Grouping  null, district school
		 */
		IDataResult EdLevelAge(string grouping = null, string groupValue = null
									, bool? report = false);

		// original custom sequel 
		IDataResult EdLevelAge();

		#endregion

		#region PupilTeacherRatio
		/**
		 * Report on edLevelAge and class level official / over/ under age
		 * Grouping  null, district school
		 */
		IDataResult PupilTeacherRatio(string grouping = null, string groupValue = null
									, bool? report = false);



		#endregion
		#region Wash

		IDataResult Wash(string content = null
										, string grouping = null, string groupValue = null
										, bool? report = false);


		IDataResult WashFilter(SchoolAccreditationFilter fltr);

		IDataResult WashQuestions();

		IDataResult WashResponses(WashResponsesFilter fltr);

		IDataResult WashToilets();

		IDataResult WashWater();
		#endregion

		#region FinancialData

		IDataResult EdExpenditure(string grouping = null
							, string groupValue = null
							, bool? report = false);
		#endregion


		#region Special Education
		IDataResult SpecialEducation(bool ReportFormat = false);
		#endregion


		#region Scholarships
		IDataResult ScholarshipRounds();

		IDataResult ScholarshipPayments();
		#endregion

		#region Exams
		/// <summary>
		/// 'typed' results (ie totals for indicatorcount, candidateCount, weighted count) 
		/// with record types for Standard, Benchmark, Indicator
		/// </summary>
		/// <returns></returns>
		IDataResult ExamTableTyped();

		/// <summary>
		/// An extended version of ExamTableTyped which has additional disaggregation support 
		/// (e.g. Island, Region) but will have a lot more data and thus will come with a penalty hit 
		/// </summary>
		/// <returns></returns>
		IDataResult ExamTableTypedX();

		// exam methods
		/// <summary>
		/// return all data from Exam School Results - for a single school
		/// </summary>
		/// <returns></returns>
		IDataResult ExamSchoolResults(string schoolNo,
			bool reportFormat = false,
			bool report = false);
		/// <summary>
		/// return all data from Exam School Results - for every school
		/// deprecated
		/// </summary>
		/// <returns></returns>
		IDataResult ExamAllSchoolResults();

		/// <summary>
		/// return all data from Exam School Results - consolidated by district
		/// </summary>
		/// <returns></returns>
		IDataResult ExamDistrictResults(string district = null,
			bool reportFormat = false,
			bool report = false);


		//DEPRECATED
		// v1 version is used by first exam dashboard implementation
		IDataResult ExamDistrictResultsv1();

		/// <summary>
		/// return a string repesenting the version of the warehouse
		/// This is passed to clients as an etag
		/// </summary>
		/// <returns></returns>
		/// 
		#endregion	Exams

		#region Disability

		IDataResult DisabilitySchool(string selectedSchoolNo = null
								, bool reportFormat = false
								, bool byAge = false
								, bool byClassLevel = false);
		#endregion Disability

		#region Dimensions

		IDataResult DimensionLevel();

		#endregion
		string WarehouseVersion();

	}
}
