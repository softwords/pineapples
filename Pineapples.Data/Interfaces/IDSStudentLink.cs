﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using Pineapples.Data.Models;
using Softwords.Web.Models;
using System.Security.Claims;

namespace Pineapples.Data
{
	public interface IDSStudentLink : IDSCrud<StudentLinkBinder>
	{
		IDataResult Filter(StudentLinkFilter fltr);
	}
}
