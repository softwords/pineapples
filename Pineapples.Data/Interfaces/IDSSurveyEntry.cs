﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;

namespace Pineapples.Data
{
    public interface IDSSurveyEntry
    {
        IDataResult PupilTable(string schoolNo, int year, string tableName);

        IDataResult SavePupilTable(PupilTable pupilTable, string user);


        object ResourceList(string schoolNo, int year, string category);

        IDataResult SaveResourceList(ResourceList resourceList, string user);

        /// <summary>
        /// CensusWorkbook interfaces are for upload from the FSM excel based data collection tools
        /// </summary>
        /// <param name="listObject">an Xml object derived from the source Excel list</param>
        /// <param name="fileReference">guid representing the source file in the FileDB</param>
        /// <param name="user">user making the upgrade</param>
        /// <returns></returns>
        Task<IDataResult> CensusWorkbookSchoolsAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> CensusWorkbookStudentsAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> CensusWorkbookStaffAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> CensusWorkbookWashAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> CensusWorkbookValidateSchoolsAsync(XDocument schoolList);
        Task<IDataResult> CensusWorkbookValidateStaffAsync(XDocument listObject);
		Task<IDataResult> CensusWorkbookValidateStudentsAsync(XDocument listObject);
		Task<IDataResult> CensusWorkbookValidateWashAsync(XDocument listObject);
		Task<IDataResult> CensusWorkbookOutputAsync(Guid fileReference);

        /// <summary>
        /// Utiliity to copy the Xml from a census workbook on to SchoolSurvey
        /// </summary>
        /// <param name="schoolXml"></param>
        /// <param name="washXml"></param>
        /// <returns></returns>
        Task<IDataResult> SaveXmlToSurvey(XDocument schoolXml, XDocument washXml, Guid fileReference);

        Task<Dictionary<string, string>> CensusWorkbookColumnMapping(string sheet);

        /// <summary>
        /// CensusRollover interfaces are for generating the 'rollover' into a new book
        /// from exisitng school, staff and student datat
        /// </summary>
        /// <param name="year"> the survey year to generate
        /// <param name="fileReference">guid representing the source file in the FileDB</param>
        /// <param name="user">user making the upgrade</param>
        /// <returns></returns>
        /// 

        Task<IDataResult> CensusRollover(int year, string schoolNo, string district);
        Task<IDataResult> CensusRolloverSchools(int year, string schoolNo, string district);
        Task<IDataResult> CensusRolloverStaff(int year, string schoolNo, string district);
        Task<IDataResult> CensusRolloverStudents(int year, string schoolNo, string district);
        Task<IDataResult> CensusRolloverWash(int year, string schoolNo, string district);
    }
}
