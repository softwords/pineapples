﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;


namespace Pineapples.Data
{
	public interface IDSSpecialEdStudent : IDSCrudAsync<StudentBinder, Guid?>
	{
		IDataResult Filter(SpecialEdStudentFilter fltr);
		IDataResult Table(string rowsplit, string colsplit, SpecialEdStudentFilter fltr);
		IDataResult Geo(string geoType, SpecialEdStudentFilter fltr);

		//// SpecialEdStudent has a specialised Read
		//IDataResult Read(Guid? studentID, bool readX);

		// access control
		void AccessControl(Guid studentID, ClaimsIdentity identity);
	}
}
