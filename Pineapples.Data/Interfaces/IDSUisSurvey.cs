﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
    public interface IDSUisSurvey
    {
        IDataResult AllSheets(int year);
        IDataResult A2(int year);
        IDataResult A3(int year);
        IDataResult A5(int year);
        IDataResult A6(int year);
        IDataResult A9(int year);
        IDataResult A10(int year);
        IDataResult A11(int year);
        IDataResult A12(int year);
        IDataResult A13(int year);

        IDataResult TeacherAudit(int year);
        IDataResult EnrolAudit(int year);

        IDataResult SchoolAudit(int year);

    }
}