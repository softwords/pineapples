﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Pineapples.Data.Models;


namespace Pineapples.Data
{
    public interface IDSScholarship
    {
		Task<IDataResult> Read(int? scholarshipID);
		Task<IDataResult> ReadAudit(int? scholarshipID);
		Task<IDataResult> NewFromStudent(Guid studentID);

		IDataResult Filter(ScholarshipFilter fltr);
		IDataResult Table(string rowsplit, string colsplit, ScholarshipFilter fltr);

		IDataResult StudyDefaults(int scholarshipID);
		void AccessControl(int scholarshipID, ClaimsIdentity identity);
	}
}
