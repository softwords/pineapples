﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSPdfSurvey 
    {
        object xfdfFunding(int? surveyID, XElement sectionxml);
        object xfdfParentCommittee(int? surveyID, XElement sectionxml);
        object xfdfDistanceTransport(int? surveyID, XElement sectionxml);
        object xfdfDisabilityNA(int? surveyID, XElement sectionxml);
        object xfdfEnrolment(int? surveyID, XElement sectionxml);
        object xfdfSite(int? surveyID, XElement sectionxml);
        object xfdfHousing(int? surveyID, XElement sectionxml);
        object xfdfPreschool(int? surveyID, XElement sectionxml);
        object xfdfSupplies(int? surveyID, XElement sectionxml);
         object xfdfToilets(int? surveyID, XElement sectionxml); 
        object xfdfWater(int? surveyID, XElement sectionxml);

        object xfdfTeachers(int? surveyID, XElement sectionxml);
        object xfdfRooms(int? surveyID, XElement sectionxml);
        object xfdfResourceList(int? surveyID, XElement sectionxml);
        object xfdfGrid(int? surveyID, XElement sectionxml);
        object xfdfClass(int? surveyID, XElement sectionxml);
        void xfdfError(int surveyID, string section, Exception ex);


        int? xfdfSurvey(XElement sectionxml);
        bool isTableDef(string pupiltablecode);

        string xfdfLogSurveyDocument(int? surveyID, string fileId, string extension, XDocument xfdf, ClaimsIdentity user);

        // prepopulation interfaces
        object prepopSchool(string schoolNo, int year);
		IDataResult prepopTeachers(string schoolNo, int year);
		IDataResult prepopToilets(string schoolNo, int year);
		IDataResult prepopClassrooms(string schoolNo, int year);
		IDataResult prepopWaterSupply(string schoolNo, int year);

	}
}
