﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pineapples.Data.DataLayer
{
	public interface IDSFactory
	{
		Pineapples.Data.DB.PineapplesEfContext Context { get; }
		IDSPerfAssess PerfAssess();
		IDSSchool School();
		IDSSchoolLink SchoolLink();
		IDSLookups Lookups();
		IDSSchoolScatter SchoolScatter();
		IDSTeacher Teacher();
		IDSTeacherDeduper TeacherDeduper();
		IDSTeacherLink TeacherLink();
		IDSExam Exam();
		IDSTeacherExam TeacherExam();
		IDSSelectors Selectors();
		IDSStudent Student();
		IDSStudentLink StudentLink();
		IDSScholarship Scholarship();
		IDSScholarshipLink ScholarshipLink();
		IDSStudentDeduper StudentDeduper();
		IDSSpecialEdStudent SpecialEdStudent();
		IDSSurvey Survey();

		IDSCensus Census();
		IDSUisSurvey UisSurvey();
		IDSSurveyEntry SurveyEntry();
		IDSBooks Books();
		IDSQuarterlyReport QuarterlyReport();
		IDSQuarterlyReportX QuarterlyReportX();
		IDSSchoolAccreditation SchoolAccreditation();
		IDSSchoolInspection SchoolInspection();
		IDSIndicators Indicators();
		IDSDocument Document();
		IDSKobo Kobo();
		IDSWarehouse Warehouse();
		IDSGovtExpenditure GovtExpenditure();
		IDSPdfSurvey PdfSurvey();
	}
}
