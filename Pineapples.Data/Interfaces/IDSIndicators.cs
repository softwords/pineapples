﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;        // really need to change this interface
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSIndicators
    {
		/// <summary>
		/// Construct and store the indicator table, from warehouse data
		/// </summary>
		/// <param name="year">first year to include</param>
		/// <returns>indicator table</returns>
        Task<System.Xml.Linq.XDocument> makeIndicatorTable(int? year);

		/// <summary>
		/// Construct and store the indicator table for a district
		/// </summary>
		/// <param name="district"></param>
		/// <param name="year"></param>
		/// <returns></returns>
        Task<System.Xml.Linq.XDocument> makeIndicatorTable(string domain, int? year);

        Task<IDataResult> makeWarehouse(int? year);

		/// <summary>
		/// return a string repesenting the version of the warehouse
		/// This is passed to clients as an etag
		/// </summary>
		/// <returns></returns>
		string WarehouseVersion();
		string WarehouseVersion(string district);


		Task<System.Xml.Linq.XDocument> readIndicatorTable();
		Task<System.Xml.Linq.XDocument> readIndicatorTable(string domain);
	}
}
