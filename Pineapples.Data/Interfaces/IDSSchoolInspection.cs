﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSSchoolInspection 
    {
		string InspectionResult(int inspectionID);
		IDataResult RegisterInspectionPhotos(int inspectionID);
		IDataResult Filter(SchoolInspectionFilter fltr);
		IDataResult Read(int inspectionID);

		void AccessControl(int ID, ClaimsIdentity identity);

	}
}
