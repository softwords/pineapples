﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using Pineapples.Data.Models;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSCensus
    {
        Task<IDataResult> Read(int surveyYear);
        Task<IDataResult> ReadCurrent();
        Task<IDataResult> CreateCensus(Census census, ClaimsIdentity identity);

        Task<IDataResult> EffectiveTeacherQuals(int surveyYear);
    }
}
