﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSTeacherDeduper 
    {
        IDataResult Filter(TeacherDeduperFilter fltr);
		IDataResult Surveys(int id1, int id2);

		// perform the merge
		IDataResult Merge(int targetID, int sourceID);

	}
}
