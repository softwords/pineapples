﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;


namespace Pineapples.Data
{
    public interface IDSStudent : IDSCrudAsync<StudentBinder, Guid?>
    {
        IDataResult Filter(StudentFilter fltr);
        IDataResult Table(string rowsplit, string colsplit, StudentFilter fltr);
        IDataResult Geo(string geoType, StudentFilter fltr);
		void AccessControl(Guid studentID, ClaimsIdentity identity);
	}
}
