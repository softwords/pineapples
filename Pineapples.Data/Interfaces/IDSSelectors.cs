﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;        // really need to change this interface
using Softwords.DataTools;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace Pineapples.Data
{
    public interface IDSSelectors
    {
        IDataResult Schools(string s);
        IDataResult Teachers(string s);
		IDataResult Students(string s, int? year, string schoolNo);
        IDataResult ExamCandidates(string s, int? year, string examCode, string schoolNo);

	}
}
