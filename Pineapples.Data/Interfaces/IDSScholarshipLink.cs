﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using Pineapples.Data.Models;
using Softwords.Web.Models;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSScholarshipLink 
    {
		Task<IDataResult> Create(DynamicBinder<ScholarshipLink> binder, ClaimsIdentity identity);
		IDataResult Read(int ID);
		Task<DynamicBinder<ScholarshipLink>>  Update(DynamicBinder<ScholarshipLink> binder, ClaimsIdentity identity);
		DynamicBinder<ScholarshipLink>  Delete(int ID, ClaimsIdentity identity);
	}
}
