﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
    [Table("pInspectionRead.QuarterlyReports")]
    public partial class QuarterlyReport : CreateTagged
    {
        public int inspID { get; set; }

        // Since this is actually an updatable view the ID handling strategy 
        // is different. Se CreateTagged base class
        //[Key]
        //[Required(ErrorMessage = "Quarterly Report ID is required")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Quarterly Report ID")]
        public int? qrID { get; set; }

        [Required(ErrorMessage = "School ID is required")]
        [Display(Name = "School ID")]
        public string schNo { get; set; }

        [Display(Name = "School Name")]
        public string schName { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "Inspected By")]
        public string InspectedBy { get; set; }

        [Display(Name = "Num Days Open")]
        public Single? NumDaysOpen { get; set; }

        [Display(Name = "Agg Days Att M")]
        public Single? AggDaysAttM { get; set; }

        [Display(Name = "Agg Days Att F")]
        public Single? AggDaysAttF { get; set; }

        [Display(Name = "Agg Days Att")]
        public Single? AggDaysAtt { get; set; }

        [Display(Name = "Agg Days Abs M")]
        public Single? AggDaysAbsM { get; set; }

        [Display(Name = "Agg Days Abs F")]
        public Single? AggDaysAbsF { get; set; }

        [Display(Name = "Agg Days Abs")]
        public Single? AggDaysAbs { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Agg Days Mem M")]
        public Single? AggDaysMemM { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Agg Days Mem F")]
        public Single? AggDaysMemF { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Agg Days Mem")]
        public Single? AggDaysMem { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Avg Daily Att M")]
        public Single? AvgDailyAttM { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Avg Daily Att F")]
        public Single? AvgDailyAttF { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Avg Daily Att")]
        public Single? AvgDailyAtt { get; set; }

        [Display(Name = "Avg Daily Mem M")]
        public Single? AvgDailyMemM { get; set; }

        [Display(Name = "Avg Daily Mem F")]
        public Single? AvgDailyMemF { get; set; }

        [Display(Name = "Avg Daily Mem")]
        public Single? AvgDailyMem { get; set; }

        [Display(Name = "Tot Enrol To Date M")]
        public int? TotEnrolToDateM { get; set; }

        [Display(Name = "Tot Enrol To Date F")]
        public int? TotEnrolToDateF { get; set; }

        [Display(Name = "Tot Enrol To Date")]
        public int? TotEnrolToDate { get; set; }

        [Display(Name = "Dropout M")]
        public int? DropoutM { get; set; }

        [Display(Name = "Dropout F")]
        public int? DropoutF { get; set; }

        [Display(Name = "Dropout")]
        public int? Dropout { get; set; }

        [Display(Name = "Trin M")]
        public int? TrinM { get; set; }

        [Display(Name = "Trin F")]
        public int? TrinF { get; set; }

        [Display(Name = "Trin")]
        public int? Trin { get; set; }

        [Display(Name = "Trout M")]
        public int? TroutM { get; set; }

        [Display(Name = "Trout F")]
        public int? TroutF { get; set; }

        [Display(Name = "Trout")]
        public int? Trout { get; set; }

        [Display(Name = "Grad8 M")]
        public int? Grad8M { get; set; }

        [Display(Name = "Grad8 F")]
        public int? Grad8F { get; set; }

        [Display(Name = "Grad8")]
        public int? Grad8 { get; set; }

        [Display(Name = "Grad12 M")]
        public int? Grad12M { get; set; }

        [Display(Name = "Grad12 F")]
        public int? Grad12F { get; set; }

        [Display(Name = "Grad12")]
        public int? Grad12 { get; set; }

        [Display(Name = "Pupil Enrol Last Day")]
        public int? PupilEnrolLastDay { get; set; }

        [Display(Name = "Inspection Quarterly Report")]
        public string InspQuarterlyReport { get; set; }

        [Display(Name = "Inspection Type")]
        public string inspsetType { get; set; }

    }

}