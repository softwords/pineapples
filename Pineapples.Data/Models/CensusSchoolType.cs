﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Pineapples.Data.Models
{
    [Table("SurveyYearSchoolTypes")]
    [Description(@"Table that determines settings for school type in a survey, incuding:
- the survey form to use;
-the teacher form to use;
-grid age ranges.
The configuration of the survey form - hiding or showing pages - is also stored in XML on this record.")]
public class CensusSchoolType: ChangeTracked
    {
        [Key]
        [Column("svyYear", TypeName = "int", Order = 0)]
        [Required]
        [Display(Name = "Year")]
        public int svyYear { get; set; }
        [Key]
        [Column("stCode", TypeName = "nvarchar", Order = 1)]
        [MaxLength(10)]
        [StringLength(10)]
        [Required]
        [Display(Name = "Code")]
        public string stCode { get; set; }
        [Column("ytForm", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Form")]
        public string ytForm { get; set; }
        [Column("ytTeacherForm", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Teacher Form")]
        public string ytTeacherForm { get; set; }
        [Column("ytAgeMin", TypeName = "smallint")]
        [Display(Name = "Age Min")]
        [Description("Minimum age for enrolment grids")]
        public short? ytAgeMin { get; set; }
        [Column("ytAgeMax", TypeName = "smallint")]
        [Display(Name = "Age Max")]
        [Description("Maximum age for enrolment grids")]
        public short? ytAgeMax { get; set; }
        
        [Column("ytConfig", TypeName = "ntext")]
        [Display(Name = "Config")]
        public string ytConfig { get; set; }
        [Column("ytLayout", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Layout")]
        public string ytLayout { get; set; }

        [Column("ytDynamic", TypeName = "int")]
        [Required]
        [Display(Name = "Dynamic")]
        public int ytDynamic { get; set; }

        [Column("ytEForm", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "E Form")]
        [Description("Name of the EForm to use for this school type")]
        public string ytEForm { get; set; }
    }

}