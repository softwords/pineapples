﻿using System;
using System.Collections.Generic;
using System.Linq;
using Softwords.DataTools;

namespace Pineapples.Data
{
	public class TeacherExamFilter : Filter
	{
		public string SchoolNo { get; set; }
		public int? TeacherExamID { get; set; }
		public string TeacherExamCode { get; set; }
		public string TeacherExamYear { get; set; }
		public DateTime? TeacherExamDate { get; set; }
	}
}
