﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
	[Table("EdExpenditureState")]
	public class EdExpenditure: ChangeTracked
	{
		[Key]
		[Column("xedID", TypeName = "int")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ID")]
		public int xedID { get; set; }

		[Column("fnmID", TypeName = "int")]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ID")]
		public int fnmID { get; set; }

		[Column("xedCostCentre", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Cost Centre")]
		public string xedCostCentre { get; set; }

		[Column("xedItem", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Item")]
		public string xedItem { get; set; }
		[Column("xedXtrn", TypeName = "int")]
		[Display(Name = "Xtrn")]
		public int? xedXtrn { get; set; }
		[Column("sofCode", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Code")]
		public string sofCode { get; set; }
		[Column("xedTotA", TypeName = "money")]
		[Display(Name = "Tot A")]
		public decimal? xedTotA { get; set; }
		[Column("xedCurrentA", TypeName = "money")]
		[Display(Name = "Current A")]
		public decimal? xedCurrentA { get; set; }
		[Column("xedTotB", TypeName = "money")]
		[Display(Name = "Tot B")]
		public decimal? xedTotB { get; set; }
		[Column("xedCurrentB", TypeName = "money")]
		[Display(Name = "Current B")]
		public decimal? xedCurrentB { get; set; }
	}

}


