using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	// note we retain the original table name, but use new terminology 'Census' in code
	// in Sql , it's still 'Survey'
	[Table("Survey")]		
	[Description("Survey definition record. One record for each survey year. Defines some key characteristics of the survey, such as the Official Start Age for primary school, census date, etc.")]
	public class Census : ChangeTracked
	{
		[Key]
		[Column("svyYear", TypeName = "smallint")]
		[Required]
		[Display(Name = "Year")]
		public short svyYear { get; set; }
		[Column("svyPSAge", TypeName = "int")]
		[Display(Name = "PS Age")]
		public int? svyPSAge { get; set; }
		[Column("svyMonth", TypeName = "int")]
		[Display(Name = "Month")]
		public int? svyMonth { get; set; }
		[Column("svyCensusDate", TypeName = "datetime")]
		[Display(Name = "Census Date")]
		public DateTime? svyCensusDate { get; set; }
		[Column("svyCollectionDate", TypeName = "datetime")]
		[Display(Name = "Collection Date")]
		public DateTime? svyCollectionDate { get; set; }
		[Column("svyClosed", TypeName = "bit")]
		[Display(Name = "Closed")]
		public bool? svyClosed { get; set; }
		[Column("svyDefaultPath", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Default Path")]
		public string svyDefaultPath { get; set; }
	}
}