﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
    [Table("pInspectionRead.SchoolAccreditations")]
    public partial class SchoolAccreditation : CreateTagged
    {
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Display(Name = "Inspection ID")]
        public int inspID { get; set; }

        [Display(Name = "School ID")]
        public string schNo { get; set; }

        [Display(Name = "School Name")]
        public string schName { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "Inspected By")]
        public string InspectedBy { get; set; }

        // Since this is actually an updatable view the ID handling strategy 
        // is different. Se CreateTagged base class
        //[Key]
        //[Required(ErrorMessage = "School Accreditation ID is required")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "School Accreditation ID")]
        public int? saID { get; set; }

        [Display(Name = "SE.1.1")]
		[Column("SE.1.1")]
        public int? SE_1_1 { get; set; }

        [Display(Name = "L2")]
		[Column("SE.1.2")]
		public int? SE_1_2 { get; set; }

        [Display(Name = "L3")]
		[Column("SE.1.3")]
		public int? L3 { get; set; }

        [Display(Name = "L4")]
		[Column("SE.1.4")]
		public int? L4 { get; set; }

        [Display(Name = "T1")]
		[Column("SE.2.1")]
		public int? T1 { get; set; }

        [Display(Name = "T2")]
		[Column("SE.2.2")]
		public int? T2 { get; set; }

        [Display(Name = "T3")]
		[Column("SE.2.3")]
		public int? T3 { get; set; }

        [Display(Name = "T4")]
		[Column("SE.2.4")]
		public int? T4 { get; set; }

        [Display(Name = "D1")]
		[Column("SE.3.1")]
		public int? D1 { get; set; }

        [Display(Name = "D2")]
		[Column("SE.3.2")]
		public int? D2 { get; set; }

        [Display(Name = "D3")]
		[Column("SE.3.3")]
		public int? D3 { get; set; }

        [Display(Name = "D4")]
		[Column("SE.3.4")]
		public int? D4 { get; set; }

        [Display(Name = "N1")]
		[Column("SE.4.1")]
		public int? N1 { get; set; }

        [Display(Name = "N2")]
		[Column("SE.4.2")]
		public int? N2 { get; set; }

        [Display(Name = "N3")]
		[Column("SE.4.3")]
		public int? N3 { get; set; }

        [Display(Name = "N4")]
		[Column("SE.4.4")]
		public int? N4 { get; set; }

        [Display(Name = "F1")]
		[Column("SE.5.1")]
		public int? F1 { get; set; }

        [Display(Name = "F2")]
		[Column("SE.5.2")]
		public int? F2 { get; set; }

        [Display(Name = "F3")]
		[Column("SE.5.3")]
		public int? F3 { get; set; }

        [Display(Name = "F4")]
		[Column("SE.5.4")]
		public int? F4 { get; set; }

        [Display(Name = "S1")]
		[Column("SE.6.1")]
		public int? S1 { get; set; }

        [Display(Name = "S2")]
		[Column("SE.6.2")]
		public int? S2 { get; set; }

        [Display(Name = "LS3")]
		[Column("SE.6.3")]
		public int? S3 { get; set; }

        [Display(Name = "S4")]
		[Column("SE.6.4")]
		public int? S4 { get; set; }

        [Display(Name = "CO1")]
		[Column("CO.1")]
		public int? CO1 { get; set; }

        [Display(Name = "CO2")]
		[Column("CO.2")]
		public int? CO2 { get; set; }

        [Display(Name = "LT1")]
		[Column("Level1")]
        public int? LT1 { get; set; }

        [Display(Name = "LT2")]
		[Column("Level2")]
		public int? LT2 { get; set; }

        [Display(Name = "LT3")]
		[Column("Level3")]
		public int? LT3 { get; set; }

        [Display(Name = "LT4")]
		[Column("Level4")]
		public int? LT4 { get; set; }

        [Display(Name = "Total")]
		[Column("ST")]
		public int? T { get; set; }

        [Display(Name = "School Level")]
        public string SchLevel { get; set; }

        [Display(Name = "Inspection Year")]
        public string InspYear { get; set; }

        [Display(Name = "Inspection Type")]
        public string inspsetType { get; set; }

    }

}