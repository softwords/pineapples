﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Pineapples.Data.Models
{
	[Table("SpEdEvaluation")]
	[Description("Detailed records of special education evaluation life cycle.  Student_ stuID is the foreign key.")]
	public class SpecialEdEvaluation : ChangeTracked
	{
		[Key]
		[Column("spedEvalID", TypeName = "int")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ID")]
		public int spedEvalID { get; set; }

		[Column("stuID", TypeName = "uniqueidentifier")]
		[Display(Name = "Student ID")]
		public Guid stuID { get; set; }

		[Column("spedEvalFirstTime", TypeName = "bit")]
		[Display(Name = "First time evaluation")]
		public bool? spedEvalFirstTime { get; set; }

		[Column("spedEvalEventCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Evaluation Event")]
		public string spedEvalEventCode { get; set; }

		[Column("spedEvalEventDate", TypeName = "datetime")]
		[Display(Name = "Event Date")]
		public DateTime? spedEvalEventDate { get; set; }

		[Column("spedEvalComment", TypeName = "nvarchar(MAX)")]
		[Display(Name = "Comment")]
		public string spedEvalComment { get; set; }

	}

}
