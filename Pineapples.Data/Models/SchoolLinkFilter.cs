﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class SchoolLinkFilter : Filter
    {
        public int? LinkID { get; set; }
        public string SchoolNo { get; set; }
        public string SchoolName { get; set; }
        public Guid? DocumentID { get; set; }
        public string Keyword { get; set; }
        public string Function { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public string DocumentSource { get; set; }
        public string DocType { get; set; }
        public int? IsImage { get; set; }
    }
}
