﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
	[Table("pInspectionRead.SchoolInspections")]
	public partial class SchoolInspection: CreateTagged
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("inspID", TypeName = "int")]
		[Display(Name = "ID")]
		public int? ID { get; set; }

		[Column("schNo", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "No")]
		public string schNo { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		[Column("schName", TypeName = "nvarchar")]
		[MaxLength(200)]
		[StringLength(200)]
		[Display(Name = "Name")]
		public string schName { get; set; }

		[Column("PlannedDate", TypeName = "datetime")]
		[Display(Name = "Planned Date")]
		public DateTime? PlannedDate { get; set; }

		[Column("StartDate", TypeName = "datetime")]
		[Display(Name = "Start Date")]
		public DateTime? StartDate { get; set; }

		[Column("EndDate", TypeName = "datetime")]
		[Display(Name = "End Date")]
		public DateTime? EndDate { get; set; }

		[Column("Note", TypeName = "ntext")]
		[Display(Name = "Note")]
		public string Note { get; set; }

		[Column("InspectedBy", TypeName = "nvarchar")]
		[MaxLength(500)]
		[StringLength(500)]
		[Display(Name = "Inspected By")]
		public string InspectedBy { get; set; }

		[Column("InspectionSetID", TypeName = "int")]
		[Display(Name = "Inspection Set ID")]
		public int? InspectionSetID { get; set; }

		[Column("InspectionContent", TypeName = "xml")]
		[Display(Name = "Inspection Content")]
		public string InspectionContent { get; set; }

		[Column("InspectionSetName", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Set Name")]
		public string InspectionSetName { get; set; }


		[Column("InspectionYear", TypeName = "int")]
		[Display(Name = "Insp Year")]
		public int? InspectionYear { get; set; }


		[Column("InspectionResult", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Result")]
		[Description("An overall assessment level or score achieved on this inspection")]
		public string InspectionResult { get; set; }

		[Column("SourceId", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Source")]
		[Description("A reference to a source document e.g. google drive file FileId")]
		public string SourceId { get; set; }

		
		[Column("InspTypeCode", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Type Code")]
		public string InspTypeCode { get; set; }


		[Column("InspectionType", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Type")]
		public string InspectionType { get; set; }

		[ForeignKey("InspTypeCode")]	
		public InspectionType Type { get; set;}
	}
}