﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("pExamRead.ExamFrames")]
	public class ExamFrame:ChangeTracked
	{
		[Key]
		[Column("ID", TypeName = "nvarchar")]
		[MaxLength(11)]
		[StringLength(11)]
		[Display(Name = "ID")]
		public string ID { get; set; }

		[Column("RowType", TypeName = "varchar")]
		[MaxLength(1)]
		[StringLength(1)]
		[Required]
		[Display(Name = "Row Type")]
		public string RowType { get; set; }

		[Column("RowID", TypeName = "int")]
		[Required]
		[Display(Name = "Row ID")]
		public int RowID { get; set; }

		[Column("ExamID", TypeName = "int")]
		[Required]
		[Display(Name = "Exam ID")]
		public int ExamID { get; set; }

		[Column("Code", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Required]
		[Display(Name = "Code")]
		public string Code { get; set; }

		[Column("Description", TypeName = "nvarchar")]
		[MaxLength(500)]
		[StringLength(500)]
		[Display(Name = "Description")]
		public string Description { get; set; }
	}
}
