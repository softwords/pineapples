﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
    [Table("paAssessmentLine_")]
    public partial class PerfAssessmentIndicator
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("palID")]
        public int LineId { get; set; }

        [NotMapped]
        public string IndicatorCode { get; set; }

        [Column("paindID")]
        public int IndicatorId { get; set; }

        [Column("paplCode")]
        [MaxLength(10)]
        public string PerformanceLevel { get; set; }

        [Column("palComment")]
        [MaxLength(200)]
        public string Comment { get; set; }

        [Column("paID")]
        public int PerfAssessmentId { get; set; }

        [ForeignKey("PerfAssessmentId")]
        public PerfAssessment PerfAssess { get; set; }

    }

    public class PerfAssessmentIndicatorLevel
    {
        public int IndicatorId { get; set; }
        public string Level { get; set; }
        public int? Value { get; set; }
        public string Description { get; set; }
        public string Criterion { get; set; }
    }
}
