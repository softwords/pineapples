﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class SpecialEdStudentFilter : StudentFilter
    {
       // to do special ed selection criteria
		public string Disability { get; set; }
		public string EnglishLearner { get; set; }
		public string Environment { get; set; }
	}

    public class SpecilEdStudentTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public SpecialEdStudentFilter filter { get; set; }
    }
}
