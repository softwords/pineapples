﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{

   [Table("paAssessmentLine_")]
    public partial class PaAssessmentLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int palID { get; set; }
        public int paID { get; set; }
        public int paindID { get; set; }
        [MaxLength(10)]
        public string paplCode { get; set; }
        [MaxLength(200)]
        public string palComment { get; set; }
    }

}
