﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Pineapples.Data
{
    public class TeacherFilter : Filter2
    {
		public TeacherFilter()
		{
			PageNo = 1;
			PageSize = 50;
			SortDirection = "asc";
			SortColumn = "Surname";
			ColumnSet = 0;
		}
		public int? TeacherID { get; set; }
        public string Surname { get; set; }
        public string GivenName { get; set; }
        public string PayrollNo { get; set; }
        public string RegistrationNo { get; set; }
        public string ProvidentFundNo { get; set; }
        public string Gender { get; set; }
        public DateTime? DoB { get; set; }
        public DateTime? DobEnd { get; set; }
        public string Language { get; set; }
        public string PaidBy { get; set; }
        public string Qualification { get; set; }
        public string EdQualification { get; set; }
        public string Subject { get; set; }
        public int? SearchSubjectTaught { get; set; }
        public int? SearchTrained { get; set; }
        public string AtSchool { get; set; }
        public string AtSchoolType { get; set; }
        public int? InYear { get; set; }
        public string Role { get; set; }
        public string LevelTaught { get; set; }
        public int? YearOfEdMin { get; set; }
        public int? YearOfEdMax { get; set; }
        public string ISCEDSubClass { get; set; }
        public int? UseOr { get; set; }
        public int? CrossSearch { get; set; }
        public int? SoundSearch { get; set; }

        // added for AccessControl
        public string District { get; set; }
        public string Authority { get; set; }
        // use existing AtSchool for the school

        public XDocument XmlFilter { get; set; }


        public void ApplyUserFilter(ClaimsIdentity identity)
        {
            Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
            if (c != null)
            {
                District = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterAuthority");
            if (c != null)
            {
                Authority = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterSchoolNo");
            if (c != null)
            {
                AtSchool = c.Value;
            }

        }
    }

    public class TeacherTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public TeacherFilter filter { get; set; }
    }

	public class TeacherDeduperFilter:Filter
	{
		public int? MatchGiven { get; set; }
		public int? MatchSurname { get; set; }
		public int? MatchPayroll { get; set; }
		public int? MatchProvident { get; set; }
		public int? MatchRegister { get; set; }
		public int? MatchDoB { get; set; }
		public int? MatchGender { get; set; }

		public string Given { get; set; }
		public string Surname { get; set; }
		public string District { get; set; }

		public void ApplyUserFilter(ClaimsIdentity identity)
		{
			// just filter on district; should be adequate
			Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
			if (c != null)
			{
				District = c.Value;
			}
		}
	}
}
