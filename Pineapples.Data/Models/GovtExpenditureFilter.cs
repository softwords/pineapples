﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
	/// <summary>
	/// Where required, specific system-supported inspection types
	/// can subclass the SchoolInspectionFilter
	/// </summary>
    public class GovtExpenditureFilter :Filter
    {

		public int? ID { get; set; }
		public int? Year { get; set; }
		public string District { get; set; }
	}
	
}
