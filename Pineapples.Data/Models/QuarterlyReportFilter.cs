﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class QuarterlyReportFilter : SchoolInspectionFilter
    {
		public QuarterlyReportFilter()
		{
			PageNo = 1;
			PageSize = 50;
			SortDirection = "asc";
			SortColumn = "qrID";
			ColumnSet = 0;
		}

		public string QRID
		{
			get
			{
				return InspID;
			}
			set
			{
				InspID = value;
			}
		}
		public string InspQuarterlyReport { get; set; }

    }

	public class QuarterlyReportTableFilter : TableFilter<QuarterlyReportFilter> { }
}
