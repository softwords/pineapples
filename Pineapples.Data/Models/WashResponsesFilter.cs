﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
	/// <summary>
	/// Where required, specific system-supported inspection types
	/// can subclass the SchoolInspectionFilter
	/// </summary>
    public class WashResponsesFilter : SchoolInspectionFilter
    {
	

		public string Question { get; set; }
		public int? BestUpTo { get; set; }

	}
	public class WashTableFilter : TableFilter<WashResponsesFilter> { }
}
