﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
	[Table("CensusWorkbookColumnMaps")]
	[Description(@"Maps between column names set up on the sheets of the census workbook
, and the names expected by the Workbook uploader.
Note that 'vocabulary' items for District, School Type and School No are always translated, so are not required in this table.")]

	public class CensusWorkbookColumnMap: ChangeTracked
	{
		[Key]
		[Column("mapID", TypeName = "int")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required]
		[Display(Name = "ID")]
		public int ID { get; set; }

		[Column("sheetName", TypeName = "nvarchar")]
		[MaxLength(50)]
		[Display(Name = "Sheet")]
		public string sheetName { get; set; }

		[Column("columnName", TypeName = "nvarchar")]
		[MaxLength(100)]
		[Display(Name = "Column Name")]
		public string columnName { get; set; }

		[Column("mapping", TypeName = "nvarchar")]
		[MaxLength(100)]
		[Display(Name = "Mapping")]
		public string mapping { get; set; }
	}

}
