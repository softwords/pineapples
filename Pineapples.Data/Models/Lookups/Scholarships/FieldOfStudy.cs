﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


	[Table("FieldOfStudy")]
	[Description("Tertiary field of study")]
	public class FieldOfStudy: SimpleCodeTable
	{
		[Key]
		[Column("fosCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required]
		[Display(Name = "Code")]
		public override string Code { get; set; }
		[Column("fosName", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Required]
		[Display(Name = "Name")]
		public override string Description { get; set; }

		[Column("fosNameL1", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]		
		[Display(Name = "Name L1")]
		public override string DescriptionL1 { get; set; }

		[Column("fosNameL2", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Name L2")]
		public override string DescriptionL2 { get; set; }

		[Column("fosgrpCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required]
		[Display(Name = "GroupCode")]
		[ClientLookup("fieldOfStudyGroups")]
		public string GroupCode { get; set; }
	}
}
