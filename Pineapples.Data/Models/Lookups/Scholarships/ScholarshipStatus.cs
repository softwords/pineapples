﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpScholarshipStatus")]
    [Description("Scholarship Status")]
    public partial class ScholarshipStatus : SequencedCodeTable
    {
    }

}
