﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;


namespace Pineapples.Data.Models
{ 
	[Table("ScholarshipInstitutions")]
	public class ScholarshipInstitution: ChangeTracked
	{
		[Key]
		[Column("instCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required]
		[Display(Name = "Code")]
		public string Code { get; set; }

		[Column("instName", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Required]
		[Display(Name = "Name")]
		public string Description { get; set; }

		[Column("instCountry", TypeName = "nvarchar")]
		[MaxLength(2)]
		[StringLength(2)]
		[Required]
		[Display(Name = "Country")]
		[ClientLookup("nationalities")]
		public string Country { get; set; }


		[Column("instTermsPerYear", TypeName = "int")]
		[Display(Name = "Terms per Year")]
		public int? TermsPerYear { get; set; }
	}

}
