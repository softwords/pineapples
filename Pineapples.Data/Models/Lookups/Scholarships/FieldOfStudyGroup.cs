﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


	[Table("FieldOfStudyGroups")]
	[Description("Tertiary field of study grouping")]
	public class FieldOfStudyGroup: SimpleCodeTable
	{
		
	}
}
