﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("lkpTertiaryGrades")]
	public class TertiaryGrade:ChangeTracked
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		[Column("gyIndex", TypeName = "int")]
		[Required]
		[Display(Name = "Index")]
		public int ID { get; set; }
		[Column("gyName", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Required]
		[Display(Name = "Name")]
		public string Name { get; set; }
	}


}
