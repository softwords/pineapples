﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("ScholarshipTypes")]
    [Description("Scholarship types")]
    public partial class ScholarshipType : SimpleCodeTable
    {
		[Column("ConsecutiveSemesters", TypeName = "int")]
		[Display(Name = "Consecutive Semesters")]
		public int? ConsecutiveSemesters { get; set; }

		[Column("ReqGPA", TypeName = "decimal")]
		[Display(Name = "Req GPA")]
		public decimal? ReqGPA { get; set; }

	}

}
