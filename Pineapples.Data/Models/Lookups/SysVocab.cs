using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("sysVocab")]
    [Description(@"Localised vocabulary")]
    public partial class SysVocab : SimpleCodeTable
	{
        [Key]

        [Column("vocabName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "Term")]
        public override string Code { get; set; }

        [Column("vocabTerm", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "Local is required")]
        [Display(Name = "Local")]
        public override string Description { get; set; }

        [Column("vocabTermL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Local L1")]
        public override string DescriptionL1 { get; set; }

        [Column("vocabTermL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Local L2")]
        public override string DescriptionL2 { get; set; }
    }
}
