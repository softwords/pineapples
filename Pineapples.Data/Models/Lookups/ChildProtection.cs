using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpChildProtection")]
    [Description(@"Child protection groups and items for the Vanuatu Child Protection survey. Appears on PupilTables.")]
    public partial class ChildProtection : SequencedCodeTable
    {
        [Key]

        [Column("cpCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "cp Code is required")]
        [Display(Name = "cp Code")]
        public override string Code { get; set; }

        [Column("cpDesc", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "cp Desc")]
        public override string Description { get; set; }

        [Column("cpGrp", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "cp Grp")]
        public string cpGroup { get; set; }

        [Column("cpSeq", TypeName = "int")]
        [Display(Name = "cp Seq")]
        public override int? Seq { get; set; }

        [Column("cpDescL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "cp Desc L1")]
        public override string DescriptionL1 { get; set; }

        [Column("cpDescL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "cp Desc L2")]
        public override string DescriptionL2 { get; set; }
    }
}
