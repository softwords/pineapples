using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpSiteHost")]
    public partial class SiteHost : SimpleCodeTable
    {
        [Key]

        [Column("hostCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "host Code is required")]
        [Display(Name = "host Code")]
        public override string Code { get; set; }

        [Column("hostDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "host Description")]
        public override string Description { get; set; }

        [Column("hostSeq", TypeName = "int")]
        [Display(Name = "host Seq")]
        public int? hostSeq { get; set; }

        [Column("hostDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "host Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("hostDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "host Description L2")]
        public override string DescriptionL2 { get; set; }
    }
}
