﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("ISCEDField")]
	[Description(@"UNESCO defined fields of study. Holds all detailed fields of ISCED-F 2013.")]
	public partial class ISCEDField : ChangeTracked
	{
		[Key]
		[Column("ifID", TypeName = "nvarchar")]
		[MaxLength(4)]
		[StringLength(4)]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ISCED Field (Detailed) ID")]
		public string ID { get; set; }

		[Column("ifName", TypeName = "nvarchar")]
		[MaxLength(250)]
		[StringLength(250)]
		[Display(Name = "ISCED Field (Detailed) Name")]
		public string Name { get; set; }

		[Column("ifDesc", TypeName = "nvarchar")]
		[Display(Name = "ISCED Field (Detailed) Description")]
		public string Dsscription { get; set; }

		[Column("ifgID", TypeName = "nvarchar")]
		[MaxLength(3)]
		[StringLength(3)]
		[Display(Name = "ISCED Field Group (Narrow)")]
		[ClientLookup("iscedFieldGroups")]
		public string ISCEDGroup { get; set; }
	}

}
