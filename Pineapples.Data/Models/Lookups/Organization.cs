﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpOrganizations")]
    [Description(@"Organizations employ teachers. This list of Organizations supplies the dropdown values for
the Organisation column in the Staff page of the annual census.")]
    public class Organization: ChangeTracked
    {
        [Key]
        [Column("employerCode", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Column("employerName", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Column("employerSeq", TypeName = "int")]
        [Display(Name = "Seq")]
        public int? Seq { get; set; }
    }

}
