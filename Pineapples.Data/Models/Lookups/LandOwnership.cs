using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpLandOwnership")]
    [Description(@"Method of landownership. Foreign key on schools, schoolsurvey. Used in Vanuatu.")]
    public partial class LandOwnership : SimpleCodeTable
    {


        [Column("authCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Auth Code")]
        public string authCode { get; set; }

    }
}
