﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("EducationPathLevels")]
	[Description(@"Do not change without coordinating with someone with deep knowledge of the inner working of the system.
    Set of class Levels that make an Education Path. Middle table between EducationPaths and lkpLevels.")]
	public partial class EducationPathLevel : ChangeTracked
	{
		[Key]
		[Column("plID", TypeName = "int")]
		[Display(Name = "ID")]
		public int ID { get; set; }

		[Column("pathCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Education Path")]
		[ClientLookup("educationPaths")]
		public string pathCode { get; set; }

		[Column("levelCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Class Level")]
		[ClientLookup("levels")]
		public string levelCode { get; set; }
	}
}