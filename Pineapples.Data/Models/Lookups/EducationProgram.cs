﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("EducationPrograms")]
	[Description(@"Education Programs is used only to populate the Quest Survey. 
    However, it is required for that to work properly.")]
	public partial class EducationProgram : ChangeTracked
	{
		[Key]
		[Column("pgID", TypeName = "int")]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ID")]
		public int ID { get; set; }

		[Column("pgName", TypeName = "nvarchar")]
		[MaxLength(200)]
		[StringLength(200)]
		[Display(Name = "Name")]
		public string Name { get; set; }

		[Column("ilsCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "ISCED Sub Level")]
		[ClientLookup("iscedLevelsSub")]
		public string ilsCode { get; set; }

		[Column("pgOrientation", TypeName = "nvarchar")]
		[MaxLength(1)]
		[StringLength(1)]
		[Display(Name = "Orientation")]
		public string pgOrientation { get; set; }

		[Column("secCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Education Sector")]
		[ClientLookup("educationSectors")]
		public string secCode { get; set; }

		[Column("pgStartAge", TypeName = "smallint")]
		[Display(Name = "Start Age")]
		public short? pgStartAge { get; set; }

		[Column("pgDuration", TypeName = "smallint")]
		[Display(Name = "Duration")]
		public short? pgDuration { get; set; }

		[Column("pgPartTime", TypeName = "smallint")]
		[Display(Name = "Part Time")]
		public short? pgPartTime { get; set; }
	}
}