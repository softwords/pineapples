﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("Authorities")]
	[Description(@"Authorities are organisations that run schools. Authorities may be the receipients of grants.
	Authorities are grouped by type, and then as Govt / Non - govt.
	schAuth is a foreign key on the school table.
	Authority of a school may change over time - ssAuth is the Authority reported on the survey for any year.
	Planned future changes to the Authority of a school can be recorded on SchoolEstablishment.")]
	public partial class Authority : ChangeTracked
	{
		[Key]
		[Column("authCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "Code is required")]
		[Display(Name = "Code")]
		public string Code { get; set; }

		[Column("authName", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Name")]
		public string Name { get; set; }

		[Column("authType", TypeName = "nvarchar")]
		[MaxLength(1)]
		[StringLength(1)]
		[Display(Name = "Type")]
		[ClientLookup("authorityTypes")]
		public string authType { get; set; }

		[Column("authContact", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Contact")]
		public string Contact { get; set; }

		[Column("authAddr1", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Addr1")]
		public string Addr1 { get; set; }

		[Column("authAddr2", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Addr2")]
		public string Addr2 { get; set; }

		[Column("authAddr3", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Addr3")]
		public string Addr3 { get; set; }

		[Column("authAddr4", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Addr4")]
		public string Addr4 { get; set; }

		[Column("authPh1", TypeName = "nvarchar")]
		[MaxLength(30)]
		[StringLength(30)]
		[Display(Name = "Ph1")]
		public string Ph1 { get; set; }

		[Column("authPh2", TypeName = "nvarchar")]
		[MaxLength(30)]
		[StringLength(30)]
		[Display(Name = "Ph2")]
		public string Ph2 { get; set; }

		[Column("authFax", TypeName = "nvarchar")]
		[MaxLength(30)]
		[StringLength(30)]
		[Display(Name = "Fax")]
		public string Fax { get; set; }

		[Column("authEmail", TypeName = "nvarchar")]
		[MaxLength(30)]
		[StringLength(30)]
		[Display(Name = "Email")]
		public string Email { get; set; }

		[Column("authNameL1", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Name L1")]
		public string NameL1 { get; set; }

		[Column("authNameL2", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Name L2")]
		public string NameL2 { get; set; }

		[Column("authBank", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Bank")]
		public string Bank { get; set; }

		[Column("authBSB", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "BSB")]
		public string BSB { get; set; }

		[Column("authAccountNo", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Account No")]
		public string AccountNo { get; set; }

		[Column("authID", TypeName = "int")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ID")]
		public int authID { get; set; }

		[Column("authOrgUnitNumber", TypeName = "int")]
		[Display(Name = "Org Unit Number")]
		public int? OrgUnitNumber { get; set; }

		[Column("authSuperOrgUnitNumber", TypeName = "int")]
		[Display(Name = "Super Org Unit Number")]
		public int? hSuperOrgUnitNumber { get; set; }
	}
}