using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("SourceOfFunds")]
	[Description("Sources of funding to education system or schools")]
	public partial class FundingSource : SimpleCodeTable
	{
		[Key]
		[Column("sofCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "Code is required")]
		[Display(Name = "Code")]
		public override string Code { get; set; }
		[Column("sofDesc", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Desc")]
		public override string Description { get; set; }

		// purpose of this no longer known?
		//[Column("sofC", TypeName = "bit")]
		//[Display(Name = "C")]
		//public bool? sofC { get; set; }

		[Column("sfgCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "GroupCode")]
		[ClientLookup("fundingSourceGroups")]
		public string GroupCode { get; set; }

		[Column("sofDescL1", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Desc L1")]
		public override string DescriptionL1 { get; set; }

		[Column("sofDescL2", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Desc L2")]
		public override string DescriptionL2 { get; set; }
	}
}
