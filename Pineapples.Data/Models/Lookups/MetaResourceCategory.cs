﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("metaResourceCategories")]
	[Description(@"WARNING: Changes here have wide repercussions throughout various components of the system (PDF Survey, etc.). Change with great care with someone very familiar with the system. "
				+ "List of possible resource categories. Metadata? Or lookup? Somewhere between. Survey Resource grids a constructed by Category. Category code is foreign key on metaResourceDefs.")]
	public partial class MetaResourceCategory : ChangeTracked
	{

		[Key]
		[Column("mresCatCode", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Required(ErrorMessage = "Resource Category Code is required")]
		[Display(Name = "Resource Category Code")]
		public string Code { get; set; }

		[Column("mresCatName", TypeName = "nvarchar")]
		[MaxLength(150)]
		[StringLength(150)]
		[Display(Name = "Resource Category Name")]
		public string Name { get; set; }

		[Column("mresCatNameL1", TypeName = "nvarchar")]
		[MaxLength(150)]
		[StringLength(150)]
		[Display(Name = "Name L1")]
		public string NameL1 { get; set; }


		[Column("mresCatNameL2", TypeName = "nvarchar")]
		[MaxLength(150)]
		[StringLength(150)]
		[Display(Name = "Name L2")]
		public string NameL2 { get; set; }

		[Column("mresCatBuildingType", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Building Type")]
		public string BuildingType { get; set; }

	}

}
