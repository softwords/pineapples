using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpBuildingMaterials")]
    [Description(@"Detailed list of building materials that may be used for walls floor or roof.")]
    public partial class BuildingMaterial : SimpleCodeTable
    {
        [Key]

        [Column("bmatCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "bmat Code is required")]
        [Display(Name = "bmat Code")]
        public override string Code { get; set; }

        [Column("bmatDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "bmat Description is required")]
        [Display(Name = "bmat Description")]
        public override string Description { get; set; }

        [Column("bmatDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bmat Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("bmatDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bmat Description L2")]
        public override string DescriptionL2 { get; set; }

        [Column("bmatFloor", TypeName = "bit")]
        [Required(ErrorMessage = "bmat Floor is required")]
        [Display(Name = "bmat Floor")]
        public bool Floor { get; set; }

        [Column("bmatWall", TypeName = "bit")]
        [Required(ErrorMessage = "bmat Wall is required")]
        [Display(Name = "bmat Wall")]
        public bool Wall { get; set; }

        [Column("bmatRoof", TypeName = "bit")]
        [Required(ErrorMessage = "bmat Roof is required")]
        [Display(Name = "bmat Roof")]
        public bool Roof { get; set; }
    }
}
