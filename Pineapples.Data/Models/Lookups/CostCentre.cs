using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("CostCentres")]
	[Description(@"Part of the high level reporting of expenditure; for the calculation of EFA7 and 8. 
	A cost centre is an item or subtotal in the Ministry's General Ledger. Budget and Expenditure is recorded each year by cost centre. 
    Typically, this will be rekeyed from some presentation of the ministry accounts.
	To develop costs per sector each cost centre may be 
    'Post to sector (money used for a specific education sector)' shown as 0 below,
    'Prorate (money used/prorated against all education sectors)' shown as 1 below,
    'Ignore (money not counted/ignored for indicators computation)' shown as 2 below")]

	public partial class CostCentre : SimpleCodeTable
	{

		[Key]
		[Column("ccCode", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Required(ErrorMessage = "Code is required")]
		[Display(Name = "Code")]
		public override string Code { get; set; }

		[Column("ccDescription", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Description")]
		public override string Description { get; set; }

		[Column("ccSector", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Sector")]
		[ClientLookup("educationSectors")]
		public string Sector { get; set; }


		[Column("ccProRate", TypeName = "int")]
		[Display(Name = "Pro Rate")]
		public int? ProRate { get; set; }

		[Column("ccDescriptionL1", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Description L1")]
		public override string DescriptionL1 { get; set; }

		[Column("ccDescriptionL2", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Description L2")]
		public override string DescriptionL2 { get; set; }
	}
}
