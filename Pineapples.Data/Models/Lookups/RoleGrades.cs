using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("RoleGrades")]
	[Description(@"Grades for teacher roles that define different pay scales. Think of this as teacher roles but with added data about pay scales. This is the one actually used in the annual census workbook and thus to add new values to the census they must be added here (and optionally linked to a more general lkpTeacherRole.")]
	public partial class RoleGrades : SimpleCodeTable
	{
		[Key]

		[Column("rgCode", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Required(ErrorMessage = "rg Code is required")]
		[Display(Name = "Role Grade Code")]
		public override string Code { get; set; }

		[Column("rgDescription", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Role Grade Description")]
		public override string Description { get; set; }

		[Column("rgDescriptionL1", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Role Grade Description L1")]
		public override string DescriptionL1 { get; set; }

		[Column("rgDescriptionL2", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Role Grade Description L2")]
		public override string DescriptionL2 { get; set; }

		[Column("roleCode", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Teacher Role Code")]
		[ClientLookup("teacherRoles")]
		public string roleCode { get; set; }

		[Column("rgSalaryLevelMin", TypeName = "nvarchar")]
		[MaxLength(5)]
		[StringLength(5)]
		[Display(Name = "Salary Level Minimum")]
		public string rgSalaryLevelMin { get; set; }

		[Column("rgSalaryLevelMax", TypeName = "nvarchar")]
		[MaxLength(5)]
		[StringLength(5)]
		[Display(Name = "Salary Level Maximum")]
		public string rgSalaryLevelMax { get; set; }

		[Column("rgSort", TypeName = "int")]
		[Display(Name = "Role Grade Sort")]
		public int? rgSort { get; set; }
	}
}
