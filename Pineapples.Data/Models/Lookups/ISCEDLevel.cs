﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("ISCEDLevel")]
	[Description(@"UNESCO-defined ISCED Levels ISCED 0, ISCED 1, ISCED 2, ISCED 3 etc")]
	public partial class ISCEDLevel : ChangeTracked
	{
		[Key]
		[Column("ilCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "ISCED Code is required")]
		[Display(Name = "ISCED Level Code")]
		public string Code { get; set; }

		[Column("ilName", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "ISCED Level Name")]
		public string Name { get; set; }
	}

}
