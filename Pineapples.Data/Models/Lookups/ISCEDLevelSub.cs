﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("ISCEDLevelSub")]
	[Description(@"UNESCO Defined sublevels. Each ISCEDLevel may have multiple sublevels ISCED3A, ISCED3B. 
	The ISCEDLevel is the foreign key. Each level must have at least one sublevel. 
	System defined. The ISCEDSublevel appears on lkpLevels. 
	Since enrolments are clasifed by level, this allows aggregation of enrolment by ISCED Sub Level and ISCEDLevel")]
	public partial class ISCEDLevelSub : ChangeTracked
	{
		[Key]
		[Column("ilsCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "ISCED Level Sub Code is required")]
		[Display(Name = "ISCED Level Sub Code")]
		public string Code { get; set; }

		[Column("ilCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "ISCED Level")]
		[ClientLookup("iscedLevels")]
		public string ISCEDLevel { get; set; }

		[Column("ilsName", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "ISCED Level Sub Name")]
		public string Name { get; set; }
	}

}
