﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("EducationSectors")]
	[Description(@"Do not change without coordinating with someone with deep knowledge of the inner working of the system.
	Education sectors are part of the education system focused on particular curriculum and outcomes. 
	Education Sectors may be chronologically parallel e.g. Senior Secondary and TVET.
	Sectors are the principal means of disagregating finance data.")]
	public class EducationSector : SequencedCodeTable
	{
		[Key]
		[Column("secCode", TypeName = "nvarchar")]
		[MaxLength(3)]
		[StringLength(3)]
		[Required(ErrorMessage = "Code is required")]
		[Display(Name = "Code")]
		public override string Code { get; set; }

		[Column("secDesc", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Desc")]
		public override string Description { get; set; }

		[Column("secYears", TypeName = "int")]
		[Display(Name = "Years")]
		public int? Years { get; set; }

		[Column("secMinAge", TypeName = "int")]
		[Display(Name = "Min Age")]
		public int? MinAge { get; set; }

		[Column("secDescL1", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Desc L1")]
		public override string DescriptionL1 { get; set; }

		[Column("secDescL2", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Desc L2")]
		public override string DescriptionL2 { get; set; }

		[Column("secSort", TypeName = "int")]
		[Display(Name = "Sort")]
		[Description("Used to order sectors in reporting; e.g. Establishment Reports where SEC should be before PRI and ECE")]
		public override int? Seq { get; set; }

		[Column("secGLSalaries", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "GL Salaries")]
		[Description("GL account code for salaries in this sector. Used primarily in Payroll interface.")]
		public string GLSalaries { get; set; }
	}

}
