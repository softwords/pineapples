using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("lkpTeacherExamTypes")]
	[Description(@"Type of Teacher Exams. A foreign key on TeacherExams.")]
	public partial class TeacherExamType : ChangeTracked
	{
		[Key]

		[Column("texCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "Teacher Exam Code is required")]
		[Display(Name = "tex Code")]
		public string Code { get; set; }

		[Column("texName", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Teacher Exam Name")]
		public string TeacherExamName { get; set; }

		[Column("texNameShort", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Short Name")]
		public string TeacherExamNameShort { get; set; }

		[Column("texFirstOffered", TypeName = "int")]
		[Display(Name = "First Offered")]
		public int? YearFirstOffered { get; set; }

		[Column("texRegion", TypeName = "bit")]
		[Display(Name = "Region", Description = "Tick if this is a Regional exam")]
		public bool? RegionalExam { get; set; }
	}
}
