﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("EducationPaths")]
    [Description(@"Do not change without coordinating with someone with deep knowledge of the inner working of the system. 
    An EducationPath is a pathway through the education system, moving from one class level to 
    another at the next Year of Education. The default Path is a device to provide a ""default class level"" for every year of education. 
    In some systems, there may not be more than 1 class level per year of education, but systems using TVET beside senior secondary
    will probably have this. The default path is useful to not create duplicates when reporting on Enrolment Ratios. 
    Ie population and enrolment are calculated for each year of education, but this is presented against the ""default class level"" 
    for that year of education for reading clarity.")]
    public partial class EducationPath: ChangeTracked
    {
        [Key]
        [Column("pathCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Column("pathName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Column("pathDefault", TypeName = "bit")]
        [Required(ErrorMessage = "Default path is required")]
        [Display(Name = "Path Default")]
        public bool pathDefault { get; set; }

        [Column("pathNameL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Name L1")]
        public string NameL1 { get; set; }

        [Column("pathNameL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Name L2")]
        public string NameL2 { get; set; }
    }
}