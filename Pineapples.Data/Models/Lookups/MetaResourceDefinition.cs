﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("metaResourceDefs")]
	[Description(@"WARNING: Changes here have wide repercussions throughout various components of the system (PDF Survey, etc.). Change with great care with someone very familiar with the system. "
				+ "Definitions of Resources. Resources are grouped by Category. The category and Resource Code both appear on the Resources table.")]
	public partial class MetaResourceDefinition : ChangeTracked
	{
		[Key]
		[Column("mresID", TypeName = "int")]
		[Required(ErrorMessage = "Resource Category Code is required")]
		[Display(Name = "Resource ID")]
		public int ID { get; set; }

		[Column("mresCat", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Resource Category")]
		public string ResourceCategory { get; set; }

		[Column("mresSeq", TypeName = "int")]
		[Display(Name = "Sort")]
		[Description("Used to order resources in reporting")]
		public int? Seq { get; set; }

		[Column("mresName", TypeName = "nvarchar")]
		[MaxLength(150)]
		[StringLength(150)]
		[Display(Name = "Resource Name")]
		public string Name { get; set; }

		[Column("mresNameL1", TypeName = "nvarchar")]
		[MaxLength(150)]
		[StringLength(150)]
		[Display(Name = "Name L1")]
		public string NameL1 { get; set; }


		[Column("mresNameL2", TypeName = "nvarchar")]
		[MaxLength(150)]
		[StringLength(150)]
		[Display(Name = "Name L2")]
		public string NameL2 { get; set; }

		[Column("mresPromptAdq", TypeName = "bit")]
		[Display(Name = "Prompt Adequate")]
		public bool? PromptAdq { get; set; }

		[Column("mresPromptAvail", TypeName = "bit")]
		[Display(Name = "Prompt Available")]
		public bool? PromptAvail { get; set; }

		[Column("mresPromptNum", TypeName = "bit")]
		[Display(Name = "Prompt Number")]
		public bool? PromptNum { get; set; }

		[Column("mresPromptCondition", TypeName = "bit")]
		[Display(Name = "Prompt Condition")]
		public bool? PromptCondition { get; set; }

		[Column("mresPromptQty", TypeName = "bit")]
		[Display(Name = "Prompt Quantity")]
		public bool? PromptQty { get; set; }

		[Column("mresBuildingType", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Building Type")]
		public string BuildingType { get; set; }

		[Column("mresBuildingSubType", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Building Sub Type")]
		public string BuildingSubType { get; set; }

		[Column("mresPromptNote", TypeName = "bit")]
		[Display(Name = "Prompt Note")]
		public bool PromptNote { get; set; }

		[Column("mresSurveyed", TypeName = "bit")]
		[Display(Name = "Surveyed")]
		public bool? Surveyed { get; set; }
	}

}
