using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("SourceOfFundsGroup")]
	[Description("Categorisation of source of funds for reporting and layout on form.")]
	public partial class FundingSourceGroup : SimpleCodeTable
	{

		[Key]
		[Column("sfgCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "Code is required")]
		[Display(Name = "Code")]
		public override string Code { get; set; }
		[Column("sfgDesc", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Desc")]
		public override string Description { get; set; }

		[Column("sfgDescL1", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Desc L1")]
		public override string DescriptionL1 { get; set; }

		[Column("sfgDescL2", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Desc L2")]
		public override string DescriptionL2 { get; set; }
	}
}
