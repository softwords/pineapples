using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("lkpTeacherQual")]
	[Description(@"Qualifications a teacher may have. Collected on the teacher survey. The qualifications are divided into Ed and nonEd. The teacher's qualifications, and the sector in which they are teaching, determine if the teacher is qualified to teach and/or certified to teach.")]
	public partial class TeacherQual : SequencedCodeTable
	{
		[Key]

		[Column("codeCode", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Required(ErrorMessage = "code Code is required")]
		[Display(Name = "code Code")]
		public override string Code { get; set; }

		[MaxLength(500)]
		[StringLength(500)]
		[Display(Name = "Description L1")]
		[Column("codeDescriptionL1")]
		public override string DescriptionL1 { get; set; }

		[MaxLength(500)]
		[StringLength(500)]
		[Display(Name = "Description L2")]
		[Column("codeDescriptionL2")]
		public override string DescriptionL2 { get; set; }


		[Column("codeSeq", TypeName = "int")]
		[Display(Name = "Seq")]
		public override int? Seq { get; set; }

		[Column("codeTeach", TypeName = "bit")]
		[Display(Name = "Ed Qual")]
		public bool? codeTeach { get; set; }

		[Column("codeQualified", TypeName = "bit")]
		[Display(Name = "Qualified")]
		public bool? codeQualified { get; set; }

		[Column("codeCertified", TypeName = "bit")]
		[Display(Name = "Certified")]
		public bool? codeCertified { get; set; }

		[Column("codeGroup", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "code Group")]
		public string codeGroup { get; set; }

		[Column("codeInService", TypeName = "int")]
		[Display(Name = "In Service")]
		public int? codeInService { get; set; }

		[Column("codeDurationYears", TypeName = "int")]
		[Display(Name = "Expiry (years)")]
		public int? codeDurationYears { get; set; }

		[Column("codeSpEdTeach", TypeName = "bit")]
		[Display(Name = "SpEd Ed Qual")]
		public bool? codeSpEdTeach { get; set; }

		[Column("codeSpEdQualified", TypeName = "bit")]
		[Display(Name = "SpEd Qualified")]
		public bool? codeSpEdQualified { get; set; }

		[Column("codeSpEdCertified", TypeName = "bit")]
		[Display(Name = "SpEd Certified")]
		public bool? codeSpEdCertified { get; set; }

		[Column("codeOld", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "code Old")]
		public string codeOld { get; set; }
	}
}
