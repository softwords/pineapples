﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("ISCEDFieldGroup")]
	[Description(@"UNESCO defined field of study groups. Holds all narrow field groups of ISCED-F 2013.")]
	public partial class ISCEDFieldGroup : ChangeTracked
	{
		[Key]
		[Column("ifgID", TypeName = "nvarchar")]
		[MaxLength(3)]
		[StringLength(3)]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ISCED Field (Narrow) Group ID")]
		public string ID { get; set; }

		[Column("ifgName", TypeName = "nvarchar")]
		[MaxLength(250)]
		[StringLength(250)]
		[Display(Name = "ISCED Field (Narrow) Group Name")]
		public string Name { get; set; }

		[Column("ifgDesc", TypeName = "nvarchar")]
		[Display(Name = "ISCED Field (Narrow) Description")]
		public string Dsscription { get; set; }

		[Column("ifgbID", TypeName = "nvarchar")]
		[MaxLength(3)]
		[StringLength(3)]
		[Display(Name = "ISCED Field (Broad) Group")]
		[ClientLookup("iscedFieldGroupsBroad")]
		public string ISCEDGroupBroad { get; set; }
	}

}
