using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpBuildingSizeClass")]
    [Description(@"Partition of building floor area. Used to classify buildings, and as part of building valuation calculation, where together with BuildingValuationClass, it defines a rate per sq metre.")]
    public partial class BuildingSizeClass : SimpleCodeTable
    {
        [Key]

        [Column("bszCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "bsz Code is required")]
        [Display(Name = "bsz Code")]
        public override string Code { get; set; }

        [Column("bszDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "bsz Description is required")]
        [Display(Name = "bsz Description")]
        public override string Description { get; set; }

        [Column("bszDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bsz Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("bszDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bsz Description L2")]
        public override string DescriptionL2 { get; set; }

        [Column("bszMin", TypeName = "int")]
        [Required(ErrorMessage = "bsz Min is required")]
        [Display(Name = "bsz Min")]
        [Description(@"Min floor area (m2) for this size class")]
        public int Min { get; set; }

        [Column("bszMax", TypeName = "int")]
        [Required(ErrorMessage = "bsz Max is required")]
        [Display(Name = "bsz Max")]
        public int Max { get; set; }
    }
}
