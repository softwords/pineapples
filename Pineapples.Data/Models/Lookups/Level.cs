using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("lkpLevels")]
	[Description(@"Class Levels are the fundamental unit of recording enrolment. 
    The Class Level defines the Year Of Education (hence Education Level, Level Alt, Level Alt2) Sector, and ISCED Code,
    all critical for reporting. School types map to class level taught in that school type. 
    This metadata is used to set up survey form gtrids with the appropriate set of level codes.")]
	public partial class Level : SequencedCodeTable
	{
		[Key]
		[Column("codeCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "Code is required")]
		[Display(Name = "Code")]
		public override string Code { get; set; }

		//[Column("codeDescription", TypeName = "nvarchar")]
		//[MaxLength(50)]
		//[StringLength(50)]
		//[Display(Name = "code Description")]
		//public override string Description { get; set; }

		[Column("lvlYear", TypeName = "smallint")]
		[Display(Name = "Class Level Year")]
		public short? lvlYear { get; set; }

		[Column("ilsCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "ISCED Sub Level")]
		[ClientLookup("iscedLevelsSub")]
		public string ilsCode { get; set; }

		//[Column("codeDescriptionL1", TypeName = "nvarchar")]
		//[MaxLength(50)]
		//[StringLength(50)]
		//[Display(Name = "code Description L1")]
		//public override string DescriptionL1 { get; set; }

		//[Column("codeDescriptionL2", TypeName = "nvarchar")]
		//[MaxLength(50)]
		//[StringLength(50)]
		//[Display(Name = "code Description L2")]
		//public override string DescriptionL2 { get; set; }

		[Column("lvlGV", TypeName = "nvarchar")]
		[MaxLength(1)]
		[StringLength(1)]
		[Display(Name = "Class Level GV")]
		public string lvlGV { get; set; }

		[Column("secCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Education Sector")]
		[ClientLookup("educationSectors")]
		public string secCode { get; set; }

		[Column("lvlDefaultPath", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Class Level Default Path")]
		[ClientLookup("educationPaths")]
		public string lvlDefaultPath { get; set; }

	}
}
