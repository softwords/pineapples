﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

	[Table("ISCEDFieldGroupBroad")]
	[Description(@"UNESCO defined fields of study broad group. Holds all broad field groups of ISCED-F 2013.")]
	public partial class ISCEDFieldGroupBroad : ChangeTracked
	{
		[Key]
		[Column("ifgbID", TypeName = "nvarchar")]
		[MaxLength(2)]
		[StringLength(2)]
		[Required(ErrorMessage = "ID is required")]
		[Display(Name = "ISCED Field (Narrow) Group ID")]
		public string ID { get; set; }

		[Column("ifgbName", TypeName = "nvarchar")]
		[MaxLength(250)]
		[StringLength(250)]
		[Display(Name = "ISCED Field (Narrow) Group Name")]
		public string Name { get; set; }

		[Column("ifgbDesc", TypeName = "nvarchar")]
		[Display(Name = "ISCED Field (Narrow) Description")]
		public string Dsscription { get; set; }
	}

}
