using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("lkpEthnicity")]
	[Description(@"ethnicity of students")]
	public partial class Ethnicity : SequencedCodeTable
	{

		[Column("codeGroup", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Ethnicity Group")]
		[ClientLookup("ethnicityGroups")]
		public string EthnicityGroup { get; set; }

	}
}
