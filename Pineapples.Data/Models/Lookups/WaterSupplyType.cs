using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpWaterSupplyTypes")]
    [Description(@"Water supply types. Although this duplicates the resource Defs for water supply, it is required currently to hold the definition of Clean and Safe. Ie the Condition Level at which we consider this type of water clean and safe.")]
    public partial class WaterSupplyType : ChangeTracked
    {
        [Key]

        [Column("wsType", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "ws Type is required")]
        [Display(Name = "ws Type")]
        public string wsType { get; set; }

        [Column("wsCleanSafe", TypeName = "smallint")]
        [Display(Name = "ws Clean Safe")]
        public short? wsCleanSafe { get; set; }
    }
}
