﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpSpEdAssessmentTypes")]
    [Description("Special Education assessments")]
    public partial class SpEdAssessmentType : SequencedCodeTable
    {
    }

}
