using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("lkpSpEdEvaluationEventTypes")]
	[Description(@"Types of evaluation events in a SpEd student life cycle.")]
	public partial class SpEdEvaluationEventType : SequencedCodeTable
	{

		[Column("codeGroup", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "code Group")]
		public string codeGroup { get; set; }

		[Column("codeDurationYears", TypeName = "int")]
		[Display(Name = "Expiry (years)")]
		public int? codeDurationYears { get; set; }

	}
}
