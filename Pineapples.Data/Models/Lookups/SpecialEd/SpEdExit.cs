﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpSpEdExits")]
    [Description("Codes for manner of exiting Special Education")]
    public partial class SpEdExit:SequencedCodeTable
    {
    }

}
