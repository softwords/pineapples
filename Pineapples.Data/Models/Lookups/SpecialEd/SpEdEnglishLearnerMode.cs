﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpEnglishLearner")]
    [Description("Codes for English Learner in Special Ed module")]
    public partial class SpEdEnglishLearnerMode:SequencedCodeTable
    {
    }

}
