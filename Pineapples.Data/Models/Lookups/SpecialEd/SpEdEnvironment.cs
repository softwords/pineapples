﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpSpEdEnvironment")]
    [Description("Special Education environment in place to assist student")]
    public partial class SpEdEnvironment : SequencedCodeTable
    {
		[Key]
		[MaxLength(20)]
		[StringLength(20)]
		[Required(ErrorMessage = "Code is required")]
		[Display(Name = "Code")]
		[Column("codeCode")]
		public override string Code { get; set; }

		[MaxLength(100)]
		[StringLength(100)]
		public override string Description { get; set; }

		[MaxLength(20)]
		[StringLength(20)]
		[Required(ErrorMessage = "Group is required")]
		[Display(Name = "Group")]
		[Column("codeGroup")]
		public string Group { get; set; }

		[Column("codeText", TypeName = "nvarchar")]
		[MaxLength(2000)]
		[StringLength(2000)]
		[Display(Name = "Text")]
		public string codeText { get; set; }

	}

}
