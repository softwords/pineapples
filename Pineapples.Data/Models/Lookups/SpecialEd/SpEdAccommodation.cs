﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpSpEdAccommodations")]
    [Description("Special Education accommodations in place to assist student")]
    public partial class SpEdAccommodation : SequencedCodeTable
    {
    }

}
