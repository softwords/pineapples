using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("Publishers")]
	[Description(@"Publishers of books")]
	public partial class Publisher : SimpleCodeTable
	{
		[Key]

		[Column("pubCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required(ErrorMessage = "Publisher Code is required")]
		[Display(Name = "Publisher Code")]
		public override string Code { get; set; }

		[Column("pubName", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Required(ErrorMessage = "Publisher Code is required")]
		[Display(Name = "Publisher Name")]
		public override string Description { get; set; }

		[Column("pubDescriptionL1", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Publisher Description L1")]
		public override string DescriptionL1 { get; set; }

		[Column("pubDescriptionL2", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Publisher Description L2")]
		public override string DescriptionL2 { get; set; }
	}
}
