﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Pineapples.Data
{
	public class ScholarshipFilter : Filter
	{
		public ScholarshipFilter()
		{
			PageNo = 1;
			PageSize = 50;
			SortDirection = "asc";
			SortColumn = "stuFamilyName";
			ColumnSet = 0;
		}

		// 
		public int? ScholarshipID { get; set; }
		public string ScholarshipType { get; set; }
		public string ScholarshipStatus { get; set; }
		public DateTime? ScholarshipStatusChange { get; set; }
		public int? ScholarshipYear { get; set; }

		// selection based on student
		public Guid? StudentID { get; set; }
		public string StudentCardID { get; set; }
		public string StudentGiven { get; set; }
		public string StudentFamilyName { get; set; }
		public DateTime? StudentDoB { get; set; }
		public string StudentGender { get; set; }
		public string StudentEthnicity { get; set; }

		//selection based on enrolment
		public string EnrolledAt { get; set; }
		public int? EnrolYear { get; set; }
		public string EnrolLevel { get; set; }
		public string EnrolDistrict { get; set; }
		public string EnrolAuthority { get; set; }

		// added for AccessControl
		//public string District { get; set; }
		public void ApplyUserFilter(ClaimsIdentity identity)
		{
			Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
			if (c != null)
			{
				EnrolDistrict = c.Value;
			}
			c = identity.FindFirst(x => x.Type == "filterAuthority");
			if (c != null)
			{
				EnrolAuthority = c.Value;
			}
			c = identity.FindFirst(x => x.Type == "filterSchoolNo");
			if (c != null)
			{
				EnrolledAt = c.Value;
			}
		}
	}

	public class ScholarshipTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public ScholarshipFilter filter { get; set; }
    }
}
