﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	public class Scholarship: CreateTagged
	{
		
		[Column("schoID", TypeName = "int")]
		[Display(Name = "ID")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int? schoID { get; set; }

		[Column("stuID", TypeName = "uniqueidentifier")]
		[Display(Name = "Student ID")]
		public Guid? stuID { get; set; }

		[Column("scholCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required]
		[Display(Name = "Scholarship Type")]
		[ClientLookup("scholarshipTypes")]
		public string scholCode { get; set; }

		[Column("schrYear", TypeName = "int")]
		[Required]
		[Display(Name = "Year")]
		public int? schrYear { get; set; }

		[Column("schoExpectedCompletion", TypeName = "datetime")]
		[Display(Name = "Expected Completion")]
		public DateTime? schoExpectedCompletion { get; set; }

		[Column("schoAppliedDate", TypeName = "datetime")]
		[Display(Name = "Date Applied")]
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? schoAppliedDate { get; set; }

		[Column("schoCompliantDate", TypeName = "datetime")]
		[Display(Name = "Date Compliant")]
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? schoCompliantDate { get; set; }

		[Column("schoAwardedDate", TypeName = "datetime")]
		[Display(Name = "Date Awarded")]
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? schoAwardedDate { get; set; }

		[Column("schoCompletedDate", TypeName = "datetime")]
		[Display(Name = "Date Completed")]
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? schoCompletedDate { get; set; }

		[Column("schoNote", TypeName = "nvarchar")]
		[Display(Name = "Note")]
		[MaxLength(500)]
		[StringLength(500)]
		public string schoNote { get; set; }

		[Column("schoStatus", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Status")]
		[ClientLookup("scholarshipStatus")]
		public string schoStatus { get; set; }

		[Column("schoStatusDate", TypeName = "datetime")]
		[Display(Name = "Status Date")]
		public DateTime? schoStatusDate { get; set; }

		[Column("schoFos", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required]
		[Display(Name = "Field of Study")]
		[ClientLookup("fieldsOfStudy")]
		public string schoFos { get; set; }

		[Column("schoInst", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required]
		[Display(Name = "Institution")]
		[ClientLookup("scholarshipInstitutions")]
		public string schoInst { get; set; }

		[Column("schoSubject", TypeName = "nvarchar")]
		[MaxLength(200)]
		[StringLength(200)]
		[Display(Name = "Subject")]
		public string schoSubject { get; set; }

		[Column("schoGPA", TypeName = "decimal")]
		[Display(Name = "GPA",Description ="GPA for last year of school",Prompt = "Final school GPA")]
		public decimal? schoGPA { get; set; }


		[Column("stuCardID", TypeName = "nvarchar")]
		[MaxLength(40)]
		[StringLength(40)]
		[Display(Name = "Card ID")]
		public string stuCardID { get; set; }
		[Column("stuNamePrefix", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Name Prefix")]
		public string stuNamePrefix { get; set; }
		[Column("stuGiven", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Given")]
		public string stuGiven { get; set; }
		[Column("stuMiddleNames", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Middle Names")]
		public string stuMiddleNames { get; set; }
		[Column("stuFamilyName", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Family Name")]
		public string stuFamilyName { get; set; }

		[Column("stuNameSuffix", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Name Suffix")]
		public string stuNameSuffix { get; set; }

		[Column("stuDoB", TypeName = "date")]
		[Display(Name = "Do B")]
		public DateTime? stuDoB { get; set; }

		[Column("stuDoBEst", TypeName = "int")]
		[Display(Name = "Do B Est")]
		public int? stuDoBEst { get; set; }

		[Column("stuGender", TypeName = "nvarchar")]
		[MaxLength(1)]
		[StringLength(1)]
		[Display(Name = "Gender")]
		[ClientLookup("gender")]
		public string stuGender { get; set; }

		[Column("stuMaritalStatus", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Marital Status")]
		[ClientLookup("maritalStatus")]
		public string stuMaritalStatus { get; set; }

		[Column("stuEthnicity", TypeName = "nvarchar")]
		[MaxLength(200)]
		[StringLength(200)]
		[Display(Name = "Ethnicity")]
		[ClientLookup("ethnicities")]
		public string stuEthnicity { get; set; }

	}

}
