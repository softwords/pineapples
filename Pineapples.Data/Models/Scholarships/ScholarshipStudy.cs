﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("ScholarshipStudy_")]
	public class ScholarshipStudy:ChangeTracked
	{
		[Key]
		[Column("posID", TypeName = "int")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Display(Name = "ID")]
		public int? posID { get; set; }

		[Column("schoID", TypeName = "int")]
		[Required]
		[Display(Name = "ID")]
		public int schoID { get; set; }

		[Column("posYear", TypeName = "int")]
		[Display(Name = "Year")]
		public int? posYear { get; set; }

		[Column("gyIndex", TypeName = "int")]
		[Required]
		[Display(Name = "Index")]
		public int gyIndex { get; set; }

		[Column("posSemester", TypeName = "int")]
		[Required]
		[Display(Name = "Semester")]
		public int posSemester { get; set; }

		[Column("posAmount", TypeName = "decimal")]
		[Display(Name = "Amount")]
		public decimal? posAmount { get; set; }

		[Column("posPaid", TypeName = "datetime")]
		[Display(Name = "Paid")]
		public DateTime? posPaid { get; set; }

		[Column("posGPA", TypeName = "decimal")]
		[Display(Name = "GPA")]
		public decimal? posGPA { get; set; }

		[Column("fosCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Code")]
		[ClientLookup("fieldsOfStudy")]
		public string fosCode { get; set; }

		[Column("instCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Code")]
		[ClientLookup("scholarshipInstitutions")]
		public string instCode { get; set; }

		[Column("posSubject", TypeName = "nvarchar")]
		[MaxLength(200)]
		[StringLength(200)]
		[Display(Name = "Subject")]
		public string posSubject { get; set; }
	}

}
