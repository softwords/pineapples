﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("ScholarshipRounds")]
	public class ScholarshipRound : ChangeTracked
	{
		[Key]
		[Column("schrID", TypeName = "int")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required]
		[Display(Name = "ID")]
		public int ID { get; set; }
		[Column("scholCode", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Required]
		[ClientLookup("scholarshipTypes")]
		[Display(Name = "Code")]
		public string scholCode { get; set; }
		[Column("schrYear", TypeName = "int")]
		[Required]
		[Display(Name = "Year")]
		public int Year { get; set; }
		[Column("schrBudget", TypeName = "decimal")]
		[Display(Name = "Budget")]
		public decimal? Budget { get; set; }
	}

}
