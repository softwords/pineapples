﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{

    [Table("pScholarshipRead.ScholarshipLinks")]
    public partial class ScholarshipLink : CreateTagged
    {
        [Display(Name = "lnk ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int lnkID { get; set; }

        [Required(ErrorMessage = "Scholarship No is required")]
        [Display(Name = "Scholarship No")]
        public int? schoID { get; set; }

        [Required(ErrorMessage = "Document ID is required")]
        [Display(Name = "Doc ID")]
        public Guid docID { get; set; }

        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Function")]
        public string lnkFunction { get; set; }

        [Display(Name = "Hidden")]
        public int lnkHidden { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Document Title")]
        public string docTitle { get; set; }

        [MaxLength(400)]
        [StringLength(400)]
        [Display(Name = "Document Description")]
        public string docDescription { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Document Source")]
        public string docSource { get; set; }

        [Display(Name = "Document Date")]
        public DateTime? docDate { get; set; }

        [Required(ErrorMessage = "Rotate is required")]
        [Display(Name = "Rotate")]
        public int docRotate { get; set; }

        [MaxLength(200)]
        [StringLength(200)]
        [Display(Name = "Tags")]
        public string docTags { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "File Type")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string docType { get; set; }

    }

}
