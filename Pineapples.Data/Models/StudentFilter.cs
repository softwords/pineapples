﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Pineapples.Data
{
	public class StudentFilter : Filter2
	{
		public StudentFilter()
		{
			PageNo = 1;
			PageSize = 50;
			SortDirection = "asc";
			SortColumn = "tGiven";
			ColumnSet = 0;
		}

		public Guid? StudentID { get; set; }
		public string StudentCardID { get; set; }
		public string StudentGiven { get; set; }
		public string StudentFamilyName { get; set; }
		public DateTime? StudentDoB { get; set; }
		public string StudentGender { get; set; }
		public string StudentEthnicity { get; set; }

		//selection based on enrolment
		public string EnrolledAt { get; set; }
		public int? EnrolYear { get; set; }
		public string EnrolLevel { get; set; }
		public string EnrolDistrict { get; set; }
		public string EnrolAuthority { get; set; }

		// added for AccessControl
		//public string District { get; set; }
		public void ApplyUserFilter(ClaimsIdentity identity)
		{
			Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
			if (c != null)
			{
				EnrolDistrict = c.Value;
			}
			c = identity.FindFirst(x => x.Type == "filterAuthority");
			if (c != null)
			{
				EnrolAuthority = c.Value;
			}
			c = identity.FindFirst(x => x.Type == "filterSchoolNo");
			if (c != null)
			{
				EnrolledAt = c.Value;
			}
		}
	}

	public class StudentTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public StudentFilter filter { get; set; }
    }

	public class StudentDeduperFilter : Filter
	{
		public int? MatchGiven { get; set; }
		public int? MatchSurname { get; set; }
		public int? MatchCard { get; set; }
		public int? MatchDoB { get; set; }
		public int? MatchGender { get; set; }

		public string @Given { get; set; }
		public string @Surname { get; set; }
		public string District { get; set; }

		public void ApplyUserFilter(ClaimsIdentity identity)
		{
			// just filter on district; should be adequate
			Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
			if (c != null)
			{
				District = c.Value;
			}
		}
	}
}
