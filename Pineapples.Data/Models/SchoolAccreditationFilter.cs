﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
	/// <summary>
	/// Where required, specific system-supported inspection types
	/// can subclass the SchoolInspectionFilter
	/// </summary>
    public class SchoolAccreditationFilter : SchoolInspectionFilter
    {
		/// <summary>
		/// an alias fo InspID
		/// </summary>
        public string SAID
		{
			get
			{
				return InspID;
			}
			set
			{
				InspID = value;
			}
		}

		public string FilterColumn { get; set; }
		public int? FilterValue { get; set; }
		public int? BestUpTo { get; set; }

	}
	public class SchoolAccreditationTableFilter : TableFilter<SchoolAccreditationFilter> { }
}
