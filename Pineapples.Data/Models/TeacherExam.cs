﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Pineapples.Data.Models
{
	[Table("TeacherExams")]
	[Description("Each record in TeacherExams represents an instance of a particular teacher examination held in one year.")]
	public partial class TeacherExam
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Display(Name = "ID")]
		public int texID { get; set; }

		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Code")]
		public string texCode { get; set; }

		[Display(Name = "Year")]
		public int? texYear { get; set; }

		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "User")]
		public string texUser { get; set; }

		[Display(Name = "Date")]
		public DateTime? texDate { get; set; }

		[Display(Name = "Candidates")]
		public int? texCandidates { get; set; }

		[Display(Name = "Marks")]
		public int? texMarks { get; set; }

		[Display(Name = "Subject Score")]
		public double? texSubjectScore { get; set; }

		[Display(Name = "Candidate Score")]
		public double? texCandidateScore { get; set; }

		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Scoring Model")]
		public string texScoringModel { get; set; }

		[Display(Name = "Num Scores")]
		public int? texsmNumScores { get; set; }

		[MaxLength(255)]
		[StringLength(255)]
		[Display(Name = "Mandatory")]
		public string texsmMandatory { get; set; }

		[MaxLength(255)]
		[StringLength(255)]
		[Display(Name = "Excluded")]
		public string texsmExcluded { get; set; }

		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Exporter Version")]
		public string texExporterVersion { get; set; }

		[Display(Name = "Export Date")]
		public DateTime? texExportDate { get; set; }

		[Display(Name = "File ID")]
		public Guid? docID { get; set; }
	}
}