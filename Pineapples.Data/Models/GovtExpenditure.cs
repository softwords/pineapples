﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
	[Table("GovtExpenditureState")]
	[Description("Macroeconomic date per year, such as government expenditure, GNP GNP per capita.")]
	public class GovtExpenditure:ChangeTracked
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("fnmID", TypeName = "int")]
		public int? fnmID { get; set; }

		[Column("fnmYear", TypeName = "int")]
		[Display(Name = "Year")]
		public int? fnmYear { get; set; }

		[Column("fnmDistrict", TypeName = "nvarchar")]
		[Display(Name = "District")]
		public string fnmDistrict { get; set; }

		[Column("fnmExpTotA", TypeName = "money")]
		[Display(Name = "Exp Tot A")]
		public decimal? fnmExpTotA { get; set; }

		[Column("fnmExpTotB", TypeName = "money")]
		[Display(Name = "Exp Tot B")]
		public decimal? fnmExpTotB { get; set; }

		[Column("fnmGNP", TypeName = "money")]
		[Display(Name = "GNP")]
		public decimal? fnmGNP { get; set; }

		[Column("fnmGNPCapita", TypeName = "money")]
		[Display(Name = "GNP Capita")]
		public decimal? fnmGNPCapita { get; set; }

		[Column("fnmSource", TypeName = "nvarchar")]
		[MaxLength(100)]
		[StringLength(100)]
		[Display(Name = "Source")]
		public string fnmSource { get; set; }

		[Column("fnmNote", TypeName = "ntext")]
		[Display(Name = "Note")]
		public string fnmNote { get; set; }

		[Column("fnmGNPCurrency", TypeName = "nvarchar")]
		[MaxLength(3)]
		[StringLength(3)]
		[Display(Name = "GNP Currency")]
		public string fnmGNPCurrency { get; set; }

		[Column("fnmGNPXchange", TypeName = "float")]
		[Display(Name = "GNP Xchange")]
		public double? fnmGNPXchangeRate { get; set; }

		public virtual ICollection<EdExpenditure> EdSpending { get; set; }
	}

}
