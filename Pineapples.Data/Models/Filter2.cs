﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Reflection;
using Microsoft.Ajax.Utilities;
using System.Web.UI.WebControls;

namespace Pineapples.Data
{
	public class Filter2
	{
		// The class Filter in Softwords.DataTools is a legacy class supporting older interfaces
		// (ComInterop, WPF) that are not required here. Filter represents the filter as a Dictionary of FilterParams objects, 
		// but none of this functionality is used any longer - the modern filter simply has a property for each parameter.
		// xmlFilter in the base class works on the dictionary of parameters.
		// So this gets broken when moving to simple, flat properties to hold the search values
		// This class can replace dependency on Softwords.DataTools.Filter

		public int ColumnSet { get; set; }
		public int PageSize { get; set; }

		public int PageNo { get; set; }

		public string SortColumn { get; set; }
		
		public bool SortDesc { get; set; }

		private string _sortDirection;
		public string SortDirection
		{
			get
			{
				return _sortDirection;
			}
			set
			{
				_sortDirection = value;
				SortDesc = ((_sortDirection == "desc") ? true : false);
			}
		}

		public string xmlFilter()
		{
			XDocument doc = new XDocument();
			doc = XDocument.Parse("<Filter/>");
			Type t = this.GetType();

			// reflection, and linq
			t.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)
				.Where(p => p.GetValue(this) != null)
				.ForEach(pi => {
					XAttribute a = new XAttribute(pi.Name, pi.GetValue(this));
					doc.Root.Add(a);
				});
			return doc.ToString();
		}
		
	}
}
