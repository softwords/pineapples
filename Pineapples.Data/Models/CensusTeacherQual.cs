﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Pineapples.Data.Models
{
	[Table("SurveyYearTeacherQual")]
	[Description(@"For a given survey year and ed sector, 
defines which Teacher qualifications make the teacher certified and/or Qualified. 
Integral to reporting on Teacher Qualification/Certification.")]
	public class CensusTeacherQual : ChangeTracked
	{
		[Key]
		[Column("ytqID", TypeName = "int")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Display(Name = "ID")]
		public int? ytqID { get; set; }

		[Column("svyYear", TypeName = "int")]
		[Display(Name = "Year")]
		public int? svyYear { get; set; }

		[Column("ytqSector", TypeName = "nvarchar")]
		[MaxLength(3)]
		[StringLength(3)]
		[Display(Name = "Sector")]
		public string ytqSector { get; set; }

		[Column("ytqQual", TypeName = "nvarchar")]
		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Qual")]
		public string ytqQual { get; set; }

		[Column("ytqQualified", TypeName = "bit")]
		[Display(Name = "Qualified")]
		public bool? ytqQualified { get; set; }
		[Column("ytqCertified", TypeName = "bit")]
		[Display(Name = "Certified")]
		public bool? ytqCertified { get; set; }
	}

}