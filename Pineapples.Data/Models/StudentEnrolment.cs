﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("StudentEnrolment_")]
    public class StudentEnrolment:ChangeTracked
    {
        [Key]
        [Column("stueID", TypeName = "int")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int stueID { get; set; }

        [Column("stuID", TypeName = "uniqueidentifier")]
        [Display(Name = "ID")]
        public Guid? stuID { get; set; }

        [Column("schNo", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "No")]
        public string schNo { get; set; }

        [Column("stueYear", TypeName = "int")]
        [Display(Name = "Year")]
        public int? stueYear { get; set; }

        [Column("stueClass", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Class")]
        public string stueClass { get; set; }

        [Column("stueFrom", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "From")]
        [Description("How the student arrived in this class Promoted Repeater - from Ece - Transfer In")]
        public string stueFrom { get; set; }

        [Column("stueFromSchool", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "From School")]
        public string stueFromSchool { get; set; }
        [Column("stueFromDate", TypeName = "date")]
        [Display(Name = "From Date")]
        public DateTime? stueFromDate { get; set; }

        [Column("stueSpEd", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Display(Name = "Sp Ed")]
        public string stueSpEd { get; set; }

        [Column("stueSpEdEnv", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Sp Ed Env")]
        public string stueSpEdEnv { get; set; }

        [Column("stueSpEdDisability", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Sp Ed Disability")]
        public string stueSpEdDisability { get; set; }

        [Column("stueSpEdEnglish", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Sp Ed English")]
        public string stueSpEdEnglish { get; set; }

        [Column("stueSpEdIEP", TypeName = "int")]
        [Display(Name = "Sp Ed IEP")]
        public int? stueSpEdIEP { get; set; }

        [Column("stueSpEdHasAccommodation", TypeName = "int")]
        [Display(Name = "Sp Ed Has Accommodation")]
        public int? stueSpEdHasAccommodation { get; set; }

        [Column("stueSpEdAccommodation", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Sp Ed Accommodation")]
        public string stueSpEdAccommodation { get; set; }

        [Column("stueSpEdAssessment", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Sp Ed Assessment")]
        public string stueSpEdAssessment { get; set; }

        [Column("stueSpEdExit", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Sp Ed Exit")]
        public string stueSpEdExit { get; set; }

        [Column("stueDaysAbsent", TypeName = "decimal")]
        [Display(Name = "Days Absent")]
        public decimal? stueDaysAbsent { get; set; }

        [Column("stueCompleted", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Display(Name = "Completed")]
        public string stueCompleted { get; set; }

        [Column("stueOutcome", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Outcome")]
        public string stueOutcome { get; set; }

        [Column("stueOutcomeReason", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Outcome Reason")]
        public string stueOutcomeReason { get; set; }

        [Column("stueCreateFileRef", TypeName = "uniqueidentifier")]
        [Display(Name = "Create File Ref")]
        [Description("the filedb identifier of the workbook that created this record")]
        public Guid? stueCreateFileRef { get; set; }

        [Column("stueCreateFileRow", TypeName = "int")]
        [Display(Name = "Create File Row")]
        [Description("Index into the table of students in the source workbook")]
        public int? stueCreateFileRow { get; set; }

        [Column("stueData", TypeName = "xml")]
        [Display(Name = "Data")]
        public string stueData { get; set; }
       
    }

}
