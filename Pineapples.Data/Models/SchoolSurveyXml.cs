﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models { 

	[Table("SchoolSurveyXml_")]
	[Description(@"Header for the school survey. All the data on the survey is related to this by the ssID - primary key on this table. 
    The combination of schNo / svyYear is a unique constraint.schNo is foregn key from Schools, svyYEar is foregin key from survey.")]
	public class SchoolSurveyXml: ChangeTracked
	{
		[Key]
		[Column("ssID", TypeName = "int")]
		[Required]
		[Display(Name = "ID")]
		public int? ssID { get; set; }

		[Required]
		[Column("ssXml", TypeName = "xml")]
		[Display(Name = "Xml")]
		public string ssXml { get; set; }
	}
}
