using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models {
    [Table("SchoolSurvey")]
    [Description(@"Header for the school survey. All the data on the survey is related to this by the ssID - primary key on this table. 
    The combination of schNo / svyYear is a unique constraint.schNo is foregn key from Schools, svyYEar is foregin key from survey.")]
    public class SchoolSurvey
    {
        [Key]
        [Column("ssID", TypeName = "int")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Display(Name = "ID")]
        public int? ssID { get; set; }
        [Column("svyYear", TypeName = "smallint")]
        [Required]
        [Display(Name = "Year")]
        public short svyYear { get; set; }
        [Column("schNo", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required]
        [Display(Name = "No")]
        public string schNo { get; set; }
        [Column("ssSchType", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required]
        [Display(Name = "Sch Type")]
        public string ssSchType { get; set; }
        [Column("ssLang", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "Lang")]
        public string ssLang { get; set; }
        [Column("ssLangLower", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "Lang Lower")]
        public string ssLangLower { get; set; }
        [Column("ssSurveyFormat", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Survey Format")]
        public string ssSurveyFormat { get; set; }
        [Column("ssPrinFirstName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Prin First Name")]
        public string ssPrinFirstName { get; set; }
        [Column("ssPrinSurname", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Prin Surname")]
        public string ssPrinSurname { get; set; }

		[Column("ssPrinPh", TypeName = "nvarchar")]
		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Prin Ph")]
		public string ssPrinPh { get; set; }

		[Column("ssAuth", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Auth")]
        public string ssAuth { get; set; }
        [Column("ssOperator", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Operator")]
        public string ssOperator { get; set; }
        [Column("ssElectL", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "Elect L")]
        public string ssElectL { get; set; }
        [Column("ssElectN", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "Elect N")]
        public string ssElectN { get; set; }
        [Column("ssMuni", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "Muni")]
        public string ssMuni { get; set; }
        [Column("ssISClass", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "IS Class")]
        public string ssISClass { get; set; }
        [Column("ssISSize", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "IS Size")]
        public string ssISSize { get; set; }
        [Column("ssLandOwner", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Land Owner")]
        public string ssLandOwner { get; set; }
        [Column("ssLandUse", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Land Use")]
        public string ssLandUse { get; set; }
        [Column("ssLandUseExpiry", TypeName = "datetime")]
        [Display(Name = "Land Use Expiry")]
        public DateTime? ssLandUseExpiry { get; set; }
        [Column("ssSponsor", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Sponsor")]
        public string ssSponsor { get; set; }
        [Column("ssIncome", TypeName = "money")]
        [Display(Name = "Income")]
        public decimal? ssIncome { get; set; }
        [Column("ssFundRaising", TypeName = "money")]
        [Display(Name = "Fund Raising")]
        public decimal? ssFundRaising { get; set; }
        [Column("ssDonationPriv", TypeName = "money")]
        [Display(Name = "Donation Priv")]
        public decimal? ssDonationPriv { get; set; }
        [Column("ssDonationChurch", TypeName = "money")]
        [Display(Name = "Donation Church")]
        public decimal? ssDonationChurch { get; set; }
        [Column("ssDonationOther", TypeName = "money")]
        [Display(Name = "Donation Other")]
        public decimal? ssDonationOther { get; set; }
        [Column("ssGovtGrant", TypeName = "money")]
        [Display(Name = "Govt Grant")]
        public decimal? ssGovtGrant { get; set; }
        [Column("ssExpSupplies", TypeName = "money")]
        [Display(Name = "Exp Supplies")]
        public decimal? ssExpSupplies { get; set; }
        [Column("ssExpBooks", TypeName = "money")]
        [Display(Name = "Exp Books")]
        public decimal? ssExpBooks { get; set; }
        [Column("ssExpOther", TypeName = "money")]
        [Display(Name = "Exp Other")]
        public decimal? ssExpOther { get; set; }
        [Column("ssSchoolCouncil", TypeName = "bit")]
        [Display(Name = "School Council")]
        public bool? ssSchoolCouncil { get; set; }
        [Column("ssSCMeet", TypeName = "smallint")]
        [Display(Name = "SC Meet")]
        public short? ssSCMeet { get; set; }
        [Column("ssParentCommittee", TypeName = "bit")]
        [Display(Name = "Parent Committee")]
        public bool? ssParentCommittee { get; set; }
        [Column("ssPCMeet", TypeName = "smallint")]
        [Display(Name = "PC Meet")]
        public short? ssPCMeet { get; set; }
        [Column("ssPCSupport", TypeName = "smallint")]
        [Display(Name = "PC Support")]
        public short? ssPCSupport { get; set; }
        [Column("ssTotalPupils", TypeName = "int")]
        [Display(Name = "Total Pupils")]
        [Description("Some surveys have recorded aggregate totals of classes, teachers and pupils, as well as detailed grids of enrolment numbers and teacher lists. These fields should only be used as checksums - use ssEnrol to get the total enrolment for this survey, or add from Enrollments table")]
        public int? ssTotalPupils { get; set; }
        [Column("ssTotalClasses", TypeName = "int")]
        [Display(Name = "Total Classes")]
        public int? ssTotalClasses { get; set; }
        [Column("ssTotalTeachers", TypeName = "int")]
        [Display(Name = "Total Teachers")]
        [Description("'Total teachers' is entered on some survey formats.  Should not be used for reporting, only to cross check the teacher data stored in TeacherSurvey table")]
        public int? ssTotalTeachers { get; set; }
        [Column("ssPreSchoolM", TypeName = "int")]
        [Display(Name = "Pre School M")]
        public int? ssPreSchoolM { get; set; }
        [Column("ssPreSchoolF", TypeName = "int")]
        [Display(Name = "Pre School F")]
        public int? ssPreSchoolF { get; set; }
        [Column("ssSizeSite", TypeName = "int")]
        [Display(Name = "Size Site")]
        public int? ssSizeSite { get; set; }
        [Column("ssSizePlayground", TypeName = "int")]
        [Display(Name = "Size Playground")]
        public int? ssSizePlayground { get; set; }
        [Column("ssSizeFarm", TypeName = "int")]
        [Display(Name = "Size Farm")]
        public int? ssSizeFarm { get; set; }
        [Column("ssSizeFarmUsed", TypeName = "int")]
        [Display(Name = "Size Farm Used")]
        public int? ssSizeFarmUsed { get; set; }
        [Column("ssSizeRating", TypeName = "smallint")]
        [Display(Name = "Size Rating")]
        public short? ssSizeRating { get; set; }
        [Column("ssSiteSecure", TypeName = "smallint")]
        [Display(Name = "Site Secure")]
        public short? ssSiteSecure { get; set; }
        [Column("ssRubbishFreq", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Rubbish Freq")]
        public string ssRubbishFreq { get; set; }
        [Column("ssRubbishMethod", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Rubbish Method")]
        public string ssRubbishMethod { get; set; }
        [Column("ssToiletSea", TypeName = "bit")]
        [Display(Name = "Toilet Sea")]
        public bool? ssToiletSea { get; set; }
        [Column("ssWaterRating", TypeName = "smallint")]
        [Display(Name = "Water Rating")]
        public short? ssWaterRating { get; set; }
        [Column("ssTchHouse", TypeName = "int")]
        [Display(Name = "Tch House")]
        public int? ssTchHouse { get; set; }
        [Column("ssTchHouseOnSite", TypeName = "int")]
        [Display(Name = "Tch House On Site")]
        public int? ssTchHouseOnSite { get; set; }
        [Column("ssTchHouseOffSite", TypeName = "int")]
        [Display(Name = "Tch House Off Site")]
        public int? ssTchHouseOffSite { get; set; }
        [Column("ssTchHouseM", TypeName = "int")]
        [Display(Name = "Tch House M")]
        public int? ssTchHouseM { get; set; }
        [Column("ssTchHouseN", TypeName = "int")]
        [Display(Name = "Tch House N")]
        public int? ssTchHouseN { get; set; }
        [Column("ssNote", TypeName = "ntext")]
        [Display(Name = "Note")]
        public string ssNote { get; set; }
        [Column("ssReview", TypeName = "bit")]
        [Display(Name = "Review")]
        public bool? ssReview { get; set; }
        [Column("ssReviewComment", TypeName = "ntext")]
        [Display(Name = "Review Comment")]
        public string ssReviewComment { get; set; }
        [Column("ssFacilitiesComment", TypeName = "ntext")]
        [Display(Name = "Facilities Comment")]
        public string ssFacilitiesComment { get; set; }
        [Column("ssSvcBank", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Svc Bank")]
        public string ssSvcBank { get; set; }
        [Column("ssSvcAir", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Svc Air")]
        public string ssSvcAir { get; set; }
        [Column("ssSvcShip", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Svc Ship")]
        public string ssSvcShip { get; set; }
        [Column("ssSvcClinic", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Svc Clinic")]
        public string ssSvcClinic { get; set; }
        [Column("ssSvcPost", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Svc Post")]
        public string ssSvcPost { get; set; }
        [Column("ssSvcHospital", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Svc Hospital")]
        public string ssSvcHospital { get; set; }
        [Column("ssSvcSupplies", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Svc Supplies")]
        public string ssSvcSupplies { get; set; }
        [Column("ssExt", TypeName = "bit")]
        [Display(Name = "Ext")]
        public bool? ssExt { get; set; }
        [Column("ssIsSchool1", TypeName = "smallint")]
        [Display(Name = "Is School1")]
        public short? ssIsSchool1 { get; set; }
        [Column("ssNearestSchool1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Nearest School1")]
        public string ssNearestSchool1 { get; set; }
        [Column("ssParent", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Parent")]
        public string ssParent { get; set; }
        [Column("ssAdminLocn", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Admin Locn")]
        public string ssAdminLocn { get; set; }
        [Column("ssAdminDistance", TypeName = "float")]
        [Display(Name = "Admin Distance")]
        public double? ssAdminDistance { get; set; }
        [Column("ssAdminTime", TypeName = "float")]
        [Display(Name = "Admin Time")]
        public double? ssAdminTime { get; set; }
        [Column("ssNearestPS", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Nearest PS")]
        public string ssNearestPS { get; set; }
        [Column("ssPSDistance", TypeName = "float")]
        [Display(Name = "PS Distance")]
        public double? ssPSDistance { get; set; }
        [Column("ssPSTime", TypeName = "float")]
        [Display(Name = "PS Time")]
        public double? ssPSTime { get; set; }
        [Column("ssCatch", TypeName = "ntext")]
        [Display(Name = "Catch")]
        public string ssCatch { get; set; }
        [Column("ssAccess", TypeName = "nvarchar")]
        [MaxLength(2)]
        [StringLength(2)]
        [Display(Name = "Access")]
        public string ssAccess { get; set; }
        [Column("ssPlan0", TypeName = "int")]
        [Display(Name = "Plan0")]
        public int? ssPlan0 { get; set; }
        [Column("ssPlan1", TypeName = "int")]
        [Display(Name = "Plan1")]
        public int? ssPlan1 { get; set; }
        [Column("ssPlan2", TypeName = "int")]
        [Display(Name = "Plan2")]
        public int? ssPlan2 { get; set; }
        [Column("ssPlan3", TypeName = "int")]
        [Display(Name = "Plan3")]
        public int? ssPlan3 { get; set; }
        [Column("ssPlan4", TypeName = "int")]
        [Display(Name = "Plan4")]
        public int? ssPlan4 { get; set; }
        [Column("ssBuildingType", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Building Type")]
        public string ssBuildingType { get; set; }
        [Column("ssBuildingTypeOther", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Building Type Other")]
        public string ssBuildingTypeOther { get; set; }
        [Column("ssOperatedBy", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Operated By")]
        public string ssOperatedBy { get; set; }
        [Column("ssMissingData", TypeName = "bit")]
        [Display(Name = "Missing Data")]
        public bool? ssMissingData { get; set; }
        [Column("ssMissingDataNotSupplied", TypeName = "bit")]
        [Display(Name = "Missing Data Not Supplied")]
        public bool? ssMissingDataNotSupplied { get; set; }
        [Column("ssEnrolM", TypeName = "int")]
        [Display(Name = "Enrol M")]
        public int? ssEnrolM { get; set; }
        [Column("ssEnrolF", TypeName = "int")]
        [Display(Name = "Enrol F")]
        public int? ssEnrolF { get; set; }
        [Column("ssEnrol", TypeName = "int")]
        [Display(Name = "Enrol")]
        public int? ssEnrol { get; set; }
        [Column("ssFarmL", TypeName = "int")]
        [Display(Name = "Farm L")]
        public int? ssFarmL { get; set; }
        [Column("ssFarmUsedL", TypeName = "int")]
        [Display(Name = "Farm Used L")]
        public int? ssFarmUsedL { get; set; }
        [Column("ssFarmUsedW", TypeName = "int")]
        [Display(Name = "Farm Used W")]
        public int? ssFarmUsedW { get; set; }
        [Column("ssFarmW", TypeName = "int")]
        [Display(Name = "Farm W")]
        public int? ssFarmW { get; set; }
        [Column("ssPlaygroundL", TypeName = "int")]
        [Display(Name = "Playground L")]
        public int? ssPlaygroundL { get; set; }
        [Column("ssPlaygroundW", TypeName = "int")]
        [Display(Name = "Playground W")]
        public int? ssPlaygroundW { get; set; }
        [Column("ssSiteL", TypeName = "int")]
        [Display(Name = "Site L")]
        public int? ssSiteL { get; set; }
        [Column("ssSiteW", TypeName = "int")]
        [Display(Name = "Site W")]
        public int? ssSiteW { get; set; }
        [Column("ssSCApproved", TypeName = "bit")]
        [Display(Name = "SC Approved")]
        public bool? ssSCApproved { get; set; }
        [Column("ssPNAReason", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "PNA Reason")]
        public string ssPNAReason { get; set; }
        [Column("ssAcaYearEndM", TypeName = "int")]
        [Display(Name = "Aca Year End M")]
        public int? ssAcaYearEndM { get; set; }
        [Column("ssAcaYearEndY", TypeName = "int")]
        [Display(Name = "Aca Year End Y")]
        public int? ssAcaYearEndY { get; set; }
        [Column("ssAcaYearStartM", TypeName = "int")]
        [Display(Name = "Aca Year Start M")]
        public int? ssAcaYearStartM { get; set; }
        [Column("ssAcaYearStartY", TypeName = "int")]
        [Display(Name = "Aca Year Start Y")]
        public int? ssAcaYearStartY { get; set; }
        [Column("ssCoreSubject1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Core Subject1")]
        public string ssCoreSubject1 { get; set; }
        [Column("ssCoreSubject2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Core Subject2")]
        public string ssCoreSubject2 { get; set; }
        [Column("ssCurriculum", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Curriculum")]
        public string ssCurriculum { get; set; }
        [Column("ssPCMembersF", TypeName = "smallint")]
        [Display(Name = "PC Members F")]
        public short? ssPCMembersF { get; set; }
        [Column("ssPCMembersM", TypeName = "smallint")]
        [Display(Name = "PC Members M")]
        public short? ssPCMembersM { get; set; }
        [Column("ssRandM", TypeName = "int")]
        [Display(Name = "Rand M")]
        public int? ssRandM { get; set; }
        [Column("ssRandMResp", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Rand M Resp")]
        public string ssRandMResp { get; set; }
        [Column("ssRubbishNote", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Rubbish Note")]
        public string ssRubbishNote { get; set; }
        [Column("ssSiteHost", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Site Host")]
        public string ssSiteHost { get; set; }
        [Column("ssSiteHostDetail", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Site Host Detail")]
        public string ssSiteHostDetail { get; set; }
        [Column("ssEdSystem", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Ed System")]
        public string ssEdSystem { get; set; }
        [Column("ssMediumOfInstruction", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Medium Of Instruction")]
        public string ssMediumOfInstruction { get; set; }
        [Column("ssSCActivities", TypeName = "nvarchar")]
        [MaxLength(400)]
        [StringLength(400)]
        [Display(Name = "SC Activities")]
        public string ssSCActivities { get; set; }
        [Column("ssSCMembersF", TypeName = "smallint")]
        [Display(Name = "SC Members F")]
        public short? ssSCMembersF { get; set; }
        [Column("ssSCMembersM", TypeName = "smallint")]
        [Display(Name = "SC Members M")]
        public short? ssSCMembersM { get; set; }
        [Column("ssShifts", TypeName = "int")]
        [Display(Name = "Shifts")]
        public int? ssShifts { get; set; }
        [Column("ssClassesShift1", TypeName = "int")]
        [Display(Name = "Classes Shift1")]
        public int? ssClassesShift1 { get; set; }
        [Column("ssClassesShift2", TypeName = "int")]
        [Display(Name = "Classes Shift2")]
        public int? ssClassesShift2 { get; set; }
        [Column("ssClassesShiftAll", TypeName = "int")]
        [Display(Name = "Classes Shift All")]
        public int? ssClassesShiftAll { get; set; }
        [Column("ssSuppliesCurriculum", TypeName = "smallint")]
        [Display(Name = "Supplies Curriculum")]
        public short? ssSuppliesCurriculum { get; set; }
        [Column("ssSuppliesInUse", TypeName = "smallint")]
        [Display(Name = "Supplies In Use")]
        [Description("ssSupplies.... fields added in Kiribati 2014 survey")]
        public short? ssSuppliesInUse { get; set; }
        [Column("ssSuppliesStudent", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Supplies Student")]
        public string ssSuppliesStudent { get; set; }
        [Column("ssSuppliesTeacher", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Supplies Teacher")]
        public string ssSuppliesTeacher { get; set; }
        [Column("ssNumBuildings", TypeName = "int")]
        [Display(Name = "Num Buildings")]
        public int? ssNumBuildings { get; set; }
        [Column("ssNumClassrooms", TypeName = "int")]
        [Display(Name = "Num Classrooms")]
        public int? ssNumClassrooms { get; set; }
        [Column("ssNumClassroomsPoor", TypeName = "int")]
        [Display(Name = "Num Classrooms Poor")]
        public int? ssNumClassroomsPoor { get; set; }
        [Column("ssNumTeachers", TypeName = "int")]
        [Display(Name = "Num Teachers")]
        [Description("Number of teachers at the school. this is aggregated fronm teacher records. This is distinct from ssTotalTeachers, which is captured on a survey form as a cross check")]
        public int? ssNumTeachers { get; set; }
        [Column("ssBudget", TypeName = "int")]
        [Display(Name = "Budget")]
        [Description("Total school budget")]
        public int? ssBudget { get; set; }
        [Column("ssStartDay", TypeName = "datetime")]
        [Display(Name = "Start Day")]
        [Description("Date that tuitioon begins at this school in the survey year ")]
        public DateTime? ssStartDay { get; set; }
        [Column("ssSource", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Source")]
        [Description("Source of the survey. May point to a file in the fileDB")]
        public string ssSource { get; set; }
    }
}