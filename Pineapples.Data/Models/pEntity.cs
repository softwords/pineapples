﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Softwords.Web.Models;
using Softwords.Web.Utilities;
using Softwords.Web.Json;

namespace Pineapples.Data.Models
{
    /// <summary>
    /// Base class for EF classes that represent object with these change tracking fields
    /// </summary>
    public class ChangeTracked
    {
        private DateTime? createdatetime;
        private DateTime? editdatetime;
        [MaxLength(100)]
        [ChangeTracking(ChangeTrackingOption.CreateUser)]
        public string pCreateUser { get; set; }

        [ChangeTracking(ChangeTrackingOption.CreateDateTime)]
        public DateTime? pCreateDateTime
        {
            get
            {
                return createdatetime;
            }
            set
            {
                createdatetime = value.ToKindUtc();
            }
        }

        [MaxLength(100)]
        [ChangeTracking(ChangeTrackingOption.EditUser)]
        public string pEditUser { get; set; }

        [ChangeTracking(ChangeTrackingOption.EditDateTime)]
        [JsonConverter(typeof(UtcDateTimeConverter))]
        public DateTime? pEditDateTime
        {
            get
            {
                return editdatetime;
            }
            set
            {
                editdatetime = value.ToKindUtc();
            }
        }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] pRowversion { get; set; }
    }

    /// <summary>
    ///  Changed tracked "virtual table" ( ie based on editable view) that uses a CreateTag guid to retrieve
    ///  a newly created record
    /// </summary>
    public class CreateTagged : ChangeTracked
    {
        [Key]
        [ChangeTracking(ChangeTrackingOption.CreateTag)]
        public Guid? pCreateTag { get; set; }

    }
}
