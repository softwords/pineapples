﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
	[Table("SpecialEdStudent_")]
	[Description("Each record in Student_ represents a student registered as Special Education.")]
	public partial class SpecialEdStudent : ChangeTracked
	{

		[Key]
		[ChangeTracking(ChangeTrackingOption.CreateTag)]
		[Display(Name = "ID")]
		public Guid? stuID { get; set; }

		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Card ID")]
		public string stuCardID { get; set; }

		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Name Prefix")]
		public string stuNamePrefix { get; set; }

		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Name")]
		public string stuGiven { get; set; }

		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Middle Names")]
		public string stuMiddleNames { get; set; }

		[MaxLength(50)]
		[StringLength(50)]
		[Display(Name = "Family Name")]
		public string stuFamilyName { get; set; }

		[MaxLength(20)]
		[StringLength(20)]
		[Display(Name = "Name Suffix")]
		public string stuNameSuffix { get; set; }

		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Given Name Soundex")]
		public string stuGivenSoundex { get; set; }

		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Family Soundex")]
		public string stuFamilySoundex { get; set; }

		[Display(Name = "Date of Birth")]
		public DateTime? stuDoB { get; set; }

		[Display(Name = "Date of Birth Estimate")]
		public int? stuDoBEst { get; set; }

		[MaxLength(1)]
		[Display(Name = "Gender")]
		public string stuGender { get; set; }

		[MaxLength(20)]
		[StringLength(200)]
		[Display(Name = "Ethnicity")]
		public string stuEthnicity { get; set; }

		[Display(Name = "Create File Reference")]
		public Guid? stuCreateFileRef { get; set; }

		[Display(Name = "Edit File Reference")]
		public Guid? stuEditFileRef { get; set; }

		[Display(Name = "Intake Date")]
		public DateTime? stuSpEdIntakeDate { get; set; }

		[StringLength(10)]
		[ClientLookup("disabilities")]
		public string stueSpEdDisability { get; set; }

		[StringLength(10)]
		[ClientLookup("spEdEnglishLearner")]
		public string stueSpEdEnglish { get; set; }

		[StringLength(10)]
		[ClientLookup("spEdEnvironment")]
		public string stueSpEdEnv { get; set; }

		[Column("stueSpEdIEP", TypeName = "int")]
		[Display(Name = "Has IEP")]
		public int? stueSpEdIEP { get; set; }

		[Column("stueSpEdHasAccommodation", TypeName = "int")]
		[Display(Name = "Has Accommodation")]
		public int? stueSpEdHasAccommodation { get; set; }

		[Column("stueSpEdAccommodation", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Sp Ed Accommodation")]
		public string stueSpEdAccommodation { get; set; }

		[Column("stueSpEdAssessment", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Sp Ed Assessment")]
		public string stueSpEdAssessment { get; set; }

		[Column("stueSpEdExit", TypeName = "nvarchar")]
		[MaxLength(10)]
		[StringLength(10)]
		[Display(Name = "Sp Ed Exit")]
		public string stueSpEdExit { get; set; }

		[Display(Name = "Exiting Date")]
		public DateTime? stueSpEdExitDate { get; set; }

		[Column("stueSpEdTeacher", TypeName = "int")]
		[Display(Name = "SpEd Student's Teacher")]
		public int? stueSpEdTeacher { get; set; }

	}

}
