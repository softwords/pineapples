﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;



namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.SchoolInspection>))]
    public class SchoolInspectionBinder : Softwords.Web.Models.Entity.ModelBinder<Models.SchoolInspection>
    {
        // allow nullable to handle new record
        public int? inspID
        {
            get
            {
                return (int?)getProp("inspID");
            }
            set
            {
                definedProps.Add("inspID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolInspection ti = null;

            IQueryable<Models.SchoolInspection> qry = from schoolinspection in cxt.SchoolInspections
                                        where schoolinspection.ID == inspID
                                        select schoolinspection;

            if (qry.Count() == 0)
            {
                throw RecordNotFoundException.Make(inspID);
            }

            ti = qry.First();
            if (Convert.ToBase64String(ti.pRowversion.ToArray()) != RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(ti);
                throw ConcurrencyException.Make(this.rowVersionProp, definedProps);
            }
            ToDb(cxt, identity, ti);
        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolInspection ti = null;

            if  (getProp("inspID") != null)
            {
                throw DuplicateKeyException.Make(keyProp, getProp("inspID"));
            }

            ti = new Models.SchoolInspection();
            cxt.SchoolInspections.Add(ti);
            ToDb(cxt, identity, ti);
        }
    }
}

