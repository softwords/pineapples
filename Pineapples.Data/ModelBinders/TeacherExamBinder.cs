﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
	[JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.TeacherExam>))]
	public class TeacherExamBinder : Softwords.Web.Models.Entity.ModelBinder<Models.TeacherExam>
	{
		public int texID
		{
			get
			{
				return (int)getProp("texID");
			}
			set
			{
				definedProps.Add("texID", value);
			}
		}

		public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
		{
			Models.TeacherExam e = null;

			IQueryable<Models.TeacherExam> qry = from teacherexam in cxt.TeacherExams
												 where teacherexam.texID == texID
												 select teacherexam;

			if (qry.Count() == 0)
			{
				throw RecordNotFoundException.Make(texID);
			}

			e = qry.First();
			// Is concurrency needed?
			//if (Convert.ToBase64String(e.pRowversion.ToArray()) != this.RowVersion)
			//{
			//    // optimistic concurrency error
			//    // we return the most recent version of the record
			//    FromDb(e);
			//    throw new ConcurrencyException(this.definedProps);
			//}
			ToDb(cxt, identity, e);
		}

		public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
		{
			Models.TeacherExam e = null;

			IQueryable<Models.TeacherExam> qry = from teacherexam in cxt.TeacherExams
												 where teacherexam.texID == texID
												 select teacherexam;

			if (qry.Count() != 0)
			{
				throw DuplicateKeyException.Make(keyProp, texID);
			}

			e = new Models.TeacherExam();
			cxt.TeacherExams.Add(e);
			ToDb(cxt, identity, e);
		}
	}
}
