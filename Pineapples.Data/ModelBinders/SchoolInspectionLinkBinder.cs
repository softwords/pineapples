﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.SchoolInspectionLink>))]
    public class SchoolInspectionLinkBinder : Softwords.Web.Models.Entity.ModelBinder<Models.SchoolInspectionLink>
    {
        public int? ID
        {
            get
            {
                return (int?)getProp("lnkID");
            }
            set
            {
                definedProps.Add("lnkID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolInspectionLink e = null;

            IQueryable<Models.SchoolInspectionLink> qry = from schoolinspectionlink in cxt.SchoolInspectionLinks
                                                 where schoolinspectionlink.lnkID == ID
                                        select schoolinspectionlink;

            if (qry.Count() == 0)
            {
                throw RecordNotFoundException.Make(ID);
            }

            e = qry.First();
            if (Convert.ToBase64String(e.pRowversion.ToArray()) != this.RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(e);
                throw ConcurrencyException.Make(this.rowVersionProp, this.definedProps);
            }
            ToDb(cxt, identity, e);
        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolInspectionLink e = null;

            IQueryable<Models.SchoolInspectionLink> qry = from schoolinspectionlink in cxt.SchoolInspectionLinks
                                      where schoolinspectionlink.lnkID == ID
                                      select schoolinspectionlink;

            if (qry.Count() != 0)
            {
                throw DuplicateKeyException.Make(keyProp, ID);
            }

            e = new Models.SchoolInspectionLink();
            cxt.SchoolInspectionLinks.Add(e);
            ToDb(cxt, identity, e);
        }
    }
}
