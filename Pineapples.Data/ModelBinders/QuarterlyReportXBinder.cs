﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.QuarterlyReportX>))]
    public class QuarterlyReportXBinder : Softwords.Web.Models.Entity.ModelBinder<Models.QuarterlyReportX>
    {
        public int? qrID
        {
            get
            {
                return (int?)getProp("qrID");
            }
            set
            {
                definedProps.Add("qrID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.QuarterlyReportX qr = null;

            IQueryable<Models.QuarterlyReportX> qry = from quarterlyreport in cxt.QuarterlyReportsX
                                                         where quarterlyreport.qrID == qrID
                                                         select quarterlyreport;

            if (qry.Count() == 0)
            {
                throw RecordNotFoundException.Make(qrID);
            }

            qr = qry.First();
            if (Convert.ToBase64String(qr.pRowversion.ToArray()) != this.RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(qr);
                throw ConcurrencyException.Make(this.rowVersionProp, this.definedProps);
            }
            ToDb(cxt, identity, qr);

        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.QuarterlyReportX qr = null;

            IQueryable<Models.QuarterlyReportX> qry = from quarterlyreport in cxt.QuarterlyReportsX
                                      where quarterlyreport.qrID == qrID
                                      select quarterlyreport;

            if (qry.Count() != 0)
            {
                throw DuplicateKeyException.Make(this.keyProp, qrID);
            }

            qr = new Models.QuarterlyReportX();
            cxt.QuarterlyReportsX.Add(qr);
            ToDb(cxt, identity, qr);
        }
    }
}
