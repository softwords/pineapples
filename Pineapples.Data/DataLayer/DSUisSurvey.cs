﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;


namespace Pineapples.Data.DataLayer
{
    internal class DSUisSurvey : BaseRepository, IDSUisSurvey
    {
        public DSUisSurvey(DB.PineapplesEfContext cxt) : base(cxt)
        {

        }
        //

        public IDataResult AllSheets(int year)
        {
            SqlCommand cmd = AllCmd(year);
            return this.structuredResult(cmd, "uissurvey", 
                new string[] { "A1", "A2" ,"A3","A5","A6","A9","A10","A11","A12","A13"});
        }

        public IDataResult A1(int year)
        {
            SqlCommand cmd = A1Cmd(year);
            return sqlExec(cmd);
        }
        
        public IDataResult A2(int year)
        {
            SqlCommand cmd = A2Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A3(int year)
        {
            SqlCommand cmd = A3Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A5(int year)
        {
            SqlCommand cmd = A5Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A6(int year)
        {
            SqlCommand cmd = A6Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A9(int year)
        {
            SqlCommand cmd = A9Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A10(int year)
        {
            SqlCommand cmd = A10Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A11(int year)
        {
            SqlCommand cmd = A11Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A12(int year)
        {
            SqlCommand cmd = A12Cmd(year);
            return sqlExec(cmd);
        }
        public IDataResult A13(int year)
        {
            SqlCommand cmd = A13Cmd(year);
            return sqlExec(cmd);
        }

        public IDataResult TeacherAudit(int year)
        {
            SqlCommand cmd = TeacherAuditCmd(year);
            return this.structuredResult(cmd, "uissurvey",
                new string[] { "Summary", "ISCED0","ISCED1", "ISCED2", "ISCED3", "SECONDARY4", "SECONDARY5" });

        }
        public IDataResult EnrolAudit(int year)
        {
            SqlCommand cmd = EnrolAuditCmd(year);
            return sqlExec(cmd);
        }
        public IDataResult SchoolAudit(int year)
        {
            SqlCommand cmd = SchoolAuditCmd(year);
            return sqlExec(cmd);
        }
        #region commands

        private SqlCommand AllCmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText =
$@"
{BaseText}
exec warehouse.uisA1 @year
exec warehouse.uisA2 @year
exec warehouse.uisA3 @year
exec warehouse.uisA5 @year
exec warehouse.uisA6 @year
exec warehouse.uisA9 @year
exec warehouse.uisA9 @year  -- for A10  

exec warehouse.uisA9 @year  -- for A11 not used
exec warehouse.uisA12 @year
exec warehouse.uisA13 @year   
";

            return cmd;
        }

        private SqlCommand A1Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA1";

            return cmd;
        }
        private SqlCommand A2Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA2";

            return cmd;
        }
        private SqlCommand A3Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA3";
            return cmd;
        }

        private SqlCommand A5Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA5";
            return cmd;
        }

        private SqlCommand A6Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA6";
            return cmd;
        }

        private SqlCommand A9Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA9";
            return cmd;
        }

        private SqlCommand A10Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA10";
            return cmd;
        }
        private SqlCommand A11Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA11";
            return cmd;
        }
        private SqlCommand A12Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA12";
            return cmd;
        }
        private SqlCommand A13Cmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "warehouse.uisA13";
            return cmd;
        }

        private SqlCommand TeacherAuditCmd(int year)
		{
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = 
$@"
{BaseText}
Select * from warehouse.TeacherIscedAuditSummary(@year)
Select * from warehouse.TeacherIscedAudit('ISCED 0', @year)
Select * from warehouse.TeacherIscedAudit('ISCED 1', @year)
Select * from warehouse.TeacherIscedAudit('ISCED 2', @year)
Select * from warehouse.TeacherIscedAudit('ISCED 3', @year)
Select * from warehouse.TeacherIscedAudit('SECONDARY4', @year)
Select * from warehouse.TeacherIscedAudit('SECONDARY5', @year)
";
            return cmd;
        }

        private SqlCommand EnrolAuditCmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText =
                $@"Select * from warehouse.EnrolIscedAudit(@year)";
            return cmd;
        }


        private SqlCommand SchoolAuditCmd(int year)
        {
            SqlCommand cmd = GetCmd(year);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText =
                $@"Select * from warehouse.SchoolIscedAudit(@year)";
            return cmd;
        }
        private string BaseText = 
@"Select *
FROM Survey
WHERE svyYEar = @year
";

        private SqlCommand GetCmd(int year)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@year", SqlDbType.Int).Value = year;
            return cmd;
            #endregion
        }
    }
}
