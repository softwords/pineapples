﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


using Pineapples.Data.DB;
using Softwords.DataTools;
using Pineapples.Data.Models;

namespace Pineapples.Data.DataLayer
{
	internal class DSLookups : BaseRepository, IDSLookups
	{
		Dictionary<string, string> lkpCommands;

		public DSLookups(DB.PineapplesEfContext cxt) : base(cxt)
		{
			lkpCommands = new Dictionary<string, string>();
			lkpCommands.Add("schoolTypes", "SELECT stCode C, stDescription N from TRSchooltypes;");
			lkpCommands.Add("schoolCodes", "SELECT schNo C, schName N FROM Schools;");
			lkpCommands.Add("districts", "SELECT dID C, [dName] N from Districts ORDER BY dName;");
			lkpCommands.Add("distanceCodes", "SELECT [codeNum] C, [codeDescription] N from [lkpDistanceCodes] ORDER BY [codeNum];");
			lkpCommands.Add("authorities", "SELECT authCode C, authName N, authType T from TRAuthorities;");
			lkpCommands.Add("authorityTypes",
							"SELECT [codeDescription] N, [codeCode] C, codeGroup G from TRAuthorityType;");
			lkpCommands.Add("authorityGovts",
							"SELECT [codeDescription] N, [codeCode] C from TRAuthorityGovt;");
			lkpCommands.Add("islands",
							"SELECT [iName] N, [iCode] C, [iGroup] D, [iOuter] R FROM lkpIslands ORDER BY [iName];");
			lkpCommands.Add("regions", "SELECT [codeCode] C, [codedescription] N FROM lkpRegion ORDER BY [codedescription];");
			lkpCommands.Add("educationLevels",
							@"Select codeCode C, codeDescription N, edlMinYear MinY, edlMaxYear MaxY
                                from TREducationLevels ORDER By edlMinYear");
			lkpCommands.Add("educationLevelsAlt",
							@"Select codeCode C, codeDescription N, edlMinYear MinY, edlMaxYear MaxY
                                from TREducationLevelsAlt ORDER By edlMinYear");
			lkpCommands.Add("educationLevelsAlt2",
							@"Select codeCode C, codeDescription N, edlMinYear MinY, edlMaxYear MaxY
                                from TREducationLevelsAlt2 ORDER By edlMinYear");
			lkpCommands.Add("educationPaths",
							"SELECT [pathCode] C, [pathName] N from EducationPaths;");
			lkpCommands.Add("educationPathLevels",
							"SELECT [plID] C, [plID] N from EducationPathLevels;");
			lkpCommands.Add("educationPrograms",
							"SELECT [pgID] C, [pgName] N from EducationPrograms;");
			lkpCommands.Add("educationSectors",
						   @"Select secCode C, secDesc N 
                                from TREducationSectors ORDER By secSort");
			lkpCommands.Add("electoraten",
							"SELECT [codeDescription] N, [codeCode] C FROM lkpElectorateN ORDER BY [codeDescription];");
			lkpCommands.Add("electoratel",
							"SELECT [codeDescription] N, [codeCode] C FROM lkpElectorateL ORDER BY [codeDescription];");
			lkpCommands.Add("ethnicityGroups",
							"SELECT [codeDescription] N, [codeCode] C FROM lkpEthnicityGroup ORDER BY [codeSeq];");
			lkpCommands.Add("examTypes",
							"SELECT exCode C, exName N, exNameShort S FROM lkpExamTypes ORDER BY exName");
			lkpCommands.Add("iscedFields", "SELECT ifID C, ifName N from ISCEDField ORDER BY ifName;");
			lkpCommands.Add("iscedFieldGroups", "SELECT ifgID C, ifgName N from ISCEDFieldGroup ORDER BY ifgName;");
			lkpCommands.Add("iscedFieldGroupsBroad", "SELECT ifgbID C, ifgbName N from ISCEDFieldGroupBroad ORDER BY ifgbName;");
			lkpCommands.Add("iscedLevels", "SELECT ilCode C, ilName N from ISCEDLevel ORDER BY ilName;");
			lkpCommands.Add("iscedLevelsSub", "SELECT ilsCode C, ilsName N from ISCEDLevelSub ORDER BY ilsName;");
			lkpCommands.Add("languages",
							"SELECT [langCode] C, [langName] N  FROM TRLanguage ORDER BY [langName];");
			lkpCommands.Add("listNames",
							"SELECT [lstDescription] N, [lstName] C FROM Lists ORDER BY [lstName];");
			lkpCommands.Add("schoolClasses",
							"SELECT [codeCode] C, [codedescription] N FROM TRSchoolClass ORDER BY [codedescription];");
			lkpCommands.Add("schoolNames",
							"SELECT [schName] N, [schNo] C FROM Schools ORDER BY [schName];");
			lkpCommands.Add("teacherNames",
							"SELECT [tFullName] N, [tID] C FROM TeacherIdentity ORDER BY [tFullName];");
			// pa lookups
			lkpCommands.Add("paFrameworks",
							"SELECT [pafrmDescription] N, [pafrmCode] C FROM paFrameworks_ ORDER BY [pafrmDescription];");
			lkpCommands.Add("paCompetencies",
							"SELECT [pacomDescription] N, [comFullID] C, paComID ID FROM paCompetencies ORDER BY [comFullID];");
			lkpCommands.Add("paElements",
							"SELECT [paelmDescription] N, [elmFullID] C, paelmID ID FROM paElements ORDER BY [elmFullID];");
			lkpCommands.Add("paIndicators",
							"SELECT [paindText] N, [indFullID] C, paindID ID " +
							 " FROM paIndicators ORDER BY[indFullID]");
			lkpCommands.Add("paFields",
							"SELECT C FROM paFields ORDER BY seq");

			lkpCommands.Add("examFieldOptions",
							"SELECT f C, g from common.examFieldOptions ORDER BY seq;");
			lkpCommands.Add("examDataOptions",
							"SELECT C, N from common.examDataOptions ORDER BY seq;");
			lkpCommands.Add("teacherExamFieldOptions",
							"SELECT f C, g from common.teacherExamFieldOptions ORDER BY seq;");
			lkpCommands.Add("teacherExamDataOptions",
							"SELECT C, N from common.teacherExamDataOptions ORDER BY seq;");
			lkpCommands.Add("schoolFieldOptions",
							"SELECT f C, g from common.schoolFieldOptions ORDER BY seq;");
			lkpCommands.Add("schoolDataOptions",
							"SELECT C, N from common.schoolDataOptions ORDER BY seq;");
			lkpCommands.Add("teacherFieldOptions",
							"SELECT f C, g from common.teacherFieldOptions ORDER BY seq;");
			lkpCommands.Add("teacherDataOptions",
							"SELECT C, N from common.teacherDataOptions ORDER BY seq;");
			lkpCommands.Add("studentFieldOptions",
							"SELECT f C, g from common.studentFieldOptions ORDER BY seq;");
			lkpCommands.Add("studentDataOptions",
							"SELECT C, N from common.studentDataOptions ORDER BY seq;");
			lkpCommands.Add("appDataOptions",
							"SELECT C, N from common.appDataOptions ORDER BY seq;");

			lkpCommands.Add("vocab",
							"SELECT  vocabName C, isnull(vocabTerm, vocabName) N FROM sysVocab");
			lkpCommands.Add("sysParams",
							"SELECT paramName C, paramText N, paramInt I from sysParams");
			lkpCommands.Add("schoolRegistration",
							"SELECT rxCode C, rxDescription N FROM TRSchoolRegistration");

			lkpCommands.Add("surveyYears",
							"SELECT svyYear C, svyYear N FROM Survey ORDER BY svyYear DESC");
			// years for which there is data in the warehouse -- this distinction may be important when a new year
			// is established to create workbooks, but there is a lag before data exists in that new year
			lkpCommands.Add("warehouseYears",
							@"Select Y C, Y N
							, common.schoolYearFormat(Y) FormattedYear
							FROM
							(
							SELECT DISTINCT [Survey Data Year] Y
							FROM warehouse.DimensionSchoolSurvey
							) SUB
							ORDER BY Y DESC");

			// WHERE clause used to only retrieve table lookups that already work; in future either add to where clause of remove when they're all supported
			lkpCommands.Add("tableDefs",
							"SELECT tdefCode C, tdefName N FROM metaPupilTableDefs WHERE tdefCode = 'ENROL_PRI' OR tdefCode = 'ENROL_SEC' OR tdefCode = 'REP' OR tdefCode = 'DIS' OR tdefCode = 'TRIN' OR tdefCode = 'TROUT' UNION SELECT 'ENROL', 'Enrolments';");
			lkpCommands.Add("subjects",
							"SELECT subjCode C, subjName N FROM TRSubjects ORDER BY subjName");
			lkpCommands.Add("publishers",
							"SELECT pubCode C, pubName N FROM Publishers ORDER BY pubName");
			lkpCommands.Add("bookTG",
							"SELECT codeCode C, codeDescription N FROM TRBookTG ORDER BY codeDescription");
			lkpCommands.Add("levels",
							@"Select levelCode C, Level N, [Year of Education] YoEd
                                , edLevelCode L, sectorCode S 
                                from DimensionLevel ORDER BY[Year of Education]");

			lkpCommands.Add("gender",
							"SELECT codeCode C, codeDescription N FROM TRGender ORDER BY codeDescription");
			lkpCommands.Add("teacherRegStatus",
							"SELECT statusCode C, statusDesc N FROM lkpTeacherStatus ORDER BY statusSort");
			// documents and links
			lkpCommands.Add("teacherLinkTypes",
							"SELECT codeCode C, codeDescription N FROM TRTeacherLinkTypes ORDER BY codeSort");
			lkpCommands.Add("schoolLinkTypes",
							"SELECT codeCode C, codeDescription N FROM TRSchoolLinkTypes ORDER BY codeSort");
			lkpCommands.Add("studentLinkTypes",
							"SELECT codeCode C, codeDescription N FROM TRStudentLinkTypes ORDER BY codeSort");

			lkpCommands.Add("maritalStatus", // 
				@"SELECT codeCode C, codeDescription N FROM lkpCivilStatus ORDER BY codeSort");

			// teacher related
			lkpCommands.Add("teacherQuals",
							"SELECT codeCode C, codeDescription N, codeGroup G, codeTeach E FROM TRTeacherQual ORDER BY codeSeq");
			lkpCommands.Add("teacherRoles",
							"SELECT codeCode C, codeDescription N FROM TRTeacherRole ORDER BY codeSort, codeDescription");
			lkpCommands.Add("schoolTypeRoles",
							$@"SELECT L.codeCode C, codeDescription N, stCode T 
							From lkpTeacherRole L INNER JOIN metaSchoolTypeTeacherRoleMap M ON L.codeCode = M.codeCode
							AND M.typroleSort <= 30 ORDER BY typRoleSort"
							);
			lkpCommands.Add("teacherQualGroups",
							$@"SELECT codeCode C, codeDescription N, codeGroup G, codeTeach E 
							FROM TRTeacherQual 
							WHERE codeGroup = codeCode
							ORDER BY codeSeq"
							);
			lkpCommands.Add("teacherStatus",
							"SELECT statusCode C, statusDesc N FROM TRTeacherStatus ORDER BY statusDesc");
			lkpCommands.Add("teacherExamTypes",
							"SELECT texCode C, texName N, texNameShort S FROM lkpTeacherExamTypes ORDER BY texName");

			// metadata
			// school type level map
			lkpCommands.Add("schoolTypeLevels",
							@"Select stCode ST, tlmLevel L, lvlYear YoEd 
							from metaSchoolTypeLevelMap M
							INNER JOIN lkpLevels L
							ON M.tlmLEvel = L.codeCode 
							ORDER BY stCode, tlmLevel");
			lkpCommands.Add("accreditationTerms",
							"Select * from dbo.lkpInspectionTypeTerms('SCHOOL_ACCREDITATION') ORDER BY C");
			lkpCommands.Add("inspectionTypes",
							"Select intyCode C, intyDesc N from lkpInspectionTypes ORDER BY intyDesc");
			// resources
			lkpCommands.Add("metaResourceCategories",
							@"SELECT mresCatCode C, mresCatName N
							FROM [dbo].[metaResourceCategories]
							ORDER BY mresCatName");
			lkpCommands.Add("metaResourceDefinitions",
							@"SELECT [mresCat] Cat
								,[mresName] C
								,[mresName] N
							    ,[mresPromptAdq] PromptAdq
								,[mresPromptAvail] PromptAvail
								,[mresPromptNum] PromptNum
								,[mresPromptCondition] PromptCondition
								,[mresPromptQty] PromptQty
								,[mresBuildingType] BuildingType
								,[mresBuildingSubType] BuildingSubType
								,[mresPromptNote] PromptNote
								,[mresSurveyed] Surveyed
							FROM [dbo].[metaResourceDefs]
							ORDER BY [mresSeq]");

			// budgets and expenditure
			lkpCommands.Add("expenditurePoints",
							"Select C, N from lkpExpenditurePoints ORDER BY S,C");
			lkpCommands.Add("costCentres",
							"Select ccCode C, ccDescription N, ccProRate P, ccSector S FROM CostCentres ORDER BY ccCode");
			lkpCommands.Add("fundingSources",
							"Select sofCode C, sofDesc N, sfgCode G FROM SourceOfFunds ORDER BY sofCode");
			lkpCommands.Add("fundingSourceGroups",
							"Select sfgCode C, sfgDesc N FROM SourceOfFundsGroup ORDER BY sfgCode");

			// students and special ed
			lkpCommands.Add("ethnicities", // now derived on the workbook from this code table
							@"SELECT codeCode C, codeDescription N from lkpEthnicity ORDER BY codeDescription");
			lkpCommands.Add("nationalities", // 
							@"SELECT codeCode C, codeDescription N from lkpNationality WHERE codeSeq < 999 ORDER BY codeSeq, codeDescription");
			lkpCommands.Add("spEdEnvironment_ECE", // 
							@"SELECT codeCode C, codeDescription N from lkpSpEdEnvironmentECE
								ORDER BY codeSeq");
			lkpCommands.Add("spEdEnvironment", // 
							@"SELECT codeCode C, codeDescription + ' (' + codegroup + ')' N from lkpSpEdEnvironment
								ORDER BY codeGroup, codeSeq");
			lkpCommands.Add("spEdEnvironment_SA", // 
							@"SELECT codeCode C, codeDescription N from lkpSpEdEnvironmentSA
								ORDER BY codeSeq");
			lkpCommands.Add("disabilities", // 
							@"SELECT codeCode C, codeDescription N from lkpDisabilities ORDER BY codeSeq");
			lkpCommands.Add("spEdEnglishLearner", // 
							@"SELECT codeCode C, codeDescription N from lkpEnglishLearner ORDER BY codeSeq");
			lkpCommands.Add("spEdAccommodations", // 
							@"SELECT codeCode C, codeDescription N from lkpSpEdAccommodations ORDER BY codeSeq");
			lkpCommands.Add("spEdAssessmentTypes", // 
							@"SELECT codeCode C, codeDescription N from lkpSpEdAssessmentTypes ORDER BY codeSeq");
			lkpCommands.Add("spEdExits", // 
							@"SELECT codeCode C, codeDescription N from lkpSpEdExits ORDER BY codeSeq");
			lkpCommands.Add("spEdEvaluationEventTypes",
							"SELECT codeCode C, codeDescription N, codeGroup G FROM lkpSpEdEvaluationEventTypes ORDER BY codeSeq");

			// scholarships
			lkpCommands.Add("tertiaryGrades", // 
							@"SELECT gyIndex C, gyIndex ID, gyName N from lkpTertiaryGrades ORDER BY gyIndex");
			lkpCommands.Add("fieldsOfStudy", // 
							@"SELECT fosCode C, fosName N, fosgrpCode G FROM FieldOfStudy ORDER BY fosCode");
			lkpCommands.Add("fieldOfStudyGroups", // 
							@"SELECT codeCode C, codeDescription N FROM FieldOfStudyGroups ORDER BY codeCode");
			lkpCommands.Add("scholarshipTypes", // 
				@"SELECT codeCode C, codeDescription N FROM ScholarshipTypes ORDER BY codeCode");
			lkpCommands.Add("scholarshipStatus", // 
				@"SELECT codeCode C, codeDescription N FROM lkpScholarshipStatus ORDER BY codeSeq");
			lkpCommands.Add("scholarshipInstitutions", // 
				@"SELECT instCode C, instName N, instCountry COU FROM ScholarshipInstitutions ORDER BY instCode");
			lkpCommands.Add("scholarshipLinkTypes",
							"SELECT codeCode C, codeDescription N FROM TRScholarshipLinkTypes ORDER BY codeSort");

			// buildings / building inspection
			lkpCommands.Add("buildingTypes",
							"select bdlgCode C, bdlgDescription N from lkpBuildingTypes ORDER BY bdlgDescription");
			lkpCommands.Add("buildingSubTypes",
							"select bsubCode C, bsubDescription N, bdlgCode T from lkpBuildingSubType ORDER BY bdlgCode, bsubCode");

			// for census workbook: many of the above can be reused but some are customised
			lkpCommands.Add("powerSupplyTypes",
							@"Select mresName
									from metaResourceDefs
									WHERE mresCat = 'Power Supply'
									ORDER BY mresSeq");
			lkpCommands.Add("handWashing",
							@"Select mresName
									from metaResourceDefs
									WHERE mresCat = 'Handwashing'
									ORDER BY mresSeq");
			lkpCommands.Add("toiletTypes",
							@"Select 
								ttypName ToiletType
								FROM lkpToiletTypes
								ORDER BY ttypSort");
			lkpCommands.Add("waterSupplyTypes",
							@"Select mresName
								from metaResourceDefs
								WHERE mresCat = 'Water Supply'
								ORDER BY mresSeq");

			lkpCommands.Add("qualQuals",
							@"SELECT codeDescription QualName
									  , codeCode QualCode
									  from lkpTeacherQual
									  where codeTeach = 0
									  order by codeSeq");
			lkpCommands.Add("edQuals",
							@"SELECT codeDescription QualName
									  , codeCode QualCode
									  from lkpTeacherQual
									  where codeTeach = 1
									  order by codeSeq");
			// level codes variants for the census Lists page
			lkpCommands.Add("censusLevels",
							@"Select Level levelName, [Year of Education] YearOfEd
								, levelCode
                                from DimensionLevel ORDER BY[Year of Education]");
			lkpCommands.Add("censusYears",
							@"Select
								svyYear
								, common.SurveyYearFormat(svyYear) FormattedYear
								, convert(nvarchar(4), svyYear - 1) + '-' + convert(nvarchar(4), svyYear) FromToYear
								, svyPSAge
								, svyCensusDate

								FROM Survey
								ORDER BY svyYear DESC");
			// transfers includes 'International' as an option
			lkpCommands.Add("censusTransferSchools",
							@"Select schNo
								, schName
								FROM Schools S
								UNION ALL
								Select '' 
								, 'International'"
								);
			// master list of schools for census - may include 'xxxDOE'
			lkpCommands.Add("censusSchools",
							@"Select schName
								, schLandOwner
								, ST.stDescription SchoolType
								, A.Authority
								, A.AuthorityGroup
								, D.dName State
								, schNo
								FROM Schools S
								LEFT JOIN Islands I
									ON S.iCode = I.iCode
								LEFT JOIN Districts D
									ON I.iGroup = D.dID
								LEFT JOIN SchoolTypes ST
									ON S.schType = ST.stCode
								LEFT JOIN DimensionAuthority A
									ON S.schAuth = A.AuthorityCode
								UNION ALL
								Select dName + ' State Department of Education'
								, null
								, 'DOE'
								, dShort
								, null
								, D.dName State
								, dShort + 'DOE' 
								FROM Districts D
								LEFT JOIN sysParams P
								ON P.paramName = 'CENSUS_IGNORE_DOE'
								WHERE P.paramInt is null or P.paramInt = 0
								ORDER BY schName
								");
			lkpCommands.Add("organizations",
									"Select employerCode C, employerName N from lkpOrganizations ORDER BY employerSeq");

			// subset of teacher roles for the census, derived from RoleGrade
			// rgSort provides a simplem means to further filter this list
			lkpCommands.Add("censusTeacherRoles",
							@"select rgCode C, rgDescription N from RoleGrades
								WHERE rgSort < 1000		
								ORDER BY rgDescription
								");
		}
		public DataSet CoreLookups()
		{
			string[] lookups = new string[]
						{
							"schoolTypes",
							"schoolCodes",
							"districts",
							"distanceCodes",
							"authorities",
							"authorityTypes",
							"authorityGovts",
							"islands",
							"regions",
							"educationLevels",
							"educationLevelsAlt",
							"educationLevelsAlt2",
							"educationPaths",
							"educationPathLevels",
							"educationPrograms",
							"educationSectors",
							"electoraten",
							"electoratel",
							"examTypes",
							"iscedFields",
							"iscedFieldGroups",
							"iscedFieldGroupsBroad",
							"iscedLevelsSub",
							"iscedLevels",
							"languages",
							"listNames",
							"schoolClasses",
							"schoolNames",
							"teacherNames",
							"vocab",
							"sysParams",
							"paFields",
							"paFrameworks",
							"examFieldOptions",
							"examDataOptions",
							"teacherExamFieldOptions",
							"teacherExamDataOptions",
							"schoolFieldOptions",
							"schoolDataOptions",
							"teacherFieldOptions",
							"teacherDataOptions",
							"studentFieldOptions",
							"studentDataOptions",
							"schoolRegistration",
							"surveyYears",
							"warehouseYears",
							"tableDefs",
							"subjects",
							"publishers",
							"bookTG",
							"levels",
							"gender",
							"teacherRegStatus",
							"teacherLinkTypes",
							"teacherExamTypes",
							"maritalStatus",
							"schoolLinkTypes",
							"studentLinkTypes",
							"teacherQuals",
							"teacherRoles",
							"teacherStatus",
							"schoolTypeLevels",
							"accreditationTerms",
							"inspectionTypes",
							"expenditurePoints",

							"buildingTypes",
							"buildingSubTypes",

							"spEdEvaluationEventTypes",

							"ethnicities",
							"ethnicityGroups",
							"nationalities",
							"metaResourceCategories",
							"metaResourceDefinitions"

						};
			return getLookupSets(lookups);
		}

		public DataSet PaLookups()
		{
			string[] lookups = new string[]
						 {
							"paCompetencies",
							"paElements",
							"paIndicators"

						 };
			return getLookupSets(lookups);
		}

		public DataSet CensusWorkbookLookups()
		{
			string[] lookups = new string[]
						 {
							"schoolTypes",
							"schoolCodes",
							"authorities",
							"authorityTypes",
							"authorityGovts",
							"islands",
							"regions",
							"educationLevels",
							"educationLevelsAlt",
							"educationLevelsAlt2",
							"educationSectors",
							"electoraten",
							"electoratel",
							"examTypes",
							"listNames",
							"schoolClasses",
							"schoolNames",
							"distanceCodes",

							"sysParams",
							"paFields",
							"paFrameworks",
							"examFieldOptions",
							"examDataOptions",
							"teacherExamFieldOptions",
							"teacherExamDataOptions",
							"schoolFieldOptions",
							"schoolDataOptions",
							"teacherFieldOptions",
							"teacherDataOptions",
							"studentFieldOptions",
							"studentDataOptions",
							"schoolRegistration",
							"surveyYears",
							"tableDefs",
							"subjects",
							"publishers",
							"bookTG",
							"teacherRegStatus",
							"teacherLinkTypes",
							"teacherExamTypes",
							"teacherStatus",
							"censusTeacherRoles", // same as RoleGrades
							"organizations",
							"maritalStatus",
							"schoolLinkTypes",


							"gender",
							"nationalities",
							"ethnicities",
							"languages",
							"censusLevels",
							"censusYears",
							"qualQuals", // teacher non-teaching qualifications
							"edQuals",		// teacher teaching qualifications
							"fundingSources",
							"districts",

							"powerSupplyTypes",
							"waterSupplyTypes",
							"toiletTypes",
							"handWashing",

							"spEdEnvironment_ECE",
							"spEdEnvironment_SA",
							"spEdEnvironment",
							"spEdEnglishLearner",
							"spEdAccommodations",
							"spEdAssessmentTypes",
							"spEdExits",
							"disabilities",
							"vocab",
							"censusSchools",
							"censusTransferSchools"
						 };
			return getLookupSets(lookups);
		}

		public DataSet CensusPDFLookups()
		{
			string[] lookups = new string[]
						 {
							// Add a needed those not found in core lookups
							"powerSupplyTypes",
							"waterSupplyTypes",
							"toiletTypes",
							"handWashing",
							"metaResourceCategories",
							"metaResourceDefinitions",
							"schoolTypeRoles",
							"teacherQualGroups"
						 };
			return getLookupSets(lookups);
		}
		public DataSet StudentLookups()
		{
			string[] lookups = new string[]
						 {
							"spEdEnvironment_ECE",
							"spEdEnvironment_SA",
							"spEdEnvironment",
							"spEdEnglishLearner",
							"spEdAccommodations",
							"spEdAssessmentTypes",
							"spEdEvaluationEventTypes",
							"spEdExits",

							"disabilities"

						 };
			return getLookupSets(lookups);
		}

		public DataSet FinDataLookups()
		{
			string[] lookups = new string[]
						 { "costCentres",
							"fundingSources",
							"fundingSourceGroups"
						 };
			return getLookupSets(lookups);
		}

		public DataSet ScholarshipLookups()
		{
			string[] lookups = new string[]
						 {
							"tertiaryGrades",
							"fieldsOfStudy",
							"fieldOfStudyGroups",
							"scholarshipTypes",
							"scholarshipStatus",
							"scholarshipInstitutions",
							"scholarshipLinkTypes"
						 };
			return getLookupSets(lookups);
		}

		public DataSet getLookupSets(string[] lookups)
		{
			string lkpSql = string.Empty;
			string cmdSql = string.Empty;
			StringBuilder sb = new StringBuilder(String.Empty);

			foreach (string lkpName in lookups)
			{
				if (lkpCommands.TryGetValue(lkpName, out lkpSql))
				{
					sb.AppendLine(lkpSql);
				}
				else
				{
					throw new Exception(String.Format(@"Lookup table {0} is not defined", lkpName));
				}
			}

			// all were identified
			SqlCommand cmd = new SqlCommand();

			cmd.CommandType = CommandType.Text;
			// !!!!!!
			// IMPORTANT - this method should only be used when sql is a LITERAL
			cmd.CommandText = sb.ToString();
			DataSet ds = cmdToDataset(cmd);
			for (int i = 0; i < lookups.Length; i++)
			{
				ds.Tables[i].TableName = lookups[i];
			}
			return ds;
		}


		private IDataResult getLookups(string sql)
		{
			SqlCommand cmd = new SqlCommand();

			cmd.CommandType = CommandType.Text;
			// !!!!!!
			// IMPORTANT - this method should only be used when sql is a LITERAL
			cmd.CommandText = sql;
			return sqlExec(cmd, false);

		}

		public void SaveEntry(string lookupName, Models.LookupEntry entry)
		{

		}
	}
}
