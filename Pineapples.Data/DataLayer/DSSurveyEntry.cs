﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Xml.Linq;


namespace Pineapples.Data.DataLayer
{
    internal class DSSurveyEntry : BaseRepository, IDSSurveyEntry
    {
        public DSSurveyEntry(DB.PineapplesEfContext cxt) : base(cxt)
        {

        }
        //

        #region Survey methods

        public IDataResult PupilTable(string schoolNo, int year, string tableName)
        {
            SqlCommand cmd = pupilTableCmd(schoolNo, year, tableName);
            return sqlExec(cmd, false, "pupiltable");
        }
        public IDataResult SavePupilTable(PupilTable pupilTable, string user)
        {
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(PupilTable));
            var mem = new System.IO.MemoryStream();
            xs.Serialize(mem, pupilTable);
            var xml = System.Xml.Linq.XDocument.Parse(Encoding.ASCII.GetString(mem.ToArray()));
            SqlCommand cmd = savePupilTableCmd(pupilTable.SchoolNo,
                pupilTable.Year, pupilTable.TableName, xml, user);
            return sqlExec(cmd, false, "pupiltable");
        }

        public object ResourceList(string schoolNo, int year, string category)
        {
            SqlCommand cmd = resourceListCmd(schoolNo, year, category, null, null);
            string[] relatedTableNames = new string[] { "resourceCategory", "rows", "data" };
            StructureOptions[] options = new StructureOptions[] { StructureOptions.One, StructureOptions.Many, StructureOptions.Many };
            return structuredResult(cmd, "resourcelist", relatedTableNames, options);
        }
        public IDataResult SaveResourceList(ResourceList resourceList, string user)
        {
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(ResourceList));
            var mem = new System.IO.MemoryStream();
            xs.Serialize(mem, resourceList);
            var xml = System.Xml.Linq.XDocument.Parse(Encoding.ASCII.GetString(mem.ToArray()));
            SqlCommand cmd = saveResourceListCmd(resourceList.SchoolNo,
                resourceList.Year, resourceList.Category, resourceList.Split, resourceList.SchoolNo,
                xml, user);
            string[] relatedTableNames = new string[] { "resourceCategory", "rows", "data" };
            StructureOptions[] options = new StructureOptions[] { StructureOptions.One, StructureOptions.Many, StructureOptions.Many };
            return structuredResult(cmd, "resourcelist", relatedTableNames, options);
        }


        #endregion


        #region CensusWorkbook Excel based imports
        async public Task<IDataResult> CensusWorkbookStudentsAsync( XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = CensusWorkbookStudentsCmd(listObject, fileReference, user);
			return await sqlExecAsync(cmd, false, "pupiltable");
        }

		async public Task<IDataResult> CensusWorkbookOutputAsync(Guid fileReference)
		{
			SqlCommand cmd = CensusWorkbookOutputCmd(fileReference);
			//return await sqlExecAsync(cmd, false, "pupiltable");

			string[] tableNames = new string[] {
					"schoolList"
					, "levelList"
					, "ageList"
					, "enrolmentTableData"
					, "repeatersData"
					, "psaTableData"
					, "disabCodes"
					, "disabTableData"
					, "dropoutColumns"
					, "dropoutTableData"
					, "trinTableData"
					, "troutTableData"
					// from staff
					, "staffInfo"		// ignored
					, "staffSchoolList"
					, "tamList"
					, "tamTableData"
				};
			StructureOptions[] options =
			{
					StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many

					, StructureOptions.Ignore	
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
					, StructureOptions.Many
				};
				return await structuredResultAsync(cmd, "census", tableNames, options);

	}
	async public Task<IDataResult> CensusWorkbookSchoolsAsync(XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = CensusWorkbookSchoolsCmd(listObject, fileReference, user);
            return await sqlExecAsync(cmd, false, "CensusWorkbookSchools");
        }

        async public Task<IDataResult> CensusWorkbookStaffAsync(XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = CensusWorkbookStaffCmd(listObject, fileReference, user);
            return await sqlExecAsync(cmd, false, "CensusWorkbookStaff");
        }

        async public Task<IDataResult> CensusWorkbookWashAsync(XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = CensusWorkbookWashCmd(listObject, fileReference, user);
            return await sqlExecAsync(cmd, false, "CensusWorkbookWash");
        }
        async public Task<IDataResult> CensusWorkbookValidateSchoolsAsync(XDocument schoolList)
        {
            SqlCommand cmd = CensusWorkbookValidateSchoolsCmd(schoolList);
            return await sqlExecAsync(cmd, false, "CensusWorkbookSchoolList");
        }
        async public Task<IDataResult> CensusWorkbookValidateStaffAsync(XDocument listObject)
        {
            SqlCommand cmd = CensusWorkbookValidateStaffCmd(listObject);
            return await sqlExecAsync(cmd, false, "CensusWorkbookStaffValidations");
        }
		async public Task<IDataResult> CensusWorkbookValidateStudentsAsync(XDocument listObject)
		{
			SqlCommand cmd = CensusWorkbookValidateStudentsCmd(listObject);
			return await sqlExecAsync(cmd, false, "CensusWorkbookStudentValidations");
		}
		async public Task<IDataResult> CensusWorkbookValidateWashAsync(XDocument listObject)
		{
			SqlCommand cmd = CensusWorkbookValidateWashCmd(listObject);
			return await sqlExecAsync(cmd, false, "CensusWorkbookWashValidations");
		}
		async public Task<Dictionary<string, string>> CensusWorkbookColumnMapping(string sheet)
		{
			SqlCommand cmd = CensusWorkbookMappingCmd(sheet);
			DataSet ds = await cmdToDatasetAsync(cmd);
			var result = new Dictionary<string, string>();
			foreach (DataRow row in ds.Tables[0].Rows)
			{
				result.Add(row[0].ToString(), row[1].ToString());
			}
			return result;
		}

        /// <summary>
        /// Utility routine to put the two Xml rows onto their relevant SchoolSurvey records
        /// 
        /// </summary>
        /// <param name="schoolXml"></param>
        /// <param name="washXml"></param>
        async public Task<IDataResult> SaveXmlToSurvey(XDocument schoolXml, XDocument washXml, Guid fileReference)
		{
            SqlCommand cmd = SaveXmlToSurveyCmd(schoolXml, washXml, fileReference);
            return await sqlExecAsync(cmd, false, "schools");
        }
        #endregion

        #region Census workbook rollover
        public async Task<IDataResult> CensusRollover(int year, string schoolNo, string district)
        {
            SqlCommand cmd = CensusRolloverCmd(year, schoolNo, district);
            return await structuredResultAsync(cmd,"census", 
                new string[] { "schoolsMap", "staffMap", "studentsMap", "washMap", "schools", "staff" ,"students", "wash"} );
        }

        public async Task<IDataResult> CensusRolloverSchools(int year, string schoolNo, string district)
		{
            SqlCommand cmd = CensusRolloverSchoolsCmd(year, schoolNo, district);
            return await sqlExecAsync(cmd, false, "schools");
        }

        public async Task<IDataResult> CensusRolloverStaff(int year, string schoolNo, string district)
        {
            SqlCommand cmd = CensusRolloverStaffCmd(year, schoolNo, district);
            return await sqlExecAsync(cmd, false, "staff");
        }

        public async Task<IDataResult> CensusRolloverStudents(int year, string schoolNo, string district)
        {
            SqlCommand cmd = CensusRolloverStudentsCmd(year, schoolNo, district);
            return await sqlExecAsync(cmd, false, "students");
        }
        public async Task<IDataResult> CensusRolloverWash(int year, string schoolNo, string district)
        {
            SqlCommand cmd = CensusRolloverWashCmd(year, schoolNo, district);
            return await sqlExecAsync(cmd, false, "wash");
        }
        #endregion

        #region commands

        private SqlCommand CensusRolloverCmd(int year, string schoolNo, string district)
        {
            SqlCommand cmd = GetCmd(year, schoolNo, district);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 90;
            cmd.CommandText =
$@"
{BaseText}
exec pSurveyOps.CensusWorkbookColumnMapping 'schools'
exec pSurveyOps.CensusWorkbookColumnMapping 'schoolStaff'
exec pSurveyOps.CensusWorkbookColumnMapping 'students'
exec pSurveyOps.CensusWorkbookColumnMapping 'wash'
exec pSurveyOps.CensusRolloverSchools @SurveyYear, @schoolNo, @DistrictCode
exec pSurveyOps.CensusRolloverStaff @SurveyYear, @schoolNo, @DistrictCode
exec pSurveyOps.CensusRolloverStudents @SurveyYear, @schoolNo, @DistrictCode
exec pSurveyOps.CensusRolloverWash @SurveyYear, @schoolNo, @DistrictCode
";

            return cmd;
        }

        private string BaseText =
@"Select @SurveyYear SurveyYear, @schoolNo SchoolNo, @DistrictCode District";

        private SqlCommand GetCmd(int year, string schoolNo, string district)
		{
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@surveyYear", SqlDbType.Int)
                .Value = year;
            cmd.Parameters.Add("@schoolNo", SqlDbType.NVarChar, 50)
                .Value = (object)schoolNo??DBNull.Value;
            cmd.Parameters.Add("@districtCode", SqlDbType.NVarChar, 50)
                .Value = (object)district ?? DBNull.Value;

            return cmd;

        }
        private SqlCommand CensusRolloverSchoolsCmd(int year, string schoolNo, string district)
        {
            SqlCommand cmd = GetCmd(year, schoolNo, district);
            cmd.CommandText = "pSurveyOps.censusRolloverSchools";
            return cmd;
        }

        private SqlCommand CensusRolloverStaffCmd(int year, string schoolNo, string district)
        {
            SqlCommand cmd = GetCmd(year, schoolNo, district);
            cmd.CommandText = "pSurveyOps.censusRolloverStaff";
            return cmd;
        }
        private SqlCommand CensusRolloverStudentsCmd(int year, string schoolNo, string district)
        {
            SqlCommand cmd = GetCmd(year, schoolNo, district);
            cmd.CommandText = "pSurveyOps.censusRolloverStudents";
            return cmd;
        }
        private SqlCommand CensusRolloverWashCmd(int year, string schoolNo, string district)
        {
            SqlCommand cmd = GetCmd(year, schoolNo, district);
            cmd.CommandText = "pSurveyOps.censusRolloverWash";
            return cmd;
        }

        private SqlCommand SaveXmlToSurveyCmd(XDocument schoolXml, XDocument washXml, Guid fileReference)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyOps.censusSaveXml";
            cmd.Parameters.Add("@schoolXml", SqlDbType.Xml)
                .Value = schoolXml.ToString();
            cmd.Parameters.Add("@washXml", SqlDbType.Xml)
                .Value = washXml.ToString();
            cmd.Parameters.Add("@fileReference", SqlDbType.UniqueIdentifier)
                .Value = fileReference;

            return cmd;


        }


        private SqlCommand pupilTableCmd(string schoolNo, int year, string tableName)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pEnrolmentRead.PupilTableRead";

            cmd.Parameters.Add("@SchoolNo", SqlDbType.NVarChar, 50)
                .Value = schoolNo;
            cmd.Parameters.Add("@SurveyYear", SqlDbType.Int)
                .Value = year;
            cmd.Parameters.Add("@TableName", SqlDbType.NVarChar, 50)
                .Value = tableName;
            return cmd;


        }

        private SqlCommand savePupilTableCmd(string schoolNo, int year, string tableName,
            System.Xml.Linq.XDocument xml, string user)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pEnrolmentWrite.PupilTableSave";

            cmd.Parameters.Add("@SchoolNo", SqlDbType.NVarChar, 50)
                .Value = schoolNo;
            cmd.Parameters.Add("@SurveyYear", SqlDbType.Int)
                .Value = year;
            cmd.Parameters.Add("@TableName", SqlDbType.NVarChar, 50)
                .Value = tableName;
            cmd.Parameters.Add("@grid", SqlDbType.Xml)
                .Value = xml.ToString();
            cmd.Parameters.Add("@user", SqlDbType.NVarChar, 50)
                .Value = user;

            return cmd;
        }

        private SqlCommand resourceListCmd(string schoolNo, int year, string category, string split, string schoolType)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyRead.ResourceList";

            cmd.Parameters.Add("@SchoolNo", SqlDbType.NVarChar, 50)
                .Value = schoolNo;
            cmd.Parameters.Add("@SurveyYear", SqlDbType.Int)
                .Value = year;
            cmd.Parameters.Add("@category", SqlDbType.NVarChar, 100)
                .Value = category;
            cmd.Parameters.Add("@split", SqlDbType.NVarChar, 50)
                 .Value = split;
            cmd.Parameters.Add("@schoolType", SqlDbType.NVarChar, 10)
                .Value = schoolType;
            return cmd;
        }

        private SqlCommand saveResourceListCmd(string schoolNo, int year, string category, string split, string schoolType,
             System.Xml.Linq.XDocument xml, string user)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyWrite.ResourceListSave";

            cmd.Parameters.Add("@list", SqlDbType.Xml)
                .Value = xml.ToString();
            cmd.Parameters.Add("@user", SqlDbType.NVarChar, 50)
                .Value = user;
            return cmd;
            #endregion

        }

        #region CensusWorkbook Commands
        private SqlCommand CensusWorkbookStudentsCmd(XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyOps.censusLoadStudents";
            cmd.CommandTimeout = 240;

            cmd.Parameters.Add("@xml", SqlDbType.Xml)
                .Value = listObject.ToString();
            cmd.Parameters.Add("@fileReference", SqlDbType.UniqueIdentifier)
                .Value = fileReference;
            cmd.Parameters.Add("@user", SqlDbType.NVarChar, 50)
                .Value = user;
            return cmd;

        }
		private SqlCommand CensusWorkbookOutputCmd(Guid fileReference)
		{
			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "pSurveyOps.censusOutput";
			cmd.CommandTimeout = 240;

			cmd.Parameters.Add("@fileReference", SqlDbType.UniqueIdentifier)
				.Value = fileReference;
			return cmd;
		}

		private SqlCommand CensusWorkbookSchoolsCmd(XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyOps.censusLoadSchools";

            cmd.Parameters.Add("@xml", SqlDbType.Xml)
                .Value = listObject.ToString();
            cmd.Parameters.Add("@fileReference", SqlDbType.UniqueIdentifier)
                .Value = fileReference;
            cmd.Parameters.Add("@user", SqlDbType.NVarChar, 50)
                .Value = user;
            return cmd;

        }

        private SqlCommand CensusWorkbookStaffCmd(XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyOps.censusLoadStaff";
            cmd.CommandTimeout = 240;

            cmd.Parameters.Add("@xml", SqlDbType.Xml)
                .Value = listObject.ToString();
            cmd.Parameters.Add("@fileReference", SqlDbType.UniqueIdentifier)
                .Value = fileReference;
            cmd.Parameters.Add("@user", SqlDbType.NVarChar, 50)
                .Value = user;
            return cmd;

        }


        private SqlCommand CensusWorkbookWashCmd(XDocument listObject, Guid fileReference, string user)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyOps.censusLoadWash";

            cmd.Parameters.Add("@xml", SqlDbType.Xml)
                .Value = listObject.ToString();
            cmd.Parameters.Add("@fileReference", SqlDbType.UniqueIdentifier)
                .Value = fileReference;
            cmd.Parameters.Add("@user", SqlDbType.NVarChar, 50)
                .Value = user;
            return cmd;
        }
        private SqlCommand CensusWorkbookValidateSchoolsCmd(XDocument schoolsList)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyOps.censusValidateSchools";

            cmd.Parameters.Add("@xml", SqlDbType.Xml)
                .Value = schoolsList.ToString();
            return cmd;

        }

        private SqlCommand CensusWorkbookValidateStaffCmd(XDocument listObject)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyOps.censusValidateStaff";

            cmd.Parameters.Add("@xml", SqlDbType.Xml)
                .Value = listObject.ToString();
            return cmd;

        }

		private SqlCommand CensusWorkbookValidateWashCmd(XDocument listObject)
		{
			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "pSurveyOps.censusValidateWash";

			cmd.Parameters.Add("@xml", SqlDbType.Xml)
				.Value = listObject.ToString();
			return cmd;

		}

		private SqlCommand CensusWorkbookValidateStudentsCmd(XDocument listObject)
		{
			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "pSurveyOps.censusValidateStudents";
			cmd.CommandTimeout = 240;

			cmd.Parameters.Add("@xml", SqlDbType.Xml)
				.Value = listObject.ToString();
			return cmd;
		}

		private SqlCommand CensusWorkbookMappingCmd(string sheet)
		{
			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "pSurveyOps.CensusWorkbookColumnMapping";

			cmd.Parameters.Add("@sheetName", SqlDbType.NVarChar, 50)
				.Value = sheet;
			return cmd;


		}

		#endregion
	}
}
