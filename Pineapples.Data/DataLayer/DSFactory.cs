﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB = Pineapples.Data.DB;


namespace Pineapples.Data.DataLayer
{
	public class DSFactory : IDSFactory
	{
		private DB.PineapplesEfContext cxt;

		// defaut constructor initialises the Efcontext from the config
		public DSFactory()
		{
			string server = System.Web.Configuration.WebConfigurationManager.AppSettings["server"]; // "10.0.50.69";
			string database = System.Web.Configuration.WebConfigurationManager.AppSettings["database"]; // "10.0.50.69";
			string appname = System.Web.Configuration.WebConfigurationManager.AppSettings["appname"] ?? "Pineapples.Data"; // "10.0.50.69";
			cxt = getContext(server, database, appname);
		}

		public DSFactory(string server, string database)
		{
			cxt = getContext(server, database);
		}
		private DB.PineapplesEfContext getContext(string server, string database, string appname)
		{
			string conn = String.Format("Data Source={0};Initial Catalog={1};Application Name={2};Integrated Security=True", server, database, appname);
			return new DB.PineapplesEfContext(conn);
		}
		public DB.PineapplesEfContext getContext(string server, string database)
		{
			return getContext(server, database, "pineapples.Injected");
		}

		/// <summary>
		/// EF Context is exposed to allow simple Linq operations in controllers
		/// </summary>
		public DB.PineapplesEfContext Context
		{
			get
			{
				return cxt;
			}
		}

		// return the repositories
		public IDSPerfAssess PerfAssess()
		{
			return new DSPerfAssess(cxt);
		}

		public IDSSchool School()
		{
			return new DSSchool(cxt);
		}
		public IDSLookups Lookups()
		{
			return new DSLookups(cxt);
		}
		public IDSSchoolScatter SchoolScatter()
		{
			return new DSSchoolScatter(cxt);
		}

		public IDSTeacher Teacher()
		{
			return new DSTeacher(cxt);
		}
		public IDSTeacherDeduper TeacherDeduper()
		{
			return new DSTeacherDeduper(cxt);
		}

		public IDSTeacherLink TeacherLink()
		{
			return new DSTeacherLink(cxt);
		}
		public IDSExam Exam()
		{
			return new DSExam(cxt);
		}
		public IDSTeacherExam TeacherExam()
		{
			return new DSTeacherExam(cxt);
		}
		public IDSSelectors Selectors()
		{
			return new DSSelectors(cxt);
		}

		public IDSCensus Census()
		{
			return new DSCensus(cxt);
		}
		public IDSSurveyEntry SurveyEntry()
		{
			return new DSSurveyEntry(cxt);
		}

		public IDSUisSurvey UisSurvey()
		{
			return new DSUisSurvey(cxt);
		}
		public IDSBooks Books()
		{
			return new DSBooks(cxt);
		}

		public IDSQuarterlyReport QuarterlyReport()
		{
			return new DSQuarterlyReport(cxt);
		}

		public IDSQuarterlyReportX QuarterlyReportX()
		{
			return new DSQuarterlyReportX(cxt);
		}

		public IDSSchoolAccreditation SchoolAccreditation()
		{
			return new DSSchoolAccreditation(cxt);
		}

		public IDSSchoolInspection SchoolInspection()
		{
			return new DSSchoolInspection(cxt);
		}

		public IDSSchoolLink SchoolLink()
		{
			return new DSSchoolLink(cxt);
		}

		public IDSSurvey Survey()
		{
			return new DSSurvey(cxt);
		}

		public IDSIndicators Indicators()
		{
			return new DSIndicators(cxt);
		}
		public IDSDocument Document()
		{
			return new DSDocument(cxt);
		}

		public IDSKobo Kobo()
		{
			return new DSKobo(cxt);
		}

		public IDSStudent Student()
		{
			return new DSStudent(cxt);
		}
		public IDSStudentLink StudentLink()
		{
			return new DSStudentLink(cxt);
		}

		public IDSScholarship Scholarship()
		{
			return new DSScholarship(cxt);
		}
		public IDSScholarshipLink ScholarshipLink()
		{
			return new DSScholarshipLink(cxt);
		}
		public IDSSpecialEdStudent SpecialEdStudent()
		{
			return new DSSpecialEdStudent(cxt);
		}
		public IDSWarehouse Warehouse()
		{
			return new DSWarehouse(cxt);
		}
		public IDSGovtExpenditure GovtExpenditure()
		{
			return new DSGovtExpenditure(cxt);
		}

		public IDSStudentDeduper StudentDeduper()
		{
			return new DSStudentDeduper(cxt);
		}

		public IDSPdfSurvey PdfSurvey()
		{
			return new DSPdfSurvey(cxt);
		}


	}
}
