﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Xml.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using iText.StyledXmlParser.Jsoup.Select;
using Pineapples.Data.Models;

namespace Pineapples.Data.DataLayer
{
	internal class DSPdfSurvey : BaseRepository, IDSPdfSurvey
	{
		public DSPdfSurvey(DB.PineapplesEfContext cxt) : base(cxt)
		{

		}

		public object xfdfParentCommittee(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfParentCommittee", surveyID, sectionxml);
			return sqlExec(cmd);
		}
		public object xfdfClass(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfGridCommand("pEnrolmentWrite.xfdfClass", surveyID, sectionxml);
			return sqlExec(cmd, false);
		}

		public object xfdfDisabilityNA(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pEnrolmentWrite.xfdfDisabilityNA", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfDistanceTransport(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfDistanceTransport", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfEnrolment(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfGridCommand("pEnrolmentWrite.xfdfEnrolment", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfFunding(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfFunding", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfGrid(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfGridCommand("dbo.xfdfGrid", surveyID, sectionxml);
			return sqlExec(cmd, false);
		}

		public object xfdfHousing(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfHousing", surveyID, sectionxml);
			return sqlExec(cmd);
		}



		public object xfdfPreschool(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfPreschool", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfResourceList(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfGridCommand("pSchoolWrite.xfdfResourceList", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfSite(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfSite", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfSupplies(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfSupplies", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfTeachers(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pTeacherWrite.xfdfTeachers", surveyID, sectionxml);
			return sqlExec(cmd, false);
		}
		public object xfdfRooms(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfRoomCommand("pSchoolWrite.xfdfRooms", surveyID, sectionxml);
			return sqlExec(cmd);
		}
		public object xfdfToilets(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfToilets", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public object xfdfWater(int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = xfdfCommand("pSurveyWrite.xfdfWater", surveyID, sectionxml);
			return sqlExec(cmd);
		}

		public int? xfdfSurvey(XElement sectionxml)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("xml", SqlDbType.Xml).Value = sectionxml.ToString();
			cmd.CommandText = "pSurveyWrite.xfdfSurvey";
			var result = sqlExec(cmd);
			System.Data.DataTable t = result.ResultSet as System.Data.DataTable;
			return (int?)(t.Rows[0]["SurveyID"]);
		}
		public string xfdfLogSurveyDocument(int? surveyID, string fileId, string extension, XDocument xfdf, ClaimsIdentity user)
		{

			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("surveyID", SqlDbType.Int).Value = surveyID;
			cmd.Parameters.Add("fileID", SqlDbType.NVarChar, 1000).Value = fileId;
			cmd.Parameters.Add("extension", SqlDbType.NVarChar, 10).Value = extension;
			cmd.Parameters.Add("xml", SqlDbType.Xml).Value = xfdf.ToString();
			cmd.Parameters.Add("user", SqlDbType.NVarChar, 50).Value = user.Name;
			cmd.CommandText = "pSurveyWrite.xfdfLogSurveyDocument";
			var result = sqlExec(cmd);
			System.Data.DataTable t = result.ResultSet as System.Data.DataTable;
			return (string)(t.Rows[0][0].ToString());
		}

		public void xfdfError(int surveyID, string section, Exception ex)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("ssID", SqlDbType.Int).Value = surveyID;
			cmd.Parameters.Add("error", SqlDbType.NVarChar, 1000).Value = ex.Source;
			cmd.Parameters.Add("section", SqlDbType.NVarChar, 100).Value = section;
			cmd.Parameters.Add("data", SqlDbType.NVarChar, 1000).Value = ex.Message;

			cmd.CommandText = "audit.xfdfError";
			var result = sqlExec(cmd);
		}
		public bool isTableDef(string code)
		{
			var ptt = Context.PupilTableTypes.Find(code);
			return (ptt != null);
		}
		#region commands

		private SqlCommand xfdfCommand(string spname, int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("surveyID", SqlDbType.Int).Value = surveyID;
			cmd.Parameters.Add("xml", SqlDbType.Xml).Value = sectionxml.ToString();
			cmd.CommandText = spname;
			return cmd;
		}

		private SqlCommand xfdfGridCommand(string spname, int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("surveyID", SqlDbType.Int).Value = surveyID;
			cmd.Parameters.Add("grid", SqlDbType.Xml).Value = sectionxml.ToString();
			cmd.CommandText = spname;
			return cmd;
		}
		private SqlCommand xfdfRoomCommand(string spname, int? surveyID, XElement sectionxml)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("surveyID", SqlDbType.Int).Value = surveyID;
			cmd.Parameters.Add("roomXml", SqlDbType.Xml).Value = sectionxml.ToString();
			cmd.CommandText = spname;
			return cmd;
		}

		private SqlCommand prepopTeachersCmd(string schoolNo, int? surveyYear)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("schNo", SqlDbType.NVarChar, 50).Value = schoolNo;
			cmd.Parameters.Add("targetYear", SqlDbType.Int).Value = surveyYear;
			cmd.CommandText = "pTeacherRead.TeacherSurveyValidationList";
			return cmd;

		}

		private SqlCommand prepopToiletsCmd(string schoolNo, int? surveyYear)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Add("schNo", SqlDbType.NVarChar, 50).Value = schoolNo;
			cmd.Parameters.Add("targetYear", SqlDbType.Int).Value = surveyYear;
			cmd.CommandText =
@"
Select svyYear, schNo, toiletType, UsedBy,
sum(M) M,
sum(F) F,
sum([All]) [All]
from
(
Select 
svyYear, schNo, case toiType 
when 'Flush' then '00'
	when 'Waterseal' then '01'
	when 'Composting' then '02' end ToiletType,
    case ToiUse 
	WHEN 'Boys' then 'Pupil'
	WHEN 'Girls' then 'Pupil'
	else ToiUse end UsedBy, 
	case ToiUse WHEN 'Boys' then toiNum else null end M,
	case ToiUse WHEN 'Girls' then toiNum else null end F,
	case ToiUse WHEN 'Staff' then toiNum
		WHEN 'Wheelchair' then	toiNum
		else null end [All],
	toiCondition C


FROM
(
Select svyYear, schNo, Toilets.* from Toilets 
INNER JOIN SchoolSurvey SS
ON SS.ssID = Toilets.ssID
WHERE schNo = @schNo and svyYear = @targetYear
) SUB
) SUB2
GROUP BY svyYear, schNo, toiletType, UsedBy
";
			return cmd;
		}

		private SqlCommand prepopClassroomsCmd(string schoolNo, int? surveyYear)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Add("schNo", SqlDbType.NVarChar, 50).Value = schoolNo;
			cmd.Parameters.Add("targetYear", SqlDbType.Int).Value = surveyYear;
			cmd.CommandText =
@"
Select 
	case rmMaterials when 'S' then 'SP' 
		else rmMaterials 
	end Material,
	count(*) Num
from Rooms
INNER JOIN SchoolSurvey SS
ON Rooms.ssID = SS.ssID
WHERE ss.svyYear = @targetYear
AND SS.schNo = @schNo
AND rmType = 'CLASS'
GROUP BY rmMaterials
";


			return cmd;
		}
		private SqlCommand prepopWaterSupplyCmd(string schoolNo, int? surveyYear)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Add("schNo", SqlDbType.NVarChar, 50).Value = schoolNo;
			cmd.Parameters.Add("targetYear", SqlDbType.Int).Value = surveyYear;
			cmd.CommandText =

@"
Select case resSplit 
	when 'Overhead Tank' then '00'
	when 'Roof Tank' then '01'
	when 'Rain Water Tank' then '01'
	when 'Well (drinkable)' then '02'
	when 'Well (non-drinkable)' then '03' 
	end R, 
	resNumber Num,
	resQty Qty,
	case resFunctioning when 1 then 'Y' when 0 then 'N' else null end OK
FROM Resources
INNER JOIN SchoolSurvey SS
ON Resources.ssID = SS.ssID
AND svyYear = @targetYear
AND schNo = @schNo
AND resName = 'Water Supply'
";


			return cmd;
		}

		#endregion

		#region Prepopulation

		public object prepopSchool(string schoolNo, int year)
		{
			return new NotImplementedException();
		}

		public IDataResult prepopTeachers(string schoolNo, int year)
		{
			SqlCommand cmd = prepopTeachersCmd(schoolNo, year);
			return sqlExec(cmd);
		}
		public IDataResult prepopToilets(string schoolNo, int year)
		{
			SqlCommand cmd = prepopToiletsCmd(schoolNo, year);
			return sqlExec(cmd);
		}
		public IDataResult prepopClassrooms(string schoolNo, int year)
		{
			SqlCommand cmd = prepopClassroomsCmd(schoolNo, year);
			return sqlExec(cmd);
		}
		public IDataResult prepopWaterSupply(string schoolNo, int year)
		{
			SqlCommand cmd = prepopWaterSupplyCmd(schoolNo, year);
			return sqlExec(cmd);
		}
		#endregion
	}
}
