﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;

namespace Pineapples.Data.DataLayer
{
    public class DSSchoolInspection : BaseRepository, IDSSchoolInspection
    {
        public DSSchoolInspection(DB.PineapplesEfContext cxt) : base(cxt)
        {

        }

		/// <summary>
		/// Calculate the 'result' (aka Level) of the inspection
		/// </summary>
		/// <param name="inspectionId"></param>
		/// <returns></returns>
		public string InspectionResult(int inspectionId)
		{
			// this table is CreateTagged, so we have to use the predicate to find by ID
			string inspType = cxt.SchoolInspections.First(x => x.ID == inspectionId).InspTypeCode;
			// TO DO entityfrmework should be able to elegantly handles this, like so:
			//string resultfunction = cxt.SchoolInspections.First(x => x.ID == inspectionId).Type.intyResultFunction;
			// but for now this does not return type as expected -- this may now be fixed by introduction of InspTypeCode
			string resultfunction = cxt.InspectionTypes.Find(inspType).intyResultFunction;
			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = String.Format("select {0}({1})", resultfunction, inspectionId);
			return db2str(cxt.execScalar(cmd));
		}

		/// <summary>
		/// Create a record in the document tabe - with appropriate school link - for 
		/// each photo detected in the inspection survey data
		/// </summary>
		/// <param name="inspectionID"></param>
		/// <returns></returns>
		public IDataResult RegisterInspectionPhotos(int inspectionID)
		{
			SqlCommand cmd = RegisterInspectionPhotosCmd(inspectionID);
			return sqlExec(cmd);
		}

		public IDataResult Read(int inspectionID)
		{
			SqlCommand cmd = readCmd(inspectionID);
			return sqlXml(cmd, "schoolinspection");
		}


		#region Collection Methods
		public IDataResult Filter(SchoolInspectionFilter fltr)
        {
            SqlCommand cmd = filterCmd(fltr);
            return sqlExec(cmd);
        }
        #endregion

        #region Commands
        private SqlCommand filterCmd(SchoolInspectionFilter fltr)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pInspectionRead.SchoolInspectionFilterPaged";

            cmd.Parameters.Add("@ColumnSet", SqlDbType.Int).Value = (object)(fltr.ColumnSet) ?? DBNull.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = (object)(fltr.PageSize) ?? DBNull.Value;
            cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = (object)(fltr.PageNo) ?? DBNull.Value;
            cmd.Parameters.Add("@SortColumn", SqlDbType.NVarChar, 128).Value = (object)(fltr.SortColumn) ?? DBNull.Value;
            cmd.Parameters.Add("@SortDir", SqlDbType.Int).Value = (object)(fltr.SortDesc) ?? DBNull.Value;

            cmd.Parameters.Add("@InspID", SqlDbType.Int).Value = (object)fltr.InspID ?? DBNull.Value;
            cmd.Parameters.Add("@School", SqlDbType.NVarChar, 50).Value = (object)fltr.School ?? DBNull.Value;
            cmd.Parameters.Add("@SchoolNo", SqlDbType.NVarChar, 50).Value = (object)fltr.SchoolNo ?? DBNull.Value;
            cmd.Parameters.Add("@InspYear", SqlDbType.NVarChar, 50).Value = (object)fltr.InspYear ?? DBNull.Value;
			cmd.Parameters.Add("@InspBy", SqlDbType.NVarChar, 50).Value = (object)fltr.InspBy ?? DBNull.Value;
			cmd.Parameters.Add("@InspResult", SqlDbType.NVarChar, 50).Value = (object)fltr.InspResult ?? DBNull.Value;
			cmd.Parameters.Add("@InspType", SqlDbType.NVarChar, 50).Value = (object)fltr.InspType ?? DBNull.Value;

			cmd.Parameters.Add("@FileSource", SqlDbType.NVarChar, 50).Value = (object)fltr.FileSource ?? DBNull.Value;
			cmd.Parameters.Add("@District", SqlDbType.NVarChar, 10).Value = (object)fltr.District ?? DBNull.Value;
            cmd.Parameters.Add("@Authority", SqlDbType.NVarChar, 10).Value = (object)fltr.Authority ?? DBNull.Value;


            return cmd;
        }

        /// <summary>
        /// read command read a single school accreditation
        /// </summary>
        private SqlCommand readCmd(int inspID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pInspectionRead.Inspection_ReadXml";

            cmd.Parameters.Add("@inspID", SqlDbType.Int).Value = inspID;
            return cmd;
        }

		private SqlCommand RegisterInspectionPhotosCmd(int inspID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "pInspectionWrite.RegisterInspectionPhotos";

			cmd.Parameters.Add("@inspID", SqlDbType.Int).Value = inspID;
			return cmd;
		}

		private SqlCommand accessControlCmd(int id, string district, string authority, string userSchool)
		{

			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "pInspectionRead.accessControl";

			cmd.Parameters.Add("@inspID", SqlDbType.Int)
				.Value = id;
			cmd.Parameters.Add("@district", SqlDbType.NVarChar, 10)
				.Value = district;
			cmd.Parameters.Add("@authority", SqlDbType.NVarChar, 20)
				.Value = authority;
			cmd.Parameters.Add("@userSchool", SqlDbType.NVarChar, 50)
				.Value = userSchool;
			return cmd;
		}
		#endregion

		#region accesscontrol
		public void AccessControl(int id, ClaimsIdentity identity)
		{
			string district = null;
			string authority = null;
			string userSchool = null;
			Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
			if (c != null)
			{
				district = c.Value;
			}
			c = identity.FindFirst(x => x.Type == "filterAuthority");
			if (c != null)
			{
				authority = c.Value;
			}

			c = identity.FindFirst(x => x.Type == "filterSchoolNo");
			if (c != null)
			{
				userSchool = c.Value;
			}

			if (district == null && authority == null && userSchool == null)
			{
				return;                 // no issues here
			}
			SqlCommand cmd = accessControlCmd(id, district, authority, userSchool);
			sqlExec(cmd);

		}
		#endregion
	}
}
