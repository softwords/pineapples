﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		//================================================================================
		// For Exams dashboards
		#region Exam endpoints

		public IDataResult ExamTableTyped()
		{
			SqlCommand cmd = ExamTableTypedCmd();

			return sqlExec(cmd);

		}

		public IDataResult ExamTableTypedX()
		{
			SqlCommand cmd = ExamTableTypedXCmd();

			return sqlExec(cmd);

		}

		public IDataResult ExamSchoolResults(string schoolNo, bool reportFormat = false, bool report = false)
		{
			SqlCommand cmd = new WarehouseExamCommands(reportFormat, report)
											.SchoolCmd(schoolNo);

			return sqlExec(cmd);
		}

		public IDataResult ExamAllSchoolResults()
		{
			SqlCommand cmd = examSchoolResultsCmdV1(null);
			return sqlExec(cmd);
		}

		public IDataResult ExamDistrictResults(string district, bool reportFormat = false, bool report = false)
		{
			SqlCommand cmd = new WarehouseExamCommands(reportFormat, report)
											.DistrictCmd(district);

			return sqlExec(cmd);
		}

		public IDataResult ExamDistrictResultsv1()
		{
			SqlCommand cmd = examDistrictResultsV1Cmd();
			return sqlExec(cmd);
		}
		#endregion


		#region commands

		private SqlCommand ExamTableTypedCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = "Select * from warehouse.ExamTableResultsTyped";

			cmd.CommandText = @"
SELECT examCode
      ,examYear
      ,Gender
      ,DistrictCode
      ,SchoolTypeCode
      ,AuthorityCode
      ,AuthorityGovtCode
      ,RecordType
      ,[Key]
      ,Description
      ,achievementLevel
	  ,achievementDesc
      ,sum(indicatorCount) indicatorCount
      ,sum([weight]) weight
      ,sum(candidateCount) candidateCount
FROM warehouse.ExamTableResultsTyped
GROUP BY examCode
      ,examYear
      ,Gender
      ,DistrictCode
      ,SchoolTypeCode
      ,AuthorityCode
      ,AuthorityGovtCode
      ,RecordType
      ,ID
      ,[Key]
      ,Description
      ,achievementLevel
	  ,achievementDesc

";
			//	cmd.CommandText = "Select * from warehouse.ExamTableResultsTyped";

			return cmd;
		}

		private SqlCommand ExamTableTypedXCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = "Select * from warehouse.ExamTableResultsTypedX";

			cmd.CommandText = @"
SELECT examCode
      ,examYear
      ,Gender
      ,DistrictCode
      ,SchoolTypeCode
      ,AuthorityCode
      ,AuthorityGovtCode	  
	  ,IslandCode
	  ,RegionCode
      ,RecordType
      ,[Key]
      ,Description
      ,achievementLevel
	  ,achievementDesc
      ,sum(indicatorCount) indicatorCount
      ,sum([weight]) weight
      ,sum(candidateCount) candidateCount
FROM warehouse.ExamTableResultsTypedX
GROUP BY examCode
      ,examYear
      ,Gender
      ,DistrictCode
      ,SchoolTypeCode
      ,AuthorityCode
      ,AuthorityGovtCode	  
	  ,IslandCode
	  ,RegionCode
      ,RecordType
      ,ID
      ,[Key]
      ,Description
      ,achievementLevel
	  ,achievementDesc

";
			//	cmd.CommandText = "Select * from warehouse.ExamTableResultsTypedX";

			return cmd;
		}

		#endregion

		#region deprecated
		private SqlCommand examSchoolResultsCmdV1(string SchoolNo)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                      SELECT[examCode] + ': ' + [examName]
        AS Exam
     ,[examYear] AS ExamYear
     ,[StateID] AS DistrictCode
     ,[schNo] As SchoolNo
     ,[standardCode] + ': ' + [standardDesc]
        AS ExamStandard
     ,[benchmarkCode] + ': ' + [exbnchDescription]
        AS ExamBenchmark
     , sum(case Gender when 'M' then[Candidates] else null end) AS[CandidatesM]
	  ,sum(case Gender when 'M' then[1] else null end) AS[WellBelowCompetentM]
	  ,sum(case Gender when 'M' then[2] else null end) [ApproachingCompetenceM]
	  ,sum(case Gender when 'M' then[3] else null end) [MinimallyCompetentM]
	  ,sum(case Gender when 'M' then[4] else null end) [CompetentM]
	  ,sum(case Gender when 'F' then[Candidates] else null end) AS[CandidatesF]
	  ,sum(case Gender when 'F' then[1] else null end) AS[WellBelowCompetentF]
	  ,sum(case Gender when 'F' then[2] else null end) [ApproachingCompetenceF]
	  ,sum(case Gender when 'F' then[3] else null end) [MinimallyCompetentF]
	  ,sum(case Gender when 'F' then[4] else null end) [CompetentF]
        FROM [warehouse].[ExamSchoolResultsX]
        EX
INNER JOIN[dbo].[ExamBenchmarks] EB ON EX.benchmarkCode = EB.exbnchCode
WHERE (schNo = @schoolNo or @schoolNo is null)
GROUP BY
[examCode] + ': ' + [examName]
      ,[examYear]
      ,[StateID]
      ,[schNo]
      ,[standardCode] + ': ' + [standardDesc]
      ,[benchmarkCode] + ': ' + [exbnchDescription]";
			cmd.CommandTimeout = 300;            // 5 mins
			cmd.Parameters.Add("@schoolNo", SqlDbType.NVarChar, 50)
					.Value = (object)(SchoolNo) ?? DBNull.Value;

			return cmd;
		}


		private SqlCommand examDistrictResultsV1Cmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                    SELECT [examCode] + ': ' + [examName] AS Exam
      ,[examYear] AS ExamYear
      ,[StateID] AS DistrictCode
      ,[standardCode] + ': ' + [standardDesc] AS ExamStandard
      ,[benchmarkCode] + ': ' + [exbnchDescription] AS ExamBenchmark
	  ,sum(case Gender when 'M' then [Candidates] else null end) AS [CandidatesM]
	  ,sum(case Gender when 'M' then [1] else null end) AS [1M]
	  ,sum(case Gender when 'M' then [2] else null end) [ApproachingCompetenceM]
	  ,sum(case Gender when 'M' then [3] else null end) [MinimallyCompetentM]
	  ,sum(case Gender when 'M' then [4] else null end) [CompetentM]
	  ,sum(case Gender when 'F' then [Candidates] else null end) AS [CandidatesF]
	  ,sum(case Gender when 'F' then [1] else null end) AS [WellBelowCompetentF]
	  ,sum(case Gender when 'F' then [2] else null end) [ApproachingCompetenceF]
	  ,sum(case Gender when 'F' then [3] else null end) [MinimallyCompetentF]
	  ,sum(case Gender when 'F' then [4] else null end) [CompetentF]
  FROM [warehouse].[ExamStateResultsX] ESX
  INNER JOIN [dbo].[ExamBenchmarks] EB ON ESX.benchmarkCode = EB.exbnchCode
  GROUP BY
  [examCode] + ': ' + [examName]
      ,[examYear]
      ,[StateID]
      ,[standardCode] + ': ' + [standardDesc]
      ,[benchmarkCode] + ': ' + [exbnchDescription]";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		#endregion
	}
}
