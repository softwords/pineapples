﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		/// <summary>
		/// Produces teacher count data. This is the older general teachercount method basically
		/// working on school, table and nation groups. There is a trend towards possible a better
		/// more flexible API but I think this can remain for now.
		/// </summary>
		/// <param name="grouping">The primary level of disaggregation (e.g. Nation, District, Island) </param>
		/// <param name="filterCode">The selected entity (schNo CHK1002, IslandCode 01, etc.)</param>
		/// <param name="report">Whether to produce the report format (i.e. Gender denormalized)</param>
		/// <returns></returns>
		public IDataResult TeacherCount(string grouping = null, string filterCode = null
									, bool? report = false)
		{

			string r = report == null ? "" : ((bool)report ? "R" : "");
			string selector = $"{grouping}code";
			switch (grouping.ToLower())
			{
				case "school":
					selector = "schNo";
					break;
				case "table":
				case "nation":
					selector = null;
					break;

			}
			string whereclause = (selector != null) ? $" WHERE {selector} = @Selected OR @Selected is null" : "";
			string sql =
$@"Select * from warehouse.TeacherCount{grouping}{r} {whereclause} ORDER BY SurveyYear";

			SqlCommand cmd = new SqlCommand(sql);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 50).Value = (object)filterCode ?? DBNull.Value;
			return sqlExec(cmd);
		}

		#region Teachers (counts)
		public IDataResult SchoolTeacherCount(string schoolNo)
		{
			SqlCommand cmd = schoolTeacherCountCmd(schoolNo);
			return sqlExec(cmd);
		}
		public IDataResult AllSchoolTeacherCount()
		{
			SqlCommand cmd = schoolTeacherCountCmd(null);
			return sqlExec(cmd);
		}
		public IDataResult SchoolTeacherPupilRatio()
		{
			SqlCommand cmd = schoolTeacherPupilRatioCmd();
			return sqlExec(cmd);
		}
		// Can also be used for individual Teacher dashboard
		public IDataResult TeacherQual()
		{
			SqlCommand cmd = teacherQualCmd();
			return sqlExec(cmd);
		}
		#endregion

		#region Teacher Data handling (i.e. TeacherActivity, TeacherJob, TeacherEdLevel, etc.)
		/// <summary>
		/// A more general method to produce am IDataResult in a flexible manner. Merely a thin wrapper to produce
		/// the SQL and execute it.
		/// </summary>
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <param name="grouping">The primary level of disaggregation (e.g. Nation, District, Island) </param>
		/// <param name="filterCode">The selected entity (schNo CHK1002, IslandCode 01, etc.)</param>
		/// <param name="report">Whether to produce the report format (i.e. Gender denormalized)</param>
		/// <param name="xl">Whether to produce and export an Excel spreadsheet with PivotTable</param>
		/// <param name="byActivity">Optional field used in fine tuning data for pivot tables for the excel export</param>
		/// <param name="selectedYear">Optional year for which the data should be exclusively produced, also used in the Excel exports</param>
		/// <returns>The result of the executed SQL in an IDataResult object</returns>
		public IDataResult TeacherData(string content = null, string grouping = null, string filterCode = null
			, bool? report = false, string xl = ""
			, bool byActivity = false, int? selectedYear = null)
		{
			if (xl != string.Empty)
			{
				switch (grouping)
				{
					case "Nation":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).NationCmd(content));
					case "School":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).SchoolCmd(content, filterCode));
					case "District":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).DistrictCmd(content, filterCode));
					case "Authority":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).AuthorityCmd(content, filterCode));
					case "Island":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).IslandCmd(content, filterCode));
					case "Region":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).RegionCmd(content, filterCode));
					case "SchoolType":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).SchoolTypeCmd(content, filterCode));
					case "X":
						return sqlExec(new WarehouseTeachersCommands(content, grouping, filterCode, report, byActivity, selectedYear).ExtendedCmd(content));
				}
			}

			SqlCommand cmd = new WarehouseTeachersCommands(content, grouping, filterCode, report).GeneralCmd();
			return sqlExec(cmd);

		}
		#endregion

	}
}
