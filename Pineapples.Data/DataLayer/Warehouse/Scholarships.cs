﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		public IDataResult ScholarshipRounds()
		{
			SqlCommand cmd = new WarehouseScholarshipCommands()
									.ScholarshipRoundsCommand();
			return sqlExec(cmd);
		}
		public IDataResult ScholarshipPayments()
		{
			SqlCommand cmd = new WarehouseScholarshipCommands()
									.ScholarshipPaymentsCommand();
			return sqlExec(cmd);
		}
	}
}
