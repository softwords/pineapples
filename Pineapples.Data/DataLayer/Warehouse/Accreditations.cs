﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		#region Accreditations

		public IDataResult Accreditations(string content
							, string grouping, string groupValue
							, bool? report)
		{
			SqlCommand cmd = null;
			var wac = new WarehouseAccreditationCommands(content, grouping, report);
			cmd = wac.BaseCommand(groupValue);
			return sqlExec(cmd);
		}

		public IDataResult AccreditationsFilter(SchoolAccreditationFilter fltr)
		{
			SqlCommand cmd = WarehouseAccreditationCommands
								.FilterCmd(fltr);
			return sqlExec(cmd);
		}

		#endregion

	}
}
