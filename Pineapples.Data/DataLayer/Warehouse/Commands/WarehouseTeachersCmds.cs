﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
	class WarehouseTeachersCommands
	{
		internal string content { get; set; }
		internal string grouping { get; set; }
		internal string filterCode { get; set; }
		internal bool byActivity { get; set; }
		internal int? selectedYear { get; set; }
		internal bool report { get; set; }


		internal WarehouseTeachersCommands(string content)
		{
			this.content = content;
		}

		/// <summary>
		/// This implementation is a first draft at abstracting some of the content stuff in here
		/// It should be able to produce the desired data for the API but with also support to produce the Excel
		/// export. 
		/// </summary>
		/// <param name="content">The content or subtance of the data (e.g. TeacherActivity, TeacherJob, TeacherEdLevel, etc.)</param>
		/// <param name="grouping">The primary level of disaggregation (e.g. Nation, District, Island)</param>
		/// <param name="filterCode">The selected entity (schNo CHK1002, IslandCode 01, etc.)</param>
		/// <param name="report">Whether to produce the report format (i.e. Gender denormalized)</param> 
		internal WarehouseTeachersCommands(string content, string grouping, string filterCode, bool? report)
		{
			this.content = content;
			this.grouping = grouping;
			this.filterCode = filterCode;
			this.report = report ?? false;
		}

		/// <summary>
		/// This implementation is a first draft at abstracting some of the content stuff in here
		/// It should be able to produce the desired data for the API but with also support to produce the Excel
		/// export. 
		/// </summary>
		/// <param name="content">The content or subtance of the data (e.g. TeacherActivity, TeacherJob, TeacherEdLevel, etc.)</param>
		/// <param name="grouping">The primary level of disaggregation (e.g. Nation, District, Island)</param>
		/// <param name="filterCode">The selected entity (schNo CHK1002, IslandCode 01, etc.)</param>
		/// <param name="byActivity">Optional field used in fine tuning data for pivot tables for the excel export</param>
		/// <param name="selectedYear">Optional year for which the data should be exclusively produced, also used in the Excel exports</param>
		/// <param name="report">Whether to produce the report format (i.e. Gender denormalized)</param> 
		internal WarehouseTeachersCommands(string content, string grouping, string filterCode, bool? report
			, bool byActivity, int? selectedYear = null)
		{
			this.content = content;
			this.grouping = grouping;
			this.filterCode = filterCode;
			this.report = report ?? false;
			this.byActivity = byActivity;
			this.selectedYear = selectedYear;
		}

		#region General
		/// <summary>
		/// This was the previously used way of producing Teacher data (before support for Excel workbook export) 
		/// I don't think the API getting this data from here was used yet.
		/// 
		/// NOTE: Should this be used or should the baseTemplate be used? 
		/// This is used one place while the baseTemplate is not currently used. Moreover, all the {Grouping}Template methods
		/// are in used at least for the Excel exports.
		/// 
		/// It differs from the other SQL queries generating functions below in that those are 
		/// aggregate queries excluding some fields and more fit for the purpose of the version of the API that 
		/// exports the Excel workbook with Pivot table and chart. However, no reason this can not be used for 
		/// an export to Excel also.
		/// </summary>
		/// <returns>Returns the SQL to retrieve data from the whole warehouse table/view requested in full.</returns>
		internal SqlCommand GeneralCmd()
		{
			string r = ((bool)report ? "R" : "");
			string selector = $"{grouping}Code";
			switch (grouping)
			{
				case "School":
					selector = "schNo";
					break;
				case "Table":
				case "Nation":
					selector = null;
					break;

			}
			string whereclause = (selector != null) ? $" WHERE {selector} = @Selected OR @Selected is null" : "";
			string sql =
		$@"Select * from warehouse.Teacher{content}{grouping}{r} {whereclause} ORDER BY SurveyYear";

			SqlCommand cmd = new SqlCommand(sql);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 50).Value = (object)filterCode ?? DBNull.Value;
			return cmd;
		}
		#endregion

		#region Nation
		/// <summary>
		/// Teacher data consolidated for the nation
		/// </summary>
		///  
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <returns>The SQL query string to execute</returns>
		private string NationTemplate(string content)
		{
			return createTemplate(content, "Nation", "", "''");
		}

		/// <summary>
		/// Teacher data consolidated for the nation in report format 
		/// (not implemented; no support in warehouse)
		/// </summary>
		///  
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <returns></returns>
		//private string NationReportTemplate (string content)
		//{			
		//}

		/// <summary>
		/// Teacher data consolidated for the nation
		/// </summary>
		///  
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <returns>SqlCommand to produce teacher data with Nation grouping</returns>
		internal SqlCommand NationCmd(string content)
		{
			//string cmdTemplate = (report ? NationReportTemplate(content) : NationTemplate(content));
			string cmdTemplate = NationTemplate(content);
			return CreateCommand2(cmdTemplate, null);
		}
		#endregion

		#region School
		/// TODO - The SQL command for School grouping currently only supports TeacherActivity 
		/// (i.e. the first teacher data that was done for Excel export). Make this support other Teacher data as well.
		private string SchoolTemplate(string content)
		{
			string query = @"			
					SELECT [SurveyYear]      
						  , S.schNo SchoolNo
						  , DSS.[School Name] [School]
						  , {0}
						  , SUM([Staff]) [Staff]
						  , SUM([StaffQ]) [StaffQ]
						  , SUM([StaffC]) [StaffC]
						  , SUM([StaffQC]) [StaffQC]
						  , SUM([StaffW]) [StaffW]
						  , SUM([StaffQ_W]) [StaffQ_W]
						  , SUM([StaffC_W]) [StaffC_W]
						  , SUM([StaffQC_W]) [StaffQC_W]
						  , SUM([Enrol]) [Enrol]
					FROM [warehouse].Teacher{0}School S
					LEFT JOIN warehouse.DimensionSchoolSurvey DSS ON S.SurveyDimensionID = DSS.[Survey ID]
					WHERE (schNo = @Selected or @Selected is null)
							AND ( SurveyYear = @SelectedYear or @SelectedYear is null)
					GROUP BY [SurveyYear]
						  , S.schNo
						  , DSS.[School Name]
						  , {0}
					HAVING sum([Staff]) is not null or sum([StaffQ]) is not null or sum([StaffC]) is not null or 
						sum([StaffQC]) is not null or sum([StaffW]) is not null or sum([StaffQ_W]) is not null or 
						sum([StaffC_W]) is not null or sum([StaffQC_W]) is not null or sum([Enrol]) is not null
					ORDER BY [SurveyYear]
						  , S.schNo
						  , DSS.[School Name]
						  , {0}
			";
			return string.Format(query, content);

		}
		/// <summary>
		/// Teachers data consolidated to school in 'cube' format
		/// </summary>
		/// <returns></returns>
		//private string SchoolReportTemplate
		//{
		//}

		internal SqlCommand SchoolCmd(string content, string school)
		{
			//string cmdTemplate = (report ? SchoolReportTemplate : SchoolTemplate);
			string cmdTemplate = SchoolTemplate(content);
			return CreateCommand2(cmdTemplate, school);
		}
		#endregion

		#region District
		private string DistrictTemplate(string content)
		{
			return createTemplate(content, "District");
		}
		internal SqlCommand DistrictCmd(string content, string district)
		{
			//string cmdTemplate = (report ? DistrictReportTemplate : DistrictTemplate);
			string cmdTemplate = DistrictTemplate(content);
			return CreateCommand2(cmdTemplate, district);
		}
		#endregion

		#region Authority
		private string AuthorityTemplate(string content)
		{
			string fields = @", AuthorityCode
					, Authority
					, AuthorityTypeCode
					, Authoritytype
					, AuthorityGovtCode
					, AuthorityGovt
					";
			return createTemplate(content, "Authority", fields);

		}
		internal SqlCommand AuthorityCmd(string content, string authority)
		{
			//string cmdTemplate = (report ? AuthorityReportTemplate(content) : AuthorityTemplate(content));
			string cmdTemplate = AuthorityTemplate(content);
			return CreateCommand2(cmdTemplate, authority);
		}
		#endregion

		#region Island
		private string IslandTemplate(string content)
		{
			string fields = @", IslandCode
				, Island
				, DistrictCode
				, District
				, RegionCode
				, Region";

			return createTemplate(content, "Island", fields);
		}
		internal SqlCommand IslandCmd(string content, string region)
		{
			//string cmdTemplate = (report ? IslandReportTemplate(content) : IslandTemplate(content));
			string cmdTemplate = IslandTemplate(content);
			return CreateCommand2(cmdTemplate, region);
		}
		#endregion

		#region Region
		private string RegionTemplate(string content)
		{
			return createTemplate(content, "Region");
		}
		internal SqlCommand RegionCmd(string content, string region)
		{
			//string cmdTemplate = (report ? RegionReportTemplate(content) : RegionTemplate(content));
			string cmdTemplate = RegionTemplate(content);
			return CreateCommand2(cmdTemplate, region);
		}
		#endregion

		#region SchoolType
		private string SchoolTypeTemplate(string content)
		{
			return createTemplate(content, "SchoolType");
		}
		internal SqlCommand SchoolTypeCmd(string content, string schoolType)
		{
			//string cmdTemplate = (report ? SchoolTypeReportTemplate(content) : SchoolTypeTemplate(content));
			string cmdTemplate = SchoolTypeTemplate(content);
			return CreateCommand2(cmdTemplate, schoolType);
		}
		#endregion

		#region Table
		/// <summary>
		/// 
		/// </summary>
		internal SqlCommand TableCmd()
		{
			return null;
		}
		#endregion

		#region Extended version
		/// <summary>
		/// Teachers data extended data version. This is he biggest data export containing all disaggregations
		/// and is usually he one preferred by users as they can do their own analysis.
		/// </summary>
		/// 
		/// <param name="content">A string defining the content of data (e.g. TeacherActivity, TeacherJob, TeacherHousing)</param>
		/// <returns>The SQL query string to execute to retrieve the data.</returns>
		private string ExtendedTemplate(string content)
		{
			string query = @"SELECT * FROM [warehouse].Teacher{0}X";
			return string.Format(query, content);
		}
		/// <summary>
		/// Teachers data consolidated to school in 'cube' format
		/// </summary>
		/// <returns></returns>
		//private string ExtendedReportTemplate
		//{
		//}

		internal SqlCommand ExtendedCmd(string content)
		{
			//string cmdTemplate = (report ? ExtendedReportTemplate(content) : ExtendedTemplate(content));
			string cmdTemplate = ExtendedTemplate(content);
			return CreateCommand(cmdTemplate);
		}
		#endregion

		#region SQL command generation (original ones mostly geared towards the ones used in dashboards and elsewhere)
		SqlCommand CreateCommand(string cmdTemplate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = String.Format(cmdTemplate, content);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

		SqlCommand CreateCommand(string cmdTemplate, string selected)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = String.Format(cmdTemplate, content);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 10).Value = ((object)selected ?? DBNull.Value);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

		string baseTemplate()
		{
			string t = string.Empty;
			switch (grouping)
			{
				case "Nation":
					t = @"Select * from warehouse.{0}{1}{3}
						ORDER BY SurveyYear {2}
						";
					break;

				case "Table":
					t = @"Select * from warehouse.{0}{1}{3}
						ORDER BY SurveyYear, DistrictCode, AuthorityCode, SchoolTypeCode {2}
						";
					break;
				case "School":
					t = @"Select * from warehouse.{0}{1}{3}
						WHERE schNo = @Selected or @selected is null
						ORDER BY SurveyYear, SchNo {2}
						";
					break;

				default:
					t = @"Select * from warehouse.{0}{1}{3}
					WHERE {1}Code = @Selected or @selected is null
					ORDER BY SurveyYear, {1}Code {2}";
					break;
			}

			// I am not sure what this accreditationbystandard is doing here...
			string extraSort = String.Empty;
			switch (content.ToLower())
			{
				case "accreditationbystandard":
					extraSort = ", Standard ";
					break;
				case "accreditation":
					extraSort = report ? "" : ", InspectionResult ";       // in report mode we don;t have inspectionresult
					break;

			}
			return string.Format(t, content, grouping, extraSort, report ? "R" : "");
		}

		/// <summary>
		/// Not currently used. 
		/// The "base template" SQL command construction is done in <see cref="WarehouseTeacherActivityCommands.GeneralCmd"/>directly
		/// with a similar (if not identical result).
		/// </summary>
		internal SqlCommand BaseCommand(string selected)
		{
			return CreateCommand(baseTemplate(), selected);
		}
		#endregion

		#region Alternative SQL command generations (more geared towards the newer RESTful APIs that can also export to Excel)
		/// <summary>
		/// Creates commands and templates as in WarehouseEnrolCmds.cs (specifically for the purpose of 
		/// Excel workbook exports.
		/// </summary>
		/// 
		/// <param name="cmdTemplate">A string based SQL query</param>
		/// <param name="selected">A selected entity (e.g. a school CHK100)</param>
		/// <returns></returns>
		SqlCommand CreateCommand2(string cmdTemplate, string selected)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			string disaggs = (byActivity ? ", Activity" : "");
			cmd.CommandText = String.Format(cmdTemplate, disaggs);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 10).Value = ((object)selected ?? DBNull.Value);
			cmd.Parameters.Add("@SelectedYear", SqlDbType.Int).Value = ((object)selectedYear ?? DBNull.Value);

			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

		/// <summary>
		/// Creates commands and templates as in WarehouseEnrolCmds.cs (specifically for the purpose of 
		/// Excel workbook exports.
		/// </summary>
		/// 
		/// <param name="cmdTemplate">A string based SQL query</param>
		/// <returns></returns>
		SqlCommand CreateCommand2(string cmdTemplate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			string disaggs = (byActivity ? ", Activity" : "");
			cmd.CommandText = String.Format(cmdTemplate, disaggs);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

		/// <summary>
		/// Creates an SQL template for getting data from the warehouse. This is meant for aggregated 
		/// versions of the warehouse.Teacher{Something} tables and views. It is for the flexible RESTful API.
		/// 
		/// </summary>
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <param name="baseAggregator">The disaggregation (e.g. Nation, District, Islands)</param>
		/// <returns>The SQL template to execute</returns>
		string createTemplate(string content, string baseAggregator)
		{
			string groupingFields = String.Format(@", {0}Code, {0}", baseAggregator);
			return createTemplate(content, baseAggregator, groupingFields);
		}
		/// <summary>
		/// Creates an SQL template for getting data from the warehouse. This is meant for aggregated 
		/// versions of the warehouse.Teacher{Something} tables and views. It is for the flexible RESTful API
		/// that can take grouping fields 
		/// 
		/// </summary>
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <param name="baseAggregator">The disaggregation (e.g. Nation, District, Islands)</param>
		/// <param name="groupingFields">Part fields in the SQL SELECT clause (e.g. ", District, DistrictCode")</param>
		/// <returns>The SQL template to execute</returns>
		string createTemplate(string content, string baseAggregator, string groupingFields)
		{
			string selectionField = baseAggregator + "Code";
			return createTemplate(content, baseAggregator, groupingFields, selectionField);
		}
		/// <summary>
		/// Creates an SQL template for getting data from the warehouse. This is meant for aggregated 
		/// versions of the warehouse.Teacher{Something} tables and views. It is for the flexible RESTful API
		/// that can take grouping fields and selected entity.
		/// 
		/// </summary>
		/// <param name="content">The data content (e.g. The string "Activity" for TeacherActivity, the string "Job" for TeacherJob)</param>
		/// <param name="baseAggregator">The disaggregation (e.g. Nation, District, Islands)</param>
		/// <param name="groupingFields">Part fields in the SQL SELECT clause (e.g. ", District, DistrictCode")</param>
		/// <param name="selectionField">(e.g. SchoolCode, DistrictCode, IslandCode)</param>
		/// <returns>The SQL template to execute</returns>
		string createTemplate(string content, string baseAggregator, string groupingFields, string selectionField)
		{
			string baseView = "Teacher" + content + baseAggregator;
			string templateBase;

			string teacherActivityTemplateBase = @"
				SELECT [SurveyYear]      
					  {0}
					  {{0}}
					  ,SUM([Staff]) [Staff]
					  ,SUM([StaffQ]) [StaffQ]
					  ,SUM([StaffC]) [StaffC]
					  ,SUM([StaffQC]) [StaffQC]
					  ,SUM([StaffW]) [StaffW]
					  ,SUM([StaffQ_W]) [StaffQ_W]
					  ,SUM([StaffC_W]) [StaffC_W]
					  ,SUM([StaffQC_W]) [StaffQC_W]
					  ,SUM([Enrol]) [Enrol]
				FROM [warehouse].{1}
				WHERE ({2} = @Selected or @Selected is null)
						AND ( SurveyYear = @SelectedYear or @SelectedYear is null)
				GROUP BY [SurveyYear]
					  {0}
					  {{0}}
				HAVING sum([Staff]) is not null or sum([StaffQ]) is not null or sum([StaffC]) is not null or 
					sum([StaffQC]) is not null or sum([StaffW]) is not null or sum([StaffQ_W]) is not null or 
					sum([StaffC_W]) is not null or sum([StaffQC_W]) is not null or sum([Enrol]) is not null
				ORDER BY [SurveyYear]
					  {0}
					  {{0}}
			";

			switch (content)
			{
				case "Activity":
					templateBase = teacherActivityTemplateBase;
					break;
				// Not implemented other teacher data yet...
				//case "Job":
				//	templateBase = teacherJobTemplateBase;
				//	break;
				//case "EdLevel":
				//	templateBase = teacherEdLevelTemplateBase;
				//	break;
				default:
					templateBase = @"SELECT * FROM warehouse.Teacher{1}";
					break;
			}

			return String.Format(templateBase, groupingFields, baseView, selectionField);
		}
		#endregion

		#region Filter
		// I am not sure what this School Accreditation stuff is doing here...
		public static SqlCommand FilterCmd(SchoolAccreditationFilter fltr)
		{
			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "warehouse.AccreditationFilterPaged";

			cmd.Parameters.Add("@ColumnSet", SqlDbType.Int).Value = (object)(fltr.ColumnSet) ?? DBNull.Value;
			cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = (object)(fltr.PageSize) ?? DBNull.Value;
			cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = (object)(fltr.PageNo) ?? DBNull.Value;
			cmd.Parameters.Add("@SortColumn", SqlDbType.NVarChar, 128).Value = (object)(fltr.SortColumn) ?? DBNull.Value;
			cmd.Parameters.Add("@SortDir", SqlDbType.Int).Value = (object)(fltr.SortDesc) ?? DBNull.Value;

			cmd.Parameters.Add("@SAID", SqlDbType.Int).Value = (object)fltr.SAID ?? DBNull.Value;
			cmd.Parameters.Add("@School", SqlDbType.NVarChar, 50).Value = (object)fltr.School ?? DBNull.Value;
			cmd.Parameters.Add("@SchoolNo", SqlDbType.NVarChar, 50).Value = (object)fltr.SchoolNo ?? DBNull.Value;
			cmd.Parameters.Add("@InspYear", SqlDbType.NVarChar, 50).Value = (object)fltr.InspYear ?? DBNull.Value;
			cmd.Parameters.Add("@InspResult", SqlDbType.NVarChar, 50).Value = (object)fltr.InspResult ?? DBNull.Value;
			cmd.Parameters.Add("@District", SqlDbType.NVarChar, 10).Value = (object)fltr.District ?? DBNull.Value;
			cmd.Parameters.Add("@Authority", SqlDbType.NVarChar, 10).Value = (object)fltr.Authority ?? DBNull.Value;
			cmd.Parameters.Add("@AuthorityGroup", SqlDbType.NVarChar, 10).Value = (object)fltr.AuthorityGroup ?? DBNull.Value;
			cmd.Parameters.Add("@SchoolType", SqlDbType.NVarChar, 10).Value = (object)fltr.SchoolType ?? DBNull.Value;

			cmd.Parameters.Add("@FilterColumn", SqlDbType.NVarChar, 10).Value = (object)fltr.FilterColumn ?? DBNull.Value;
			cmd.Parameters.Add("@FilterValue", SqlDbType.Int).Value = (object)fltr.FilterValue ?? DBNull.Value;
			cmd.Parameters.Add("@BestUpTo", SqlDbType.Int).Value = (object)fltr.BestUpTo ?? DBNull.Value;



			return cmd;
		}
		#endregion

	}
}
