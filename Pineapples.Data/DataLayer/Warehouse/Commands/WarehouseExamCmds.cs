﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	/// <summary>
	/// Utility class responsible for constructing the SQL statements
	/// coresponding to the exam methods of the warehouse
	/// </summary>
	class WarehouseExamCommands
	{
		bool reportFormat;
		bool report;
		internal WarehouseExamCommands()
		{
			// null constructor for now
		}
		internal WarehouseExamCommands(bool reportFormat, bool report)
		{
			this.reportFormat = reportFormat;
			this.report = report;
		}


		string DistrictTemplate()
		{
			return @"Select *
, StateID DistrictCode
, State District
FROM warehouse.ExamStateResults{0}
WHERE StateID = @selected or @Selected is null";
		}

		string DistrictReportTemplate()
		{
			return @"Select *
, StateID DistrictCode
, State District
FROM warehouse.ExamStateResultsR{0}
WHERE StateID = @selected or @Selected is null";
		}

		internal SqlCommand DistrictCmd(string district)
		{
			string cmdTemplate;
			if (this.report)
			{
				cmdTemplate = String.Format(DistrictReportTemplate(), (this.reportFormat ? "X" : ""));
			} else
			{
				cmdTemplate = String.Format(DistrictTemplate(), (this.reportFormat ? "X" : ""));
			}
			return CreateCommand(cmdTemplate, district);
		}

		string SchoolTemplate()
		{
			return @"Select *
FROM warehouse.ExamSchoolResults{0}
WHERE schNo = @selected or @Selected is null";
		}

		string SchoolReportTemplate()
		{
			return @"Select *
FROM warehouse.ExamSchoolResultsR{0}
WHERE schNo = @selected or @Selected is null";
		}

		internal SqlCommand SchoolCmd(string schoolNo)
		{
			string cmdTemplate;
			if (this.report)
			{
				cmdTemplate = String.Format(SchoolReportTemplate(), (this.reportFormat ? "X" : ""));
			}
			else {
				cmdTemplate = String.Format(SchoolTemplate(), (this.reportFormat ? "X" : ""));
			}
			return CreateCommand(cmdTemplate, schoolNo);
		}
		SqlCommand CreateCommand(string cmdTemplate, string selected)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = cmdTemplate;
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 10).Value = ((object)selected ?? DBNull.Value);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}
	}
}
