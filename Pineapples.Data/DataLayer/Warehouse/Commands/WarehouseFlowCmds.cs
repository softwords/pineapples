﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	/// <summary>
	/// Utility class responsible for constructing the SQL statements
	/// coresponding to the enrolment methods of the warehouse
	/// These sql statements follow a common pattern, based on views
	/// warehouse.Enrol<XXXX> , warehouse.Enrol<XXXX>R
	/// Therefore, the Sql is assembed by sustitution into templates
	/// </summary>
	class WarehouseFlowCommands
	{
		internal bool reportFormat { get; set; }
		internal bool asPerc { get; set; }

		internal WarehouseFlowCommands(bool reportFormat, bool asPerc)
		{
			this.reportFormat = reportFormat;
			this.asPerc = asPerc;
		}
		#region School
		/// <summary>
		/// Enrolments consolidated to district in 'cube' format
		/// </summary>
		/// <returns></returns>
		private string SchoolTemplate
		{

			get
			{
				return createTemplate(", SchoolNo, [School Name]", "FlowSchool","SchoolNo");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string SchoolReportTemplate
		{

			get
			{
				return createReportTemplate(", SchoolNo, [School Name]", "FlowSchoolR", "SchoolNo");
			}
		}
		internal SqlCommand SchoolCmd(string district)
		{
			string cmdTemplate = (reportFormat ? SchoolReportTemplate : SchoolTemplate);
			return CreateCommand(cmdTemplate, district);
		}
		#endregion


		#region District
		/// <summary>
		/// Enrolments consolidated to district in 'cube' format
		/// </summary>
		/// <returns></returns>
		private string DistrictTemplate
		{
			get
			{

				return createTemplate("District");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string DistrictReportTemplate
		{

			get
			{
				return createReportTemplate("District");
			}
		}
		internal SqlCommand DistrictCmd(string district)
		{
			string cmdTemplate = (reportFormat ? DistrictReportTemplate : DistrictTemplate);
			return CreateCommand(cmdTemplate, district);
		}
		#endregion

		#region Nation
		/// <summary>
		/// Enrolments consolidated to district in 'cube' format
		/// </summary>
		/// <returns></returns>
		private string NationTemplate
		{
			get
			{

				return createTemplate("", "FlowNation","''");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string NationReportTemplate
		{

			get
			{
				return createReportTemplate("", "FlowNationR","''");
			}
		}
		internal SqlCommand NationCmd()
		{
			string cmdTemplate = (reportFormat ? NationReportTemplate : NationTemplate);
			return CreateCommand(cmdTemplate, null);
		}
		#endregion
		SqlCommand CreateCommand(string cmdTemplate, string selected)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			string scale = (asPerc ? "100 *" : ""); 
			cmd.CommandText = String.Format(cmdTemplate, scale);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 10).Value = ((object)selected ?? DBNull.Value);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}
		SqlCommand CreateCommand(string cmdTemplate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			string scale = (asPerc ? "100 *" : "");
			cmd.CommandText = String.Format(cmdTemplate, scale);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

		string createTemplate(string baseAggregator)
		{
			string groupingFields = String.Format(@", {0}Code, {0}", baseAggregator);
			return createTemplate(groupingFields, baseAggregator);
		}
		string createTemplate(string groupingFields, string baseAggregator)
		{
			string baseView = "Flow" + baseAggregator;
			string selectionField = baseAggregator + "Code";
			return createTemplate(groupingFields, baseView, selectionField);
		}

		string createTemplate(string groupingFields, string baseView, string selectionField)
		{
			string templateBase = @"
			Select 
			SurveyYear
			, YearOfEd
			{0}
			, GenderCode
			, Enrol
			, Rep
			, RepNY
			, EnrolNYNextLevel
			, RepNYNextLevel
			, {{0}} RepeatRate RepeatRate
			, {{0}} PromoteRate PromoteRate
			, {{0}} DropoutRate DropoutRate
			, {{0}} SurvivalRate SurvivalRate

			From Warehouse.{1}
			WHERE {2} = @Selected or @Selected is null
			ORDER BY
			SurveyYear
			, YearOfEd
			{0}
			, GenderCode";
			return String.Format(templateBase, groupingFields, baseView, selectionField);
		}

		string createReportTemplate(string baseAggregator)
		{
			string groupingFields = String.Format(@", {0}Code, {0}", baseAggregator);
			return createReportTemplate(groupingFields, baseAggregator);
		}

		string createReportTemplate(string groupingFields, string baseAggregator)
		{
			string baseView = "Flow" + baseAggregator + "R";       // e.g. EnrolIslandR
			string selectionField = baseAggregator + "Code";        // e.g. IslandCode
			return createReportTemplate(groupingFields, baseView, selectionField);
		}

		string createReportTemplate(string groupingFields, string baseView, string selectionField)
		{
			string templateBase = @"
			Select 
			SurveyYear
			, YearOfEd
			{0}
			-- all fields are duplicated by gender
			, Enrol
			, Rep
			, RepNY
			, EnrolNYNextLevel
			, RepNYNextLevel
			, {{0}} RepeatRate RepeatRate
			, {{0}} PromoteRate PromoteRate
			, {{0}} DropoutRate DropoutRate
			, {{0}} SurvivalRate SurvivalRate

			, EnrolM
			, RepM
			, RepNYM
			, EnrolNYNextLevelM
			, RepNYNextLevelM
			, {{0}} RepeatRateM RepeatRateM
			, {{0}} PromoteRateM PromoteRateM
			, {{0}} DropoutRateM DropoutRateM
			, {{0}} SurvivalRateM SurvivalRateM


			, EnrolF
			, RepF
			, RepNYF
			, EnrolNYNextLevelF
			, RepNYNextLevelF
			, {{0}} RepeatRateF RepeatRateF
			, {{0}} PromoteRateF PromoteRateF
			, {{0}} DropoutRateF DropoutRateF
			, {{0}} SurvivalRateF SurvivalRateF

			From Warehouse.{1}
			WHERE {2} = @Selected or @Selected is null
			ORDER BY
			SurveyYear
			, YearOfEd
			{0}
			";
			return String.Format(templateBase, groupingFields, baseView, selectionField);
		}
	}
}