﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	/// <summary>
	/// Utility class responsible for constructing the SQL statements
	/// coresponding to the exam methods of the warehouse
	/// </summary>
	class WarehouseSpEdCommands
	{
		bool reportFormat;
		internal WarehouseSpEdCommands()
		{
			// null constructor for now
		}
		internal WarehouseSpEdCommands(bool reportFormat)
		{
			this.reportFormat = reportFormat;
		}

		string Template()
		{
			return @"Select * 
from warehouse.SpecialEd{0}
";
		}

		internal SqlCommand SpecialEdCmd()
		{
			string cmdTemplate = String.Format(Template(), (this.reportFormat ? "R" : ""));
			return CreateCommand(cmdTemplate);

		}

		SqlCommand CreateCommand(string cmdTemplate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = cmdTemplate;
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

		SqlCommand CreateCommand(string cmdTemplate, string selected)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = cmdTemplate;
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 10).Value = ((object)selected ?? DBNull.Value);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

	}
}
