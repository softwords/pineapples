﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	class WarehouseDisabilityCommands
	{
		internal bool reportFormat { get; set; }
		internal bool byAge { get; set; }
		internal bool byClassLevel { get; set; }

		internal WarehouseDisabilityCommands(bool reportFormat, bool byAge, bool byClassLevel)
		{
			this.reportFormat = reportFormat;
			this.byAge = byAge;
			this.byClassLevel = byClassLevel;
		}

		#region School
		/// <summary>
		/// Enrolments consolidated to district in 'cube' format
		/// </summary>
		/// <returns></returns>
		private string SchoolTemplate
		{

			get
			{
				return @"
			select 
				SurveyYear
				, E.schNo SchoolNo
				, DSS.[School Name]
				{0}
				, GenderCode
				, DisCode DisabilityCode
				, Disability
				, sum(Disab) Disab

			  from warehouse.Disability E
				LEFT JOIN warehouse.DimensionSchoolSurvey DSS
				ON E.SurveyDimensionID = DSS.[Survey ID]
				LEFT JOIN Districts D
					ON @selected = D.dID
			  where ( @selected is null
					OR E.schNo = @Selected 
					OR D.dID = DSS.districtCode
			  )
			  GROUP BY
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
				, GenderCode
				, DisCode
				, Disability

			  ORDER BY 
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
				, GenderCode
				, DisCode
			";
			}
		}
		private string SchoolReportTemplate
		{

			get
			{
				return @"
			select 
				SurveyYear
				, E.schNo SchoolNo
				, DSS.[School Name]
				{0}
				, GenderCode
				, DisCode DisabilityCode
				, Disability
				, sum(Disab) Disab

			  from warehouse.Disability E
				LEFT JOIN warehouse.DimensionSchoolSurvey DSS
				ON E.SurveyDimensionID = DSS.[Survey ID]
				LEFT JOIN Districts D
					ON @selected = D.dID
			  where ( @selected is null
					OR E.schNo = @Selected 
					OR D.dID = DSS.districtCode
			  )
			  GROUP BY
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
				, GenderCode
				, DisCode
				, Disability

			  ORDER BY 
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
				, GenderCode
				, DisCode
			";
			}
		}

		internal SqlCommand SchoolCmd(string district)
		{
			string cmdTemplate = (reportFormat ? SchoolReportTemplate : SchoolTemplate);
			return CreateCommand(cmdTemplate, district);
		}

		#endregion
		SqlCommand CreateCommand(string cmdTemplate, string selected)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			string disaggs = (byClassLevel ? ", ClassLevel" : "") + (byAge ? ", Age" : "");
			cmd.CommandText = String.Format(cmdTemplate, disaggs);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 10).Value = ((object)selected ?? DBNull.Value);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}
	}
}
