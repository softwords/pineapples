﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	/// <summary>
	/// Utility class responsible for constructing the SQL statements
	/// coresponding to the enrolment methods of the warehouse
	/// These sql statements follow a common pattern, based on views
	/// warehouse.Enrol<XXXX> , warehouse.Enrol<XXXX>R
	/// Therefore, the Sql is assembed by sustitution into templates
	/// </summary>
	class WarehouseEnrolCommands
	{
		internal bool reportFormat { get; set; }
		internal bool byAge { get; set; }
		internal bool byClassLevel { get; set; }

		internal int? selectedYear { get;set; }

		internal string selectedClassLevel { get; set; }


		internal WarehouseEnrolCommands( bool reportFormat, bool byAge, bool byClassLevel,
			int? selectedYear = null, string selectedClassLevel = null)
		{
			this.reportFormat = reportFormat;
			this.byAge = byAge;
			this.selectedYear = selectedYear;
			this.selectedClassLevel = selectedClassLevel;
			this.byClassLevel = (selectedClassLevel == null)?byClassLevel: true;


		}

		#region School
		/// <summary>
		/// Enrolments consolidated to district in 'cube' format
		/// </summary>
		/// <returns></returns>
		private string SchoolTemplate
		{
			
			get
			{
				return @"
			select 
				SurveyYear
				, E.schNo SchoolNo
				, DSS.[School Name]
				{0}
				, GenderCode
				, sum(Enrol) Enrol
				, sum(Rep) Rep

			  from warehouse.enrol E
				LEFT JOIN warehouse.DimensionSchoolSurvey DSS
				ON E.SurveyDimensionID = DSS.[Survey ID]
				LEFT JOIN Districts D
					ON @selected = D.dID
			  where ( @selected is null
					OR E.schNo = @Selected 
					OR D.dID = DSS.districtCode
			  ) 
				AND	(SurveyYear= @SelectedYear OR @selectedYear is null)
				AND	(ClassLevel= @SelectedClassLevel OR @selectedClassLevel is null)
			  GROUP BY
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
				, GenderCode

			  ORDER BY 
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
				, GenderCode
			";
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string SchoolReportTemplate
		{

			get
			{
				return @"
			select 
				SurveyYear
				, E.schNo SchoolNo
				, DSS.[School Name]
				{0}
				, sum(EnrolF) EnrolF
				, sum(EnrolM) EnrolM
				, sum(Enrol) Enrol
				, sum(RepF) RepF
				, sum(RepM) RepM
				, sum(Rep) Rep

			  from warehouse.enrolR E
				LEFT JOIN warehouse.DimensionSchoolSurvey DSS
				ON E.SurveyDimensionID = DSS.[Survey ID]
				LEFT JOIN Districts D
					ON @selected = D.dID
			  where ( @selected is null
					OR E.schNo = @Selected 
					OR D.dID = DSS.districtCode
			  )
				AND	(SurveyYear= @SelectedYear OR @selectedYear is null)
				AND	(ClassLevel= @SelectedClassLevel OR @selectedClassLevel is null)
			  GROUP BY
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
			  HAVING sum(Enrol) is not null OR sum(rep) is not null
			  ORDER BY 
				SurveyYear
  				, E.schNo
				, DSS.[School Name]
				{0}
			";
			}
		}
		internal SqlCommand SchoolCmd(string schoolNo)
		{
			string cmdTemplate = (reportFormat ? SchoolReportTemplate : SchoolTemplate);
			return CreateCommand(cmdTemplate, schoolNo);
		}
		#endregion

		#region District
		/// <summary>
		/// Enrolments consolidated to district in 'cube' format
		/// </summary>
		/// <returns></returns>
		private string DistrictTemplate
		{
			get {

				return createTemplate("District");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string DistrictReportTemplate
		{

			get
			{
				return createReportTemplate("District");
			}
		}
		internal SqlCommand DistrictCmd(string district)
		{
			string cmdTemplate = (reportFormat ? DistrictReportTemplate : DistrictTemplate);
			return CreateCommand(cmdTemplate, district);
		}
		#endregion

		#region Authority
		private string AuthorityTemplate
		{
			get
			{
				string fields = @", AuthorityCode
					, Authority
					, AuthorityTypeCode
					, Authoritytype
					, AuthorityGroupCode
					, AuthorityGroup
					";
				return createTemplate(fields, "Authority");
			}
		}
		/// <summary>
		/// Enrolments consolidated to Authority in report format
		/// </summary>
		/// <returns></returns>
		private string AuthorityReportTemplate
		{

			get
			{
				string fields = @", AuthorityCode
					, Authority
					, AuthorityTypeCode
					, Authoritytype
					, AuthorityGroupCode
					, AuthorityGroup
					";
				return createReportTemplate(fields, "Authority");
			}
		}
		internal SqlCommand AuthorityCmd(string authority)
		{
			string cmdTemplate = (reportFormat ? AuthorityReportTemplate : AuthorityTemplate);
			return CreateCommand(cmdTemplate, authority);
		}

		#endregion

		#region ElectorateL
		private string ElectorateLTemplate
		{
			get
			{
				string fields = @"
				, DistrictCode
				, District
				, LocalElectorateNo
				, [Local Electorate]
				";
				return createTemplate(fields, "EnrolLocalElectorate","LocalElectorateNo");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string ElectorateLReportTemplate
		{
			get { 
			string fields = @"
			, DistrictCode
			, District
			, LocalElectorateNo
			, [Local Electorate]
			";
				return createReportTemplate(fields, "EnrolLocalElectorateR", "LocalElectorateNo");
			}
		}
		internal SqlCommand ElectorateLCmd(string electorate)
		{
			string cmdTemplate = (reportFormat ? ElectorateLReportTemplate : ElectorateLTemplate);
			return CreateCommand(cmdTemplate, electorate);
		}
		#endregion

		#region ElectorateN
		private string ElectorateNTemplate
		{
			get
			{
				string fields = @", NationalElectorateNo, [National Electorate]";
				return createTemplate(fields, "EnrolNationalElectorate", "NationalElectorateNo");
			}

		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string ElectorateNReportTemplate
		{
			get
			{
				string fields = @", NationalElectorateNo, [National Electorate]";
				return createReportTemplate(fields, "EnrolNationalElectorateR", "NationalElectorateNo");
			}

		}
		internal SqlCommand ElectorateNCmd(string electorate)
		{
			string cmdTemplate = (reportFormat ? ElectorateNReportTemplate : ElectorateNTemplate);
			return CreateCommand(cmdTemplate, electorate);
		}
		#endregion

		#region Island
		private string IslandTemplate
		{
			get
			{
				string fields = @", IslandCode
				, Island
				, DistrictCode
				, District
				, RegionCode
				, Region";

				return createTemplate(fields, "Island");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string IslandReportTemplate
		{
			get
			{
				string fields = @", IslandCode
				, Island
				, DistrictCode
				, District
				, RegionCode
				, Region";

				return createReportTemplate(fields, "Island");
			}
		}
		internal SqlCommand IslandCmd(string region)
		{
			string cmdTemplate = (reportFormat ? IslandReportTemplate : IslandTemplate);
			return CreateCommand(cmdTemplate, region);
		}

		#endregion Island

		#region Region
		private string RegionTemplate
		{
			get
			{
					return createTemplate("Region");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string RegionReportTemplate
		{
			get
			{
				return createReportTemplate("Region");
			}
		}
		internal SqlCommand RegionCmd(string region)
		{
			string cmdTemplate = (reportFormat ? RegionReportTemplate : RegionTemplate);
			return CreateCommand(cmdTemplate, region);
		}
		#endregion

		#region SchoolType
		private string SchoolTypeTemplate
		{
			get
			{
				return createTemplate("SchoolType");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string SchoolTypeReportTemplate
		{
			get
			{
				return createReportTemplate("SchoolType");
			}
		}
		internal SqlCommand SchoolTypeCmd(string schoolType)
		{
			string cmdTemplate = (reportFormat ? SchoolTypeReportTemplate : SchoolTypeTemplate);
			return CreateCommand(cmdTemplate, schoolType);
		}
		#endregion

		#region Nation
		private string NationTemplate
		{
			get
			{
				return createTemplate("", "EnrolNation", "''");
			}
		}
		/// <summary>
		/// Enrolments consolidated to district in report format
		/// </summary>
		/// <returns></returns>
		private string NationReportTemplate
		{
			get
			{
			return @"
			Select 
			SurveyYear
			{0}
			, sum(EnrolM) EnrolM
			, sum(EnrolF) EnrolF
			, sum(Enrol) Enrol

			From Warehouse.EnrolNationR
			GROUP BY 
			SurveyYear
			{0}
			ORDER BY
			SurveyYear
			{0}
			";
			}
		}
		internal SqlCommand NationCmd()
		{
			string cmdTemplate = (reportFormat ? NationReportTemplate : NationTemplate);
			return CreateCommand(cmdTemplate, null);
		}
		#endregion

		#region ISCED (uis survey audit)
		internal SqlCommand IscedCmd(int year)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Add("year",SqlDbType.Int).Value = year;
			cmd.CommandText = "Select * from warehouse.EnrolISCEDAudit(@year)";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}
		#endregion

		SqlCommand CreateCommand(string cmdTemplate, string selected)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			string disaggs = (byClassLevel ? ", ClassLevel" : "") + (byAge ? ", Age" : "");
			cmd.CommandText = String.Format(cmdTemplate, disaggs);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 10).Value = ((object)selected ?? DBNull.Value);
			cmd.Parameters.Add("@SelectedYear", SqlDbType.Int).Value = ((object)this.selectedYear ?? DBNull.Value);
			cmd.Parameters.Add("@SelectedClassLevel", SqlDbType.NVarChar, 10).Value = ((object)this.selectedClassLevel ?? DBNull.Value);

			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}
		SqlCommand CreateCommand(string cmdTemplate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			string disaggs = (byClassLevel ? ", ClassLevel" : "") + (byAge ? ", Age" : "");
			cmd.CommandText = String.Format(cmdTemplate, disaggs);
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}

		string createTemplate( string baseAggregator)
		{
			string groupingFields = String.Format(@", {0}Code, {0}", baseAggregator);
			return createTemplate(groupingFields, baseAggregator);
		}
		string createTemplate(string groupingFields, string baseAggregator)
		{
			string baseView = "Enrol" + baseAggregator;
			string selectionField = baseAggregator + "Code";
			return createTemplate(groupingFields, baseView, selectionField);
		}

		string createTemplate(string groupingFields, string baseView, string selectionField)
		{
			string templateBase = @"
			Select 
			SurveyYear
			{0}
			{{0}}
			, GenderCode
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			-- , sum(PSA) PSA

			From Warehouse.{1}
			WHERE (	{2} = @Selected or @Selected is null)
			AND ( SurveyYear = @SelectedYear or @SelectedYear is null)
			AND ( ClassLevel = @SelectedClassLevel or @SelectedClassLevel is null)
			GROUP BY 
			SurveyYear
			{0}
			{{0}}
			, GenderCode
			HAVING sum(Enrol) is not null or sum(Rep) is not null
			ORDER BY
			SurveyYear
			{0}
			{{0}}
			, GenderCode
			";
			return String.Format(templateBase, groupingFields, baseView, selectionField);
		}

		string createReportTemplate(string baseAggregator)
		{
			string groupingFields = String.Format(@", {0}Code, {0}", baseAggregator);
			return createReportTemplate(groupingFields, baseAggregator);
		}

		string createReportTemplate(string groupingFields, string baseAggregator)
		{
			string baseView = "Enrol" + baseAggregator + "R";		// e.g. EnrolIslandR
			string selectionField = baseAggregator + "Code";		// e.g. IslandCode
			return createReportTemplate(groupingFields, baseView, selectionField);
		}

		string createReportTemplate(string groupingFields, string baseView, string selectionField)
		{
			string templateBase = @"
			Select 
			SurveyYear
			{0}
			{{0}}
			, sum(EnrolM) EnrolM
			, sum(EnrolF) EnrolF
			, sum(Enrol) Enrol

			From Warehouse.{1}
			WHERE (	{2} = @Selected or @Selected is null)
			AND ( SurveyYear = @SelectedYear or @SelectedYear is null)
			AND ( ClassLevel = @SelectedClassLevel or @SelectedClassLevel is null)
			GROUP BY 
			SurveyYear
			{0}
			{{0}}
			ORDER BY
			SurveyYear
			{0}
			{{0}}
			";
			return String.Format(templateBase, groupingFields, baseView, selectionField);
		}
	}
}
