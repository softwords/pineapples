﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	class WarehouseScholarshipCommands
	{
		internal SqlCommand ScholarshipRoundsCommand()
		{

			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = "Select * from warehouse.ScholarshipRounds";
			return cmd;
		}
		internal SqlCommand ScholarshipPaymentsCommand()
		{

			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = "Select * from warehouse.ScholarshipPayments";
			return cmd;
		}
	}
}
