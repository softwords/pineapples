﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;


namespace Pineapples.Data
{
	class WarehouseFinancialDataCommands
	{
		string content;
		string grouping;
		bool report;

		internal WarehouseFinancialDataCommands(string content)
		{
			this.content = content;
		}
		internal WarehouseFinancialDataCommands(string content, string grouping, bool? report)
		{
			this.content = content;
			this.grouping = grouping;
			this.report = report ?? false;
		}

		internal SqlCommand EdExpCommand(string groupValue)
		{

			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = template();
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 20).Value = ((object)groupValue ?? DBNull.Value);
			return cmd;
		}

		string template()
		{
			switch (this.grouping.ToLower())
			{
				case "nation":
					return "Select * from warehouse.ExpenditureNation";
				case "district":
					return @"Select * from warehouse.ExpenditureDistrict 
WHERE DistrictCode = @Selected or @selected is null";
				case "sector":
					return @"Select * from warehouse.ExpenditureSector 
WHERE SectorCode = @Selected or @selected is null";
				case "costcentre":
					return @"Select * from warehouse.ExpenditureCostCentre 
WHERE CostCentreCode = @Selected or @selected is null";
				default:
					return "Select * from warehouse.Expenditure";
			}
		}
	}
}
