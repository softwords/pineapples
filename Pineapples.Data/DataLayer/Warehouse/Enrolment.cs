﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		//================================================================================
		#region Enrol

		public IDataResult TableEnrol(bool reportFormat = false)
		{
			SqlCommand cmd = (reportFormat ? tableEnrolCmdReport() : tableEnrolCmd());
			return sqlExec(cmd);
		}
		public IDataResult TableEnrolX(bool reportFormat = false)
		{
			SqlCommand cmd = (reportFormat ? tableEnrolXCmdReport() : tableEnrolXCmd());
			return sqlExec(cmd);
		}

		public IDataResult EnrolNation(bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null)
		{
			//SqlCommand cmd = (reportFormat ? tableNationEnrolCmdReport(byAge, byClassLevel)
			//							: tableNationEnrolCmd(byAge, byClassLevel));

			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
											, selectedYear, selectedClassLevel)
											.NationCmd();

			return sqlExec(cmd);
		}

		// For individual School dashboards
		public IDataResult EnrolSchool(string selectedSchoolNo = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
											, selectedYear, selectedClassLevel)
											.SchoolCmd(selectedSchoolNo);
			return sqlExec(cmd);
		}

		public IDataResult EnrolDistrict(string selectedDistrict = null
										, bool reportFormat = false
										, bool byAge = false
										, bool byClassLevel = false
										, int? selectedYear = null
										, string selectedClassLevel = null)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
											, selectedYear, selectedClassLevel)
											.DistrictCmd(selectedDistrict);
			return sqlExec(cmd);
		}
		public IDataResult EnrolAuthority(string selectedAuthority = null
									, bool reportFormat = false
									, bool byAge = false
									, bool byClassLevel = false
									, int? selectedYear = null
									, string selectedClassLevel = null)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
									, selectedYear, selectedClassLevel)
									.AuthorityCmd(selectedAuthority);
			return sqlExec(cmd);
		}

		public IDataResult EnrolIsland(string selectedIsland = null
							, bool reportFormat = false
							, bool byAge = false
							, bool byClassLevel = false
							, int? selectedYear = null
							, string selectedClassLevel = null)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
									, selectedYear, selectedClassLevel)
									.IslandCmd(selectedIsland);
			return sqlExec(cmd);
		}

		public IDataResult EnrolRegion(string selectedRegion = null
							, bool reportFormat = false
							, bool byAge = false
							, bool byClassLevel = false
							, int? selectedYear = null
							, string selectedClassLevel = null)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
								, selectedYear, selectedClassLevel)
								.RegionCmd(selectedRegion);
			return sqlExec(cmd);
		}
		public IDataResult EnrolElectorateL(string selectedElectorate = null
							, bool reportFormat = false
							, bool byAge = false
							, bool byClassLevel = false
							, int? selectedYear = null
							, string selectedClassLevel = null)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
								, selectedYear, selectedClassLevel)
								.ElectorateLCmd(selectedElectorate);
			return sqlExec(cmd);
		}

		public IDataResult EnrolElectorateN(string selectedElectorate = null
					, bool reportFormat = false
					, bool byAge = false
					, bool byClassLevel = false
					, int? selectedYear = null
					, string selectedClassLevel = null)

		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
								, selectedYear, selectedClassLevel)
								.ElectorateNCmd(selectedElectorate);
			return sqlExec(cmd);
		}

		public IDataResult EnrolSchoolType(string selectedSchoolType = null
					, bool reportFormat = false
					, bool byAge = false
					, bool byClassLevel = false
					, int? selectedYear = null
					, string selectedClassLevel = null)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(reportFormat, byAge, byClassLevel
								, selectedYear, selectedClassLevel)
								.SchoolTypeCmd(selectedSchoolType);
			return sqlExec(cmd);
		}

		public IDataResult EnrolISCED(int year)
		{
			SqlCommand cmd = new WarehouseEnrolCommands(false, false, false)
								.IscedCmd(year);
			return sqlExec(cmd);
		}
		#endregion


		#region Grid
		public IDataResult EnrolGridMaker(string groupBy, int? startYear, int? endYear, string filter, string dataItem,
			int? ignoreAge, int? rowTotalsOnly)
		{
			SqlCommand cmd = GridCmd(groupBy, startYear, endYear, filter, dataItem, ignoreAge, rowTotalsOnly);
			return sqlExec(cmd);
		}

		SqlCommand GridCmd(string groupBy, int? startYear, int? endYear, string filter, string dataItem,
			int? ignoreAge, int? rowTotalsOnly)
		{
			SqlCommand cmd = new SqlCommand();
			// generated by a tool
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "warehouse.gridMaker";

			cmd.Parameters.Add("@GroupBy", SqlDbType.NVarChar, 200).Value = groupBy;
			cmd.Parameters.Add("@StartYear", SqlDbType.Int).Value = startYear;
			cmd.Parameters.Add("@EndYear", SqlDbType.Int).Value = endYear;
			cmd.Parameters.Add("@DataItem", SqlDbType.NVarChar, 20).Value = dataItem;
			cmd.Parameters.Add("@IgnoreAge", SqlDbType.Int).Value = ignoreAge ?? 0;
			cmd.Parameters.Add("@rowTotalsOnly", SqlDbType.Int).Value = rowTotalsOnly ?? 0;
			cmd.Parameters.Add("@filter", SqlDbType.NVarChar, 2048).Value = filter;

			return cmd;
		}
		#endregion
	}
}
