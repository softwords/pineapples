﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		public IDataResult DimensionLevel()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = "Select * from DimensionLevel";
			return sqlExec(cmd);
		}
	}
}
