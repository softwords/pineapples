﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		/**
 * Report on teacher activity
 * Content - not used
 * Grouping  null, district school
 */
		public IDataResult EdLevelAge( string grouping = null, string groupValue = null
									, bool? report = false)
		{

			string r = report == null ? "" : ((bool)report ? "R" : "");
			string selector = $"{grouping}code";
			switch (grouping.ToLower())
			{
				case "school":
					selector = "schNo";
					break;
				case "table":
				case "nation":
					selector = null;
					break;

			}
			string whereclause = (selector != null) ? $" WHERE {selector} = @Selected OR @Selected is null" : "";
			string sql =
$@"Select * from warehouse.EdLevelAge{grouping}{r} {whereclause} ORDER BY SurveyYear";

			SqlCommand cmd = new SqlCommand(sql);
			cmd.Parameters.Add("@Selected", SqlDbType.NVarChar, 50).Value = (object)groupValue ?? DBNull.Value;
			return sqlExec(cmd);
		}
	}
}
