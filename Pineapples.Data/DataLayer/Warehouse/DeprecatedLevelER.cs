﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		// For Indicators Dashboards
		public IDataResult ClassLevelER()
		{
			SqlCommand cmd = classLevelERCmd();
			return sqlExec(cmd);
		}

		public IDataResult EdLevelER()
		{
			SqlCommand cmd = edLevelERCmd();
			return sqlExec(cmd);
		}

		public IDataResult FlowRates()
		{
			SqlCommand cmd = flowRatesCmd();
			return sqlExec(cmd);
		}
		// For Students dashboards
		// contains all of DistrictEdLevelAge and NationEdLevelAge
		public IDataResult EdLevelAge()
		{
			SqlCommand cmd = edLevelAgeCmd();
			return sqlExec(cmd);
		}


		#region commands
		private SqlCommand edLevelAgeCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                    SELECT[surveyYear]
      ,[ClassLevel]
      ,[districtCode]
      ,ELA.[AuthorityCode]
	  , AU.AuthorityGroupCode AuthorityGovtCode
      ,[SchoolTypecode]
      ,[yearOfEd]
      ,[EdLevel]
      , sum(case Gendercode when 'M' then UnderAge else null end) UnderAgeM
      ,sum(case Gendercode when 'M' then OfficialAge else null end) OfficialAgeM
      ,sum(case Gendercode when 'M' then OverAge else null end) OverAgeM
      ,sum(case Gendercode when 'M' then Enrol else null end) EnrolM
      ,sum(case Gendercode when 'M' then EstimatedEnrol else null end) EstimatedEnrolM
	  ,sum(case Gendercode when 'F' then UnderAge else null end) UnderAgeF
      ,sum(case Gendercode when 'F' then OfficialAge else null end) OfficialAgeF
      ,sum(case Gendercode when 'F' then OverAge else null end) OverAgeF
      ,sum(case Gendercode when 'F' then Enrol else null end) EnrolF
      ,sum(case Gendercode when 'F' then EstimatedEnrol else null end) EstimatedEnrolF
  FROM[warehouse].[EdLevelAge]
        ELA
   LEFT JOIN DimensionAuthority AU
       ON ELA.AuthorityCode = AU.AuthorityCode
 GROUP BY
      [surveyYear]
      ,[ClassLevel]
      ,[districtCode]
      ,ELA.[AuthorityCode]
	  , AU.AuthorityGroupCode
      ,[SchoolTypecode]
      ,[yearOfEd]
      ,[EdLevel];";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		private SqlCommand edLevelERCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
             SELECT [SurveyYear]
      ,[edLevelCode] AS EducationLevel
      ,[districtCode] AS DistrictCode
      ,[enrolM] AS EnrolmentM
      ,[enrolF] AS EnrolmentF
      ,[enrol] AS EnrolmentT
      ,[repM] AS RepeatersM
      ,[repF] AS RepeatersF
      ,[rep] AS RepeatersT
      ,[intakeM] AS IntakeM
      ,[intakeF] AS IntakeF
      ,[intake] AS IntakeMT
      ,[nEnrolM] AS NetEnrolmentM
      ,[nEnrolF] AS NetEnrolmentF
      ,[nEnrol] AS NetEnrolmentT
      ,[nRepM] AS NetRepeatersM
      ,[nRepF] AS NetRepeatersF
      ,[nRep] AS NetRepeatersT
      ,[nIntakeM] AS NetIntakeM
      ,[nIntakeF] AS NetIntakeF
      ,[nIntake] AS NetIntakeT
      ,[popM] AS PopulationM
      ,[popF] AS PopulationF
      ,[pop] AS PopulationT
      ,[firstYear] AS FirstYearOfEducationLevel
      ,[lastYear] AS LastYearOfEducationLevel
      ,[numYears] AS NumberOFYearsInEducationLevel
	  ,100.0 * [enrolM] / [popM] AS GERM /* from here all percentages to be rounded to single decimal */
	  ,100.0 * [enrolF] / [popF] AS GERF
	  ,100.0 * [enrol] / [pop] AS GER
	  ,100.0 * [nEnrolM] / [popM] AS NERM
	  ,100.0 * [nEnrolF] / [popF] AS NERF
	  ,100.0 * [nEnrol] / [pop] AS NER
	  ,100.0 * [intakeM] / [popM] AS GIRM
	  ,100.0 * [intakeF] / [popF] AS GIRF
	  ,100.0 * [intake] / [pop] AS GIR
	  ,100.0 * [nIntakeM] / [popM] AS NIRM
	  ,100.0 * [nIntakeF] / [popF] AS NIRF
	  ,100.0 * [nIntake] / [pop] AS NIR
  FROM [warehouse].[EdLevelERDistrict]";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		#endregion
	}
}
