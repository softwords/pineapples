﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse : BaseRepository, IDSWarehouse
	{
		public DSWarehouse(DB.PineapplesEfContext cxt) : base(cxt) { }

		#region version management
		public string WarehouseVersion()
		{
			string cmdText = @"Select cast(versionID as nvarchar(50)) ID from warehouse.VersionInfo";
			SqlCommand cmd = new SqlCommand(cmdText);
			IDataResult result = sqlExec(cmd);
			DataTable dt = result.ResultSet as DataTable;
			return dt.Rows[0]["ID"].ToString();
		}
		#endregion


		//===========================================================================
		#region Flow Rates

		public IDataResult FlowSchool(string selectedSchool = null
				, bool reportFormat = false
				, bool asPerc = false)
		{
			SqlCommand cmd = new WarehouseFlowCommands(reportFormat, asPerc)
								.SchoolCmd(selectedSchool);
			return sqlExec(cmd);
		}

		public IDataResult FlowDistrict(string selectedDistrict = null
				, bool reportFormat = false
				, bool asPerc = false)
		{
			SqlCommand cmd = new WarehouseFlowCommands(reportFormat, asPerc)
								.DistrictCmd(selectedDistrict);
			return sqlExec(cmd);
		}

		public IDataResult FlowNation(bool reportFormat = false
				, bool asPerc = false)
		{
			SqlCommand cmd = new WarehouseFlowCommands(reportFormat, asPerc)
								.NationCmd();
			return sqlExec(cmd);
		}

		// deprecated - use FlowSchool
		public IDataResult SchoolFlowRates(string schoolNo)
		{
			SqlCommand cmd = schoolFlowRatesCmd(schoolNo);
			return sqlExec(cmd);
		}
		public IDataResult AllSchoolFlowRates()
		{
			SqlCommand cmd = schoolFlowRatesCmd(null);
			return sqlExec(cmd);
		}

		#endregion flow


		#region Wash

		public IDataResult Wash(string content
							, string grouping, string groupValue
							, bool? report)
		{
			SqlCommand cmd = null;
			var wac = new WarehouseWashCommands(content, grouping, report);
			cmd = wac.BaseCommand(groupValue);
			return sqlExec(cmd);
		}

		public IDataResult WashFilter(SchoolAccreditationFilter fltr)
		{
			SqlCommand cmd = WarehouseWashCommands
								.FilterCmd(fltr);
			return sqlExec(cmd);
		}
		public IDataResult WashResponses(WashResponsesFilter fltr)
		{
			SqlCommand cmd = WarehouseWashCommands
								.ResponsesCmd(fltr);
			return sqlExec(cmd, false);
		}
		public IDataResult WashQuestions()
		{
			SqlCommand cmd = WarehouseWashCommands
								.WashQuestions();
			return sqlExec(cmd);
		}
		public IDataResult WashToilets()
		{
			SqlCommand cmd = WarehouseWashCommands
								.WashToilets();
			return sqlExec(cmd);
		}
		public IDataResult WashWater()
		{
			SqlCommand cmd = WarehouseWashCommands
								.WashWater();
			return sqlExec(cmd);
		}
		#endregion
		#region FinancialData

		public IDataResult EdExpenditure(string grouping
											, string groupValue
											, bool? report)
		{
			SqlCommand cmd = null;

			var wc = new WarehouseFinancialDataCommands(null, grouping, report);
			cmd = wc.EdExpCommand(groupValue);
			return sqlExec(cmd);

		}
		#endregion

		#region Special Education
		public IDataResult SpecialEducation(bool reportFormat = false)
		{
			var cmds = new WarehouseSpEdCommands(reportFormat);
			SqlCommand cmd = cmds.SpecialEdCmd();
			return sqlExec(cmd);
		}
		#endregion
		#region Teachers
		// For Teachers Dashboards
		public IDataResult TeacherCount()
		{
			SqlCommand cmd = teacherCountCmd();
			return sqlExec(cmd);
		}

		public IDataResult TeacherCountX()
		{
			SqlCommand cmd = teacherCountXCmd();
			return sqlExec(cmd);
		}

		public IDataResult TeacherPupilRatio()
		{
			SqlCommand cmd = teacherPupilRatioCmd();
			return sqlExec(cmd);
		}
		#endregion



		#region Commands

		private SqlCommand tableEnrolBySchoolCmd(string schNo)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = @"
			  select 
				SurveyYear,
				ClassLevel,
				Age,
				EnrolF,
				EnrolM,
				Enrol
			  from warehouse.enrolR
			  where schNo = @SchNo";
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}



		private SqlCommand contentsOf(string warehouseTableName, bool reportFormat = false)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			warehouseTableName += reportFormat ? "R" : "";
			cmd.CommandText = @"Select * from " + warehouseTableName;
			cmd.CommandTimeout = 300;            // 5 mins
			return cmd;
		}


		// version that disaggregates on gender
		private SqlCommand tableEnrolCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
SELECT [SurveyYear]
      ,[ClassLevel]
      ,[Age]
      ,[DistrictCode]
      ,TE.[AuthorityCode]
	  , AU.AuthorityGroupCode AuthorityGovt
	   , GenderCode
      ,[SchoolTypeCode]
      , sum(enrol) Enrol
  FROM [warehouse].[tableEnrol] TE
	LEFT JOIN DimensionAuthority AU
		ON TE.AuthorityCode = AU.AuthorityCode
  GROUP BY 
		SurveyYear
		, [ClassLevel]
		, [Age]
		, [DistrictCode]
		, TE.[AuthorityCode]
		, [SchoolTypeCode]
		, AU.AuthorityGroupCode
		, GenderCode";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		// version that denormalises gender
		private SqlCommand tableEnrolCmdReport()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                    SELECT [SurveyYear]
      ,[ClassLevel]
      ,[Age]
      ,[DistrictCode]
      ,TE.[AuthorityCode]
	  , AU.AuthorityGroupCode AuthorityGovt
      ,[SchoolTypeCode]
      , sum(case Gendercode when 'M' then enrol else null end) EnrolM
	  , sum(case Gendercode when 'F' then enrol else null end) EnrolF
  FROM [warehouse].[tableEnrol] TE
	LEFT JOIN DimensionAuthority AU
		ON TE.AuthorityCode = AU.AuthorityCode
  GROUP BY 
  SurveyYear
      ,[ClassLevel]
      ,[Age]
      ,[DistrictCode]
      ,TE.[AuthorityCode]
      ,[SchoolTypeCode]
	  ,  AU.AuthorityGroupCode";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		// version that disaggregates on gender
		private SqlCommand tableEnrolXCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
SELECT [SurveyYear]
      ,[ClassLevel]
      ,[Age]
      ,[GenderCode]
      ,[DistrictCode]
	  ,[District]
      ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode] AuthorityGovtCode
	  ,[AuthorityGroup] AuthorityGovt
      ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[IslandCode]
	  ,[Island]
	  ,[RegionCode]
	  ,[Region]
      , sum(Enrol) Enrol
  FROM [warehouse].[tableEnrolX] TE
  GROUP BY [SurveyYear]
      ,[ClassLevel]
      ,[Age]
      ,[GenderCode]
      ,[DistrictCode]
	  ,[District]
      ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode]
	  ,[AuthorityGroup]
      ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[IslandCode]
	  ,[Island]
	  ,[RegionCode]
	  ,[Region]";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		// version that denormalises gender
		private SqlCommand tableEnrolXCmdReport()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
SELECT [SurveyYear]
      ,[ClassLevel]
      ,[Age]
      ,[DistrictCode]
	  ,[District]
      ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode] AuthorityGovtCode
	  ,[AuthorityGroup] AuthorityGovt
      ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[IslandCode]
	  ,[Island]
	  ,[RegionCode]
	  ,[Region]
      , sum(case GenderCode when 'M' then enrol else null end) EnrolM
	  , sum(case GenderCode when 'F' then enrol else null end) EnrolF
  FROM [warehouse].[tableEnrolX] TE
  GROUP BY [SurveyYear]
      ,[ClassLevel]
      ,[Age]
      ,[DistrictCode]
	  ,[District]
      ,[AuthorityCode]
	  ,[Authority]
	  ,[AuthorityGroupCode]
	  ,[AuthorityGroup]
      ,[SchoolTypeCode]
	  ,[SchoolType]
	  ,[IslandCode]
	  ,[Island]
	  ,[RegionCode]
	  ,[Region]";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}


		private SqlCommand schoolFlowRatesCmd(string schoolNo)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                    SELECT [schNo]
      ,[surveyYear]
      ,[YearOfEd]
      ,100.0 * [RepeatRate] as RepeatRate /* from here all percentages to be rounded to single decimal */
      ,100.0 * [PromoteRate] as PromoteRate
      ,100.0 * [DropoutRate] as DropoutRate
      ,100.0 * [SurvivalRate] as SurvivalRate
  FROM [warehouse].[SchoolFlow]
 WHERE (schNo = @SchoolNo or @schoolNo is null)";
			cmd.CommandTimeout = 300;            // 5 mins
			cmd.Parameters.Add("@schoolNo", SqlDbType.NVarChar, 50)
					.Value = (object)(schoolNo) ?? DBNull.Value;

			return cmd;
		}

		private SqlCommand schoolTeacherCountCmd(string schoolNo)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                    SELECT [SchNo]
      ,SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass
	  , sum(case GenderCode when 'M' then NumTeachers else null end) NumTeachersM
	  , sum(case GenderCode when 'F' then NumTeachers else null end) NumTeachersF
	  , sum(case GenderCode when 'M' then Certified else null end) CertifiedM
	  , sum(case GenderCode when 'F' then Certified else null end) CertifiedF
	  , sum(case GenderCode when 'M' then Qualified else null end) QualifiedM
	  , sum(case GenderCode when 'F' then Qualified else null end) QualifiedF
	  , sum(case GenderCode when 'M' then CertQual else null end) CertQualM
	  , sum(case GenderCode when 'F' then CertQual else null end) CertQualF
  FROM warehouse.TeacherCountSchool T
        LEFT JOIN DimensionAuthority AU
		            ON T.AuthorityCode = AU.AuthorityCode
WHERE (schNo = @schoolNo or @schoolNo is null)
GROUP BY 
      SchNo
      ,SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode 
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass";
			cmd.CommandTimeout = 300;            // 5 mins
			cmd.Parameters.Add("@schoolNo", SqlDbType.NVarChar, 50)
					.Value = (object)(schoolNo) ?? DBNull.Value;

			return cmd;
		}

		private SqlCommand schoolTeacherPupilRatioCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                      SELECT [SchNo]
      ,SurveyYear
      ,DistrictCode
      ,STPR.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector      
      ,[NumTeachers]
      ,[Certified]
      ,[Qualified]
      ,[CertQual]
      ,[Enrol]
  FROM warehouse.[SchoolTeacherPupilRatio] STPR
        LEFT JOIN DimensionAuthority AU
		            ON STPR.AuthorityCode = AU.AuthorityCode";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}
		private SqlCommand teacherPupilRatioCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
                      SELECT SurveyYear
      ,DistrictCode
      ,STPR.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector      
      ,sum([NumTeachers]) AS NumTeachers
      ,sum([Certified]) AS Certified
      ,sum([Qualified]) AS Qualified
      ,sum([CertQual]) AS CertQual
	  ,sum([Enrol]) AS Enrol
  FROM warehouse.[SchoolTeacherPupilRatio] STPR
        LEFT JOIN DimensionAuthority AU
		            ON STPR.AuthorityCode = AU.AuthorityCode
GROUP BY
	  SurveyYear
      ,DistrictCode
      ,STPR.AuthorityCode
	  , AU.AuthorityGroupCode
      ,SchoolTypeCode
      ,Sector";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		private SqlCommand classLevelERCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
             SELECT [SurveyYEar] AS SurveyYear
      ,[ClassLevel] AS Grade
      ,[OfficialAge]
      ,[districtCode] AS DistrictCode
      ,[enrolM] AS EnrolmentM
      ,[enrolF] AS EnrolmentF
      ,[enrol] AS EnrolmentT
      ,[repM] AS RepeatersM
      ,[repF] AS RepeatersF
      ,[rep] AS RepeatersT
      ,[psaM] AS PreSchoolAttendersM
      ,[psaF] AS PreSchoolAttendersF
      ,[psa] AS PreSchoolAttendersT
      ,[intakeM] AS IntakeM
      ,[intakeF] AS IntakeF
      ,[intake] AS IntakeMT
      ,[nEnrolM] AS NetEnrolmentM
      ,[nEnrolF] AS NetEnrolmentF
      ,[nEnrol] AS NetEnrolmentT
      ,[nRepM] AS NetRepeatersM
      ,[nRepF] AS NetRepeatersF
      ,[nRep] AS NetRepeatersT
      ,[nIntakeM] AS NetIntakeM
      ,[nIntakeF] AS NetIntakeF
      ,[nIntake] AS NetIntakeT
      ,[popM] AS PopulationM
      ,[popF] AS PopulationF
      ,[pop] AS PopulationT
	  ,100.0 * [enrolM] / [popM] AS GERM /* from here all percentages to be rounded to single decimal */
	  ,100.0 * [enrolF] / [popF] AS GERF
	  ,100.0 * [enrol] / [pop] AS GER
	  ,100.0 * [nEnrolM] / [popM] AS NERM
	  ,100.0 * [nEnrolF] / [popF] AS NERF
	  ,100.0 * [nEnrol] / [pop] AS NER
	  ,100.0 * [intakeM] / [popM] AS GIRM
	  ,100.0 * [intakeF] / [popF] AS GIRF
	  ,100.0 * [intake] / [pop] AS GIR
	  ,100.0 * [nIntakeM] / [popM] AS NIRM
	  ,100.0 * [nIntakeF] / [popF] AS NIRF
	  ,100.0 * [nIntake] / [pop] AS NIR
  FROM [warehouse].[classLevelERDistrict]";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}


		private SqlCommand flowRatesCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;

			cmd.CommandText = @"
             SELECT [SurveyYear]
      ,[YearOfEd]
      ,[DistrictCode]
	  ,sum(case GenderCode when 'M' then [RepeatRate] else null end) [RepeatRateM] /* from here all percentages to be rounded to single decimal */
	  ,sum(case GenderCode when 'F' then [RepeatRate] else null end) [RepeatRateF]
	  ,sum(case GenderCode when 'M' then [PromoteRate] else null end) [PromoteRateM] 
	  ,sum(case GenderCode when 'F' then [PromoteRate] else null end) [PromoteRateF]
	  ,sum(case GenderCode when 'M' then [DropoutRate] else null end) [DropoutRateM] 
	  ,sum(case GenderCode when 'F' then [DropoutRate] else null end) [DropoutRateF]
	  ,sum(case GenderCode when 'M' then [SurvivalRate] else null end) [SurvivalRateM]
	  ,sum(case GenderCode when 'F' then [SurvivalRate] else null end) [SurvivalRateF]
  FROM [warehouse].[DistrictFlow]
  GROUP BY
   [SurveyYear]
   ,[YearOfEd]
   ,[DistrictCode]";
			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}



		private SqlCommand teacherCountCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			// denormalise on gender
			cmd.CommandText = @"
SELECT SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass
	  , sum(case GenderCode when 'M' then NumTeachers else null end) NumTeachersM
	  , sum(case GenderCode when 'F' then NumTeachers else null end) NumTeachersF
	  , sum(case GenderCode when 'M' then Certified else null end) CertifiedM
	  , sum(case GenderCode when 'F' then Certified else null end) CertifiedF
	  , sum(case GenderCode when 'M' then Qualified else null end) QualifiedM
	  , sum(case GenderCode when 'F' then Qualified else null end) QualifiedF
	  , sum(case GenderCode when 'M' then CertQual else null end) CertQualM
	  , sum(case GenderCode when 'F' then CertQual else null end) CertQualF
  FROM warehouse.TeacherCountschool T
        LEFT JOIN DimensionAuthority AU
		            ON T.AuthorityCode = AU.AuthorityCode
GROUP BY 
SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode 
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass";

			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		private SqlCommand teacherCountXCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			// denormalise on gender
			cmd.CommandText = @"
SELECT 
  [SurveyYear],
  [AgeGroup],
  TCS.[DistrictCode],
  DSS.[Island Code] AS [IslandCode],
  DSS.[Region Code] AS [RegionCode],
  DSS.[AuthorityCode],
  DSS.[AuthorityGovtCode] AS [AuthorityGovtCode],
  DSS.[SchoolTypeCode],
  [Sector],
  [ISCEDSubClass] AS [ISCEDSubClassCode],
  SUM(CASE WHEN GenderCode = 'M' THEN NumTeachers ELSE NULL END) AS NumTeachersM,
  SUM(CASE WHEN GenderCode = 'F' THEN NumTeachers ELSE NULL END) AS NumTeachersF,
  SUM(CASE WHEN GenderCode NOT IN ('M', 'F') OR GenderCode IS NULL THEN NumTeachers ELSE NULL END) AS NumTeachersNA,
  SUM(CASE WHEN GenderCode = 'M' THEN Certified ELSE NULL END) AS CertifiedM,
  SUM(CASE WHEN GenderCode = 'F' THEN Certified ELSE NULL END) AS CertifiedF,
  SUM(CASE WHEN GenderCode NOT IN ('M', 'F') OR GenderCode IS NULL THEN Certified ELSE NULL END) AS CertifiedNA,
  SUM(CASE WHEN GenderCode = 'M' THEN Qualified ELSE NULL END) AS QualifiedM,
  SUM(CASE WHEN GenderCode = 'F' THEN Qualified ELSE NULL END) AS QualifiedF,
  SUM(CASE WHEN GenderCode NOT IN ('M', 'F') OR GenderCode IS NULL THEN Qualified ELSE NULL END) AS QualifiedNA,
  SUM(CASE WHEN GenderCode = 'M' THEN CertQual ELSE NULL END) AS CertQualM,
  SUM(CASE WHEN GenderCode = 'F' THEN CertQual ELSE NULL END) AS CertQualF,
  SUM(CASE WHEN GenderCode NOT IN ('M', 'F') OR GenderCode IS NULL THEN CertQual ELSE NULL END) AS CertQualNA
FROM 
  [warehouse].[TeacherCountSchool] TCS
INNER JOIN 
  [warehouse].[dimensionSchoolSurvey] DSS 
ON 
  TCS.SchNo = DSS.[School No] AND TCS.SurveyYear = DSS.[Survey Data Year]
GROUP BY 
  [SurveyYear],
  [AgeGroup],
  TCS.[DistrictCode],
  DSS.[Island Code],
  DSS.[Region Code],
  DSS.[AuthorityCode],
  DSS.[AuthorityGovtCode],
  DSS.[SchoolTypeCode],
  [Sector],
  [ISCEDSubClass]";

			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		private SqlCommand teacherQualCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			// denormalise on gender
			cmd.CommandText = @"
SELECT TI.[tID]
	  ,TI.tGiven AS FirstName
	  ,TI.tSurname AS LastName
      ,[yr] As Year
      ,[tchQual] AS QualificationCode
	  ,[codeDescription] AS Qualification
	  ,[codeGroup] AS QualificationType
  FROM [warehouse].[TeacherQual] AS TQ
  INNER JOIN [dbo].[lkpTeacherQual] AS TQL ON TQ.tchQual = TQL.codeCode
  INNER JOIN [dbo].TeacherIdentity AS TI ON TQ.tID = TI.tID";

			cmd.CommandTimeout = 300;            // 5 mins

			return cmd;
		}

		private SqlCommand AccreditationsCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = @"Select * from warehouse.tableInspections WHERE InspectionTypeCode = 'SCHOOL_ACCREDITATION'";
			cmd.CommandTimeout = 300;
			return cmd;
		}

		private SqlCommand StandardsCmd()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = @"Select * from warehouse.tableStandards";
			cmd.CommandTimeout = 300;
			return cmd;
		}
		#endregion
	}
}