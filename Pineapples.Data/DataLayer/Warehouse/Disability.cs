﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
	public partial class DSWarehouse
	{
		//================================================================================
		#region Disability

		//public IDataResult TableDisability(bool reportFormat = false)
		//{
		//	SqlCommand cmd = (reportFormat ? tableEnrolCmdReport() : tableEnrolCmd());
		//	return sqlExec(cmd);
		//}

		public IDataResult DisabilitySchool(string selectedSchoolNo = null
								, bool reportFormat = false
								, bool byAge = false
								, bool byClassLevel = false)
		{
			SqlCommand cmd = new WarehouseDisabilityCommands(reportFormat, byAge, byClassLevel)
											.SchoolCmd(selectedSchoolNo);
			return sqlExec(cmd);
		}

		#endregion Disability
	}
}