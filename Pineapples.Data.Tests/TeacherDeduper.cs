﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Pineapples.Data.Tests
{
	[TestClass]
	public class TeacherDeduper
	{

	   [TestMethod]
		public void Read()
		{
			//
			// TODO: Add test logic here
			//
			DataLayer.IDSFactory factory = new DataLayer.DSFactory();
			IDSTeacherDeduper dp = factory.TeacherDeduper();
			TeacherDeduperFilter fltr = new TeacherDeduperFilter()
			{
				PageSize = 50,
				MatchGiven = 1,
				MatchSurname = 1
			};
			object result = dp.Filter(fltr);
		}
	}
}