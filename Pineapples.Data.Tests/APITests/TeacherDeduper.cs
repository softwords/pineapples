﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Configuration;


namespace Pineapples.Data.Tests.APITests
{
	
	[TestClass]
	public class TeacherDeduper
	{
		static HttpClient client;
		static string bearertoken = String.Empty;
		[ClassInitialize()]
		public static void Initialize(TestContext testContext)
		{
			client = new HttpClient();
			client.BaseAddress = new Uri("https://localhost:44301/");
			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));
			Login();

		}
		[TestMethod]
		public async Task Filter()
		{

			var filter = (dynamic)new JObject();
			filter.pageNo = 1;
			filter.pageSize = 27;
			filter.MatchGiven = 1;
			filter.MatchSurname = 1;
			filter.MatchDob = 0;
			var content = new StringContent(filter.ToString(), Encoding.UTF8, "application/json");
			HttpResponseMessage response = await client.PostAsync("api/teacherdeduper/collection/filter",
				content);
			response.EnsureSuccessStatusCode();
			var jsonstring = await response.Content.ReadAsStringAsync();
			dynamic payload = Newtonsoft.Json.Linq.JObject.Parse(jsonstring);
			int page = payload.PageNo;
			Debug.Assert(page == 1);
		}
		[TestMethod]
		public async Task Surveys()
		{
			HttpResponseMessage response = await client.GetAsync("api/teacherdeduper/surveys/34/44");
			response.EnsureSuccessStatusCode();
			var jsonstring = await response.Content.ReadAsStringAsync();
			dynamic payload = Newtonsoft.Json.Linq.JObject.Parse(jsonstring);
			int page = payload.PageNo;
			Debug.Assert(page == 1);
		}

		//[TestInitialize]
		public static void Login()
		{
			if (bearertoken == string.Empty)
			{
				Dictionary<string, string> p = new Dictionary<string, string>();
				p.Add("grant_type", "password");
				p.Add("username", ConfigurationManager.AppSettings["username"]);
				p.Add("password", ConfigurationManager.AppSettings["password"]);

				string url = "api/token";
				HttpResponseMessage response = client.PostAsync(url, new FormUrlEncodedContent(p)).Result;
				var token = response.Content.ReadAsStringAsync().Result;
				dynamic d = Newtonsoft.Json.Linq.JObject.Parse(token);
				bearertoken = d.access_token;
				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearertoken);
			}
		}
	}

}
