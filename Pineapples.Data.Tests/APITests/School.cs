﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Configuration;

namespace Pineapples.Data.Tests.APITests
{
	[TestClass]
	public class School
	{
		static HttpClient client;
		static string bearertoken = String.Empty;
		[ClassInitialize()]
		public static void Initialize(TestContext testContext)
		{
			client = new HttpClient();
			client.BaseAddress = new Uri("https://localhost:44301/");
			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));
			Login();

		}
		[TestMethod]
		public async Task ReadSchool()
		{
			
			HttpResponseMessage response = await client.GetAsync("api/schools/CHK002");
			response.EnsureSuccessStatusCode();
			var jsonstring = await response.Content.ReadAsStringAsync();
			dynamic payload = Newtonsoft.Json.Linq.JObject.Parse(jsonstring);
			string schNo = payload.ResultSet.schNo;
			Debug.Assert(schNo == "CHK002");
		}
		[TestMethod]
		public async Task FilterSchools()
		{
			
			var filter = (dynamic)new JObject();
			filter.pageNo = 1;
			filter.pageSize = 27;
			var content = new StringContent(filter.ToString(), Encoding.UTF8, "application/json");
			HttpResponseMessage response = await client.PostAsync("api/schools/collection/filter",
				content);
			response.EnsureSuccessStatusCode();
			var jsonstring = await response.Content.ReadAsStringAsync();
			dynamic payload = Newtonsoft.Json.Linq.JObject.Parse(jsonstring);
			int page = payload.PageNo;
			Debug.Assert(page == 1);
		}
		//[TestInitialize]
		public static void Login()
		{
			if (bearertoken == string.Empty)
			{
				Dictionary<string, string> p = new Dictionary<string, string>();
				p.Add("grant_type", "password");
				p.Add("username", ConfigurationManager.AppSettings["username"]);
				p.Add("password", ConfigurationManager.AppSettings["password"]);

				string url = "api/token";
				HttpResponseMessage response = client.PostAsync(url, new FormUrlEncodedContent(p)).Result;
				var token = response.Content.ReadAsStringAsync().Result;
				dynamic d = Newtonsoft.Json.Linq.JObject.Parse(token); 
				bearertoken = d.access_token;
				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearertoken);
			}
		}
	}

	
}
