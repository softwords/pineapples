﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pineapples.Data.DataLayer;

namespace Pineapples.Data.Tests
{
    [TestClass]
    public class TeacherLinks
    {
        [TestMethod]
        public void TeacherLinkRead()
        {
            DataLayer.IDSFactory factory = new DataLayer.DSFactory();
            object result = factory.TeacherLink().Read(44);
            // to do
        }

        [TestMethod]
        public void TeacherLinkFilter()
        {
            DataLayer.IDSFactory factory = new DataLayer.DSFactory();
            TeacherLinkFilter fltr = new TeacherLinkFilter();
            fltr.Keyword = "Classroom";
            object result = factory.TeacherLink().Filter(fltr);
 
        }

    }
}
