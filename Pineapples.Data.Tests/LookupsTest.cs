﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.PerformanceData;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Pineapples.Data.Tests
{
  /// <summary>
  /// Summary description for Lookups
  /// </summary>
  [TestClass]
  public class LookupsTest
  {
    public LookupsTest()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestMethod]
    public void getCoreLookups()
    {
      //
      // ensure that the appropriate items are returned from the core lookups
      //
      Lookups lkp = new Lookups("localhost","siemis");
      DataSet ds = lkp.CoreLookups();

      Assert.AreEqual(7, ds.Tables.Count);

      Assert.IsNotNull(ds.Tables["electoraten"]);
      Assert.IsNotNull(ds.Tables["electoratel"]);
      Assert.IsNotNull(ds.Tables["listNames"],"listNames not defined");
    }
  }
}
